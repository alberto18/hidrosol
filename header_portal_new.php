<?php
include("variables_globales_gestproject.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110568121-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-110568121-1');
</script>



<meta http-equiv="x-ua-compatible" content="ie=edge">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="author" content=""> 
<meta charset="UTF-8">
<!--<title>Hidrosol Canarias</title>--> 

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- PLACE FAVICON.ICO IN THE ROOT DIRECTORY -->
<link rel="icon" href="documentos/favicon.png">

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1782137655183056');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1782137655183056&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code —>

<!-- CSS Style -->

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="all">
<link rel="stylesheet" type="text/css" href="css/simple-line-icons.css" media="all">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="css/owl.theme.css">
<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
<link rel="stylesheet" type="text/css" href="css/jquery.mobile-menu.css">
<link rel="stylesheet" type="text/css" href="css/revslider.css">
<link rel="stylesheet" type="text/css" href="css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="css/blog.css" media="all">




<link rel="stylesheet" type="text/css" href="css_portal/slider-pro.css">
<link rel="stylesheet" type="text/css" href="css_portal/slider-pro.min.css">





<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,600,500,700,800' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">




		<?php 
			$title_seo="Hidrosol Canarias";
			if($_REQUEST['file'] == 'ver_contenido'){
				$contenido_id = base64_decode(base64_decode($_REQUEST['id']));
				$cons = $conn->prepare("select title_seo, description_seo, keywords_seo from nuke_stories where id = :id");
				$cons->bindValue('id', $contenido_id, PDO::PARAM_INT);
				$cons->execute();					
				while($lin = $cons->fetch(PDO::FETCH_ASSOC)) {

					$title_seo = empty($lin['title_seo']) ? $title : $lin['title_seo'];
					$description_seo = substr($lin['description_seo'],0,50);
					$keywords_seo = $lin['keywords_seo'];
				


				
		
				}
			}	

			elseif($_REQUEST['file'] == 'ver_productos'){
				$contenido_id = base64_decode(base64_decode($_REQUEST['id']));
				$cons = $conn_pdo_erp->prepare("select * from articulos_t where id = :id");
				$cons->bindValue('id', $contenido_id, PDO::PARAM_INT);
				$cons->execute();					
				while($lin = $cons->fetch(PDO::FETCH_ASSOC)) {

					//$title_seo = empty($lin['title_seo']) ? $title : $lin['title_seo'];
					$description_seo = substr($lin['nombre'],0,50);
					$keywords_seo = $lin['etiquetas'];
				}
			}
			else{
				$keywords_seo ="canarias, hidrosol, piscinas, riego, talasoterapia, hidropresión, aire acondicionado, energía solar, climatización ";
				$description_seo =" Hidrosol Canarias, empresa lider en la distribución de productos relacionados con piscinas, riego, talasoterapia, hidropresión, aire acondicionado, energía solar, climatización,saunas, infrarrojos, spas, jacuzzis, turca, finlandesa, vapor, masaje, cervicales, cascada, masaje, solar, Termica, Fotovoltaica, fecales, sumergible, circuladoras, calefacción, clima, acondicionado, chimenea, conducto, gasoil, gas, biomasa, geotermia, eólica, ahorro, contadores, saunas, infrared, spas, jacuzzis, turkish, finnish, steam, massage, cervical, waterfall, massage, solar, thermal, photovoltaic, fecal, submersible, circulators, heating, climate, conditioning, chimney, duct, diesel, gas, biomass, geothermal, wind, saving, meters,sauna, infrarot, spas, jacuzzis, türkisch, finnisch, dampf, massage, cervical, wasserfall, massage, solar, thermal, photovoltaik, fäkalien, unterwasser, zirkulatoren, heizung, klima, konditionierung, schornstein, kanal, diesel, gas, biomasse, Geothermie, Wind, Speichern, Meter"; 
			}	

		
		
		?>
		
		<meta name="keywords" content="<?php echo $keywords_seo?>"/>
		<meta name="description" content="<?php echo $description_seo?>"/>
		<title><?php echo $title_seo ?></title>
		
		
		
</head>

<body class="common-home">
<div id="page"> 
  <!-- Header -->
  <header>
    <div class="header-container">
      <div class="header-top">
        <div class="container">
          <div class="row">
            <div class="header-text"> Distribuciones HIDROSOL CANARIAS </div>
			<div class="social" >
			
				<style>
					.goog-te-gadget-simple {
						
						color:#777;
						font-family: 'Montserrat', sans-serif;
						border: none;
						outline: none;						
						margin-top:-4px;			
						border: 1px #ddd solid;
						width: auto;
						
					}   
					.goog-te-menu-value{
						font-size: 12px !important;
						
					}
				</style>
				<ul>
					<li class="pull-left" > 
					<div  id="google_translate_element" style="display:inline-block;"></div>
						<div class="custom_select">
							<script type="text/javascript">
							function googleTranslateElementInit() {
							  new google.translate.TranslateElement({pageLanguage: 'es', includedLanguages: 'es,en,fr,de,it', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
							}
						</script>
					</div>
					<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


				</li>
                <!--<li class="fb pull-left"><a href="#"></a></li>
                <li class="tw pull-left"><a href="#" target="_blank"></a></li>
                <li class="googleplus pull-left"><a href="#" target="_blank"></a></li>
                <li class="rss pull-left"><a href="#" target="_blank"></a></li>
                <li class="pintrest pull-left"><a href="#" target="_blank"></a></li>
                <li class="linkedin pull-left"><a href="#" target="_blank"></a></li>
                <li class="youtube pull-left"><a href="#" target="_blank"></a></li>-->
				<li class="sobre pull-left"><a href="contactar" title="Contactar"></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12 logo-block"> 
            <!-- Header Logo -->
            <div class="logo"> <a title="Hidrosol Canarias" href="inicio-hidrosol"><img alt="Hidrosol Canarias" src="images/logo_hidrosol.png"> </a> 
			</div>
            <!-- End Header Logo --> 
          </div>
          <div class="col-lg-8 col-md-9 col-sm-9 col-xs-12 thm-top-cart">
            <div class="cart-account">
              
			  <div class="dropdown login">
                <div class="user-circle"> <a role="button" data-toggle="dropdown" data-target="#" class="dropdown-toggle" > <i class="fa fa-user" aria-hidden="true"></i></a>
                  <ul class="user-info dropdown-menu" role="menu">
                   
					<li>Bienvenido <?php echo ucfirst($_SESSION['tienda']['login']) ?> </li>

					<?php 
					if (!isset($_SESSION['tienda']['login'])){
					
					?>

				    <li><a href="login" title="Conecta con tu cuenta" rel="nofollow"> <span class="hidden-md-down">Entrar</span> </a> 
				    <li><a href="registro" title="Crea una cuenta de usuario" rel="nofollow"> <span class="hidden-md-down">Registrar</span> </a> </li>
				    <?php 
				   
				    }
				    else{
				    ?>
				   
				    <li><a href='perfil'><span class="hidden-md-down">Mi Perfil</span> </a> / <a href='mis-pedidos'><span class="hidden-md-down">Mis Pedidos</span> </a></li>
					<li><a href="logout" title="Desconectar" rel="nofollow"> <span class="hidden-md-down">Desconectar </span> </a> </li>
					
					
					<?php 
					}
					?>
                   
                  </ul>
                </div>
              </div>
			  
			  
			  
			  
			
		
			  
			  
           <div class="top-cart-contain pull-right"> 
                <!-- Top Cart -->
             <div class="mini-cart">
                  <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="shopping_cart.html"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> </a> </div>

					  <div>
						<div class="top-cart-content"> 
							
							<div class="open-panel">
								<ul class="mini-products-list" id="cart-sidebar">
								<?php 
								if (!isset($_SESSION['tienda']['articulos']) or ($_SESSION['tienda']['articulos'])==NULL){	
									echo "<li class='item first'>La cesta no contiene artículos.</li>";
								}
								else{
								
									foreach($_SESSION['tienda']['articulos'] as $id => $articulo){
									
									?>
															
										<li class="item first">
										  <div class="item-inner"> <a class="product-image" title="<?php echo $articulo['nombre'];?>"href="modules.php?mod=portal&file=ver_productos&id=<?php echo base64_encode(base64_encode($id));?>&categoria=<?php echo $categoria_cod ?>"><img alt="<?php echo $articulo['nombre'];?>" src="<?php echo $articulo['img'];?>" > </a>
											<div class="product-details">
											  <div class="access"><a class="btn-remove1" title="Eliminar este producto" href="modules.php?mod=tienda&file=eliminar_articulo_cesta&id=<?php echo base64_encode(base64_encode($id));?>&cantidad=<?php echo $articulo['cantidad'];?>">Eliminar</a> 
											  
											  <!--Editar-->
											  <a class="btn-edit" title="Edit item" href="modules.php?mod=portal&file=ver_productos&id=<?php echo base64_encode(base64_encode($id));?>&categoria=<?php echo $categoria_cod ?>"><i class="icon-pencil"></i><span class="hidden">Edit item</span></a> </div>
											  <!--access--><strong><?php echo $articulo['cantidad'];?></strong> x <span class="price"><?php 
							  $precio_coma=str_replace(".",",",$articulo['precio_web']);
							  echo $precio_coma ;
							  
							  ?> &#8364;</span>
											 
											 
											 
											 
											  <p class="product-name"><a href="modules.php?mod=portal&file=ver_productos&id=<?php echo base64_encode(base64_encode($id));?>&categoria=<?php echo $categoria_cod ?>"> <?php echo $articulo['nombre'];?></a> </p>
											</div>
										  </div>
										</li>
								
									
									<?php
								
									}
								}
								
								?>
								</ul>
								
								<?php
									
								if(isset($_SESSION['tienda']['cesta']['total_precio'])){
								
										if( intval($_SESSION['tienda']['cesta']['total_precio']) > 0){
								?>
											
											
										  <div class="actions">
											<a href="mi-cesta" class="btn btn-main pull-right bold higher">COMPRAR<i class="icon-shopping-cart"></i></a>
											<a href="#" class="btn btn-main pull-right bold higher" class="view-cart"><span>Total: <?php 
											
											
											$precio_coma=str_replace(".",",",$_SESSION['tienda']['cesta']['total_precio']);
							 
							  
											echo !isset($_SESSION['tienda']['cesta']['total_precio']) ? 0 : 
											
											$precio_coma;?>&euro; </span></a>
											</div>
										
								<?php 
										}
									}
									
									
								?>	
							</div><!-- fin class open-panel-->
						</div>
			 <!-- /cart -->
					</div>
				</div>
			  
               
              </div>
			  <!--fin carrito -->
           </div>
            <div class="care tmservicecms">
              <div class="text1">Contacto</div>
              <div class="text2">info@hidrosolcanarias.com</div>
			  <div class="text2">+34 928 184 180</div>
            </div>
			<div class="chat tmservicecms">
			
			
			
					<div class="text1">Bienvenido venta online <?php echo ucfirst($_SESSION['tienda']['login']) ?> </div>
			
					<?php 
						if (!isset($_SESSION['tienda']['login'])){
					
					?>

						<div class="text2"><a href='login'>Entrar </a></div>
						<div class="text2"><a href='registro'>Registrar</a></div>
					<?php 
						}
						else{
				    ?>
				   
					<div class="text2"><a href='perfil'>Mi Perfil </a> / <a href='mis-pedidos'>Mis Pedidos </a></div>
					<!-- Aqui-->
					<div class="text2"><a href="logout" title="Desconectar" rel="nofollow"> Desconectar</a></div> 


				
					<?php 
					}
					?>
                   
				
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- end header --> 
  
  <!-- Navigation -->
  <nav class="">
    <div class="container">
      <div class="mm-toggle-wrap">
        <div class="mm-toggle"><i class="fa fa-bars"></i><span class="mm-label">Menu</span> </div>
      </div>
      <div class="nav-inner"> 
        <!-- BEGIN NAV -->
        <ul id="nav" class="hidden-xs">
          <li class="level0 parent drop-menu" id="nav-home"><a href="modules.php?mod=portal&file=index" class="level-top"><span>Inicio</span></a> </li>
          <li class="level0 nav-6 level-top drop-menu"> <a class="level-top" href="representaciones"> <span>Representaciones</span> </a>
            
          </li>
	 
            <!--level0-wrapper dropdown-6col--> 
            <!--mega menu-->
          <li class="mega-menu"> <a class="level-top" href="tienda"><span>VENTA ONLINE</span></a>
            <div class="level0-wrapper dropdown-6col">
              <div class="container">
                <div class="level0-wrapper2"> 
             
                  <div class="nav-block nav-block-center"> 
                    <!--mega menu-->
                    <ul class="level0">			
					
					  <?php
					  
						$cons = $conn_pdo_erp->prepare("select * from maestro_categorias_articulos_t where mostrar_web='on' order by orden asc limit 6");
						try { 
							$cons->execute();
						} catch (PDOException $e) { 
							echo "Error en la consulta: ".$e->getMessage(); 
						} 

						while($lin = $cons->fetch(PDO::FETCH_ASSOC)) {
							$exito = 1;
							$id_categoria = $lin['id'];
							$nombre = $lin['nombre'];
							$img_categoria=$lin['nombre_fichero'];
							
							
							?>
							
							<li class="level3 nav-6-1 parent item"> <a href="modules.php?mod=portal&file=listado_productos&categoria=<?php echo $id_categoria ?>"><span><?php echo $lin['nombre']; ?></span></a>
								<ul class="level1">
								
								  <?php 
								
									$cons2 = $conn_pdo_erp->prepare("select * from articulos_t where (categoria_id=:categoria_id or categoria_id2=:categoria_id2 or categoria_id3=:categoria_id3)  and venta_en_web='on' and de_baja<>'on' order by id desc limit 5");
									$cons2->bindValue(':categoria_id', $lin['id'], PDO::PARAM_INT);
									$cons2->bindValue(':categoria_id2', $lin['id'], PDO::PARAM_INT);
									$cons2->bindValue(':categoria_id3', $lin['id'], PDO::PARAM_INT);
									try { 
										$cons2->execute();
									} catch (PDOException $e) { 
										echo "Error en la consulta: ".$e->getMessage(); 
									} 

									while($lin2 = $cons2->fetch(PDO::FETCH_ASSOC)) {
										
										$id_producto=$lin2['id'];									
										$id = base64_encode(base64_encode($id_producto));
										
										$nombre_articulo = $lin2['nombre'];
										
										?>
										 <li class="level2 nav-6-1-1"><a href="modules.php?mod=portal&file=ver_productos&id=<?php echo $id ?>&categoria=<?php echo $id_categoria ?>"><span><?php echo substr($nombre_articulo,0,20); ?></span></a></li>
										 
										<?php
									}

								  
								  ?>
								
								  <div class="menu-img-block"><a href="modules.php?mod=portal&file=listado_productos&categoria=<?php echo $id_categoria ?>"><img src="articulos/<?php echo $img_categoria ?>" title="<?php echo $nombre ?>"></a></div>
								</ul>
						    </li>
							
							<?php
						}					  
					  
					  ?>
					
                    </ul>
                  </div>
                </div>
        
                
                <!--container--> 
                 
              </div>
            </div>
          </li>
          <li class="level0 nav-6 level-top drop-menu"> <a class="level-top" href="#"> <span>Hidrosol</span> </a>
            <ul class="level1">
              <li class="level2 first"><a href="quienes-somos"><span>¿Quiénes somos?</span></a> </li>
              <li class="level2 nav-10-2"> <a href="formacion"> <span>Formación</span> </a> </li>
              <li class="level2 nav-10-3"> <a href="servicios"> <span>Servicios</span> </a> </li>
              <li class="level2 nav-10-4"> <a href="obras-referencia"> <span>Obras de referencia</span> </a> </li>
              
            </ul>
          </li>
          <li class="mega-menu"> <a class="level-top" href="clientes" target="_blank"><span>Clientes</span></a> </li>
          <li class="mega-menu"> <a class="level-top" href="contactar"><span>Contactar</span></a> </li>
		  <li class="mega-menu"> <a class="level-top" href="sat"><span>Asistencia Técnica</span></a> </li>
          <!--nav-->
          
        </ul>
        <!--nav-->
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs search-block">
          <div class="search-box">
		  
		  
		  
			<form action="modules.php?mod=portal&file=listado_productos"  method="POST">
				 <input type="text" placeholder="Buscar producto..."  maxlength="70" name="etiquetas"  id="search">
              <button type="button" class="search-btn-bg "><span class="glyphicon glyphicon-search"></span>&nbsp;</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </nav>
  
  <!-- end nav -->
