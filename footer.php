            <footer class="footer bg-white b-t b-light">
              <p>Sistema de Gestión de Contenidos de HIDROSOL CANARIAS - <?php echo date('Y'); ?> - <a target=tac7 href=http://www.tac7.com>TAC7</a></p>
            </footer>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>
  </section>
   
 
  <script src="js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="js/bootstrap.js"></script>
  <!-- App -->
  <script src="js/app.js"></script>
  <script src="js/app.plugin.js"></script>
  <script src="js/slimscroll/jquery.slimscroll.min.js"></script>
  
    <script src="js/charts/easypiechart/jquery.easy-pie-chart.js"></script>
  <script src="js/charts/sparkline/jquery.sparkline.min.js"></script>
  <script src="js/charts/flot/jquery.flot.min.js"></script>
  <script src="js/charts/flot/jquery.flot.tooltip.min.js"></script>
  <script src="js/charts/flot/jquery.flot.resize.js"></script>
  <script src="js/charts/flot/jquery.flot.grow.js"></script>
  <script src="js/charts/flot/demo.js"></script>

  <script src="js/calendar/bootstrap_calendar.js"></script>
  <script src="js/calendar/demo.js"></script>

  <script src="js/sortable/jquery.sortable.js"></script>
  
  <!-- Magnific Popup core JS file -->
  <script src="js/jquery.magnific-popup.min.js"></script>

  <!-- calendario -->
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
  





       <script src="inicio/js/jquery.js"></script>
        <script src="inicio/js/bootstrap.js"></script>
        <script src="inicio/js/slimmenu.js"></script>
        <script src="inicio/js/bootstrap-datepicker.js"></script>
        <script src="inicio/js/bootstrap-timepicker.js"></script>
        <script src="inicio/js/nicescroll.js"></script>
        <script src="inicio/js/dropit.js"></script>
        <script src="inicio/js/ionrangeslider.js"></script>
        <script src="inicio/js/icheck.js"></script>
        <script src="inicio/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="inicio/js/typeahead.js"></script>
        <script src="inicio/js/card-payment.js"></script>
        <script src="inicio/js/magnific.js"></script>
        <script src="inicio/js/owl-carousel.js"></script>
        <script src="inicio/js/fitvids.js"></script>
        <script src="inicio/js/tweet.js"></script>
        <script src="inicio/js/countdown.js"></script>
        <script src="inicio/js/gridrotator.js"></script>
        <script src="inicio/js/custom.js"></script>
        <script src="inicio/js/switcher.js"></script>
    </div>
</body>

</html>