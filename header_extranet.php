<!DOCTYPE html>
<html lang="en" class="app">
<head>
  <?php
 

  include("variables_globales_gestproject.php");
  include("funciones.php");   

  
  ?>
  <meta charset="utf-8" />
  <title>TACgestorcontenidos</title>
  <meta name="description" content="app22, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  <link rel="icon" href="documentos/favicon.png">
  <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="css/animate.css" type="text/css" />
  <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="css/font.css" type="text/css" />
  <link rel="shortcut icon" type="image/png" href="documentos/favicon.png"/>
  <link rel="stylesheet" href="css/app.css" type="text/css" /> 
	<!--[if lt IE 9]>
    <script src="js/ie/html5shiv.js"></script>
    <script src="js/ie/respond.min.js"></script>
    <script src="js/ie/excanvas.js"></script>
  <![endif]-->
  
  
  <!-- ckeditor -->
<script src="extras/ckeditor/ckeditor.js"></script>

 <script type="text/javascript" src="extras/plupload/js/plupload.full.min.js"></script>

  
  <?php
	if ($_REQUEST[file] == "index_cuadrante_new") {
  ?>
    <link rel="stylesheet" href="js/fullcalendar/fullcalendar.css" type="text/css"  />
    <link rel="stylesheet" href="js/fullcalendar/theme.css" type="text/css" />
  <?php
	} else {
  ?>
     <link rel="stylesheet" href="js\calendar/bootstrap_calendar.css" type="text/css" />
  <?php
    }
  ?>
  
  <!-- Magnific Popup core CSS file -->
  <link rel="stylesheet" href="styles/magnific-popup.css"> 
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" />

  <style>
	@media print
	{
	.noprint {display:none;}
	}
  </style>

  <?php
  include("js/funciones.php");
  include("js/calendar_files.php");
  
  // Obtenemos los dias de asuntos propios/particulares que restan de este ano y los de vacaciones
  $ano_actual = date('Y');
  /*
  // ASUNTOS PROPIOS
  $dias_asuntos_propios_permitidos = 0;
  $consulta6 = "select dias_permitidos from maestro_dias_asuntos_propios_t where ano='$ano_actual'";
  $resultado6 = mysql_query($consulta6) or die("La consulta fall&oacute;: $consulta6 " . mysql_error());
  while ($linea6 = mysql_fetch_array($resultado6, MYSQL_ASSOC)) {
	$dias_asuntos_propios_permitidos = $linea6[dias_permitidos];
  }
  
  */
  /*
  $dias_asuntos_propios_utilizados = 0;
  $consulta6 = "select fecha_ini, fecha_fin from solicitud_t where user_id='$user_id' and estado_departamento_id=1 and estado_concejal_id=1 and fecha_ini>='$ano_actual-01-01 00:00:00' and fecha_ini<='$ano_actual-12-31 23:59:59'";
  $resultado6 = mysql_query($consulta6) or die("La consulta fall&oacute;: $consulta6 " . mysql_error());
  while ($linea6 = mysql_fetch_array($resultado6, MYSQL_ASSOC)) {
	$dias_asuntos_propios_permitidos = $linea6[dias_permitidos];
  }
  */
  
  $dias_asuntos_propios = $dias_asuntos_propios_permitidos - $dias_asuntos_propios_utilizados;
  // FIN ASUNTOS PROPIOS
  
  ?>
</head>
<body>
  <section class="vbox">

    <header class="bg-black dk header navbar navbar-fixed-top-xs" style="background-image: url('https://ablehd20.files.wordpress.com/2013/03/fondos-vectoriales-01.jpg');">
      <div class="navbar-header aside-md">
        <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html">
          <i class="fa fa-bars"></i>
        </a>
       <!-- <a href="modules.php?mod=gestproject&file=index" class="navbar-brand"><img src="images/logo_tacportalempleado.png" class="m-r-sm">tacportal</a>-->
	  <a href="modules.php?mod=extranet&file=index_facturas_new" class="navbar-brand" style="color: #FFFFFF;">TACgestor</a>
        <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user">
          <i class="fa fa-cog"></i>
        </a>

      </div>
      <ul class="nav navbar-nav hidden-xs">
      <!--  <li class="dropdown">
          <a href="#" class="dropdown-toggle dker" data-toggle="dropdown">
            <i class="fa fa-building-o"></i> 
            <span class="font-bold">Resumen d&iacute;as</span>
          </a>
          <section class="dropdown-menu aside-xl on animated fadeInLeft no-borders lt">
            <div class="wrapper lter m-t-n-xs">
              <a href="#" class="thumb pull-left m-r">
                <img src="images/avatar.jpg" class="img-circle">
              </a>
              <div class="clear">
                <a href="#"><span class="text-white font-bold"><?= $nombre_empleado ?></a></span>
                <small class="block"><?= $cargo_empleado ?></small>
                <a href="modules.php?mod=gestproject&file=index_miperfil_new" class="btn btn-xs btn-success m-t-xs">Perfil</a>
              </div>
            </div>
            <div class="row m-l-none m-r-none m-b-n-xs text-center">
              <div class="col-xs-6">
                <div class="padder-v">
                  <span class="m-b-xs h4 block text-white">22</span>
                  <small class="text-muted">Vacaciones disponibles</small>
                </div>
              </div>
              <div class="col-xs-6 dk">
                <div class="padder-v">
                  <span class="m-b-xs h4 block text-white"><?= $dias_asuntos_propios ?></span>
                  <small class="text-muted">Asuntos propios disponibles</small>
                </div>
              </div>

            </div>
          </section>
        </li>
		-->
		<!--
        <li>
          <div class="m-t m-l">
            <a href="price.html" class="dropdown-toggle btn btn-xs btn-primary" title="Upgrade"><i class="fa fa-long-arrow-up"></i></a>
          </div>
        </li>
		-->
      </ul>      
	  
	  <?php
	  
	  /*
		// Vemos el numero de mensajes/notificaciones no leidas
		$consulta6 = "select id, asunto, fecha, remitente_id from mensajes_t where user_id='$user_id' && (fecha_leido='' or fecha_leido is null);";
		//echo "$consulta6";
		$mensajes_sin_leer = 0;
		$contenido_mensajes = "";
		$resultado6 = mysql_query($consulta6) or die("La consulta fall&oacute;: $consulta6 " . mysql_error());
		while ($linea6 = mysql_fetch_array($resultado6, MYSQL_ASSOC)) {
			
			*/
				$mensajes_sin_leer++;
			//	list($mensaje_fecha, $mensaje_hora) = explode(' ', $linea6[fecha]);
			//	list($mensaje_ano, $mensaje_mes, $mensaje_dia) = explode('-', $mensaje_fecha);
			//	list($mensaje_hora, $mensaje_min) = explode(':', $mensaje_hora);
			//	$nombre_remitente = obtener_campo('nombre','usuarios_t','','id='.$linea6[remitente_id]);
			//	$mensaje_id_encript = base64_encode(base64_encode($linea6[id]));
				
				/*
				$contenido_mensajes .= '
                <a href="modules.php?mod=gestproject&file=index_mensajes_lectura_new&accion=formmodificar&id='.$mensaje_id_encript.'" class="media list-group-item">
                  <span class="pull-left thumb-sm">
                    <img src="images/avatar.jpg" class="img-circle">
                  </span>
                  <span class="media-body block m-b-none">
                    '.$nombre_remitente.' ha remitido un mensaje<br>
                    <small class="text-muted">'.$mensaje_dia.'/'.$mensaje_mes.'/'.$mensaje_ano.' '.$mensaje_hora.':'.$mensaje_min.'</small>
                  </span>
                </a>				
				';*/
		//}
		
		
	  ?>
	  
      <ul class="nav navbar-nav navbar-right hidden-xs nav-user">
        <!--<li class="hidden-xs">
          <a href="modules.php?mod=gestproject&file=index_mensajes_new" class="dropdown-toggle dk" data-toggle="dropdown">
            <i class="fa fa-bell"></i>
            <span class="badge badge-sm up bg-danger m-l-n-sm count"><?= $mensajes_sin_leer ?></span>
          </a>
          <section class="dropdown-menu aside-xl">
            <section class="panel bg-white">
              <header class="panel-heading b-light bg-light">
                <strong>Tienes <span class="count"><?= $mensajes_sin_leer ?></span> notificaciones</strong>
              </header>
              <div class="list-group list-group-alt animated fadeInRight">
					<?= $contenido_mensajes ?>
              </div>
              <footer class="panel-footer text-sm">
                <a href="modules.php?mod=gestproject&file=index_mensajes_new" class="pull-right"><i class="fa fa-cog"></i></a>
                <a href="modules.php?mod=gestproject&file=index_mensajes_new" data-toggle="class:show animated fadeInRight">Ver todas las notificaciones</a>
              </footer>
            </section>
          </section>
        </li>
        <li class="dropdown hidden-xs">
          <a href="#" class="dropdown-toggle dker" data-toggle="dropdown"><i class="fa fa-fw fa-search"></i></a>
          <section class="dropdown-menu aside-xl animated fadeInUp">
            <section class="panel bg-white">
              <form role="search" method=get action=modules.php>
			  <input type=hidden name=mod value=gestproject>
			  <input type=hidden name=file value=index_noticias_new>
                <div class="form-group wrapper m-b-none">
                  <div class="input-group">
                    <input name="buscar_cliente_1" class="form-control" placeholder="Buscar en noticias">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-info btn-icon"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </div>
              </form>
            </section>
          </section>
        </li>
		-->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="thumb-sm avatar pull-left">
              <img src="images/avatar.jpg">
            </span>
            <?= $nombre_empleado ?> <b class="caret"></b>
          </a>
          <ul class="dropdown-menu animated fadeInRight">
            <span class="arrow top"></span>
            <!--<li>
              <a href="modules.php?mod=extranet&file=index_miperfil_new">Perfil</a>
            </li>-->
         <!--   <li>
              <a href="modules.php?mod=gestproject&file=index_mensajes_new">
                <span class="badge bg-danger pull-right"><?= $mensajes_sin_leer ?></span>
                Notificaciones
              </a>
            </li>-->
			<!--
            <li>
              <a href="docs.html">Help</a>
            </li>
			-->
            <li class="divider"></li>
            <li>
              <a href="modules.php?mod=extranet&file=logout">Salir</a>
            </li>
          </ul>
        </li>
      </ul>      
    </header>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <aside class="bg-black lter aside-md hidden-print" id="nav">          
          <section class="vbox">
		  	<!--
            <header class="header bg-primary lter text-center clearfix">
              <div class="btn-group">
                <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                <div class="btn-group hidden-nav-xs">
                  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
                    Switch Project
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu text-left">
                    <li><a href="#">Project</a></li>
                    <li><a href="#">Another Project</a></li>
                    <li><a href="#">More Projects</a></li>
                  </ul>
                </div>
              </div>
            </header>
			-->
            <section class="w-f scrollable">
              <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">
                
                <!-- nav -->
                <nav class="nav-primary hidden-xs">
                  <ul class="nav">
				  
				  	<!--<li >
                      <a href="#uikit"  >
                        <i class="fa fa-user icon">
                          <b class="bg-success"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Mi Perfil</span>
                      </a>
                      <ul class="nav lt">
                        <li >
                          <a href="modules.php?mod=extranet&file=index_miperfil_new" >
                            <i class="fa fa-angle-right"></i>
                            <span>Mis datos</span>
                          </a>
                        </li>
                        <li >
                          <a href="modules.php?mod=extranet&file=index_miperfil_cambiarclave_new" >                            
                          
                            <i class="fa fa-angle-right"></i>
                            <span>Cambiar clave</span>
                          </a>
                        </li>
                      </ul>
                    </li>-->
				  
					<li>
                      <a href="modules.php?mod=extranet&file=index_facturas_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span>Facturas</span>
                      </a>
                    </li>
					<li>
                      <a href="modules.php?mod=extranet&file=index_albaranes_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span>Albaranes</span>
                      </a>
                    </li>
					<li>
                      <a href="modules.php?mod=extranet&file=index_presupuestos_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span>Presupuestos</span>
                      </a>
                    </li>
					

				
		
		<!--
                    <li  class="active">
                      <a href="#layout">
                        <i class="fa fa-clock-o icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Solicitudes</span>
                      </a>
                      <ul class="nav lt">
                        <li  class="active">
                          <a href="modules.php?mod=gestproject&file=index_missolicitudes_new">                                                        
                            <i class="fa fa-angle-right"></i>
                            <span>Nueva solicitud</span>
                          </a>
                        </li>
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_missolicitudes_new&diabuscar_cliente_1_desde=01&mesbuscar_cliente_1_desde=01&anobuscar_cliente_1_desde=2014&diabuscar_cliente_1_hasta=31&mesbuscar_cliente_1_hasta=12&anobuscar_cliente_1_hasta=2014" >                                                        
                            <i class="fa fa-angle-right"></i>
                            <span>Solicitudes 2014</span>
                          </a>
                        </li>
						
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_missolicitudes_new" >                                                        
                            <i class="fa fa-angle-right"></i>
                            <span>Hist&oacute;rico solicitudes</span>
                          </a>
                        </li>
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_missolicitudes_new&buscar_cliente_4=on" >                                                        
                            <i class="fa fa-angle-right"></i>
                            <span>Solicitudes anuladas</span>
                          </a>
                        </li>
						
					-->
						
						<?php
						
						/*
							if ($grupo == 1) {
								// Responsable de departamento
								echo '
								<li >
								  <a href="modules.php?mod=gestproject&file=index_missolicitudes_gestion_new">                                                        
									<i class="fa fa-angle-right"></i>
									<span>Peticiones</span>
								  </a>
								</li>								
								';
							
							} else if ($grupo == 2) {
								// Concejal de departamento
								echo '
								<li >
								  <a href="modules.php?mod=gestproject&file=index_missolicitudes_gestion_new">                                                        
									<i class="fa fa-angle-right"></i>
									<span>Peticiones</span>
								  </a>
								</li>								
								';
							
							} else if ($grupo == 3) {
								// Administrador
								echo '
								<li >
								  <a href="modules.php?mod=gestproject&file=index_missolicitudes_gestion_new">                                                        
									<i class="fa fa-angle-right"></i>
									<span>Peticiones</span>
								  </a>
								</li>								
								';			
							}
						*/
						
						?>
                     <!--   <li >
                          <a href="modules.php?mod=gestproject&file=index_misdias_new" >                                                        
                            <i class="fa fa-angle-right"></i>
                            <span>Resumen d&iacute;as</span>
                          </a>
                        </li>
                      </ul>
                    </li>-->
                    
					
				
					
					<!--<li>
                      <a href="modules.php?mod=gestproject&file=index_minomina_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Mi n&oacute;mina</span>
                      </a>
                    </li>
					    <li>
                      <a href="modules.php?mod=gestproject&file=index_mis_fichajes_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Mis fichajes</span>
                      </a>
                    </li>
                    <li>
                      <a href="modules.php?mod=gestproject&file=index_mensajes_new">
                        <i class="fa fa-envelope icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Mis notificaciones</span>
                      </a>
                    </li>
                    <li>
                      <a href="modules.php?mod=gestproject&file=index_directorio_new">
                        <i class="fa fa-phone icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Directorio</span>
                      </a>
                    </li>
                    <li>
                      <a href="modules.php?mod=gestproject&file=index_noticias_new">
                        <i class="fa fa-phone icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Comunicaciones</span>
                      </a>
                    </li>
					-->
 			
			<!--
                    <li >
                      <a href="#uikit"  >
                        <i class="fa fa-inbox icon">
                          <b class="bg-success"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Otros</span>
                      </a>
                      <ul class="nav lt">
                        <li >
                          <a href="buttons.html" >                                                        
                            <i class="fa fa-angle-right"></i>
                            <span>Sugerencias</span>
                          </a>
                        </li>
                        <li >
                          <a href="icons.html" >                            
                            <b class="badge bg-info pull-right">369</b>                                                        
                            <i class="fa fa-angle-right"></i>
                            <span>Mis anuncios</span>
                          </a>
                        </li>
                        <li >
                          <a href="icons.html" >                            
                            <b class="badge bg-info pull-right">369</b>                                                        
                            <i class="fa fa-angle-right"></i>
                            <span>Tabl&oacute;n de anuncios</span>
                          </a>
                        </li>
                        <li >
                          <a target=aula href="http://casiopea.tac7.es/moodle_laspalmas/" >                            
                            <b class="badge bg-info pull-right">369</b>                                                        
                            <i class="fa fa-angle-right"></i>
                            <span>Aula de formaci&oacute;n</span>
                          </a>
                        </li>
                      </ul>
                    </li>
				-->

					
                  </ul>
                </nav>
                <!-- / nav -->
              </div>
            </section>
            
			
            <footer class="footer lt hidden-xs b-t b-black">

              <a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-black btn-icon">
                <i class="fa fa-angle-left text"></i>
                <i class="fa fa-angle-right text-active"></i>
              </a>

            </footer>
		
			
          </section>
        </aside>
        <!-- /.aside -->
		
	

