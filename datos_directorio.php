<?php
$es_windows = 0;
if ($es_windows == 1)
{
	$separador_carpeta = '\\';
	$nombre_carpeta_htdocs = "tacgestorcontenidos_hidrosol2017";
}
else
{
	$separador_carpeta = '/';
	$nombre_carpeta_htdocs = "tacgestorcontenidos_hidrosol2017";
}
$enlaceurl = "http://localhost/$nombre_carpeta_htdocs";
$ruta_mailing = "http://gestproject.tac7.com/".$nombre_carpeta_htdocs;

$ruta_imagenes = "http://gestproject.tac7.com/".$nombre_carpeta_htdocs."/images/";

//*********************** DIRECTORIOS DONDE SE SUBIRAN ARCHIVOS **********************
$carpeta_base = "documentos";
$carpeta_grafica = "graficas";
$carpeta_incidencias = "doc_incidencias";
$carpeta_planning = "doc_planning";

$uploaddir  = "/var/www/html/tacgestorcontenidos_hidrosol2017/documentos/";
$uploaddir_relativo  = "/tacgestorcontenidos_hidrosol2017/documentos/";
$uploaddir_excel = "/var/www/html/tacgestorcontenidos_hidrosol2017/csv/"; 

//*********************** DIRECTORIOS DONDE SE SUBIRAN LOS DOCUMENTOS DEL FCKEDITOR **********************
// Editor de texto
	//$usar_tinymce = 0;
	$usar_fckeditor = 1;
// Ruta relativa
	$fckeditor_camino_relativo = $separador_carpeta.$carpeta_base.$separador_carpeta;
// Ruta absoluta
	if ($es_windows == 1)
	{
		// Para windows
		$fckeditor_camino_absoluto = $uploaddir_base.$carpeta_base.$separador_carpeta;
	}
	else
	{
		// Para Unix
		$fckeditor_camino_absoluto = $uploaddir_base.$carpeta_base.$separador_carpeta;
	}
?>