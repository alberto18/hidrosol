<?php
/*
Funciones en el script:

BusquedaFechasVisitas ($valor_programacion, $numero_meses)
VerPlanningProv ($prov_planning_id)
CorreoOfertaCliente ($valor_oferta_cliente)
CorreoOfertaProv ($valor_oferta)
CorreoOferta ($valor_oferta)
DevolverProvCli ($valor_cliente, $tipo_prov)
DatosIncidencia ($incidencia_id)
DatosPlanningProv ($valor_dato)
*/

function BusquedaFechasVisitas ($valor_programacion, $numero_meses)
{
global $intervalo_especial;
	$array_fechas = array();
	$fecha_hoy = date("Y-m-d");
	$consultamod = "select * from programacion_visitas_t where id='".$valor_programacion."';";
	$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: " . mysql_error());
	// El resultado lo metemos en un array asociativo
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC))
	{
		$fecha_inicio = $lineasmod['fecha_visita'];
		$fecha_fin = CalcularNuevaFecha($fecha_inicio, $numero_meses." month");
		
		$hora_inicio = $lineasmod['hora'];
		$nueva_fecha = "";
		$consulta_fecha = "select date_add('".date("Y-m-d")." ".$hora_inicio.":00', interval ".$lineasmod['duracion']." minute) as resultado;";
		//echo "$consulta_fecha<br>";
		$resultado_fecha = mysql_query($consulta_fecha) or die("$consulta_fecha, La consulta fall&oacute;: " . mysql_error());
		while ($linea_fecha = mysql_fetch_array($resultado_fecha, MYSQL_ASSOC))
		{
			$nueva_fecha = $linea_fecha['resultado'];
		}
		list($tmp, $hora_fin) = explode(' ', $nueva_fecha);
		
		$intervalo = 0;
		if ($lineasmod['frecuencia_id'] > 0)
		{
			$cons = "select * from maestro_frecuencias_t where id='".$lineasmod['frecuencia_id']."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$intervalo = $lin['num_dias'];
			}
			$dia_preferido_visita = 0;
			$cons = "select maestro_dias_t.* from maestro_dias_t join clientes_t on clientes_t.dia_preferido_visita_id=maestro_dias_t.id where clientes_t.id='".$lineasmod['cliente_id']."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$dia_preferido_visita = $lin['valor_php'];
			}
			while (strtotime($fecha_inicio) < strtotime($fecha_fin))
			{
				$dia_semana = date("w",strtotime($fecha_inicio));
				// caso especial de 30 dias
				//if ($intervalo == $intervalo_especial)
				//{
					// en caso de ser la frecuencia de 30 dias se comprobara si es el dia de la semana preferido o se forzara
					// 2012-08-30
					if ($dia_semana != $dia_preferido_visita)
					{
						// fuerzo al siguiente fecha que sea el dia elegido
						$incremento = $dia_preferido_visita-$dia_semana;
						if ($incremento < 0) { $incremento += 7; }
						$fecha_inicio = CalcularNuevaFecha($fecha_inicio, $incremento." day");
						$dia_semana = date("w",strtotime($fecha_inicio));
					}
				//}
				if ($dia_semana == 0 || $dia_semana == 6)
				{
					if ($dia_semana == 0) { $incremento = 1; }
					if ($dia_semana == 6) { $incremento = 2; }
					$fecha_inicio = CalcularNuevaFecha($fecha_inicio, $incremento." day");
					$dia_semana = date("w",strtotime($fecha_inicio));
				}
				$no_disponible = 0;
				$cons = "select id from usuarios_ausencias_t where usuario_id='".$lineasmod['usuario_id']."' and (fecha_inicio<='$fecha_inicio' and fecha_fin>='$fecha_inicio');";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$no_disponible++;
				}
				$existe_visita = 0;
				$cons = "select id from agenda_t where cliente_id='".$lineasmod['cliente_id']."' and fecha='$fecha_inicio' and estado_agenda_id<>'2';";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$existe_visita++;
				}
				$existe_solape_comercial = 0;
				$cons = "select count(id) as total from agenda_t where comercial_visita_id='".$lineasmod['usuario_id']."' and fecha='$fecha_inicio' and (('$hora_inicio' >= hora_inicio and '$hora_inicio' < hora_fin) or ('$hora_fin' > hora_inicio and '$hora_fin' <= hora_fin) or ('$hora_inicio' <= hora_inicio and '$hora_fin' >= hora_fin)) and estado_agenda_id<>'2';";
				//echo "$cons<br>";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$existe_solape_comercial = $lin['total'];
				}
				// se controlan:
				// - fechas pasadas
				// - domingos (0) y sabados (6)
				// - no disponibilidad del comercial
				// - la existencia de una visita a ese cliente el mismo dia (independiente de la hora)
				// - la existencia de visitas en ese horario para el mismo comercial
				if (strtotime($fecha_inicio) >= strtotime($fecha_hoy) && $dia_semana != 0 && $dia_semana != 6 && $no_disponible == 0 && $existe_visita == 0 && $existe_solape_comercial == 0)
				{
					//echo "($fecha_inicio)=>".$dia_semana."<br>";
					$array_fechas[] = $fecha_inicio;
				}
				else
				{
					//echo "<span style='font-style:italic; color:#dddddd;'>($fecha_inicio)=>".$dia_semana."</span><br>";
				}
				$fecha_inicio = CalcularNuevaFecha($fecha_inicio, $intervalo." day");
			}
		}
		//echo "*($fecha_fin)<br>";
	} // del while
	return $array_fechas;
}

function VerPlanningProv ($prov_planning_id)
{
global $oferta_color_texto, $oferta_color_borde, $oferta_estilo_tabla, $oferta_estilo1, $oferta_estilo_texto, $oferta_estilo1_no_fondo, $oferta_estilo2, $oferta_estilo2_top;

$tabla_proveedores = "proveedores_t";
$tabla_planning = "prov_planning_t";
$tabla_logistica = "prov_planning_logistica_t";
$tabla_agenda = "agenda_t";
$tabla_cli_direcciones = "clientes_direcciones_t";
$tabla_municipios = "maestro_municipios_t";
$tabla_islas = "maestro_islas_t";
$tabla_usuarios = "usuarios_t";
$tabla_clientes = "clientes_t";
$tabla_desplaza = "prov_plan_logis_desplaza_t";
$tabla_transportes = "maestro_tipos_transportes_t";

$array_dias_semana = array(1 => "lunes", 2 => "martes", 3 => "miercoles", 4 => "jueves", 5 => "viernes", 6 => "sabado", 0 => "domingo");

$minutos_horario = 30;
$tamano_fuente = 14;
$transporte_avion = 1;
$transporte_barco = 3;

$color_azul_1 = "3366FF";
$color_azul_2 = "000080";
$color_gris = "C0C0C0";

	$planning_prov = "";
	$tabla_datos_prov = "";
	$tabla_datos_tiempo = "";
	$tabla_calendario = "";
	$tabla_datos_direc = "";
	$tabla_datos_facturacion = "";
	
	$fecha_inicio = "";
	$fecha_fin = "";
	$proveedor_id = 0;
	$cons = "select $tabla_planning.fecha_inicio, $tabla_planning.fecha_fin, $tabla_planning.proveedor_id from $tabla_planning where $tabla_planning.id='".$prov_planning_id."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$fecha_inicio = $lin['fecha_inicio'];
		$fecha_fin = $lin['fecha_fin'];
		$proveedor_id = $lin['proveedor_id'];
	}
	
	// pesta�a 1: numero clientes
	$array_comerciales_planning = array();
	$cons = "select $tabla_agenda.comercial_visita_id from $tabla_agenda where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' group by $tabla_agenda.comercial_visita_id;";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$array_comerciales_planning[] = $lin['comercial_visita_id'];
	}
	//echo print_r($array_comerciales_planning,true);
	
	if (count($array_comerciales_planning) > 0)
	{
		$array_islas_planning = array();
		$cons = "select $tabla_municipios.isla_id from $tabla_agenda join $tabla_cli_direcciones on $tabla_cli_direcciones.id=$tabla_agenda.cliente_direccion_id join $tabla_municipios on $tabla_municipios.id=$tabla_cli_direcciones.municipio_id where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' group by $tabla_municipios.isla_id;";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$array_islas_planning[] = $lin['isla_id'];
		}
		//echo print_r($array_islas_planning,true);
		
		$array_tabla1 = array();
		$array_visitas_total = array();
		$array_visitas_tiempo = array();
		
		$titulo_col_comercial = "
		<td align=\"left\" class='tipo1'>TOT</td>
		<td align=\"left\" class='tipo1'>TI</td>";
		
		// fila	columna
		$array_tabla1[0][0] = "
		<td class='tipo1_no_fondo'>&nbsp;</td>";
		$array_tabla1[1][0] = "
		<td class='tipo1'>&nbsp;</td>";
		$filas = 2; // primera fila con isla
		//$columnas = 1; // primera columna con comercial
		// lo recorremos por filas
		foreach ($array_islas_planning as $key_isla => $valor_isla)
		{
			$nombre_isla = "";
			$cons = "select * from $tabla_islas where id='".$valor_isla."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$nombre_isla = $lin['nombre_corto'];
			}
			$array_tabla1[$filas][0] = "
		<td align=\"left\" class='tipo1'>$nombre_isla</td>";
			
			$columnas = 1;
			foreach ($array_comerciales_planning as $key_comercial => $valor_comercial)
			{
				$nombre_comercial = "";
				$cons = "select * from $tabla_usuarios where id='".$valor_comercial."';";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$nombre_comercial = $lin['nombre'];
				}
				$array_tabla1[0][$columnas] = "
		<td colspan=\"2\" align=\"left\" class='tipo1'>$nombre_comercial</td>";
				$array_tabla1[1][$columnas] = $titulo_col_comercial;
				$numero_visitas = 0;
				$cons = "select count($tabla_agenda.id) as total from $tabla_agenda join $tabla_cli_direcciones on $tabla_cli_direcciones.id=$tabla_agenda.cliente_direccion_id join $tabla_municipios on $tabla_municipios.id=$tabla_cli_direcciones.municipio_id where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' and $tabla_agenda.comercial_visita_id='".$valor_comercial."' and $tabla_municipios.isla_id='".$valor_isla."';";
				//echo "$cons<br>";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$numero_visitas = $lin['total'];
				}
				$tiempo_invertido = 0;
				$cons = "select sum($tabla_agenda.duracion) as total from $tabla_agenda join $tabla_cli_direcciones on $tabla_cli_direcciones.id=$tabla_agenda.cliente_direccion_id join $tabla_municipios on $tabla_municipios.id=$tabla_cli_direcciones.municipio_id where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' and $tabla_agenda.comercial_visita_id='".$valor_comercial."' and $tabla_municipios.isla_id='".$valor_isla."';";
				//echo "$cons<br>";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$tiempo_invertido = number_format($lin['total'],0);
				}
				$array_tabla1[$filas][$columnas] = "
		<td class='tipo2'>$numero_visitas</td>
		<td class='tipo2'>$tiempo_invertido</td>";
				$array_visitas_total[$valor_comercial] += $numero_visitas;
				$array_visitas_tiempo[$valor_comercial] += $tiempo_invertido;
				$columnas++;
			}
			
			// eso solo se pone la primera vez
			if ($filas == 2)
			{
				$array_tabla1[0][$columnas] = "
		<td class='tipo1_no_fondo'>&nbsp;</td>";
				$array_tabla1[1][$columnas] = "
		<td class='tipo1'>FACTURACION</td>";
			}
			//jgs : hay que calcular la facturacion
			$array_tabla1[$filas][$columnas] = "
		<td class='tipo2'>0 &euro;</td>";
			$filas++;
		}
		// fila de totales
		$array_tabla1[$filas][0] = "
		<td class='tipo1'>Total cliente</td>";
		$columnas = 1;
		foreach ($array_comerciales_planning as $key_comercial => $valor_comercial)
		{
			$array_tabla1[$filas][$columnas] = "
		<td class='tipo2'>".$array_visitas_total[$valor_comercial]."</td>
		<td class='tipo2'>".$array_visitas_tiempo[$valor_comercial]."</td>";
			$columnas++;
		}
		$array_tabla1[$filas][$columnas] = "
		<td class='tipo2'>0 &euro;</td>";
		
		// visualizacion
		$tabla_datos_prov = "
<table width=\"100%\" cellspacing=\"0\" class='tabla'>
	<tr>
		<td class='tipo1'>FABRICA</td>
		<td class='tipo2_top'>";
		$cons = "select * from $tabla_proveedores where id='".$proveedor_id."';";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$tabla_datos_prov .= $lin['nombre'];
		}
		$tabla_datos_prov .= "</td>
	</tr>
	<tr>
		<td class='tipo1'>TIPO DE SERVICIO</td>
		<td class='tipo2'>";
		$cons = "select maestro_tipos_servicios_t.nombre from maestro_tipos_servicios_t join $tabla_proveedores on $tabla_proveedores.tipo_servicio_id=maestro_tipos_servicios_t.id where $tabla_proveedores.id='".$proveedor_id."';";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$tabla_datos_prov .= $lin['nombre'];
		}
		$tabla_datos_prov .= "</td>
	</tr>
	<tr>
		<td class='tipo1'>FINALIDAD DE LA VISITA</td>
		<td class='tipo2'>";
		$cons = "select * from $tabla_planning where $tabla_planning.id='".$prov_planning_id."';";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$tabla_datos_prov .= $lin['observaciones'];
		}
		$tabla_datos_prov .= "</td>
	</tr>
	<tr>
		<td class='tipo1'>FECHA PROPUESTA</td>
		<td class='tipo2'>Desde ".date("d-m-Y",strtotime($fecha_inicio))." hasta ".date("d-m-Y",strtotime($fecha_fin))."</td>
	</tr>
	<tr>
		<td class='tipo1'>ISLA/S</td>
		<td class='tipo2'>";
		$nombre_islas = "";
		foreach ($array_islas_planning as $key_isla => $valor_isla)
		{
			if ($nombre_islas != "") { $nombre_islas .= ", "; }
			$cons = "select * from $tabla_islas where id='".$valor_isla."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$nombre_islas .= $lin['nombre'];
			}
		}
		$tabla_datos_prov .= $nombre_islas."</td>
	</tr>
</table>";
		$tabla_datos_tiempo = "
<table width=\"100%\" cellspacing=\"0\" class='tabla'>";
		foreach ($array_tabla1 as $key_fila => $valores_fila)
		{
			$tabla_datos_tiempo .= "
	<tr align=\"right\">";
			foreach ($valores_fila as $key_columna => $valores_col)
			{
				//echo "($key_fila)($key_columna)";
				$tabla_datos_tiempo .= $valores_col;
			}
			$tabla_datos_tiempo .= "
	</tr>";
		}
		$tabla_datos_tiempo .= "
</table>
</div>";
		$tabla_datos_tiempo .= "
<div style=\"margin-top:10px; margin-bottom:10px;\">
<table cellspacing=\"0\" class='tabla'>
	<tr>
		<td class='tipo1'>TOT:</td>
		<td class='tipo2_top'>N&ordm; CLIENTES COMERCIAL CON FABRICA</td>
	</tr>
	<tr>
		<td class='tipo1'>TI:</td>
		<td class='tipo2'>TIEMPO NECESARIO</td>
	</tr>
</table>";
		
		// pesta�a 2: planning
		//$array_fechas = array();
		$array_semanas = array();
		$fecha_tmp = $fecha_inicio;
		while (strtotime($fecha_tmp) <= strtotime($fecha_fin))
		{
			$dia_semana = date("w",strtotime($fecha_tmp));
			if ($dia_semana != 0 && $dia_semana != 6) // si no es domingo o sabado
			{
				//$array_fechas[] = substr($fecha_tmp,0,10);
				$array_semanas[date("W",strtotime($fecha_tmp))][] = substr($fecha_tmp,0,10);
			}
			$fecha_tmp = CalcularNuevaFecha($fecha_tmp, "1 day");
		}
		//echo print_r($array_semanas,true);
		
		$hora_minima = "00:00";
		$cons = "select $tabla_agenda.hora_inicio from $tabla_agenda where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' order by $tabla_agenda.hora_inicio asc limit 1;";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			//$hora_minima = substr($lin['hora_inicio'],0,5);
			$hora_minima = $lin['hora_inicio'];
		}
		// comprobar si no hay una hora anterior en los desplazamientos
		$hora_minima_desplaza = "";
		$cons = "select $tabla_desplaza.* from $tabla_desplaza 
join $tabla_logistica on $tabla_logistica.id=$tabla_desplaza.prov_planning_logistica_id
where $tabla_logistica.fecha>='$fecha_inicio' and $tabla_logistica.fecha<='$fecha_fin' and $tabla_logistica.prov_planning_id='".$prov_planning_id."' order by $tabla_desplaza.hora_inicio asc limit 1;";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$hora_minima_desplaza = $lin['hora_inicio'];
		}
		if ($hora_minima_desplaza != "" && strtotime(date("Y-m-d")." ".$hora_minima_desplaza) < strtotime(date("Y-m-d")." ".$hora_minima))
		{
			$hora_minima = $hora_minima_desplaza;
		}
		$hora_maxima = "23:59";
		$cons = "select $tabla_agenda.hora_fin from $tabla_agenda where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' order by $tabla_agenda.hora_fin desc limit 1;";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			//$hora_maxima = substr($lin['hora_fin'],0,5);
			$hora_maxima = $lin['hora_fin'];
		}
		// comprobar si no hay una hora superior en los desplazamientos
		$hora_maxima_desplaza = "";
		$cons = "select $tabla_desplaza.* from $tabla_desplaza 
join $tabla_logistica on $tabla_logistica.id=$tabla_desplaza.prov_planning_logistica_id
where $tabla_logistica.fecha>='$fecha_inicio' and $tabla_logistica.fecha<='$fecha_fin' and $tabla_logistica.prov_planning_id='".$prov_planning_id."' order by $tabla_desplaza.hora_fin desc limit 1;";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$hora_maxima_desplaza = $lin['hora_fin'];
		}
		if ($hora_maxima_desplaza != "" && strtotime(date("Y-m-d")." ".$hora_maxima_desplaza) > strtotime(date("Y-m-d")." ".$hora_maxima))
		{
			$hora_maxima = $hora_maxima_desplaza;
		}
		
		$array_horas_visitas = array();
		$fecha_minima = date("Y-m-d")." ".$hora_minima;
		$fecha_maxima = date("Y-m-d")." ".$hora_maxima;
		while (strtotime($fecha_minima) <= strtotime($fecha_maxima))
		{
			$array_horas_visitas[] = substr($fecha_minima,11);
			$fecha_minima = CalcularNuevaFecha($fecha_minima, $minutos_horario." minute");
		}
		//echo print_r($array_horas_visitas,true);
		
		$tabla_calendario = "";

		foreach ($array_semanas as $array_fechas)
		{
 			$array_tabla2 = array();
			$array_tabla2[0][0] = "
		<td class='tipo1_no_fondo'>&nbsp;</td>";
			$array_tabla2[1][0] = "
		<td class='tipo1_no_fondo'>&nbsp;</td>";
			$array_tabla2[2][0] = "
		<td class='tipo1_no_fondo'>&nbsp;</td>";
			$filas = 3; // primera fila con hora
			//$columnas = 1; // primera columna con comercial
			
			// lo recorremos por columnas
			$indice_horas = 0;
			$total_horas = count($array_horas_visitas);
			while ($indice_horas < ($total_horas-1))
			{
				$array_tabla2[$filas][0] = "
		<td class='tipo1'>".substr($array_horas_visitas[$indice_horas],0,5)." - ".substr($array_horas_visitas[$indice_horas+1],0,5)."</td>";
				$indice_horas++;
				$filas++;
			}
			$array_tabla2[$filas][0] = "
		<td class='tipo1'>HOTEL</td>";
			$columnas = 1;
			foreach ($array_fechas as $key_fecha => $valor_fecha)
			{
				$dia_semana = date("w",strtotime($valor_fecha));
				// dia semana
				$array_tabla2[0][$columnas] = "
		<td class='tipo1'>".strtoupper($array_dias_semana[$dia_semana])."</td>";
				// fecha
				$array_tabla2[1][$columnas] = "
		<td class='tipo1'>".date("d-m-Y",strtotime($valor_fecha))."</td>";
				$nombre_isla = "";
				$cons = "select $tabla_islas.nombre_corto from $tabla_logistica join $tabla_islas on $tabla_islas.id=$tabla_logistica.isla_id where $tabla_logistica.prov_planning_id='".$prov_planning_id."' and $tabla_logistica.fecha='".$valor_fecha."';";
				//echo "$cons<br>";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					if ($nombre_isla != "") { $nombre_isla .= ", "; }
					$nombre_isla .= $lin['nombre_corto'];
				}
				// nombre isla
				$array_tabla2[2][$columnas] = "
		<td class='tipo1'>".$nombre_isla."</td>";
				$filas = 3; // primera fila con hora
				$indice_horas = 0;
				while ($indice_horas < ($total_horas-1))
				{
					$visita_id = 0;
					$rowspan_visita = 0;
					$contenido_celda = "";
					$cons = "select $tabla_agenda.* from $tabla_agenda where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha='".$valor_fecha."' and $tabla_agenda.hora_inicio>='".$array_horas_visitas[$indice_horas]."' and $tabla_agenda.hora_inicio<'".$array_horas_visitas[$indice_horas+1]."';";
					//echo "$cons<br>";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
					while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
					{
						$visita_id = $lin['id'];
						$duracion = $lin['duracion'];
						if ($lin['hora_inicio'] != $array_horas_visitas[$indice_horas])
						{
							// si la hora de inicio no coincide con la de la tabla hay que aumentar la duracion para que se dibuje bien
							$desfase = (strtotime("$valor_fecha $lin[hora_inicio]")-strtotime("$valor_fecha $array_horas_visitas[$indice_horas]"))/60;
							$duracion += $desfase;
						}
						while ($duracion >= $minutos_horario)
						{
							$duracion -= $minutos_horario;
							$rowspan_visita++;
						}
						$nombre_cliente = "";
						$cons_cli = "select * from $tabla_clientes where id='".$lin['cliente_visitado_id']."';";
						$res_cli = mysql_query($cons_cli) or die("La consulta fall&oacute;: $cons_cli " . mysql_error());
						while ($lin_cli = mysql_fetch_array($res_cli, MYSQL_ASSOC))
						{
							$nombre_cliente = $lin_cli['nombre_corto'];
						}
						$nombre_comercial = "";
						$cons_com = "select * from $tabla_usuarios where id='".$lin['comercial_visita_id']."';";
						$res_com = mysql_query($cons_com) or die("La consulta fall&oacute;: $cons_com " . mysql_error());
						while ($lin_com = mysql_fetch_array($res_com, MYSQL_ASSOC))
						{
							$nombre_comercial = $lin_com['nombre'];
						}
						$contenido_celda = substr($lin['hora_inicio'],0,5)." - ".substr($lin['hora_fin'],0,5)."<br>".$nombre_cliente."<br>".$nombre_comercial;
					}
					if ($rowspan_visita == 0)
					{
						// no hay ninguna visita en ese horario
//jgs
						// se comprueba si hay desplazamientos
						$rowspan_desplaza = 0;
						$futuras_visitas = 0;
						$contenido_desplaza = "";
						$cons = "select $tabla_desplaza.* from $tabla_desplaza 
join $tabla_logistica on $tabla_logistica.id=$tabla_desplaza.prov_planning_logistica_id
where $tabla_logistica.fecha='".$valor_fecha."' and $tabla_logistica.prov_planning_id='".$prov_planning_id."' and $tabla_desplaza.hora_inicio>='".$array_horas_visitas[$indice_horas]."' and $tabla_desplaza.hora_inicio<'".$array_horas_visitas[$indice_horas+1]."';";
						//echo "$cons<br>";
						$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
						while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
						{
							if ($lin['tipo_transporte_id'] == $transporte_avion || $lin['tipo_transporte_id'] == $transporte_barco)
							{
								$duracion = (strtotime("$valor_fecha $lin[hora_fin]")-strtotime("$valor_fecha $lin[hora_inicio]"))/60;
								if ($lin['hora_inicio'] != $array_horas_visitas[$indice_horas])
								{
									// si la hora de inicio no coincide con la de la tabla hay que aumentar la duracion para que se dibuje bien
									$desfase = (strtotime("$valor_fecha $lin[hora_inicio]")-strtotime("$valor_fecha $array_horas_visitas[$indice_horas]"))/60;
									$duracion += $desfase;
								}
								while ($duracion >= $minutos_horario)
								{
									$duracion -= $minutos_horario;
									$rowspan_desplaza++;
								}
							}
							else
							{
								// transporte que no sea avion o barco
								$rowspan_desplaza++;
							}
							// en caso de existir desplazamiento se comprueba si chocaria con una visita (que tiene prioridad)
							$cons2 = "select count($tabla_agenda.id) as total from $tabla_agenda where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha='".$valor_fecha."' and $tabla_agenda.hora_inicio>='".$array_horas_visitas[$indice_horas]."' and $tabla_agenda.hora_inicio<'".$array_horas_visitas[$indice_horas+$rowspan_desplaza]."';";
							//echo "$cons2<br>";
							$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
							while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
							{
								$futuras_visitas = $lin2['total'];
							}
							if ($futuras_visitas == 0)
							{
								// si no choca con otras visitas preparamos el contenido
								$datos_transporte = "";
								$hora_transporte = "";
								switch ($lin['tipo_transporte_id'])
								{
									case 1:
										// avion
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['avion_vuelo'];
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['avion_trayecto'];
										$hora_transporte = substr($lin['hora_inicio'],0,5)." - ".substr($lin['hora_fin'],0,5);
										break;
									case 2:
										// coche
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['coche_compania'];
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['coche_modelo'];
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['coche_recogida'];
										$hora_transporte = substr($lin['hora_inicio'],0,5);
										break;
									case 3:
										// barco
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['barco_compania'];
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['barco_trayecto'];
										$hora_transporte = substr($lin['hora_inicio'],0,5)." - ".substr($lin['hora_fin'],0,5);
										break;
								}
								$nombre_transporte = "";
								$cons_trans = "select * from $tabla_transportes where id='".$lin['tipo_transporte_id']."';";
								$res_trans = mysql_query($cons_trans) or die("La consulta fall&oacute;: $cons_trans " . mysql_error());
								while ($lin_trans = mysql_fetch_array($res_trans, MYSQL_ASSOC))
								{
									$nombre_transporte = $lin_trans['nombre'];
								}
								if ($contenido_desplaza != "") { $contenido_desplaza .= "<br>"; }
								$contenido_desplaza .= "$nombre_transporte ($hora_transporte): $datos_transporte";
							}
						}
						if ($rowspan_desplaza == 1 && $futuras_visitas == 0)
						{
							// hay desplazamiento que solo ocupa una fila y no choca con visitas
							$array_tabla2[$filas][$columnas] = "
		<td class='tipo2' align=\"left\">&nbsp;$contenido_desplaza</td>";
							$filas++;
							$indice_horas++;
						}
						elseif ($rowspan_desplaza > 1 && $futuras_visitas == 0)
						{
							// hay desplazamiento que ocupa mas de una fila y no choca con visitas
							$array_tabla2[$filas][$columnas] = "
		<td rowspan=\"".$rowspan_desplaza."\" class='tipo2' align=\"left\">&nbsp;$contenido_desplaza</td>";
							$filas++;
							$indice_horas++;
							for ($i=1; $i<$rowspan_desplaza; $i++)
							{
								$array_tabla2[$filas][$columnas] = "";
								$filas++;
								$indice_horas++;
							}
						}
						else
						{
							// no hay desplazamientos
							$array_tabla2[$filas][$columnas] = "
		<td class='tipo2'>&nbsp;</td>";
							$filas++;
							$indice_horas++;
						}
					}
					elseif ($rowspan_visita == 1)
					{
						// hay visita en ese horario y solo ocupa una fila
						$array_tabla2[$filas][$columnas] = "
		<td class='tipo2'>$contenido_celda</td>";
						$filas++;
						$indice_horas++;
					}
					else
					{
						// hay visita en ese horario y ocupa mas de una fila
						$array_tabla2[$filas][$columnas] = "
		<td rowspan=\"".$rowspan_visita."\" class='tipo2'>$contenido_celda</td>";
						$filas++;
						$indice_horas++;
						for ($i=1; $i<$rowspan_visita; $i++)
						{
							$array_tabla2[$filas][$columnas] = "";
							$filas++;
							$indice_horas++;
						}
					}
				}
				$datos_hotel = "";
				$cons = "select $tabla_logistica.hotel_nombre, $tabla_islas.nombre_corto from $tabla_logistica join $tabla_islas on $tabla_islas.id=$tabla_logistica.isla_id where $tabla_logistica.prov_planning_id='".$prov_planning_id."' and $tabla_logistica.fecha='".$valor_fecha."' and $tabla_logistica.hotel_nombre<>'';";
				//echo "$cons<br>";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					if ($datos_hotel != "") { $datos_hotel .= "<br>"; }
					$datos_hotel .= "$lin[nombre_corto]: $lin[hotel_nombre]";
				}
				// datos hotel
				$array_tabla2[$filas][$columnas] = "
		<td class='tipo2' align=\"left\">".$datos_hotel."</td>";
				$filas++;
				$columnas++;
			}
			//echo "<pre>".print_r($array_tabla2,true)."</pre>";
			
			// visualizacion
			$tabla_calendario .= "
<table width=\"100%\" cellspacing=\"0\" class='tabla'>";
			foreach ($array_tabla2 as $key_fila => $valores_fila)
			{
				$tabla_calendario .= "
	<tr align=\"center\">";
				foreach ($valores_fila as $key_columna => $valores_col)
				{
					//echo "($key_fila)($key_columna)";
					$tabla_calendario .= $valores_col;
				}
				$tabla_calendario .= "
	</tr>";
			}
			$tabla_calendario .= "
</table>";
			// fin array semanas
		}
		
		// datos direcciones
		$campos_listado = array ('cliente_id','direccion','cp','poblacion','provincia_id',
'municipio_id','telefono','telefono2','telefono3','fax');
		$tablas_campos_listado = array($tabla_cli_direcciones,$tabla_cli_direcciones,$tabla_cli_direcciones,$tabla_cli_direcciones,$tabla_cli_direcciones,
$tabla_cli_direcciones,$tabla_cli_direcciones,$tabla_cli_direcciones,$tabla_cli_direcciones,$tabla_cli_direcciones);
		$nombres_listado = array('Cliente','Direccion','CP','Poblacion','Provincia',
'Municipio','Telefono','Telefono2','Telefono3','Fax');
		$campos_listado_decod = array ('si;clientes_t;nombre;id','','','','si;maestro_provincias_t;nombre;id',
'si;maestro_municipios_t;nombre;id','','','','');
		
		$string_para_select = "$tabla_agenda.cliente_direccion_id,";
		foreach($campos_necesarios_listado as $indice => $campo)
		{
			$string_para_select .= $tablas_campos_necesarios[$indice].".$campo,";
		}
		$tabla_datos_direc = "
<table width=\"100%\" cellspacing=\"0\" class='tabla'>
	<tr>";
		$columnas = 0;
		foreach ($campos_listado as $campo)
		{
			//echo "<td><b><font color='#ffffff'>$nombres_listado[$columnas]</font></b></td>";
			$tabla_datos_direc .= "<td class='tipo1'>$nombres_listado[$columnas]</td>";
			$string_para_select .= $tablas_campos_listado[$columnas].".$campo,";
			$columnas++;
		}
		// Eliminamos el ultimo caracter
		$string_para_select = substr($string_para_select,0,-1);
		$tabla_datos_direc .= "</tr>";
		
		$cons = "select $string_para_select from $tabla_agenda join $tabla_cli_direcciones on $tabla_cli_direcciones.id=$tabla_agenda.cliente_direccion_id where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' group by $tabla_agenda.cliente_direccion_id order by $tabla_agenda.fecha, $tabla_agenda.hora_inicio, $tabla_agenda.hora_fin;";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$tabla_datos_direc .= "<tr>";
			foreach ($campos_listado as $cuenta_campos => $campo)
			{
				$nombre = "";
				list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod[$cuenta_campos]);
				if ($ele != 'si')
				{
					$nombre = "$lin[$campo]";
				}
				elseif ($ele2 == "date")
				{
					if ($lin[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
					else { $nombre = date("d-m-Y",strtotime($lin[$campo])); }
				}
				elseif ($ele2 == "datetime")
				{
					if ($lin[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
					else { $nombre = date("d-m-Y H:i",strtotime($lin[$campo])); }
				}
				elseif ($ele2 == "checkbox")
				{
					($lin[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
				}
				elseif ($ele2 == "time")
				{
					list($temp1, $temp2, $temp3) = explode(':',$lin[$campo]);
					$nombre = $temp1.":".$temp2;
				}
				elseif ($lin[$campo] != "")
				{
					$consultaselect = "select * from $ele2 where $ele4='$lin[$campo]';";
					//echo "$consultaselect";
					$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
					while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
					{
						$nombre = $lineaselect[$ele3];
					}
				}
				$tabla_datos_direc .= "<td class='tipo2'>$nombre</td>";
			}
			$tabla_datos_direc .= "</tr>";
			//echo "<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas'></td></tr>";
		} // fin while
		$tabla_datos_direc .= "
</table>";
		
		// pesta�a 3: nombre clientes
		// fila	columna
		$array_tabla3 = array();
		$array_num_cli_islas = array();
		$array_facturacion_total = array();
		
		$titulo_col_isla = "
		<td class='tipo1'>CLIENTE</td>
		<td class='tipo1'>FAC.</td>";
		
		$columnas = 0;
		// lo recorremos por columnas
		foreach ($array_islas_planning as $key_isla => $valor_isla)
		{
			$nombre_isla = "";
			$cons = "select * from $tabla_islas where id='".$valor_isla."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$nombre_isla = $lin['nombre'];
			}
			$array_tabla3[0][$columnas] = "
		<td colspan=\"2\" align=\"center\" class='tipo1'>$nombre_isla</td>";
			$array_tabla3[1][$columnas] = $titulo_col_isla;
			$num_clientes = 0;
			$filas = 2;
			$cons = "select $tabla_agenda.cliente_visitado_id, $tabla_clientes.nombre from $tabla_agenda join $tabla_cli_direcciones on $tabla_cli_direcciones.id=$tabla_agenda.cliente_direccion_id join $tabla_municipios on $tabla_municipios.id=$tabla_cli_direcciones.municipio_id join $tabla_clientes on $tabla_clientes.id=$tabla_agenda.cliente_visitado_id where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' and $tabla_municipios.isla_id='".$valor_isla."' order by $tabla_clientes.nombre;";
			//echo "$cons<br>";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$array_tabla3[$filas][$columnas] = "
		<td class='tipo2'>".$lin['nombre']."</td>
		<td align=\"right\" class='tipo2'>0</td>";
				$array_facturacion_total[$columnas] += 0;
				$num_clientes++;
				$filas++;
			}
			$array_num_cli_islas[$columnas] = $num_clientes;
			$columnas++;
		}
		
		// completar la tabla con filas vacias en las islas con menos clientes
		$maximo_clientes = max($array_num_cli_islas);
		$columnas = 0;
		// lo recorremos por columnas
		foreach ($array_islas_planning as $key_isla => $valor_isla)
		{
			if ($array_num_cli_islas[$columnas] < $maximo_clientes)
			{
				for ($i = $array_num_cli_islas[$columnas]; $i < $maximo_clientes; $i++)
				{
					$array_tabla3[$i+2][$columnas] = "
		<td class='tipo2'>&nbsp;</td>
		<td class='tipo2'>&nbsp;</td>";
				}
			}
			$columnas++;
		}
		$fila_totales = $maximo_clientes+2;
		$columnas = 0;
		// lo recorremos por columnas
		foreach ($array_islas_planning as $key_isla => $valor_isla)
		{
			$array_tabla3[$fila_totales][$columnas] = "
		<td class='tipo1'>TOTAL</td>
		<td class='tipo2'>".$array_facturacion_total[$columnas]." &euro;</td>";
			$columnas++;
		}
		
		// visualizacion
		$tabla_datos_facturacion = "
<table width=\"100%\" cellspacing=\"0\" class='tabla'>";
		foreach ($array_tabla3 as $key_fila => $valores_fila)
		{
			$tabla_datos_facturacion .= "
	<tr>";
			ksort($valores_fila);
			foreach ($valores_fila as $key_columna => $valores_col)
			{
				//echo "($key_fila)($key_columna)";
				$tabla_datos_facturacion .= $valores_col;
			}
			$tabla_datos_facturacion .= "
	</tr>";
		}
		$tabla_datos_facturacion .= "
</table>";
	} // fin count $array_comerciales_planning
	
	$planning_prov = $tabla_datos_prov."<div style=\"margin-top:10px; margin-bottom:10px;\">".$tabla_calendario."</div>"."<div style=\"margin-top:10px; margin-bottom:10px;\">".$tabla_datos_tiempo."</div>"."<div style=\"margin-top:10px; margin-bottom:10px;\">".$tabla_datos_facturacion."</div>";
	//."<div style=\"margin-top:10px; margin-bottom:10px;\">".$tabla_datos_direc."</div>"
	
	$planning_prov = str_replace("class='tabla'", $oferta_estilo_tabla, str_replace("class='texto'", $oferta_estilo_texto, str_replace("class='tipo2'", $oferta_estilo2, str_replace("class='tipo2_top'", $oferta_estilo2_top, str_replace("class='tipo1'", $oferta_estilo1, str_replace("class='tipo1_no_fondo'", $oferta_estilo1_no_fondo, $planning_prov))))));
	
	return $planning_prov;
}

function CorreoOfertaCliente ($valor_oferta_cliente)
{
$tabla_ofertas = "prov_ofertas_t";
$tabla_ofertas_cli = "prov_ofertas_clientes_t";

	$oferta_id = 0;
	$cons = "select $tabla_ofertas.id from $tabla_ofertas 
left join $tabla_ofertas_cli on $tabla_ofertas.id=$tabla_ofertas_cli.prov_oferta_id where $tabla_ofertas_cli.id='".$valor_oferta_cliente."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$oferta_id = $lin['id'];
	}
	$mensaje_oferta = "";
	if ($oferta_id > 0)
	{
		$mensaje_oferta = CorreoOferta($oferta_id);
	}
	return $mensaje_oferta;
}

function CorreoOfertaProv ($valor_oferta)
{
global $oferta_color_texto, $oferta_color_borde, $oferta_estilo_tabla, $oferta_estilo1, $oferta_estilo_texto, $oferta_estilo1_no_fondo, $oferta_estilo2, $oferta_estilo2_top;

$tabla_ofertas = "prov_ofertas_t";
$tabla_log = "log_ofertas_t";
$tabla_clientes = "clientes_t";
	
	$mensaje_oferta = "";
	$cons = "select $tabla_ofertas.* from $tabla_ofertas where $tabla_ofertas.id='".$valor_oferta."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$mensaje_oferta .= "
<span class='texto'>La oferta \"$lin[nombre]\" se ha enviado a los siguientes clientes:</span>";
	}
	$mensaje_oferta .= "
<table cellspacing='0' class='tabla'>
	<tr>
		<td class='tipo1'>Cliente</td>
		<td class='tipo1'>Fecha</td>
	</tr>";
	$cons = "select $tabla_log.*, $tabla_clientes.nombre from $tabla_log join $tabla_clientes on $tabla_clientes.id=$tabla_log.cliente_id where $tabla_log.cliente_id>0 and $tabla_log.prov_oferta_id='".$valor_oferta."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$mensaje_oferta .= "
	<tr>
		<td class='tipo2'>$lin[nombre]</td>
		<td class='tipo2'>".date("d-m-Y H:i", strtotime($lin['fecha']." ".$lin['hora']))."</td>
	</tr>";
	}
	$mensaje_oferta .= "
</table>";
	$mensaje_oferta .= "
<span class='texto'>Informacion enviada</span>";
	
	$mensaje_oferta = str_replace("class='tabla'", $oferta_estilo_tabla, str_replace("class='texto'", $oferta_estilo_texto, str_replace("class='tipo2'", $oferta_estilo2, str_replace("class='tipo2_top'", $oferta_estilo2_top, str_replace("class='tipo1'", $oferta_estilo1, str_replace("class='tipo1_no_fondo'", $oferta_estilo1_no_fondo, $mensaje_oferta))))));
	
	$mensaje_oferta .= CorreoOferta($valor_oferta);
	return $mensaje_oferta;
}

function CorreoOferta ($valor_oferta)
{
global $fckeditor_camino_relativo, $ruta_mailing, $oferta_color_texto, $oferta_color_borde, $oferta_estilo_tabla, $oferta_estilo1, $oferta_estilo_texto, $oferta_estilo1_no_fondo, $oferta_estilo2, $oferta_estilo2_top;

$tabla_ofertas = "prov_ofertas_t";
$tabla_proveedores = "proveedores_t";
$tabla_ofertas_cli = "prov_ofertas_clientes_t";
$tabla_ofertas_fam = "prov_ofertas_familias_t";
$tabla_familias = "maestro_familias_t";
$tabla_ofertas_fam_art = "prov_ofertas_fam_articulos_t";
$tabla_articulos = "articulos_t";
$tabla_ofertas_imagenes = "prov_ofertas_imagenes_t";
	
	$mensaje_oferta = "";
	$cons = "select $tabla_ofertas.*, $tabla_proveedores.nombre as nombre_proveedor from $tabla_ofertas 
left join $tabla_proveedores on $tabla_ofertas.proveedor_id=$tabla_proveedores.id 
where $tabla_ofertas.id='".$valor_oferta."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$mensaje_oferta .= "
<table cellspacing='0' class='tabla'>
	<tr>
		<td class='tipo1'>Nombre oferta</td>
		<td class='tipo2_top'>$lin[nombre]</td>
	</tr>
	<tr>
		<td class='tipo1'>Proveedor</td>
		<td class='tipo2'>$lin[nombre_proveedor]</td>
	</tr>
	<tr>
		<td class='tipo1'>Fecha</td>
		<td class='tipo2'>".date("d-m-Y", strtotime($lin['fecha_inicio']))." hasta ".date("d-m-Y", strtotime($lin['fecha_fin']))."</td>
	</tr>
</table>
<span class='texto'>Detalle de la oferta</span>";
		if ($lin['min_importe_regalo'] > 0)
		{
			$mensaje_oferta .= "
<table cellspacing='0' class='tabla'>
	<tr>
		<td class='tipo1'>Regalo por compra</td>
	</tr>
	<tr>
		<td class='tipo2'>Si compra ".number_format($lin['min_importe_regalo'],2,",",".")." &euro; le regalamos $lin[regalo]</td>
	</tr>
</table>";
		}
	}
	$num_familias = 0;
	$cons = "select count(id) as total from $tabla_ofertas_fam where prov_oferta_id='".$valor_oferta."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$num_familias = $lin['total'];
	}
	if ($num_familias > 0)
	{
		$cons = "select $tabla_ofertas_fam.*, $tabla_familias.nombre from $tabla_ofertas_fam join $tabla_familias on $tabla_familias.id=$tabla_ofertas_fam.familia_id where prov_oferta_id='".$valor_oferta."' order by $tabla_familias.nombre;";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$mensaje_oferta .= "
<table cellspacing='0' class='tabla'>
	<tr>
		<td class='tipo1_no_fondo'>&nbsp;</td>
		<td class='tipo1'>Dto % de la familia</td>
	</tr>
	<tr>
		<td class='tipo1'>$lin[nombre]</td>
		<td class='tipo2'>";
			if ($lin['dto_porc_oferta_fam'] > 0)
			{
				$mensaje_oferta .= number_format($lin['dto_porc_oferta_fam'],2,",",".")." %";
			}
			$mensaje_oferta .= "</td>
	</tr>
</table>";
			$num_articulos = 0;
			$cons2 = "select count($tabla_ofertas_fam_art.id) as total from $tabla_ofertas_fam_art 
join $tabla_ofertas_fam on $tabla_ofertas_fam.id=$tabla_ofertas_fam_art.prov_oferta_familia_id 
where $tabla_ofertas_fam.prov_oferta_id='".$valor_oferta."';";
			$cons2 = "select count($tabla_ofertas_fam_art.id) as total from $tabla_ofertas_fam_art 
where $tabla_ofertas_fam_art.prov_oferta_familia_id='".$lin['id']."';";
			$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
			while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
			{
				$num_articulos = $lin2['total'];
			}
			if ($num_articulos > 0)
			{
				$mensaje_oferta .= "
<table cellspacing='0' class='tabla'>
	<tr>
		<td class='tipo1_no_fondo'>Descuentos por articulos</td>
		<td class='tipo1'>Dto % del articulo</td>
		<td class='tipo1'>Neto del articulo</td>
		<td class='tipo1'>Reduccion precio unidad</td>
		<td class='tipo1'>Regalo de unidades</td>
	</tr>";
				$cons2 = "select $tabla_ofertas_fam_art.*, $tabla_articulos.nombre from $tabla_ofertas_fam_art join $tabla_articulos on $tabla_articulos.id=$tabla_ofertas_fam_art.articulo_id where prov_oferta_familia_id='".$lin['id']."' order by $tabla_articulos.nombre;";
				$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
				while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
				{
					$mensaje_oferta .= "
	<tr>
		<td class='tipo1'>$lin2[nombre]</td>
		<td class='tipo2'>";
					if ($lin2['dto_porc_oferta_art'] > 0)
					{
						$mensaje_oferta .= number_format($lin2['dto_porc_oferta_art'],2,",",".")." %";
					}
					$mensaje_oferta .= "</td>
		<td class='tipo2'>";
					if ($lin2['dto_porc_oferta_art'] > 0)
					{
						$mensaje_oferta .= number_format($lin2['dto_neto_oferta_art'],2,",",".")." &euro;";
					}
					$mensaje_oferta .= "</td>
		<td class='tipo2'>";
					if ($lin2['min_unidades_dto'] > 0 && $lin2['precio_unidad_dto'] > 0)
					{
						$mensaje_oferta .= "Comprando $lin2[min_unidades_dto] unidades, la unidad sale a ".number_format($lin2['precio_unidad_dto'],2,",",".")." &euro;";
					}
					$mensaje_oferta .= "</td>
		<td class='tipo2'>";
					if ($lin2['min_unidades_gratis'] > 0 && $lin2['num_unidades_gratis'] > 0)
					{
						$mensaje_oferta .= "Comprando $lin2[min_unidades_gratis] unidades, le regalamos $lin2[num_unidades_gratis]";
					}
					$mensaje_oferta .= "</td>
	</tr>";
				}
				$mensaje_oferta .= "
</table>";
			} // fin if $num_articulos
		}
	}
	
	$mensaje_oferta = str_replace("class='tabla'", $oferta_estilo_tabla, str_replace("class='texto'", $oferta_estilo_texto, str_replace("class='tipo2'", $oferta_estilo2, str_replace("class='tipo2_top'", $oferta_estilo2_top, str_replace("class='tipo1'", $oferta_estilo1, str_replace("class='tipo1_no_fondo'", $oferta_estilo1_no_fondo, $mensaje_oferta))))));
	
	$contenido_imagenes = "";
	$cons = "select * from $tabla_ofertas_imagenes where prov_oferta_id='".$valor_oferta."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$contenido_imagenes = str_replace('src="'.$fckeditor_camino_relativo,'src="'.$ruta_mailing.$fckeditor_camino_relativo,$lin['contenido']);
	}

	$mensaje_oferta .= $contenido_imagenes;
	return $mensaje_oferta;
}

function DevolverProvCli ($valor_cliente, $tipo_prov)
{
	// $tipo_prov = 1 => clientes_prov_t
	// $tipo_prov = 2 => clientes_prov_potenciales_t
	$array_resultados = array();
	if ($tipo_prov == 1)
	{
		// devuelve los clientes_prov_t.proveedor_id
		$cons = "select distinct proveedor_id from clientes_prov_t where clientes_prov_t.cliente_id='".$valor_cliente."';";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$array_resultados[] = $lin['proveedor_id'];
		}
	}
	elseif ($tipo_prov == 2)
	{
		// devuelve los clientes_prov_potenciales_t.proveedor_id
		$cons = "select distinct proveedor_id from clientes_prov_potenciales_t where clientes_prov_potenciales_t.cliente_id='".$valor_cliente."';";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$array_resultados[] = $lin['proveedor_id'];
		}
	}
	return $array_resultados;
}

function DatosIncidencia ($incidencia_id)
{
	$array_resultados = array();
	$dato_origen = "";
	$dato_destino = "";
	$dato_inicio = "";
	$dato_tipo = "";
	$dato_estado = "";
	$consulta_padre = "select * from incidencias_t where id='".$incidencia_id."';";
	//echo "$consulta_padre";
	$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
	while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
	{
		// fecha y hora inicio
		if ($linea_padre['fecha_inicio'] != "0000-00-00")
		{
			$dato_fecha = $linea_padre['fecha_inicio'];
			if ($linea_padre['hora_inicio'] != "00:00:00")
			{
				$dato_fecha .= " ".$linea_padre['hora_inicio'];
			}
			$dato_inicio = date("d-m-Y H:i:s",strtotime($dato_fecha));
		}
		// origen
		if ($linea_padre['origen_cliente_id'] > 0)
		{
			$consultaselect = "select * from clientes_t where id='$linea_padre[origen_cliente_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_origen = $lineaselect['nombre_corto'];
			}
			$dato_origen = "(C) ".$dato_origen;
		}
		elseif ($linea_padre['origen_proveedor_id'] > 0)
		{
			$consultaselect = "select * from proveedores_t where id='$linea_padre[origen_proveedor_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_origen = $lineaselect['nombre_corto'];
			}
			$dato_origen = "(P) ".$dato_origen;
		}
		elseif ($linea_padre['origen_interna'] == "on")
		{
			$dato_origen = "(I) Interna";
		}
		// destino
		if ($linea_padre['destino_cliente_id'] > 0)
		{
			$consultaselect = "select * from clientes_t where id='$linea_padre[destino_cliente_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_destino = $lineaselect['nombre_corto'];
			}
			$dato_destino = "(C) ".$dato_destino;
		}
		elseif ($linea_padre['destino_proveedor_id'] > 0)
		{
			$consultaselect = "select * from proveedores_t where id='$linea_padre[destino_proveedor_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_destino = $lineaselect['nombre_corto'];
			}
			$dato_destino = "(P) ".$dato_destino;
		}
		elseif ($linea_padre['destino_interna'] == "on")
		{
			$dato_destino = "(I) Interna";
		}
		if ($linea_padre['tipo_incidencia_id'] > 0)
		{
			$consultaselect = "select * from maestro_tipos_incidencias_t where id='$linea_padre[tipo_incidencia_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_tipo = $lineaselect['nombre'];
			}
		}
		if ($linea_padre['estado_inci_id'] > 0)
		{
			$consultaselect = "select * from maestro_estados_inci_t where id='$linea_padre[estado_inci_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_estado = $lineaselect['nombre'];
			}
		}
	}
	
	$array_resultados[] = $dato_origen;
	$array_resultados[] = $dato_destino;
	$array_resultados[] = $dato_inicio;
	$array_resultados[] = $dato_tipo;
	$array_resultados[] = $dato_estado;
	return $array_resultados;
}

function DatosPlanningProv ($valor_dato)
{
$tabla_1 = "prov_planning_t";
	$array_datos = array();
	$consulta_1 = "select * from $tabla_1 where $tabla_1.id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{
		$array_datos[] = $linea_1['proveedor_id'];
		$array_datos[] = $linea_1['fecha_inicio'];
		$array_datos[] = $linea_1['fecha_fin'];
	}
	return $array_datos;
}

?>