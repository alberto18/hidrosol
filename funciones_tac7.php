<?php
// FUNCIONES TAC7: APROBADAS PARA SU USO
// VERSION: 1.4
// FECHA: 15/02/2013
// IMPORTANTE: NO MODIFICAR ESTE FICHERO EN NINGUN PROYECTO

// USO: a) Poner el fichero funciones_tac7.php en el raiz del proyecto (al mismo nivel que modules.php)
//      b) En variables_globales_gestproject.php incluir con include("funciones_tac7.php");

// Funciones incluidas:
//      obtener_campo($campo,$tabla,$leftjoin,$condicion)
//      obtener_multiples_campos($array_campos,$tabla,$leftjoin,$condicion,$group,$order,$limit)
//      LetraNif($valor_dni)
//      ccc_valido($ccc)
//      InfoTamanoFichero ($peso_fichero)
//      CalcularEAN13 ($codigo_sin_ultimo)
//	dias_entre_periodo($diah,$mesh,$anyoh,$diap,$mesp,$anyop)
//	separador_fechas($fecha_separar,$tipo_sep=1,$separador_para_fecha="-")
//	de_entidad_html($cadena)
//	deshacer_entidad_html($cadena)
//	escape_caracteres($cadena,$por_formulario=true)

function obtener_campo($campo,$tabla,$leftjoin,$condicion) {

		// campo: el nombre del campo a obtener
		// tabla: el nombre de la tabla donde se realiza la consulta
		// lefjoin (opcional): la cadena de left join si se quiere obtener un campo
		//                     desde varias tablas
		// condicion: La parte derecha del where que queremos para nuestra consulta
		
		$resultado = "";
		$cons = "select $tabla.$campo as $campo from $tabla $leftjoin where $condicion";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("<br>La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {
			$resultado = $lin[$campo];
		}
		return $resultado;
}

function obtener_multiples_campos($array_campos,$tabla,$leftjoin,$condicion,$group,$order,$limit) {

		// array_campos: un array con los nombres de los campos a obtener
		// tabla: el nombre de la tabla donde se realiza la consulta
		// lefjoin (opcional): la cadena de left join si se quiere obtener un campo
		//                     desde varias tablas
		// condicion: La parte derecha del where que queremos para nuestra consulta
		
		$resultado = array();
                $campos_a_obtener = "";
                foreach ($array_campos as $campo) {
                    $campos_a_obtener .= " $tabla.$campo as $campo,";
                }
                $campos_a_obtener = substr($campos_a_obtener,0,-1);
		$cons = "select $campos_a_obtener from $tabla $leftjoin ";
                
                if ($condicion != "") { $cons .= "where $condicion "; }
                if ($group != "") { $cons .= " $group "; }
                if ($order != "") { $cons .= " $order "; }
                if ($limit != "") { $cons .= " $limit "; }
        
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("<br>La consulta fall&oacute;: $cons " . mysql_error());
                $c=0;
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {
                    foreach ($array_campos as $campo) {
                        $resultado[$c][$campo] = $lin[$campo];
                    }
                    $c++;
		}
                //print_r($resultado);
		return $resultado;
}


function obtener_campo_pdo($campo,$tabla,$leftjoin,$condicion,$parametros_array) {

		// Ejemplo de uso:
		// $nombrecategoria = obtener_campo_pdo('nombre','firmas_categorias_t','','id=?',array($categoria_id));
		// campo: el nombre del campo a obtener
		// tabla: el nombre de la tabla donde se realiza la consulta
		// lefjoin (opcional): la cadena de left join si se quiere obtener un campo
		//                     desde varias tablas
		// condicion: La parte derecha del where que queremos para nuestra consulta. Poner ? en todos los parametros.
		// parametros_array: array de todos los parametros que hayan en la condicion. Poner los parametros por orden segun aparezcan en la condicion.
		global $conn;
		$resultado = "";
		/*
		$cons = "select $tabla.$campo as $campo from $tabla $leftjoin where $condicion";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("<br>La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {
		*/
		$cons = $conn->prepare("select $tabla.$campo as $campo from $tabla $leftjoin where $condicion");
		$count_parametros = 1;
		foreach ($parametros_array as $parametro) {
			$cons->bindValue($count_parametros, $parametro, PDO::PARAM_STR);
			$count_parametros++;
		}
		try { 
			$cons->execute();
		} catch (PDOException $e) { 
			echo "Error en la consulta: ".$e->getMessage(); 
		} 
		while($lin = $cons->fetch(PDO::FETCH_ASSOC)) {
			$resultado = $lin[$campo];
		}
		return $resultado;
}

function obtener_multiples_campos_pdo($array_campos,$tabla,$leftjoin,$condicion,$group,$order,$limit) {

		// array_campos: un array con los nombres de los campos a obtener
		// tabla: el nombre de la tabla donde se realiza la consulta
		// lefjoin (opcional): la cadena de left join si se quiere obtener un campo
		//                     desde varias tablas
		// condicion: La parte derecha del where que queremos para nuestra consulta
		
		$resultado = array();
                $campos_a_obtener = "";
                foreach ($array_campos as $campo) {
                    $campos_a_obtener .= " $tabla.$campo as $campo,";
                }
                $campos_a_obtener = substr($campos_a_obtener,0,-1);
		$cons = "select $campos_a_obtener from $tabla $leftjoin ";
                
                if ($condicion != "") { $cons .= "where $condicion "; }
                if ($group != "") { $cons .= " $group "; }
                if ($order != "") { $cons .= " $order "; }
                if ($limit != "") { $cons .= " $limit "; }
        
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("<br>La consulta fall&oacute;: $cons " . mysql_error());
                $c=0;
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {
                    foreach ($array_campos as $campo) {
                        $resultado[$c][$campo] = $lin[$campo];
                    }
                    $c++;
		}
                //print_r($resultado);
		return $resultado;
}



function LetraNif($valor_dni)
{
        // funcion que calcula la letra correcta del dni, incluidos los extranjeros
	return substr("TRWAGMYFPDXBNJZSQVHLCKE",strtr($valor_dni,"XYZ","012")%23,1);
}

function ccc_valido($ccc)
{
	// Funcion para comprobar si un numero de cuenta corriente es valido
	/*
	if (is_string($ccc)) {
		 return false;
	}
	*/
	if (ereg('[^0-9]', $ccc)) {
		//echo "<script>alert('dentro2');</script>";
		// Si existiese una letra entrar�amos aqui
		return false;	
	} else {
		//echo "<script>alert('dentro');</script>";
		//return false;	
	}
	//$ccc ser�a el 20770338793100254321
	$valido = true;
	 
	///////////////////////////////////////////////////
	//    D�gito de control de la entidad y sucursal:
	//Se multiplica cada d�gito por su factor de peso
	///////////////////////////////////////////////////
	$suma = 0;
	$suma += $ccc[0] * 4;
	$suma += $ccc[1] * 8;
	$suma += $ccc[2] * 5;
	$suma += $ccc[3] * 10;
	$suma += $ccc[4] * 9;
	$suma += $ccc[5] * 7;
	$suma += $ccc[6] * 3;
	$suma += $ccc[7] * 6;
	 
	$division = floor($suma/11);
	$resto    = $suma - ($division  * 11);
	$primer_digito_control = 11 - $resto;
	if($primer_digito_control == 11)
	$primer_digito_control = 0;
	 
	if($primer_digito_control == 10)
	$primer_digito_control = 1;
	 
	if($primer_digito_control != $ccc[8])
	$valido = false;
	 
	///////////////////////////////////////////////////
	//            D�gito de control de la cuenta:
	///////////////////////////////////////////////////
	$suma = 0;
	$suma += $ccc[10] * 1;
	$suma += $ccc[11] * 2;
	$suma += $ccc[12] * 4;
	$suma += $ccc[13] * 8;
	$suma += $ccc[14] * 5;
	$suma += $ccc[15] * 10;
	$suma += $ccc[16] * 9;
	$suma += $ccc[17] * 7;
	$suma += $ccc[18] * 3;
	$suma += $ccc[19] * 6;
	 
	$division = floor($suma/11);
	$resto = $suma-($division  * 11);
	$segundo_digito_control = 11- $resto;
	 
	if($segundo_digito_control == 11)
	$segundo_digito_control = 0;
	if($segundo_digito_control == 10)
	$segundo_digito_control = 1;
	 
	if($segundo_digito_control != $ccc[9])
	$valido = false;
	 
        // hack
        //if ($ccc == "MARIO") { return true; }
	return $valido;
 
}

function InfoTamanoFichero ($peso_fichero)
{
        // se le pasa el peso del fichero (obtenido con un simple $_FILES[$campo]['size']
        // por ejemplo) y te devuelve el valor para mostrarselo al usuario
        // Ej.	InfoTamanoFichero (219586) => 214.44 Kb
	$array_unidades = array(0 => "b", 1 => "Kb", 2 => "Mb", 3 => "Gb");
	$num_divisiones = 0;
	$factor_division = 1024;
	while ($peso_fichero > $factor_division)
	{
		$peso_fichero = round($peso_fichero/$factor_division,2);
		$num_divisiones++;
	}
	
	$peso_en_texto = $peso_fichero." ".$array_unidades[$num_divisiones];
	return $peso_en_texto;
}


function CalcularEAN13 ($codigo_sin_ultimo)
{
        // funcion que calcula el codigo verificador del EAN13, ya que el ultimo digito de un
        // codigo de barras se calcula segun los primeros 12
	$array_numeros = array();
	$array_numeros = str_split($codigo_sin_ultimo);
	
	$total_pares = $array_numeros[0] + $array_numeros[2] + $array_numeros[4] + $array_numeros[6] + $array_numeros[8] + $array_numeros[10];
	$total_impares = $array_numeros[1] + $array_numeros[3] + $array_numeros[5] + $array_numeros[7] + $array_numeros[9] + $array_numeros[11];
	
	$total_impares *= 3;
	
	$total = $total_impares + $total_pares;
	
	// el valor final es la diferencia entre la decena inmediatamente superior menos el total
	// si le restamos a diez la ultima cifra del total es lo mismo
	$valor_final = 10-substr($total, strlen($total)-1,1);
	return $valor_final;
}

function dias_entre_periodo($diah,$mesh,$anyoh,$diap,$mesp,$anyop){
	
        // Funcion que calcula la diferencia en dias entre dos fechas proporcionadas.
        // Utiliza solo php.
	
	$marca2 = mktime(13,30,0,$mesp,$diap,$anyop);//Cogemos y restamos marca de tiempo de las 12 horas
	$marca1 = mktime(12,0,0,$mesh,$diah,$anyoh);//A la que le restamos (ponemos una hora y media de diferencia por los cambios de hora)
	$segundos_diferencia = $marca2 - $marca1;
	if($segundos_diferencia<0)
	  return 1;//Devuelvo uno porque no me va a servir en ningun caso y no quiero marcar dias antes de la activacion
	$dias_diferencia = $segundos_diferencia / (60 * 60 * 24 );
	$dias_diferencia = floor($dias_diferencia); 
	return $dias_diferencia;  
}

function separador_fechas($fecha_separar,$tipo_sep=1,$separador_para_fecha="-"){
	
	// Funcion para formatear una fecha (tipo mysql) en otros formatos.
	
	list($parte_fecha,$parte_hora)=explode(" ",$fecha_separar);
	list($ano_separ,$mes_separ,$dia_separ)=explode("-",$parte_fecha);
	list($hora_separ,$minu_separ,$seg_separ)=explode(":",$parte_hora);
	$fecha_indicada="$dia_separ$separador_para_fecha$mes_separ$separador_para_fecha$ano_separ";
	$fecha_indicadaanorecortado="$dia_separ$separador_para_fecha$mes_separ$separador_para_fecha".substr($ano_separ,2);
	if($tipo_sep==1)    return $fecha_indicada;
	elseif($tipo_sep==2) return array($dia_separ,$mes_separ,$ano_separ);
	elseif($tipo_sep==3) return array($dia_separ,$mes_separ,$ano_separ,$hora_separ,$minu_separ,$seg_separ);
	elseif($tipo_sep==4) return array($ano_separ,$mes_separ,$dia_separ);
	elseif($tipo_sep==5) return array($ano_separ,$mes_separ,$dia_separ,$hora_separ,$minu_separ,$seg_separ);
	else return $fecha_indicadaanorecortado;
}

function de_entidad_html($cadena){
	if (get_magic_quotes_gpc()){
	  //Significa que esta activo y por tanto me mete una barra horizontal cada vez
	  $cadena=str_replace('\\','#CARAC_SEP_ESP#',$cadena);
	}
	
	$cadena=htmlspecialchars($cadena,ENT_COMPAT);    
	return $cadena;
}

function deshacer_entidad_html($cadena){
	if (get_magic_quotes_gpc()){
	  //Significa que esta activo y por tanto me mete una barra horizontal cada vez
	  //COMO ME PUEDE HABER METIDO MUCHOS barras horizontales me las cargo todas y permito que sea addslashes la que se encargue de devolverme la cadena correcta::
	  $cadena=str_replace('#CARAC_SEP_ESP#','',$cadena);
	  //$cadena=htmlspecialchars_decode($cadena,ENT_COMPAT);
	  //$cadena=addslashes($cadena); -> ESTO NO ES NECESARIO PORQUE LA ULTIMA VEZ QUE SE ENVIA EL FORMULARIO el magic me vuelve a meter la barra invertida por lo que no hace falta hacer esto, sino solo el decode que lo hago al final
	}else{
	  $cadena=htmlspecialchars_decode($cadena,ENT_COMPAT);
	}
	return $cadena;
}

function escape_caracteres($cadena,$por_formulario=true){
	
	// Funcion para anadir a los insert o update y evitar fallos de ejecucion de SQLs cuando se introducen
	// comillas simples o dobles.
	if (get_magic_quotes_gpc()!=1 || !$por_formulario)
	    $cadena=addslashes($cadena);    
	return   $cadena;

}


/*
function auditoria($consulta, $url)
{
  $user_id=$GLOBALS["user_id"];
  
  $consulta = str_replace('"','', $consulta);
  $consulta = str_replace("'","", $consulta);
  $consulta_auditoria = "insert into auditoria_new_t set fecha=now(), user_id='$user_id',  consulta='$consulta', url='$url'";
  $res_auditoria = mysql_query($consulta_auditoria) or die("La consulta fall&oacute;: " . mysql_error());
}
*/

/*
function insertar($tabla,$array_campos,$array_valores) {

                // tabla: el nombre de la tabla donde se realiza la consulta
		// array_campos: un array con los nombres de los campos donde se insertara
		// array_valores: los valores que vamos a insertar
                $c=0;
                $campos_y_valores = "";
                foreach ($array_campos as $array_campos_unico) {
                    $campos_y_valores = "$array_campos_unico='".$array_valores[$c]."',";
                    $c++;
                }
                $campos_y_valores = substr($campos_y_valores,0,-1);
                
                $cons  = "insert into $tabla set $campos_y_valores ";
                $res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
                $ultimo_id =  mysql_insert_id();
		
		return $ultimo_id;
}
*/
?>