<?php 

$tiene_imagenes = '_none';
$contenido_id = base64_decode(base64_decode($_REQUEST[id]));
$imagenes = array();
$cons43 = $conn->prepare("select * from nuke_stories_galerias_t where padre_id = :contenido_id");
$cons43->bindValue('contenido_id', $contenido_id, PDO::PARAM_INT);
$cons43->execute(); 
while($lin43 = $cons43->fetch(PDO::FETCH_ASSOC)) {
    $imagenes[] = array('path' => 'galeria/' . $lin43['padre_id'] . '/' . $lin43['nombre_fichero'], 'titulo' => $lin43['titulo'], 'descripcion' => $lin43['descripcion']);
    $tiene_imagenes = '';
}

?>
 
		<?php
            if($tiene_imagenes == ''){echo "<h2 class='widget-title'>Galer&iacute;a</h2>";}
		?>	
			
            <div id="Img_carousel<?php echo $tiene_imagenes ?>" class="slider-pro">
                <div class="sp-slides">
                
                <?php
                    
                    foreach($imagenes as $imagen){
                    
                        $ruta = $imagen['path'];
                        echo "<div class='sp-slide'>
                            <img alt='$imagen[titulo]' class='sp-image' src='images/blank.gif' 
                            data-src='$ruta' 
                            data-small='$ruta' 
                            data-medium='$ruta' 
                            data-large='$ruta' 
                            data-retina='$ruta'>";
                        
                        if(!empty($imagen['titulo'])){
                        
                            echo "<h3 class='sp-layer sp-black sp-padding' data-horizontal='40' data-vertical='40' data-show-transition='left'>$imagen[titulo]</h3>";
                        }
                        
                        
                        if(!empty($imagen['descripcion'])){
                        
                            echo "<p class='sp-layer sp-black sp-padding' data-position='bottomLeft' data-vertical='0' data-width='100%' data-show-transition='up'>
                                $imagen[descripcion]
                            </p>";
                        }
                        
                        echo "</div>";
                    }
                ?>
                </div>
                <div class="sp-thumbnails">
                <?php
                    
                    foreach($imagenes as $imagen){
                        echo "<img alt='$imagen[titulo]' class='sp-thumbnail' src='$imagen[path]'>";
                    }
                ?>
                </div>
            </div>
            