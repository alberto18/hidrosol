<?php
//error_reporting(0);
//ini_set('display_errors', 0);

//error_reporting(E_ALL);
//ini_set('display_errors',true);

// Variables globales para todos los scripts
session_cache_limiter('private,must-revalidate');

session_start();

$lang = $_SESSION['lang'];

// variables dentro de la session
// login
// grupo
// tiempo
// navegador_usuario

// Variables globales para todos los scripts

// navegador
// jessica
// mozilla	=> Mozilla/5.0 (Windows NT 6.1; rv:16.0) Gecko/20100101 Firefox/16.0
// explorer	=> Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)
// chrome	=> Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11

// enrique
// mozilla	=> Mozilla/5.0 (Windows NT 6.0; WOW64; rv:16.0) Gecko/20100101 Firefox/16.0
// explorer	=> Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; WOW64; Trident/5.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; Media Center PC 5.1; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C)
// chrome	=> Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11

// datalogic
// Mozilla/4.0 (compatible; MSIE 6.0; Winsows CE; Datalogic Memor; 240x320)

if ($_SESSION['navegador_usuario'] == "")
{
	if (is_bool(strrpos($_SERVER['HTTP_USER_AGENT'],"Datalogic")) == true)
	{
		// el user_agent no tiene la palabra datalogic => es un pc
		$_SESSION['navegador_usuario'] = 1;
	}
	else
	{
		// el user_agent tiene la palabra datalogic => es una pda
		$_SESSION['navegador_usuario'] = 2;
	}
}

$minutos_sesion = 60;
$tiempo_sesion = $minutos_sesion*60;
if ($_SESSION['tiempo'] == "") { $_SESSION['tiempo'] = time(); }
else
{
	if(($_SESSION['tiempo']+$tiempo_sesion) < time())
	{
		setcookie(session_name(), "", time() - 3600, '/', false, 0);
		session_destroy();
		
		session_start();
	}
	$_SESSION['tiempo'] = time();
}

include("datos_directorio.php");
include("abrir_db_erp.php");
include("abrir_db_pdo_erp.php");

if ($_REQUEST[mod]!="extranet"){
include("abrir_db_pdo.php");
include("abrir_db.php");
}


include("variables_db.php");
include("funciones_tac7.php");


if ($_REQUEST['mod'] != "") { $mod = $_REQUEST['mod']; }
if ($_REQUEST['file'] != "") { $file = $_REQUEST['file']; }

$login = $_SESSION["login"];
$uname = $_SESSION["login"];
$grupo = $_SESSION["grupo"];


if ($_REQUEST[nombredesdepda] != "") {
    // Se trata de validarme desde una aplicacion externa en el programa nuevo a partir del nombre de usuario que deben coincidir
	$_SESSION["login"] = $_REQUEST[nombredesdepda];
	$login = $_REQUEST[nombredesdepda];
	$uname = $_REQUEST[nombredesdepda];
}
 
if ($login != "") {

	/*
	$consulta = "select id, grupo_id from usuarios_t where login='$login';";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("$consulta, La consulta fall&oacute;: " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	*/
	$consulta = $conn->prepare("select id, grupo_id from usuarios_t where login=:login");
	$consulta->bindValue(':login', $login, PDO::PARAM_STR);
	$consulta->execute();
	while($linea = $consulta->fetch(PDO::FETCH_ASSOC)) {
		$user_id = $linea['id'];
		//$user_permiso_precios = $linea['permiso_precios'];
		$grupo = $linea['grupo_id'];
	}
	
	// Otros datos
	//$salida_array_perfil = obtener_multiples_campos(array('nombre'),'usuarios_t','','id='.$user_id,'','','');
	//$nombre_empleado = $salida_array_perfil[0]['nombre'];
	//$cargo_empleado_id = $salida_array_perfil[0]['cargo_id'];

	//$salida_array_perfil = obtener_multiples_campos(array('nombre'),'maestro_cargos_t','','id='.$cargo_empleado_id,'','','');
	//$cargo_empleado = $salida_array_perfil[0]['nombre'];

}

// echo "grupo: $grupo | user_id: $user_id | login: $login";
/*
if ($grupo != "")
{
	$consulta = "select nombre from grupos_t where id='$grupo';";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		$nombre_grupo = $linea['nombre'];
	}
}

*/



// Utilizacion de la hoja de estilos
if (strpos($mod, ".") === true) { exit; }
if (strpos($file, ".") === true) { exit; }
if (strpos($mod, "/") === true) { exit; }
if (strpos($file, "/") === true) { exit; }

if ($mod != "")
{
	if (($_SESSION['navegador_usuario'] == 1 && $mod == "gestproject" && $file != "ver_prov_planning" && $file != "ver_correo_oferta" && $file != "ver_correo_pedido" && $file != "ver_correo_presupuesto" && $file != "ver_correo_albaran") || ( $mod == "informes"))
	{
		//echo '<link rel="StyleSheet" href="styles/styleNN.css" type="text/css">';
echo '<link href="styles/estilo_nuevos.css" rel="stylesheet" type="text/css" />';
//echo '<link href="styles/style_forms.css" rel="stylesheet" type="text/css" />';
	}
}


 
?>