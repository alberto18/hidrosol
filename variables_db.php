<?php
// variables necesarias en algunos puntos del programa

// tipos de registros log de estadisticas y accesos
$tipo_log_estadisticas = 1;
$tipo_log_acceso = 2;
$tipo_log_salida = 3;

// estado de la agenda
$estado_bloqueada = 3; // se usa en: enviar_agenda.php, gestproject/index_usuarios_ausencias_new, gestproject/index_agenda_new

// frecuencia de visitas de clientes
$intervalo_especial = 30;

//--------------DEPOSITO-----------------
// estados de presupuestos deposito
$presu_estado_creado = 1; // se llama pendiente
$presu_estado_aceptado = 2;
$presu_estado_cerrado = 3; // se llama anulado

// estados de pedidos deposito
$pedido_estado_creado = 1;
$pedido_estado_pendiente_servir = 2;
$pedido_estado_entrega_parcial = 3;
$pedido_estado_entrega_total = 4;
$pedido_estado_cerrado = 5;
$pedido_estado_anulado = 6;

// estados de albaranes depostivo
$albaran_estado_pendiente_entrega = 1;
$albaran_estado_entregado = 2;


//--------------DIRECTO-----------------
// estados de pedidos directo
$pedido_directo_estado_anulado = 1;
$pedido_directo_estado_solicitado = 2;
$pedido_directo_estado_camino = 3;
$pedido_directo_estado_parcial = 4;
$pedido_directo_estado_total = 5;

// tipo de embalaje especial
$tipo_embalaje_unitario = 5;

// isla de gran canaria (para calculo de portes)
$identificador_gran_canaria = 3;

// colores a utilizar en los scripts
$marron_tienda = "847248";
$marron_oscuro_tienda = "AD9151";
$marron_claro_tienda = "E8CE91";
$azul_personal = "003B6F";
$azul_oscuro_personal = "0156A2";
$azul_claro_personal = "84C5Ff";
$verde_oscuro = "519331";
$verde_claro = "CCFDB5";
$fondo_gris1 = "B3BCA7";
$fondo_gris2 = "EFF5E7";
$fondo_amarillo1 = "F4FAD0";
$fondo_verde1 = "9AF867";
$fondo_verde2 = "C4FAA7";
$color_texto = "9AA237";
$color_entorno = $verde_oscuro; $color_entorno_claro = $verde_claro;
	$fondo_verde1 = $color_entorno;
	$fondo_verde2 = $color_entorno_claro;
//$color_fondo_amarillo = "F8F8D2";
//$color_fondo_amarillo = "EEEE91";
$color_fondo_amarillo = "F2EDCC";


// ESTILOS INFORMES
$oferta_color_texto = "6D929B";
$oferta_color_borde = "C1DAD7";
$oferta_estilo_tabla = " style=\"margin-top:5px; margin-bottom: 10px;\" ";
$oferta_estilo1 = " style=\"font: bold 11px Verdana, Arial, Helvetica, sans-serif; color: #".$oferta_color_texto."; letter-spacing: 2px; text-transform: uppercase; text-align: left;";
$oferta_estilo_texto = $oferta_estilo1."\" ";
$oferta_estilo1 .= " border-right: 1px solid #".$oferta_color_borde."; border-bottom: 1px solid #".$oferta_color_borde."; border-top: 1px solid #".$oferta_color_borde."; padding: 6px 6px 6px 12px;";
$oferta_estilo1_no_fondo = $oferta_estilo1."\" ";
$oferta_estilo1 .= " background-color: #CAE8EA;\" ";
$oferta_estilo2 = " style=\"border-right: 1px solid #".$oferta_color_borde."; border-bottom: 1px solid #".$oferta_color_borde."; background: #fff; padding: 6px 6px 6px 12px; color: #".$oferta_color_texto.";";
$oferta_estilo2_top = $oferta_estilo2." border-top: 1px solid #".$oferta_color_borde.";\" ";
$oferta_estilo2 .= "\" ";


// ESTILOS PRESUPUESTOS, PEDIDOS, ALBARANES
$presupuesto_estilo_est11 = " style=\"font-size:11px;";
$presupuesto_estilo_est11b = $presupuesto_estilo_est11." font-weight:bold;\" ";
$presupuesto_estilo_est11 .= "\" ";
$presupuesto_estilo_est12 = " style=\"font-size:12px;";
$presupuesto_estilo_est12b = $presupuesto_estilo_est12." font-weight:bold;\" ";
$presupuesto_estilo_est12 .= "\" ";
$presupuesto_estilo_est13 = " style=\"font-size:13px;";
$presupuesto_estilo_est13b = $presupuesto_estilo_est13." font-weight:bold;\" ";
$presupuesto_estilo_est13 .= "\" ";
$presupuesto_estilo_est14 = " style=\"font-size:14px;";
$presupuesto_estilo_est14b = $presupuesto_estilo_est14." font-weight:bold;\" ";
$presupuesto_estilo_est14 .= "\" ";
$presupuesto_estilo_est15 = " style=\"font-size:15px;";
$presupuesto_estilo_est15b = $presupuesto_estilo_est15." font-weight:bold;\" ";
$presupuesto_estilo_est15 .= "\" ";
$presupuesto_estilo_est16 = " style=\"font-size:14px;";
$presupuesto_estilo_est16b = $presupuesto_estilo_est16." font-weight:bold;\" ";
$presupuesto_estilo_est16 .= "\" ";
$presupuesto_estilo_separador = " style=\"page-break-after:always;\" ";
$presupuesto_estilo_logo = " style=\"font-size:18px; font-weight:bold;\" ";

// ESTILOS DE LA HERRAMIENTA
$estilo_inactivo = "color:#888888; font-style:italic;";

// ESTILOS DE LA AGENDA
$agenda_color_cliente_defecto = "6ca4ec";
$agenda_color_cliente_cancelada = "cccccc";
$agenda_color_cliente_bloqueada = "ff0000";
$agenda_color_cliente_realizada = "01A401";

$agenda_color_proveedor_defecto = "806600";
$agenda_color_proveedor_cancelada = "636363";
$agenda_color_proveedor_bloqueada = "B52900";
$agenda_color_proveedor_realizada = "417700";

//$usuario_correo = "gir@ayalaehijo.net";
//$clave_correo = "a1b2c3";

$usuario_correo = "joseayalaehijo@gmail.com";
$clave_correo = "tac7tac7";

// OTROS DATOS
// cadena de comprobacion de emails
$patron_comprobacion_email = "^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$";

// array con las extensiones de archivos no permitidas
$array_ext_excluidas = array("exe","bat");

// array con los nombres de los meses
$array_nombres_meses = array("01" => "Enero", "02" => "Febrero", "03" => "Marzo", "04" => "Abril", "05" => "Mayo", "06" => "Junio", "07" => "Julio", "08" => "Agosto", "09" => "Septiembre", "10" => "Octubre", "11" => "Noviembre", "12" => "Diciembre");

// colores para el jpgraph
$array_posibles_colores_jpgraph = array("aqua","lime","teal","whitesmoke","gainsboro","oldlace","linen","antiquewhite","papayawhip","blanchedalmond",
"bisque","peachpuff","navajowhite","moccasin","cornsilk","ivory","lemonchiffon","seashell","mintcream","azure","aliceblue","lavender",
"lavenderblush","mistyrose","white","black","darkslategray","dimgray","slategray","lightslategray","gray","lightgray","midnightblue","navy",
"cornflowerblue","darkslateblue","slateblue","mediumslateblue","lightslateblue","mediumblue","royalblue","blue","dodgerblue","deepskyblue",
"skyblue","lightskyblue","steelblue","lightred","lightsteelblue","lightblue","powderblue","paleturquoise","darkturquoise","mediumturquoise",
"turquoise","cyan","lightcyan","cadetblue","mediumaquamarine","aquamarine","darkgreen","darkolivegreen","darkseagreen","seagreen",
"mediumseagreen","lightseagreen","palegreen","springgreen","lawngreen","green","chartreuse","mediumspringgreen","greenyellow","limegreen",
"yellowgreen","forestgreen","olivedrab","darkkhaki","khaki","palegoldenrod","lightgoldenrodyellow","lightyellow","yellow","gold",
"lightgoldenrod","goldenrod","darkgoldenrod","rosybrown","indianred","saddlebrown","sienna","peru","burlywood","beige","wheat","sandybrown",
"tan","chocolate","firebrick","brown","darksalmon","salmon","lightsalmon","orange","darkorange","coral","lightcoral","tomato","orangered",
"red","hotpink","deeppink","pink","lightpink","palevioletred","maroon","mediumvioletred","violetred","magenta","violet","plum","orchid",
"mediumorchid","darkorchid","darkviolet","blueviolet","purple","mediumpurple","thistle","snow1","snow2","snow3","snow4","seashell1","seashell2",
"seashell3","seashell4","AntiqueWhite1","AntiqueWhite2","AntiqueWhite3","AntiqueWhite4","bisque1","bisque2","bisque3","bisque4","peachPuff1",
"peachpuff2","peachpuff3","peachpuff4","navajowhite1","navajowhite2","navajowhite3","navajowhite4","lemonchiffon1","lemonchiffon2",
"lemonchiffon3","lemonchiffon4","ivory1","ivory2","ivory3","ivory4","honeydew","lavenderblush1","lavenderblush2","lavenderblush3",
"lavenderblush4","mistyrose1","mistyrose2","mistyrose3","mistyrose4","azure1","azure2","azure3","azure4","slateblue1","slateblue2",
"slateblue3","slateblue4","royalblue1","royalblue2","royalblue3","royalblue4","dodgerblue1","dodgerblue2","dodgerblue3","dodgerblue4",
"steelblue1","steelblue2","steelblue3","steelblue4","deepskyblue1","deepskyblue2","deepskyblue3","deepskyblue4","skyblue1","skyblue2",
"skyblue3","skyblue4","lightskyblue1","lightskyblue2","lightskyblue3","lightskyblue4","slategray1","slategray2","slategray3","slategray4",
"lightsteelblue1","lightsteelblue2","lightsteelblue3","lightsteelblue4","lightblue1","lightblue2","lightblue3","lightblue4","lightcyan1",
"lightcyan2","lightcyan3","lightcyan4","paleturquoise1","paleturquoise2","paleturquoise3","paleturquoise4","cadetblue1","cadetblue2",
"cadetblue3","cadetblue4","turquoise1","turquoise2","turquoise3","turquoise4","cyan1","cyan2","cyan3","cyan4","darkslategray1","darkslategray2",
"darkslategray3","darkslategray4","aquamarine1","aquamarine2","aquamarine3","aquamarine4","darkseagreen1","darkseagreen2","darkseagreen3",
"darkseagreen4","seagreen1","seagreen2","seagreen3","seagreen4","palegreen1","palegreen2","palegreen3","palegreen4","springgreen1",
"springgreen2","springgreen3","springgreen4","chartreuse1","chartreuse2","chartreuse3","chartreuse4","olivedrab1","olivedrab2","olivedrab3",
"olivedrab4","darkolivegreen1","darkolivegreen2","darkolivegreen3","darkolivegreen4","khaki1","khaki2","khaki3","khaki4","lightgoldenrod1",
"lightgoldenrod2","lightgoldenrod3","lightgoldenrod4","yellow1","yellow2","yellow3","yellow4","gold1","gold2","gold3","gold4","goldenrod1",
"goldenrod2","goldenrod3","goldenrod4","darkgoldenrod1","darkgoldenrod2","darkgoldenrod3","darkgoldenrod4","rosybrown1","rosybrown2",
"rosybrown3","rosybrown4","indianred1","indianred2","indianred3","indianred4","sienna1","sienna2","sienna3","sienna4","burlywood1",
"burlywood2","burlywood3","burlywood4","wheat1","wheat2","wheat3","wheat4","tan1","tan2","tan3","tan4","chocolate1","chocolate2","chocolate3",
"chocolate4","firebrick1","firebrick2","firebrick3","firebrick4","brown1","brown2","brown3","brown4","salmon1","salmon2","salmon3","salmon4",
"lightsalmon1","lightsalmon2","lightsalmon3","lightsalmon4","orange1","orange2","orange3","orange4","darkorange1","darkorange2","darkorange3",
"darkorange4","coral1","coral2","coral3","coral4","tomato1","tomato2","tomato3","tomato4","orangered1","orangered2","orangered3",
"orangered4","deeppink1","deeppink2","deeppink3","deeppink4","hotpink1","hotpink2","hotpink3","hotpink4","pink1","pink2","pink3",
"pink4","lightpink1","lightpink2","lightpink3","lightpink4","palevioletred1","palevioletred2","palevioletred3","palevioletred4","maroon1",
"maroon2","maroon3","maroon4","violetred1","violetred2","violetred3","violetred4","magenta1","magenta2","magenta3","magenta4","mediumred","orchid1",
"orchid2","orchid3","orchid4","plum1","plum2","plum3","plum4","mediumorchid1","mediumorchid2","mediumorchid3","mediumorchid4","darkorchid1",
"darkorchid2","darkorchid3","darkorchid4","purple1","purple2","purple3","purple4","mediumpurple1","mediumpurple2","mediumpurple3","mediumpurple4",
"thistle1","thistle2","thistle3","thistle4","gray1","gray2","gray3","gray4","gray5","gray6","gray7","gray8","gray9","darkgray","darkblue",
"darkcyan","darkmagenta","darkred","silver","eggplant","lightgreen");

?>