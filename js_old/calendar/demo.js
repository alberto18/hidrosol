$(document).ready( function(){
  var cTime = new Date(), month = cTime.getMonth()+1, year = cTime.getFullYear();

	theMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

	theDays = ["S", "M", "T", "W", "T", "F", "S"];
    events = [
      [
        "5/"+month+"/"+year, 
        'Vacaciones', 
        '#', 
        '#fb6b5b', 
        'Solicitud ACEPTADA'
      ],
      [
        "8/"+month+"/"+year, 
        'Dia no habil', 
        '#', 
        '#ffba4d', 
        'Festividad de San Antonio'
      ],
      [
        "18/"+month+"/"+year, 
        'Asuntos propios', 
        '#', 
        '#ffba4d', 
        'Solicitud EN TRAMITE'
      ]
    ];
    $('#calendar').calendar({
        months: theMonths,
        days: theDays,
        events: events,
        popover_options:{
            placement: 'top',
            html: true
        }
    });
});