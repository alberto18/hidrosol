<?php
echo "
<script type=\"text/JavaScript\">
<!--";

//FUNCION QUE SE LE PASA EL NOMBRE DE UN INPUT Y COMRPUEBA QUE SOLO TENGA NUMEROS
//SINO ES ASI BORRA EL CAMPO
echo "
function validar(nombre)
{
	var nombre;
	var patron = /^\d+$/;
	var largo = nombre.value.length;
	if (largo > 0 && !nombre.value.match(patron))
	{
		alert('Solo se pueden escribir numeros.');
		nombre.value = nombre.value.substring(0,largo-1);
	}
}";

echo "
function validarFloat(nombre)
{
	var nombre;
	var patron = /^\d+(\.\d{0,2})?$/;
	var largo = nombre.value.length;
	if (largo > 0 && !nombre.value.match(patron))
	{
		alert('Solo se pueden escribir numeros con dos decimales.');
		nombre.value = nombre.value.substring(0,largo-1);
	}
}
";

/*
echo "
function validarLogin(nombre)
{
	var nombre;
	var patron = /[\s|'|".'"'."]+/;
	var largo = nombre.value.length;
	if (largo > 0 && nombre.value.match(patron))
	{
		alert('No se permiten los espacios, comas simples o dobles en este tipo de campos.');
		nombre.value = nombre.value.substring(0,largo-1);
	}
}
";
*/

//FUNCION UTILIZADA EN LOS SELECT PARA QUE RECARGUE LA PAGINA QUE TIENE CADA
//OPTION EN EL VALUE
//FORMA DE LLAMARLA: PONER LO SIGUIENTE EN LA DEFINICION DEL SELECT
//onchange=\"saltoPagina('parent',this,0)\"
echo "
function saltoPagina(targ,selObj,restore)
{ 
	eval(targ+\".location='\"+selObj.options[selObj.selectedIndex].value+\"'\");
	if (restore) selObj.selectedIndex = 0;
}
";

//FUNCION QUE ABRE UN POPUP CON UN MAPA GOOGLE
echo "
function abrirMapa(cto_id)
{
	var cto_id;
	var url = \"modules.php?mod=portal&file=ver_mapa&cto=\"+cto_id+\"\";
	var miPopup;
	miPopup = window.open(url,\"mapa\",\"width=600,height=400,menubar=no,scrollbars=yes\");
}
";

//FUNCION QUE ABRE UN POPUP CON LA AYUDA DEL BUSCADOR
echo "
function abrirAyuda(ayu_id)
{
	var ayu_id;
	var url = \"modules.php?mod=tienda&file=ver_ayuda&ayu=\"+ayu_id+\"\";
	var miPopup;
	miPopup = window.open(url,\"mapa\",\"width=600,height=400,menubar=no,scrollbars=yes\");
}
";

//FUNCION QUE ABRE UN POPUP CON LAS EXISTENCIAS DE UN ARTICULO
echo "
function abrirStock(articulo_id, prov_id)
{
	var articulo_id;
	var prov_id;
	var url = \"modules.php?mod=gestproject&file=ver_stock_articulo&articulo_id=\"+articulo_id+\"&proveedor_id=\"+prov_id+\"\";
	var miPopup;
	miPopup = window.open(url,\"stock\",\"width=600,height=400,menubar=no,scrollbars=yes\");
}
";

echo "
function validarEmail(valor) {
	var resultado;
	var patron = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3,4})+$/;
	if (!valor.match(patron))
	{
		resultado = 'error';
	}
	return resultado;
}";

echo "
//-->
</script>";

?>