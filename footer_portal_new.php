<?php
$ano_actual = date("Y");
?>

 <!-- Footer -->
<footer id="footer">
  <div class="footer-container">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
          <div class="block-contact">
            <ul id="block-contact_list">
              <li class="block_1"> <a href="https://www.google.es/maps/place/Hidrosol+Canarias/@27.8757945,-15.4208567,3a,75y,211.79h,84.59t/data=!3m6!1e1!3m4!1syqnh84Eem4rXRhFrCXyKUA!2e0!7i13312!8i6656!4m12!1m6!3m5!1s0xc409f45f32821c9:0x13c3dea2ec2a6390!2sHidrosol+Canarias!8m2!3d27.8768981!4d-15.4229161!3m4!1s0xc409f45f32821c9:0x13c3dea2ec2a6390!8m2!3d27.8768981!4d-15.4229161" target="_blank"><i class="fa fa-map-marker"></i> <span class="contactdiv">C/De la Cizalla M3 37-P3N
			Polig. Ind. Arinaga CP:35118 </a><br> Tel 928 184 180</span> </li>
              <li class="block_2"> <a href="https://www.google.es/maps/place/Calle+Corregidor+Aguirre,+2,+35219+Telde,+Las+Palmas/@27.9696309,-15.3903964,3a,75y,48.69h,100.3t/data=!3m8!1e1!3m6!1sLlsVYunniNXkWpfPuRfaKw!2e0!5s20120801T000000!6s%2F%2Fgeo0.ggpht.com%2Fcbk%3Fpanoid%3DLlsVYunniNXkWpfPuRfaKw%26output%3Dthumbnail%26cb_client%3Dmaps_sv.tactile.gps%26thumb%3D2%26w%3D203%26h%3D100%26yaw%3D222.25047%26pitch%3D0%26thumbfov%3D100!7i13312!8i6656!4m5!3m4!1s0xc40a2bbcc08e569:0xfac90f9156cb035a!8m2!3d27.9697132!4d-15.3903701" target="_blank"><i class="fa fa-map-marker"></i> <span>C/Corregidor Aguirre, 2
			Polig. Ind. El Goro (Telde) CP:35200 </a><br> Tel. 928 700 052</span> </li>
              <li class="block_3"> <i class="fa fa-envelope"></i> <span>
			  info@hidrosolcanarias.com</span> </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
          <div class="block_newsletter links">
           <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 links wrapper">
              <h3> <a href="representaciones" rel="nofollow"> Representaciones </a> </h3>
              <ul class="account-list collapse" id="footer_account_list">
                <li> <a href="http://www.fluidra.es/" title="Página web de Fluidra" rel="nofollow" target="_blank"> Fluidra </a> </li>
                <li> <a href="http://www.inoxidables.com/flexinoxpool/" title="Página web de Flexinox" rel="nofollow" target="_blank"> Flexinox </a> </li>
                <li> <a href="http://idegis.es/" title="Página web de Idegis" rel="nofollow" target="_blank"> Idegis </a> </li>
                <li> <a href="http://www.certikin.com/" title="Página web de Certikin" rel="nofollow" target="_blank"> Certikin </a> </li>
				<li> <a href="http://www.grupopid.com/" title="Página web de Piscinas PID" rel="nofollow" target="_blank"> Piscinas PID </a> </li>
				<li> <a href="http://www.hayward.es/" title="Página web de Hayward" rel="nofollow" target="_blank"> Hayward, bombas de calor </a> </li>
				<li> <a href="http://www.wolfiberica.es/" title="Página web de WolfIberica" rel="nofollow" target="_blank"> Wolf </a> </li>
				<li> <a href="http://www.chromagen.es/" title="Página web de Chromagen" rel="nofollow" target="_blank"> Chromagen </a> </li>
				<li> <a href="http://www.dabpumps.es/" title="Página Web de DAB" rel="nofollow" target="_blank"> Dab </a> </li>
				<li> <a href="http://www.bombaprinze.com/" title="Página web de Bomba Prinze" rel="nofollow" target="_blank"> Bomba Prinze </a> </li>
				<li> <a href="http://www.ferroli.es" title="Página web de Ferroli" rel="nofollow" target="_blank"> Ferroli </a> </li>
				<li> <a href="http://www.thermor.es/" title="Página web de Thermor" rel="nofollow" target="_blank"> Thermor </a> </li>
				<li> <a href="http://www.astralpool.com/" title="Página web de astrapool" rel="nofollow" target="_blank"> Astrapool</a> </li>
				<li> <a href="http://www.scpeurope.es/" title="Página web de Scp Pool" rel="nofollow" target="_blank"> Scp Pool</a> </li>
				<li> <a href="http://www.proindecsa.com/" title="Página web de Proindecsa" rel="nofollow" target="_blank"> Proindecsa</a> </li>
				<li> <a href="http://inerox.com/" title="Página web de Inerox" rel="nofollow" target="_blank"> Inerox</a> </li>
				<li> <a href="#" title="Página web de HA+ TECH" rel="nofollow" target="_blank"> HA+ TECH</a> </li>
				<li> <a href="http://suicalsa.com/" title="Página web de Suicalsa" rel="nofollow" target="_blank"> Suicalsa </a> </li>
				<li> <a href="http://www.hpe-technology.com" title="Página web de HPE" rel="nofollow" target="_blank"> HPE </a> </li>
				<li> <a href="http://www.convesa.es/" title="Página web de Convesa" rel="nofollow" target="_blank"> Convensa </a> </li>
				<li> <a href="http://hidro-water.com/" title="Página web de Hidrowater" rel="nofollow" target="_blank"> Hidrowater </a> </li>
				<li> <a href="http://www.retex.es/" title="Página web de Bubblesun" rel="nofollow" target="_blank"> Bubblesun </a> </li>
				
              </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 links block links">
              <h3><a href="tienda"><span>Productos</span></a></h3>
              <ul class="collapse block_content">
                
				  <?php
					  
						$cons = $conn_pdo_erp->prepare("select * from maestro_categorias_articulos_t where mostrar_web='on'");
						try { 
							$cons->execute();
						} catch (PDOException $e) { 
							echo "Error en la consulta: ".$e->getMessage(); 
						} 

						while($lin = $cons->fetch(PDO::FETCH_ASSOC)) {
							$exito = 1;
							$id_categoria = $lin['id'];
							$nombre = $lin['nombre'];
							//$id_categoria = base64_encode(base64_encode($id_categoria));							
							// Aqui tengo que meter el codigo HTML necesario para CADA COLUMNA
							$nombre=ucfirst(mb_strtolower($nombre,"UTF-8"));
							?>
							
							<li class="level3 nav-6-1 parent item"> <a href="modules.php?mod=portal&file=listado_productos&categoria=<?php echo $id_categoria ?>"><span><?php echo $nombre ?></span></a>
						
						    </li>
							
							<?php
						}					  
					  
					  ?>
				
				
              </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 links block links">
              <h3>HIDROSOL CANARIAS</h3>
              <ul class="collapse block_content">
                <li> <a id="link-cms-page-1-2" class="cms-page-link" href="quienes-somos" title="Información de nuestra empresa"> ¿Quiénes somos? </a> </li>
                <li> <a id="link-cms-page-2-2" class="cms-page-link" href="formacion" title="Formate con nosotros"> Formación </a> </li>
                <li> <a id="link-cms-page-4-2" class="cms-page-link" href="servicios" title="Aprende más sobre nosotros"> Servicios </a> </li>
                <li> <a id="link-cms-page-5-2" class="cms-page-link" href="obras-referencia" title="Trabajos realizados"> Obras de referencia </a> </li>
                <li> <a id="link-static-page-contact-2" class="cms-page-link" href="contactar" title="Pongase en contacto con nosotros"> Contacto </a> </li>
                <li> <a id="link-static-page-sitemap-2" class="cms-page-link" href="aviso-legal" title="Aviso legal"> Aviso legal </a> </li>
                <li> <a id="link-static-page-stores-2" class="cms-page-link" href="politica-de-cookies" title=""> Política de cookies </a> </li>
              </ul>
            </div>
           </div>
          </div>
        </div>
      </div><!--row-->
    <div class="footer-bottom">
		<div class="container">
		<div id="links_block_left" class="block col-md-4 links">
            <ul id="tm_blocklink" class="block_content collapse">
              <li> <a href="aviso-legal" title="Aviso legal">AVISO LEGAL</a></li>
              <li> <a href="politica-de-cookies" title="Política de cookies">POLITICA DE COOKIES</a></li>
              <li> <a href="contactar" title="Telefono">Tel. 928 184 180</a></li>
              <li> <a href="contactar" title="Fax">Fax. 928 180 729</a></li>
              <li> <a href="contactar" title="Email">info@hidrosolcanarias.com</a></li>
            </ul>
          </div>
			
			<div class="copyright"><a class="_blank" href="inicio-hidrosol" target="_blank">   &copy; Sitio web oficial de Hidrosol Canarias. </a></div><br/>
			<div class="copyright"><img src="images/logos_innobonos.jpg" title="Fondo Europeo de Desarrollo Regional"></div>
		</div>
	</div>
   </div>
   
  </div>
  <div id="cookie-policy" class="notice hidden">
		<div class="bg"></div>
		<div class="content22">
			<br>
			<p>Utilizamos cookies propias y de terceros para mejorar nuestros servicios y mostrarle publicidad relacionada con sus preferencias mediante el an&aacute;isis de sus h&aacute;bitos de navegaci&oacute;n. Si continua navegando, consideramos que acepta su uso. Puede cambiar la configuraci&oacute;n u obtener m&aacute;s informaci&oacute;n <a href="politica-de-cookies">aqu&iacute;</a>.<br><a class="close" href="#">Entendido</a></p>
		</div>
	</div>
</footer>

<!-- End Footer --> 

<!-- mobile menu -->
<div id="mobile-menu">
  <ul>
    <li>
      <div class="mm-search">
        <form action="modules.php?mod=portal&file=listado_productos" id="search1" method="POST">
          <div class="input-group">
            <div class="input-group-btn">
              <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> </button>
            </div>
			 <input type="text" placeholder="Buscar producto..."  maxlength="70" name="etiquetas"  id="srch-term" class="form-control simple">
          <!--  <input type="text" class="form-control simple" placeholder="Buscar producto..." name="srch-term" id="srch-term">-->
          </div>
        </form>
		
      </div>
    </li>
    <li><a href="inicio-hidrosol">Inicio</a> </li>
	<li><a href="representaciones">Representaciones</a> </li>
    <li><a href="tienda">VENTA ONLINE</a>
      <ul>
	  
	  
	   <?php
					  
						$cons = $conn_pdo_erp->prepare("select * from maestro_categorias_articulos_t where mostrar_web='on'  order by orden asc limit 6");
						try { 
							$cons->execute();
						} catch (PDOException $e) { 
							echo "Error en la consulta: ".$e->getMessage(); 
						} 

						while($lin = $cons->fetch(PDO::FETCH_ASSOC)) {
							$exito = 1;
							$id_categoria = $lin['id'];
							$nombre = $lin['nombre'];
							$img_categoria=$lin['nombre_fichero'];
							
							?>
							
							<li class="level3 nav-6-1 parent item"> <a href="modules.php?mod=portal&file=listado_productos&categoria=<?php echo $id_categoria ?>"><span><?php echo $lin['nombre'] ?></span></a>
							<ul class="level1">
							
							  <?php 
								// sacar 5 productos de esta categoria 
								$cons2 = $conn_pdo_erp->prepare("select * from articulos_t where (categoria_id=:categoria_id or categoria_id2=:categoria_id2 or categoria_id3=:categoria_id3)  and venta_en_web='on' and de_baja<>'on' order by id desc limit 5");
								$cons2->bindValue(':categoria_id', $lin['id'], PDO::PARAM_INT);
								$cons2->bindValue(':categoria_id2', $lin['id'], PDO::PARAM_INT);
								$cons2->bindValue(':categoria_id3', $lin['id'], PDO::PARAM_INT);
								try { 
									$cons2->execute();
								} catch (PDOException $e) { 
									echo "Error en la consulta: ".$e->getMessage(); 
								} 

								while($lin2 = $cons2->fetch(PDO::FETCH_ASSOC)) {
									
									$id_producto=$lin2['id'];									
									$id = base64_encode(base64_encode($id_producto));
									
									$nombre_articulo = utf8_encode($lin2['nombre']);
									
									?>
									 <li class="level2 nav-6-1-1"><a href="modules.php?mod=portal&file=ver_productos&id=<?php echo $id ?>&categoria=<?php echo $id_categoria ?>"><span><?php echo substr($nombre_articulo,0,20); ?></span></a></li>
									<?php
								}

							  
							  ?>
							
							  <div class="menu-img-block"> <a href="modules.php?mod=portal&file=listado_productos&categoria=<?php echo $id_categoria ?>"><img src="articulos/<?php echo  $img_categoria ?>"></a></div>
							</ul>
						    </li>
							
							<?php
						}					  
					  
					  ?>
     
      </ul>
    </li>
    <li><a href="#">Hidrosol</a>
      <ul>
       
		<li> <a href="quienes-somos" class="">¿Quiénes Somos</a> </li>
		<li> <a href="formacion" class="">Formación</a> </li>
		<li> <a href="servicios" class="">Servicios</a> </li>
		<li> <a href="obras-referencia" class="">Obras de referencia</a> </li>
      </ul>
    </li>
    <li><a href="clientes">Clientes</a>     
    </li>
    <li><a href="contactar">Contactar</a>      
    </li>
    <li><a href="sat">Asistencia técnica</a> </li>
    
  </ul>
  <div class="top-links">
    <ul class="links">
	
	
	
	
	<?php 
		if (!isset($_SESSION['tienda']['login'])){
		
		?>

		<li><a href="login" title="Conecta con tu cuenta" rel="nofollow"> <span class="hidden-md-down">Entrar</span> </a> 
		<li><a href="registro" title="Crea una cuenta de usuario" rel="nofollow"> <span class="hidden-md-down">Registrar</span> </a> </li>
		<?php 
	   
		}
		else{
		?>
	   
		<li><a href='perfil'>Mi Perfil</a></li> 
		<li><a href='mis-pedidos'>Mis Pedidos</a></li>
		<li><a  href="mi-cesta">Cesta</a> </li>
		<li><a href="logout" title="Desconectar" rel="nofollow"> <span class="hidden-md-down">Desconectar </span> </a> </li>
		
		
		<?php 
		}
		?>
	
    
    </ul>
  </div>
</div>


<!-- JavaScript --> 


<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/jquery.sliderPro.js"></script>

<script type="text/javascript" src="js/jquery.sliderPro.min.js"></script>


<script type="text/javascript" src="js/bootstrap.min.js"></script> 
<script type="text/javascript" src="js/common.js"></script> 
<script type="text/javascript" src="js/jquery.flexslider.js"></script> 
<script type="text/javascript" src="js/owl.carousel.min.js"></script> 
<script type="text/javascript" src="js/jquery.mobile-menu.min.js"></script> 
<script type="text/javascript" src="js/cloud-zoom.js"></script>
<script type="text/javascript" src="js/revslider.js"></script> 
<script type="text/javascript" src="js/countdown.js"></script> 
<script type='text/javascript'>
jQuery(document).ready(function() {
	jQuery('#rev_slider_4').show().revolution({
	dottedOverlay: 'none',
	delay: 5000,
	startwidth:1170,
	startheight: 684,
	hideThumbs: 200,
	thumbWidth: 200,
	thumbHeight: 50,
	thumbAmount: 2,
	navigationType: 'thumb',
	navigationArrows: 'solo',
	navigationStyle: 'round',
	touchenabled: 'on',
	onHoverStop: 'on',
	swipe_velocity: 0.7,
	swipe_min_touches: 1,
	swipe_max_touches: 1,
	drag_block_vertical: false,
	spinner: 'spinner0',
	keyboardNavigation: 'off',
	navigationHAlign: 'center',
	navigationVAlign: 'bottom',
	navigationHOffset: 0,
	navigationVOffset: 20,
	soloArrowLeftHalign: 'left',
	soloArrowLeftValign: 'center',
	soloArrowLeftHOffset: 20,
	soloArrowLeftVOffset: 0,
	soloArrowRightHalign: 'right',
	soloArrowRightValign: 'center',
	soloArrowRightHOffset: 20,
	soloArrowRightVOffset: 0,
	shadow: 0,
	fullWidth: 'on',
	fullScreen: 'off',
	stopLoop: 'off',
	stopAfterLoops: -1,
	stopAtSlide: -1,
	shuffle: 'off',
	autoHeight: 'off',
	forceFullWidth: 'on',
	fullScreenAlignForce: 'on',
	minFullScreenHeight: 0,
	hideNavDelayOnMobile: 1500,
	hideThumbsOnMobile: 'off',
	hideBulletsOnMobile: 'off',
	hideArrowsOnMobile: 'off',
	hideThumbsUnderResolution: 0,
	hideSliderAtLimit: 0,
	hideCaptionAtLimit: 0,
	hideAllCaptionAtLilmit: 0,
	startWithSlide: 0,
	fullScreenOffsetContainer: ''
});
});
</script> 
<!-- Hot Deals Timer 1--> 
<script type="text/javascript">
var dthen1 = new Date("12/25/17 11:59:00 PM");
	start = "05/09/15 03:02:11 AM";
	start_date = Date.parse(start);
	var dnow1 = new Date(start_date);
	if (CountStepper > 0)
	ddiff = new Date((dnow1) - (dthen1));
	else
	ddiff = new Date((dthen1) - (dnow1));
	gsecs1 = Math.floor(ddiff.valueOf() / 1000);
	
	var iid1 = "countbox_1";
	CountBack_slider(gsecs1, "countbox_1", 1);
</script>
<!-- JavaScript --> 
<script>

	$(document).ready(function(){
	
	
		$( "#add_cart_btn" ).on( "click", function() {
		
			var num_items = $('#qty').val();
			var url = "modules.php?mod=tienda&file=cesta&categoria=<?php echo $_REQUEST[categoria] ?>&amount=" + num_items + "&item=<?php echo htmlentities($_REQUEST['id'], ENT_QUOTES);?>";
			window.location.href = url;
		});
		
	});
	
	$(document).ready(function(){
	
	
		$( "#update_cart_btn" ).on( "click", function() {
		
			var num_items = $('#qty').val();
			
			var url = "modules.php?mod=tienda&file=cesta&categoria=<?php echo $_REQUEST[categoria] ?>&update=" + num_items + "&item=<?php echo htmlentities($_REQUEST['id'], ENT_QUOTES);?>";
			window.location.href = url;
		});
		
	});
	


	
	
	
	
	$(document).ready(function() {


  $(".update1_cart_btn").on("click", function() {

    var $td = $(this).parent();
    var id = $td.siblings('[name="id"]').val();
   // alert(id);
    /*var otra = $('#qty').val()*/
    var categoria = $('#categoria').val();
    var value = $td.siblings('.wrapper-qty').find('[name="qty"]').val();
   	var url = "modules.php?mod=tienda&file=cesta&categoria="+ categoria + "&update=" + value + "&item=" + id +"&origen=1";
		//alert (url);	
			window.location.href = url;
  });

});
	
	</script>
		<script type="text/javascript">
    $( document ).ready(function( $ ) {
      
      if($('#Img_carousel').length > 0){
      
        $( '#Img_carousel' ).sliderPro({
          width: 960,
          height: 500,
          fade: true,
          arrows: true,
          buttons: false,
          fullScreen: false,
          smallSize: 500,
          startSlide: 0,
          mediumSize: 1000,
          largeSize: 3000,
          thumbnailArrows: true,
          autoplay: false
        });
      
      }
    });


	
</script>
</body>
</html>