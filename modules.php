<?php
// Formato modules.php?mod=gestproject&file=index

if ($_REQUEST['mod'] != "") { $mod = $_REQUEST['mod']; }
if ($_REQUEST['file'] != "") { $file = $_REQUEST['file']; }

if (strpos($mod, ".") === true) { exit; }
if (strpos($file, ".") === true) { exit; }
if (strpos($mod, "/") === true) { exit; }
if (strpos($file, "/") === true) { exit; }

/*
$time_start = microtime_float();
function microtime_float()
{
   list($usec, $sec) = explode(" ", microtime());
   return ((float)$usec + (float)$sec);
}
*/

$enventanita = "no";
if ($_REQUEST['enventanita'] != "") { $enventanita = $_REQUEST['enventanita']; }
$mostrarencabezado = false;
$mostrarpie = false;
if ($enventanita == "no" && !(strpos($file,"embebido")>-1) && !(strpos($file,"_tabla")>-1))
{
	$mostrarencabezado = true;
	$mostrarpie = true;
}

//********************************************
//***************** HEADERS ******************
//********************************************
$array_scripts_sin_header_footer = array("index_productos_busquedas_new","index_empresas_busquedas_new","index_locales_busquedas_new","index_vehiculos_pda_new","index_empresas_contactos_new","index_empresas_locales_new","index_locales_contactos_new","index_contratos_new","index_contratos_locales_new","index_empresas_noconformidades_gestiones_new","index_empresas_noconformidades_partes_adhoc","informe_recibos","informe_ejemplo_barras","informe_ejemplo_quesos","informe_encuesta_calidad","informe_materiales","iframe_estadisticas","iframe_estadisticas2","iframe_estadisticas3","iframe_estadisticas4","iframe_estadisticas5","iframe_estadisticas6","iframe_estadisticas7","iframe_estadisticas8","iframe_estadisticas9","iframe_estadisticas10","iframe_estadisticas11","iframe_estadisticas12","iframe_estadisticas13","imprimir_contactos",'ver_presupuesto','ver_albaran','ver_factura_imp');

if (!in_array($file,$array_scripts_sin_header_footer)) {

		$array_gestproject_header_inicio = array("ver_prov_planning","ver_correo_oferta","ver_cod_barra","ver_correo_pedido","ver_correo_presupuesto","ver_correo_albaran","ver_stock_articulo","iframe_estadisticas","iframe_estadisticas2","iframe_estadisticas3","iframe_estadisticas4","iframe_estadisticas5","iframe_estadisticas6","iframe_estadisticas7","iframe_estadisticas8","iframe_estadisticas9","iframe_estadisticas10","iframe_estadisticas11","iframe_estadisticas12","iframe_estadisticas13","imprimir_contactos",'ver_factura_imp');
		$array_files_agenda = array("index_agenda_new");
		if ($mod == "gestproject" || $mod == "informes")
		{
			if (in_array($file,$array_gestproject_header_inicio)) { include("header_inicio.php"); }
			//elseif ($_REQUEST['decalendario'] == "Si" && in_array($file,$array_files_agenda)) { include("header_inicio.php"); }
			elseif ($mostrarencabezado == true) { include("header.php"); }
		}
		if ($mod == "inicio") { include("header_inicio.php"); }
		if ($mod == "extranet" and $file !== 'index') { include("header_extranet.php"); }
		if ($mod == "extranet" and $file == 'index') { include("header_inicio.php"); }
		if ($mod == "bolsa") { include("header_portal_new_bolsa.php"); }
		if ($mod == "portal") { include("header_portal_new.php"); }
		if ($mod == "tienda") { include("header_portal_new.php"); }
		//if ($mod == "portal") { include("header_portal.php"); }
} else {
	if ($_REQUEST['file']!="ver_factura_imp"){
		include("header_limpio.php");
	}
}


 
$fichero = "modules/$mod/$file.php";
//echo "Fichero: $fichero";
include("$fichero");

//********************************************
//***************** FOOTERS ******************
//********************************************
if (!in_array($file,$array_scripts_sin_header_footer)) {

		$array_gestproject_footer_inicio = array("ver_prov_planning","ver_correo_oferta","ver_cod_barra","ver_correo_pedido","ver_correo_presupuesto","ver_correo_albaran","ver_stock_articulo","index_cuadrante_new",'ver_factura_imp');
		if (($mod == "gestproject" || $mod == "extranet") && $mostrarpie == true)// && !($_REQUEST['decalendario'] == "Si" && in_array($file,$array_files_agenda)))
		{
			if (!in_array($file,$array_gestproject_footer_inicio)) { 
				if ($_REQUEST['file']!="ver_factura_imp"){
					include("footer.php");
				}
			}	
		}
		
		if ($mod == "portal") { include("footer_portal_new.php"); }
		if ($mod == "tienda") { include("footer_portal_new.php"); }
		
		/*
		if ($mod == "inicio" && $file == "index")// && !($_REQUEST['decalendario'] == "Si" && in_array($file,$array_files_agenda)))
		{
			include("footer.php");
		}
		*/
		
}

?>