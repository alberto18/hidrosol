/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';
	config.allowedContent = true;
	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';

	// MARIO modificaciones para el upload de ficheros
	config.filebrowserBrowseUrl = '/tacgestorcontenidos_hidrosol2017/extras/ckeditor/filemanager/browser/default/browser.html?Connector=../../connectors/php/connector.php';
	config.filebrowserImageBrowseUrl = '/tacgestorcontenidos_hidrosol2017/extras/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=../../connectors/php/connector.php';
	config.filebrowserFlashBrowseUrl = '/tacgestorcontenidos_hidrosol2017/extras/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=../../connectors/php/connector.php';
	config.filebrowserUploadUrl = '/tacgestorcontenidos_hidrosol2017/extras/ckeditor/filemanager/connectors/php/upload.php?Type=File';
	config.filebrowserImageUploadUrl = '/tacgestorcontenidos_hidrosol2017/extras/ckeditor/filemanager/connectors/php/upload.php?Type=Image';
	config.filebrowserFlashUploadUrl = '/tacgestorcontenidos_hidrosol2017/extras/ckeditor/filemanager/connectors/php/upload.php?Type=Flash';
};
