<!DOCTYPE html>
<html lang="en" class="app">
<head>
  <?php
  include("variables_globales_gestproject.php");
  include("funciones.php");
  ?>
  <meta charset="utf-8" />
  <title>TACgestorcontenidos</title>
  <link rel="icon" href="documentos/favicon.png">


  <meta name="description" content="app22, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="css/animate.css" type="text/css" />
  <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="css/font.css" type="text/css" />

  <link rel="stylesheet" href="css/app.css" type="text/css" /> 
	<!--[if lt IE 9]>
    <script src="js/ie/html5shiv.js"></script>
    <script src="js/ie/respond.min.js"></script>
    <script src="js/ie/excanvas.js"></script>
  <![endif]-->
  
  
  <!-- ckeditor -->
<script src="extras/ckeditor/ckeditor.js"></script>

  
  <?php
	if ($_REQUEST[file] == "index_cuadrante_new") {
  ?>
    <link rel="stylesheet" href="js/fullcalendar/fullcalendar.css" type="text/css"  />
    <link rel="stylesheet" href="js/fullcalendar/theme.css" type="text/css" />
  <?php
	} else {
  ?>
     <link rel="stylesheet" href="js\calendar/bootstrap_calendar.css" type="text/css" />
  <?php
    }
  ?>
  
  <!-- Magnific Popup core CSS file -->
  <link rel="stylesheet" href="styles/magnific-popup.css"> 
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" />

  <script type="text/javascript" src="extras/plupload/js/plupload.full.min.js"></script>
  
  <style>
	@media print
	{
	.noprint {display:none;}
	}
  </style>

  <?php
  include("js/funciones.php");
  include("js/calendar_files.php"); 
  ?>
  
  

</head>
<body>
  <section class="vbox">

    <header class="bg-black dk header navbar navbar-fixed-top-xs" style="background-image: url('https://ablehd20.files.wordpress.com/2013/03/fondos-vectoriales-01.jpg');">
      <div class="navbar-header aside-md">
        <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html">
          <i class="fa fa-bars"></i>
        </a>
       <!-- <a href="modules.php?mod=gestproject&file=index" class="navbar-brand"><img src="images/logo_tacportalempleado.png" class="m-r-sm">tacportal</a>-->
	  <a href="#" class="navbar-brand" style="color: #FFFFFF;">TACgestor</a>
        <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user">
          <i class="fa fa-cog"></i>
        </a>
      </div>
      <ul class="nav navbar-nav hidden-xs">
      </ul>      
	  
	  <?php
	  
	  /*
		// Vemos el numero de mensajes/notificaciones no leidas
		$consulta6 = "select id, asunto, fecha, remitente_id from mensajes_t where user_id='$user_id' && (fecha_leido='' or fecha_leido is null);";
		//echo "$consulta6";
		$mensajes_sin_leer = 0;
		$contenido_mensajes = "";
		$resultado6 = mysql_query($consulta6) or die("La consulta fall&oacute;: $consulta6 " . mysql_error());
		while ($linea6 = mysql_fetch_array($resultado6, MYSQL_ASSOC)) {
			
			*/
				$mensajes_sin_leer++;
			//	list($mensaje_fecha, $mensaje_hora) = explode(' ', $linea6[fecha]);
			//	list($mensaje_ano, $mensaje_mes, $mensaje_dia) = explode('-', $mensaje_fecha);
			//	list($mensaje_hora, $mensaje_min) = explode(':', $mensaje_hora);
			//	$nombre_remitente = obtener_campo('nombre','usuarios_t','','id='.$linea6[remitente_id]);
			//	$mensaje_id_encript = base64_encode(base64_encode($linea6[id]));
				
				/*
				$contenido_mensajes .= '
                <a href="modules.php?mod=gestproject&file=index_mensajes_lectura_new&accion=formmodificar&id='.$mensaje_id_encript.'" class="media list-group-item">
                  <span class="pull-left thumb-sm">
                    <img src="images/avatar.jpg" class="img-circle">
                  </span>
                  <span class="media-body block m-b-none">
                    '.$nombre_remitente.' ha remitido un mensaje<br>
                    <small class="text-muted">'.$mensaje_dia.'/'.$mensaje_mes.'/'.$mensaje_ano.' '.$mensaje_hora.':'.$mensaje_min.'</small>
                  </span>
                </a>				
				';*/
		//}
		
		
	  ?>
	  
      <ul class="nav navbar-nav navbar-right hidden-xs nav-user">
        <!--<li class="hidden-xs">
          <a href="modules.php?mod=gestproject&file=index_mensajes_new" class="dropdown-toggle dk" data-toggle="dropdown">
            <i class="fa fa-bell"></i>
            <span class="badge badge-sm up bg-danger m-l-n-sm count"><?= $mensajes_sin_leer ?></span>
          </a>
          <section class="dropdown-menu aside-xl">
            <section class="panel bg-white">
              <header class="panel-heading b-light bg-light">
                <strong>Tienes <span class="count"><?= $mensajes_sin_leer ?></span> notificaciones</strong>
              </header>
              <div class="list-group list-group-alt animated fadeInRight">
					<?= $contenido_mensajes ?>
              </div>
              <footer class="panel-footer text-sm">
                <a href="modules.php?mod=gestproject&file=index_mensajes_new" class="pull-right"><i class="fa fa-cog"></i></a>
                <a href="modules.php?mod=gestproject&file=index_mensajes_new" data-toggle="class:show animated fadeInRight">Ver todas las notificaciones</a>
              </footer>
            </section>
          </section>
        </li>
        <li class="dropdown hidden-xs">
          <a href="#" class="dropdown-toggle dker" data-toggle="dropdown"><i class="fa fa-fw fa-search"></i></a>
          <section class="dropdown-menu aside-xl animated fadeInUp">
            <section class="panel bg-white">
              <form role="search" method=get action=modules.php>
			  <input type=hidden name=mod value=gestproject>
			  <input type=hidden name=file value=index_noticias_new>
                <div class="form-group wrapper m-b-none">
                  <div class="input-group">
                    <input name="buscar_cliente_1" class="form-control" placeholder="Buscar en noticias">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-info btn-icon"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </div>
              </form>
            </section>
          </section>
        </li>
		-->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="thumb-sm avatar pull-left">
              <img src="images/avatar.jpg">
            </span>
            <?= $nombre_empleado ?> <b class="caret"></b>
          </a>
          <ul class="dropdown-menu animated fadeInRight">
            <span class="arrow top"></span>
            <li>
              <a href="modules.php?mod=gestproject&file=index_miperfil_new">Perfil</a>
            </li>
         <!--   <li>
              <a href="modules.php?mod=gestproject&file=index_mensajes_new">
                <span class="badge bg-danger pull-right"><?= $mensajes_sin_leer ?></span>
                Notificaciones
              </a>
            </li>-->
			<!--
            <li>
              <a href="docs.html">Help</a>
            </li>
			-->
            <li class="divider"></li>
            <li>
              <a href="modules.php?mod=inicio&file=logout">Salir</a>
            </li>
          </ul>
        </li>
      </ul>      
    </header>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <aside class="bg-black lter aside-md hidden-print" id="nav">          
          <section class="vbox">
		  	<!--
            <header class="header bg-primary lter text-center clearfix">
              <div class="btn-group">
                <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                <div class="btn-group hidden-nav-xs">
                  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
                    Switch Project
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu text-left">
                    <li><a href="#">Project</a></li>
                    <li><a href="#">Another Project</a></li>
                    <li><a href="#">More Projects</a></li>
                  </ul>
                </div>
              </div>
            </header>
			-->
            <section class="w-f scrollable">
              <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">
                
                <!-- nav -->
                <nav class="nav-primary hidden-xs">
                  <ul class="nav">
				  
				   <?php
						if($grupo == "2" || $grupo == "7"){
				   ?>
				    <li>
                      <a href="#uikit"  >
                        <i class="fa fa-user icon">
                          <b class="bg-success"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Mi Perfil</span>
                      </a>
                      <ul class="nav lt">
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_miperfil_new" >
                            <i class="fa fa-angle-right"></i>
                            <span>Mis datos</span>
                          </a>
                        </li>
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_miperfil_cambiarclave_new" >                            
                            <!--<b class="badge bg-info pull-right">369</b>-->
                            <i class="fa fa-angle-right"></i>
                            <span>Cambiar clave</span>
                          </a>
                        </li>
                      </ul>
                    </li>
					<li>
                      <a href="modules.php?mod=gestproject&file=index_firmas_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span>Firmas</span>
                      </a>
                    </li>
					
				  <?php
						} elseif($grupo == "5") {
				   ?>
				   
				   
				   
					   
					  <li>
                      <a href="modules.php?mod=gestproject&file=index_contenidos_blogger_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span>Contenidos</span>
                      </a>
                    </li>
					   <li>
                      <a href="#uikit"  >
                        <i class="fa fa-user icon">
                          <b class="bg-success"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Mi Perfil</span>
                      </a>
                      <ul class="nav lt">
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_miperfil_new" >
                            <i class="fa fa-angle-right"></i>
                            <span>Mis datos</span>
                          </a>
                        </li>
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_miperfil_cambiarclave_new" >                            
                            <!--<b class="badge bg-info pull-right">369</b>-->
                            <i class="fa fa-angle-right"></i>
                            <span>Cambiar clave</span>
                          </a>
                        </li>
                      </ul>
                    </li>
					
					
					 <?php
						} elseif($grupo == "6") {
				   ?>
					   
					  <li>
                      <a href="modules.php?mod=gestproject&file=index_contenidos_streetstyle_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span>Contenidos</span>
                      </a>
                    </li>
					   <li>
                      <a href="#uikit"  >
                        <i class="fa fa-user icon">
                          <b class="bg-success"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Mi Perfil</span>
                      </a>
                      <ul class="nav lt">
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_miperfil_new" >
                            <i class="fa fa-angle-right"></i>
                            <span>Mis datos</span>
                          </a>
                        </li>
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_miperfil_cambiarclave_new" >                            
                            <!--<b class="badge bg-info pull-right">369</b>-->
                            <i class="fa fa-angle-right"></i>
                            <span>Cambiar clave</span>
                          </a>
                        </li>
                      </ul>
                    </li>
					
					
					 <?php
						} elseif($grupo == "8") { // press room
				   ?>
					   
					  <li>
                      <a href="modules.php?mod=gestproject&file=index_contenidos_pressroom_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span>Contenidos</span>
                      </a>
                    </li>
					<li>
                      <a href="#uikit"  >
                        <i class="fa fa-user icon">
                          <b class="bg-success"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Mi Perfil</span>
                      </a>
                      <ul class="nav lt">
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_miperfil_new" >
                            <i class="fa fa-angle-right"></i>
                            <span>Mis datos</span>
                          </a>
                        </li>
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_miperfil_cambiarclave_new" >                            
                            <!--<b class="badge bg-info pull-right">369</b>-->
                            <i class="fa fa-angle-right"></i>
                            <span>Cambiar clave</span>
                          </a>
                        </li>
                      </ul>
                    </li>
					
					
					
				   <?php
						} else {
				   ?>
					<!-- <li>
                      <a href="#uikit"  >
                        <i class="fa fa-user icon">
                          <b class="bg-success"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Contactos</span>
                      </a>
                      <ul class="nav lt">
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_contactos_new" >
                            <i class="fa fa-angle-right"></i>
                            <span>Todos los contactos</span>
                          </a>
                        </li>
                        <li>
                          <a href="modules.php?mod=gestproject&file=index_contactos_new&buscar_cliente_2=11" >                            
                            <!--<b class="badge bg-info pull-right">369</b>-->
               <!--             <i class="fa fa-angle-right"></i>
                            <span>Showroom</span>
                          </a>
                        </li>
						<li>
                          <a href="modules.php?mod=gestproject&file=index_contactos_new&buscar_cliente_2=1" >                            
                            <!--<b class="badge bg-info pull-right">369</b>-->
               <!--             <i class="fa fa-angle-right"></i>
                            <span>Blogosfera</span>
                          </a>
                        </li>
						<li>
                          <a href="modules.php?mod=gestproject&file=index_contactos_new&buscar_todos_press=1" >                            
                            <!--<b class="badge bg-info pull-right">369</b>-->
                 <!--           <i class="fa fa-angle-right"></i>
                            <span>Press Room</span>
                          </a>
                        </li>
                      </ul>
                    </li>-->  
                   
					<li>
                      <a href="modules.php?mod=gestproject&file=index_contenidos_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span>Contenidos</span>
                      </a>
                    </li>
					<!--<li>
                      <a href="modules.php?mod=gestproject&file=index_firmas_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span>Firmas</span>
                      </a>
                    </li>-->
					<li>
                      <a href="modules.php?mod=gestproject&file=index_topicos2_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                      <span>T&oacute;picos</span>
                      </a>
                    </li>
					<!--<li>
                      <a href="modules.php?mod=gestproject&file=index_eventos_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span>Eventos</span>
                      </a>
                    </li>-->
					<li>
                      <a href="modules.php?mod=gestproject&file=index_usuarios_new"  >
                        <i class="fa fa-user icon">
                          <b class="bg-danger"></b>
                        </i>
                        <span>Usuarios</span>
                      </a>
                    </li>
					<!--<li>
                      <a href="#uikit"  >
                        <i class="fa fa-user icon">
                          <b class="bg-success"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Mi Perfil</span>
                      </a>
                      <ul class="nav lt">
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_miperfil_new" >
                            <i class="fa fa-angle-right"></i>
                            <span>Mis datos</span>
                          </a>
                        </li>
                        <li >
                          <a href="modules.php?mod=gestproject&file=index_miperfil_cambiarclave_new" >                            
                            <!--<b class="badge bg-info pull-right">369</b>-->
                           <!-- <i class="fa fa-angle-right"></i>
                            <span>Cambiar clave</span>
                          </a>
                        </li>
                      </ul>
                    </li>-->
					 <li>
                      <a href="modules.php?mod=gestproject&file=configuracion">
                        <i class="fa fa-gear icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Configuraci&oacute;n</span>
                      </a>
                    </li>
					
				   <?php
						}
				   ?>
				  
				  <?php
				  if($login == "celena"){
				  ?>
				   <li>
                      <a href="modules.php?mod=gestproject&file=index_contenidos_modelos_new">
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span>Contenidos</span>
                      </a>
                    </li>
				 <?php
				 }// del if
				  ?>
                  </ul>
                </nav>
                <!-- / nav -->
              </div>
            </section>
            
			
            <footer class="footer lt hidden-xs b-t b-black">

              <a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-black btn-icon">
                <i class="fa fa-angle-left text"></i>
                <i class="fa fa-angle-right text-active"></i>
              </a>

            </footer>
		
			
          </section>
        </aside>
        <!-- /.aside -->
		
	

