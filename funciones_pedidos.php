<?php
/*
Funciones en el script:

VerPedido ($valor_pedido)
CalcularPedido ($pedido_id)
RecalcularPortesPedido ($pedido_id)
ActualiarPortesImportesPedido ($pedido_id)
AgTransporteProvPedido ($pedido_id, $proveedor_id)
*/

function VerPedido ($valor_pedido)
{
global $fckeditor_camino_relativo, $ruta_mailing, $oferta_color_texto, $oferta_color_borde, $oferta_estilo_tabla, $oferta_estilo1, $oferta_estilo_texto, $oferta_estilo1_no_fondo, $oferta_estilo2, $oferta_estilo2_top, $presupuesto_estilo_est11, $presupuesto_estilo_est11b, $presupuesto_estilo_est12, $presupuesto_estilo_est12b, $presupuesto_estilo_est13, $presupuesto_estilo_est13b, $presupuesto_estilo_est14, $presupuesto_estilo_est14b, $presupuesto_estilo_est15, $presupuesto_estilo_est15b, $presupuesto_estilo_est16, $presupuesto_estilo_est16b, $presupuesto_estilo_separador, $presupuesto_estilo_logo, $ruta_imagenes;

$tabla_pedidos = "pedidos_t";
$tabla_ped_art = "ped_articulos_t";
$tabla_articulos = "articulos_t";
$tabla_m_embalajes = "maestro_tipos_embalajes_t";
$tabla_m_igic = "maestro_igic_t";
$tabla_presupuestos = "presupuestos_t";
$tabla_clientes = "clientes_t";
$tabla_cli_direcciones = "clientes_direcciones_t";
$tabla_proveedores = "proveedores_t";
$tabla_ped_portes = "ped_portes_t";

$articulos_pagina = 15;
	
	$mensaje_pedido = "";
	$consulta_1 = "select * from $tabla_pedidos where id='".$valor_pedido."';";
	//echo "$consulta_1<br>";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1 La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{
		$nombre_cliente = "";
		$nif_cliente = "";
		$datos_direccion_cliente = "";
		$consulta_presu = "select * from $tabla_presupuestos where id='".$linea_1['presupuesto_id']."';";
		//echo "$consulta_presu";
		$resultado_presu = mysql_query($consulta_presu) or die("$consulta_presu La consulta fall&oacute;: " . mysql_error());
		while ($linea_presu = mysql_fetch_array($resultado_presu, MYSQL_ASSOC))
		{
			$consulta_2 = "select * from $tabla_clientes where id='".$linea_presu['cliente_id']."';";
			//echo "$consulta_2";
			$resultado_2 = mysql_query($consulta_2) or die("$consulta_2 La consulta fall&oacute;: " . mysql_error());
			$linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC);
			
			$nombre_cliente = $linea_2['nombre'];
			$nif_cliente = $linea_2['nif'];
			
			$consulta_dir = "select * from $tabla_cli_direcciones where id='".$linea_presu['cliente_direccion_id']."';";
			//echo "$consulta_dir";
			$resultado_dir = mysql_query($consulta_dir) or die("$consulta_dir La consulta fall&oacute;: " . mysql_error());
			while ($linea_dir = mysql_fetch_array($resultado_dir, MYSQL_ASSOC))
			{
				$datos_direccion_cliente = $linea_dir['direccion'];
				if ($linea_dir['direccion'] != "") { $datos_direccion_cliente .= ", "; }
				$datos_direccion_cliente .= $linea_dir['cp'];
				if ($datos_direccion_cliente != "") { $datos_direccion_cliente .= "<br>"; }
				$datos_direccion_cliente .= $linea_dir['poblacion'];
				if ($linea_dir['poblacion'] != "") { $datos_direccion_cliente .= "<br>"; }
				$nombre_provincia = "";
				$cons1_tmp = "select * from maestro_provincias_t where id='".$linea_dir['provincia_id']."';";
				$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp " . mysql_error());
				while ($lin1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
				{
					$nombre_provincia = $lin1_tmp['nombre'];
				}
				$nombre_municipio = "";
				$cons1_tmp = "select * from maestro_municipios_t where id='".$linea_dir['municipio_id']."';";
				$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp " . mysql_error());
				while ($lin1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
				{
					$nombre_municipio = $lin1_tmp['nombre'];
				}
				$datos_direccion_cliente .= $nombre_provincia;
				if ($nombre_provincia != "") { $datos_direccion_cliente .= ", "; }
				$datos_direccion_cliente .= $nombre_municipio;
			}
		}
		$contenido_logo = "<img src=\"images/logotipo_ayala.png\" /><br /><span class=\"est-logo\"></span>";
		$cabecera_pagina = "
<table width=\"99%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<!-- inicio cabecera -->
	<tr>
		<td style=\"padding-bottom:3px;\">
		<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
			<tr valign=\"top\">
				<td width=\"45%\" align=\"center\">".$contenido_logo."</td>
				<td width=\"55%\">
				<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
					<tr valign=\"bottom\">
						<td width=\"50%\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"left\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
							<tr align=\"center\">
								<td style=\"border-style:solid; border-width:1px; border-color:#000000;\"><span class=\"est15\">DOCUMENTO</span></td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;<span class=\"est16b\">PEDIDO</span></td>
							</tr>
						</table>
						</td>
						<td width=\"50%\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"right\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>
								<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
									<tr align=\"center\">
										<td width=\"25%\"><span class=\"est15\">P&aacute;gina:</span></td>
										<td width=\"75%\"><span class=\"est16\">#PAGINA#</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					<tr valign='top'>
						<td style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"left\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>&nbsp;&nbsp;<span class=\"est15b\">N&uacute;mero:</span> <span class=\"est16\">".$linea_1['numero']."</span></td>
							</tr>
						</table>
						</td>
						<td style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"right\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>
								<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
									<tr align=\"center\">
										<td width=\"25%\"><span class=\"est15\">Fecha:</span></td>
										<td width=\"75%\"><span class=\"est16\">".date("d/m/Y",strtotime($linea_1['fecha']))."</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan=\"2\" style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\" align=\"center\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>
								<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">
									<tr>
										<td colspan=\"2\"><span class=\"est15b\">".$nombre_cliente."</span></td>
									</tr>
									<tr>
										<td colspan=\"2\"><span class=\"est15\">".$datos_direccion_cliente."&nbsp;</span></td>
									</tr>
									<tr>
										<td width=\"75%\"><span class=\"est15b\">C.I.F./N.I.F.:</span> <span class=\"est15\">".$nif_cliente."</span></td>
										<td width=\"25%\"><span class=\"est15b\">&nbsp;</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
<!-- fin cabecera -->
</table>";

/*
			<tr>
				<td style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
				<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" height=\"40\">
					<tr valign='top'>
						<td><span class=\"est15b\">OBSERV.:</span> ";
		if ($linea_1['observaciones_privado'] != "")
		{
			$pie_pagina .= "<span class=\"est15\">".$linea_1['observaciones_privado']."</span>";
		}
		$pie_pagina .= "</td>
					</tr>
				</table>
				</td>
			</tr>
*/
		$pie_pagina = "
<table width=\"99%\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#ffffff;\">
<!-- inicio pie -->
	<tr>
		<td>
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
			<tr>
				<td>
				<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
					<tr>
						<td width=\"16%\" align=\"right\"><span class=\"est16b\">Importe Base:</span></td>
						<td width=\"17%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_sin_igic'],2,",",".")." &euro;</span></td>
						<td width=\"16%\" align=\"right\"><span class=\"est16b\">Importe I.G.I.C.:</span></td>
						<td width=\"17%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_igic'],2,",",".")." &euro;</span></td>
						<td width=\"17%\" align=\"right\"><span class=\"est16b\">Importe Total:</span></td>
						<td width=\"17%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_total'],2,",",".")." &euro;</span></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr valign=\"top\">
		<td style=\"padding-top:3px;\">
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" height=\"200\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
			<tr valign=\"top\">
				<td><span class=\"est15b\">PORTES:</span><br>";
		$cons_portes = "select * from $tabla_ped_portes where pedido_id='".$valor_pedido."' order by id;";
		$res_portes = mysql_query($cons_portes) or die("La consulta fall&oacute;: $cons_portes " . mysql_error());
		while ($lin_portes = mysql_fetch_array($res_portes, MYSQL_ASSOC))
		{
			$importe_total_portes += $lin_portes['importe_portes'];
			$nombre_proveedor = "";
			$cons1_tmp = "select * from proveedores_t where id='$lin_portes[proveedor_id]';";
			//echo "$cons1_tmp<br>";
			$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp" . mysql_error());
			while ($linea1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
			{
				$nombre_proveedor = $linea1_tmp['nombre'];
			}
			$estado_porte = "";
			if ($lin_portes['usar_cliente'] == "on") { $estado_porte = "debidos"; }
			else { $estado_porte = "pagados"; }
			$nombre_ag_transp = "";
			if ($lin_portes['usar_cliente'] == "on") { $cons1_tmp = "select * from maestro_agencias_transportes_t where id='$lin_portes[cli_ag_transporte_id]';"; }
			else { $cons1_tmp = "select * from maestro_agencias_transportes_t where id='$lin_portes[prov_ag_transporte_id]';"; }
			//echo "$cons1_tmp<br>";
			$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp" . mysql_error());
			while ($linea1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
			{
				$nombre_ag_transp = $linea1_tmp['nombre'];
			}
			$pie_pagina .= "<span class=\"est15\">Para proveedor ".$nombre_proveedor." portes ".$estado_porte.". Agencia: ".$nombre_ag_transp."</span><br />";
		}
		$pie_pagina .= "</td>
			</tr>
		</table>
		</td>
	</tr>
<!-- fin pie -->
</table>";
		
		// busqueda de los articulos del presupuesto
		$array_articulos_id = array();
		$consulta_3 = "select $tabla_ped_art.id from $tabla_ped_art where $tabla_ped_art.pedido_id='".$valor_pedido."' order by $tabla_ped_art.id;";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{
			$array_articulos_id[] = $linea_3['id'];
		}
		//echo print_r($array_articulos_id,true);
		// contabilizar articulos
		$num_arti = 0;
		$num_arti = count($array_articulos_id);
/*
		$consulta_3 = "select count($tabla_ped_art.id) as total from $tabla_ped_art where $tabla_ped_art.pedido_id='".$valor_pedido."';";
		//echo "$consulta_3<br>";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{
			$num_arti = $linea_3['total'];
		}
*/
		$total_paginas = (int) ceil($num_arti/$articulos_pagina);
		//echo "($num_arti)($total_paginas)<br>";
		$cuenta_articulos = 0;
		for ($pag = 1; $pag <= $total_paginas; $pag++)
		{
			//$limit = " limit ".(($pag-1)*$articulos_pagina).",$articulos_pagina";
			//echo "($limit)";
			$inicio = ($pag-1)*$articulos_pagina;
			$fin = $inicio+$articulos_pagina-1;
			// hay que comprobar que el fin definido no sobrepase el total real de articulos
			if ($fin >= count($array_articulos_id)) { $fin = count($array_articulos_id)-1; }
			$cuerpo_pagina = "
<table width=\"99%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<!-- inicio cuerpo -->
	<tr>
		<td>
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
			<tr>
				<td>
				<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">
					<tr align=\"right\">
						<td width=\"8%\"><span class=\"est16\">Referencia</span></td>
						<td width=\"22%\"><span class=\"est16\">Articulo</span></td>
						<td width=\"12%\"><span class=\"est16\">Prov.</span></td>
						<td width=\"8%\"><span class=\"est16\">Unid.</span></td>
						<td width=\"8%\"><span class=\"est16\">Precio</span></td>
						<td width=\"8%\"><span class=\"est16\">% Dto1</span></td>
						<td width=\"8%\"><span class=\"est16\">% Dto2</span></td>
						<td width=\"8%\"><span class=\"est16\">% Dto3</span></td>
						<td width=\"8%\"><span class=\"est16\">% Igic</span></td>
						<td width=\"10%\"><span class=\"est16\">Importe</span></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td style=\"padding-top:3px;\">
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" height=\"450\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
			<tr valign=\"top\">
				<td>
				<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
			for ($indice = $inicio; $indice <= $fin; $indice++)
			{
				$consulta_3 = "select $tabla_ped_art.* from $tabla_ped_art where $tabla_ped_art.id='".$array_articulos_id[$indice]."';";
				//echo "$consulta_3<br>";
				$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
				while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
				{
					$consulta_4 = "select * from $tabla_articulos where id='".$linea_3['articulo_id']."';";
					//echo "$consulta_4<br>";
					$resultado_4 = mysql_query($consulta_4) or die("$consulta_4 La consulta fall&oacute;: " . mysql_error());
					$linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC);
/*
					$nombre_embalaje = "";
					$consulta_5 = "select $tabla_m_embalajes.* from $tabla_m_embalajes where $tabla_m_embalajes.id='".$linea_3['tipo_embalaje_id']."';";
					//echo "$consulta_5<br>";
					$resultado_5 = mysql_query($consulta_5) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
					{
						$nombre_embalaje = "$linea_5[nombre] de $linea_3[unidades_embalaje]";
					}
						<td width=\"18%\"><span class=\"est12\">".$nombre_embalaje."</span></td>
*/
					$nombre_proveedor = "";
					$consulta_5 = "select $tabla_proveedores.* from $tabla_proveedores where $tabla_proveedores.id='".$linea_3['proveedor_id']."';";
					//echo "$consulta_5<br>";
					$resultado_5 = mysql_query($consulta_5) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
					{
						$nombre_proveedor = $linea_5['nombre_corto'];
					}
					$valor_igic = "";
					$consulta_5 = "select * from $tabla_m_igic where $tabla_m_igic.id='".$linea_3['igic_id']."';";
					$resultado_5 = mysql_query($consulta_5) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
					{
						$valor_igic = $linea_5['valor'];
					}
					$cuerpo_pagina .= "
					<tr align=\"right\">
						<td width=\"8%\"><span class=\"est12\">".$linea_4['referencia']."</span></td>
						<td width=\"22%\"><span class=\"est12\">".$linea_4['nombre']."</span></td>
						<td width=\"12%\"><span class=\"est12\">$nombre_proveedor</span></td>
						<td width=\"8%\"><span class=\"est12\">".$linea_3['unidades'];
					if ($linea_3['unidades_gratis'] > 0)
					{
						$cuerpo_pagina .= " + ".$linea_3['unidades_gratis'];
					}
					$cuerpo_pagina .= "</span></td>
						<td width=\"8%\"><span class=\"est12\">".number_format($linea_3['precio_unidad'],2,",",".")." &euro;</span></td>
						<td width=\"8%\"><span class=\"est12\">";
					if ($linea_3['dto_porc'] > 0)
					{
						$cuerpo_pagina .= number_format($linea_3['dto_porc'],2,",",".")." %";
					}
					$cuerpo_pagina .= "</span></td>
						<td width=\"8%\"><span class=\"est12\">";
					if ($linea_3['dto2'] > 0)
					{
						$cuerpo_pagina .= number_format($linea_3['dto2'],2,",",".")." %";
					}
					$cuerpo_pagina .= "</span></td>
						<td width=\"8%\"><span class=\"est12\">";
					if ($linea_3['dto3'] > 0)
					{
						$cuerpo_pagina .= number_format($linea_3['dto3'],2,",",".")." %";
					}
					$cuerpo_pagina .= "</span></td>
						<td width=\"8%\"><span class=\"est12\">".number_format($valor_igic,2,",",".")."</span></td>
						<td width=\"10%\"><span class=\"est12\">";
					if ($linea_3['reservado'] == "")
					{
						$cuerpo_pagina .= number_format($linea_3['importe_articulo'],2,",",".")." &euro;";
					}
					$cuerpo_pagina .= "</span></td>
					</tr>";
					$cuenta_articulos++;
				} // fin while linea_6
			} // fin del for
			if ($pag < $total_paginas)
			{
				$cuerpo_pagina .= "
					<tr>
						<td colspan=\"10\" align=\"left\"><span class=\"est12\">Suma y sigue</span></td>
					</tr>
";
			}
			$cuerpo_pagina .= "
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
<!-- fin cuerpo -->
</table>";
			//echo str_replace("#PAGINA#", $pag, $cabecera_pagina).$cuerpo_pagina.$pie_pagina;
			$mensaje_pedido .= "<table width=\"100%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
	<tr>
		<td>".str_replace("#PAGINA#", $pag, $cabecera_pagina)."</td>
	</tr>
	<tr>
		<td>".$cuerpo_pagina."</td>
	</tr>
	<tr>
		<td style=\"padding-top:3px;\">".$pie_pagina."</td>
	</tr>";
			$mensaje_pedido .= "
</table>";
/*
			if ($_REQUEST['logo'] == 1)
			{
				echo "
<span class=\"est11\">Seg&uacute;n el art 5 de la Ley 15/1999, los datos de car&aacute;cter personal de este documento, est&aacute;n incorporados en un fichero automatizado utilizado para la gesti&oacute;n administrativa de la empresa Cuymon 2000 S.L., por lo que tiene derecho a acceder a sus datos personales, rectificarlos o en su caso cancelarlos, solicit&aacute;ndolo en el domicilio se&ntilde;alado en el mismo.</span>";
			}
*/
			if ($pag < $total_paginas)
			{
				$mensaje_pedido .= "
<div class=\"separador\">&nbsp;</div>";
			}
		} // fin for paginado
		$fin_pagina = "
</body>
</html>
";
		//$mensaje_presupuesto .= $fin_pagina;
	} //fin while linea_2
	
	$mensaje_pedido = str_replace("class=\"est11\"", $presupuesto_estilo_est11, str_replace("class=\"est11b\"", $presupuesto_estilo_est11b, str_replace("class=\"est12\"", $presupuesto_estilo_est12, str_replace("class=\"est12b\"", $presupuesto_estilo_est12b, str_replace("class=\"est13\"", $presupuesto_estilo_est13, str_replace("class=\"est13b\"", $presupuesto_estilo_est13b, str_replace("class=\"est14\"", $presupuesto_estilo_est14, str_replace("class=\"est14b\"", $presupuesto_estilo_est14b, str_replace("class=\"est15\"", $presupuesto_estilo_est15, str_replace("class=\"est15b\"",$presupuesto_estilo_est15b, str_replace("class=\"est16\"", $presupuesto_estilo_est16, str_replace("class=\"est16b\"", $presupuesto_estilo_est16b, str_replace("class=\"separador\"", $presupuesto_estilo_separador, str_replace("class=\"est-logo\"", $presupuesto_estilo_logo, str_replace("images/", $ruta_imagenes, $mensaje_pedido)))))))))))))));
	
	return $mensaje_pedido;
}

function CalcularPedido ($pedido_id)
{
$tabla_ped_articulos = "ped_articulos_t";

	// los articulos reservados no se cobran
	$importe_total_sin = 0;
	$cons = "select sum(importe_articulo) as total from $tabla_ped_articulos where pedido_id='".$pedido_id."';";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$importe_total_sin = $lin['total'];
	}
	$importe_total_igic = 0;
	$cons = "select sum(importe_igic) as total from $tabla_ped_articulos where pedido_id='".$pedido_id."';";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$importe_total_igic = $lin['total'];
	}
	
	$array_final = array();
	$array_final[0] = $importe_total_sin;
	$array_final[1] = $importe_total_igic;
	$array_final[2] = $importe_total_sin+$importe_total_igic;
	return $array_final;
}

function RecalcularPortesPedido ($pedido_id)
{
global $identificador_gran_canaria;

$tabla_presupuestos = "presupuestos_t";
$tabla_pedidos = "pedidos_t";
$tabla_ped_art = "ped_articulos_t";
$tabla_clientes_direcciones = "clientes_direcciones_t";
$tabla_ped_portes = "ped_portes_t";

	$array_importes_proveedores = array();
	$cons3 = "select sum(importe_articulo) as total, $tabla_ped_art.proveedor_id from $tabla_ped_art where pedido_id='".$pedido_id."' group by $tabla_ped_art.proveedor_id;";
	//echo "$cons3<br>";
	$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
	while ($lin3 = mysql_fetch_array($res3, MYSQL_ASSOC))
	{
		$array_importes_proveedores[$lin3['proveedor_id']]["importe_arti"] = $lin3['total'];
	}
	//echo print_r($array_importes_proveedores,true);
	$consulta_cat = "select $tabla_clientes_direcciones.municipio_id from $tabla_clientes_direcciones 
join $tabla_presupuestos on $tabla_presupuestos.cliente_direccion_id=$tabla_clientes_direcciones.id 
join $tabla_pedidos on $tabla_pedidos.presupuesto_id=$tabla_presupuestos.id 
where $tabla_pedidos.id='".$pedido_id."';";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		$cons1 = "select * from maestro_municipios_t where id='".$linea_cat['municipio_id']."';";
		$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
		while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
		{
			if ($lin1['isla_id'] != $identificador_gran_canaria)
			{
				// hay que calcular portes
				foreach ($array_importes_proveedores as $valor_prov => $array_datos)
				{
					// busqueda del minimo que hay que superar para que no pague el cliente (primero el definido en clientes_prov_t, despues en proveedores_t)
					$importe_min_portes_pagados = 0;
					$existe_porte = 0;
					$cons_porte = "select * from $tabla_ped_portes where pedido_id='".$pedido_id."' and proveedor_id='".$valor_prov."';";
					//echo "$cons_porte<br>";
					$res_porte = mysql_query($cons_porte) or die("La consulta fall&oacute;: $cons_porte " . mysql_error());
					while ($lin_porte = mysql_fetch_array($res_porte, MYSQL_ASSOC))
					{
						$existe_porte = 1;
						$importe_min_portes_pagados = $lin_porte['importe_min_portes_pagados'];
					}
					if ($existe_porte == 1)
					{
						// contabilizo el importe sin igic
						$importe_total_prov = $array_datos["importe_arti"];//$array_datos["importe_igic"]
						$cons_update_porte = "update $tabla_ped_portes set ";
						if ($importe_total_prov <= $importe_min_portes_pagados)
						{
							// no supera el minimo para que sea el proveedor quien pague los portes: se marca que se debe usar la ag transportes del cliente
							$cons_update_porte .= " usar_cliente='on'";
						}
						else
						{
							// si se supera el minimo: como lo paga el proveedor el importe es cero y se marca que se debe usar la ag transportes del proveedor
							$cons_update_porte .= " usar_cliente=''";
						}
						$cons_update_porte .= " where pedido_id='".$pedido_id."' and proveedor_id='".$valor_prov."';";
						//echo "$cons_update_porte<br>";
						$resultado_update_porte = mysql_query($cons_update_porte) or die("$cons_update_porte, La consulta fall�: " . mysql_error());
					}
				} // fin foreach
			} // fin if
		} // $lin1
	} // $linea_cat
}

function ActualiarPortesImportesPedido ($pedido_id)
{
	// se recalculan los portes
	RecalcularPortesPedido ($pedido_id);
	// se actualizan los importes en el pedido
	$array_resul = CalcularPedido($pedido_id);
	$importe_sin_igic = $array_resul[0];
	$importe_igic = $array_resul[1];
	$importe_total = $array_resul[2];
	$consulta_pedidoo2 = "update pedidos_t set importe_sin_igic=\"".$importe_sin_igic."\", importe_igic=\"".$importe_igic."\", importe_total=\"".$importe_total."\" where id='".$pedido_id."';";
	//echo "$consulta_pedidoo2<br>";
	$res_pedido2 = mysql_query($consulta_pedidoo2) or die("$consulta_pedidoo2, La consulta fall�: " . mysql_error());
}

function AgTransporteProvPedido ($pedido_id, $proveedor_id)
{
$tabla_ped_portes = "ped_portes_t";
	$valor_ag_transporte_id = 0;
	$cons2 = "select * from $tabla_ped_portes where pedido_id='".$pedido_id."' and proveedor_id='".$proveedor_id."';";
	$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
	while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
	{
		if ($lin2['usar_cliente'] == "on") { $valor_ag_transporte_id = $lin2['cli_ag_transporte_id']; }
		else { $valor_ag_transporte_id = $lin2['prov_ag_transporte_id']; }
	}
	return $valor_ag_transporte_id;
}

?>