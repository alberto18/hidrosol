<!DOCTYPE html>
<html lang="en" class="app">
<head>
  <?php
  include("variables_globales_gestproject.php");
  include("funciones.php");
  ?>
  <meta charset="utf-8" />
  <title>HIDROSOL CANARIAS</title>
  <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="css/animate.css" type="text/css" />
  <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="css/font.css" type="text/css" />
    <link rel="stylesheet" href="css/app.css" type="text/css" />
  <!--[if lt IE 9]>
    <script src="js/ie/html5shiv.js"></script>
    <script src="js/ie/respond.min.js"></script>
    <script src="js/ie/excanvas.js"></script>
  <![endif]-->
  
  <!-- Magnific Popup core CSS file -->
  <link rel="stylesheet" href="styles/magnific-popup.css"> 
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" />



  <?php
  include("js/funciones.php");
  include("js/calendar_files.php");
  ?>
</head>
<body>
