<?php
// paso 1
// en excel:
// - si el codigo de barras no es de 13 cifras (colB=ERROR o colC=ERROR) => borrar lo que venga en los datos (colJ o colN)
// - corregir las familias que no esten exactamente escritas, las que no se puedan arreglar => borrar la linea
// - eliminar los dobles espacios, puntos y comas y comillas simples

// paso 2
// copiar los articulos validos en otro excel, borrar las columnas de comprobaciones
// dejar: proveedor_id, familia_id, nombre, codigo barras fabricante, referencia fabricante, precio, codigo barras y referencia
// quitar familia en texto, esta entre precio y codigo barras unificado
// a�adir una columna con espacio al final para que el ultimo valor no tenga el retorno de carro
// guardar como csv
// editar el csv con el bloc de notas: reemplazar lo siguiente
//		;"	;
//		""	"
//		";	;


// paso 3
// comienza la importacion
include("abrir_db.php");
include("funciones_tac7.php");
include("funciones.php");

$tabla_articulos = "articulos_t";
$tabla_precios = "articulos_precios_t";

$nombre_fichero = "articulos.csv";
$direccion_art = $nombre_fichero;

$fichero_art = fopen($direccion_art, 'r');
if (!$fichero_art)
{
	echo "No se pudo abrir el archivo.<br>";
}
else
{
	//$array_art_ids = array();
	$cuenta_lineas = 0;
	//while (!feof($fichero_art))
	while ($cuenta_lineas<50)
	{
		$cuenta_lineas++;
		//bufer tiene una linea del archivo
		$bufer = fgets($fichero_art, 8096);
		//echo "($bufer)<hr>";
		if ($bufer != "")
		{
			$array_explode = explode (';', $bufer);
			// contenido array_explode
			// 0	proveedor_id
			// 1	familia_id
			// 2	nombre
			// 3	codigo_fabricante (articulos_precios_t)
			// 4	referencia (articulos_precios_t)
			// 5	precio
			// 6	codigo_fabricante (articulos_t)
			// 7	referencia (articulos_t)
			
			// borro caracteres indeseados
			foreach ($array_explode as $indi_explode => $valor_explode)
			{
				$array_explode[$indi_explode] = str_replace("\\","",str_replace("'","",$valor_explode));//str_replace('"',"",
			}
			$articulo_id = 0;
			// busco si existe algun articulo con el campo referencia (unificada) del valor pedido
			$array_result_arti = array();
			$array_result_arti = obtener_multiples_campos(array('id'),$tabla_articulos,""," $tabla_articulos.referencia='".$array_explode[7]."'","","","");
			
			if (count($array_result_arti) == 0)
			{
				// no hay ningun articulo que coincida => se crea
				$cod_random = "";
				while ($cod_random == "")
				{
					$cod_random = str_pad(rand(1, 9999999999), 12, "0", STR_PAD_LEFT);
					$codigo_ean = CalcularEAN13($cod_random);
					$existe = 0;
					$consultaselect = "select id from $tabla_articulos where (codigo='".$cod_random.$codigo_ean."' or codigo_fabricante='".$cod_random.$codigo_ean."');";
					//echo "$consultaselect<br>";
					$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
					while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
					{
						$existe = 1;
					}
					if ($existe == 1)
					{
						$cod_random = "";
					}
				}
				$codigo_EAN13 = $cod_random.CalcularEAN13($cod_random);
				$cons_insert_arti = "insert into $tabla_articulos set nombre='".utf8_encode($array_explode[2])."', familia_id='".$array_explode[1]."', codigo_fabricante='".$array_explode[6]."', referencia='".$array_explode[7]."', codigo='".$codigo_EAN13."';";
				echo "$cons_insert_arti<br>";
				//$res_insert_arti = mysql_query($cons_insert_arti) or die("La consulta fall&oacute;: $cons_insert_arti " . mysql_error());
				$articulo_id =  mysql_insert_id();
			}
			elseif (count($array_result_arti) == 1)
			{
				// existe articulo
				$articulo_id = $array_result_arti[0]['id'];
			}
			else
			{
				echo "<b>Ha habido un error, no puede haber mas de un articulo con la misma referencia.</b>";
				echo "<pre>".print_r($array_explode,true)."</pre>";
			}
			if ($articulo_id > 0)
			{
				// ya existe el articulo
				// comprobacion de si existe el precio para ese proveedor
				$arti_precio_id = 0;
				$array_result_arti_precio = array();
				$array_result_arti_precio = obtener_multiples_campos(array('id'),$tabla_precios,"","$tabla_precios.articulo_id='".$articulo_id."' and $tabla_precios.proveedor_id='".$array_explode[0]."'","","","");
				if (count($array_result_arti_precio) == 0)
				{
					// no hay ningun precio para ese articulo y proveedor => se crea
					$cons_insert_arti_precio = "insert into $tabla_precios set articulo_id='".$articulo_id."', precio='".round(str_replace(",",".",$array_explode[5]),2)."', proveedor_id='".$array_explode[0]."', codigo_fabricante='".$array_explode[3]."', referencia='".$array_explode[4]."';";
					echo "$cons_insert_arti_precio<br>";
					$res_insert_arti_precio = mysql_query($cons_insert_arti_precio) or die("La consulta fall&oacute;: $cons_insert_arti_precio " . mysql_error());
					$arti_precio_id =  mysql_insert_id();
				}
				elseif (count($array_result_arti_precio) == 1)
				{
					// existe articulo
					$arti_precio_id = $array_result_arti_precio[0]['id'];
					$cons_update_arti_precio = "update $tabla_precios set precio='".round(str_replace(",",".",$array_explode[5]),2)."', codigo_fabricante='".$array_explode[3]."', referencia='".$array_explode[4]."' where id='".$arti_precio_id."';";
					//echo "$cons_update_arti_precio<br>";
					//$res_update_arti_precio = mysql_query($cons_update_arti_precio) or die("La consulta fall&oacute;: $cons_update_arti_precio" . mysql_error());
				}
				else
				{
					echo "<b>Ha habido un error, no puede haber mas de un proveedor para un articulo.</b>";
					echo "<pre>".print_r($array_explode,true)."</pre>";
				}
			}
			//echo "<pre>".print_r($array_explode,true);echo print_r($array_result_arti,true)."</pre>";
		} // fin if bufer != ""
	} // fin del while feof
} // fin else de se abrio el archivo
fclose($fichero_art);
//unlink ("tacproject_joseayala/".$direccion_art);
echo "Termino";
?>