<?php
/*
Funciones en el script:

VerPresupuesto ($valor_presupuesto)
VerPedido ($valor_pedido)
VerAlbaran ($valor_albaran)
CalcularPrecioArtPresupuesto ($valor_articulo, $valor_cliente, $valor_proveedor, $valor_fecha, $unidades_pedidas)
CalcularPresupuesto ($presupuesto_id)
RecalcularPortesPresupuesto ($presupuesto_id)
CalcularPedido ($pedido_id)
RecalcularPortesPedido ($pedido_id)
CalcularAlbaran ($albaran_id)
BusquedaFechasVisitas ($valor_programacion, $numero_meses)
VerPlanningProv ($prov_planning_id)
CorreoOfertaCliente ($valor_oferta_cliente)
CorreoOfertaProv ($valor_oferta)
CorreoOferta ($valor_oferta)
DevolverProvCli ($valor_cliente, $tipo_prov)
DatosIncidencia ($incidencia_id)
DatosPlanningProv ($valor_dato)
*/

function VerPresupuesto ($valor_presupuesto)
{
global $fckeditor_camino_relativo, $ruta_mailing, $oferta_color_texto, $oferta_color_borde, $oferta_estilo_tabla, $oferta_estilo1, $oferta_estilo_texto, $oferta_estilo1_no_fondo, $oferta_estilo2, $oferta_estilo2_top, $presupuesto_estilo_est11, $presupuesto_estilo_est11b, $presupuesto_estilo_est12, $presupuesto_estilo_est12b, $presupuesto_estilo_est13, $presupuesto_estilo_est13b, $presupuesto_estilo_est14, $presupuesto_estilo_est14b, $presupuesto_estilo_est15, $presupuesto_estilo_est15b, $presupuesto_estilo_est16, $presupuesto_estilo_est16b, $presupuesto_estilo_separador, $presupuesto_estilo_logo, $ruta_imagenes;


$tabla_presupuestos = "presupuestos_t";
$tabla_pre_art = "pre_articulos_t";
$tabla_pre_portes = "pre_portes_t";
$tabla_clientes = "clientes_t";
$tabla_cli_direcciones = "clientes_direcciones_t";
$tabla_m_embalajes = "maestro_tipos_embalajes_t";
$tabla_m_igic = "maestro_igic_t";
$tabla_articulos = "articulos_t";
$tabla_proveedores = "proveedores_t";

$articulos_pagina = 15;
	
	$mensaje_presupuesto = "";
	$consulta_1 = "select * from $tabla_presupuestos where id='".$valor_presupuesto."';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1 La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{
		$consulta_2 = "select * from $tabla_clientes where id='".$linea_1['cliente_id']."';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2 La consulta fall&oacute;: " . mysql_error());
		$linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC);
		
		$datos_direccion_cliente = "";
		$consulta_dir = "select * from $tabla_cli_direcciones where id='".$linea_1['cliente_direccion_id']."';";
		//echo "$consulta_dir";
		$resultado_dir = mysql_query($consulta_dir) or die("$consulta_dir La consulta fall&oacute;: " . mysql_error());
		while ($linea_dir = mysql_fetch_array($resultado_dir, MYSQL_ASSOC))
		{
			$datos_direccion_cliente = $linea_dir['direccion'];
			if ($linea_dir['direccion'] != "") { $datos_direccion_cliente .= ", "; }
			$datos_direccion_cliente .= $linea_dir['cp'];
			if ($datos_direccion_cliente != "") { $datos_direccion_cliente .= "<br />"; }
			$datos_direccion_cliente .= $linea_dir['poblacion'];
			if ($linea_dir['poblacion'] != "") { $datos_direccion_cliente .= "<br />"; }
			$nombre_provincia = "";
			$cons1_tmp = "select * from maestro_provincias_t where id='".$linea_dir['provincia_id']."';";
			$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp " . mysql_error());
			while ($lin1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
			{
				$nombre_provincia = $lin1_tmp['nombre'];
			}
			$nombre_municipio = "";
			$cons1_tmp = "select * from maestro_municipios_t where id='".$linea_dir['municipio_id']."';";
			$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp " . mysql_error());
			while ($lin1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
			{
				$nombre_municipio = $lin1_tmp['nombre'];
			}
			$datos_direccion_cliente .= $nombre_provincia;
			if ($nombre_provincia != "") { $datos_direccion_cliente .= ", "; }
			$datos_direccion_cliente .= $nombre_municipio;
		}
		$contenido_logo = "<img src=\"images/logotipo_ayala.png\" /><br /><span class=\"est-logo\"></span>";
		$cabecera_pagina = "
<table width=\"99%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<!-- inicio cabecera -->
	<tr>
		<td style=\"padding-bottom:3px;\">
		<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
			<tr valign=\"top\">
				<td width=\"45%\" align=\"center\">".$contenido_logo."</td>
				<td width=\"55%\">
				<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
					<tr valign=\"bottom\">
						<td width=\"50%\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"left\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
							<tr align=\"center\">
								<td style=\"border-style:solid; border-width:1px; border-color:#000000;\"><span class=\"est15\">DOCUMENTO</span></td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;<span class=\"est16b\">PRESUPUESTO</span></td>
							</tr>
						</table>
						</td>
						<td width=\"50%\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"right\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>
								<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
									<tr align=\"center\">
										<td width=\"25%\"><span class=\"est15\">P&aacute;gina:</span></td>
										<td width=\"75%\"><span class=\"est16\">#PAGINA#</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					<tr valign='top'>
						<td style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"left\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>&nbsp;&nbsp;<span class=\"est15b\">N&uacute;mero:</span> <span class=\"est16\">".$linea_1['numero']."</span></td>
							</tr>
						</table>
						</td>
						<td style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"right\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>
								<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
									<tr align=\"center\">
										<td width=\"25%\"><span class=\"est15\">Fecha:</span></td>
										<td width=\"75%\"><span class=\"est16\">".date("d/m/Y",strtotime($linea_1['fecha']))."</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan=\"2\" style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\" align=\"center\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>
								<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">
									<tr>
										<td colspan=\"2\"><span class=\"est15b\">".$linea_2['nombre']."</span></td>
									</tr>
									<tr>
										<td colspan=\"2\"><span class=\"est15\">".$datos_direccion_cliente."&nbsp;</span></td>
									</tr>
									<tr>
										<td width=\"75%\"><span class=\"est15b\">C.I.F./N.I.F.:</span> <span class=\"est15\">".$linea_2['nif']."</span></td>
										<td width=\"25%\"><span class=\"est15b\">&nbsp;</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
<!-- fin cabecera -->
</table>";
		$pie_pagina = "
<table width=\"99%\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#ffffff;\">
<!-- inicio pie -->
	<tr>
		<td>
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
			<tr>
				<td style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
				<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" height=\"40\">
					<tr valign='top'>
						<td><span class=\"est15b\">OBSERV.:</span> ";
		if ($linea_1['observaciones_privado'] != "")
		{
			$pie_pagina .= "<span class=\"est15\">".$linea_1['observaciones_privado']."</span>";
		}
		$pie_pagina .= "</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>
				<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
					<tr>
						<td width=\"16%\" align=\"right\"><span class=\"est16b\">Importe Base:</span></td>
						<td width=\"17%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_sin_igic'],2,",",".")." &euro;</span></td>
						<td width=\"16%\" align=\"right\"><span class=\"est16b\">Importe I.G.I.C.:</span></td>
						<td width=\"17%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_igic'],2,",",".")." &euro;</span></td>
						<td width=\"17%\" align=\"right\"><span class=\"est16b\">Importe Total:</span></td>
						<td width=\"17%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_total'],2,",",".")." &euro;</span></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr valign=\"top\">
		<td style=\"padding-top:3px;\">
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" height=\"200\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
			<tr valign=\"top\">
				<td><span class=\"est15b\">PORTES:</span><br>";
		$cons_portes = "select * from $tabla_pre_portes where presupuesto_id='".$valor_presupuesto."' order by id;";
		$res_portes = mysql_query($cons_portes) or die("La consulta fall&oacute;: $cons_portes " . mysql_error());
		while ($lin_portes = mysql_fetch_array($res_portes, MYSQL_ASSOC))
		{
			$importe_total_portes += $lin_portes['importe_portes'];
			$nombre_proveedor = "";
			$cons1_tmp = "select * from proveedores_t where id='$lin_portes[proveedor_id]';";
			//echo "$cons1_tmp<br>";
			$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp" . mysql_error());
			while ($linea1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
			{
				$nombre_proveedor = $linea1_tmp['nombre'];
			}
			$estado_porte = "";
			if ($lin_portes['usar_cliente'] == "on") { $estado_porte = "debidos"; }
			else { $estado_porte = "pagados"; }
			$nombre_ag_transp = "";
			if ($lin_portes['usar_cliente'] == "on") { $cons1_tmp = "select * from maestro_agencias_transportes_t where id='$lin_portes[cli_ag_transporte_id]';"; }
			else { $cons1_tmp = "select * from maestro_agencias_transportes_t where id='$lin_portes[prov_ag_transporte_id]';"; }
			//echo "$cons1_tmp<br>";
			$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp" . mysql_error());
			while ($linea1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
			{
				$nombre_ag_transp = $linea1_tmp['nombre'];
			}
			$pie_pagina .= "<span class=\"est15\">Para proveedor ".$nombre_proveedor." portes ".$estado_porte.". Agencia: ".$nombre_ag_transp."</span><br />";
		}
		$pie_pagina .= "</td>
			</tr>
		</table>
		</td>
	</tr>
<!-- fin pie -->
</table>";
		
		
		// busqueda de los articulos del presupuesto
		$array_articulos_id = array();
		$consulta_3 = "select $tabla_pre_art.id from $tabla_pre_art where $tabla_pre_art.presupuesto_id='".$valor_presupuesto."' order by $tabla_pre_art.id;";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{
			$array_articulos_id[] = $linea_3['id'];
		}
		//echo print_r($array_articulos_id,true);
		// contabilizar articulos
		$num_arti = 0;
		$num_arti = count($array_articulos_id);
/*
		$consulta_3 = "select count($tabla_pre_art.id) as total from $tabla_pre_art where $tabla_pre_art.presupuesto_id='".$valor_presupuesto."';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{
			$num_arti = $linea_3['total'];
		}
*/
		$total_paginas = (int) ceil($num_arti/$articulos_pagina);
		//echo "($num_arti)($total_paginas)<br>";
		$cuenta_articulos = 0;
		for ($pag = 1; $pag <= $total_paginas; $pag++)
		{
			//$limit = " limit ".(($pag-1)*$articulos_pagina).",$articulos_pagina";
			//echo "($limit)";
			$inicio = ($pag-1)*$articulos_pagina;
			$fin = $inicio+$articulos_pagina-1;
			// hay que comprobar que el fin definido no sobrepase el total real de articulos
			if ($fin >= count($array_articulos_id)) { $fin = count($array_articulos_id)-1; }
//						<td width=\"17%\"><span class=\"est16\">Embalaje</span></td>
			$cuerpo_pagina = "
<table width=\"99%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<!-- inicio cuerpo -->
	<tr>
		<td>
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
			<tr>
				<td>
				<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">
					<tr align=\"right\">
						<td width=\"8%\"><span class=\"est16\">Referencia</span></td>
						<td width=\"22%\"><span class=\"est16\">Articulo</span></td>
						<td width=\"12%\"><span class=\"est16\">Prov.</span></td>
						<td width=\"8%\"><span class=\"est16\">Unid.</span></td>
						<td width=\"8%\"><span class=\"est16\">Precio</span></td>
						<td width=\"8%\"><span class=\"est16\">% Dto1</span></td>
						<td width=\"8%\"><span class=\"est16\">% Dto2</span></td>
						<td width=\"8%\"><span class=\"est16\">% Dto3</span></td>
						<td width=\"8%\"><span class=\"est16\">% Igic</span></td>
						<td width=\"10%\"><span class=\"est16\">Importe</span></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td style=\"padding-top:3px;\">
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" height=\"450\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
			<tr valign=\"top\">
				<td>
				<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
			for ($indice = $inicio; $indice <= $fin; $indice++)
			{
				$consulta_3 = "select $tabla_pre_art.* from $tabla_pre_art where $tabla_pre_art.id='".$array_articulos_id[$indice]."';";
				//echo "$consulta_3<br>";
				$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
				while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
				{
					$consulta_4 = "select * from $tabla_articulos where id='".$linea_3['articulo_id']."';";
					//echo "$consulta_4<br>";
					$resultado_4 = mysql_query($consulta_4) or die("$consulta_4 La consulta fall&oacute;: " . mysql_error());
					$linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC);
/*
					$nombre_embalaje = "";
					$consulta_5 = "select $tabla_m_embalajes.* from $tabla_m_embalajes where $tabla_m_embalajes.id='".$linea_3['tipo_embalaje_id']."';";
					//echo "$consulta_5<br>";
					$resultado_5 = mysql_query($consulta_5) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
					{
						$nombre_embalaje = "$linea_5[nombre] de $linea_3[unidades_embalaje]";
					}
//						<td width=\"17%\"><span class=\"est12\">".$nombre_embalaje."</span></td>
*/
					$nombre_proveedor = "";
					$consulta_5 = "select $tabla_proveedores.* from $tabla_proveedores where $tabla_proveedores.id='".$linea_3['proveedor_id']."';";
					//echo "$consulta_5<br>";
					$resultado_5 = mysql_query($consulta_5) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
					{
						$nombre_proveedor = $linea_5['nombre_corto'];
					}
					$valor_igic = "";
					$consulta_5 = "select * from $tabla_m_igic where $tabla_m_igic.id='".$linea_3['igic_id']."';";
					$resultado_5 = mysql_query($consulta_5) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
					{
						$valor_igic = $linea_5['valor'];
					}
					$cuerpo_pagina .= "
					<tr align=\"right\">
						<td width=\"8%\"><span class=\"est12\">".$linea_4['referencia']."</span></td>
						<td width=\"22%\"><span class=\"est12\">".$linea_4['nombre']."</span></td>
						<td width=\"12%\"><span class=\"est12\">$nombre_proveedor</span></td>
						<td width=\"8%\"><span class=\"est12\">".$linea_3['unidades']."</span></td>
						<td width=\"8%\"><span class=\"est12\">".number_format($linea_3['precio_unidad'],2,",",".")." &euro;</span></td>
						<td width=\"8%\"><span class=\"est12\">";
					if ($linea_3['dto_porc'] > 0)
					{
						$cuerpo_pagina .= number_format($linea_3['dto_porc'],2,",",".")." %";
					}
					$cuerpo_pagina .= "</span></td>
						<td width=\"8%\"><span class=\"est12\">";
					if ($linea_3['dto2'] > 0)
					{
						$cuerpo_pagina .= number_format($linea_3['dto2'],2,",",".")." %";
					}
					$cuerpo_pagina .= "</span></td>
						<td width=\"8%\"><span class=\"est12\">";
					if ($linea_3['dto3'] > 0)
					{
						$cuerpo_pagina .= number_format($linea_3['dto3'],2,",",".")." %";
					}
					$cuerpo_pagina .= "</span></td>
						<td width=\"8%\"><span class=\"est12\">".number_format($valor_igic,2,",",".")."</span></td>
						<td width=\"10%\"><span class=\"est12\">";
					if ($linea_3['reservado'] == "")
					{
						$cuerpo_pagina .= number_format($linea_3['importe_articulo'],2,",",".")." &euro;";
					}
					$cuerpo_pagina .= "</span></td>
					</tr>";
					$cuenta_articulos++;
				} // fin while linea_6
			} // fin del for
			if ($pag < $total_paginas)
			{
				$cuerpo_pagina .= "
					<tr>
						<td colspan=\"10\" align=\"left\"><span class=\"est12\">Suma y sigue</span></td>
					</tr>
";
			}
			$cuerpo_pagina .= "
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
<!-- fin cuerpo -->
</table>";
			//echo str_replace("#PAGINA#", $pag, $cabecera_pagina).$cuerpo_pagina.$pie_pagina;
			$mensaje_presupuesto .= "<table width=\"100%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
	<tr>
		<td>".str_replace("#PAGINA#", $pag, $cabecera_pagina)."</td>
	</tr>
	<tr>
		<td>".$cuerpo_pagina."</td>
	</tr>
	<tr>
		<td style=\"padding-top:3px;\">".$pie_pagina."</td>
	</tr>
</table>";
/*
			if ($_REQUEST['logo'] == 1)
			{
				echo "
<span class=\"est11\">Seg&uacute;n el art 5 de la Ley 15/1999, los datos de car&aacute;cter personal de este documento, est&aacute;n incorporados en un fichero automatizado utilizado para la gesti&oacute;n administrativa de la empresa Cuymon 2000 S.L., por lo que tiene derecho a acceder a sus datos personales, rectificarlos o en su caso cancelarlos, solicit&aacute;ndolo en el domicilio se&ntilde;alado en el mismo.</span>";
			}
*/
			if ($pag < $total_paginas)
			{
				$mensaje_presupuesto .= "
<div class=\"separador\">&nbsp;</div>";
			}
		} // fin for paginado
		$fin_pagina = "
</body>
</html>
";
		//$mensaje_presupuesto .= $fin_pagina;
	} //fin while linea_2
	
	//$mensaje_presupuesto = str_replace("class='tabla'", $oferta_estilo_tabla, str_replace("class='texto'", $oferta_estilo_texto, str_replace("class='tipo2'", $oferta_estilo2, str_replace("class='tipo2_top'", $oferta_estilo2_top, str_replace("class='tipo1'", $oferta_estilo1, str_replace("class='tipo1_no_fondo'", $oferta_estilo1_no_fondo, $mensaje_presupuesto))))));
	$mensaje_presupuesto = str_replace("class=\"est11\"", $presupuesto_estilo_est11, str_replace("class=\"est11b\"", $presupuesto_estilo_est11b, str_replace("class=\"est12\"", $presupuesto_estilo_est12, str_replace("class=\"est12b\"", $presupuesto_estilo_est12b, str_replace("class=\"est13\"", $presupuesto_estilo_est13, str_replace("class=\"est13b\"", $presupuesto_estilo_est13b, str_replace("class=\"est14\"", $presupuesto_estilo_est14, str_replace("class=\"est14b\"", $presupuesto_estilo_est14b, str_replace("class=\"est15\"", $presupuesto_estilo_est15, str_replace("class=\"est15b\"",$presupuesto_estilo_est15b, str_replace("class=\"est16\"", $presupuesto_estilo_est16, str_replace("class=\"est16b\"", $presupuesto_estilo_est16b, str_replace("class=\"separador\"", $presupuesto_estilo_separador, str_replace("class=\"est-logo\"", $presupuesto_estilo_logo, str_replace("images/", $ruta_imagenes, $mensaje_presupuesto)))))))))))))));
	
	return $mensaje_presupuesto;
}

function VerPedido ($valor_pedido)
{
global $fckeditor_camino_relativo, $ruta_mailing, $oferta_color_texto, $oferta_color_borde, $oferta_estilo_tabla, $oferta_estilo1, $oferta_estilo_texto, $oferta_estilo1_no_fondo, $oferta_estilo2, $oferta_estilo2_top, $presupuesto_estilo_est11, $presupuesto_estilo_est11b, $presupuesto_estilo_est12, $presupuesto_estilo_est12b, $presupuesto_estilo_est13, $presupuesto_estilo_est13b, $presupuesto_estilo_est14, $presupuesto_estilo_est14b, $presupuesto_estilo_est15, $presupuesto_estilo_est15b, $presupuesto_estilo_est16, $presupuesto_estilo_est16b, $presupuesto_estilo_separador, $presupuesto_estilo_logo, $ruta_imagenes;

$tabla_pedidos = "pedidos_t";
$tabla_ped_art = "ped_articulos_t";
$tabla_articulos = "articulos_t";
$tabla_m_embalajes = "maestro_tipos_embalajes_t";
$tabla_m_igic = "maestro_igic_t";
$tabla_presupuestos = "presupuestos_t";
$tabla_clientes = "clientes_t";
$tabla_cli_direcciones = "clientes_direcciones_t";
$tabla_proveedores = "proveedores_t";

$articulos_pagina = 15;
	
	$mensaje_pedido = "";
	$consulta_1 = "select * from $tabla_pedidos where id='".$valor_pedido."';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1 La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{
		$nombre_cliente = "";
		$nif_cliente = "";
		$datos_direccion_cliente = "";
		$consulta_presu = "select * from $tabla_presupuestos where id='".$linea_1['presupuesto_id']."';";
		//echo "$consulta_presu";
		$resultado_presu = mysql_query($consulta_presu) or die("$consulta_presu La consulta fall&oacute;: " . mysql_error());
		while ($linea_presu = mysql_fetch_array($resultado_presu, MYSQL_ASSOC))
		{
			$consulta_2 = "select * from $tabla_clientes where id='".$linea_presu['cliente_id']."';";
			//echo "$consulta_2";
			$resultado_2 = mysql_query($consulta_2) or die("$consulta_2 La consulta fall&oacute;: " . mysql_error());
			$linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC);
			
			$nombre_cliente = $linea_2['nombre'];
			$nif_cliente = $linea_2['nif'];
			
			$consulta_dir = "select * from $tabla_cli_direcciones where id='".$linea_presu['cliente_direccion_id']."';";
			//echo "$consulta_dir";
			$resultado_dir = mysql_query($consulta_dir) or die("$consulta_dir La consulta fall&oacute;: " . mysql_error());
			while ($linea_dir = mysql_fetch_array($resultado_dir, MYSQL_ASSOC))
			{
				$datos_direccion_cliente = $linea_dir['direccion'];
				if ($linea_dir['direccion'] != "") { $datos_direccion_cliente .= ", "; }
				$datos_direccion_cliente .= $linea_dir['cp'];
				if ($datos_direccion_cliente != "") { $datos_direccion_cliente .= "<br>"; }
				$datos_direccion_cliente .= $linea_dir['poblacion'];
				if ($linea_dir['poblacion'] != "") { $datos_direccion_cliente .= "<br>"; }
				$nombre_provincia = "";
				$cons1_tmp = "select * from maestro_provincias_t where id='".$linea_dir['provincia_id']."';";
				$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp " . mysql_error());
				while ($lin1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
				{
					$nombre_provincia = $lin1_tmp['nombre'];
				}
				$nombre_municipio = "";
				$cons1_tmp = "select * from maestro_municipios_t where id='".$linea_dir['municipio_id']."';";
				$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp " . mysql_error());
				while ($lin1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
				{
					$nombre_municipio = $lin1_tmp['nombre'];
				}
				$datos_direccion_cliente .= $nombre_provincia;
				if ($nombre_provincia != "") { $datos_direccion_cliente .= ", "; }
				$datos_direccion_cliente .= $nombre_municipio;
			}
		}
		$nombre_proveedor = "";
		$consulta_prov = "select * from $tabla_proveedores where id='".$linea_1['proveedor_id']."';";
		//echo "$consulta_prov";
		$resultado_prov = mysql_query($consulta_prov) or die("$consulta_prov La consulta fall&oacute;: " . mysql_error());
		while ($linea_prov = mysql_fetch_array($resultado_prov, MYSQL_ASSOC))
		{
			$nombre_proveedor = $linea_prov['nombre'];
		}
		$contenido_logo = "<img src=\"images/logotipo_ayala.png\" /><br /><span class=\"est-logo\">S.T.P., S.A.<br>N.I.F. A58822354</span>";
		$cabecera_pagina = "
<table width=\"99%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<!-- inicio cabecera -->
	<tr>
		<td style=\"padding-bottom:3px;\">
		<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
			<tr valign=\"top\">
				<td width=\"45%\" align=\"center\">".$contenido_logo."</td>
				<td width=\"55%\">
				<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
					<tr valign=\"bottom\">
						<td width=\"50%\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"left\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
							<tr align=\"center\">
								<td style=\"border-style:solid; border-width:1px; border-color:#000000;\"><span class=\"est15\">DOCUMENTO</span></td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;<span class=\"est16b\">PEDIDO</span></td>
							</tr>
						</table>
						</td>
						<td width=\"50%\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"right\">
							<tr>
								<td style=\"border-style:solid; border-width:1px; border-color:#000000;\">
								<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
									<tr align=\"center\">
										<td width=\"25%\"><span class=\"est15\">P&aacute;gina:</span></td>
										<td width=\"75%\"><span class=\"est16\">#PAGINA#</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					<tr valign='top'>
						<td style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"left\">
							<tr>
								<td style=\"border-style:solid; border-width:1px; border-color:#000000;\">&nbsp;&nbsp;<span class=\"est15b\">N&uacute;mero:</span> <span class=\"est16\">".$linea_1['numero']."</span></td>
							</tr>
						</table>
						</td>
						<td style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"right\" style=\"border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>
								<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
									<tr align=\"center\">
										<td width=\"25%\"><span class=\"est15\">Fecha:</span></td>
										<td width=\"75%\"><span class=\"est16\">".date("d/m/Y",strtotime($linea_1['fecha']))."</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan=\"2\" style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\" align=\"center\" style=\"border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>
								<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">
									<tr>
										<td colspan=\"2\"><span class=\"est15b\">".$nombre_cliente."</span></td>
									</tr>
									<tr>
										<td colspan=\"2\"><span class=\"est15\">".$datos_direccion_cliente."&nbsp;</span></td>
									</tr>
									<tr>
										<td width=\"75%\"><span class=\"est15b\">C.I.F./N.I.F.:</span> <span class=\"est15\">".$nif_cliente."</span></td>
										<td width=\"25%\"><span class=\"est15b\">&nbsp;</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan=\"2\" style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\" align=\"center\" style=\"border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td><span class=\"est15b\">PROVEEDOR:</span> <span class=\"est15\">".$nombre_proveedor."</span></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
<!-- fin cabecera -->
</table>";

/*
		$pie_pagina = "
<table width=\"99%\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
<!-- inicio pie -->
	<tr>
		<td style=\"border-style:solid; border-width:1px; border-color:#000000;\">
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
			<tr>
				<td width=\"12%\" align=\"right\"><span class=\"est16b\">Importe Base:</span></td>
				<td width=\"13%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_sin_igic'],2,",",".")." &euro;</span></td>
				<td width=\"12%\" align=\"right\"><span class=\"est16b\">Importe I.G.I.C.:</span></td>
				<td width=\"13%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_igic'],2,",",".")." &euro;</span></td>
				<td width=\"12%\" align=\"right\"><span class=\"est16b\">Importe Total:</span></td>
				<td width=\"13%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_portes'],2,",",".")." &euro;</span></td>
				<td width=\"12%\" align=\"right\"><span class=\"est16b\">Importe Total:</span></td>
				<td width=\"13%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_total'],2,",",".")." &euro;</span></td>
			</tr>
		</table>
		</td>
	</tr>
<!-- fin pie -->
</table>";
*/
		
		// contabilizar articulos
		$num_arti = 0;
		$consulta_3 = "select count($tabla_ped_art.id) as total from $tabla_ped_art where $tabla_ped_art.pedido_id='".$valor_pedido."';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{
			$num_arti = $linea_3['total'];
		}
		$total_paginas = (int) ceil($num_arti/$articulos_pagina);
		//echo "($num_arti)($total_paginas)<br>";
		
		// busqueda de los articulos del presupuesto
		$array_articulos_id = array();
		$consulta_3 = "select $tabla_ped_art.id from $tabla_ped_art where $tabla_ped_art.pedido_id='".$valor_pedido."' order by $tabla_ped_art.id;";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{
			$array_articulos_id[] = $linea_3['id'];
		}
		//echo print_r($array_articulos_id,true);
		$cuenta_articulos = 0;
		for ($pag = 1; $pag <= $total_paginas; $pag++)
		{
			//$limit = " limit ".(($pag-1)*$articulos_pagina).",$articulos_pagina";
			//echo "($limit)";
			$inicio = ($pag-1)*$articulos_pagina;
			$fin = $inicio+$articulos_pagina-1;
			// hay que comprobar que el fin definido no sobrepase el total real de articulos
			if ($fin >= count($array_articulos_id)) { $fin = count($array_articulos_id)-1; }
			$cuerpo_pagina = "
<table width=\"99%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<!-- inicio cuerpo -->
	<tr>
		<td>
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-style:solid; border-width:1px; border-color:#000000;\">
			<tr>
				<td>
				<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">
					<tr align=\"right\">
						<td width=\"9%\"><span class=\"est16\">Codigo</span></td>
						<td width=\"18%\"><span class=\"est16\">Articulo</span></td>
						<td width=\"18%\"><span class=\"est16\">Embalaje</span></td>
						<td width=\"9%\"><span class=\"est16\">Cant.</span></td>
						<td width=\"9%\"><span class=\"est16\">Unid.</span></td>
						<td width=\"9%\"><span class=\"est16\">Precio</span></td>
						<td width=\"9%\"><span class=\"est16\">% Dto1</span></td>
						<td width=\"9%\"><span class=\"est16\">% Dto2</span></td>
						<td width=\"10%\"><span class=\"est16\">% Igic</span></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td style=\"padding-top:3px;\">
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" height=\"550\" style=\"border-style:solid; border-width:1px; border-color:#000000;\">
			<tr valign=\"top\">
				<td>
				<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
			for ($indice = $inicio; $indice <= $fin; $indice++)
			{
				$consulta_3 = "select $tabla_ped_art.* from $tabla_ped_art where $tabla_ped_art.id='".$array_articulos_id[$indice]."';";
				//echo "$consulta_3<br>";
				$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
				while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
				{
					$consulta_4 = "select * from $tabla_articulos where id='".$linea_3['articulo_id']."';";
					//echo "$consulta_4<br>";
					$resultado_4 = mysql_query($consulta_4) or die("$consulta_4 La consulta fall&oacute;: " . mysql_error());
					$linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC);
					$nombre_embalaje = "";
					$consulta_5 = "select $tabla_m_embalajes.* from $tabla_m_embalajes where $tabla_m_embalajes.id='".$linea_3['tipo_embalaje_id']."';";
					//echo "$consulta_5<br>";
					$resultado_5 = mysql_query($consulta_5) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
					{
						$nombre_embalaje = "$linea_5[nombre] de $linea_3[unidades_embalaje]";
					}
					$valor_igic = "";
					$consulta_5 = "select * from $tabla_m_igic where $tabla_m_igic.id='".$linea_3['igic_id']."';";
					$resultado_5 = mysql_query($consulta_5) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
					{
						$valor_igic = $linea_5['valor'];
					}
					$cuerpo_pagina .= "
					<tr align=\"right\">
						<td width=\"9%\"><span class=\"est12\">".$linea_4['codigo']."</span></td>
						<td width=\"18%\"><span class=\"est12\">".$linea_4['nombre']."</span></td>
						<td width=\"18%\"><span class=\"est12\">".$nombre_embalaje."</span></td>
						<td width=\"9%\"><span class=\"est12\">".$linea_3['num_embalajes']."</span></td>
						<td width=\"9%\"><span class=\"est12\">".$linea_3['unidades']."</span></td>
						<td width=\"9%\"><span class=\"est12\">".number_format($linea_3['precio_unidad'],2,",",".")." &euro;</span></td>
						<td width=\"9%\"><span class=\"est12\">";
					if ($linea_3['dto_porc'] > 0)
					{
						$cuerpo_pagina .= number_format($linea_3['dto_porc'],2,",",".")." %";
					}
					$cuerpo_pagina .= "</span></td>
						<td width=\"9%\"><span class=\"est12\">";
					if ($linea_3['dto2'] > 0)
					{
						$cuerpo_pagina .= number_format($linea_3['dto2'],2,",",".")." %";
					}
					$cuerpo_pagina .= "</span></td>
						<td width=\"10%\"><span class=\"est12\">".number_format($valor_igic,2,",",".")."</span></td>
					</tr>";
					$cuenta_articulos++;
				} // fin while linea_6
			} // fin del for
			if ($pag < $total_paginas)
			{
				$cuerpo_pagina .= "
					<tr>
						<td colspan=\"10\" align=\"left\"><span class=\"est12\">Suma y sigue</span></td>
					</tr>
";
			}
			$cuerpo_pagina .= "
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
<!-- fin cuerpo -->
</table>";
			//echo str_replace("#PAGINA#", $pag, $cabecera_pagina).$cuerpo_pagina.$pie_pagina;
			$mensaje_pedido .= "<table width=\"100%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
	<tr>
		<td>".str_replace("#PAGINA#", $pag, $cabecera_pagina)."</td>
	</tr>
	<tr>
		<td>".$cuerpo_pagina."</td>
	</tr>";
/*
			$mensaje_pedido .= "
	<tr>
		<td style=\"padding-top:3px;\">".$pie_pagina."</td>
	</tr>";
*/
			$mensaje_pedido .= "
</table>";
/*
			if ($_REQUEST['logo'] == 1)
			{
				echo "
<span class=\"est11\">Seg&uacute;n el art 5 de la Ley 15/1999, los datos de car&aacute;cter personal de este documento, est&aacute;n incorporados en un fichero automatizado utilizado para la gesti&oacute;n administrativa de la empresa Cuymon 2000 S.L., por lo que tiene derecho a acceder a sus datos personales, rectificarlos o en su caso cancelarlos, solicit&aacute;ndolo en el domicilio se&ntilde;alado en el mismo.</span>";
			}
*/
			if ($pag < $total_paginas)
			{
				$mensaje_pedido .= "
<div class=\"separador\">&nbsp;</div>";
			}
		} // fin for paginado
		$fin_pagina = "
</body>
</html>
";
		//$mensaje_presupuesto .= $fin_pagina;
	} //fin while linea_2
	
	//$mensaje_pedido = str_replace("class='tabla'", $oferta_estilo_tabla, str_replace("class='texto'", $oferta_estilo_texto, str_replace("class='tipo2'", $oferta_estilo2, str_replace("class='tipo2_top'", $oferta_estilo2_top, str_replace("class='tipo1'", $oferta_estilo1, str_replace("class='tipo1_no_fondo'", $oferta_estilo1_no_fondo, $mensaje_pedido))))));
	$mensaje_pedido = str_replace("class=\"est11\"", $presupuesto_estilo_est11, str_replace("class=\"est11b\"", $presupuesto_estilo_est11b, str_replace("class=\"est12\"", $presupuesto_estilo_est12, str_replace("class=\"est12b\"", $presupuesto_estilo_est12b, str_replace("class=\"est13\"", $presupuesto_estilo_est13, str_replace("class=\"est13b\"", $presupuesto_estilo_est13b, str_replace("class=\"est14\"", $presupuesto_estilo_est14, str_replace("class=\"est14b\"", $presupuesto_estilo_est14b, str_replace("class=\"est15\"", $presupuesto_estilo_est15, str_replace("class=\"est15b\"",$presupuesto_estilo_est15b, str_replace("class=\"est16\"", $presupuesto_estilo_est16, str_replace("class=\"est16b\"", $presupuesto_estilo_est16b, str_replace("class=\"separador\"", $presupuesto_estilo_separador, str_replace("class=\"est-logo\"", $presupuesto_estilo_logo, str_replace("images/", $ruta_imagenes, $mensaje_pedido)))))))))))))));
	
	return $mensaje_pedido;
}

function VerAlbaran ($valor_albaran)
{
global $fckeditor_camino_relativo, $ruta_mailing, $oferta_color_texto, $oferta_color_borde, $oferta_estilo_tabla, $oferta_estilo1, $oferta_estilo_texto, $oferta_estilo1_no_fondo, $oferta_estilo2, $oferta_estilo2_top, $presupuesto_estilo_est11, $presupuesto_estilo_est11b, $presupuesto_estilo_est12, $presupuesto_estilo_est12b, $presupuesto_estilo_est13, $presupuesto_estilo_est13b, $presupuesto_estilo_est14, $presupuesto_estilo_est14b, $presupuesto_estilo_est15, $presupuesto_estilo_est15b, $presupuesto_estilo_est16, $presupuesto_estilo_est16b, $presupuesto_estilo_separador, $presupuesto_estilo_logo, $ruta_imagenes;


$tabla_albaranes = "albaranes_t";
$tabla_presupuestos = "presupuestos_t";
$tabla_clientes = "clientes_t";
$tabla_cli_direcciones = "clientes_direcciones_t";
$tabla_agencias_transp = "maestro_agencias_transportes_t";
$tabla_alb_avisos = "alb_avisos_t";
$tabla_alb_art = "alb_articulos_t";
$tabla_m_embalajes = "maestro_tipos_embalajes_t";
$tabla_m_igic = "maestro_igic_t";
$tabla_articulos = "articulos_t";

$articulos_pagina = 15;
	
	$mensaje_albaran = "";
	$consulta_1 = "select * from $tabla_albaranes where id='".$valor_albaran."';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1 La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{
		$consulta_presu = "select * from $tabla_presupuestos where id='".$linea_1['presupuesto_id']."';";
		//echo "$consulta_presu";
		$resultado_presu = mysql_query($consulta_presu) or die("$consulta_presu La consulta fall&oacute;: " . mysql_error());
		while ($linea_presu = mysql_fetch_array($resultado_presu, MYSQL_ASSOC))
		{
			$consulta_2 = "select * from $tabla_clientes where id='".$linea_presu['cliente_id']."';";
			//echo "$consulta_2";
			$resultado_2 = mysql_query($consulta_2) or die("$consulta_2 La consulta fall&oacute;: " . mysql_error());
			$linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC);
			
			$datos_direccion_cliente = "";
			$consulta_dir = "select * from $tabla_cli_direcciones where id='".$linea_1['cliente_direccion_id']."';";
			//echo "$consulta_dir";
			$resultado_dir = mysql_query($consulta_dir) or die("$consulta_dir La consulta fall&oacute;: " . mysql_error());
			while ($linea_dir = mysql_fetch_array($resultado_dir, MYSQL_ASSOC))
			{
				$datos_direccion_cliente = $linea_dir['direccion'];
				if ($linea_dir['direccion'] != "") { $datos_direccion_cliente .= ", "; }
				$datos_direccion_cliente .= $linea_dir['cp'];
				if ($datos_direccion_cliente != "") { $datos_direccion_cliente .= "<br>"; }
				$datos_direccion_cliente .= $linea_dir['poblacion'];
				if ($linea_dir['poblacion'] != "") { $datos_direccion_cliente .= "<br>"; }
				$nombre_provincia = "";
				$cons1_tmp = "select * from maestro_provincias_t where id='".$linea_dir['provincia_id']."';";
				$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp " . mysql_error());
				while ($lin1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
				{
					$nombre_provincia = $lin1_tmp['nombre'];
				}
				$nombre_municipio = "";
				$cons1_tmp = "select * from maestro_municipios_t where id='".$linea_dir['municipio_id']."';";
				$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp " . mysql_error());
				while ($lin1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
				{
					$nombre_municipio = $lin1_tmp['nombre'];
				}
				$datos_direccion_cliente .= $nombre_provincia;
				if ($nombre_provincia != "") { $datos_direccion_cliente .= ", "; }
				$datos_direccion_cliente .= $nombre_municipio;
			}
			$contenido_logo = "<img src=\"images/logotipo_ayala.png\" /><br /><span class=\"est-logo\">S.T.P., S.A.<br>N.I.F. A58822354</span>";
			$cabecera_pagina = "
<table width=\"99%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<!-- inicio cabecera -->
	<tr>
		<td style=\"padding-bottom:3px;\">
		<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
			<tr valign=\"top\">
				<td width=\"45%\" align=\"center\">".$contenido_logo."</td>
				<td width=\"55%\">
				<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
					<tr valign=\"bottom\">
						<td width=\"50%\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"left\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
							<tr align=\"center\">
								<td style=\"border-style:solid; border-width:1px; border-color:#000000;\"><span class=\"est15\">DOCUMENTO</span></td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;<span class=\"est16b\">ALBAR&Aacute;N</span></td>
							</tr>
						</table>
						</td>
						<td width=\"50%\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"right\">
							<tr>
								<td style=\"border-style:solid; border-width:1px; border-color:#000000;\">
								<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
									<tr align=\"center\">
										<td width=\"25%\"><span class=\"est15\">P&aacute;gina:</span></td>
										<td width=\"75%\"><span class=\"est16\">#PAGINA#</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					<tr valign='top'>
						<td style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"left\">
							<tr>
								<td style=\"border-style:solid; border-width:1px; border-color:#000000;\">&nbsp;&nbsp;<span class=\"est15b\">N&uacute;mero:</span> <span class=\"est16\">".$linea_1['numero']."</span></td>
							</tr>
						</table>
						</td>
						<td style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"right\" style=\"border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>
								<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
									<tr align=\"center\">
										<td width=\"25%\"><span class=\"est15\">Fecha:</span></td>
										<td width=\"75%\"><span class=\"est16\">".date("d/m/Y",strtotime($linea_1['fecha']))."</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan=\"2\" style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\" align=\"center\" style=\"border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>
								<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">
									<tr>
										<td colspan=\"2\"><span class=\"est15b\">".$linea_2['nombre']."</span></td>
									</tr>
									<tr>
										<td colspan=\"2\"><span class=\"est15\">".$datos_direccion_cliente."&nbsp;</span></td>
									</tr>
									<tr>
										<td width=\"75%\"><span class=\"est15b\">C.I.F./N.I.F.:</span> <span class=\"est15\">".$linea_2['nif']."</span></td>
										<td width=\"25%\"><span class=\"est15b\">&nbsp;</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
<!-- fin cabecera -->
</table>";
			$pie_pagina = "
<table width=\"99%\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
<!-- inicio pie -->
	<tr>
		<td style=\"border-style:solid; border-width:1px; border-color:#000000;\">
		<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" height=\"40\">
			<tr valign='top'>
				<td><span class=\"est15b\">AGENCIA TRANSPORTE:</span> <span class=\"est15\">";
			$consulta_ag_transpor = "select * from $tabla_agencias_transp where id='".$linea_1['agencia_transporte_id']."';";
			//echo "$consulta_ag_transpor";
			$resultado_ag_transpor = mysql_query($consulta_ag_transpor) or die("$consulta_ag_transpor La consulta fall&oacute;: " . mysql_error());
			while ($linea_ag_transpor = mysql_fetch_array($resultado_ag_transpor, MYSQL_ASSOC))
			{
				$pie_pagina .= $linea_ag_transpor['nombre'];
				if ($linea_ag_transpor['telefono_contacto1'] != "")
				{
					$pie_pagina .= ", ".$linea_ag_transpor['telefono_contacto1'];
				}
			}
			$consulta_avisos = "select * from $tabla_alb_avisos where albaran_id='".$valor_albaran."' and agencia_transporte_id='".$linea_1['agencia_transporte_id']."' order by fecha desc, hora desc limit 1;";
			//echo "$consulta_avisos";
			$resultado_avisos = mysql_query($consulta_avisos) or die("$consulta_avisos La consulta fall&oacute;: " . mysql_error());
			while ($linea_avisos = mysql_fetch_array($resultado_avisos, MYSQL_ASSOC))
			{
				$pie_pagina .= " (Ultimo aviso: ".date("d-m-Y H:i",strtotime($linea_avisos['fecha']." ".$linea_avisos['hora'])).")";
			}
			$pie_pagina .= "</span></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
			<tr>
				<td><span class=\"est15b\">OBSERV. PRESUPUESTO:</span> <span class=\"est15\">".$linea_presu['observaciones_publico']."</span></td>
			</tr>
		</table>
		</td>
	</tr>";
/*
			$pie_pagina .= "
	<tr>
		<td style=\"border-style:solid; border-width:1px; border-color:#000000;\">
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
			<tr>
				<td width=\"12%\" align=\"right\"><span class=\"est16b\">Importe Base:</span></td>
				<td width=\"13%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_sin_igic'],2,",",".")." &euro;</span></td>
				<td width=\"12%\" align=\"right\"><span class=\"est16b\">Importe I.G.I.C.:</span></td>
				<td width=\"13%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_igic'],2,",",".")." &euro;</span></td>
				<td width=\"12%\" align=\"right\"><span class=\"est16b\">Importe Total:</span></td>
				<td width=\"13%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_portes'],2,",",".")." &euro;</span></td>
				<td width=\"12%\" align=\"right\"><span class=\"est16b\">Importe Total:</span></td>
				<td width=\"13%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_total'],2,",",".")." &euro;</span></td>
			</tr>
		</table>
		</td>
	</tr>";
*/
			$pie_pagina .= "
<!-- fin pie -->
</table>";
			
			// contabilizar articulos
			$num_arti = 0;
			$consulta_3 = "select count($tabla_alb_art.id) as total from $tabla_alb_art where $tabla_alb_art.albaran_id='".$valor_albaran."';";
			//echo "$consulta_3";
			$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
			while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
			{
				$num_arti = $linea_3['total'];
			}
			$total_paginas = (int) ceil($num_arti/$articulos_pagina);
			//echo "($num_arti)($total_paginas)<br>";
			
			// busqueda de los articulos del presupuesto
			$array_articulos_id = array();
			$consulta_3 = "select $tabla_alb_art.id from $tabla_alb_art where $tabla_alb_art.albaran_id='".$valor_albaran."' order by $tabla_alb_art.id;";
			//echo "$consulta_3";
			$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
			while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
			{
				$array_articulos_id[] = $linea_3['id'];
			}
			//echo print_r($array_articulos_id,true);
			$cuenta_articulos = 0;
			for ($pag = 1; $pag <= $total_paginas; $pag++)
			{
				//$limit = " limit ".(($pag-1)*$articulos_pagina).",$articulos_pagina";
				//echo "($limit)";
				$inicio = ($pag-1)*$articulos_pagina;
				$fin = $inicio+$articulos_pagina-1;
				// hay que comprobar que el fin definido no sobrepase el total real de articulos
				if ($fin >= count($array_articulos_id)) { $fin = count($array_articulos_id)-1; }
				$cuerpo_pagina = "
<table width=\"99%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<!-- inicio cuerpo -->
	<tr>
		<td>
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-style:solid; border-width:1px; border-color:#000000;\">
			<tr>
				<td>
				<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">
					<tr align=\"right\">
						<td width=\"9%\"><span class=\"est16\">Codigo</span></td>
						<td width=\"18%\"><span class=\"est16\">Articulo</span></td>
						<td width=\"18%\"><span class=\"est16\">Embalaje</span></td>
						<td width=\"9%\"><span class=\"est16\">Cant.</span></td>
						<td width=\"9%\"><span class=\"est16\">Unid.</span></td>
						<td width=\"9%\"><span class=\"est16\">Precio</span></td>
						<td width=\"9%\"><span class=\"est16\">% Dto1</span></td>
						<td width=\"9%\"><span class=\"est16\">% Dto2</span></td>
						<td width=\"10%\"><span class=\"est16\">% Igic</span></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td style=\"padding-top:3px;\">
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" height=\"550\" style=\"border-style:solid; border-width:1px; border-color:#000000;\">
			<tr valign=\"top\">
				<td>
				<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
				for ($indice = $inicio; $indice <= $fin; $indice++)
				{
					$consulta_3 = "select $tabla_alb_art.* from $tabla_alb_art where $tabla_alb_art.id='".$array_articulos_id[$indice]."';";
					//echo "$consulta_3<br>";
					$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
					while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
					{
						$consulta_4 = "select * from $tabla_articulos where id='".$linea_3['articulo_id']."';";
						//echo "$consulta_4<br>";
						$resultado_4 = mysql_query($consulta_4) or die("$consulta_4 La consulta fall&oacute;: " . mysql_error());
						$linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC);
						$nombre_embalaje = "";
						$consulta_5 = "select $tabla_m_embalajes.* from $tabla_m_embalajes where $tabla_m_embalajes.id='".$linea_3['tipo_embalaje_id']."';";
						//echo "$consulta_5<br>";
						$resultado_5 = mysql_query($consulta_5) or die("La consulta fall&oacute;: " . mysql_error());
						while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
						{
							$nombre_embalaje = "$linea_5[nombre] de $linea_3[unidades_embalaje]";
						}
						$valor_igic = "";
						$consulta_5 = "select * from $tabla_m_igic where $tabla_m_igic.id='".$linea_3['igic_id']."';";
						$resultado_5 = mysql_query($consulta_5) or die("La consulta fall&oacute;: " . mysql_error());
						while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
						{
							$valor_igic = $linea_5['valor'];
						}
						$cuerpo_pagina .= "
					<tr align=\"right\">
						<td width=\"9%\"><span class=\"est12\">".$linea_4['codigo']."</span></td>
						<td width=\"18%\"><span class=\"est12\">".$linea_4['nombre']."</span></td>
						<td width=\"18%\"><span class=\"est12\">".$nombre_embalaje."</span></td>
						<td width=\"9%\"><span class=\"est12\">".$linea_3['num_embalajes']."</span></td>
						<td width=\"9%\"><span class=\"est12\">".$linea_3['unidades']."</span></td>
						<td width=\"9%\"><span class=\"est12\">".number_format($linea_3['precio_unidad'],2,",",".")." &euro;</span></td>
						<td width=\"9%\"><span class=\"est12\">";
						if ($linea_3['dto_porc'] > 0)
						{
							$cuerpo_pagina .= number_format($linea_3['dto_porc'],2,",",".")." %";
						}
						$cuerpo_pagina .= "</span></td>
						<td width=\"9%\"><span class=\"est12\">";
						if ($linea_3['dto2'] > 0)
						{
							$cuerpo_pagina .= number_format($linea_3['dto2'],2,",",".")." %";
						}
						$cuerpo_pagina .= "</span></td>
						<td width=\"10%\"><span class=\"est12\">".number_format($valor_igic,2,",",".")."</span></td>
					</tr>";
						$cuenta_articulos++;
					} // fin while linea_6
				} // fin del for
				if ($pag < $total_paginas)
				{
					$cuerpo_pagina .= "
					<tr>
						<td colspan=\"10\" align=\"left\"><span class=\"est12\">Suma y sigue</span></td>
					</tr>
";
				}
				$cuerpo_pagina .= "
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
<!-- fin cuerpo -->
</table>";
				//echo str_replace("#PAGINA#", $pag, $cabecera_pagina).$cuerpo_pagina.$pie_pagina;
				$mensaje_albaran .= "<table width=\"100%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
	<tr>
		<td>".str_replace("#PAGINA#", $pag, $cabecera_pagina)."</td>
	</tr>
	<tr>
		<td>".$cuerpo_pagina."</td>
	</tr>
	<tr>
		<td style=\"padding-top:3px;\">".$pie_pagina."</td>
	</tr>
</table>";
/*
			if ($_REQUEST['logo'] == 1)
			{
				echo "
<span class=\"est11\">Seg&uacute;n el art 5 de la Ley 15/1999, los datos de car&aacute;cter personal de este documento, est&aacute;n incorporados en un fichero automatizado utilizado para la gesti&oacute;n administrativa de la empresa Cuymon 2000 S.L., por lo que tiene derecho a acceder a sus datos personales, rectificarlos o en su caso cancelarlos, solicit&aacute;ndolo en el domicilio se&ntilde;alado en el mismo.</span>";
			}
*/
				if ($pag < $total_paginas)
				{
					$mensaje_albaran .= "
<div class=\"separador\">&nbsp;</div>";
				}
			} // fin for paginado
			$fin_pagina = "
</body>
</html>
";
		//$mensaje_albaran .= $fin_pagina;
		} //fin while linea_presu
	} //fin while linea_1
	
	//$mensaje_albaran = str_replace("class='tabla'", $oferta_estilo_tabla, str_replace("class='texto'", $oferta_estilo_texto, str_replace("class='tipo2'", $oferta_estilo2, str_replace("class='tipo2_top'", $oferta_estilo2_top, str_replace("class='tipo1'", $oferta_estilo1, str_replace("class='tipo1_no_fondo'", $oferta_estilo1_no_fondo, $mensaje_albaran))))));
	$mensaje_albaran = str_replace("class=\"est11\"", $presupuesto_estilo_est11, str_replace("class=\"est11b\"", $presupuesto_estilo_est11b, str_replace("class=\"est12\"", $presupuesto_estilo_est12, str_replace("class=\"est12b\"", $presupuesto_estilo_est12b, str_replace("class=\"est13\"", $presupuesto_estilo_est13, str_replace("class=\"est13b\"", $presupuesto_estilo_est13b, str_replace("class=\"est14\"", $presupuesto_estilo_est14, str_replace("class=\"est14b\"", $presupuesto_estilo_est14b, str_replace("class=\"est15\"", $presupuesto_estilo_est15, str_replace("class=\"est15b\"",$presupuesto_estilo_est15b, str_replace("class=\"est16\"", $presupuesto_estilo_est16, str_replace("class=\"est16b\"", $presupuesto_estilo_est16b, str_replace("class=\"separador\"", $presupuesto_estilo_separador, str_replace("class=\"est-logo\"", $presupuesto_estilo_logo, str_replace("images/", $ruta_imagenes, $mensaje_albaran)))))))))))))));
	
	return $mensaje_albaran;
}

function CalcularPrecioArtPresupuesto ($valor_articulo, $valor_cliente, $valor_proveedor, $valor_fecha, $unidades_pedidas)
{
$tabla_articulos = "articulos_t";
$tabla_cli_prov = "clientes_prov_t";
$tabla_cli_prov_fam = "clientes_prov_familias_t";
$tabla_cli_prov_fam_art = "clientes_prov_fam_articulos_t";
$tabla_prov_familias = "prov_familias_t";
$tabla_proveedores = "proveedores_t";
$tabla_prov_ofertas = "prov_ofertas_t";
$tabla_prov_ofertas_cli = "prov_ofertas_clientes_t";
$tabla_prov_oferta_fam = "prov_ofertas_familias_t";
$tabla_prov_oferta_fam_art = "prov_ofertas_fam_articulos_t";

	$array_precios = array();
	$array_descuentos = array();
	$array_netos = array();
	$valor_familia = 0;
	$valor_precio = 0;
	$cons = "select * from $tabla_articulos where id='".$valor_articulo."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$valor_familia = $lin['familia_id'];
		$valor_precio = $lin['precio'];
	}
	$array_precios[] = $valor_precio;
	// calculo del precio segun condiciones del cliente
	$precio_cliente = 0;
	$valor_cliente_prov_id = 0;
	$cons = "select * from $tabla_cli_prov where cliente_id='".$valor_cliente."' and proveedor_id='".$valor_proveedor."' and bloqueado='';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$valor_cliente_prov_id = $lin['id'];
	}
	if ($valor_cliente_prov_id > 0)
	{
		$cons1 = "select * from $tabla_cli_prov_fam where cliente_prov_id='".$valor_cliente_prov_id."' and familia_id='".$valor_familia."';";
		$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
		while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
		{
			$cons2 = "select * from $tabla_cli_prov_fam_art where cliente_prov_familia_id='".$lin1['id']."' and articulo_id='".$valor_articulo."';";
			$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
			while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
			{
				if ($lin2['precio_neto_cli_art'] > 0)
				{
					$precio_cliente = $lin2['precio_neto_cli_art'];
					$array_netos[1] = $lin2['precio_neto_cli_art'];
				}
				elseif ($lin2['dto_porc_cli_art'] > 0)
				{
					$dto_cliente = round(($valor_precio*$lin2['dto_porc_cli_art'])/100, 2);
					$precio_cliente = $valor_precio-$dto_cliente;
					$array_descuentos[1] = $lin2['dto_porc_cli_art'];
				}
				elseif ($lin1['dto_porc_cli_fam'] > 0)
				{
					$dto_cliente = round(($valor_precio*$lin1['dto_porc_cli_fam'])/100, 2);
					$precio_cliente = $valor_precio-$dto_cliente;
					$array_descuentos[1] = $lin2['dto_porc_cli_fam'];
				}
			}
		}
	}
	$array_precios[] = $precio_cliente;
	
	// calculo del precio segun condiciones del proveedor
	$precio_prov = 0;
	$cons = "select $tabla_prov_familias.dto_porc_prov_fam from $tabla_prov_familias join $tabla_proveedores on $tabla_proveedores.id=$tabla_prov_familias.proveedor_id where $tabla_prov_familias.proveedor_id='".$valor_proveedor."' and $tabla_prov_familias.familia_id='".$valor_familia."' and $tabla_proveedores.inactivo='';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		if ($lin['dto_porc_prov_fam'] > 0)
		{
			$dto_prov = round(($valor_precio*$lin['dto_porc_prov_fam'])/100, 2);
			$precio_prov = $valor_precio-$dto_prov;
			$array_descuentos[2] = $lin['dto_porc_prov_fam'];
		}
	}
	$array_precios[] = $precio_prov;
	
	// calculo del precio segun alguna posible oferta
	$precio_oferta = 0;
	$valor_prov_oferta_id = 0;
	$cons = "select $tabla_prov_ofertas.id from $tabla_prov_ofertas 
join $tabla_proveedores on $tabla_proveedores.id=$tabla_prov_ofertas.proveedor_id 
join $tabla_prov_ofertas_cli on $tabla_prov_ofertas_cli.prov_oferta_id=$tabla_prov_ofertas.id 
where $tabla_prov_ofertas.proveedor_id='".$valor_proveedor."' and $tabla_prov_ofertas.fecha_inicio<='".$valor_fecha."' and $tabla_prov_ofertas.fecha_fin>='".$valor_fecha."' and $tabla_proveedores.inactivo='' and $tabla_prov_ofertas_cli.cliente_id='".$valor_cliente."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$valor_prov_oferta_id = $lin['id'];
	}
	if ($valor_prov_oferta_id > 0)
	{
		$cons1 = "select * from $tabla_prov_oferta_fam where prov_oferta_id='".$valor_prov_oferta_id."' and familia_id='".$valor_familia."';";
		$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
		while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
		{
			$cons2 = "select * from $tabla_prov_oferta_fam_art where prov_oferta_familia_id='".$lin1['id']."' and articulo_id='".$valor_articulo."';";
			$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
			while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
			{
				if ($lin2['precio_neto_oferta_art'] > 0)
				{
					$precio_oferta = $lin2['precio_neto_oferta_art'];
					$array_netos[3] = $lin2['precio_neto_oferta_art'];
				}
				elseif ($lin2['dto_porc_oferta_art'] > 0)
				{
					$dto_oferta = round(($valor_precio*$lin2['dto_porc_oferta_art'])/100, 2);
					$precio_oferta = $valor_precio-$dto_oferta;
					$array_descuentos[3] = $lin2['dto_porc_oferta_art'];
				}
				elseif ($lin2['min_unidades_dto'] > 0 && $lin2['precio_unidad_dto'] > 0 && $lin2['min_unidades_dto'] <= $unidades_pedidas)
				{
					$precio_oferta = $lin2['precio_unidad_dto'];
					$array_netos[3] = $lin2['precio_unidad_dto'];
				}
				elseif ($lin1['dto_porc_oferta_fam'] > 0)
				{
					$dto_oferta = round(($valor_precio*$lin1['dto_porc_oferta_fam'])/100, 2);
					$precio_oferta = $valor_precio-$dto_oferta;
					$array_descuentos[3] = $lin2['dto_porc_oferta_fam'];
				}
			}
		}
	}
	$array_precios[] = $precio_oferta;
	$array_final = array();
	$array_final[0] = $array_precios;
	$array_final[1] = $array_descuentos;
	$array_final[2] = $array_netos;
	return $array_final;
}

function CalcularPresupuesto ($presupuesto_id)
{
$tabla_presupuestos_articulos = "pre_articulos_t";

	// los articulos reservados no se cobran
	$importe_total_sin = 0;
	$cons = "select sum(importe_articulo) as total from $tabla_presupuestos_articulos where presupuesto_id='".$presupuesto_id."' and reservado='';";// and fecha_max_reserva='0000-00-00';";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$importe_total_sin = $lin['total'];
	}
	$importe_total_igic = 0;
	$cons = "select sum(importe_igic) as total from $tabla_presupuestos_articulos where presupuesto_id='".$presupuesto_id."' and reservado='';";// and fecha_max_reserva='0000-00-00';";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$importe_total_igic = $lin['total'];
	}
	
	$array_final = array();
	$array_final[0] = $importe_total_sin;
	$array_final[1] = $importe_total_igic;
	$array_final[2] = $importe_total_sin+$importe_total_igic;
	return $array_final;
}

function RecalcularPortesPresupuesto ($presupuesto_id)
{
global $identificador_gran_canaria;

$tabla_presupuestos = "presupuestos_t";
$tabla_pre_art = "pre_articulos_t";
$tabla_clientes_direcciones = "clientes_direcciones_t";
$tabla_pre_portes = "pre_portes_t";

	$array_importes_proveedores = array();
	$cons3 = "select sum(importe_articulo) as total, $tabla_pre_art.proveedor_id from $tabla_pre_art where presupuesto_id='".$presupuesto_id."' and reservado='' group by $tabla_pre_art.proveedor_id;";// and fecha_max_reserva='0000-00-00'
	//echo "$cons3<br>";
	$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
	while ($lin3 = mysql_fetch_array($res3, MYSQL_ASSOC))
	{
		$array_importes_proveedores[$lin3['proveedor_id']]["importe_arti"] = $lin3['total'];
	}
	//echo print_r($array_importes_proveedores,true);
	$consulta_cat = "select $tabla_clientes_direcciones.municipio_id, $tabla_presupuestos.cliente_id from $tabla_clientes_direcciones join $tabla_presupuestos on $tabla_presupuestos.cliente_direccion_id=$tabla_clientes_direcciones.id where $tabla_presupuestos.id='".$presupuesto_id."';";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		$cons1 = "select * from maestro_municipios_t where id='".$linea_cat['municipio_id']."';";
		$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
		while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
		{
			if ($lin1['isla_id'] != $identificador_gran_canaria)
			{
				// hay que calcular portes
				foreach ($array_importes_proveedores as $valor_prov => $array_datos)
				{
					// busqueda del minimo que hay que superar para que no pague el cliente (primero el definido en clientes_prov_t, despues en proveedores_t)
					//$porc_portes = 0; => ya no se va a calcular
					$importe_min_portes_pagados = 0;
					$existe_porte = 0;
					$cons_porte = "select * from $tabla_pre_portes where presupuesto_id='".$presupuesto_id."' and proveedor_id='".$valor_prov."';";
					//echo "$cons_porte<br>";
					$res_porte = mysql_query($cons_porte) or die("La consulta fall&oacute;: $cons_porte " . mysql_error());
					while ($lin_porte = mysql_fetch_array($res_porte, MYSQL_ASSOC))
					{
						$existe_porte = 1;
						//$porc_portes = $lin_porte['prov_porc_portes']; => ya no se va a calcular
						$importe_min_portes_pagados = $lin_porte['importe_min_portes_pagados'];
					}
					if ($existe_porte == 1)
					{
						// contabilizo el importe sin igic
						$importe_total_prov = $array_datos["importe_arti"];//$array_datos["importe_igic"]
						//, porc_portes='".$porc_portes."' => ya no se va a calcular
						$cons_update_porte = "update $tabla_pre_portes set ";
						if ($importe_total_prov <= $importe_min_portes_pagados)
						{
							// no supera el minimo para que sea el proveedor quien pague los portes: se marca que se debe usar la ag transportes del cliente
							$cons_update_porte .= " usar_cliente='on'";
						}
						else
						{
							// si se supera el minimo: como lo paga el proveedor el importe es cero y se marca que se debe usar la ag transportes del proveedor
							$cons_update_porte .= " usar_cliente=''";
						}
						$cons_update_porte .= " where presupuesto_id='".$presupuesto_id."' and proveedor_id='".$valor_prov."';";
						//echo "$cons_update_porte<br>";
						$resultado_update_porte = mysql_query($cons_update_porte) or die("$cons_update_porte, La consulta fall�: " . mysql_error());
					}
				} // fin foreach
			} // fin if
		} // $lin1
	} // $linea_cat
}

function CalcularPedido ($pedido_id)
{
$tabla_ped_articulos = "ped_articulos_t";

	// los articulos reservados no se cobran
	$importe_total_sin = 0;
	$cons = "select sum(importe_articulo) as total from $tabla_ped_articulos where pedido_id='".$pedido_id."' and reservado='';";// and fecha_max_reserva='0000-00-00';";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$importe_total_sin = $lin['total'];
	}
	$importe_total_igic = 0;
	$cons = "select sum(importe_igic) as total from $tabla_ped_articulos where pedido_id='".$pedido_id."' and reservado='';";// and fecha_max_reserva='0000-00-00';";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$importe_total_igic = $lin['total'];
	}
	
	$array_final = array();
	$array_final[0] = $importe_total_sin;
	$array_final[1] = $importe_total_igic;
	$array_final[2] = $importe_total_sin+$importe_total_igic;
	return $array_final;
}

function RecalcularPortesPedido ($pedido_id)
{
global $identificador_gran_canaria;

$tabla_presupuestos = "presupuestos_t";
$tabla_pedidos = "pedidos_t";
$tabla_ped_art = "ped_articulos_t";
$tabla_clientes_direcciones = "clientes_direcciones_t";
$tabla_ped_portes = "ped_portes_t";

	$array_importes_proveedores = array();
	$cons3 = "select sum(importe_articulo) as total, $tabla_ped_art.proveedor_id from $tabla_ped_art where pedido_id='".$pedido_id."' and reservado='' group by $tabla_ped_art.proveedor_id;";// and fecha_max_reserva='0000-00-00'
	//echo "$cons3<br>";
	$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
	while ($lin3 = mysql_fetch_array($res3, MYSQL_ASSOC))
	{
		$array_importes_proveedores[$lin3['proveedor_id']]["importe_arti"] = $lin3['total'];
	}
	//echo print_r($array_importes_proveedores,true);
	$consulta_cat = "select $tabla_clientes_direcciones.municipio_id from $tabla_clientes_direcciones 
join $tabla_presupuestos on $tabla_presupuestos.cliente_direccion_id=$tabla_clientes_direcciones.id 
join $tabla_pedidos on $tabla_pedidos.presupuesto_id=$tabla_presupuestos.id 
where $tabla_pedidos.id='".$pedido_id."';";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		$cons1 = "select * from maestro_municipios_t where id='".$linea_cat['municipio_id']."';";
		$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
		while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
		{
			if ($lin1['isla_id'] != $identificador_gran_canaria)
			{
				// hay que calcular portes
				foreach ($array_importes_proveedores as $valor_prov => $array_datos)
				{
					// busqueda del minimo que hay que superar para que no pague el cliente (primero el definido en clientes_prov_t, despues en proveedores_t)
					$importe_min_portes_pagados = 0;
					$existe_porte = 0;
					$cons_porte = "select * from $tabla_ped_portes where pedido_id='".$pedido_id."' and proveedor_id='".$valor_prov."';";
					//echo "$cons_porte<br>";
					$res_porte = mysql_query($cons_porte) or die("La consulta fall&oacute;: $cons_porte " . mysql_error());
					while ($lin_porte = mysql_fetch_array($res_porte, MYSQL_ASSOC))
					{
						$existe_porte = 1;
						$importe_min_portes_pagados = $lin_porte['importe_min_portes_pagados'];
					}
					if ($existe_porte == 1)
					{
						// contabilizo el importe sin igic
						$importe_total_prov = $array_datos["importe_arti"];//$array_datos["importe_igic"]
						$cons_update_porte = "update $tabla_ped_portes set ";
						if ($importe_total_prov <= $importe_min_portes_pagados)
						{
							// no supera el minimo para que sea el proveedor quien pague los portes: se marca que se debe usar la ag transportes del cliente
							$cons_update_porte .= " usar_cliente='on'";
						}
						else
						{
							// si se supera el minimo: como lo paga el proveedor el importe es cero y se marca que se debe usar la ag transportes del proveedor
							$cons_update_porte .= " usar_cliente=''";
						}
						$cons_update_porte .= " where pedido_id='".$pedido_id."' and proveedor_id='".$valor_prov."';";
						//echo "$cons_update_porte<br>";
						$resultado_update_porte = mysql_query($cons_update_porte) or die("$cons_update_porte, La consulta fall�: " . mysql_error());
					}
				} // fin foreach
			} // fin if
		} // $lin1
	} // $linea_cat
}

function CalcularAlbaran ($albaran_id)
{
$tabla_alb_articulos = "alb_articulos_t";

	// los articulos reservados no se cobran
	$importe_total_sin = 0;
	$cons = "select sum(importe_articulo) as total from $tabla_alb_articulos where albaran_id='".$albaran_id."';";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$importe_total_sin = $lin['total'];
	}
	$importe_total_igic = 0;
	$cons = "select sum(importe_igic) as total from $tabla_alb_articulos where albaran_id='".$albaran_id."';";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$importe_total_igic = $lin['total'];
	}
	
	$array_final = array();
	$array_final[0] = $importe_total_sin;
	$array_final[1] = $importe_total_igic;
	$array_final[2] = $importe_total_sin+$importe_total_igic;
	return $array_final;
}

function BusquedaFechasVisitas ($valor_programacion, $numero_meses)
{
global $intervalo_especial;
	$array_fechas = array();
	$fecha_hoy = date("Y-m-d");
	$consultamod = "select * from programacion_visitas_t where id='".$valor_programacion."';";
	$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: " . mysql_error());
	// El resultado lo metemos en un array asociativo
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC))
	{
		$fecha_inicio = $lineasmod['fecha_visita'];
		$fecha_fin = CalcularNuevaFecha($fecha_inicio, $numero_meses." month");
		
		$hora_inicio = $lineasmod['hora'];
		$nueva_fecha = "";
		$consulta_fecha = "select date_add('".date("Y-m-d")." ".$hora_inicio.":00', interval ".$lineasmod['duracion']." minute) as resultado;";
		//echo "$consulta_fecha<br>";
		$resultado_fecha = mysql_query($consulta_fecha) or die("$consulta_fecha, La consulta fall&oacute;: " . mysql_error());
		while ($linea_fecha = mysql_fetch_array($resultado_fecha, MYSQL_ASSOC))
		{
			$nueva_fecha = $linea_fecha['resultado'];
		}
		list($tmp, $hora_fin) = explode(' ', $nueva_fecha);
		
		$intervalo = 0;
		if ($lineasmod['frecuencia_id'] > 0)
		{
			$cons = "select * from maestro_frecuencias_t where id='".$lineasmod['frecuencia_id']."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$intervalo = $lin['num_dias'];
			}
			$dia_preferido_visita = 0;
			$cons = "select maestro_dias_t.* from maestro_dias_t join clientes_t on clientes_t.dia_preferido_visita_id=maestro_dias_t.id where clientes_t.id='".$lineasmod['cliente_id']."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$dia_preferido_visita = $lin['valor_php'];
			}
			while (strtotime($fecha_inicio) < strtotime($fecha_fin))
			{
				$dia_semana = date("w",strtotime($fecha_inicio));
				// caso especial de 30 dias
				//if ($intervalo == $intervalo_especial)
				//{
					// en caso de ser la frecuencia de 30 dias se comprobara si es el dia de la semana preferido o se forzara
					// 2012-08-30
					if ($dia_semana != $dia_preferido_visita)
					{
						// fuerzo al siguiente fecha que sea el dia elegido
						$incremento = $dia_preferido_visita-$dia_semana;
						if ($incremento < 0) { $incremento += 7; }
						$fecha_inicio = CalcularNuevaFecha($fecha_inicio, $incremento." day");
						$dia_semana = date("w",strtotime($fecha_inicio));
					}
				//}
				if ($dia_semana == 0 || $dia_semana == 6)
				{
					if ($dia_semana == 0) { $incremento = 1; }
					if ($dia_semana == 6) { $incremento = 2; }
					$fecha_inicio = CalcularNuevaFecha($fecha_inicio, $incremento." day");
					$dia_semana = date("w",strtotime($fecha_inicio));
				}
				$no_disponible = 0;
				$cons = "select id from usuarios_ausencias_t where usuario_id='".$lineasmod['usuario_id']."' and (fecha_inicio<='$fecha_inicio' and fecha_fin>='$fecha_inicio');";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$no_disponible++;
				}
				$existe_visita = 0;
				$cons = "select id from agenda_t where cliente_id='".$lineasmod['cliente_id']."' and fecha='$fecha_inicio';";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$existe_visita++;
				}
				$existe_solape_comercial = 0;
				$cons = "select count(id) as total from agenda_t where comercial_visita_id='".$lineasmod['usuario_id']."' and fecha='$fecha_inicio' and (('$hora_inicio' >= hora_inicio and '$hora_inicio' < hora_fin) or ('$hora_fin' > hora_inicio and '$hora_fin' <= hora_fin) or ('$hora_inicio' <= hora_inicio and '$hora_fin' >= hora_fin));";
				//echo "$cons<br>";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$existe_solape_comercial = $lin['total'];
				}
				// se controlan:
				// - fechas pasadas
				// - domingos (0) y sabados (6)
				// - no disponibilidad del comercial
				// - la existencia de una visita a ese cliente el mismo dia (independiente de la hora)
				// - la existencia de visitas en ese horario para el mismo comercial
				if (strtotime($fecha_inicio) >= strtotime($fecha_hoy) && $dia_semana != 0 && $dia_semana != 6 && $no_disponible == 0 && $existe_visita == 0 && $existe_solape_comercial == 0)
				{
					//echo "($fecha_inicio)=>".$dia_semana."<br>";
					$array_fechas[] = $fecha_inicio;
				}
				else
				{
					//echo "<span style='font-style:italic; color:#dddddd;'>($fecha_inicio)=>".$dia_semana."</span><br>";
				}
				$fecha_inicio = CalcularNuevaFecha($fecha_inicio, $intervalo." day");
			}
		}
		//echo "*($fecha_fin)<br>";
	} // del while
	return $array_fechas;
}

function VerPlanningProv ($prov_planning_id)
{
global $oferta_color_texto, $oferta_color_borde, $oferta_estilo_tabla, $oferta_estilo1, $oferta_estilo_texto, $oferta_estilo1_no_fondo, $oferta_estilo2, $oferta_estilo2_top;

$tabla_proveedores = "proveedores_t";
$tabla_planning = "prov_planning_t";
$tabla_logistica = "prov_planning_logistica_t";
$tabla_agenda = "agenda_t";
$tabla_cli_direcciones = "clientes_direcciones_t";
$tabla_municipios = "maestro_municipios_t";
$tabla_islas = "maestro_islas_t";
$tabla_usuarios = "usuarios_t";
$tabla_clientes = "clientes_t";
$tabla_desplaza = "prov_plan_logis_desplaza_t";
$tabla_transportes = "maestro_tipos_transportes_t";

$array_dias_semana = array(1 => "lunes", 2 => "martes", 3 => "miercoles", 4 => "jueves", 5 => "viernes", 6 => "sabado", 0 => "domingo");

$minutos_horario = 30;
$tamano_fuente = 14;
$transporte_avion = 1;
$transporte_barco = 3;

$color_azul_1 = "3366FF";
$color_azul_2 = "000080";
$color_gris = "C0C0C0";

	$planning_prov = "";
	$tabla_datos_prov = "";
	$tabla_datos_tiempo = "";
	$tabla_calendario = "";
	$tabla_datos_direc = "";
	$tabla_datos_facturacion = "";
	
	$fecha_inicio = "";
	$fecha_fin = "";
	$proveedor_id = 0;
	$cons = "select $tabla_planning.fecha_inicio, $tabla_planning.fecha_fin, $tabla_planning.proveedor_id from $tabla_planning where $tabla_planning.id='".$prov_planning_id."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$fecha_inicio = $lin['fecha_inicio'];
		$fecha_fin = $lin['fecha_fin'];
		$proveedor_id = $lin['proveedor_id'];
	}
	
	// pesta�a 1: numero clientes
	$array_comerciales_planning = array();
	$cons = "select $tabla_agenda.comercial_visita_id from $tabla_agenda where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' group by $tabla_agenda.comercial_visita_id;";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$array_comerciales_planning[] = $lin['comercial_visita_id'];
	}
	//echo print_r($array_comerciales_planning,true);
	
	if (count($array_comerciales_planning) > 0)
	{
		$array_islas_planning = array();
		$cons = "select $tabla_municipios.isla_id from $tabla_agenda join $tabla_cli_direcciones on $tabla_cli_direcciones.id=$tabla_agenda.cliente_direccion_id join $tabla_municipios on $tabla_municipios.id=$tabla_cli_direcciones.municipio_id where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' group by $tabla_municipios.isla_id;";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$array_islas_planning[] = $lin['isla_id'];
		}
		//echo print_r($array_islas_planning,true);
		
		$array_tabla1 = array();
		$array_visitas_total = array();
		$array_visitas_tiempo = array();
		
		$titulo_col_comercial = "
		<td align=\"left\" class='tipo1'>TOT</td>
		<td align=\"left\" class='tipo1'>TI</td>";
		
		// fila	columna
		$array_tabla1[0][0] = "
		<td class='tipo1_no_fondo'>&nbsp;</td>";
		$array_tabla1[1][0] = "
		<td class='tipo1'>&nbsp;</td>";
		$filas = 2; // primera fila con isla
		//$columnas = 1; // primera columna con comercial
		// lo recorremos por filas
		foreach ($array_islas_planning as $key_isla => $valor_isla)
		{
			$nombre_isla = "";
			$cons = "select * from $tabla_islas where id='".$valor_isla."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$nombre_isla = $lin['nombre_corto'];
			}
			$array_tabla1[$filas][0] = "
		<td align=\"left\" class='tipo1'>$nombre_isla</td>";
			
			$columnas = 1;
			foreach ($array_comerciales_planning as $key_comercial => $valor_comercial)
			{
				$nombre_comercial = "";
				$cons = "select * from $tabla_usuarios where id='".$valor_comercial."';";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$nombre_comercial = $lin['nombre'];
				}
				$array_tabla1[0][$columnas] = "
		<td colspan=\"2\" align=\"left\" class='tipo1'>$nombre_comercial</td>";
				$array_tabla1[1][$columnas] = $titulo_col_comercial;
				$numero_visitas = 0;
				$cons = "select count($tabla_agenda.id) as total from $tabla_agenda join $tabla_cli_direcciones on $tabla_cli_direcciones.id=$tabla_agenda.cliente_direccion_id join $tabla_municipios on $tabla_municipios.id=$tabla_cli_direcciones.municipio_id where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' and $tabla_agenda.comercial_visita_id='".$valor_comercial."' and $tabla_municipios.isla_id='".$valor_isla."';";
				//echo "$cons<br>";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$numero_visitas = $lin['total'];
				}
				$tiempo_invertido = 0;
				$cons = "select sum($tabla_agenda.duracion) as total from $tabla_agenda join $tabla_cli_direcciones on $tabla_cli_direcciones.id=$tabla_agenda.cliente_direccion_id join $tabla_municipios on $tabla_municipios.id=$tabla_cli_direcciones.municipio_id where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' and $tabla_agenda.comercial_visita_id='".$valor_comercial."' and $tabla_municipios.isla_id='".$valor_isla."';";
				//echo "$cons<br>";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$tiempo_invertido = number_format($lin['total'],0);
				}
				$array_tabla1[$filas][$columnas] = "
		<td class='tipo2'>$numero_visitas</td>
		<td class='tipo2'>$tiempo_invertido</td>";
				$array_visitas_total[$valor_comercial] += $numero_visitas;
				$array_visitas_tiempo[$valor_comercial] += $tiempo_invertido;
				$columnas++;
			}
			
			// eso solo se pone la primera vez
			if ($filas == 2)
			{
				$array_tabla1[0][$columnas] = "
		<td class='tipo1_no_fondo'>&nbsp;</td>";
				$array_tabla1[1][$columnas] = "
		<td class='tipo1'>FACTURACION</td>";
			}
			//jgs : hay que calcular la facturacion
			$array_tabla1[$filas][$columnas] = "
		<td class='tipo2'>0 &euro;</td>";
			$filas++;
		}
		// fila de totales
		$array_tabla1[$filas][0] = "
		<td class='tipo1'>Total cliente</td>";
		$columnas = 1;
		foreach ($array_comerciales_planning as $key_comercial => $valor_comercial)
		{
			$array_tabla1[$filas][$columnas] = "
		<td class='tipo2'>".$array_visitas_total[$valor_comercial]."</td>
		<td class='tipo2'>".$array_visitas_tiempo[$valor_comercial]."</td>";
			$columnas++;
		}
		$array_tabla1[$filas][$columnas] = "
		<td class='tipo2'>0 &euro;</td>";
		
		// visualizacion
		$tabla_datos_prov = "
<table width=\"100%\" cellspacing=\"0\" class='tabla'>
	<tr>
		<td class='tipo1'>FABRICA</td>
		<td class='tipo2_top'>";
		$cons = "select * from $tabla_proveedores where id='".$proveedor_id."';";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$tabla_datos_prov .= $lin['nombre'];
		}
		$tabla_datos_prov .= "</td>
	</tr>
	<tr>
		<td class='tipo1'>TIPO DE SERVICIO</td>
		<td class='tipo2'>";
		$cons = "select maestro_tipos_servicios_t.nombre from maestro_tipos_servicios_t join $tabla_proveedores on $tabla_proveedores.tipo_servicio_id=maestro_tipos_servicios_t.id where $tabla_proveedores.id='".$proveedor_id."';";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$tabla_datos_prov .= $lin['nombre'];
		}
		$tabla_datos_prov .= "</td>
	</tr>
	<tr>
		<td class='tipo1'>FINALIDAD DE LA VISITA</td>
		<td class='tipo2'>";
		$cons = "select * from $tabla_planning where $tabla_planning.id='".$prov_planning_id."';";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$tabla_datos_prov .= $lin['observaciones'];
		}
		$tabla_datos_prov .= "</td>
	</tr>
	<tr>
		<td class='tipo1'>FECHA PROPUESTA</td>
		<td class='tipo2'>Desde ".date("d-m-Y",strtotime($fecha_inicio))." hasta ".date("d-m-Y",strtotime($fecha_fin))."</td>
	</tr>
	<tr>
		<td class='tipo1'>ISLA/S</td>
		<td class='tipo2'>";
		$nombre_islas = "";
		foreach ($array_islas_planning as $key_isla => $valor_isla)
		{
			if ($nombre_islas != "") { $nombre_islas .= ", "; }
			$cons = "select * from $tabla_islas where id='".$valor_isla."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$nombre_islas .= $lin['nombre'];
			}
		}
		$tabla_datos_prov .= $nombre_islas."</td>
	</tr>
</table>";
		$tabla_datos_tiempo = "
<table width=\"100%\" cellspacing=\"0\" class='tabla'>";
		foreach ($array_tabla1 as $key_fila => $valores_fila)
		{
			$tabla_datos_tiempo .= "
	<tr align=\"right\">";
			foreach ($valores_fila as $key_columna => $valores_col)
			{
				//echo "($key_fila)($key_columna)";
				$tabla_datos_tiempo .= $valores_col;
			}
			$tabla_datos_tiempo .= "
	</tr>";
		}
		$tabla_datos_tiempo .= "
</table>
</div>";
		$tabla_datos_tiempo .= "
<div style=\"margin-top:10px; margin-bottom:10px;\">
<table cellspacing=\"0\" class='tabla'>
	<tr>
		<td class='tipo1'>TOT:</td>
		<td class='tipo2_top'>N&ordm; CLIENTES COMERCIAL CON FABRICA</td>
	</tr>
	<tr>
		<td class='tipo1'>TI:</td>
		<td class='tipo2'>TIEMPO NECESARIO</td>
	</tr>
</table>";
		
		// pesta�a 2: planning
		//$array_fechas = array();
		$array_semanas = array();
		$fecha_tmp = $fecha_inicio;
		while (strtotime($fecha_tmp) <= strtotime($fecha_fin))
		{
			$dia_semana = date("w",strtotime($fecha_tmp));
			if ($dia_semana != 0 && $dia_semana != 6) // si no es domingo o sabado
			{
				//$array_fechas[] = substr($fecha_tmp,0,10);
				$array_semanas[date("W",strtotime($fecha_tmp))][] = substr($fecha_tmp,0,10);
			}
			$fecha_tmp = CalcularNuevaFecha($fecha_tmp, "1 day");
		}
		//echo print_r($array_semanas,true);
		
		$hora_minima = "00:00";
		$cons = "select $tabla_agenda.hora_inicio from $tabla_agenda where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' order by $tabla_agenda.hora_inicio asc limit 1;";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			//$hora_minima = substr($lin['hora_inicio'],0,5);
			$hora_minima = $lin['hora_inicio'];
		}
		// comprobar si no hay una hora anterior en los desplazamientos
		$hora_minima_desplaza = "";
		$cons = "select $tabla_desplaza.* from $tabla_desplaza 
join $tabla_logistica on $tabla_logistica.id=$tabla_desplaza.prov_planning_logistica_id
where $tabla_logistica.fecha>='$fecha_inicio' and $tabla_logistica.fecha<='$fecha_fin' and $tabla_logistica.prov_planning_id='".$prov_planning_id."' order by $tabla_desplaza.hora_inicio asc limit 1;";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$hora_minima_desplaza = $lin['hora_inicio'];
		}
		if ($hora_minima_desplaza != "" && strtotime(date("Y-m-d")." ".$hora_minima_desplaza) < strtotime(date("Y-m-d")." ".$hora_minima))
		{
			$hora_minima = $hora_minima_desplaza;
		}
		$hora_maxima = "23:59";
		$cons = "select $tabla_agenda.hora_fin from $tabla_agenda where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' order by $tabla_agenda.hora_fin desc limit 1;";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			//$hora_maxima = substr($lin['hora_fin'],0,5);
			$hora_maxima = $lin['hora_fin'];
		}
		// comprobar si no hay una hora superior en los desplazamientos
		$hora_maxima_desplaza = "";
		$cons = "select $tabla_desplaza.* from $tabla_desplaza 
join $tabla_logistica on $tabla_logistica.id=$tabla_desplaza.prov_planning_logistica_id
where $tabla_logistica.fecha>='$fecha_inicio' and $tabla_logistica.fecha<='$fecha_fin' and $tabla_logistica.prov_planning_id='".$prov_planning_id."' order by $tabla_desplaza.hora_fin desc limit 1;";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$hora_maxima_desplaza = $lin['hora_fin'];
		}
		if ($hora_maxima_desplaza != "" && strtotime(date("Y-m-d")." ".$hora_maxima_desplaza) > strtotime(date("Y-m-d")." ".$hora_maxima))
		{
			$hora_maxima = $hora_maxima_desplaza;
		}
		
		$array_horas_visitas = array();
		$fecha_minima = date("Y-m-d")." ".$hora_minima;
		$fecha_maxima = date("Y-m-d")." ".$hora_maxima;
		while (strtotime($fecha_minima) <= strtotime($fecha_maxima))
		{
			$array_horas_visitas[] = substr($fecha_minima,11);
			$fecha_minima = CalcularNuevaFecha($fecha_minima, $minutos_horario." minute");
		}
		//echo print_r($array_horas_visitas,true);
		
		$tabla_calendario = "";

		foreach ($array_semanas as $array_fechas)
		{
 			$array_tabla2 = array();
			$array_tabla2[0][0] = "
		<td class='tipo1_no_fondo'>&nbsp;</td>";
			$array_tabla2[1][0] = "
		<td class='tipo1_no_fondo'>&nbsp;</td>";
			$array_tabla2[2][0] = "
		<td class='tipo1_no_fondo'>&nbsp;</td>";
			$filas = 3; // primera fila con hora
			//$columnas = 1; // primera columna con comercial
			
			// lo recorremos por columnas
			$indice_horas = 0;
			$total_horas = count($array_horas_visitas);
			while ($indice_horas < ($total_horas-1))
			{
				$array_tabla2[$filas][0] = "
		<td class='tipo1'>".substr($array_horas_visitas[$indice_horas],0,5)." - ".substr($array_horas_visitas[$indice_horas+1],0,5)."</td>";
				$indice_horas++;
				$filas++;
			}
			$array_tabla2[$filas][0] = "
		<td class='tipo1'>HOTEL</td>";
			$columnas = 1;
			foreach ($array_fechas as $key_fecha => $valor_fecha)
			{
				$dia_semana = date("w",strtotime($valor_fecha));
				// dia semana
				$array_tabla2[0][$columnas] = "
		<td class='tipo1'>".strtoupper($array_dias_semana[$dia_semana])."</td>";
				// fecha
				$array_tabla2[1][$columnas] = "
		<td class='tipo1'>".date("d-m-Y",strtotime($valor_fecha))."</td>";
				$nombre_isla = "";
				$cons = "select $tabla_islas.nombre_corto from $tabla_logistica join $tabla_islas on $tabla_islas.id=$tabla_logistica.isla_id where $tabla_logistica.prov_planning_id='".$prov_planning_id."' and $tabla_logistica.fecha='".$valor_fecha."';";
				//echo "$cons<br>";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					if ($nombre_isla != "") { $nombre_isla .= ", "; }
					$nombre_isla .= $lin['nombre_corto'];
				}
				// nombre isla
				$array_tabla2[2][$columnas] = "
		<td class='tipo1'>".$nombre_isla."</td>";
				$filas = 3; // primera fila con hora
				$indice_horas = 0;
				while ($indice_horas < ($total_horas-1))
				{
					$visita_id = 0;
					$rowspan_visita = 0;
					$contenido_celda = "";
					$cons = "select $tabla_agenda.* from $tabla_agenda where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha='".$valor_fecha."' and $tabla_agenda.hora_inicio>='".$array_horas_visitas[$indice_horas]."' and $tabla_agenda.hora_inicio<'".$array_horas_visitas[$indice_horas+1]."';";
					//echo "$cons<br>";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
					while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
					{
						$visita_id = $lin['id'];
						$duracion = $lin['duracion'];
						if ($lin['hora_inicio'] != $array_horas_visitas[$indice_horas])
						{
							// si la hora de inicio no coincide con la de la tabla hay que aumentar la duracion para que se dibuje bien
							$desfase = (strtotime("$valor_fecha $lin[hora_inicio]")-strtotime("$valor_fecha $array_horas_visitas[$indice_horas]"))/60;
							$duracion += $desfase;
						}
						while ($duracion >= $minutos_horario)
						{
							$duracion -= $minutos_horario;
							$rowspan_visita++;
						}
						$nombre_cliente = "";
						$cons_cli = "select * from $tabla_clientes where id='".$lin['cliente_visitado_id']."';";
						$res_cli = mysql_query($cons_cli) or die("La consulta fall&oacute;: $cons_cli " . mysql_error());
						while ($lin_cli = mysql_fetch_array($res_cli, MYSQL_ASSOC))
						{
							$nombre_cliente = $lin_cli['nombre_corto'];
						}
						$nombre_comercial = "";
						$cons_com = "select * from $tabla_usuarios where id='".$lin['comercial_visita_id']."';";
						$res_com = mysql_query($cons_com) or die("La consulta fall&oacute;: $cons_com " . mysql_error());
						while ($lin_com = mysql_fetch_array($res_com, MYSQL_ASSOC))
						{
							$nombre_comercial = $lin_com['nombre'];
						}
						$contenido_celda = substr($lin['hora_inicio'],0,5)." - ".substr($lin['hora_fin'],0,5)."<br>".$nombre_cliente."<br>".$nombre_comercial;
					}
					if ($rowspan_visita == 0)
					{
						// no hay ninguna visita en ese horario
//jgs
						// se comprueba si hay desplazamientos
						$rowspan_desplaza = 0;
						$futuras_visitas = 0;
						$contenido_desplaza = "";
						$cons = "select $tabla_desplaza.* from $tabla_desplaza 
join $tabla_logistica on $tabla_logistica.id=$tabla_desplaza.prov_planning_logistica_id
where $tabla_logistica.fecha='".$valor_fecha."' and $tabla_logistica.prov_planning_id='".$prov_planning_id."' and $tabla_desplaza.hora_inicio>='".$array_horas_visitas[$indice_horas]."' and $tabla_desplaza.hora_inicio<'".$array_horas_visitas[$indice_horas+1]."';";
						//echo "$cons<br>";
						$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
						while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
						{
							if ($lin['tipo_transporte_id'] == $transporte_avion || $lin['tipo_transporte_id'] == $transporte_barco)
							{
								$duracion = (strtotime("$valor_fecha $lin[hora_fin]")-strtotime("$valor_fecha $lin[hora_inicio]"))/60;
								if ($lin['hora_inicio'] != $array_horas_visitas[$indice_horas])
								{
									// si la hora de inicio no coincide con la de la tabla hay que aumentar la duracion para que se dibuje bien
									$desfase = (strtotime("$valor_fecha $lin[hora_inicio]")-strtotime("$valor_fecha $array_horas_visitas[$indice_horas]"))/60;
									$duracion += $desfase;
								}
								while ($duracion >= $minutos_horario)
								{
									$duracion -= $minutos_horario;
									$rowspan_desplaza++;
								}
							}
							else
							{
								// transporte que no sea avion o barco
								$rowspan_desplaza++;
							}
							// en caso de existir desplazamiento se comprueba si chocaria con una visita (que tiene prioridad)
							$cons2 = "select count($tabla_agenda.id) as total from $tabla_agenda where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha='".$valor_fecha."' and $tabla_agenda.hora_inicio>='".$array_horas_visitas[$indice_horas]."' and $tabla_agenda.hora_inicio<'".$array_horas_visitas[$indice_horas+$rowspan_desplaza]."';";
							//echo "$cons2<br>";
							$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
							while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
							{
								$futuras_visitas = $lin2['total'];
							}
							if ($futuras_visitas == 0)
							{
								// si no choca con otras visitas preparamos el contenido
								$datos_transporte = "";
								$hora_transporte = "";
								switch ($lin['tipo_transporte_id'])
								{
									case 1:
										// avion
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['avion_vuelo'];
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['avion_trayecto'];
										$hora_transporte = substr($lin['hora_inicio'],0,5)." - ".substr($lin['hora_fin'],0,5);
										break;
									case 2:
										// coche
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['coche_compania'];
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['coche_modelo'];
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['coche_recogida'];
										$hora_transporte = substr($lin['hora_inicio'],0,5);
										break;
									case 3:
										// barco
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['barco_compania'];
										if ($datos_transporte != "") { $datos_transporte .= ", "; }
										$datos_transporte .= $lin['barco_trayecto'];
										$hora_transporte = substr($lin['hora_inicio'],0,5)." - ".substr($lin['hora_fin'],0,5);
										break;
								}
								$nombre_transporte = "";
								$cons_trans = "select * from $tabla_transportes where id='".$lin['tipo_transporte_id']."';";
								$res_trans = mysql_query($cons_trans) or die("La consulta fall&oacute;: $cons_trans " . mysql_error());
								while ($lin_trans = mysql_fetch_array($res_trans, MYSQL_ASSOC))
								{
									$nombre_transporte = $lin_trans['nombre'];
								}
								if ($contenido_desplaza != "") { $contenido_desplaza .= "<br>"; }
								$contenido_desplaza .= "$nombre_transporte ($hora_transporte): $datos_transporte";
							}
						}
						if ($rowspan_desplaza == 1 && $futuras_visitas == 0)
						{
							// hay desplazamiento que solo ocupa una fila y no choca con visitas
							$array_tabla2[$filas][$columnas] = "
		<td class='tipo2' align=\"left\">&nbsp;$contenido_desplaza</td>";
							$filas++;
							$indice_horas++;
						}
						elseif ($rowspan_desplaza > 1 && $futuras_visitas == 0)
						{
							// hay desplazamiento que ocupa mas de una fila y no choca con visitas
							$array_tabla2[$filas][$columnas] = "
		<td rowspan=\"".$rowspan_desplaza."\" class='tipo2' align=\"left\">&nbsp;$contenido_desplaza</td>";
							$filas++;
							$indice_horas++;
							for ($i=1; $i<$rowspan_desplaza; $i++)
							{
								$array_tabla2[$filas][$columnas] = "";
								$filas++;
								$indice_horas++;
							}
						}
						else
						{
							// no hay desplazamientos
							$array_tabla2[$filas][$columnas] = "
		<td class='tipo2'>&nbsp;</td>";
							$filas++;
							$indice_horas++;
						}
					}
					elseif ($rowspan_visita == 1)
					{
						// hay visita en ese horario y solo ocupa una fila
						$array_tabla2[$filas][$columnas] = "
		<td class='tipo2'>$contenido_celda</td>";
						$filas++;
						$indice_horas++;
					}
					else
					{
						// hay visita en ese horario y ocupa mas de una fila
						$array_tabla2[$filas][$columnas] = "
		<td rowspan=\"".$rowspan_visita."\" class='tipo2'>$contenido_celda</td>";
						$filas++;
						$indice_horas++;
						for ($i=1; $i<$rowspan_visita; $i++)
						{
							$array_tabla2[$filas][$columnas] = "";
							$filas++;
							$indice_horas++;
						}
					}
				}
				$datos_hotel = "";
				$cons = "select $tabla_logistica.hotel_nombre, $tabla_islas.nombre_corto from $tabla_logistica join $tabla_islas on $tabla_islas.id=$tabla_logistica.isla_id where $tabla_logistica.prov_planning_id='".$prov_planning_id."' and $tabla_logistica.fecha='".$valor_fecha."' and $tabla_logistica.hotel_nombre<>'';";
				//echo "$cons<br>";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					if ($datos_hotel != "") { $datos_hotel .= "<br>"; }
					$datos_hotel .= "$lin[nombre_corto]: $lin[hotel_nombre]";
				}
				// datos hotel
				$array_tabla2[$filas][$columnas] = "
		<td class='tipo2' align=\"left\">".$datos_hotel."</td>";
				$filas++;
				$columnas++;
			}
			//echo "<pre>".print_r($array_tabla2,true)."</pre>";
			
			// visualizacion
			$tabla_calendario .= "
<table width=\"100%\" cellspacing=\"0\" class='tabla'>";
			foreach ($array_tabla2 as $key_fila => $valores_fila)
			{
				$tabla_calendario .= "
	<tr align=\"center\">";
				foreach ($valores_fila as $key_columna => $valores_col)
				{
					//echo "($key_fila)($key_columna)";
					$tabla_calendario .= $valores_col;
				}
				$tabla_calendario .= "
	</tr>";
			}
			$tabla_calendario .= "
</table>";
			// fin array semanas
		}
		
		// datos direcciones
		$campos_listado = array ('cliente_id','direccion','cp','poblacion','provincia_id',
'municipio_id','telefono','telefono2','telefono3','fax');
		$tablas_campos_listado = array($tabla_cli_direcciones,$tabla_cli_direcciones,$tabla_cli_direcciones,$tabla_cli_direcciones,$tabla_cli_direcciones,
$tabla_cli_direcciones,$tabla_cli_direcciones,$tabla_cli_direcciones,$tabla_cli_direcciones,$tabla_cli_direcciones);
		$nombres_listado = array('Cliente','Direccion','CP','Poblacion','Provincia',
'Municipio','Telefono','Telefono2','Telefono3','Fax');
		$campos_listado_decod = array ('si;clientes_t;nombre;id','','','','si;maestro_provincias_t;nombre;id',
'si;maestro_municipios_t;nombre;id','','','','');
		
		$string_para_select = "$tabla_agenda.cliente_direccion_id,";
		foreach($campos_necesarios_listado as $indice => $campo)
		{
			$string_para_select .= $tablas_campos_necesarios[$indice].".$campo,";
		}
		$tabla_datos_direc = "
<table width=\"100%\" cellspacing=\"0\" class='tabla'>
	<tr>";
		$columnas = 0;
		foreach ($campos_listado as $campo)
		{
			//echo "<td><b><font color='#ffffff'>$nombres_listado[$columnas]</font></b></td>";
			$tabla_datos_direc .= "<td class='tipo1'>$nombres_listado[$columnas]</td>";
			$string_para_select .= $tablas_campos_listado[$columnas].".$campo,";
			$columnas++;
		}
		// Eliminamos el ultimo caracter
		$string_para_select = substr($string_para_select,0,-1);
		$tabla_datos_direc .= "</tr>";
		
		$cons = "select $string_para_select from $tabla_agenda join $tabla_cli_direcciones on $tabla_cli_direcciones.id=$tabla_agenda.cliente_direccion_id where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' group by $tabla_agenda.cliente_direccion_id order by $tabla_agenda.fecha, $tabla_agenda.hora_inicio, $tabla_agenda.hora_fin;";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$tabla_datos_direc .= "<tr>";
			foreach ($campos_listado as $cuenta_campos => $campo)
			{
				$nombre = "";
				list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod[$cuenta_campos]);
				if ($ele != 'si')
				{
					$nombre = "$lin[$campo]";
				}
				elseif ($ele2 == "date")
				{
					if ($lin[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
					else { $nombre = date("d-m-Y",strtotime($lin[$campo])); }
				}
				elseif ($ele2 == "datetime")
				{
					if ($lin[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
					else { $nombre = date("d-m-Y H:i",strtotime($lin[$campo])); }
				}
				elseif ($ele2 == "checkbox")
				{
					($lin[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
				}
				elseif ($ele2 == "time")
				{
					list($temp1, $temp2, $temp3) = explode(':',$lin[$campo]);
					$nombre = $temp1.":".$temp2;
				}
				elseif ($lin[$campo] != "")
				{
					$consultaselect = "select * from $ele2 where $ele4='$lin[$campo]';";
					//echo "$consultaselect";
					$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
					while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
					{
						$nombre = $lineaselect[$ele3];
					}
				}
				$tabla_datos_direc .= "<td class='tipo2'>$nombre</td>";
			}
			$tabla_datos_direc .= "</tr>";
			//echo "<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas'></td></tr>";
		} // fin while
		$tabla_datos_direc .= "
</table>";
		
		// pesta�a 3: nombre clientes
		// fila	columna
		$array_tabla3 = array();
		$array_num_cli_islas = array();
		$array_facturacion_total = array();
		
		$titulo_col_isla = "
		<td class='tipo1'>CLIENTE</td>
		<td class='tipo1'>FAC.</td>";
		
		$columnas = 0;
		// lo recorremos por columnas
		foreach ($array_islas_planning as $key_isla => $valor_isla)
		{
			$nombre_isla = "";
			$cons = "select * from $tabla_islas where id='".$valor_isla."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$nombre_isla = $lin['nombre'];
			}
			$array_tabla3[0][$columnas] = "
		<td colspan=\"2\" align=\"center\" class='tipo1'>$nombre_isla</td>";
			$array_tabla3[1][$columnas] = $titulo_col_isla;
			$num_clientes = 0;
			$filas = 2;
			$cons = "select $tabla_agenda.cliente_visitado_id, $tabla_clientes.nombre from $tabla_agenda join $tabla_cli_direcciones on $tabla_cli_direcciones.id=$tabla_agenda.cliente_direccion_id join $tabla_municipios on $tabla_municipios.id=$tabla_cli_direcciones.municipio_id join $tabla_clientes on $tabla_clientes.id=$tabla_agenda.cliente_visitado_id where $tabla_agenda.tipo_agenda_id='2' and $tabla_agenda.proveedor_id='".$proveedor_id."' and $tabla_agenda.fecha>='$fecha_inicio' and $tabla_agenda.fecha<='$fecha_fin' and $tabla_municipios.isla_id='".$valor_isla."' order by $tabla_clientes.nombre;";
			//echo "$cons<br>";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$array_tabla3[$filas][$columnas] = "
		<td class='tipo2'>".$lin['nombre']."</td>
		<td align=\"right\" class='tipo2'>0</td>";
				$array_facturacion_total[$columnas] += 0;
				$num_clientes++;
				$filas++;
			}
			$array_num_cli_islas[$columnas] = $num_clientes;
			$columnas++;
		}
		
		// completar la tabla con filas vacias en las islas con menos clientes
		$maximo_clientes = max($array_num_cli_islas);
		$columnas = 0;
		// lo recorremos por columnas
		foreach ($array_islas_planning as $key_isla => $valor_isla)
		{
			if ($array_num_cli_islas[$columnas] < $maximo_clientes)
			{
				for ($i = $array_num_cli_islas[$columnas]; $i < $maximo_clientes; $i++)
				{
					$array_tabla3[$i+2][$columnas] = "
		<td class='tipo2'>&nbsp;</td>
		<td class='tipo2'>&nbsp;</td>";
				}
			}
			$columnas++;
		}
		$fila_totales = $maximo_clientes+2;
		$columnas = 0;
		// lo recorremos por columnas
		foreach ($array_islas_planning as $key_isla => $valor_isla)
		{
			$array_tabla3[$fila_totales][$columnas] = "
		<td class='tipo1'>TOTAL</td>
		<td class='tipo2'>".$array_facturacion_total[$columnas]." &euro;</td>";
			$columnas++;
		}
		
		// visualizacion
		$tabla_datos_facturacion = "
<table width=\"100%\" cellspacing=\"0\" class='tabla'>";
		foreach ($array_tabla3 as $key_fila => $valores_fila)
		{
			$tabla_datos_facturacion .= "
	<tr>";
			ksort($valores_fila);
			foreach ($valores_fila as $key_columna => $valores_col)
			{
				//echo "($key_fila)($key_columna)";
				$tabla_datos_facturacion .= $valores_col;
			}
			$tabla_datos_facturacion .= "
	</tr>";
		}
		$tabla_datos_facturacion .= "
</table>";
	} // fin count $array_comerciales_planning
	
	$planning_prov = $tabla_datos_prov."<div style=\"margin-top:10px; margin-bottom:10px;\">".$tabla_calendario."</div>"."<div style=\"margin-top:10px; margin-bottom:10px;\">".$tabla_datos_tiempo."</div>"."<div style=\"margin-top:10px; margin-bottom:10px;\">".$tabla_datos_facturacion."</div>";
	//."<div style=\"margin-top:10px; margin-bottom:10px;\">".$tabla_datos_direc."</div>"
	
	$planning_prov = str_replace("class='tabla'", $oferta_estilo_tabla, str_replace("class='texto'", $oferta_estilo_texto, str_replace("class='tipo2'", $oferta_estilo2, str_replace("class='tipo2_top'", $oferta_estilo2_top, str_replace("class='tipo1'", $oferta_estilo1, str_replace("class='tipo1_no_fondo'", $oferta_estilo1_no_fondo, $planning_prov))))));
	
	return $planning_prov;
}

function CorreoOfertaCliente ($valor_oferta_cliente)
{
$tabla_ofertas = "prov_ofertas_t";
$tabla_ofertas_cli = "prov_ofertas_clientes_t";

	$oferta_id = 0;
	$cons = "select $tabla_ofertas.id from $tabla_ofertas 
left join $tabla_ofertas_cli on $tabla_ofertas.id=$tabla_ofertas_cli.prov_oferta_id where $tabla_ofertas_cli.id='".$valor_oferta_cliente."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$oferta_id = $lin['id'];
	}
	$mensaje_oferta = "";
	if ($oferta_id > 0)
	{
		$mensaje_oferta = CorreoOferta($oferta_id);
	}
	return $mensaje_oferta;
}

function CorreoOfertaProv ($valor_oferta)
{
global $oferta_color_texto, $oferta_color_borde, $oferta_estilo_tabla, $oferta_estilo1, $oferta_estilo_texto, $oferta_estilo1_no_fondo, $oferta_estilo2, $oferta_estilo2_top;

$tabla_ofertas = "prov_ofertas_t";
$tabla_log = "log_ofertas_t";
$tabla_clientes = "clientes_t";
	
	$mensaje_oferta = "";
	$cons = "select $tabla_ofertas.* from $tabla_ofertas where $tabla_ofertas.id='".$valor_oferta."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$mensaje_oferta .= "
<span class='texto'>La oferta \"$lin[nombre]\" se ha enviado a los siguientes clientes:</span>";
	}
	$mensaje_oferta .= "
<table cellspacing='0' class='tabla'>
	<tr>
		<td class='tipo1'>Cliente</td>
		<td class='tipo1'>Fecha</td>
	</tr>";
	$cons = "select $tabla_log.*, $tabla_clientes.nombre from $tabla_log join $tabla_clientes on $tabla_clientes.id=$tabla_log.cliente_id where $tabla_log.cliente_id>0 and $tabla_log.prov_oferta_id='".$valor_oferta."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$mensaje_oferta .= "
	<tr>
		<td class='tipo2'>$lin[nombre]</td>
		<td class='tipo2'>".date("d-m-Y H:i", strtotime($lin['fecha']." ".$lin['hora']))."</td>
	</tr>";
	}
	$mensaje_oferta .= "
</table>";
	$mensaje_oferta .= "
<span class='texto'>Informacion enviada</span>";
	
	$mensaje_oferta = str_replace("class='tabla'", $oferta_estilo_tabla, str_replace("class='texto'", $oferta_estilo_texto, str_replace("class='tipo2'", $oferta_estilo2, str_replace("class='tipo2_top'", $oferta_estilo2_top, str_replace("class='tipo1'", $oferta_estilo1, str_replace("class='tipo1_no_fondo'", $oferta_estilo1_no_fondo, $mensaje_oferta))))));
	
	$mensaje_oferta .= CorreoOferta($valor_oferta);
	return $mensaje_oferta;
}

function CorreoOferta ($valor_oferta)
{
global $fckeditor_camino_relativo, $ruta_mailing, $oferta_color_texto, $oferta_color_borde, $oferta_estilo_tabla, $oferta_estilo1, $oferta_estilo_texto, $oferta_estilo1_no_fondo, $oferta_estilo2, $oferta_estilo2_top;

$tabla_ofertas = "prov_ofertas_t";
$tabla_proveedores = "proveedores_t";
$tabla_ofertas_cli = "prov_ofertas_clientes_t";
$tabla_ofertas_fam = "prov_ofertas_familias_t";
$tabla_familias = "maestro_familias_t";
$tabla_ofertas_fam_art = "prov_ofertas_fam_articulos_t";
$tabla_articulos = "articulos_t";
$tabla_ofertas_imagenes = "prov_ofertas_imagenes_t";
	
	$mensaje_oferta = "";
	$cons = "select $tabla_ofertas.*, $tabla_proveedores.nombre as nombre_proveedor from $tabla_ofertas 
left join $tabla_proveedores on $tabla_ofertas.proveedor_id=$tabla_proveedores.id 
where $tabla_ofertas.id='".$valor_oferta."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$mensaje_oferta .= "
<table cellspacing='0' class='tabla'>
	<tr>
		<td class='tipo1'>Nombre oferta</td>
		<td class='tipo2_top'>$lin[nombre]</td>
	</tr>
	<tr>
		<td class='tipo1'>Proveedor</td>
		<td class='tipo2'>$lin[nombre_proveedor]</td>
	</tr>
	<tr>
		<td class='tipo1'>Fecha</td>
		<td class='tipo2'>".date("d-m-Y", strtotime($lin['fecha_inicio']))." hasta ".date("d-m-Y", strtotime($lin['fecha_fin']))."</td>
	</tr>
</table>
<span class='texto'>Detalle de la oferta</span>";
		if ($lin['min_importe_regalo'] > 0)
		{
			$mensaje_oferta .= "
<table cellspacing='0' class='tabla'>
	<tr>
		<td class='tipo1'>Regalo por compra</td>
	</tr>
	<tr>
		<td class='tipo2'>Si compra ".number_format($lin['min_importe_regalo'],2,",",".")." &euro; le regalamos $lin[regalo]</td>
	</tr>
</table>";
		}
	}
	$num_familias = 0;
	$cons = "select count(id) as total from $tabla_ofertas_fam where prov_oferta_id='".$valor_oferta."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$num_familias = $lin['total'];
	}
	if ($num_familias > 0)
	{
		$cons = "select $tabla_ofertas_fam.*, $tabla_familias.nombre from $tabla_ofertas_fam join $tabla_familias on $tabla_familias.id=$tabla_ofertas_fam.familia_id where prov_oferta_id='".$valor_oferta."' order by $tabla_familias.nombre;";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$mensaje_oferta .= "
<table cellspacing='0' class='tabla'>
	<tr>
		<td class='tipo1_no_fondo'>&nbsp;</td>
		<td class='tipo1'>Dto % de la familia</td>
	</tr>
	<tr>
		<td class='tipo1'>$lin[nombre]</td>
		<td class='tipo2'>";
			if ($lin['dto_porc_oferta_fam'] > 0)
			{
				$mensaje_oferta .= number_format($lin['dto_porc_oferta_fam'],2,",",".")." %";
			}
			$mensaje_oferta .= "</td>
	</tr>
</table>";
			$num_articulos = 0;
			$cons2 = "select count($tabla_ofertas_fam_art.id) as total from $tabla_ofertas_fam_art 
join $tabla_ofertas_fam on $tabla_ofertas_fam.id=$tabla_ofertas_fam_art.prov_oferta_familia_id 
where $tabla_ofertas_fam.prov_oferta_id='".$valor_oferta."';";
			$cons2 = "select count($tabla_ofertas_fam_art.id) as total from $tabla_ofertas_fam_art 
where $tabla_ofertas_fam_art.prov_oferta_familia_id='".$lin['id']."';";
			$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
			while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
			{
				$num_articulos = $lin2['total'];
			}
			if ($num_articulos > 0)
			{
				$mensaje_oferta .= "
<table cellspacing='0' class='tabla'>
	<tr>
		<td class='tipo1_no_fondo'>Descuentos por articulos</td>
		<td class='tipo1'>Dto % del articulo</td>
		<td class='tipo1'>Neto del articulo</td>
		<td class='tipo1'>Reduccion precio unidad</td>
		<td class='tipo1'>Regalo de unidades</td>
	</tr>";
				$cons2 = "select $tabla_ofertas_fam_art.*, $tabla_articulos.nombre from $tabla_ofertas_fam_art join $tabla_articulos on $tabla_articulos.id=$tabla_ofertas_fam_art.articulo_id where prov_oferta_familia_id='".$lin['id']."' order by $tabla_articulos.nombre;";
				$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
				while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
				{
					$mensaje_oferta .= "
	<tr>
		<td class='tipo1'>$lin2[nombre]</td>
		<td class='tipo2'>";
					if ($lin2['dto_porc_oferta_art'] > 0)
					{
						$mensaje_oferta .= number_format($lin2['dto_porc_oferta_art'],2,",",".")." %";
					}
					$mensaje_oferta .= "</td>
		<td class='tipo2'>";
					if ($lin2['dto_porc_oferta_art'] > 0)
					{
						$mensaje_oferta .= number_format($lin2['dto_neto_oferta_art'],2,",",".")." &euro;";
					}
					$mensaje_oferta .= "</td>
		<td class='tipo2'>";
					if ($lin2['min_unidades_dto'] > 0 && $lin2['precio_unidad_dto'] > 0)
					{
						$mensaje_oferta .= "Comprando $lin2[min_unidades_dto] unidades, la unidad sale a ".number_format($lin2['precio_unidad_dto'],2,",",".")." &euro;";
					}
					$mensaje_oferta .= "</td>
		<td class='tipo2'>";
					if ($lin2['min_unidades_gratis'] > 0 && $lin2['num_unidades_gratis'] > 0)
					{
						$mensaje_oferta .= "Comprando $lin2[min_unidades_gratis] unidades, le regalamos $lin2[num_unidades_gratis]";
					}
					$mensaje_oferta .= "</td>
	</tr>";
				}
				$mensaje_oferta .= "
</table>";
			} // fin if $num_articulos
		}
	}
	
	$mensaje_oferta = str_replace("class='tabla'", $oferta_estilo_tabla, str_replace("class='texto'", $oferta_estilo_texto, str_replace("class='tipo2'", $oferta_estilo2, str_replace("class='tipo2_top'", $oferta_estilo2_top, str_replace("class='tipo1'", $oferta_estilo1, str_replace("class='tipo1_no_fondo'", $oferta_estilo1_no_fondo, $mensaje_oferta))))));
	
	$contenido_imagenes = "";
	$cons = "select * from $tabla_ofertas_imagenes where prov_oferta_id='".$valor_oferta."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$contenido_imagenes = str_replace('src="'.$fckeditor_camino_relativo,'src="'.$ruta_mailing.$fckeditor_camino_relativo,$lin['contenido']);
	}

	$mensaje_oferta .= $contenido_imagenes;
	return $mensaje_oferta;
}

function DevolverProvCli ($valor_cliente, $tipo_prov)
{
	// $tipo_prov = 1 => clientes_prov_t
	// $tipo_prov = 2 => clientes_prov_potenciales_t
	$array_resultados = array();
	if ($tipo_prov == 1)
	{
		// devuelve los clientes_prov_t.proveedor_id
		$cons = "select distinct proveedor_id from clientes_prov_t where clientes_prov_t.cliente_id='".$valor_cliente."';";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$array_resultados[] = $lin['proveedor_id'];
		}
	}
	elseif ($tipo_prov == 2)
	{
		// devuelve los clientes_prov_potenciales_t.proveedor_id
		$cons = "select distinct proveedor_id from clientes_prov_potenciales_t where clientes_prov_potenciales_t.cliente_id='".$valor_cliente."';";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$array_resultados[] = $lin['proveedor_id'];
		}
	}
	return $array_resultados;
}

function DatosIncidencia ($incidencia_id)
{
	$array_resultados = array();
	$dato_origen = "";
	$dato_destino = "";
	$dato_inicio = "";
	$dato_tipo = "";
	$dato_estado = "";
	$consulta_padre = "select * from incidencias_t where id='".$incidencia_id."';";
	//echo "$consulta_padre";
	$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
	while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
	{
		// fecha y hora inicio
		if ($linea_padre['fecha_inicio'] != "0000-00-00")
		{
			$dato_fecha = $linea_padre['fecha_inicio'];
			if ($linea_padre['hora_inicio'] != "00:00:00")
			{
				$dato_fecha .= " ".$linea_padre['hora_inicio'];
			}
			$dato_inicio = date("d-m-Y H:i:s",strtotime($dato_fecha));
		}
		// origen
		if ($linea_padre['origen_cliente_id'] > 0)
		{
			$consultaselect = "select * from clientes_t where id='$linea_padre[origen_cliente_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_origen = $lineaselect['nombre_corto'];
			}
			$dato_origen = "(C) ".$dato_origen;
		}
		elseif ($linea_padre['origen_proveedor_id'] > 0)
		{
			$consultaselect = "select * from proveedores_t where id='$linea_padre[origen_proveedor_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_origen = $lineaselect['nombre_corto'];
			}
			$dato_origen = "(P) ".$dato_origen;
		}
		elseif ($linea_padre['origen_interna'] == "on")
		{
			$dato_origen = "(I) Interna";
		}
		// destino
		if ($linea_padre['destino_cliente_id'] > 0)
		{
			$consultaselect = "select * from clientes_t where id='$linea_padre[destino_cliente_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_destino = $lineaselect['nombre_corto'];
			}
			$dato_destino = "(C) ".$dato_destino;
		}
		elseif ($linea_padre['destino_proveedor_id'] > 0)
		{
			$consultaselect = "select * from proveedores_t where id='$linea_padre[destino_proveedor_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_destino = $lineaselect['nombre_corto'];
			}
			$dato_destino = "(P) ".$dato_destino;
		}
		elseif ($linea_padre['destino_interna'] == "on")
		{
			$dato_destino = "(I) Interna";
		}
		if ($linea_padre['tipo_incidencia_id'] > 0)
		{
			$consultaselect = "select * from maestro_tipos_incidencias_t where id='$linea_padre[tipo_incidencia_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_tipo = $lineaselect['nombre'];
			}
		}
		if ($linea_padre['estado_inci_id'] > 0)
		{
			$consultaselect = "select * from maestro_estados_inci_t where id='$linea_padre[estado_inci_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_estado = $lineaselect['nombre'];
			}
		}
	}
	
	$array_resultados[] = $dato_origen;
	$array_resultados[] = $dato_destino;
	$array_resultados[] = $dato_inicio;
	$array_resultados[] = $dato_tipo;
	$array_resultados[] = $dato_estado;
	return $array_resultados;
}

function DatosPlanningProv ($valor_dato)
{
$tabla_1 = "prov_planning_t";
	$array_datos = array();
	$consulta_1 = "select * from $tabla_1 where $tabla_1.id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{
		$array_datos[] = $linea_1['proveedor_id'];
		$array_datos[] = $linea_1['fecha_inicio'];
		$array_datos[] = $linea_1['fecha_fin'];
	}
	return $array_datos;
}

?>