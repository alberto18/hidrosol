<?php
// MAESTRO_FORMULARIO: CENTRALIZACION DE LA GENERACION DE FORMULARIOS Y REJILLAS
// VERSION: 2.15
// FECHA: 14/05/2013
// IMPORTANTE: PROCEDIMIENTO DE MODIFICACION DE MAESTRO_FORMULARIO.PHP
// Ante una necesidad de modificar el maestro_formulario, seguiremos los siguientes pasos:
// a) Pensar (analizar), exactamente la modificación que se necesita. Consultar con Mario para que la modificación sea genérica y parametrizable.
// b) Implantar la modificación en el maestro_formulario (y en los index_xxx_new) correspondientes. Enviar a Mario por email las lineas modificadas de cada script junto con los scripts en si. enviar a Mario una url (con las claves) para comprobar su funcionamiento.
// c) Si el resultado es positivo, esa modificación formará parte de maestro_formulario para todos los desarrollos y Mario la enviará en la siguiente versión del TOOLBOX.

// USO: a) Poner el maestro_formulario.php en el raiz del proyecto (al mismo nivel que modules.php)
//      b) En cada index_meta_new.php incluir con include("maestro_formulario.php");

// Elementos de formulario incluidos:
//      text;100   or  text;class="span2"   (100: pixeles)

//      password;100     text;class="span2"   (100: pixeles)

//      textarea;100;50;class="estilo-form"    (ancho, alto en pixeles)
//		Para usar el parametro ckeditor se debe usar la clase: class="ckeditor"
//		1) Colocar ckeditor en la carpeta extras/ckeditor/ (solicitar a Mario la ultima version de este paquete)
//		2) En el header.php (o header_portal.php) insertar la hoja de estilos y el js (cada uno en su zona):
//		     <!-- ckeditor -->
//		     <script src="/extras/ckeditor/samples/ckeditor.js"></script>

//      select;tabla;campo_mostrar;campo_para_value;filtrar_por_noc  (filtrar_por_noc es opcional para proyectos multiempresa)

//      select_join;tabla;campo_mostrar;campo_para_value;filtrar_por_noc;consulta_select_exacta
//          Ejemplo de consulta exacta: select bancos_pagadores_t.id as id, bancos_pagadores_t.num_cuenta as num_cuenta from bancos_pagadores_t left join facturas_t on bancos_pagadores_t.alumno_id=facturas_t.pagador_id where facturas_t.id='.$padre_id_desencript

//      checkbox

//      datetime (Utiliza el datetime de jquery-ui.
//                Requiere tener en variables_globales_gestproject las lineas:
//                <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" />
//                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
//                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

//	datetime2 (un unico campo y con calendario)

//      datetime3 (Formato de fechas con tres campos: dia, mes y ano)

//      file (subida de un fichero a una carpeta)

//      'file_db;8388608;descargar_pre_incidencia;tipo_fichero;peso_fichero;fichero_binario' (subida de un fichero a la base de datos. Los parametros son el tamano maximo del fichero, el nombre del script que va a descargar el fichero, y los nombres de los campos que dejamos como estan)
//        Se deben crear los siguientes campos en la tabla:
//          tipo_fichero text NOT NULL,
//          nombre_fichero text NOT NULL,
//          peso_fichero text NOT NULL,
//          fichero_binario longblob,

//	mostrar_dato_text (mostrar un campo de texto pero que no se modifique (sirve para textarea y text, no para select...). El value de este campo se sigue pasando como un hidden.

// Operaciones incluidas:
//      accion==accioncrear
//      accion==accionmodificar
//      accion==accionborrar
//      accion==form_crear
//      accion==form_modificar
//      accion==form_borrar

// El buscador es dinamico (se genera automaticamente a partir de lineas de configuracion
// en el index_meta_new)
// Ejemplo de configuracion:
// $buscadores[] = "input;nombre|referencia;;;;Buscar por nombre";
        // tipo input para buscar un texto
        // Buscara en los campos definidos entre barras
        // el ultimo campo incluye un texto que aparecera en el input
// $buscadores[] = "select;estado_id;estados_publicacion_t;nombre;id;buscar por estado";
        // busca en estado_id. El select se carga desde estados_publicacion_t
        // el ultimo campo incluye un texto que aparecera en el select
// $buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";
        // tipo checkbox


// Maestro_formulario tambien se puede utilizar en scripts publicos (que no requieren contrasena)
if ($maestro_formulario_publico != 1) { 
  if ($user_id == "") {
    echo "DEBE INICIAR UNA SESION. <a href=http://www.tacnetting.com>Introduzca nuevamente si nombre de usuario y password</a>";
    include ("footer.php");
    exit;
   }
}

// COMIENZA EL SCRIPT
$accion = $_REQUEST['accion'];
$id = $_REQUEST['id'];
$pag = $_REQUEST['pag']; if ($pag == "") { $pag = "0"; }
if ($_REQUEST['campo_order'] != "") { $orderby = " order by $tabla.".$_REQUEST['campo_order']; } else { $orderby = " order by $tabla.$campo_busqueda";  }
$padre_id = $_REQUEST['padre_id'];

// Si las variables en el index_xxx_new modulo_script no esta definida, le asignamos por defecto gestproject
if ($modulo_script == "") { $modulo_script = "gestproject"; }


// Los campos dinamicos (pueden ser cualquier numero) del buscador
// Dependiendo del numero que se pasen, pueden ser buscar_cliente_1, buscar_cliente_2, etc
if ($habilitar_buscador == 1) { 
  $c=1;
  foreach ($buscadores as $buscador) {
     list ($tipo, $campos, $opciones_adicionales) = explode(';', $buscador);
     
        $nombre_campo = "buscar_cliente_".$c;
        if ($_POST[$nombre_campo] != "") { $$nombre_campo = $_POST[$nombre_campo]; } else { $$nombre_campo = $_GET[$nombre_campo]; }
		
		// Para el buscador de fechas
		$nombre_campo_desde_dia = "dia". $nombre_campo . "_desde";
		$nombre_campo_desde_mes = "mes". $nombre_campo . "_desde";
		$nombre_campo_desde_ano = "ano". $nombre_campo . "_desde";

		$nombre_campo_hasta_dia = "dia". $nombre_campo . "_hasta";
		$nombre_campo_hasta_mes = "mes". $nombre_campo . "_hasta";
		$nombre_campo_hasta_ano = "ano". $nombre_campo . "_hasta";
			
        if ($_POST[$nombre_campo_desde_dia] != "") { $$nombre_campo_desde_dia = $_POST[$nombre_campo_desde_dia]; } else { $$nombre_campo_desde_dia = $_GET[$nombre_campo_desde_dia]; }
		if ($_POST[$nombre_campo_desde_mes] != "") { $$nombre_campo_desde_mes = $_POST[$nombre_campo_desde_mes]; } else { $$nombre_campo_desde_mes = $_GET[$nombre_campo_desde_mes]; }
		if ($_POST[$nombre_campo_desde_ano] != "") { $$nombre_campo_desde_ano = $_POST[$nombre_campo_desde_ano]; } else { $$nombre_campo_desde_ano = $_GET[$nombre_campo_desde_ano]; }
		
        if ($_POST[$nombre_campo_hasta_dia] != "") { $$nombre_campo_hasta_dia = $_POST[$nombre_campo_hasta_dia]; } else { $$nombre_campo_hasta_dia = $_GET[$nombre_campo_hasta_dia]; }
		if ($_POST[$nombre_campo_hasta_mes] != "") { $$nombre_campo_hasta_mes = $_POST[$nombre_campo_hasta_mes]; } else { $$nombre_campo_hasta_mes = $_GET[$nombre_campo_hasta_mes]; }
		if ($_POST[$nombre_campo_hasta_ano] != "") { $$nombre_campo_hasta_ano = $_POST[$nombre_campo_hasta_ano]; } else { $$nombre_campo_hasta_ano = $_GET[$nombre_campo_hasta_ano]; }
    
     $c++;
  }
}
// Fin de los campos dinamicos del buscador

if ($padre_id != "") {
 $padre_id_desencript = base64_decode(base64_decode($padre_id));
 $consulta_nombre_padre = str_replace('#PADREID#', $padre_id_desencript, $consulta_nombre_padre);
 $resultadomod = mysql_query($consulta_nombre_padre) or die("La consulta fall&oacute;: $consulta_nombre_padre " . mysql_error()); 
 while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC)) {
   $nombre_padre = "(" . $lineasmod[nombre] . ")";
 }
 
 $insercion_modificacion_padre = " $campo_padre='$padre_id_desencript',  ";
 $filtro_padre = " and $campo_padre='$padre_id_desencript'";
}


if ($accion != "formcrear" && $accion != "formborrar" && $accion != "formmodificar") {
echo "<center><b>$titulo</b> $nombre_padre<br></center>";

if ($padre_id != "") {
    echo "<center><a href=\"".$enlace_volver_a_padre."\">".$texto_volver_a_padre."</a></center>";
}

echo "<table border=0 width=100%><tr><td><div align=left>";

if ($permitir_creacion_de_registros == 1) {
echo "
<a $clase_boton_crear href=modules.php?mod=$modulo_script&file=$script&accion=formcrear&pag=$pag&padre_id=$padre_id>$titulo_boton_crear</a>";
}

echo "
</div>
</td>";

if ($habilitar_buscador == 1) {

    $paso_valores_buscador = "";
    echo "<td><div align=right>
      <form name=form_buscar method=post action=modules.php?mod=$modulo_script&file=$script>
            <input type=hidden name=accion value=buscar>
            <input type=hidden name=padre_id value='$padre_id'>";
          
      $c=1;
      foreach ($buscadores as $buscador) {
         list ($tipo, $campos, $tabla_para_select, $campo_en_tabla, $id_campo_en_tabla, $texto_inicial_busqueda  ) = explode(';', $buscador);
         
         $nombre_campo = "buscar_cliente_".$c;
         
         if ($tipo == "input") {
            echo " <input type=text id=".$nombre_campo." name=".$nombre_campo." value='".$$nombre_campo."' $clase_buscador_input placeholder='$texto_inicial_busqueda'>";
         }

         if ($tipo == "select") {
            $nombre_campo = "buscar_cliente_".$c;
            echo " <select $clase_buscador_select id=".$nombre_campo." name=".$nombre_campo."><option value=''>$texto_inicial_busqueda</option>";
            
            $salida_array = obtener_multiples_campos(array($id_campo_en_tabla,$campo_en_tabla),$tabla_para_select,'','id>0','','order by '.$campo_en_tabla.' asc','');
            foreach ($salida_array as $num_registro => $array_valores) {
                echo "<option value='".$array_valores[$id_campo_en_tabla]."' "; if ($$nombre_campo == $array_valores[$id_campo_en_tabla]) { echo " selected "; } echo ">".$array_valores[$campo_en_tabla]."</option>";
            }
           
            echo "</select>";
         }
         
         if ($tipo == "checkbox") {
            echo " $texto_inicial_busqueda: <input $clase_buscador_checkbox type=checkbox id=".$nombre_campo." name=".$nombre_campo; if ($$nombre_campo == "on") { echo " checked "; } echo ">";
         }
         
         if ($tipo == "intervalo_fechas") {
		 
		 // http://informate.faycanes.es/erpfaycanes/modules.php?mod=gestproject&file=index_gestioncobros_new&pag=0&est_op=1&buscar_cliente_2=2&hoja_ruta=1&diabuscar_cliente_4_desde=03&mesbuscar_cliente_4_desde=05&anobuscar_cliente_4_desde=2013&diabuscar_cliente_4_hasta=03&mesbuscar_cliente_4_hasta=05&anobuscar_cliente_4_hasta=2013
		 
            $nombre_campo_desde_dia = "dia". $nombre_campo . "_desde";
			$nombre_campo_desde_mes = "mes". $nombre_campo . "_desde";
			$nombre_campo_desde_ano = "ano". $nombre_campo . "_desde";

            $nombre_campo_hasta_dia = "dia". $nombre_campo . "_hasta";
			$nombre_campo_hasta_mes = "mes". $nombre_campo . "_hasta";
			$nombre_campo_hasta_ano = "ano". $nombre_campo . "_hasta";
			
            //$nombre_campo_hasta = $nombre_campo . "_hasta";
            echo " $texto_inicial_busqueda. ";
            //echo " Desde: <input name=".$nombre_campo_desde." class='input-medium' >";
            //echo " Hasta: <input name=".$nombre_campo_hasta." class='input-medium' >";
			
			echo " Desde: ";
			echo " <input type=text size=2 id=".$nombre_campo_desde_dia." name=".$nombre_campo_desde_dia." value='".$$nombre_campo_desde_dia."' $clase_buscador_input>/";
			echo " <input type=text size=2 id=".$nombre_campo_desde_mes." name=".$nombre_campo_desde_mes." value='".$$nombre_campo_desde_mes."' $clase_buscador_input>/";
			echo " <input type=text size=4 id=".$nombre_campo_desde_ano." name=".$nombre_campo_desde_ano." value='".$$nombre_campo_desde_ano."' $clase_buscador_input>";
			
			echo " Hasta: ";
			echo " <input type=text size=2 id=".$nombre_campo_hasta_dia." name=".$nombre_campo_hasta_dia." value='".$$nombre_campo_hasta_dia."' $clase_buscador_input>/";
			echo " <input type=text size=2 id=".$nombre_campo_hasta_mes." name=".$nombre_campo_hasta_mes." value='".$$nombre_campo_hasta_mes."' $clase_buscador_input>/";
			echo " <input type=text size=4 id=".$nombre_campo_hasta_ano." name=".$nombre_campo_hasta_ano." value='".$$nombre_campo_hasta_ano."' $clase_buscador_input>";
			
			
			
			
         }
         
         
         $c++;
      }
        
echo "        
        <input $clase_boton_buscar type=submit value=Buscar>
  </form>
  </div>
</td>";

}

echo "</tr>
</table>
";
}

// ACCION CREAR
if ($accion == "accioncrear") {
    
if ($proceso_pre_accioncrear  != "") { include ("$proceso_pre_accioncrear"); }

$consulta2  = "insert into $tabla set $filtro_noc_para_insert $campos_automaticos_para_insert $insercion_modificacion_padre";
// Preparamos los campos a insertar
$c2 = 0;
foreach ($campos_col1 as $campo) {
    list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$c2]);
    if ($ele == "datetime") { $campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo"; $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\",";
    } else if ($ele == "datetime2") {
	list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
	$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
    } else if ($ele == "datetime3") {
        $campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
        $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\",";
    } else if ($ele == "file_db")  {
		if ($_FILES[$campo]['name'] != "")
		{
			// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
			$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
			$posicion_punto = strrpos($nombre_fichero,".");
			if (is_bool($posicion_punto) == false)
			{
				$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
				$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
				
				$binario_peso = $_FILES[$campo]['size']; 
				$binario_tipo = $_FILES[$campo]['type']; 
				
				if ($binario_peso < 1)
				{
					echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tamaño maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
				}
				else
				{
					$consulta2 .= " $campo='$nombre_fichero', $ele4='$binario_tipo', $ele5='$binario_peso', $ele6='$binario_contenido',";
				}
			}
			else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo subido no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
		}
    } else {
       $consulta2 .= " $campo=\"$_POST[$campo]\",";
    }
 $c2++;
}

// Eliminamos la coma final
$consulta2 = substr($consulta2, 0, strlen($consulta2)-1);
$consulta2 .= ';';

$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());
//echo "$consulta2";
echo "<b>Registro creado</b>";

if ($proceso_post_accioncrear  != "") { include ("$proceso_post_accioncrear"); }

}
// FIN ACCION CREAR

// ACCION MODIFICAR 
if ($accion == "accionmodificar") {
    
if ($proceso_pre_accionmodificar  != "") { include ("$proceso_pre_accionmodificar"); }

$consulta2  = "update $tabla set ";
// Preparamos los campos a insertar
$c2 = 0;
foreach ($campos_col1 as $campo) {

    list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$c2]);
    if ($ele == "datetime") { $campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo"; $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\",";
    } else if ($ele == "datetime2") {
	list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
	$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
    }  else if ($ele == "datetime3") {
        $campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
        $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\",";
    } else if ($ele == "file_db") {
		if ($_FILES[$campo]['name'] != "")
		{
			// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
			$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
			$posicion_punto = strrpos($nombre_fichero,".");
			if (is_bool($posicion_punto) == false)
			{
				$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
				$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
				
				$binario_peso = $_FILES[$campo]['size']; 
				$binario_tipo = $_FILES[$campo]['type']; 
				
				if ($binario_peso < 1)
				{
					echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tamaño maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
				}
				else
				{
					$consulta2 .= " $campo='$nombre_fichero', $ele4='$binario_tipo', $ele5='$binario_peso', $ele6='$binario_contenido',";
				}
			}
			else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo subido no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
		}
	} else { 
       $consulta2 .= " $campo=\"$_POST[$campo]\",";
    }
 $c2++;
}

// Eliminamos la coma final
$consulta2 = substr($consulta2, 0, strlen($consulta2)-1);
$id_desencript = base64_decode(base64_decode($_POST[id]));
$consulta2 .= " where id='$id_desencript';";
//echo "$consulta2";


$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());
echo "<b>Registro modificado</b>";



if ($_FILES['userfile']['name'] != "") {
 $nombre_fichero = ereg_replace(' ', '_', $_FILES['userfile']['name']);
 //$uploadfile = $uploaddir_logotipos . $_POST[id]. '_foto.jpg';
 $uploadfile = $uploaddir_logotipos . $id_desencript . '_foto.jpg';
 //echo "subir: $uploadfile<br>". $_FILES['userfile']['name'];

 move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);
 //exit;
}

if ($proceso_post_accionmodificar  != "") { include ("$proceso_post_accionmodificar"); }

echo "
<form name=form_regresar method=get action=modules.php>
<input type=hidden name=mod value='$modulo_script'>
<input type=hidden name=file value='$script'>
<!--
<input type=hidden name=accion value=formmodificar>
<input type=hidden name=id value='$_POST[id]'>
-->
<input type=hidden name=pag value='$pag'>
<input type=hidden name=padre_id value='$padre_id'>
</form>
<script>document.form_regresar.submit();</script>
";



}
// FIN ACCION MODIFICAR 


// ACCION BORRAR
if ($accion == "accionborrar") {
 $id_desencript = base64_decode(base64_decode($_POST[id]));
 $consulta2 = "delete from $tabla where id='$id_desencript';";
 $resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());
}
// FIN ACCION BORRAR

// FORMULARIO PARA BORRAR UN REGISTRO
if ($accion == "formborrar") {
echo "<table width=100%><tr><td colspan=2><center><b>BORRADO DE REGISTRO</b> <a $clase_boton_volver  href=modules.php?mod=$modulo_script&file=$script&padre_id=$padre_id>Volver sin borrar</a></center></td></tr><tr><td valign=top>";

/*
$id_desencript = base64_decode(base64_decode($_GET[id]));

$consulta6 = "select nombre from $tabla where id='$id_desencript';";
//echo "$consulta6";
$resultado6 = mysql_query($consulta6) or die("La consulta fall&oacute;: $consulta6 " . mysql_error());
while ($linea6 = mysql_fetch_array($resultado6, MYSQL_ASSOC)) {
        $nombre_registro = $linea6[nombre];
}
*/

echo "
Est&aacute; seguro que desea borrar el siguiente registro: <i>$nombre_registro</i>?<br>
<form name=form_buscar method=post action=modules.php class='form-horizontal'>
<input type=hidden name=mod value='$modulo_script'>
<input type=hidden name=file value='$script'>
<input type=hidden name=accion value='accionborrar'>
<input type=hidden name=id value='$_GET[id]'>
<input type=hidden name=padre_id value='$padre_id'>
<input type=submit value='Confirmar el borrado del registro' class='btn btn-danger'>
</form>
";
}
// FIN FORMULARIO BORRAR

// FORMULARIO PARA LA CREACION/MODIFICACION DE UN NUEVO REGISTRO
if ($accion == "formcrear" || $accion == "formmodificar") {

echo "
<form name=form_buscar method=post action=modules.php  enctype=multipart/form-data>
<input type=hidden name=mod value='$modulo_script'>
<input type=hidden name=file value='$script'>
<input type=hidden name=pag value='$pag'>
<input type=hidden name=orderby value='$orderby'>
<input type=hidden name=padre_id value='$padre_id'>
";

if ($accion == "formcrear") {
echo "
<input type=hidden name=accion value=accioncrear>
";
}
if ($accion == "formmodificar") {

$id_desencript = base64_decode(base64_decode($_GET[id]));

echo "
<input type=hidden name=id value=$_GET[id]>
<input type=hidden name=accion value=accionmodificar>
";
 // Obtenemos los valores actuales del registro que se esta modificando
 $consultamod = "select * from $tabla where id='$id_desencript'";
 //echo "$consultamod<br>";
 $resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: $consultamod " . mysql_error());
 // El resultado lo metemos en un array asociativo
 $arraymod = array();
 while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC)) {
   foreach ($campos_col1 as $campo) {
    $arraymod[$campo] = $lineasmod[$campo];
   }
 } // del while
}
//print_r($arraymod);

if ($proceso_pre_formcrear  != "" && $accion == "formcrear") { include ("$proceso_pre_formcrear"); }
if ($proceso_pre_formmodificar  != "" && $accion == "formmodificar") { include ("$proceso_pre_formmodificar"); }

$count = 0;

foreach ($campos_col1 as $campo) {

 $campo_obligatorio = "";
 if ($campos_col1_obligatorios[$count] == "on") { $campo_obligatorio = "required"; }
 
 $pattern = "";
 if ($campos_col1_mascaras[$count] != "") { $pattern = $campos_col1_mascaras[$count]; }
 
 $campo_readonly = "";
 if ($campos_col1_readonly[$count] != "") { $campo_readonly = " readonly "; }
 
 list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = split ('[;]', $tipos_col1[$count]);
 
 if ($ele == "text") {
                       
        $tamano = "";
        if (is_integer($ele2)) { $tamano = "style='width:".$ele2."px;'"; } else { $tamano = $ele2; }
                       
        $contenido_plantilla_insercion = str_replace("[$campo]","<input $pattern type=text id=$campo name=$campo $tamano value='$arraymod[$campo]' $campo_obligatorio $campo_readonly>",$contenido_plantilla_insercion);
        
        $campoayuda = "$campo" . "_ayuda";
        $contenido_plantilla_insercion = str_replace("[$campoayuda]","$ayudas_col1[$count]",$contenido_plantilla_insercion);
        
 }
 
 if ($ele == "password") {
    
        $tamano = "";
        if (is_integer($ele2)) { $tamano = "style='width:".$ele2."px;'"; } else { $tamano = $ele2; }
                       
        $contenido_plantilla_insercion = str_replace("[$campo]","<input $pattern type=password id=$campo name=$campo $tamano value='$arraymod[$campo]' $campo_obligatorio $campo_readonly>",$contenido_plantilla_insercion);
        
        $campoayuda = "$campo" . "_ayuda";
        $contenido_plantilla_insercion = str_replace("[$campoayuda]","$ayudas_col1[$count]",$contenido_plantilla_insercion);
        
 }
 
 if ($ele == "textarea") {
                       
        $tamano = "style='width:".$ele2."px;height:".$ele3."px;' $ele4";
        
        $contenido_plantilla_insercion = str_replace("[$campo]","<textarea $tamano $pattern id=$campo name=$campo>$arraymod[$campo]</textarea>",$contenido_plantilla_insercion);
        
        $campoayuda = "$campo" . "_ayuda";
        $contenido_plantilla_insercion = str_replace("[$campoayuda]","$ayudas_col1[$count]",$contenido_plantilla_insercion);
        
 }
 
 if ($ele == "datetime") {
    
        $codigo_datepicker_nuevo = '
	      <script>
              $(document).ready(function () {
		  $(function() {
		    $("#'.$campo.'").datepicker({
			changeMonth: true,
			changeYear: true,
                        dateFormat: "dd/mm/yy"
		    });
		  });
              });
	      </script>
	';
        
        list($fecha, $hora) = split ('[ ]', $arraymod[$campo]);
        list($ano, $mes, $dia) = split ('[-]', $fecha);
        
        if ($dia == 0) { $dia = ""; }
        if ($mes == 0) { $mes = ""; }
        if ($ano == 0) { $ano = ""; }
        
        $valor = "$dia/$mes/$ano";
	if ($valor == "//") { $valor = ""; }
        
	// Version nueva un solo campo con los tres campos en un solo input
	$contenido_plantilla_insercion = str_replace("[$campo]","<input $campo_obligatorio $campo_readonly  type='text' id='$campo' name='$campo' value='$valor' size=9> $codigo_datepicker_nuevo",$contenido_plantilla_insercion);
                
        $campoayuda = "$campo" . "_ayuda";
        $contenido_plantilla_insercion = str_replace("[$campoayuda]","$ayudas_col1[$count]",$contenido_plantilla_insercion);
        
 }
 

if ($ele == "datetime2")
    {
        if ($campo_readonly == "")
        {
                
            $codigo_datepicker_nuevo = '
            <script>
            $(document).ready(function () {
              $(function() {
                $("#'.$campo.'").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd/mm/yy"
                });
              });
            });
            </script>
            ';
        }
        
        list($fecha, $hora) = explode(' ', $arraymod[$campo]);
        list($ano, $mes, $dia) = explode('-', $fecha);
        
        if ($dia == 0) { $dia = ""; }
        if ($mes == 0) { $mes = ""; }
        if ($ano == 0) { $ano = ""; }
        
        $valor = "$dia/$mes/$ano";
        if ($valor == "//") { $valor = ""; }
        
        // Version nueva un solo campo con los tres campos en un solo input
        $contenido_plantilla_insercion = str_replace("[$campo]","<input $campo_obligatorio $campo_readonly type='text' id='$campo' name='$campo' value='$valor' size='9' $ele2> $codigo_datepicker_nuevo",$contenido_plantilla_insercion);
        
        $campoayuda = "$campo" . "_ayuda";
        $contenido_plantilla_insercion = str_replace("[$campoayuda]","$ayudas_col1[$count]",$contenido_plantilla_insercion);
}
 

  if ($ele == "datetime3") {
    
        list($fecha, $hora) = split ('[ ]', $arraymod[$campo]);
        list($ano, $mes, $dia) = split ('[-]', $fecha);
        
        if ($dia == 0) { $dia = ""; }
        if ($mes == 0) { $mes = ""; }
        if ($ano == 0) { $ano = ""; }
        
	$contenido_plantilla_insercion = str_replace("[$campo]","D: <input type=text $campo_obligatorio  $campo_readonly pattern='\d{1,2}' name=dia$campo size=2 value='$dia'>M: <input type=text $campo_obligatorio  $campo_readonly pattern='\d{1,2}' name=mes$campo size=2 value='$mes'>A: <input type=text $campo_obligatorio $campo_readonly pattern='\d{4}' name=ano$campo size=4 value='$ano'>",$contenido_plantilla_insercion);
                                
        $campoayuda = "$campo" . "_ayuda";
        $contenido_plantilla_insercion = str_replace("[$campoayuda]","$ayudas_col1[$count]",$contenido_plantilla_insercion);
        
 }
 
 
 
  if ($ele == "checkbox") {
    
        $checked = "";
        if ($arraymod[$campo] == "on") { $checked = " checked "; }
        
        $contenido_plantilla_insercion = str_replace("[$campo]","<input $pattern type=checkbox class='checkbox' $checked id=$campo name=$campo $campo_obligatorio $campo_readonly>",$contenido_plantilla_insercion);
        
        $campoayuda = "$campo" . "_ayuda";
        $contenido_plantilla_insercion = str_replace("[$campoayuda]","$ayudas_col1[$count]",$contenido_plantilla_insercion);
        
 }
 
 
 if ($ele == "select") {

        $campo_readonly = "";
        if ($campos_col1_readonly[$count] != "") { $campo_readonly = " onchange='this.selectedIndex = 1;' "; }
    
        //filtro noc
        $filtro_noc = "";
        if ($ele6 != "") { $filtro_noc = "and ". $ele6;}
        
        $consultaselect = "select * from $ele2 where id>0 $filtro_noc order by $ele3 asc";
        $valores_select = "";
        $resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: $consultaselect" . mysql_error());
        while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
            $valores_select.= "<option value=$lineaselect[$ele4] "; if ($lineaselect[$ele4] == "$arraymod[$campo]") { $valores_select.= " selected "; } $valores_select.= " >$lineaselect[$ele3]</option>";
        }
        $valores_select.= "</select>";
        
        $contenido_plantilla_insercion = str_replace("[$campo]","$localizador <select class='select' $campo_obligatorio $campo_readonly name=$campo id=$campo type=select style='width:$ele16;'><option></option>$valores_select</select> ",$contenido_plantilla_insercion);
        
      
        $campoayuda = "$campo" . "_ayuda";
        $contenido_plantilla_insercion = str_replace("[$campoayuda]","$ayudas_col1[$count]",$contenido_plantilla_insercion);
        
  } // del tipo select

 
 if ($ele == "select_join") {

        $campo_readonly = "";
        if ($campos_col1_readonly[$count] != "") { $campo_readonly = " onchange='this.selectedIndex = 1;' "; }
    
        //filtro noc
        $filtro_noc = "";
        if ($ele6 != "") { $filtro_noc = "and ". $ele6;}
        
        //$consultaselect = "select * from $ele2 where id>0 $filtro_noc order by $ele3 asc";
        $consultaselect = "$ele7";
        $valores_select = "";
        $resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: $consultaselect" . mysql_error());
        while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
            $valores_select.= "<option value=$lineaselect[$ele4] "; if ($lineaselect[$ele4] == "$arraymod[$campo]") { $valores_select.= " selected "; } $valores_select.= " >$lineaselect[$ele3]</option>";
        }
        $valores_select.= "</select>";
        
        $contenido_plantilla_insercion = str_replace("[$campo]","$localizador <select class='select' $campo_obligatorio $campo_readonly name=$campo id=$campo type=select style='width:$ele16;'><option></option>$valores_select</select> ",$contenido_plantilla_insercion);
        
      
        $campoayuda = "$campo" . "_ayuda";
        $contenido_plantilla_insercion = str_replace("[$campoayuda]","$ayudas_col1[$count]",$contenido_plantilla_insercion);
        
  } // del tipo select
  

 if ($ele == "file") {
                                             
        $contenido_plantilla_insercion = str_replace("[$campo]","<input type=hidden name=MAX_FILE_SIZE value=90000000 /><input name=userfile type=file />",$contenido_plantilla_insercion);
        
        $campoayuda = "$campo" . "_ayuda";
        $contenido_plantilla_insercion = str_replace("[$campoayuda]","$ayudas_col1[$count]",$contenido_plantilla_insercion);
        
 } // del tipo file
 
 if ($ele == "file_db")	{
    
        $texto_replace = "<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"".$ele2."\" /><input name=\"$campo\" type=\"file\" /><br><span style='font-size:10px;'>Tama&ntilde;o m&aacute;ximo: ".InfoTamanoFichero($ele2)."</span>";
        if ($accion == "formmodificar" && $arraymod[$campo] != "")
        {
                $texto_replace .= "<br>Archivo existente: <a target=new href='".$ele3.".php?id=".$_GET['id']."'>".$arraymod[$campo]."</a>";
        }
        $contenido_plantilla_insercion = str_replace("[$campo]",$texto_replace,$contenido_plantilla_insercion);
        
        $campoayuda = "$campo" . "_ayuda";
        $contenido_plantilla_insercion = str_replace("[$campoayuda]","$ayudas_col1[$count]",$contenido_plantilla_insercion);
        
 } // del tipo file_db
 
 if ($ele == "mostrar_dato_text") {
        $contenido_plantilla_insercion = str_replace("[$campo]","$arraymod[$campo]<input type='hidden' id='$campo' name='$campo' value='$arraymod[$campo]'>",$contenido_plantilla_insercion);
        
        $campoayuda = "$campo" . "_ayuda";
        $contenido_plantilla_insercion = str_replace("[$campoayuda]","$ayudas_col1[$count]",$contenido_plantilla_insercion);
        
 } // del tipo mostrar_dato_text
        
 $count++;
} // del foreach

echo $contenido_plantilla_insercion;

echo "<input type=submit value=Guardar $clase_boton_guardar >";

if ($ocultar_botones_volver_sin_cambios != 1) {
	echo "<a $clase_boton_volver  href='modules.php?mod=$modulo_script&file=$script&pag=0&padre_id=$padre_id'>Volver sin cambios</a>";
}

echo "</form>";
}
// FIN FORMULARIO CREACION DE UN NUEVO REGISTRO


// OBTENEMOS EL LISTADO DE REGISTROS
if ($visualizar_listado == 1) {

	if ($proceso_pre_listado  != "") { include ("$proceso_pre_listado"); }
    // Comienzo filtro del buscador
    // if ($buscar_cliente != "") { $filtro_buscar = " and $campo_busqueda like '%$buscar_cliente%' "; }
    $filtro_buscar = "";
    if ($habilitar_buscador == 1) { 
      $c=1;
      foreach ($buscadores as $buscador) {
         list ($tipo, $campos, $opciones_adicionales  ) = explode(';', $buscador);
         $nombre_campo = "buscar_cliente_".$c;
		 
         if ($tipo == "input") {
            if ($$nombre_campo != "") {
                $filtro_buscar .= " and (";
				$condicion_input = "";
                $campos_individuales = explode ('|', $campos);
				// Vamos a trocear lo escrito por espacio en blanco para buscar con un OR
				$trozos = array();
				$trozos = explode(" ",$$nombre_campo);
				foreach ($trozos as $valor) {
					foreach ($campos_individuales as $campo_individual) {
						//$filtro_buscar .= " $campo_individual like '%".$$nombre_campo."%' or"; 
						if ($condicion_input != "") { $condicion_input .= " or "; }
						$condicion_input .= "$campo_individual like '%$valor%'";
					}					
				} // fin foreach trozos
                $filtro_buscar .= $condicion_input.")";
				$paso_valores_buscador_para_primera_fila_tabla .= "&".$nombre_campo."=".$$nombre_campo;
            }
         }
         
         if ($tipo == "select") {
            if ($$nombre_campo != "") {
                $filtro_buscar .= " and ($campos='".$$nombre_campo."')";
				$paso_valores_buscador_para_primera_fila_tabla .= "&".$nombre_campo."=".$$nombre_campo;
            }
         }

         if ($tipo == "checkbox") {
            if ($$nombre_campo != "") {
                $filtro_buscar .= " and ($campos='".$$nombre_campo."')";
				$paso_valores_buscador_para_primera_fila_tabla .= "&".$nombre_campo."=".$$nombre_campo;
            }
         }

         if ($tipo == "intervalo_fechas") {
            if ($$nombre_campo_desde_dia != "") {
                $filtro_buscar .= " and ($campos>='".$$nombre_campo_desde_ano."-".$$nombre_campo_desde_mes."-".$$nombre_campo_desde_dia."')";
				$paso_valores_buscador_para_primera_fila_tabla .= "&".$nombre_campo_desde_dia."=".$$nombre_campo_desde_dia."&".$nombre_campo_desde_mes."=".$$nombre_campo_desde_mes."&".$nombre_campo_desde_ano."=".$$nombre_campo_desde_ano;
            }
			
            if ($$nombre_campo_hasta_dia != "") {
                $filtro_buscar .= " and ($campos<='".$$nombre_campo_hasta_ano."-".$$nombre_campo_hasta_mes."-".$$nombre_campo_hasta_dia."')";
				$paso_valores_buscador_para_primera_fila_tabla .= "&".$nombre_campo_hasta_dia."=".$$nombre_campo_hasta_dia."&".$nombre_campo_hasta_mes."=".$$nombre_campo_hasta_mes."&".$nombre_campo_hasta_ano."=".$$nombre_campo_hasta_ano;
            }
		}         
		 
		 
		 
         $c++;
        
      }
    }
    //echo "$nombre_campo: ". $buscar_cliente_1 ;        
    // Fin filtro del buscador
    
    $string_para_select = "";
    echo "<table width=100% $clase_tabla_listado>
    <thead><tr>";
    $c=0;
    foreach ($campos_listado as $campo) {
     if ($campo != "id") {
       echo "<th scope='col'><a href='modules.php?mod=".$modulo_script."&file=".$script."&campo_order=$campo&pag=$pag&padre_id=$padre_id".$paso_valores_buscador_para_primera_fila_tabla."'><b>$nombres_listado[$c]</b></a></th>";
     }
     $string_para_select .= "$tabla.$campo,";
     $c++;
    }
    // Eliminamos el ultimo caracter
    $string_para_select = substr($string_para_select,0,-1);
    
    echo "<th colspan=2><b>Acciones</b></th>";
    echo "</tr></thead><tbody>";
   
    $consulta  = "select $string_para_select from $tabla $left_join_inicial where $tabla.id>0 $filtro_noc_para_listado $filtro_buscar $filtro_padre $filtros_iniciales";
        
    // Vemos si existe un orderby
    if ($pag != "") {
            // la primera pagina es la 0
            $inicio = $pag*$registros_por_pagina;
            $limit = " limit $inicio,$registros_por_pagina";
    }
    else { $limit = ""; }
    $consulta .= "$orderby $limit;";
            
	// Fin del color
    echo "$consulta";
    //@mysql_query("SET NAMES 'latin1'");
    $resultado = mysql_query($consulta) or die("La consulta fall&oacute;: " . mysql_error());
    while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC)) {
	
	  // Color de los registros de la rejilla
	  $bgcolor_registro = "";
	  if ($nombre_funcion_bgcolor_por_registro != "") {
		$bgcolor_registro = $nombre_funcion_bgcolor_por_registro($linea['id']);
	  }
	
      echo "<tr bgcolor='$bgcolor_registro'>";
      $c = 0;
      foreach ($campos_listado as $campo)
      {
          if ($campo != "id") {
              $nombre = "";
              list ($ele, $ele2, $ele3, $ele4, $ele5) = split ('[;]', $campos_listado_decod[$c]);
              if ($ele != 'si') {
                      $nombre = "$linea[$campo]";
              } else if ($ele2 == "datetime") {
                              list($fecha, $reloj) = split('[ ]', $linea[$campo]);
                              list($ano,$mes,$dia) = split('[-]',$fecha);
                              $nombre = "$dia-$mes-$ano";
			  } elseif ($ele2 == "checkbox") {
					($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
              } else if ($linea[$campo] != "") {

                                      $consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
                                      //echo "$consultaselect";
                                      $resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
                                      while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
                                      {
                                              $nombre = $lineaselect[$ele3];
                                      }
			  }
              echo "<td><b>$nombre</b></td>";
          } // del if de id
          $c++;
      }
    
     $id_encript = base64_encode(base64_encode($linea[id]));
     //echo "$linea[id] -> $id_encript<br>";
    
      echo '<td class="action">';
	  if ($nombre_funcion_acciones_por_registro == "" || function_exists($nombre_funcion_acciones_por_registro) === false) {
	    // 1. MODO GENERICO
        $d = 0;
        foreach ($acciones_por_registro as $acciones_por_registro_unico) {
            $acciones_por_registro_unico = str_replace('#ID#', $id_encript, $acciones_por_registro_unico);
            $acciones_por_registro_unico = str_replace('#PADREID#', $padre_id, $acciones_por_registro_unico);
            
            // Las opciones o botones se pueden filtrar en funcion del registro exacto que se este sacando
            $visualizar_opcion = 1;
            if ($condiciones_visibilidad_por_registro[$d] != "") {
                $condiciones_visibilidad_por_registro[$d] = str_replace('#ID#', $linea[id], $condiciones_visibilidad_por_registro[$d]);
                $resultadoselect = mysql_query($condiciones_visibilidad_por_registro[$d]) or die("9 La consulta fall&oacute;: " . mysql_error());
                while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
                    {
                            $visualizar_opcion = $lineaselect[visualizar_opcion];
                    }
            }
            
            if ($visualizar_opcion == 1) {
               echo "$acciones_por_registro_unico ";
            }
            $d++;
        } 
	  } else {
        // 2. MODO PERSONALIZADO. Llamamos a la funcion definida en el index_xxx_new
		$nombre_funcion_acciones_por_registro($linea['id']);
	  }
	
      echo '</td></tr>';
    } 
    echo "</tbody></table>";
    
	// Paginado
	if ($pag != "")
	{
		// Parametros de los enlaces del paginado
		$parametros_paginado = "";
		if ($paso_valores_buscador_para_primera_fila_tabla != "") { $parametros_paginado .= $paso_valores_buscador_para_primera_fila_tabla; }
		if ($_REQUEST['orderby'] != "") { $parametros_paginado .= "&orderby=".$_REQUEST['orderby']; }
		if ($padre_id != "") { $parametros_paginado .= "&padre_id=".$padre_id; }
		// Visualizamos las paginas existentes
		$consulta2  = "select count($tabla.id) as num from $tabla where id>0 $filtro_noc_para_listado $filtro_padre;";
		$resultado2 = mysql_query($consulta2) or die("$consulta2, La consulta fall&oacute;: " . mysql_error());
		//echo "$consulta2<br>";
		while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC))
		{
			$exp = $linea2['num'];
		}
		if ($exp > $registros_por_pagina)
		{
			$nombre_parametro_pagina = "pag";
			//if ($nombre_alt_pag != "") { $nombre_parametro_pagina = $nombre_alt_pag; }
			$contenido_paginado = "
<table width='100%' cellpadding='2' cellspacing='3' border='0'>
	<tr align='center'>
		<td>";
			$total_pag_inicial = $exp/$registros_por_pagina;
			list($total_pag, $decimales) = explode('.', $total_pag_inicial);
			if ($decimales > 0) { $total_pag++; }
			$pag_anterior = $pag-1;
			if ($pag > 0)
			{
				// ir a pagina anterior
				$contenido_paginado .= "<a href='modules.php?mod=".$modulo_script."&file=".$script."&".$nombre_parametro_pagina."=$pag_anterior"."$parametros_paginado' ".$clase_paginado.">&laquo;</a> ";
			}
			$numero_pag = -1;
			$numero_pag_visual = 0;
			while ($exp > 0)
			{
				$numero_pag++;
				$numero_pag_visual++;
				if ($numero_pag == $pag)
				{
					$contenido_paginado .= "<b>[<a href='modules.php?mod=".$modulo_script."&file=".$script."&".$nombre_parametro_pagina."=$numero_pag"."$parametros_paginado' ".$clase_paginado.">$numero_pag_visual</a>]</b> ";
				}
				else
				{
					if ($paginado_reducido == 0 || ($paginado_reducido == 1 && ($total_pag <= 5 || ($total_pag > 5 && in_array($numero_pag,array(0,$pag-1,$pag,$pag+1,$total_pag-1))))))
					{
						$contenido_paginado .= "[<a href='modules.php?mod=".$modulo_script."&file=".$script."&".$nombre_parametro_pagina."=$numero_pag"."$parametros_paginado' ".$clase_paginado.">$numero_pag_visual</a>] ";
					}
				}
				if ($paginado_reducido == 1 && ($total_pag > 5 && (($numero_pag == 0 && $pag != 0 && $pag != 1 && $pag != 2) || ($numero_pag == ($pag+1) && $pag != ($total_pag-3) && $pag != ($total_pag-2) && $pag != ($total_pag-1)))))
				{
					$contenido_paginado .= "...";
				}
				$exp = $exp - $registros_por_pagina;
			}
			$pag_siguiente = $pag+1;
			if ($pag < $numero_pag)
			{
				// ir a pagina siguiente
				$contenido_paginado .= " <a href='modules.php?mod=".$modulo_script."&file=".$script."&".$nombre_parametro_pagina."=$pag_siguiente"."$parametros_paginado' ".$clase_paginado.">&raquo;</a>";
			}
			$contenido_paginado .= "
		</td>
	</tr>
</table>";
			echo $contenido_paginado;
		}
	}
	// Fin paginado
	
	if ($proceso_post_listado  != "") { include ("$proceso_post_listado"); }
	
} // de visualizar_listado
// FIN DE OBTENER EL LISTADO INICIAL



?>


