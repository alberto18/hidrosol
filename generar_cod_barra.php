<?php
require_once "jpgraph/src/jpgraph.php";
require_once "jpgraph/src/jpgraph_canvas.php";
require_once "jpgraph/src/jpgraph_barcode.php";

$code = 8;
$pswidth = 1;
$scale = 1;
$file = '';
$checksum = false;
$notext = false;
$vertical = false;
$showframe = false;
$codificacionbas = false;
$height = 60;
$info = false;

$codigogenerar = "$_GET[codigo_barras]";
//echo "($codigogenerar)";exit;
if (strlen($codigogenerar) == 12)
{
	//hay que a�adir un cero al principio y quitar el ultimo digito eso lo haremos siendo ya de ean 13
	$codigogenerar = "0".$codigogenerar;
}
if (strlen($codigogenerar) == 13)
{
	$codigogenerar = substr($codigogenerar,0,12);
	$codificacionbas = true;
}

if($codificacionbas) { $encoder = BarcodeFactory::Create(ENCODING_EAN13); }
else { $encoder = BarcodeFactory::Create(ENCODING_CODE128); }
$b='IMAGE';
$e = BackendFactory::Create($b,$encoder);

if ($e)
{
	if( $pswidth!='' )
	{
		$modwidth = $pswidth;
	}
	$e->SetModuleWidth($modwidth);
	$e->AddChecksum($checksum);
	$e->NoText($notext);
	$e->SetScale($scale);
	$e->SetVertical($vertical);
	$e->ShowFrame($showframe);
	$e->SetHeight($height);
	$r = $e->Stroke($codigogenerar,$file,$info,$info);
	if ($r)
		echo nl2br(htmlspecialchars($r));
	if( $file != '' )
		echo "<p>Wrote file $file.";
}
else
{
	echo "<h3>Can't create choosen backend: $backend.</h3>";
}
?>
