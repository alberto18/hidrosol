<?php 

// Parametros de conexion
$servername = 'localhost';
$dbname     = 'tacproject_hidrosol';
$port       =  3306;
$username   = 'root';
$password   = 'root';
$charset    = 'utf8';

// Persistencia
class DatabaseManagerErp {

    private $conn;
    private $servername;
    private $dbname;
    private $port;
    private $username;
    private $password;
    private $charset;

    /**
     * DatabaseManager constructor.
     */
    public function __construct($servername, $dbname, $port, $username, $password, $charset = 'utf8' )
    {
        $this->servername = $servername;
        $this->dbname = $dbname;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->charset = $charset;
		
        try {
            //;charset=$this->charset
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname;port=$this->port", $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (\Exception $e) {
            echo "Connection failed: {$e->getMessage()}";
            die();
        }

    }

    public function getConnection()
    {
        return $this->conn;
    }

} // Fin DatabaseManager

// Conexion
$databaseManager = new DatabaseManagerErp( $servername,
                                        $dbname,
                                        $port,
                                        $username,
                                        $password,
                                        $charset
);

$conn_pdo_erp = $databaseManager->getConnection();
?>
