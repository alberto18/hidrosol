<?php

function compress_image($source_url, $destination_url, $quality) {
	$info = getimagesize($source_url);
 
	if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
	elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
	elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
 
	//save file
	imagejpeg($image, $destination_url, $quality);
 
	//return destination file
	return $destination_url;
}
 
 
 
// http://casiopea.tac7.es/portal_zca_app/modules/gestproject/json_actividades.php
$db = "tacgestorcontenidos_modacalida";
$usuario_db = "root";
$passwd_db = "root";

$enlace_db_web = mysql_connect("localhost", $usuario_db, $passwd_db)
   or die("No pudo conectarse : " . mysql_error());
//echo "Conexi&oacute;n exitosa";
mysql_select_db($db) or die("No pudo seleccionarse la BD.");

$salida = array();
$meses = array('Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic');

if ($_REQUEST[fecha_inicio] != "") { $filtro_fecha_inicio = " and fecha_inicio>='$_REQUEST[fecha_inicio] 00:00:00' "; }

// ATENCION COMENTADO EL FILTRO DE FECHA FINAL.
// TENEMOS QUE BUSCAR OTRA FORMA DE QUITAR LAS QUE YA ESTAN VENCIDAS

//if ($_REQUEST[fecha_final] != "") { $filtro_fecha_final = " and fecha_fin>='$_REQUEST[fecha_final] 23:59:59' "; }

if ($_REQUEST[fecha_final] != "") { $filtro_fecha_final = " and fecha_fin>=now()"; }


if ($_REQUEST[zca] != "") { $filtro_zca = " and zca_id='$_REQUEST[zca]' "; }
if ($_REQUEST[id] != "") { $filtro_id = " and id='$_REQUEST[id]' "; }

// Filtro por proximos_eventos: 1 (7 dias en adelante), 2 (30 dias en adelante), 3 (todos los eventos en el futuro)
$dia_hoy = date('Y').'-'.date('m').'-'.date('d');

if ($_REQUEST[proximos_eventos] == "1") { 
	$dia_final = date('Y-m-d', strtotime("+1 week"));
	$filtro_proximos_eventos = " and ((fecha_inicio>='$dia_hoy 00:00:00' and fecha_inicio<='$dia_final 23:59:59') or (fecha_fin>='$dia_hoy 00:00:00' and fecha_fin<='$dia_final 23:59:59')) ";
}

if ($_REQUEST[proximos_eventos] == "2") { 
	$dia_final = date('Y-m-d', strtotime("+1 month"));
	$filtro_proximos_eventos = " and ((fecha_inicio>='$dia_hoy 00:00:00' and fecha_inicio<='$dia_final 23:59:59') or (fecha_fin>='$dia_hoy 00:00:00' and fecha_fin<='$dia_final 23:59:59')) ";
}

if ($_REQUEST[proximos_eventos] == "3") { 
	//$dia_final = date('Y-m-d', strtotime("+1 month"));
	$filtro_proximos_eventos = " and ((fecha_inicio>='$dia_hoy 00:00:00') or (fecha_fin>='$dia_hoy 00:00:00')) ";
}


if ($_REQUEST[tipo_producto] != "") { 
	//$dia_final = date('Y-m-d', strtotime("+1 month"));
	$filtro_tipo_producto = " and (tipo_producto_id='$_REQUEST[tipo_producto]') ";
}

//$cons = "select * from zca_actividades_t where publicado=1 and tipo_actividad_id=11 $filtro_fecha_inicio $filtro_fecha_final $filtro_zca $filtro_id $filtro_proximos_eventos $filtro_tipo_producto order by fecha_inicio;";

//$cons = "select * from zca_actividades_t where publicado=1 and tipo_actividad_id=11 $filtro_fecha_final $filtro_zca $filtro_id $filtro_proximos_eventos $filtro_tipo_producto order by fecha_inicio;";

$cons = "select * from firmas_t;";

//echo $cons ."<br>";

$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {

	/*
   $nombre_fichero = "";
   $url_imagen = "";
   if ($lin[nombre_fichero] != "") {
	//$nombre_fichero = "<img width=90% src=http://casiopea.tac7.es/portal_zca_app/documentos/" . $lin[nombre_fichero] .">";
	$nombre_fichero = "<img width=90% src=http://casiopea.tac7.es//portal_zca_app/timthumb.php?src=http://casiopea.tac7.es/portal_zca_app/documentos/" . $lin[nombre_fichero] ."&w=200>";
	$url_imagen = "http://casiopea.tac7.es/portal_zca_app/documentos/" . $lin[nombre_fichero];
   }
   
   // Obtenemos los datos de la zca
   $cons2 = "select * from zca_t where id = '$lin[zca_id]'";
   $res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons " . mysql_error());
   while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC)) {
	   $nombre_zca = $lin2['nombre'];
	   $lon_lat = $lin2['lon_lat'];
	   $nucleo_tradicional = $lin2['nucleo_tradicional'];
	   $centro_historico = $lin2['centro_historico']; 
	   $nucleo_litoral = $lin2['nucleo_litoral'];
	   $ciudad_turistica = $lin2['ciudad_turistica'];
	   $eje = $lin2['eje'];
   }
   
   // Si existe una fecha_fin distinta a la fecha_inicio, ponemos un rango
   // Lo enviamos en nombre_zca


   if ($lin[fecha_fin] != $lin[fecha_inicio]) {
		list ($fecha_fin, $hora) = explode (' ', $lin[fecha_fin]);
		list ($ano_fin, $mes_fin, $dia_fin) = explode('-', $fecha_fin);
		
		list ($fecha_inicio, $hora) = explode (' ', $lin[fecha_inicio]);
		list ($ano, $mes, $dia) = explode('-', $fecha_inicio);
	
		$nombre_zca .= "<br><font color=green>Evento continuo. Desde $dia/$mes/$ano hasta $dia_fin/$mes_fin/$ano_fin </font> "; 
   }
   
   // Obtenemos los datos del tipo de actividad
   $cons2 = "select * from zca_tipo_actividades_t where id = '$lin[tipo_actividad_id]'";
   $res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons " . mysql_error());
   while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC)) {
	   $tipo_actividad = $lin2['nombre'];
   }
   
   
   
   
   list($fecha_inicio, $n) = explode (' ', $lin[fecha_inicio]);
   list ($fecha_inicio_ano, $fecha_inicio_mes, $fecha_inicio_dia) = explode('-', $fecha_inicio);
   $mes_inicio_entero = $fecha_inicio_mes - 1;
   $fecha_inicio_mes_textual = $meses[$mes_inicio_entero];
   
   if ($lin[likes] == "") {$likes = "0"; } else { $likes = $lin[likes]; }
   
   // Obtenemos los datos del comercio
   $cons2 = "select * from zca_comercios_t where id = '$lin[comercio_id]'";
   $res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons " . mysql_error());
   while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC)) {
	   $nombre_comercio = $lin2['nombre'];
	   $direccion = $lin2['direccion'];
	   $tlf = $lin2['tlf'];
	   $email = $lin2['email']; 
	   $horario = $lin2['horario'];
	   $web = $lin2['web'];
   }
   */
   

   $salida[] = array("id" => "$lin[id]", "nombre" => "$lin[nombre]", "nombre_disenador" => "$lin[nombre_disenador]", "email" => "$lin[email]", "tlf" => "$lin[tlf]", "facebook" => "$lin[facebook]", "twitter" => "$lin[twitter]", "puntos_venta_fisicos_lonlat" => "$lin[puntos_venta_fisicos_lonlat]");
}
//print_r($salida);
echo json_encode($salida);
//echo "FIN";
?>