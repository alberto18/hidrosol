<?php
//include("abrir_db.php");
include("variables_globales_gestproject.php");
include("funciones.php");

$fecha_hoy = date("Y-m-d");

$tabla_reservas = "stock_reserva_t";
$tabla_presupuestos = "presupuestos_t";
$tabla_pre_art = "pre_articulos_t";
$tabla_pre_portes = "pre_portes_t";
$tabla_stock = "stock_t";
$tabla_clientes_direcciones = "clientes_direcciones_t";
$tabla_ped_art = "ped_articulos_t";
$tabla_pedidos = "pedidos_t";

// reservas en presupuestos
$cons1 = "select * from $tabla_pre_art where $tabla_pre_art.reservado='on' and $tabla_pre_art.fecha_max_reserva<'".$fecha_hoy."';";
//echo "$cons1<br>";
$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
{
	$cons2 = "update $tabla_pre_art set reservado='' where id='".$lin1['id']."';";
	//echo "cons2: $cons2<br>";
	$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2" . mysql_error());
	// se devuelven las unidades al almacen
	$cons2 = "select * from $tabla_reservas where $tabla_reservas.pre_articulo_id='".$lin1['id']."';";
	$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
	while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
	{
		// se busca el articulo en el stock para insertar o actualizar
		$existe_stock = 0;
		$unidades_stock = 0;
		$cons3 = "select * from $tabla_stock where articulo_id='".$lin2['articulo_id']."' and almacen_id='".$lin2['almacen_id']."' and proveedor_id='".$lin2['proveedor_id']."' and estanteria='".$lin2['estanteria']."' and hueco='".$lin2['hueco']."' and piso='".$lin2['piso']."' and palet='".$lin2['palet']."';";
		//echo "cons3: $cons3<br>";
		$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
		while ($lin3 = mysql_fetch_array($res3, MYSQL_ASSOC))
		{
			$existe_stock = $lin3['id'];
			$unidades_stock = $lin3['unidades'];
		}
		
		if ($existe_stock == 0)
		{
			// no existe ese articulo => se crea
			$cons3 = "insert into $tabla_stock set articulo_id='".$lin2['articulo_id']."', unidades='".$lin2['unidades']."', almacen_id='".$lin2['almacen_id']."', proveedor_id='".$lin2['proveedor_id']."', estanteria='".$lin2['estanteria']."', hueco='".$lin2['hueco']."', piso='".$lin2['piso']."', palet='".$lin2['palet']."';";
			//echo "cons3: $cons3<br>";
			$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
		}
		else
		{
			$nuevas_unidades = $unidades_stock + $lin2['unidades'];
			$cons3 = "update $tabla_stock set unidades='".$nuevas_unidades."' where id='".$existe_stock."';";
			//echo "cons3: $cons3<br>";
			$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3" . mysql_error());
		}
		
		// se borra el registro de la reserva
		$cons4 = "delete from $tabla_reservas where id='".$lin2['id']."';";
		//echo "cons4: $cons4<br>";
		$res4 = mysql_query($cons4) or die("La consulta fall&oacute;: $cons4 " . mysql_error());
	}
	
	// se recalculan los portes
	RecalcularPortesPresupuesto ($lin1['presupuesto_id']);
	// se actualizan los importes en el presupuesto
	$array_resul = CalcularPresupuesto($lin1['presupuesto_id']);
	$importe_sin_igic = $array_resul[0];
	$importe_igic = $array_resul[1];
	$importe_total = $array_resul[2];
	$consulta_presupuesto2 = "update $tabla_presupuestos set importe_sin_igic=\"".$importe_sin_igic."\", importe_igic=\"".$importe_igic."\", importe_total=\"".$importe_total."\" where id='".$lin1['presupuesto_id']."';";
	//echo "$consulta_presupuesto2<br>";
	$res_pedido2 = mysql_query($consulta_presupuesto2) or die("$consulta_presupuesto2, La consulta fall�: " . mysql_error());
} // fin while $lin1

/*
// las reservas en pedidos (stock_reservado_t) estan confirmadas por los clientes asi que no se tocan mas hasta el albaran

// reservas en pedidos
$cons1 = "select * from $tabla_ped_art where $tabla_ped_art.reservado='on' and $tabla_ped_art.fecha_max_reserva<'".$fecha_hoy."';";
//echo "$cons1<br>";
$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
{
	$cons2 = "update $tabla_ped_art set reservado='' where id='".$lin1['id']."';";
	//echo "cons2: $cons2<br>";
	$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2" . mysql_error());
	// se devuelven las unidades al almacen
	$cons2 = "select * from $tabla_reservas where $tabla_reservas.ped_articulo_id='".$lin1['id']."';";
	$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
	while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
	{
		// se busca el articulo en el stock para insertar o actualizar
		$existe_stock = 0;
		$unidades_stock = 0;
		$cons3 = "select * from $tabla_stock where articulo_id='".$lin2['articulo_id']."' and almacen_id='".$lin2['almacen_id']."' and proveedor_id='".$lin2['proveedor_id']."' and estanteria='".$lin2['estanteria']."' and hueco='".$lin2['hueco']."' and piso='".$lin2['piso']."' and palet='".$lin2['palet']."';";
		//echo "cons3: $cons3<br>";
		$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
		while ($lin3 = mysql_fetch_array($res3, MYSQL_ASSOC))
		{
			$existe_stock = $lin3['id'];
			$unidades_stock = $lin3['unidades'];
		}
		
		if ($existe_stock == 0)
		{
			// no existe ese articulo => se crea
			$cons3 = "insert into $tabla_stock set articulo_id='".$lin2['articulo_id']."', unidades='".$lin2['unidades']."', almacen_id='".$lin2['almacen_id']."', proveedor_id='".$lin2['proveedor_id']."', estanteria='".$lin2['estanteria']."', hueco='".$lin2['hueco']."', piso='".$lin2['piso']."', palet='".$lin2['palet']."';";
			//echo "cons3: $cons3<br>";
			$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
		}
		else
		{
			$nuevas_unidades = $unidades_stock + $lin2['unidades'];
			$cons3 = "update $tabla_stock set unidades='".$nuevas_unidades."' where id='".$existe_stock."';";
			//echo "cons3: $cons3<br>";
			$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3" . mysql_error());
		}
		
		// se borra el registro de la reserva
		$cons4 = "delete from $tabla_reservas where id='".$lin2['id']."';";
		//echo "cons4: $cons4<br>";
		$res4 = mysql_query($cons4) or die("La consulta fall&oacute;: $cons4 " . mysql_error());
	}
	
	// se recalculan los portes
	RecalcularPortesPedido ($lin1['pedido_id']);
	// se actualizan los importes en el pedido
	$array_resul = CalcularPedido($lin1['pedido_id']);
	$importe_sin_igic = $array_resul[0];
	$importe_igic = $array_resul[1];
	$importe_total = $array_resul[2];
	$consulta_pedido2 = "update $tabla_pedidos set importe_sin_igic=\"".$importe_sin_igic."\", importe_igic=\"".$importe_igic."\", importe_total=\"".$importe_total."\" where id='".$lin1['pedido_id']."';";
	//echo "$consulta_pedido2<br>";
	$res_pedido2 = mysql_query($consulta_pedido2) or die("$consulta_pedido2, La consulta fall�: " . mysql_error());
} // fin while $lin1
*/

?>