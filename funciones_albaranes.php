<?php
/*
Funciones en el script:

VerAlbaran ($valor_albaran)
CalcularAlbaran ($albaran_id)
*/

function VerAlbaran ($valor_albaran)
{
global $fckeditor_camino_relativo, $ruta_mailing, $oferta_color_texto, $oferta_color_borde, $oferta_estilo_tabla, $oferta_estilo1, $oferta_estilo_texto, $oferta_estilo1_no_fondo, $oferta_estilo2, $oferta_estilo2_top, $presupuesto_estilo_est11, $presupuesto_estilo_est11b, $presupuesto_estilo_est12, $presupuesto_estilo_est12b, $presupuesto_estilo_est13, $presupuesto_estilo_est13b, $presupuesto_estilo_est14, $presupuesto_estilo_est14b, $presupuesto_estilo_est15, $presupuesto_estilo_est15b, $presupuesto_estilo_est16, $presupuesto_estilo_est16b, $presupuesto_estilo_separador, $presupuesto_estilo_logo, $ruta_imagenes;


$tabla_albaranes = "albaranes_t";
$tabla_presupuestos = "presupuestos_t";
$tabla_clientes = "clientes_t";
$tabla_cli_direcciones = "clientes_direcciones_t";
$tabla_agencias_transp = "maestro_agencias_transportes_t";
$tabla_alb_avisos = "alb_avisos_t";
$tabla_alb_art = "alb_articulos_t";
$tabla_m_embalajes = "maestro_tipos_embalajes_t";
$tabla_m_igic = "maestro_igic_t";
$tabla_articulos = "articulos_t";

$articulos_pagina = 15;
	
	$mensaje_albaran = "";
	$consulta_1 = "select * from $tabla_albaranes where id='".$valor_albaran."';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1 La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{
		$consulta_presu = "select * from $tabla_presupuestos where id='".$linea_1['presupuesto_id']."';";
		//echo "$consulta_presu";
		$resultado_presu = mysql_query($consulta_presu) or die("$consulta_presu La consulta fall&oacute;: " . mysql_error());
		while ($linea_presu = mysql_fetch_array($resultado_presu, MYSQL_ASSOC))
		{
			$consulta_2 = "select * from $tabla_clientes where id='".$linea_presu['cliente_id']."';";
			//echo "$consulta_2";
			$resultado_2 = mysql_query($consulta_2) or die("$consulta_2 La consulta fall&oacute;: " . mysql_error());
			$linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC);
			
			$datos_direccion_cliente = "";
			$consulta_dir = "select * from $tabla_cli_direcciones where id='".$linea_1['cliente_direccion_id']."';";
			//echo "$consulta_dir";
			$resultado_dir = mysql_query($consulta_dir) or die("$consulta_dir La consulta fall&oacute;: " . mysql_error());
			while ($linea_dir = mysql_fetch_array($resultado_dir, MYSQL_ASSOC))
			{
				$datos_direccion_cliente = $linea_dir['direccion'];
				if ($linea_dir['direccion'] != "") { $datos_direccion_cliente .= ", "; }
				$datos_direccion_cliente .= $linea_dir['cp'];
				if ($datos_direccion_cliente != "") { $datos_direccion_cliente .= "<br>"; }
				$datos_direccion_cliente .= $linea_dir['poblacion'];
				if ($linea_dir['poblacion'] != "") { $datos_direccion_cliente .= "<br>"; }
				$nombre_provincia = "";
				$cons1_tmp = "select * from maestro_provincias_t where id='".$linea_dir['provincia_id']."';";
				$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp " . mysql_error());
				while ($lin1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
				{
					$nombre_provincia = $lin1_tmp['nombre'];
				}
				$nombre_municipio = "";
				$cons1_tmp = "select * from maestro_municipios_t where id='".$linea_dir['municipio_id']."';";
				$res1_tmp = mysql_query($cons1_tmp) or die("La consulta fall&oacute;: $cons1_tmp " . mysql_error());
				while ($lin1_tmp = mysql_fetch_array($res1_tmp, MYSQL_ASSOC))
				{
					$nombre_municipio = $lin1_tmp['nombre'];
				}
				$datos_direccion_cliente .= $nombre_provincia;
				if ($nombre_provincia != "") { $datos_direccion_cliente .= ", "; }
				$datos_direccion_cliente .= $nombre_municipio;
			}
			$contenido_logo = "<img src=\"images/logotipo_ayala.png\" /><br /><span class=\"est-logo\">S.T.P., S.A.<br>N.I.F. A58822354</span>";
			$cabecera_pagina = "
<table width=\"99%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<!-- inicio cabecera -->
	<tr>
		<td style=\"padding-bottom:3px;\">
		<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
			<tr valign=\"top\">
				<td width=\"45%\" align=\"center\">".$contenido_logo."</td>
				<td width=\"55%\">
				<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
					<tr valign=\"bottom\">
						<td width=\"50%\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"left\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
							<tr align=\"center\">
								<td style=\"border-style:solid; border-width:1px; border-color:#000000;\"><span class=\"est15\">DOCUMENTO</span></td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;<span class=\"est16b\">ALBAR&Aacute;N</span></td>
							</tr>
						</table>
						</td>
						<td width=\"50%\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"right\">
							<tr>
								<td style=\"border-style:solid; border-width:1px; border-color:#000000;\">
								<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
									<tr align=\"center\">
										<td width=\"25%\"><span class=\"est15\">P&aacute;gina:</span></td>
										<td width=\"75%\"><span class=\"est16\">#PAGINA#</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					<tr valign='top'>
						<td style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"left\">
							<tr>
								<td style=\"border-style:solid; border-width:1px; border-color:#000000;\">&nbsp;&nbsp;<span class=\"est15b\">N&uacute;mero:</span> <span class=\"est16\">".$linea_1['numero']."</span></td>
							</tr>
						</table>
						</td>
						<td style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"95%\" align=\"right\" style=\"border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>
								<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
									<tr align=\"center\">
										<td width=\"25%\"><span class=\"est15\">Fecha:</span></td>
										<td width=\"75%\"><span class=\"est16\">".date("d/m/Y",strtotime($linea_1['fecha']))."</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan=\"2\" style=\"padding-top:3px;\">
						<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\" align=\"center\" style=\"border-style:solid; border-width:1px; border-color:#000000;\">
							<tr>
								<td>
								<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">
									<tr>
										<td colspan=\"2\"><span class=\"est15b\">".$linea_2['nombre']."</span></td>
									</tr>
									<tr>
										<td colspan=\"2\"><span class=\"est15\">".$datos_direccion_cliente."&nbsp;</span></td>
									</tr>
									<tr>
										<td width=\"75%\"><span class=\"est15b\">C.I.F./N.I.F.:</span> <span class=\"est15\">".$linea_2['nif']."</span></td>
										<td width=\"25%\"><span class=\"est15b\">&nbsp;</span></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
<!-- fin cabecera -->
</table>";
			$pie_pagina = "
<table width=\"99%\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; border-style:solid; border-width:1px; border-color:#000000;\">
<!-- inicio pie -->
	<tr>
		<td style=\"border-style:solid; border-width:1px; border-color:#000000;\">
		<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" height=\"40\">
			<tr valign='top'>
				<td><span class=\"est15b\">AGENCIA TRANSPORTE:</span> <span class=\"est15\">";
			$consulta_ag_transpor = "select * from $tabla_agencias_transp where id='".$linea_1['agencia_transporte_id']."';";
			//echo "$consulta_ag_transpor";
			$resultado_ag_transpor = mysql_query($consulta_ag_transpor) or die("$consulta_ag_transpor La consulta fall&oacute;: " . mysql_error());
			while ($linea_ag_transpor = mysql_fetch_array($resultado_ag_transpor, MYSQL_ASSOC))
			{
				$pie_pagina .= $linea_ag_transpor['nombre'];
				if ($linea_ag_transpor['telefono_contacto1'] != "")
				{
					$pie_pagina .= ", ".$linea_ag_transpor['telefono_contacto1'];
				}
			}
			$consulta_avisos = "select * from $tabla_alb_avisos where albaran_id='".$valor_albaran."' and agencia_transporte_id='".$linea_1['agencia_transporte_id']."' order by fecha desc, hora desc limit 1;";
			//echo "$consulta_avisos";
			$resultado_avisos = mysql_query($consulta_avisos) or die("$consulta_avisos La consulta fall&oacute;: " . mysql_error());
			while ($linea_avisos = mysql_fetch_array($resultado_avisos, MYSQL_ASSOC))
			{
				$pie_pagina .= " (Ultimo aviso: ".date("d-m-Y H:i",strtotime($linea_avisos['fecha']." ".$linea_avisos['hora'])).")";
			}
			$pie_pagina .= "</span></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
			<tr>
				<td><span class=\"est15b\">OBSERV. PRESUPUESTO:</span> <span class=\"est15\">".$linea_presu['observaciones_publico']."</span></td>
			</tr>
		</table>
		</td>
	</tr>";
/*
			$pie_pagina .= "
	<tr>
		<td style=\"border-style:solid; border-width:1px; border-color:#000000;\">
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
			<tr>
				<td width=\"12%\" align=\"right\"><span class=\"est16b\">Importe Base:</span></td>
				<td width=\"13%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_sin_igic'],2,",",".")." &euro;</span></td>
				<td width=\"12%\" align=\"right\"><span class=\"est16b\">Importe I.G.I.C.:</span></td>
				<td width=\"13%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_igic'],2,",",".")." &euro;</span></td>
				<td width=\"12%\" align=\"right\"><span class=\"est16b\">Importe Total:</span></td>
				<td width=\"13%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_portes'],2,",",".")." &euro;</span></td>
				<td width=\"12%\" align=\"right\"><span class=\"est16b\">Importe Total:</span></td>
				<td width=\"13%\" align=\"center\"><span class=\"est16\">".number_format($linea_1['importe_total'],2,",",".")." &euro;</span></td>
			</tr>
		</table>
		</td>
	</tr>";
*/
			$pie_pagina .= "
<!-- fin pie -->
</table>";
			
			// contabilizar articulos
			$num_arti = 0;
			$consulta_3 = "select count($tabla_alb_art.id) as total from $tabla_alb_art where $tabla_alb_art.albaran_id='".$valor_albaran."';";
			//echo "$consulta_3";
			$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
			while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
			{
				$num_arti = $linea_3['total'];
			}
			$total_paginas = (int) ceil($num_arti/$articulos_pagina);
			//echo "($num_arti)($total_paginas)<br>";
			
			// busqueda de los articulos del presupuesto
			$array_articulos_id = array();
			$consulta_3 = "select $tabla_alb_art.id from $tabla_alb_art where $tabla_alb_art.albaran_id='".$valor_albaran."' order by $tabla_alb_art.id;";
			//echo "$consulta_3";
			$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
			while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
			{
				$array_articulos_id[] = $linea_3['id'];
			}
			//echo print_r($array_articulos_id,true);
			$cuenta_articulos = 0;
			for ($pag = 1; $pag <= $total_paginas; $pag++)
			{
				//$limit = " limit ".(($pag-1)*$articulos_pagina).",$articulos_pagina";
				//echo "($limit)";
				$inicio = ($pag-1)*$articulos_pagina;
				$fin = $inicio+$articulos_pagina-1;
				// hay que comprobar que el fin definido no sobrepase el total real de articulos
				if ($fin >= count($array_articulos_id)) { $fin = count($array_articulos_id)-1; }
				$cuerpo_pagina = "
<table width=\"99%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<!-- inicio cuerpo -->
	<tr>
		<td>
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-style:solid; border-width:1px; border-color:#000000;\">
			<tr>
				<td>
				<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">
					<tr align=\"right\">
						<td width=\"9%\"><span class=\"est16\">Codigo</span></td>
						<td width=\"18%\"><span class=\"est16\">Articulo</span></td>
						<td width=\"18%\"><span class=\"est16\">Embalaje</span></td>
						<td width=\"9%\"><span class=\"est16\">Cant.</span></td>
						<td width=\"9%\"><span class=\"est16\">Unid.</span></td>
						<td width=\"9%\"><span class=\"est16\">Precio</span></td>
						<td width=\"9%\"><span class=\"est16\">% Dto1</span></td>
						<td width=\"9%\"><span class=\"est16\">% Dto2</span></td>
						<td width=\"10%\"><span class=\"est16\">% Igic</span></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td style=\"padding-top:3px;\">
		<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" height=\"550\" style=\"border-style:solid; border-width:1px; border-color:#000000;\">
			<tr valign=\"top\">
				<td>
				<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
				for ($indice = $inicio; $indice <= $fin; $indice++)
				{
					$consulta_3 = "select $tabla_alb_art.* from $tabla_alb_art where $tabla_alb_art.id='".$array_articulos_id[$indice]."';";
					//echo "$consulta_3<br>";
					$resultado_3 = mysql_query($consulta_3) or die("$consulta_3 La consulta fall&oacute;: " . mysql_error());
					while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
					{
						$consulta_4 = "select * from $tabla_articulos where id='".$linea_3['articulo_id']."';";
						//echo "$consulta_4<br>";
						$resultado_4 = mysql_query($consulta_4) or die("$consulta_4 La consulta fall&oacute;: " . mysql_error());
						$linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC);
						$nombre_embalaje = "";
						$consulta_5 = "select $tabla_m_embalajes.* from $tabla_m_embalajes where $tabla_m_embalajes.id='".$linea_3['tipo_embalaje_id']."';";
						//echo "$consulta_5<br>";
						$resultado_5 = mysql_query($consulta_5) or die("La consulta fall&oacute;: " . mysql_error());
						while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
						{
							$nombre_embalaje = "$linea_5[nombre] de $linea_3[unidades_embalaje]";
						}
						$valor_igic = "";
						$consulta_5 = "select * from $tabla_m_igic where $tabla_m_igic.id='".$linea_3['igic_id']."';";
						$resultado_5 = mysql_query($consulta_5) or die("La consulta fall&oacute;: " . mysql_error());
						while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
						{
							$valor_igic = $linea_5['valor'];
						}
						$cuerpo_pagina .= "
					<tr align=\"right\">
						<td width=\"9%\"><span class=\"est12\">".$linea_4['codigo']."</span></td>
						<td width=\"18%\"><span class=\"est12\">".$linea_4['nombre']."</span></td>
						<td width=\"18%\"><span class=\"est12\">".$nombre_embalaje."</span></td>
						<td width=\"9%\"><span class=\"est12\">".$linea_3['num_embalajes']."</span></td>
						<td width=\"9%\"><span class=\"est12\">".$linea_3['unidades']."</span></td>
						<td width=\"9%\"><span class=\"est12\">".number_format($linea_3['precio_unidad'],2,",",".")." &euro;</span></td>
						<td width=\"9%\"><span class=\"est12\">";
						if ($linea_3['dto_porc'] > 0)
						{
							$cuerpo_pagina .= number_format($linea_3['dto_porc'],2,",",".")." %";
						}
						$cuerpo_pagina .= "</span></td>
						<td width=\"9%\"><span class=\"est12\">";
						if ($linea_3['dto2'] > 0)
						{
							$cuerpo_pagina .= number_format($linea_3['dto2'],2,",",".")." %";
						}
						$cuerpo_pagina .= "</span></td>
						<td width=\"10%\"><span class=\"est12\">".number_format($valor_igic,2,",",".")."</span></td>
					</tr>";
						$cuenta_articulos++;
					} // fin while linea_6
				} // fin del for
				if ($pag < $total_paginas)
				{
					$cuerpo_pagina .= "
					<tr>
						<td colspan=\"10\" align=\"left\"><span class=\"est12\">Suma y sigue</span></td>
					</tr>
";
				}
				$cuerpo_pagina .= "
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
<!-- fin cuerpo -->
</table>";
				//echo str_replace("#PAGINA#", $pag, $cabecera_pagina).$cuerpo_pagina.$pie_pagina;
				$mensaje_albaran .= "<table width=\"100%\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
	<tr>
		<td>".str_replace("#PAGINA#", $pag, $cabecera_pagina)."</td>
	</tr>
	<tr>
		<td>".$cuerpo_pagina."</td>
	</tr>
	<tr>
		<td style=\"padding-top:3px;\">".$pie_pagina."</td>
	</tr>
</table>";
/*
			if ($_REQUEST['logo'] == 1)
			{
				echo "
<span class=\"est11\">Seg&uacute;n el art 5 de la Ley 15/1999, los datos de car&aacute;cter personal de este documento, est&aacute;n incorporados en un fichero automatizado utilizado para la gesti&oacute;n administrativa de la empresa Cuymon 2000 S.L., por lo que tiene derecho a acceder a sus datos personales, rectificarlos o en su caso cancelarlos, solicit&aacute;ndolo en el domicilio se&ntilde;alado en el mismo.</span>";
			}
*/
				if ($pag < $total_paginas)
				{
					$mensaje_albaran .= "
<div class=\"separador\">&nbsp;</div>";
				}
			} // fin for paginado
			$fin_pagina = "
</body>
</html>
";
		//$mensaje_albaran .= $fin_pagina;
		} //fin while linea_presu
	} //fin while linea_1
	
	//$mensaje_albaran = str_replace("class='tabla'", $oferta_estilo_tabla, str_replace("class='texto'", $oferta_estilo_texto, str_replace("class='tipo2'", $oferta_estilo2, str_replace("class='tipo2_top'", $oferta_estilo2_top, str_replace("class='tipo1'", $oferta_estilo1, str_replace("class='tipo1_no_fondo'", $oferta_estilo1_no_fondo, $mensaje_albaran))))));
	$mensaje_albaran = str_replace("class=\"est11\"", $presupuesto_estilo_est11, str_replace("class=\"est11b\"", $presupuesto_estilo_est11b, str_replace("class=\"est12\"", $presupuesto_estilo_est12, str_replace("class=\"est12b\"", $presupuesto_estilo_est12b, str_replace("class=\"est13\"", $presupuesto_estilo_est13, str_replace("class=\"est13b\"", $presupuesto_estilo_est13b, str_replace("class=\"est14\"", $presupuesto_estilo_est14, str_replace("class=\"est14b\"", $presupuesto_estilo_est14b, str_replace("class=\"est15\"", $presupuesto_estilo_est15, str_replace("class=\"est15b\"",$presupuesto_estilo_est15b, str_replace("class=\"est16\"", $presupuesto_estilo_est16, str_replace("class=\"est16b\"", $presupuesto_estilo_est16b, str_replace("class=\"separador\"", $presupuesto_estilo_separador, str_replace("class=\"est-logo\"", $presupuesto_estilo_logo, str_replace("images/", $ruta_imagenes, $mensaje_albaran)))))))))))))));
	
	return $mensaje_albaran;
}

function CalcularAlbaran ($albaran_id)
{
$tabla_alb_articulos = "alb_articulos_t";

	// los articulos reservados no se cobran
	$importe_total_sin = 0;
	$cons = "select sum(importe_articulo) as total from $tabla_alb_articulos where albaran_id='".$albaran_id."';";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$importe_total_sin = $lin['total'];
	}
	$importe_total_igic = 0;
	$cons = "select sum(importe_igic) as total from $tabla_alb_articulos where albaran_id='".$albaran_id."';";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$importe_total_igic = $lin['total'];
	}
	
	$array_final = array();
	$array_final[0] = $importe_total_sin;
	$array_final[1] = $importe_total_igic;
	$array_final[2] = $importe_total_sin+$importe_total_igic;
	return $array_final;
}


?>