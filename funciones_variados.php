<?php
/*
Funciones en el script:

AnadirEstadistica ($usuario_id, $tipo_log_id, $opcion_id)
NombreAgTransporte ($ag_transporte_id)
CantidadStock ($articulo_id, $proveedor_id)
CantidadPedido ($articulo_id, $proveedor_id)
OpcionesProveedores ($valor_proveedor)
OpcionesClientes ($valor_cliente)
OpcionesProvOfertas ($valor_prov_oferta_id)
CrearCondicionConsulta($array_valores,$tabla_ref,$campo_ref,$operador,$and_or)
CalcularNuevaFecha($fecha_inicial, $intervalo_fecha)
ComprobarLoginPasswd($nombre)
AvisarLoginPasswd($nombre)
ComprobarNombreArchivo($nombre)
PaginadoListado($valor_pag, $direccion, $valor_parametros, $valor_max, $num_por_pagina, $nombre_alt_pag)
EnviarEmail($correo_from, $nombre_from, $array_direcciones, $correo_replay, $nombre_replay, $asunto, $mensaje, $array_archivos)
GenerarPDF ($valor_crear, $nombrefichero, $cabecera, $contenido, $firma)
*/

function AnadirEstadistica ($usuario_id, $tipo_log_id, $opcion_id)
{
	$cons_esta = "insert into log_estadisticas_t set fecha=now(), hora=now(), user_id='".$usuario_id."', tipo_id='".$tipo_log_id."', opcion_id='".$opcion_id."';";
	$res_esta = mysql_query($cons_esta) or die("La consulta fall&oacute;: $cons_esta " . mysql_error());
}

function NombreAgTransporte ($ag_transporte_id)
{
	$nombre_ag_transporte = "";
	$cons = "select * from maestro_agencias_transportes_t where id='".$ag_transporte_id."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$nombre_ag_transporte = $lin['nombre'];
	}
	return $nombre_ag_transporte;
}

function CantidadStock ($articulo_id, $proveedor_id)
{
$tabla_stock = "stock_t";

	$cantidad_stock = 0;
	$cons_stock = "select sum(unidades) as total from $tabla_stock where articulo_id='".$articulo_id."' and proveedor_id='".$proveedor_id."';";
	//echo $cons_stock;
	$res_stock = mysql_query($cons_stock) or die("La consulta fall&oacute;: $cons_stock " . mysql_error());
	while ($lin_stock = mysql_fetch_array($res_stock, MYSQL_ASSOC))
	{
		$cantidad_stock = (int) $lin_stock['total'];
	}
	return $cantidad_stock;
}

function CantidadPedido ($articulo_id, $proveedor_id)
{
global $pedido_estado_pendiente_servir, $pedido_estado_entrega_parcial;

$tabla_ped_art = "ped_articulos_t";
$tabla_pedidos = "pedidos_t";

	$cantidad_pedido = 0;
	$cons_pedido = "select sum($tabla_ped_art.unidades+$tabla_ped_art.unidades_gratis-$tabla_ped_art.unidades_entregadas) as total from $tabla_ped_art 
join $tabla_pedidos on $tabla_pedidos.id=$tabla_ped_art.pedido_id 
where ($tabla_pedidos.estado_id='".$pedido_estado_pendiente_servir."' or $tabla_pedidos.estado_id='".$pedido_estado_entrega_parcial."') and $tabla_ped_art.articulo_id='".$articulo_id."' and $tabla_ped_art.proveedor_id='".$proveedor_id."';";
	$res_pedido = mysql_query($cons_pedido) or die("La consulta fall&oacute;: $cons_pedido " . mysql_error());
	while ($lin_pedido = mysql_fetch_array($res_pedido, MYSQL_ASSOC))
	{
		$cantidad_pedido = (int) $lin_pedido['total'];
	}
	return $cantidad_pedido;
}

function OpcionesProveedores ($valor_proveedor)
{
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_proveedores_new";
$script_direcciones = "index_prov_direcciones_new";
$script_documentos = "index_prov_documentos_new";
$script_transportes = "index_prov_agencias_transportes_new";
$script_contactos = "index_prov_contactos_new";
$script_bancos = "index_prov_bancos_new";
$script_familias = "index_prov_familias_new";
$script_planning = "index_prov_planning_new";
$script_ofertas = "index_prov_ofertas_new";

	$dni_correcto = 0;
	$cons = "select * from proveedores_t where id='".$valor_proveedor."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		if (strlen($lin['nif']) == 9 && (ereg('([a-zA-Z]{1}[0-9]{8})', $lin['nif']) || ereg('([0-9]{8}[a-zA-Z]{1})', $lin['nif']) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $lin['nif']))) { $dni_correcto = 1; }
	}
	if ($dni_correcto == 1)
	{
		echo "
<table align='center'>
	<tr>
		<td>
			<a href='$enlacevolver"."$script_direcciones&proveedor_id=$valor_proveedor&pag=0'><img src='images/map.png' alt='Direcciones' title='Direcciones' border='0' /></a> 
			<a href='$enlacevolver"."$script_contactos&proveedor_id=$valor_proveedor&pag=0'><img src='images/book_addresses.png' alt='Contactos' title='Contactos' border='0' /></a> 
			<a href='$enlacevolver"."$script_bancos&proveedor_id=$valor_proveedor&pag=0'><img src='images/building_euro.png' alt='Datos bancarios' title='Datos bancarios' border='0' /></a> 
			<a href='$enlacevolver"."$script_transportes&proveedor_id=$valor_proveedor&pag=0'><img src='images/lorry.png' alt='Ag. transportes' title='Ag. transportes' border='0' /></a> 
			<a href='$enlacevolver"."$script_documentos&proveedor_id=$valor_proveedor&pag=0'><img src='images/page_white_stack.png' alt='Documentos' title='Documentos' border='0' /></a> 
			[<a href='$enlacevolver"."$script_familias&proveedor_id=$valor_proveedor&pag=0' style='color:#0000ff;'>Familias</a>] 
			[<a href='$enlacevolver"."$script&accion=formdatos&id=$valor_proveedor&pag=$pag$parametros'>Datos</a>] 
			<a href='$enlacevolver"."$script_planning&proveedor_id=$valor_proveedor&pag=0'><img src='images/calendar_view_month.png' alt='Planning' title='Planning' border='0' /></a> 
			<a href='$enlacevolver"."$script_ofertas&proveedor_id=$valor_proveedor&pag=0'><img src='images/coins_add.png' alt='Ofertas' title='Ofertas' border='0' /></a> ";
		echo "
			<a href='$enlacevolver"."$script&accion=formmodificar&id=$valor_proveedor&pag=0'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
		$posibilidad = 0;
		$posibilidad = ComprobarProveedor($valor_proveedor);
		if ($posibilidad == 0)
		{
			echo "
			<a href='$enlacevolver"."$script&accion=formborrar&id=$valor_proveedor&pag=0'><img src='images/table_delete.png' alt='Borrar' title='Borrar' border='0' /></a> ";
		}
		echo "</td>
	</tr>
</table>";
	} // fin if dni correcto
}

function OpcionesClientes ($valor_cliente)
{
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_clientes_new";
$script_direcciones = "index_clientes_direcciones_new";
$script_comerciales = "index_clientes_comerciales_new";
$script_aduanas = "index_clientes_agencias_aduanas_new";
$script_transportes = "index_clientes_agencias_transportes_new";
$script_contactos = "index_clientes_contactos_new";
$script_bancos = "index_clientes_bancos_new";
$script_proveedores = "index_clientes_prov_new";
$script_programacion = "index_programacion_visitas_new";
$script_historico_visitas = "index_historico_visitas_clientes_new";
$script_potenciales = "index_clientes_prov_potenciales_new";
$script_crear_presupuesto = "index_crear_presupuesto_new";

// grupo empresas
$script_grupo = "index_grupos_empresas_new";
$script_componentes = "index_grupos_emp_componentes_new";

	$dni_correcto = 0;
	$tipo_cliente_id = 0;
	$cons = "select * from clientes_t where id='".$valor_cliente."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$tipo_cliente_id = $lin['tipo_cliente_id'];
		if (strlen($lin['nif']) == 9 && (ereg('([a-zA-Z]{1}[0-9]{8})', $lin['nif']) || ereg('([0-9]{8}[a-zA-Z]{1})', $lin['nif']) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $lin['nif']))) { $dni_correcto = 1; }
	}
	if ($dni_correcto == 1)
	{
		if ($tipo_cliente_id == 1)
		{
			// cliente normal
			echo "
<table align='center'>
	<tr>
		<td>
			<a href='$enlacevolver"."$script_direcciones&cliente_id=$valor_cliente&pag=0'><img src='images/map.png' alt='Direcciones' title='Direcciones' border='0' /></a> 
			<a href='$enlacevolver"."$script_contactos&cliente_id=$valor_cliente&pag=0'><img src='images/book_addresses.png' alt='Contactos' title='Contactos' border='0' /></a> 
			<a href='$enlacevolver"."$script_bancos&cliente_id=$valor_cliente&pag=0'><img src='images/building_euro.png' alt='Datos bancarios' title='Datos bancarios' border='0' /></a> 
			<a href='$enlacevolver"."$script_transportes&cliente_id=$valor_cliente&pag=0'><img src='images/lorry.png' alt='Ag. transportes' title='Ag. transportes' border='0' /></a> 
			<a href='$enlacevolver"."$script_aduanas&cliente_id=$valor_cliente&pag=0'><img src='images/world.png' alt='Ag. aduanas' title='Ag. aduanas' border='0' /></a> 
			<a href='$enlacevolver"."$script_comerciales&cliente_id=$valor_cliente&pag=0'><img src='images/user_suit.png' alt='Comerciales' title='Comerciales' border='0' /></a> 
			<a href='$enlacevolver"."$script_potenciales&cliente_id=$valor_cliente&pag=0' style='color:#0000ff;'><img src='images/group_go.png' alt='Prov. poten' title='Prov. poten' border='0' /></a> 
			<a href='$enlacevolver"."$script_proveedores&cliente_id=$valor_cliente&pag=0' style='color:#0000ff;'><img src='images/script.png' alt='Condiciones' title='Condiciones' border='0' /></a> ";
			$tiene_condiciones = 0;
			$cons = "select count(id) as total from clientes_prov_t where cliente_id='".$valor_cliente."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$tiene_condiciones = $lin['total'];
			}
			if ($tiene_condiciones == 0)
			{
				echo "
			<a href='$enlacevolver"."$script&accion=formcopiar&id=$valor_cliente&pag=0'><img src='images/cut.png' alt='Copiar condiciones' title='Copiar condiciones' border='0' /></a> ";
			}
			echo "
			<a href='$enlacevolver"."$script&accion=formcopiar2&id=$valor_cliente&pag=0'><img src='images/cut_familia.png' alt='Copiar condiciones por familia' title='Copiar condiciones familia' border='0' /></a> ";
			if ($tiene_condiciones > 0)
			{
				$tiene_visitas_prov = 0;
				$cons = "select count(id) as total from agenda_t where cliente_visitado_id='".$valor_cliente."';";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$tiene_visitas_prov = $lin['total'];
				}
				if ($tiene_visitas_prov == 0)
				{
					echo "
			<a href='$enlacevolver"."$script&accion=formquitar&id=$valor_cliente&pag=0' style='color:#0000ff;'><img src='images/script_delete.png' alt='Borrar condiciones' title='Borrar condiciones' border='0' /></a> ";
				}
			}
			echo "
			<a href='$enlacevolver"."$script_programacion&cliente_id=$valor_cliente&pag=0'><img src='images/clock_add.png' alt='Programar visitas' title='Programar visitas' border='0' /></a> ";
			echo "
			<a href='$enlacevolver"."$script_historico_visitas&cliente_id=$valor_cliente&pag=0'><img src='images/clock_red.png' alt='Historico visitas' title='Historico visitas' border='0' /></a> ";
			echo "
			<a href='$enlacevolver"."$script_crear_presupuesto&accion=accioncliente&cliente_id=$valor_cliente'><img src='images/page_paste.png' alt='Crear presupuesto' title='Crear presupuesto' border='0' /></a> ";
			echo "
			<a href='$enlacevolver"."$script&accion=formmodificar&id=$valor_cliente&pag=0'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
			$posibilidad = 0;
			$posibilidad = ComprobarCliente($valor_cliente);
			if ($posibilidad == 0)
			{
				echo "
			<a href='$enlacevolver"."$script&accion=formborrar&id=$valor_cliente&pag=0'><img src='images/table_delete.png' alt='Borrar' title='Borrar' border='0' /></a> ";
			}
			echo "</td>
	</tr>
</table>";
		}
		elseif ($tipo_cliente_id == 2)
		{
			// cliente grupo empresa
			echo "
<table align='center'>
	<tr>
		<td>
			<a href='$enlacevolver"."$script_direcciones&cliente_id=$valor_cliente&pag=0'><img src='images/map.png' alt='Direcciones' title='Direcciones' border='0' /></a> 
			<a href='$enlacevolver"."$script_contactos&cliente_id=$valor_cliente&pag=0'><img src='images/book_addresses.png' alt='Contactos' title='Contactos' border='0' /></a> 
			<a href='$enlacevolver"."$script_componentes&grupo_empresa_id=$valor_cliente&pag=0'><img src='images/group.png' alt='Componentes' title='Componentes' border='0' /></a> 
			<a href='$enlacevolver"."$script_proveedores&cliente_id=$valor_cliente&pag=0' style='color:#0000ff;'><img src='images/script.png' alt='Condiciones' title='Condiciones' border='0' /></a> ";
			echo "
			<a href='$enlacevolver"."$script_grupo&accion=formmodificar&id=$valor_cliente&pag=0'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
			$posibilidad = 0;
			$posibilidad = ComprobarGrupoEmpresa($valor_cliente);
			if ($posibilidad == 0)
			{
				echo "
			<a href='$enlacevolver"."$script&accion=formborrar&id=$valor_cliente&pag=0'><img src='images/table_delete.png' alt='Borrar' title='Borrar' border='0' /></a> ";
			}
			echo "</td>
	</tr>
</table>";
		}
	} // fin if dni correcto
}

function OpcionesProvOfertas ($valor_prov_oferta_id)
{
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_prov_ofertas_new";
$script_clientes = "index_prov_ofertas_clientes_new";
$script_familias = "index_prov_ofertas_familias_new";
$script_imagenes = "index_prov_ofertas_imagenes_new";
$script_log = "index_log_ofertas_new";
$script_ver_correo = "ver_correo_oferta";
$script_envio_masivo = "envio_masivo_prov_oferta";

	$proveedor_id = 0;
	$cons = "select * from prov_ofertas_t where id='".$valor_prov_oferta_id."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$proveedor_id = $lin['proveedor_id'];
	}
	echo "
<table align='center'>
	<tr>
		<td>
			<a href='$enlacevolver"."$script_clientes&prov_oferta_id=$valor_prov_oferta_id&pag=0'><img src='images/group.png' alt='Clientes beneficiados' title='Clientes beneficiados' border='0' /></a> 
			[<a href='$enlacevolver"."$script_familias&prov_oferta_id=$valor_prov_oferta_id&pag=0'>Familias</a>] 
			<a href='$enlacevolver"."$script_imagenes&prov_oferta_id=$valor_prov_oferta_id&pag=0'><img src='images/picture.png' alt='Documentos notificaciones' title='Documentos notificaciones' border='0' /></a> 
			<a target='_blank' href='$enlacevolver"."$script_ver_correo&prov_oferta_id=$valor_prov_oferta_id'><img src='images/eye.png' alt='Previsualizar correo' title='Previsualizar correo' border='0' /></a> 
			<a href='$enlacevolver"."$script_envio_masivo&prov_oferta_id=$valor_prov_oferta_id'><img src='images/emails.png' alt='Envio masivo' title='Envio masivo' border='0' /></a> ";
	$cuenta_contactos = 0;
	$consultaselect = "select count(prov_contactos_t.id) as total from prov_contactos_t where prov_contactos_t.proveedor_id='".$proveedor_id."' and prov_contactos_t.email<>'';";
	//echo "$consultaselect";
	$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
	while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
	{
		$cuenta_contactos = $lineaselect['total'];
	}
	if ($cuenta_contactos > 0)
	{
		echo "
		<a href='$enlacevolver"."$script&accion=formenviar&id=$valor_prov_oferta_id&pag=0&proveedor_id=$proveedor_id'><img src='images/email_prov.png' alt='Notificar' title='Notificar' border='0' /></a> ";
	}
	else
	{
		echo "<img src='images/email_gris.png' alt='Proveedor sin contactos' title='Proveedor sin contactos' border='0' /> ";
	}
	echo "
			<a href='$enlacevolver"."$script_log&prov_oferta_id=$valor_prov_oferta_id&pag=0'><img src='images/email_go.png' alt='Historial envios' title='Historial envios' border='0' /></a> 
			
			<a href='$enlacevolver"."$script&accion=formmodificar&id=$valor_prov_oferta_id&pag=0&proveedor_id=$proveedor_id'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
	$posibilidad = 0;
	$posibilidad = ComprobarProvOfertas($valor_prov_oferta_id);
	if ($posibilidad == 0)
	{
		echo "
			<a href='$enlacevolver"."$script&accion=formborrar&id=$valor_prov_oferta_id&pag=0&proveedor_id=$proveedor_id'><img src='images/table_delete.png' alt='Borrar' title='Borrar' border='0' /></a> ";
	}
	echo "</td>
	</tr>
</table>";
}

function CrearCondicionConsulta($array_valores,$tabla_ref,$campo_ref,$operador,$and_or)
{
	$condicion = "";
	foreach ($array_valores as $valor_id)
	{
		if ($condicion != "") { $condicion .= " ".$and_or." "; }
		$condicion .= $tabla_ref.".".$campo_ref.$operador."'".$valor_id."'";
	}
	return $condicion;
}

// funcion que dada una fecha (Y-m-d) devuelve otra segun el intervalo pedido
function CalcularNuevaFecha($fecha_inicial, $intervalo_fecha)
{
	$nueva_fecha = "";
	$consulta_fecha = "select date_add('".$fecha_inicial."', interval ".$intervalo_fecha.") as resultado;";
	//echo "$consulta_fecha<br>";
	$resultado_fecha = mysql_query($consulta_fecha) or die("$consulta_fecha, La consulta fall&oacute;: " . mysql_error());
	while ($linea_fecha = mysql_fetch_array($resultado_fecha, MYSQL_ASSOC))
	{
		$nueva_fecha = $linea_fecha['resultado'];
	}
	return $nueva_fecha;
}

// corrijo el nombre de un campo login o password
function ComprobarLoginPasswd($nombre)
{
	$array_codigos_erroneos  = array(" ","'",'"');
	$array_codigos_correctos = array("","","");
	foreach($array_codigos_erroneos as $indice => $codigo)
	{
		$nombre = str_replace($codigo, $array_codigos_correctos[$indice], $nombre);
	}
	return $nombre;
}

// avisa si el valor pasado tiene un espacio, una comilla simple o una doble
function AvisarLoginPasswd($nombre)
{
	$total = 0;
	$array_codigos_erroneos  = array(" ","'",'"');
	foreach($array_codigos_erroneos as $indice => $codigo)
	{
		$pos = strpos($nombre, $codigo);
		if ($pos !== false) { $total++; }
	}
	return $total;
}

// corrijo el nombre de un archivo a subir antes de crearlo
function ComprobarNombreArchivo($nombre)
{
	$array_codigos_erroneos  = array(" ","�","�","�","�","�","�","�","�","�","�","�","�","�","�");
	$array_codigos_correctos = array("_","a","e","i","o","u","A","E","I","O","U","n","N","","");
	foreach($array_codigos_erroneos as $indice => $codigo)
	{
		$nombre = str_replace($codigo, $array_codigos_correctos[$indice], $nombre);
	}
	return $nombre;
}

// funcion que escribe en pantalla el pagina de un index new
function PaginadoListado($valor_pag, $direccion, $valor_parametros, $valor_max, $num_por_pagina, $nombre_alt_pag)
{
	$nombre_parametro_pagina = "pag";
	if ($nombre_alt_pag != "") { $nombre_parametro_pagina = $nombre_alt_pag; }
	$contenido_paginado = "
<table width='100%' cellpadding='2' cellspacing='3' border='0'>
	<tr align='center'>
		<td>Paginas: ";
	$pag_anterior = $valor_pag-1;
	if ($valor_pag > 0)
	{
		$contenido_paginado .= "<a href='$direccion&".$nombre_parametro_pagina."=$pag_anterior$valor_parametros' style='color:#000000; text-decoration:none;'>&laquo;</a> ";
	}
	$numero_pag = -1;
	$numero_pag_visual = 0;
	while ($valor_max > 0)
	{
		$numero_pag++;
		$numero_pag_visual++;
		if ($numero_pag == $valor_pag)
		{
			$contenido_paginado .= "<b>[<a href='$direccion&".$nombre_parametro_pagina."=$numero_pag$valor_parametros' style='color:#000000; text-decoration:none'>$numero_pag_visual</a>]</b> ";
		}
		else
		{
			$contenido_paginado .= "[<a href='$direccion&".$nombre_parametro_pagina."=$numero_pag$valor_parametros' style='color:#000000; text-decoration:none'>$numero_pag_visual</a>] ";
		}
		$valor_max = $valor_max - $num_por_pagina;
	}
	$pag_siguiente = $valor_pag+1;
	if ($valor_pag < $numero_pag)
	{
		$contenido_paginado .= " <a href='$direccion&".$nombre_parametro_pagina."=$pag_siguiente$valor_parametros' style='color:#000000; text-decoration:none'>&raquo;</a>";
	}
	$contenido_paginado .= "
		</td>
	</tr>
</table>";
	echo $contenido_paginado;
}

// funcion que envia un correo
function EnviarEmail($correo_from, $nombre_from, $array_direcciones, $correo_replay, $nombre_replay, $asunto, $mensaje, $array_archivos)
{
	$usuario_correo = "mariotac7@gmail.com";
	$clave_correo = "dor@_imo";
	
	$resultado_envios = "";
	if (count($array_direcciones) > 0)
	{
/*
pop3 y smtp: ayalaehijo.net

gir@ayalaehijo.net

a1b2c3

sin autenticaci�n segura
*/
		require_once("phpmailer5.1/class.phpmailer.php");
		$mail = new PHPMailer();
		$mail->IsSMTP();	// send via SMTP
		// correo gmail
		$mail->Host = "smtp.gmail.com";				// sets GMAIL as the SMTP server
		$mail->Port = 465;							// set the SMTP port for the GMAIL server
		$mail->SMTPAuth = true;     // turn on SMTP authentication
		$mail->SMTPSecure = "ssl";					// sets the prefix to the servier
		//$mail->SMTPDebug = 2;
		$mail->Username = $usuario_correo; //."@gmail.com"; // SMTP username
		$mail->Password = $clave_correo; // SMTP password
		
/*
		// correo ayala
		$mail->Host = "ayalaehijo.net";
		$mail->Port = 25;							// set the SMTP port for the GMAIL server
			//$mail->Port = 110;							// set the SMTP port for the GMAIL server
		$mail->SMTPAuth = true;     // turn on SMTP authentication
		//$mail->SMTPSecure = "ssl";					// sets the prefix to the servier
		//
		$mail->SMTPDebug = 2;
		$mail->Username = $usuario_correo; // SMTP username
			//$mail->Username = "gir"; // SMTP username
			//$mail->Username = "ayala"; // SMTP username
		$mail->Password = $clave_correo; // SMTP password
*/
		
		$mail->SetFrom($correo_from, $nombre_from);
		
		if ($correo_replay != "")
		{
			$mail->AddReplyTo($correo_replay,$nombre_replay);
		}

		foreach ($array_direcciones as $email)
		{
			$mail->AddAddress($email, "");
		}
		
		$mail->IsHTML(true);	// send as HTML
		
		$mail->Subject = $asunto;
		$mail->MsgHTML($mensaje);

		if (count($array_archivos) > 0)
		{
			foreach ($array_archivos as $datos_archivo)
			{
				$mail -> AddAttachment($datos_archivo[0]."/".$datos_archivo[1]);
			}
		}
		
		if(!$mail->Send())
		{
		   $resultado_envios .= "El correo no fue enviado<br>";
		   $resultado_envios .= "El error fue: " . $mail->ErrorInfo . "<br>";
		}
		else
		{
			$resultado_envios .= "<b>Correo enviado.</b><br>";
		}
	}
	return $resultado_envios;
}

function GenerarPDF ($valor_crear, $nombrefichero, $cabecera, $contenido, $firma)
{
// $valor_crear=0 => se muestra en pantalla
// $valor_crear=1 => se crea el fichero en la ruta importada del variables globales
// $nombrefichero => nombre sin extension
global $uploaddir_planning;
	
	require_once('extras/tcpdf/config/lang/spa.php');
	require_once('extras/tcpdf/tcpdf.php');
	
	// Extend the TCPDF class to create custom Header and Footer
	class MYPDF extends TCPDF
	{
		//Page header
		public function Header()
		{
/*
			// get the current page break margin
			$bMargin = $this->getBreakMargin();
			// get current auto-page-break mode
			$auto_page_break = $this->AutoPageBreak;
			// disable auto-page-break
			$this->SetAutoPageBreak(false, 0);
			// set bacground image
			//$img_file = 'images/tac7/formato_factura.jpg';
			//$this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
*/
/*
			// restore auto-page-break status
			$this->SetAutoPageBreak($auto_page_break, $bMargin);
			// set the starting point for the page content
			$this->setPageMark();
			$this->writeHTML($GLOBALS['cabecera'], true, true, true, false, '');
*/
		}
		// Page footer
		public function Footer()
		{
			// Position at 15 mm from bottom
			$this->SetY(-15);
			// Set font
			$this->SetFont('helvetica', 'I', 8);
			// Page number
			$this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}
	
	// create new PDF document
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('TAC7');
	$pdf->SetTitle('TAC7');
	$pdf->SetSubject('TAC7');
	$pdf->SetKeywords('TCPDF, PDF');
	
	// set default header data
	//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
	
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	//set margins
	//$pdf->SetMargins('20', '70', '10',false);
	$pdf->SetMargins('25', '25', '25', false);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	
	//set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
	//set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	//set some language-dependent strings
	$pdf->setLanguageArray($l);
	
	// ---------------------------------------------------------
	
	// set font
	$pdf->SetFont('helvetica', '', 8, '', true);
	//SetFont('helvetica', 'I', 8);
	
	// add a page
	$pdf->AddPage();
	
	if ($cabecera != "")
	{
		$pdf->writeHTML($cabecera, true, true, true, false, "");
	}
	if ($contenido != "")
	{
		$pdf->writeHTML($contenido, true, true, true, false, "");
	}
	if ($firma != "")
	{
		$pdf->writeHTML($firma, true, true, true, false, "");
	}
/*
if ($_GET[solo_ver] != 1) {
$js = <<<EOD
print();
EOD;
$pdf->IncludeJS($js);
}
*/
	
	// ---------------------------------------------------------
	//Close and output PDF document
	//$pdf->Output("$nombretramite.pdf", 'I');
	if ($valor_crear == 1)
	{
		$pdf->Output($uploaddir_planning."$nombrefichero.pdf", 'F');
	}
	else
	{
		$pdf->Output("$nombrefichero.pdf", 'I');
	}
	
	//============================================================+
	// END OF FILE                                                
	//============================================================+
}

?>