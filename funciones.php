<?php
/*
Funciones en el script:

VerPresupuesto ($valor_presupuesto)
CalcularPrecioArtPresupuesto ($valor_articulo, $valor_cliente, $valor_proveedor, $valor_fecha, $unidades_pedidas)
CalcularPresupuesto ($presupuesto_id)
RecalcularPortesPresupuesto ($presupuesto_id)
ActualiarPortesImportesPresupuesto ($presupuesto_id)
AgTransporteProvPresupuesto ($presupuesto_id, $proveedor_id)

VerPedido ($valor_pedido)
CalcularPedido ($pedido_id)
RecalcularPortesPedido ($pedido_id)
ActualiarPortesImportesPedido ($pedido_id)
AgTransporteProvPedido ($pedido_id, $proveedor_id)

VerAlbaran ($valor_albaran)
CalcularAlbaran ($albaran_id)

BusquedaFechasVisitas ($valor_programacion, $numero_meses)
VerPlanningProv ($prov_planning_id)
CorreoOfertaCliente ($valor_oferta_cliente)
CorreoOfertaProv ($valor_oferta)
CorreoOferta ($valor_oferta)
DevolverProvCli ($valor_cliente, $tipo_prov)
DatosIncidencia ($incidencia_id)
DatosPlanningProv ($valor_dato)

ComprobarDetalleAgenda ($valor_dato)
ComprobarMaestroIgic ($valor_dato)
ComprobarMaestroTipoTransporte ($valor_dato)
ComprobarMaestroCargoContacto ($valor_dato)
ComprobarOrigenPresupuesto ($valor_dato)
ComprobarClienteProvPotencialFam ($valor_dato)
ComprobarAlmacen ($valor_dato)
ComprobarMaestroUnidadMedida ($valor_dato)
ComprobarMaestroFrecuencias ($valor_dato)
ComprobarMaestroMotivosAusencia ($valor_dato)
ComprobarArticulosEmbalajes ($valor_dato)
ComprobarArticulos ($valor_dato)
ComprobarMaestroFamilias ($valor_dato)
ComprobarMaestroProvincias ($valor_dato)
ComprobarMaestroMunicipios ($valor_dato)
ComprobarIncidencias ($valor_dato)
ComprobarMaestroTiposIncidencia ($valor_dato)
ComprobarMaestroTiposDocProv ($valor_dato)
ComprobarMaestroTiposDocCliProv ($valor_dato)
ComprobarProvOfertasFamArt ($valor_dato)
ComprobarProvOfertasFam ($valor_dato)
ComprobarProvOfertasCli ($valor_dato)
ComprobarProvOfertas ($valor_dato)
ComprobarProvPlanningLogistica ($valor_dato)
ComprobarProvPlanning ($valor_dato)
ComprobarProvFamArt ($valor_dato)
ComprobarProvFamilia ($valor_dato)
ComprobarProveedor ($valor_dato)
ComprobarMaestroTransportes ($valor_dato)
ComprobarMaestroBancos ($valor_dato)
ComprobarMaestroAduanas ($valor_dato)
ComprobarGrupoEmpresa ($valor_dato)
ComprobarClienteAgTransporte ($valor_dato)
ComprobarClienteComercial ($valor_dato)
ComprobarClienteContactos ($valor_dato)
ComprobarClientesDirecciones ($valor_dato)
ComprobarClienteBanco ($valor_dato)
ComprobarClienteProvFamArt ($valor_dato)
ComprobarClienteProvFam ($valor_dato)
ComprobarClienteProv ($valor_dato)
ComprobarClienteProvPotencial ($valor_dato)
ComprobarCliente ($valor_dato)
ComprobarMaestroFormaPago ($valor_dato)
ComprobarMaestroGrupo ($valor_dato)
ComprobarPresupuesto ($valor_dato)
ComprobarUsuario ($valor_dato)

AnadirEstadistica ($usuario_id, $tipo_log_id, $opcion_id)
NombreAgTransporte ($ag_transporte_id)
CantidadStock ($articulo_id, $proveedor_id)
CantidadPedido ($articulo_id, $proveedor_id)
OpcionesProveedores ($valor_proveedor)
OpcionesClientes ($valor_cliente)
OpcionesProvOfertas ($valor_prov_oferta_id)
CrearCondicionConsulta($array_valores,$tabla_ref,$campo_ref,$operador,$and_or)
CalcularNuevaFecha($fecha_inicial, $intervalo_fecha)
ComprobarLoginPasswd($nombre)
AvisarLoginPasswd($nombre)
ComprobarNombreArchivo($nombre)
PaginadoListado($valor_pag, $direccion, $valor_parametros, $valor_max, $num_por_pagina, $nombre_alt_pag)
EnviarEmail($correo_from, $nombre_from, $array_direcciones, $correo_replay, $nombre_replay, $asunto, $mensaje, $array_archivos)
GenerarPDF ($valor_crear, $nombrefichero, $cabecera, $contenido, $firma)

PermisosSecciones($v_user, $v_direccion, $array_extra)

*/

include("funciones_presupuestos.php");
include("funciones_pedidos.php");
include("funciones_albaranes.php");
include("funciones_desarrollos.php");
include("funciones_comprobaciones.php");
include("funciones_variados.php");


// funcion que gestiona los permisos a las distintas partes de la herramienta
function PermisosSecciones($v_user, $v_direccion, $array_extra)
{
	//$rol_prod = $array_roles[0];
	//$v_direccion == "index_prov_fam_articulos_new" => no se usa
	//$v_direccion == "index_stock_reserva_new"
	//$v_direccion == "index_maestro_tipos_transportes_new"
	if ($v_user == 1 && (
$v_direccion == "index_maestro_almacenes_new" || 
$v_direccion == "index_stock_new" || $v_direccion == "index_historico_ajustes_new" || 
$v_direccion == "informe_reservas" || $v_direccion == "informe_comprometido" || 
$v_direccion == "index_pedidos_new" || $v_direccion == "index_ped_articulos_new" || $v_direccion == "ver_correo_pedido" || $v_direccion == "index_ped_documentos_new" || 
$v_direccion == "index_albaranes_new" || $v_direccion == "index_alb_articulos_new" || $v_direccion == "index_alb_cobros_new" || $v_direccion == "ver_correo_albaran" || $v_direccion == "index_alb_avisos_new" || $v_direccion == "index_alb_documentos_new" || 
$v_direccion == "ver_stock_articulo" || 
$v_direccion == "ver_estadistica")) { return 1; }
	elseif (($v_user == 1) && ($v_direccion == "index_crear_presupuesto_new"|| $v_direccion == "index_maestro_origen_presupuesto_new" || $v_direccion == "index_presupuestos_new" || $v_direccion == "index_pre_articulos_new" || $v_direccion == "ver_correo_presupuesto" || $v_direccion == "index_presupuesto_aceptar_new" || $v_direccion == "index_pre_documentos_new")) { return 1; }
	elseif (($v_user == 1) && $v_direccion == "informe_preparar_albaranes") { return 1; }
	elseif (($v_user == 1 || $v_user == 20) && ($v_direccion == "index_usuarios_new" || $v_direccion == "index_usuarios_ausencias_new")) { return 1; }
	elseif ($v_direccion == "configuracion" || 
$v_direccion == "index_clientes_new" || $v_direccion == "index_clientes_agencias_aduanas_new" || $v_direccion == "index_clientes_agencias_transportes_new" || $v_direccion == "index_clientes_bancos_new" || $v_direccion == "index_clientes_comerciales_new" || $v_direccion == "index_clientes_contactos_new" || $v_direccion == "index_clientes_direcciones_new" || $v_direccion == "index_clientes_prov_new" || $v_direccion == "index_clientes_prov_documentos_new" || $v_direccion == "index_clientes_prov_familias_new" || $v_direccion == "index_clientes_prov_fam_articulos_new" || $v_direccion == "index_programacion_visitas_new" || $v_direccion == "index_historico_visitas_clientes_new" || $v_direccion == "index_clientes_prov_potenciales_new" || $v_direccion == "index_clientes_prov_potenciales_familias_new" || 
$v_direccion == "index_grupos_empresas_new" || $v_direccion == "index_grupos_emp_componentes_new" || 
$v_direccion == "index_proveedores_new" || $v_direccion == "index_prov_agencias_transportes_new" || $v_direccion == "index_prov_bancos_new" || $v_direccion == "index_prov_contactos_new" || $v_direccion == "index_prov_direcciones_new" || $v_direccion == "index_prov_documentos_new" || $v_direccion == "index_prov_familias_new" || 
$v_direccion == "index_incidencias_new" || $v_direccion == "index_detalle_incidencias_new" || $v_direccion == "index_log_incidencias_new" || 
$v_direccion == "index_articulos_new" || $v_direccion == "index_articulos_embalajes_new" || $v_direccion == "index_articulos_precios_new" || 
$v_direccion == "index_maestro_agencias_aduanas_new" || $v_direccion == "index_maestro_agencias_transportes_new" || $v_direccion == "index_maestro_bancos_new" || $v_direccion == "index_maestro_familias_new" || $v_direccion == "index_maestro_formas_pago_new" || $v_direccion == "index_maestro_frecuencias_new" || $v_direccion == "index_maestro_grupos_new" || $v_direccion == "index_maestro_motivos_ausencias_new" || $v_direccion == "index_maestro_municipios_new" || $v_direccion == "index_maestro_provincias_new" || $v_direccion == "index_maestro_tipos_doc_cli_prov_new" || $v_direccion == "index_maestro_tipos_doc_prov_new" || $v_direccion == "index_maestro_tipos_incidencias_new" || $v_direccion == "index_maestro_igic_new" || $v_direccion == "index_maestro_unidades_medidas_new" || $v_direccion == "index_maestro_cargos_contactos_new" || 
$v_direccion == "index_agenda_new" || $v_direccion == "index_detalle_agenda_new" || $v_direccion == "index_mis_tareas_new" || $v_direccion == "index_tareas_otros_new" || 
$v_direccion == "index_prov_planning_new" || $v_direccion == "index_prov_planning_logistica_new" || $v_direccion == "index_prov_plan_logis_agenda_new" || $v_direccion == "index_prov_plan_logis_desplaza_new" || $v_direccion == "ver_prov_planning" || 
$v_direccion == "index_prov_ofertas_new" || $v_direccion == "index_prov_ofertas_clientes_new" || $v_direccion == "index_prov_ofertas_familias_new" || $v_direccion == "index_prov_ofertas_fam_articulos_new" || $v_direccion == "index_prov_ofertas_imagenes_new" || $v_direccion == "ver_correo_oferta" || $v_direccion == "index_log_ofertas_new" || $v_direccion == "envio_masivo_prov_oferta"
)
	{ return 1; }
	elseif ($v_direccion == "index" || $v_direccion == "index_mi_perfil_new") { return 1; }
	else { return 0; }
// || $v_direccion == ""
}

include ("funciones2.php");
?>