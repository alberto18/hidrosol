-- MySQL dump 10.13  Distrib 5.5.13, for Linux (x86_64)
--
-- Host: localhost    Database: TACnetting_aplicacion
-- ------------------------------------------------------
-- Server version	5.5.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agenda_t`
--

DROP TABLE IF EXISTS `agenda_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda_t` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dia` int(11) DEFAULT NULL,
  `mes` int(11) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `hora` int(11) DEFAULT NULL,
  `min` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `asunto` text,
  `descripcion` text,
  `hora_fin` int(11) DEFAULT NULL,
  `min_fin` int(11) DEFAULT NULL,
  `periodicidad` text,
  `dia_fin` int(11) DEFAULT NULL,
  `mes_fin` int(11) DEFAULT NULL,
  `ano_fin` int(11) DEFAULT NULL,
  `prioridad` int(11) DEFAULT NULL,
  `periodicidad_semanal_dia` int(11) DEFAULT NULL,
  `padre` int(11) DEFAULT NULL,
  `user_id2` int(11) DEFAULT NULL,
  `user_id3` int(11) DEFAULT NULL,
  `user_id4` int(11) DEFAULT NULL,
  `user_id5` int(11) DEFAULT NULL,
  `user_id6` int(11) DEFAULT NULL,
  `user_id7` int(11) DEFAULT NULL,
  `user_id8` int(11) DEFAULT NULL,
  `user_id9` int(11) DEFAULT NULL,
  `user_id10` int(11) DEFAULT NULL,
  `creador` int(11) DEFAULT NULL,
  `noc` varchar(100) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `id_gcalendar` text,
  `aviso` int(11) DEFAULT NULL,
  `fecha_enviado_a_gcalendar` datetime DEFAULT NULL,
  `creado_desde_gcalendar` int(11) DEFAULT NULL,
  `ultima_actualizacion_desde_gcalendar` datetime DEFAULT NULL,
  `grupo_destinatario` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `hora_nueva` datetime DEFAULT NULL,
  `duracion` datetime DEFAULT NULL,
  `calendario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6088 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-03-15 12:56:26
