<?php

include("variables_globales_gestproject.php");

echo "user_id: $user_id";
if ($user_id == "" || $noc == "" ) {
  echo "DEBE INICIAR UNA SESION. <a href=http://www.tacnetting.com>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<!-- MODAL !-->
<!-- Page styles -->
<link type='text/css' href='modal/css/demo.css' rel='stylesheet' media='screen' />
<!-- Contact Form CSS files -->
<link type='text/css' href='modal/css/contact.css' rel='stylesheet' media='screen' />

<!-- FIN MODAL !-->

<link rel='stylesheet' type='text/css' href='modules/calendario/cupertino/theme.css' />

<link rel='stylesheet' type='text/css' href='modules/calendario/fullcalendar/fullcalendar.css' />
<link rel='stylesheet' type='text/css' href='modules/calendario/fullcalendar/fullcalendar.print.css' media='print' />
<script type='text/javascript' src='modules/calendario/jquery/jquery-1.7.1.min.js'></script>
<script type='text/javascript' src='modules/calendario/jquery/jquery-ui-1.8.17.custom.min.js'></script>
<!-- <script type='text/javascript' src='modules/calendario/fullcalendar/fullcalendar.min.js'></script> !-->
<script type='text/javascript' src='modules/calendario/fullcalendar/fullcalendar.js'></script>
<script type='text/javascript' src='modules/calendario/fullcalendar/jquery.scrollTo-min.js'></script>




<script type='text/javascript'>

	$(document).ready(function() {


		$('#calendar').fullCalendar({
		    /*eventAfterRender: function(event, element, view) {
                      $(element).css('width','10px');
                    },*/
			//width: 3000,
			//contentHeight: 1200,
			viewDisplay: function(view) {
				//Se ejecuta despues de cargar el calendario.
				var scrollToHourMillis = 500; //tiempo de la animacion en milisegundos
				var viewName = view.name;
				if (viewName === "agendaWeek" || viewName === "agendaDay"){
					//ATENCION SI SE CAMBIA EL FORMATO DE 'timeFormat:' EL SIGUIENTE 'HH' TAMBIEN SE DEBE CAMBIAR
				      var currentHour = $.fullCalendar.formatDate(new Date(), "HH"); 
				      currentHour = ' ' + currentHour; //PARA NO BUSCAR EJ. 10:10AM 11:10AM y solo busque 10AM
				      var $viewWrapper = $("div.fc-view-" + viewName + " div.scroll_slot");
				      var currentHourLabel = $viewWrapper.find("table tbody tr th:contains('"+ currentHour +"')");
				      var hora_actual = $("div.fc-view-" + viewName + " div.scroll_slot th:contains('"+ currentHour +"')").parent();
				      $(hora_actual).find("td.fc-widget-content").addClass("div_hora_actual");
				      $viewWrapper.animate({scrollTop: 0}, 0, function() {
					var targetOffset = currentHourLabel.offset().top - 70;//70 implica mostrar 60 minutos anteriores
					var scroll = targetOffset - $viewWrapper.offset().top - currentHourLabel.outerHeight();
					$viewWrapper.animate({scrollTop: scroll}, scrollToHourMillis);
				      });
				}
				
			},
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			slotMinutes: 15,
			allDayText: "TAREAS",
			timeFormat: { '': 'HH:mm' // default
			},
			agenda: 'hh(:mm)TT', //H implica 24horas
			axisFormat: 'HH:mm', // hh(:mm)TT
			events: {
				url:'modules/calendario/json_eventos.php',
				cache:false,
				type: 'GET',
				data: {
				   usuario_id: '<?= $user_id ?>'
				},
					error: function() {
					alert('Ha habido un error en la carga de registros!');
				},
					color: 'blue',   // a non-ajax option
					textColor: 'white' // a non-ajax option
			},
			firstHour: 7, 			
			selectable: true,
			selectHelper: true,
			select: function(start, end, allDay) { //CUANDO SE HACE CLICK EN LA AGENDA Crear nuevo registro
				  var mes= start.getMonth() + 1;
				  var tiempo = start.getHours() + ':' + start.getMinutes() + ':' +start.getSeconds();
				  var fecha1 = start.getFullYear() + '-' + mes + '-' + start.getDate() + ' 00:00:00' ;
				  //var tiempo1 =  end.getHours() + ':' + end.getMinutes() + ':' +end.getSeconds();
				  var url_crear = 'modules.php?mod=agenda&submod=formularios&file=form_agenda&accion=formcrear&fecha=' + '&fecha=' + encodeURIComponent(fecha1) + '&hora=' + encodeURIComponent(tiempo)+ '&duracion=00:30:00'; // + encodeURIComponent(tiempo1);
				$('#iframe_modal').attr('src',url_crear);				
				$('#basic-modal-content_boton_desde_agenda').modal({onClose: function(){
					$('#calendar').fullCalendar( 'refetchEvents' );		
					$.modal.close();
				}});
			},
			eventDrop: function(event, delta) {  //CUANDO SE MUEVE UN EVNETO Y SE DEJA EN LA NUEVA HORA
				  var mes= event.start.getMonth() + 1;
				  var tiempo = event.start.getHours() + ':' + event.start.getMinutes() + ':' +event.start.getSeconds();
				  var fecha1 = event.start.getFullYear() + '-' + mes + '-' + event.start.getDate() + ' 00:00:00' ;
				  var duracion  = event.end.getHours() + ':' + event.end.getMinutes() + ':' +event.end.getSeconds();
				  var url_actualizar = 'modules.php?mod=calendario&submod=procesos&file=proceso_actualizar_evento&id=' + encodeURIComponent(event.id) + '&fecha=' + encodeURIComponent(fecha1) + '&hora=' + encodeURIComponent(tiempo) + '&duracion=' + encodeURIComponent(duracion);
				  
				$('#calendar').append("<div style=display:none id='div_drop'></div>");
				$('#div_drop').load(url_actualizar, function(){
					$('#div_drop').remove();
				        $('#calendar').fullCalendar( 'refetchEvents' );
					$('#calendar').fullCalendar( 'unselect' );
				});
			},
			
			eventClick: function(calEvent, jsEvent, view) { //CUANDO SE HACE CLICK EN UN EVENTO Modificar un registro
				
				var url_modificar = 'modules.php?mod=agenda&submod=formularios&file=form_agenda&accion=formmodificar&id=' + calEvent.id;
				
				$('#iframe_modal').attr('src',url_modificar);
				$('#basic-modal-content_boton_desde_agenda').modal({onClose: function(){
					$('#calendar').fullCalendar( 'refetchEvents' );
					$.modal.close();
				}}); 
			},
			
			eventResize: function(event,dayDelta,minuteDelta,revertFunc) { //CUANDO CAMBIA DE TAMA�O EL EVENTO
				  var mes= event.start.getMonth() + 1;
				  var tiempo = event.start.getHours() + ':' + event.start.getMinutes() + ':' +event.start.getSeconds();
				  var fecha1 = event.start.getFullYear() + '-' + mes + '-' + event.start.getDate() + ' 00:00:00' ;
				  var duracion  = event.end.getHours() + ':' + event.end.getMinutes() + ':' +event.end.getSeconds();
				  var url_actualizar = 'modules.php?mod=calendario&submod=procesos&file=proceso_actualizar_evento&id=' + encodeURIComponent(event.id) + '&fecha=' + encodeURIComponent(fecha1) + '&hora=' + encodeURIComponent(tiempo) + '&duracion=' + encodeURIComponent(duracion);
				
				$('#calendar').append("<div style=display:none id='div_drop'></div>");
				$('#div_drop').load(url_actualizar, function(){
					$('#div_drop').remove();
					$('#calendar').fullCalendar( 'refetchEvents' );
					$('#calendar').fullCalendar( 'unselect' );
					
				  //$('#calendar').load('modules.php?mod=calendario&file=index');
				});
			},
			
eventMouseover: function(event, domEvent) {
				var layer1 =	'<div id="events-layer" class="fc-transparent" style="position:absolute; width:100%; height:100%; top:-1px; text-align:right; z-index:100"><a><img src="images/png/16x16/page.png" title="edit" width="14" id="edbut'+event.id+'" border="0" style="padding-right:5px; padding-top:2px;" /></a>';
				var layer2 = '<a><img src="images/png/16x16/remove.png" title="delete" width="14" id="delbut'+event.id+'" border="0" style="padding-right:5px; padding-top:2px;" /></a>';
				var layer3 = '</div>';
			if (!event.borrar) { layer2 = ''; }
				
				var layer = layer1 + layer2 + layer3;
				$(this).append(layer);
				$("#delbut"+event.id).hide();
				$("#delbut"+event.id).fadeIn(300);
				$("#delbut"+event.id).click(function() {
				  var url_borrar = 'modules.php?mod=calendario&submod=procesos&file=proceso_borrar_evento&id=' + encodeURIComponent(event.id) ;
				
				  $('#basic-modal-content_boton_desde_agenda').load(url_borrar, function(){
				  $.modal.close();
				  $('#calendar').load('modules.php?mod=calendario&file=index');
				  });
				});
			
				$("#edbut"+event.id).hide();
				$("#edbut"+event.id).fadeIn(300);
				$("#edbut"+event.id).click(function() {
				var url_modificar = 'modules.php?mod=agenda&submod=formularios&file=form_agenda&accion=formmodificar&id=' + event.id;
				$('#iframe_modal').attr('src',url_modificar);
				$('#basic-modal-content_boton_desde_agenda').modal({onClose: function(){
					$('#calendar').fullCalendar( 'refetchEvents' );
					$.modal.close();
				}});
				});
			},
eventMouseout: function(event, jsEvent, view ) {	
	$("#events-layer").hide();
},
			
			loading: function(bool) {
				if (bool) $('#loading').show();
				else $('#loading').hide();
			}
			
		});
		
	});

</script>
<style type='text/css'>

	body {
		margin-top: 40px;
		text-align: center;
		font-size: 14px;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		}
		
	#loading {
		position: absolute;
		top: 30px;
		right: 30px;
		}

	#calendar {
		width: 100%;
		height: 100%;
		margin: 0 auto;
		}

</style>
<div id='loading' style='display:none'>loading...</div>
<br><div id='calendar'></div>



<!-- <div style='display: inline;' id=basic-modal_boton_desde_agenda><a  href=# class='demo button22'>pincha aqui</a></div> !-->




<!-- ELEMENTOS DE LA ALERTA MODAL -->
<link rel='stylesheet' id='simplemodal-demos-css'  href='./alertas_modales/demos.css?ver=1.0.4' type='text/css' media='all' />

    <div align=center style="width:779px; height:449px;" id="basic-modal-content_boton_desde_agenda">
			<p align=justify>
			 <b>AGENDA</b><hr>
			<iframe style="border:0;" id='iframe_modal' width=710px height=420px src=modules.php?mod=agenda&submod=formularios&file=form_agenda&accion=formcrear></iframe>        
    </div>


<script type='text/javascript' src='./alertas_modales/jquery.simplemodal.js?ver=1.3.4'></script>
<script type='text/javascript' src='./alertas_modales/demos.js?ver=1.0.3'></script>




</body>
</html>
