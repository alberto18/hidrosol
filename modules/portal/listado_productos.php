
  <!-- Main Container -->
  <section class="main-container col2-left-layout bounceInUp animated">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-lg-12"> 
          <!-- Breadcrumbs -->
		  
		  
		  
			<div class="darker-stripe">
				<div class="container">
					<div class="row">
						<div class="span12">
							<ul class="breadcrumb">
								<li> <a href="tienda" >Tienda</a>   <span class="icon-chevron-right"></span> </li>
						 <?php
						  $titulo_categoria=$_REQUEST[categoria];
						  if ($titulo_categoria == 0){
							  $titulo_categoria="Todos los productos";
						  }
						  else{
							  $cons = $conn_pdo_erp->prepare("select * from maestro_categorias_articulos_t where id=:titulo_categoria");
								
								$cons->bindValue('titulo_categoria', $titulo_categoria, PDO::PARAM_INT);
								$cons->execute();
								while($lin = $cons->fetch(PDO::FETCH_ASSOC)) {
									$titulo_categoria = $lin['nombre'];
									
								}

						  }
						
						 ?> 
						 
							<li > <?php echo $titulo_categoria; ?> </li>
							
							</ul>
						</div>
					</div>
				</div>
			</div>
		  
		  
		  
          
                  
              
          <!-- Breadcrumbs End -->
          <div class="">
            <div class="row">
              <div class="col-sm-9 col-lg-9 col-sm-push-3">
                <div class="category-description std">
                  <div class="slider-items-products">
                    <div id="category-desc-slider" class="product-flexslider hidden-buttons">
                      <div class="slider-items slider-width-col1 owl-carousel owl-theme"> 
                       <?php
					   
					  $cons50 = $conn->prepare("select * from nuke_stories where pn_topic='38'");
						try { 
							$cons50->execute();
						} catch (PDOException $e) {  
							echo "Error en la consulta: ".$e->getMessage(); 
						} 

						while($lin50 = $cons50->fetch(PDO::FETCH_ASSOC)) {
							$titulo = $lin50['pn_title'];
							$texto = $lin50['pn_hometext'];
							$fichero = "documentos/".$lin50['nombre_fichero'];
							$id = $lin50['id'];
							$id = base64_encode(base64_encode($id));
					  
					 
					  
					  ?>
					  
                        <!-- Item -->
						<div class="item"> <!--<a href="modules.php?mod=portal&file=ver_contenido&id=<?php echo $id ?>">--><img alt="<?php echo $titulo ?>" src="<?php echo $fichero ?>" title="<?php echo $titulo ?>"><!--</a>--> </div>
                        <!-- End Item --> 
                        <?php
						}
						
						?>
                      
                          
                        
                      </div>
                    </div>
                  </div>
                </div>
                <article class="col-main">
                  <div class="page-title">
                    <h2><?php echo $titulo_categoria ?></h2>
                  </div>
				  Si no encuentra el producto que busca mándenos un <a href="contactar">correo</a> y se lo ofertamos , tenemos más de 3000 ref de productos y solo publicamos lo más demandado
                  <div class="toolbar">
					<div class="sorter">
						<?php 
						$listar=$_REQUEST[listar];
						$categoria_cod=$_REQUEST[categoria];
						$sector=$_REQUEST[sector];
						$etiquetas=$_REQUEST[etiquetas];
						$pag=$_REQUEST[pag];
						$accion=$REQUEST[accion];
						if ($listar == "list"){
						
						?>
						<div class="view-mode">
							<a class="button button-grid" title="Cuadricula" href="modules.php?mod=portal&file=listado_productos&categoria=<?php echo $categoria_cod ?>&accion=<?php echo $accion ?>&pag=<?php echo $pag ?>">&nbsp;</a>
							<span class="button button-active button-list" title="Listado">&nbsp;</span> 
						</div>
							
						<?php 
						}
						else{
						?>                      
						<div class="view-mode"> 
							<span title="Grid" class="button button-active button-grid">&nbsp;</span>
							<a href="modules.php?mod=portal&file=listado_productos&categoria=<?php echo $categoria_cod ?>&sector=<?php echo $sector ?>&etiquetas=<?php echo $etiquetas ?>&listar=list&accion=<?php echo $accion ?>&pag=<?php echo $pag ?>" title="List" class="button-list">&nbsp;</a> 
						</div>	
						<?php 
						}
						?>
					 </div>
				   </div>
					
					<div class=" align-right sm-align-left">
						<div class="form-inline sorting-by">
							<form action="modules.php?mod=portal&file=listado_productos" method="POST">
								<label for="etiquetas" class="black-clr">Etiqueta: </label>
									<input type="text" name="etiquetas" id="etiquetas" placeholder="Etiquetas" class="span2" <?php if(!empty($_REQUEST['etiquetas'])){ echo "value='{$_REQUEST['etiquetas']}'"; }?>>
								
								
								<label for="categoria" class="black-clr">Categor&iacute;a: </label>
								<select id="categoria" class="span2" name="categoria">
									<option value="0">Selecciona categor&iacute;a</option>
									<?php 
									
										$cons = $conn_pdo_erp->prepare("select * from maestro_categorias_articulos_t");
										$cons->execute();
										while($lin = $cons->fetch(PDO::FETCH_ASSOC)) {
											$nombre = $lin['nombre'];
											if($_REQUEST['categoria'] == $lin['id']){ 
												echo "<option selected value='{$lin['id']}'>{$nombre}</option>";
											}else{
												echo "<option value='{$lin['id']}'>{$nombre}</option>";
											}
										}
									?>	
								</select>
							
								
								<!--<label for="sector" class="black-clr">Sector:</label>
								<select id="sector" class="span2" name="sector">
									<option value="0">Selecciona sector</option>
									<?php 
										$cons = $conn_pdo_erp->prepare("select * from maestro_sectores_t");
										$cons->execute();
										while($lin = $cons->fetch(PDO::FETCH_ASSOC)) {
											$nombre = utf8_encode($lin['nombre']);
											if($_REQUEST['sector'] == $lin['id']){ 
												echo "<option selected value='{$lin['id']}'>{$nombre}</option>";
											}else{
												echo "<option value='{$lin['id']}'>$nombre</option>";
											}
										}
									?>	
								</select>-->
								
								<button type="submit" class="btn btn-primary">Buscar</button>
							</form>
						</div>
					</div>

					<div class="category-products">
					<?php 
					
					$articles = array();
					$pag = intval($_REQUEST['pag']); if ($pag == "" or !$pag) { $pag = "0"; }
					$registros_por_pagina = 15;
					$inicio = 0;
					if ($pag != "") {
						// la primera pagina es la 0
						$inicio = $pag*$registros_por_pagina;
						//$limit = " limit $inicio,$registros_por_pagina";				
					}
					//else { $limit = ""; }
					
					//CONTROL DE FILTROS
					$parametros_paginado = '';

					$filters = '';	
					
					
					
					//Filtros
					$filters .= !empty($_REQUEST['etiquetas']) && !is_null($_REQUEST['etiquetas']) ? ' and at.etiquetas like :etiquetas' : '';
					$filters .= !empty($_REQUEST['categoria']) && !is_null($_REQUEST['categoria']) ? ' and ( at.categoria_id =:categoria_id or at.categoria_id2 =:categoria_id2 or at.categoria_id3 = :categoria_id3)' : '';	
					$filters .= !empty($_REQUEST['sector']) && !is_null($_REQUEST['sector']) ? ' and at.sector = :sector_id ' : '';
					
						
				?>
				
					<!--cambiando las clases <div class="row popup-products">
					<div id="isotopeContainer" class="isotope-container">-->
			
							
					<div class="category-products">
						<form method="post" action="modules.php?">
						
						<?php 
						if ($listar == "list") {
						?>
							<ol class="products-list" id="products-list">
						<?php
						}
						else{
						?>
						
							<ol class="products-grid">	
							
						<?php 
						}
	
		

						try{	

						$filters_parent = " where at.venta_en_web = 'on' and de_baja<>'on' ";

						if(!empty($filters)){
							$filters_parent .= $filters;
						}else{
							//Destacado solamente cuando no se apliquen otros filtros
							//$filters_parent .= "and at.destacado = 'on' ";
						}
					
						$cons = $conn_pdo_erp->prepare("select * from articulos_t as at $filters_parent limit :inicio,:registros_por_pagina");
						
														
						if(!empty($_REQUEST['etiquetas']) && !is_null($_REQUEST['etiquetas'])){
							$cons->bindValue('etiquetas', '%' . $_REQUEST['etiquetas'] . '%', PDO::PARAM_STR);
							$parametros_paginado .= "&etiquetas={$_REQUEST['etiquetas']}";
						}

						
						if(!empty($_REQUEST['categoria']) && !is_null($_REQUEST['categoria'])){
							$cons->bindValue('categoria_id', $_REQUEST['categoria'] , PDO::PARAM_STR);
							$cons->bindValue('categoria_id2', $_REQUEST['categoria'] , PDO::PARAM_STR);
							$cons->bindValue('categoria_id3', $_REQUEST['categoria'] , PDO::PARAM_STR);
							$parametros_paginado .= "&categoria={$_REQUEST['categoria']}";
						}
																
						if(!empty($_REQUEST['sector']) && !is_null($_REQUEST['sector'])){
							$cons->bindValue('sector_id', $_REQUEST['sector'] , PDO::PARAM_INT);
							$parametros_paginado .= "&sector={$_REQUEST['sector']}";
						}
						
						$cons->bindValue('inicio', $inicio, PDO::PARAM_INT);
						$cons->bindValue('registros_por_pagina', $registros_por_pagina, PDO::PARAM_INT);
						$cons->execute();
						
						while($lin = $cons->fetch(PDO::FETCH_ASSOC)) {
						
							$precio = $lin['precio_web'];
							$descripcion = $lin['descripcion_corta_web'];
							$nombre = $lin['nombre'];
							$id = $lin['id'];
							$id_categoria= $lin['categoria_id'];
							
							$url_imagen = 'articulos/imagen_no_disponible.jpg';
							$cons2 = $conn_pdo_erp->prepare("select * from gestion_documental_articulos_t where articulo_id=:id and tipo_documento = 5 order by fecha asc limit 1");
							$cons2->bindValue('id', $id, PDO::PARAM_INT);
							$cons2->execute();
							while($lin2 = $cons2->fetch(PDO::FETCH_ASSOC)) {	
								$url_imagen = 'articulos/' . $lin2['nombre_fichero']; 
							}
							$articles[] = array('precio' => $precio, 'descripcion_corta_web' => $descripcion, 'nombre' => $nombre, 'id' => $id,'categoria_id' => $id_categoria, 'url_imagen' => $url_imagen);
						}
					


						}catch(\Exception $e){	
							var_dump('primera');
							var_dump($e->getMessage());
							die();
						}
						
						
						//CALCULO DE REGISTROS TOTALES
						try{			
							$cons2 = $conn_pdo_erp->prepare("select count(id) as exp from articulos_t as at $filters_parent");

							if(!empty($_REQUEST['etiquetas']) && !is_null($_REQUEST['etiquetas'])){
								$cons2->bindValue('etiquetas', '%' . $_REQUEST['etiquetas'] . '%', PDO::PARAM_STR);
							}
							
							if(!empty($_REQUEST['categoria']) && !is_null($_REQUEST['categoria'])){
								$cons2->bindValue('categoria_id', $_REQUEST['categoria'] , PDO::PARAM_STR);
								$cons2->bindValue('categoria_id2', $_REQUEST['categoria'] , PDO::PARAM_STR);
								$cons2->bindValue('categoria_id3', $_REQUEST['categoria'] , PDO::PARAM_STR);
							}
							
							if(!empty($_REQUEST['sector']) && !is_null($_REQUEST['sector'])){
								$cons2->bindValue('sector_id', $_REQUEST['sector'] , PDO::PARAM_INT);
							}
							$cons2->execute();
							$lin2 = $cons2->fetch(PDO::FETCH_ASSOC);
							$exp = intval($lin2['exp']);
						}catch(\Exception $e){
							var_dump('segunda');
							var_dump($e->getMessage());
							die();
						}
							
						/*var_dump($exp);
						die();*/
						
						?>
					
					
							<?php
						foreach($articles as $article){
							$descripcion = $article['descripcion_corta_web'];
							$precio = $article['precio'];
							$nombre = $article['nombre'];
							$url_imagen = $article['url_imagen'];
							$id_encoded = base64_encode(base64_encode($article['id']));
							
							
							
						?>
					
								<!--  ==========  -->
								<!--  = Single Product =  -->
								<!--  ==========  -->
								
								
						<?php 
						if ($listar == "list"){
						 
							 
							 ?>
						<li class="item first">
						  <div class="product-image"> <a href="producto-<?php echo substr($id_encoded,0,-1) ?>-<?php echo $id_categoria ?>" title="<?php echo $nombre ?>"> <img class="small-image" src="<?php echo $url_imagen ?>" width="240px" height="240px" alt="<?php echo $nombre ?>"> </a></div>
						 
						 <div class="product-shop">
							<h2 class="product-name"><a href="producto-<?php echo substr($id_encoded,0,-1) ?>-<?php echo $id_categoria ?>" title="<?php echo $nombre ?>"><?php echo $nombre ?></a></h2>
							
							<div class="desc std">
							  <p><?php echo $descripcion ?></p>
							</div>
							<div class="price-box">
							
							  <p class="special-price"> <span class="price-label"></span> <span class="price"> <?php 
							  $precio_coma=str_replace(".",",",$precio);
							  echo $precio_coma ;
							  
							  ?> &#8364;</span> </p>
							</div>
							
							<div class="actions">
						
							<input type="hidden" name="id" id="id"  value="<?php echo $id_encoded?>">
							<input type="hidden" name="categoria" id="categoria" value="<?php echo $id_categoria ?>">
							
							<a href="modules.php?mod=tienda&file=cesta&id=<?php echo $id_encoded ?>=&categoria=<?php echo $id_categoria ?>&amount=1&item=<?php echo $id_encoded ?>"><button id="add_cart_btn" type="button" class="btn-cart button"><i class="icon-shopping-cart"></i> A&#241;adir a la Cesta</button> </a>
							
							
							</div>
						</div>
					   </li> 
					 <?php 
					 }
					 else{
					 ?>
						
						
						<li class="item col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="item-inner">
							  <div class="productborder">
								<div class="item-img">
								  <div class="item-img-info"> <a class="product_image image-wrapper" href="producto-<?php echo substr($id_encoded,0,-1)  ?>-<?php echo $id_categoria ?>"> <img class="front_image" alt="<?php echo $nombre;?>" src="<?php echo $url_imagen;?>" style="width:240px; height:240px" title="<?php echo $nombre ?>" > </a>
									<div class="quickview top-right"> <a class="link-quickview" href="producto-<?php echo substr($id_encoded,0,-1)  ?>-<?php echo $id_categoria ?>"> <i class="fa fa-search" aria-hidden="true"></i></a> </div>
									<div class="box-hover">
									  <ul class="add-to-links">
										
										
										<li><a class="add-to-cart" href="modules.php?mod=tienda&file=cesta&id=<?php echo $id_encoded ?>=&categoria=<?php echo $id_categoria ?>&amount=1&item=<?php echo $id_encoded ?>"></a> </li>
										
									  </ul>
									</div>
								  </div>
								</div>
								<div class="right-block">
								  <div class="item-info">
									<div class="info-inner">
									  <div class="item-title"> <a title="<?php echo $nombre;?>" href="producto-<?php echo substr($id_encoded,0,-1)  ?>-<?php echo $id_categoria ?>"><?php echo $nombre;?></a> </div>
									  <div class="item-content">
									  <?php 
									  $descripcion =substr($descripcion,0,100 );
									  
									  ?>
										<p class="product-manufacturer"><?php echo $descripcion ?><a href="producto-<?php echo substr($id_encoded,0,-1)  ?>-<?php echo $id_categoria ?>"><?php if (strlen($descripcion)==20){ echo "..."; }?></a> </p>
									  </div>
									  <div class="item-price">
										<div class="price-box"> <span class="regular-price"> <span class="price"><?php 
							  $precio_coma=str_replace(".",",",$precio);
							  echo $precio_coma ;
							  
							  ?> &#8364;</span> </span> </div>
									  </div>
									</div>
								  </div>
								</div>
							  </div>
							</div>
						  </li>


		
						<?php 
						}
							}
							?>
						</ol>
					</form>
						
						
						<!--  ==========  -->
				<!--  = Pagination =  -->
				<!--  ==========  -->
				
				<!-- BEGIN PAGINATION -->
				
					<div class="pager">
				
					<?php 
				
				
			
				
					$num_total_paginas = 0;
					$paginado_reducido = 1;
					//$consulta2 = "select count(*) as num from inmuebles_t order by id desc";	
					$categoria_cod = $_REQUEST['categoria'];
					//Numero de registros encontrados (exp).
					if ($exp > $registros_por_pagina)
					{
						$pag = intval($_REQUEST['pag']);
						//if ($nombre_alt_pag != "") { $nombre_parametro_pagina = $nombre_alt_pag; }
						$contenido_paginado = '
						<div class="pages">
								';
						$total_pag_inicial = $exp/$registros_por_pagina;
						list($total_pag, $decimales) = explode('.', $total_pag_inicial);
						if ($decimales > 0) { $total_pag++; }
						$pag_anterior = $pag-1;
						
						if($pag_anterior < 0){
							$pag_anterior = 0;
						}
																		
						if ($pag > 0)
						{
							// ir a pagina anterior
							if ($listar =="list"){
								$contenido_paginado .= "
							<ul class='pagination'>
									
									<li><a href='modules.php?mod=portal&file=listado_productos&accion=buscar&pag=$pag_anterior$parametros_paginado&listar=list'>&#171;</a></li>
							";
							}
							else{
							$contenido_paginado .= "
							<ul class='pagination'>
									
									<li><a href='modules.php?mod=portal&file=listado_productos&accion=buscar&pag=$pag_anterior$parametros_paginado'>&#171;</a></li>
							";
							}
						} else {
							$contenido_paginado .= "
							<ul>";
						}
						$numero_pag = -1;
						$numero_pag_visual = 0;
						
						
						while ($exp > 0)
						{
							$numero_pag++;
							$numero_pag_visual++;
							if ($numero_pag == $pag)
							{		
								if ($listar=="list"){
								$contenido_paginado .= "<li class='active'><a href='modules.php?mod=portal&file=listado_productos&accion=buscar&pag=$numero_pag$parametros_paginado&listar=list'>";
								}
								else{
								$contenido_paginado .= "<li class='active'><a href='modules.php?mod=portal&file=listado_productos&accion=buscar&pag=$numero_pag$parametros_paginado'>";
								}
								$contenido_paginado .= $numero_pag+1;
								$contenido_paginado .= "</a></li>";
								
							}
							else
							{
								if ($paginado_reducido == 0 || ($paginado_reducido == 1 && ($total_pag <= 5 || ($total_pag > 5 && in_array($numero_pag,array(0,$pag-1,$pag,$pag+1,$total_pag-1))))))
								{
									if($listar=="list"){
									$contenido_paginado .= "<li><a href='modules.php?mod=portal&file=listado_productos&accion=buscar&pag=$numero_pag$parametros_paginado&listar=list'>";
									}
									else{
									$contenido_paginado .= "<li><a href='modules.php?mod=portal&file=listado_productos&accion=buscar&pag=$numero_pag$parametros_paginado'>";
									}
									$contenido_paginado .= $numero_pag+1;
									$contenido_paginado .= "</a></li>";
								}
							}
							if ($paginado_reducido == 1 && ($total_pag > 5 && (($numero_pag == 0 && $pag != 0 && $pag != 1 && $pag != 2) || ($numero_pag == ($pag+1) && $pag != ($total_pag-3) && $pag != ($total_pag-2) && $pag != ($total_pag-1)))))
							{
								$contenido_paginado .= "<li><a href=#>...</a></li>";
							}
							$exp = $exp - $registros_por_pagina;
						}
						$pag_siguiente = $pag+1;
						if ($pag < $numero_pag)
						{
							// ir a pagina siguiente
							if($listar=="list"){
								$contenido_paginado .= "
							

									<li><a href='modules.php?mod=portal&file=listado_productos&accion=buscar&pag=$pag_siguiente$parametros_paginado&listar=list'>&#187;</a></li>
							
							";
							}
							else{
							$contenido_paginado .= "
							

									<li><a href='modules.php?mod=portal&file=listado_productos&accion=buscar&pag=$pag_siguiente$parametros_paginado'>&#187;</a></li>
							
							";
							}
							echo "</ul>";
						}
						
						echo $contenido_paginado;
						?>
						</div>
						<?php 
					}
				?>
			
				<!-- END PAGINATION -->
				 <!-- /pagination -->
						
						
						
							
					</div>
				</article>
		
				
				
				
                <!--	///*///======    End article  ========= //*/// --> 
              </div>
            	<div class="col-left sidebar col-sm-3 col-xs-12 col-sm-pull-9">
					<aside class="col-left sidebar">
					  <div class="side-nav-categories">
						<div class="block-title">Categor&iacute;as</div>
						<!--block-title--> 
						<!-- BEGIN BOX-CATEGORY -->
						
						<div class="box-content box-category">
						  <ul class="level0_415" style="display:block">	
						<?php
							
							$cons = $conn_pdo_erp->prepare("select * from maestro_categorias_articulos_t where mostrar_web='on'");
							try { 
								$cons->execute();
							} catch (PDOException $e) { 
								echo "Error en la consulta: ".$e->getMessage(); 
							} 

							while($lin = $cons->fetch(PDO::FETCH_ASSOC)) {
								
								$id_categoria = $lin['id'];
								$nombre = $lin['nombre'];												
								//$id_categoria=base64_encode(base64_encode($id_categoria));
							?>
							
								 <li> <a class="active" href="modules.php?mod=portal&file=listado_productos&categoria=<?php echo $id_categoria ?>"><?php echo $nombre ?></a></li>
								  <?php 
								
							}					  
						  
						  ?>
						   </ul>
						</div>
						
						
						<!--<div class="block-title">Sectores</div>
						<!--block-title
						<!-- BEGIN BOX-CATEGORY
						
						<div class="box-content box-category">
						  <ul class="level0_415" style="display:block">	
						<?php
							
							$cons = $conn_pdo_erp->prepare("select * from maestro_sectores_t where mostrar_web='on'");
							try { 
								$cons->execute();
							} catch (PDOException $e) { 
								echo "Error en la consulta: ".$e->getMessage(); 
							} 

							while($lin = $cons->fetch(PDO::FETCH_ASSOC)) {
								
								$id_sector = $lin['id'];
								$nombre = $lin['nombre'];												
								$id_sector=base64_encode(base64_encode($id_sector));
							?>
							
								 <li> <a class="active" href="modules.php?mod=portal&file=listado_productos&sector=<?php echo $id_sector ?>"><?php echo utf8_encode($nombre) ?></a></li>
								  <?php 
								
							}					  
						  
						  ?>
						   </ul>
						</div>-->
						
						<!--box-content box-category--> 
					  </div>
					 <!-- <div class="side-banner">
						<img src="images/sidebanner.jpg">
					  </div>-->
					  <div class="block block-layered-nav"></div>                                               
					</aside>
				</div>
            </div>
          </div>
        </div>
      </div>
      <!-- Main Container End --> 
      
    </div>
  </section>
</div>

