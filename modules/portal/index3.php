<?php 
function cortar_string ($titulo, $largo) { 
   $marca = "<!--corte-->"; 

   if (strlen($titulo) > $largo) { 
        
       $titulo = wordwrap($titulo, $largo, $marca); 
       $titulo = explode($marca, $titulo); 
       $titulo = $titulo[0]; 
   } 
   return $titulo; 

} 
?>


	
<div id="main">
 <div>
    <div id="content"> 
      <!-- Slider -->
      <div id="thmsoft-slideshow" class="thmsoft-slideshow">
        <div class="">
          <div>
            <div class="thm_topsection">
              <div>
                <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container'>
                  <div id='rev_slider_4' class='rev_slider fullwidthabanner'>
                    <ul>
                      
					  <?php
					  $cons50 = $conn->prepare("select * from nuke_stories where pn_topic='31'");
						try { 
							$cons50->execute();
						} catch (PDOException $e) { 
							echo "Error en la consulta: ".$e->getMessage();  
						} 

						while($lin50 = $cons50->fetch(PDO::FETCH_ASSOC)) {
							$titulo = $lin50['pn_title'];
							$texto = $lin50['pn_hometext'];
							$fichero = "documentos/".$lin50['nombre_fichero'];
							$id = $lin50['id'];
							$id = base64_encode(base64_encode($id));
					  
					 
					  ?>
					  
					  
					  <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='#'><img src="<?php echo $fichero ?>"  alt="slide-img" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' title="<?php echo $titulo ?>"/>
                        <div class="info">
                          <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;max-width:auto;max-height:auto;white-space:nowrap;'><span><?php echo $titulo ?></span> </div>
                          <div class='tp-caption Title sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1450' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'><?php echo $texto ?></div>
                          <div class='tp-caption sfb  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'><a href='modules.php?mod=portal&file=ver_contenido&id=<?php echo $id ?>' class="buy-btn">Más información</a> </div>
                        </div>
                      </li>
                      <?php 
					  
					  }
					  ?>
					
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- div del container-->
    <!-- end Slider --> 
    
    <!-- Banner Blog -->
    
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="banner-container">
            <div class="banner-inner">
              <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8">
                  <div class="figure-content"><a> <img alt="Talasoterapia/Spas" src="images/banner01.jpg" title="Talasoterapia/Spas"> </a> </div>
                  <figcaption>
                    <div class="center white">
                      <!-- <h2 class="light h2">Modern</h2> -->
                      <!-- <p class="h4">Talasoterapias</p> -->
                     <a href="modules.php?mod=portal&file=listado_productos&categoria=7"><button class="btn-learn">Piscinas/Talasoterapia/Spas</button></a>
                    </div>
                  </figcaption>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="figure-content"><a> <img alt="Robots" src="images/banner8.jpg" title="Hidropresión"> </a> </div>
                  <figcaption>
                    <div class="center">
                      <!-- <h2 class="light h2">Beautiful</h2> -->
                      <!-- <p class="h4">Limpieza profesional</p> -->
                      <a href="modules.php?mod=portal&file=listado_productos&categoria=6"><button class="btn-learn">Hidropresi&oacute;n/<br/>Sistemas contra incendio</button></a>
                    </div>
                  </figcaption>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="banner-container">
            <div class="banner-inner">
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="figure-content"><a> <img  src="images/banner3.jpg"  alt="Sitemas de Riego" title="Sistemas de Riego"> </a> </div>
                  <figcaption>
                    <div class="center">
                    <!-- <h2 class="light h2">Sistemas de Riego</h2>  -->
                       <!-- <p class="h4">Nuevos sistemas automatizados</p>  -->
                      <a href="modules.php?mod=portal&file=listado_productos&categoria=3"><button class="btn-learn">Acumulaci&oacute;n/Riego/<br/>Accesorios</button></a>
                    </div>
                  </figcaption>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="figure-content"> <a><img alt="Aire Acondicionado" src="images/banner04.jpg" title="Aire Acondicionado"> </a> </div>
                  <figcaption>
                    <div class="center">
                      <!-- <h2 class="light h2">Decorative</h2> -->
                      <!-- <p class="h4">El mejor precio </p> -->
                       <a href="modules.php?mod=portal&file=listado_productos&categoria=8"><button class="btn-learn">Sistemas de climatizaci&oacute;n</button></a>
                    </div>
                  </figcaption>
                </div>
            

			   <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="figure-content"><a> <img alt="Piscinas" src="images/banner7.jpg" title="Piscinas"> </a> </div>
                  <figcaption>
                    <div class="center">
                      <!-- <h2 class="light h2">Luxury</h2> -->
                      <!-- <p class="h4">Pisicinas a medida</p> -->
                      <a href="modules.php?mod=portal&file=listado_productos&categoria=2"><button class="btn-learn">Energ&iacute;a solar t&eacute;rmica/<br/>fotovoltaica</button></a>
                    </div>
                  </figcaption>
                </div>
			 </div>
            </div>
          </div>
        </div>
				
		<!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="banner-container">
            <div class="banner-inner">
              <div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="figure-content"><a> <img alt="Piscinas" src="images/banner7.jpg" title="Energía solar"> </a> </div>
                  <figcaption>
                    <div class="center">
                      <!-- <h2 class="light h2">Luxury</h2> -->
                      <!-- <p class="h4">Pisicinas a medida</p> -->
       <!--               <a href="modules.php?mod=portal&file=listado_productos&categoria=2"><button class="btn-learn">Energía solar</button></a>
                    </div>
                  </figcaption>
                </div>
				<div class="col-lg-8 col-md-8 col-sm-8">
                  <div class="figure-content"><a> <img alt="Talasoterapia" src="images/ban-1.jpg" title="climatización"> </a> </div>
                  <figcaption>
                    <div class="center white">
                      <!-- <h2 class="light h2">Modern</h2> -->
                      <!-- <p class="h4">Talasoterapias</p> -->
        <!--             <a href="modules.php?mod=portal&file=listado_productos&categoria=7"><button class="btn-learn">Climatización</button></a>
                    </div>
                  </figcaption>
                </div>
			
				
              </div>
            </div>
          </div>
        </div>-->
      </div>
    </div>
    
    <!-- End Banner Blog -->
    <div class="container">
      <div class="row"> 
        <!-- Popular Products -->
        <div class="col-md-12">
          <div class="bestsell-pro">
            <div class="slider-items-products">
              <div class="bestsell-block">
				  <div class="h2 products-section-title">Ofertas</div>
					<div id="bestsell-slider" class="product-flexslider hidden-buttons">
					  <div class="slider-items slider-width-col4 products-grid block-content">
			  
			  
			  <?php
			  
			  
			  
			  $cons02 = $conn_pdo_erp->prepare("select * from articulos_t where venta_en_web='on' and destacado='on';");
			$cons02->bindValue(':id_categoria', $id_categoria, PDO::PARAM_INT);
			$cons02->bindValue(':articulo', $articulo, PDO::PARAM_INT);
			try { 
				$cons02->execute();
			} catch (PDOException $e) { 
				echo "Error en la consulta: ".$e->getMessage(); 
			} 

			while($lin02 = $cons02->fetch(PDO::FETCH_ASSOC)) {
				
				$id_producto=$lin02['id'];									
				$id = base64_encode(base64_encode($id_producto));	
				$precio_producto= $lin02['precio_web'];
				$nombre = $lin02['nombre'];
				$descripcion_corta_web = $lin02 ['descripcion_corta_web'];
				$destacado = $lin02['destacado'];
				$articulo_id =  $lin02['id'];
				$id_categoria = $lin02['categoria_id'];
				//$id_categoria = base64_encode(base64_encode($id_categoria));	
				$nombre = utf8_encode($nombre);
				$descripcion_corta_web = utf8_encode($descripcion_corta_web);
			
			  
			  ?>
			  
                    <div class="item">
                      <div class="item-inner">
                        <div class="productborder">
                          <div class="item-img">
						  
							
							<?php 
							
							$cons2222 = $conn_pdo_erp->prepare("select * from gestion_documental_articulos_t where articulo_id =:articulo_id and tipo_documento=5;");
							$cons2222->bindValue(':articulo_id', $articulo_id, PDO::PARAM_INT);
							try { 
								$cons2222->execute();
							} catch (PDOException $e) { 
								echo "Error en la consulta: ".$e->getMessage(); 
							} 		
							
							$i=0;
							while($lin2222 = $cons2222->fetch(PDO::FETCH_ASSOC)) {
								$imagen = $lin2222['nombre_fichero'];
							 
								break;
								
							}
							if ($imagen == NULL){
									$imagen = "imagen_no_disponible.jpg";
																
								}
							?>
						  
						  
						
						  
                            <div class="item-img-info"> <a class="<?php echo $nombre?>" title="<?php echo $nombre ?>" href="modules.php?mod=portal&file=ver_productos&id=<?php echo $id ?>=&categoria=<?php echo $id_categoria ?>"> <img alt="<?php echo $nombre ?>" src="articulos/<?php echo $imagen ?>" width="265" height="265"> </a>
							<div class="new-label new-top-right">
								  <?php 
								  
								  if ($destacado =='on'){
									echo "Oferta";
								  }
								  
								  
								  ?>
								  
								  </div>
								  
							
							  
                              <div class="box-hover">
                                <ul class="add-to-links">
                                 <!-- <li><a class="link-wishlist" href="wishlist.html"></a> </li> 
                                  <li><a class="link-compare" href="compare.html"></a> </li> -->
								 <!--<li><a class="add-to-cart" href="shopping_cart.html"></a> </li>-->
								  
								<li><a class="add-to-cart" href="modules.php?mod=tienda&file=cesta&id=<?php echo $id ?>=&categoria=<?php echo $id_categoria ?>&amount=1&item=<?php echo $id ?>"></a> </li>
								
								  
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="right-block">
                            <div class="item-info">
                              <div class="info-inner">
                                <div class="item-title"> <a title="<?php echo $nombre ?> " href="modules.php?mod=portal&file=ver_productos&id=<?php echo $id ?>=&categoria=<?php echo $id_categoria ?>"><?php echo $nombre ?> </a> </div>
                                <div class="item-content">
									<p class="product-manufacturer">
								<?php 
								if ($descripcion_corta_web != NULL ){
									echo "$descripcion_corta_web";
								} 
								
								?>
                                  </p>
                                </div>
                                <div class="item-price">
                                  <div class="price-box"> <span class="regular-price"> <span class="price"><?php echo $precio_producto ?> &#8364;</span> </span> </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php 
						$imagen=NULL;//reseteamos variable por si algún producto no tiene imagen no se quede con la anterior
						}
					?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Popular Products end--> 
    <!--home banner-->
    <div class="bottom-banner-section">
	
	<?php
	  $cons51 = $conn->prepare("select * from nuke_stories where pn_topic='34' Limit 1");
		try { 
			$cons51->execute();
		} catch (PDOException $e) { 
			echo "Error en la consulta: ".$e->getMessage(); 
		} 

		while($lin51 = $cons51->fetch(PDO::FETCH_ASSOC)) {
			
			$titulo_banner=$lin51['pn_title'];
			$fichero = "documentos/".$lin51['nombre_fichero'];
			//$url = $lin50[''];
		
	  
	 
	  ?>
					  
      <div class="container"> <a href="modules.php?mod=portal&file=ver_contenido&id=TlRVeU1RPT0"><img alt="banner" title="<?php echo $lin51['pn_title'] ?>" src="<?php echo $fichero ?>"></a> </div>
	  <?php 
		}
	  ?>
    </div>
  </div>
  <!--home banner end--> 

  
  <!-- Brand Logo -->
  <div class="Faq-bar">
    <div class="container">
      <div class="row">
        <div class="h2 products-section-title">Representaciones</div>
        <div id="testimonials" class="product-flexslider hidden-buttons col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="slider-items slider-width-col1 owl-carousel owl-theme">
		  
		  <?php 
		  $cons6 = $conn->prepare("select * from nuke_stories where pn_topic='35'");
			try { 
				$cons6->execute();
			} catch (PDOException $e) { 
				echo "Error en la consulta: ".$e->getMessage(); 
			} 
			$total=0;
			$cantidad=0;
			while($lin6 = $cons6->fetch(PDO::FETCH_ASSOC)) {
				//$logo[]="documentos/logos/".$lin6['nombre_fichero'];	//Quitar logos cuando funcione subir imagenes					
				//$url_representacion[] = $lin6['url_video'];
				//$nombre=$lin6['pn_title'];
				$total++;
				$logo[]="documentos/".$lin6['nombre_fichero'];	//Quitar logos cuando funcione subir imagenes					
				$url_representacion[] = $lin6['url_video'];
			}
			
				
			while($cantidad<$total){
				
		?>
				
			<!--ITEM-->
			<div class="brand">
             <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 brand-logo-size">
                  <div class="brand1"> <a href="<?php echo $url_representacion[0+$cantidad] ?>" title="<?php echo $url_representacion[0+$cantidad]?>" target="_blank"><img src="<?php echo $logo[0+$cantidad] ?>" title="<?php echo $url_representacion[0+$cantidad] ?>" alt="<?php echo $url_representacion[0+$cantidad] ?>"></a> </div>
                </div>
			
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 brand-logo-size">
                   <div class="brand1"> <a href="<?php echo $url_representacion[1+$cantidad] ?>" title="<?php echo $url_representacion[1+$cantidad] ?>" target="_blank"><img src="<?php echo $logo[1+$cantidad] ?>" title="<?php echo $url_representacion[1+$cantidad] ?>" alt="<?php echo $url_representacion[0+$cantidad] ?>"></a> </div>
                </div>
			</div>
			
              
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 brand-logo-size">
                  <div class="brand1"> <a href="<?php echo $url_representacion[2+$cantidad] ?>" title="<?php echo $url_representacion[2+$cantidad] ?>" target="_blank"><img src="<?php echo $logo[2+$cantidad] ?>" title="<?php echo $url_representacion[2+$cantidad] ?>" alt="<?php echo $url_representacion[0+$cantidad] ?>"></a> </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 brand-logo-size">
                   <div class="brand1"> <a href="<?php echo $url_representacion[3+$cantidad] ?>" title="<?php echo $url_representacion[3+$cantidad] ?>" target="_blank"><img src="<?php echo $logo[3+$cantidad] ?>" title="<?php echo $url_representacion[3+$cantidad] ?>" alt="<?php echo $url_representacion[0+$cantidad] ?>"></a> </div>
                </div>
              </div>
            </div>
			<?php 
		$cantidad=$cantidad+4;
			}
			?>
         
            
          </div>
        </div>
        <div class="faq col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div id="faq-brand" class="panel-group">
		  <?php
			 $cons7 = $conn->prepare("select * from nuke_stories where pn_topic='36'");
			try { 
				$cons7->execute();
			} catch (PDOException $e) { 
				echo "Error en la consulta: ".$e->getMessage(); 
			} 
			$var = 0;
			while($lin7 = $cons7->fetch(PDO::FETCH_ASSOC)) {
								
				
				$titulo=$lin7['pn_title'];
				$contenido=$lin7['pn_hometext'];
				
				if ($var==0){
				$var++;
				?>
		    
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"><a href="#faq-collapse1" data-toggle="collapse" data-parent="#faq-brand"><!-- <em class="icon icon-anchor">&nbsp;</em> --><?php echo $titulo ?></a></h4>
              </div>
              <div id="faq-collapse1" class="panel-collapse collapse in">
                <div class="panel-body"><?php echo $contenido ?>
                </div>
              </div>
            </div>
			<?php
				}
				else{
				$var++;	
			 ?>
			<div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"><a class="collapsed" href="#faq-collapse<?php echo $var ?>" data-toggle="collapse" data-parent="#faq-brand"><!-- <em class="icon icon-anchor">&nbsp;</em> --><?php echo $titulo ?></a></h4>
              </div>
              <div id="faq-collapse<?php echo $var ?>" class="panel-collapse collapse">
                <div class="panel-body"><?php echo $contenido ?>
                  
                </div>
              </div>
            </div>
			  
			  <?php
				}
			}
			?>
            
            
           
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<!-- end Brand Logo --> 
