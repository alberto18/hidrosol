<?php
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";

$titulo = "Configuraci&oacute;n de datos maestros";
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "configuracion";


echo '
        <section id="content">
          <section class="vbox">

            <header class="header bg-white b-b b-light">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="modules.php?mod=gestproject&file=index_contenidos_new"><i class="fa fa-home"></i> Inicio</a></li>
                <li class="active">Configuraci&oacute;n</li>
              </ul>
            </header>

            <section class="scrollable wrapper w-f">
              <p class="h4">'.$titulo.'</p>
';

echo "
<table width='100%'>";
echo "
	<tr>
		<td colspan=3><hr><b>Maestros generales</b></td>
	</tr>
	<tr>
		<td colspan=3><hr></td>
	</tr>
	<tr>
		<td><a href='$enlacevolver"."index_topicos2_new&pag=0'>- Gesti&oacute;n de t&oacute;picos</a></td>
		<td><a href='$enlacevolver"."index_topicos2_new&pag=0'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	<!--<tr>
		<td><a href='$enlacevolver"."index_maestro_categoria_eventos_new&pag=0'>- Gesti&oacute;n de categor&iacute;as de eventos</a></td>
		<td><a href='$enlacevolver"."index_maestro_categoria_eventos_new&pag=0'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_maestro_categoria_novedades_new&pag=0'>- Gesti&oacute;n de categor&iacute;as de novedades</a></td>
		<td><a href='$enlacevolver"."index_maestro_categoria_novedades_new&pag=0'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_firmas_categorias_new&pag=0'>- Gesti&oacute;n de categor&iacute;as para las firmas</a></td>
		<td><a href='$enlacevolver"."index_firmas_categorias_new&pag=0'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	
	
	<tr>
		<td><a href='$enlacevolver"."index_misolicitudes_todos_new&pag=0'>Gesti&oacute;n de todos los permisos de los empleados</a></td>
		<td><a href='$enlacevolver"."index_misolicitudes_todos_new&pag=0'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."informe_usuarios_dptos_gestionados_new&pag=0'>Informe de usuarios por departamentos gestionados</a></td>
		<td><a href='$enlacevolver"."informe_usuarios_dptos_gestionados_new&pag=0'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_cuadrante_new&pag=0'>Cuadrante de permisos</a></td>
		<td><a href='$enlacevolver"."index_cuadrante_new&pag=0'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_maestro_tipo_permisos_new&pag=0&est_op=32'>Maestro de Tipos de Permiso</a></td>
		<td><a href='$enlacevolver"."index_maestro_tipo_permisos_new&pag=0&est_op=32'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_maestro_tipo_permisos_numero_dias_new&pag=0&est_op=32'>Gesti&oacute;n de n&uacute;mero m&aacute;ximo de d&iacute;as de permiso</a></td>
		<td><a href='$enlacevolver"."index_maestro_tipo_permisos_numero_dias_new&pag=0&est_op=32'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_maestro_cargos_new&pag=0&est_op=32'>Maestro de Cargos</a></td>
		<td><a href='$enlacevolver"."index_maestro_cargos_new&pag=0&est_op=32'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	<tr>
		<td><a href='$enlacevolver"."index_maestro_dias_fiesta_new&pag=0&est_op=33'>Maestro d&iacute;as no h&aacute;biles</a></td>
		<td><a href='$enlacevolver"."index_maestro_dias_fiesta_new&pag=0&est_op=33'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	<tr>
		<td><a href='$enlacevolver"."index_maestro_departamentos_new&pag=0&est_op=34'>Maestro Departamentos/Servicios </a></td>
		<td><a href='$enlacevolver"."index_maestro_departamentos_new&pag=0&est_op=34'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>



	<tr>
		<td><a href='$enlacevolver"."index_maestro_tipo_documentacion_new&pag=0&est_op=51'>Maestro Tipo de Documentacion</a></td>
		<td><a href='$enlacevolver"."index_maestro_tipo_documentacion_new&pag=0&est_op=51'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_vigilanciasalud_new&pag=0&est_op=51'>Gesti&oacute;n de comunicaciones en VIGILANCIA Y SALUD</a></td>
		<td><a href='$enlacevolver"."index_vigilanciasalud_new&pag=0&est_op=51'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_formulariossolicitudes_new&pag=0&est_op=51'>Gesti&oacute;n de comunicaciones en FORMULARIOS Y SOLICITUDES</a></td>
		<td><a href='$enlacevolver"."index_formulariossolicitudes_new&pag=0&est_op=51'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_prevencionriesgoslaborales_new&pag=0&est_op=51'>Gesti&oacute;n de comunicaciones en PREVENCION RIESGOS LABORALES</a></td>
		<td><a href='$enlacevolver"."index_prevencionriesgoslaborales_new&pag=0&est_op=51'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_normativainteres_new&pag=0&est_op=51'>Gesti&oacute;n de comunicaciones en NORMATIVA DE INTERES</a></td>
		<td><a href='$enlacevolver"."index_normativainteres_new&pag=0&est_op=51'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_procesosselectivos_new&pag=0&est_op=51'>Gesti&oacute;n de comunicaciones en PROCESOS SELECTIVOS</a></td>
		<td><a href='$enlacevolver"."index_procesosselectivos_new&pag=0&est_op=51'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_relacionessindicales_new&pag=0&est_op=51'>Gesti&oacute;n de comunicaciones en RELACIONES SINCALES</a></td>
		<td><a href='$enlacevolver"."index_relacionessindicales_new&pag=0&est_op=51'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_ofertasinteres_new&pag=0&est_op=51'>Gesti&oacute;n de comunicaciones en OFERTAS DE INTERES</a></td>
		<td><a href='$enlacevolver"."index_ofertasinteres_new&pag=0&est_op=51'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
	<tr>
		<td><a href='$enlacevolver"."index_formacion_new&pag=0&est_op=51'>Gesti&oacute;n de comunicaciones en FORMACION</a></td>
		<td><a href='$enlacevolver"."index_formacion_new&pag=0&est_op=51'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>
	
		<tr>
		<td><a href='$enlacevolver"."index_importar_nominas_new'>Importar n&oacute;minas de los empleados</a></td>
		<td><a href='$enlacevolver"."index_formacion_new&pag=0&est_op=51'>Visualizar</a></td>
		<td><font color=green>preparado</font></td>
	</tr>-->

";
echo "
</table>";



?>

            </section>


			