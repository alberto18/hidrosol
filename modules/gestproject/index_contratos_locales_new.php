<?php 

//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
 


// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "CONTRATOS LOCALES ";
// Titulo que aparece en la pestna del navegador
$titulo_pagina = "CONTRATOS LOCALES";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";
// Texto que aparece en el boton de crear
$titulo_boton_crear = "ANADIR LOCAL AL CONTRATO";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 1;
// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// Nombre del script
$script = "index_contratos_locales_new";
// Nombre de la tabla
$tabla = "contratos_locales_t"; // OJO, la clave principal se debe llamar id
$clase_boton_crear = " class='buttonmario mediummario green' ";
$clase_boton_buscar = " class='buttonmario mediummario green' ";
$clase_boton_guardar = " class='buttonmario mediummario green' ";
$clase_boton_volver  = " class='buttonmario mediummario green' ";
$clase_boton_confirmar_borrado  = " class='buttonmario mediummario red' ";

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
$campos_col1 = array('local_id','nivel_infestacion_id','tipo_plaga','dto1','causa_del_descuento','obs_para_cliente','obs_para_administracion','obs_para_renovacion');

// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]
$ayudas_col1 = array();

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('on','on','on','','','');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('','','pattern="[CRHI]{1,4}"');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php

/*
if ($login != "Admin" && $login != "mteresa") {
	$tipos_col1  = array('textarea;200;40','select;maestro_recibos_tipo_gestion_solo_cobrador_t;nombre;id','select;usuarios_t;login;id','datetime3');
} else {
	$tipos_col1  = array('textarea;200;40','select;maestro_recibos_tipo_gestion_t;nombre;id','select;usuarios_t;login;id','datetime3');
}
*/

$contrato_desencript = base64_decode(base64_decode($_REQUEST[padre_id]));
// Obtenemos el cliente de este contrato
$salida_array = obtener_multiples_campos(array('empresa_id'),'contratos_t','','id='.$contrato_desencript,'','','');
$empresa_id = $salida_array[0][empresa_id];

$tipos_col1  = array('select;locales_t;nombre;id;nombre;empresa_id="'.$empresa_id.'";','select;maestro_nivel_infestacion_t;nombre;id','text;100','text;100','textarea;400;40','textarea;400;40','textarea;400;40','textarea;400;40');

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_insert = " noc='$noc', ";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = " user_id='$user_id', ";

// Campo para la busqueda
$campo_busqueda = "nivel_infestacion_id";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript

$plantilla_insercion = "index_contratos_locales_new.plantilla.php";

if ($plantilla_insercion != "") {
  $fichero_absoluto = $dir_raiz . "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}

// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
if ($_REQUEST[accion] != "formcrear" && $_REQUEST[accion] != "formmodificar" && $_REQUEST[accion] != "formborrar") {
	$visualizar_listado = 1;
}
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','local_id','tipo_plaga','t_base_imp');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','Local','Tipo plaga','Base imponible');
// Decodificacion si existiese de los campos
//echo "login: $login";

/*
if ($login != "Admin" && $login != "mteresa") {
	$campos_listado_decod = array ('','','','si;maestro_recibos_tipo_gestion_solo_cobrador_t;nombre;id','si;usuarios_t;login;id','si;usuarios_t;login;id','');
} else {
	$campos_listado_decod = array ('','','','si;maestro_recibos_tipo_gestion_t;nombre;id','si;usuarios_t;login;id','si;usuarios_t;login;id','');
}
*/
$campos_listado_decod = array ('','si;locales_t;nombre;id','','','');


// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-striped table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_listado = " and noc='$noc'";
// Para el paginado
$registros_por_pagina = "30";

// acciones_por_registro: Es un array de botones u opciones que debe tener cada registro
// Cada registro tendra un conjunto de acciones. Fijarse como en el ID del registro, ponemos #ID#.
// maestro_formulario.php pondra el ID correcto.
$acciones_por_registro = array();
$condiciones_visibilidad_por_registro = array();



	$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#&padre_id=#PADREID#">MODIFICAR</a>';
	$condiciones_visibilidad_por_registro[] = "";

if ($login == "rsosa" || $login == "mmartin" ) {
	$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#&padre_id=#PADREID#">BORRAR</a>';
	$condiciones_visibilidad_por_registro[] = "";
}

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear

/*
$proceso_pre_accioncrear= "modules/gestproject/proceso_pre_gestioncobros_accioncrearmodificar.php";

$proceso_post_accioncrear= "modules/gestproject/proceso_post_gestioncobros_accioncrearmodificar.php";
$proceso_post_accionmodificar= "modules/gestproject/proceso_post_gestioncobros_accioncrearmodificar.php";
*/

/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";
$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";
*/

$proceso_post_accioncrear= "modules/gestproject/index_contratos_locales_new_proceso_post_accioncrearmodificar.php";
$proceso_post_accionmodificar= "modules/gestproject/index_contratos_locales_new_proceso_post_accioncrearmodificar.php";

// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
$campo_padre = "contrato_id";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
$consulta_nombre_padre = " select tipo_contrato as nombre from contratos_t where id=#PADREID#";

// Visualizamos los datos del padre, mas completos

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 0;
$buscadores = array();
$buscadores[] = "input;nombre";


/*
echo "<center>Gestiones realizadas sobre: ";
$padre_id_desencript = base64_decode(base64_decode($_GET[padre_id]));
$cons = "select * from recibos_t where id='$padre_id_desencript';";
$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {
   echo "<b>$lin[nombre_empresa]</b> | Contrato: <b>$lin[contrato]</b> | Total del recibo: <b>$lin[total_recibo]</b>";
}
echo " </center>";
*/

// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

?>



