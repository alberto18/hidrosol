<?php 
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
?>

<?php

   //include("obtener_miperfil.php");
   
   //include("obtener_opciones.php");

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "GESTION DE SOLICITUDES";
// Titulo que aparece en la pesta񡠤el navegador
$titulo_pagina = "GESTION DE SOLICITUDES";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo '
        <section id="content">
          <section class="vbox">

            <header class="header bg-white b-b b-light">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="modules.php?mod=gestproject&file=index"><i class="fa fa-home"></i> Inicio</a></li>
                <li class="active">Nueva solicitud</li>
              </ul>
            </header>

            <section class="scrollable wrapper w-f">
              <p class="h4">'.$titulo.'</p>
';
			  
			  
// Texto que aparece en el boton de crear
$titulo_boton_crear = "CREAR NUEVA SOLICITUD";
// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.
// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 
$clase_boton_crear = " class='btn btn-success' ";
$clase_boton_buscar = " class='btn btn-mini btn-black' ";
$clase_boton_guardar = " class='btn btn-success' ";
$clase_boton_volver  = " class='btn btn-navi' ";
$clase_boton_confirmar_borrado  = " class='btn btn-danger' ";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 0; // Creamos las solicitudes de otra forma
// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// Nombre del script
$script = "index_missolicitudes_fallecimientofamiliar_2grado_new";
// Nombre de la tabla
$tabla = "solicitud_t"; // OJO, la clave principal se debe llamar id

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]

// VISITA MEDICA
$campos_col1 = array('fecha_ini','fecha_fin','grado_parentezco_id','observaciones','nombre_fichero');
$ayudas_col1 = array();
$campos_col1_obligatorios = array('on','on','on','');
$campos_col1_mascaras = array('','','','','','');
$campos_col1_readonly = array('','','','','','');
$tipos_col1  = array('datetime3','datetime3','select;maestro_grados_parentezco_2_grado_t;nombre;id;nombre','textarea;300;100','file_db;8388608;descargar_justificante;tipo_fichero;peso_fichero;fichero_binario');
$campos_automaticos_para_insert = " tipo_permiso=47, fecha_solicitud=now(), user_id='$user_id', estado_departamento_id=3, estado_concejal_id=3, estado_rrhh_id=3, ";

$plantilla_insercion = "index_missolicitudes_fallecimientofamiliar_2grado_new.plantilla.php";

$ocultar_botones_volver_sin_cambios = 1;


// Campo para la busqueda
$campo_busqueda = "fecha_ini desc";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript

if ($plantilla_insercion != "") {
  $fichero_absoluto =  "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}


// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 0;
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','tipo_permiso','fecha_solicitud','fecha_ini','fecha_fin','estado_departamento_id','estado_concejal_id');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','Tipo permiso','Fecha solicitud','Fecha inicial','Fecha fin','Estado Departamento','Estado Concejal');
// Decodificacion si existiese de los campos
$campos_listado_decod = array ('','si;maestro_tipo_permisos_t;nombre;id;nombre','si;datetime','si;datetime','si;datetime','si;maestro_estados_solicitud_t;nombre;id;nombre','si;maestro_estados_solicitud_t;nombre;id;nombre');
// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_listado = " and noc='$noc'";
// Para el paginado
$registros_por_pagina = "30";


//$filtros_iniciales = " and ($tabla.empresa_servicio=1 or $tabla.empresa_servicio=2 or $tabla.empresa_servicio=6) and ($tabla.via_cobro=1 or $tabla.via_cobro=6) and user_destino_id='$user_id'";


//$consulta_inicial =  "select $string_para_select from $tabla left join recibos_gestiones_t on $tabla.id=recibos_gestiones_t.recibo_id where recibos_gestiones_t.user_destino_id='$user_id' and $tabla.id>0 $filtro_noc_para_listado $filtro_buscar $filtro_padre $filtros_iniciales";
$visualizar_num_registros = 1;

// 2. MODO PERSONALIZADO
// Nombre de la funcion que se ejecutara para cada registro. Si se deja blanco el modo personalizado NO ESTA ACTIVO

$nombre_funcion_acciones_por_registro = "funcion_acciones_registro";
function funcion_acciones_registro($valor_id)
{

	$id_encript = base64_encode(base64_encode($valor_id));

	/*
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_comerciales_kms_new&padre_id='.$id_encript.'&pag=0">KMS</a>';
		
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_areas_new&padre_id='.$id_encript.'&pag=0">AREAS</a>';
                
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_epi_new&padre_id='.$id_encript.'&pag=0">MATERIALES</a>';

		
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_vacaciones_new&padre_id='.$id_encript.'&pag=0">VACACIONES</a>';
	*/
        
	echo '<a class="smallmario red" href="modules.php?mod=gestproject&file=index_usuarios_bajas_new&padre_id='.$id_encript.'&pag=0">ANULAR</a>';
	
	/*
    echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_documentacion_new&padre_id='.$id_encript.'&pag=0">DOCUMENTOS</a>';

	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_new&accion=formmodificar&id='.$id_encript.'"><i class="fugue-pencil" title="editar"></i> VER FICHA</a>';
	*/
	
	//echo '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
}


/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear
/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";
$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";
*/

$proceso_post_accioncrear= "modules/gestproject/index_missolicitudes_visitamedica_new_proceso_post_accioncrear.php";


// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
// $campo_padre = "";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
// $consulta_nombre_padre = " select nombre as nombre from productos_t where id=#PADREID#";

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 1;
$buscadores = array();
//$buscadores[] = "select;matricula;vehiculos_t;matricula;id;buscar por matricula";
$buscadores[] = "intervalo_fechas;fecha_ini;;;;Filtrar por fecha inicio ";
$buscadores[] = "select;tipo_permiso_id;maestro_tipo_permisos_t;nombre;id;buscar por tipo de permiso";
$buscadores[] = "select;estado_id;maestro_estados_solicitud_t;nombre;id;buscar por estado";


//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";


// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

// Imprimimos la ayuda de este permiso
$ayuda = obtener_campo('ayuda','maestro_tipo_permisos_t','','id=47');
?>
<br>
<div class="panel bg bg-info text-sm m-b-none">
<div class="panel-body">
  <span class="arrow right"></span>
  <p class="m-b-none"><b>AYUDA:</b> <?= $ayuda ?></p>
</div>
</div>


            </section>

