<?php 
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
?>

<?php

   //include("obtener_miperfil.php");
   
   //include("obtener_opciones.php");

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "GESTION DEL ALBARAN (Usuario: $user_id)";
// Titulo que aparece en la pesta�a del navegador
$titulo_pagina = "GESTION DEL ALBARAN";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";
// Texto que aparece en el boton de crear
$titulo_boton_crear = "CREAR NUEVO PRODUCTO";
// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.
// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 
$clase_boton_crear = " class='btn btn-success' ";
$clase_boton_buscar = " class='btn btn-mini btn-black' ";
$clase_boton_guardar = " class='btn btn-success' ";
$clase_boton_volver  = " class='btn btn-navi' ";
$clase_boton_confirmar_borrado  = " class='btn btn-danger' ";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 1;
// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// Nombre del script
$script = "index_albaranes_new";
// Nombre de la tabla
$tabla = "albaran_t"; // OJO, la clave principal se debe llamar id
$left_join_inicial = " left join maestro_productos_albaran_t on albaran_t.producto_id=maestro_productos_albaran_t.id ";

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
$campos_col1 = array('producto_id','cantidad','unidades_id','empresa_id');

// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]
$ayudas_col1 = array();

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('on','on','on','on');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('','','','');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('','','','');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php
$tipos_col1  = array('select;maestro_productos_albaran_t;nombre;id;','text;100','select;maestro_unidades_medidas_t;nombre;id;','select;maestro_empresas_serv_t;nombre;id;');

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
// $filtro_noc_para_insert = " noc='$noc', ";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = "";

// Campo para la busqueda
//$campo_busqueda = "maestro_productos_albaran_t.nombre";
$campo_ordenacion = "maestro_productos_albaran_t.nombre";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript
$plantilla_insercion = "index_albaranes_new.plantilla.php";
if ($plantilla_insercion != "") {
  $fichero_absoluto =  "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}


// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 1;
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','producto_id','cantidad','unidades_id','empresa_id');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','Producto','Cantidad','Unidad de medida','Empresa');
// Decodificacion si existiese de los campos
$campos_listado_decod = array ('','si;maestro_productos_albaran_t;nombre;id','','si;maestro_unidades_medidas_t;nombre;id','si;maestro_empresas_serv_t;nombre;id');
// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_listado = " and noc='$noc'";
// Para el paginado
$registros_por_pagina = "50";
$paginado_reducido = 1;


//$filtros_iniciales = " and ($tabla.empresa_servicio=1 or $tabla.empresa_servicio=2 or $tabla.empresa_servicio=6) and ($tabla.via_cobro=1 or $tabla.via_cobro=6) and user_destino_id='$user_id'";


//$consulta_inicial =  "select $string_para_select from $tabla left join recibos_gestiones_t on $tabla.id=recibos_gestiones_t.recibo_id where recibos_gestiones_t.user_destino_id='$user_id' and $tabla.id>0 $filtro_noc_para_listado $filtro_buscar $filtro_padre $filtros_iniciales";
$visualizar_num_registros = 1;

// 2. MODO PERSONALIZADO
// Nombre de la funcion que se ejecutara para cada registro. Si se deja blanco el modo personalizado NO ESTA ACTIVO

$nombre_funcion_acciones_por_registro = "funcion_acciones_registro";
function funcion_acciones_registro($valor_id)
{
      //BUSCAMOS LA CANTIDAD DEL REGISTRO PARA COMPARAR EN EL PROCESO DE SALIDA
      $cons = "select cantidad from albaran_t where id = '$valor_id'";
      $res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
      $lin = mysql_fetch_array($res, MYSQL_ASSOC);
      
	$id_encript = base64_encode(base64_encode($valor_id));
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_albaranes_entrada_new&padre_id='.$id_encript.'&pag=0">Entrada</a>';
      if ($lin[cantidad] > '0') {//SI LA CANTIDAD ES MAYOR 0 MOSTRAMOS EL BOTON DE SALIDA	
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_albaranes_salida_new&padre_id='.$id_encript.'&pag=0">Salida</a>';
      }	else {
        echo '<span class="smallmario gray">Salida</span>';
      }
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_albaranes_new&accion=formmodificar&id='.$id_encript.'"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';	
	
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_albaranes_new&accion=formborrar&id='.$id_encript.'"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
}


/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear
/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";
$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";
*/

//$proceso_pre_accioncrear= "modules/gestproject/index_vehiculos_new_proceso_pre_accioncrearmodificar.php";

// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
// $campo_padre = "";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
// $consulta_nombre_padre = " select nombre as nombre from productos_t where id=#PADREID#";

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 1;
$buscadores = array();
//$buscadores[] = "select;matricula;vehiculos_t;matricula;id;buscar por matricula";
$buscadores[] = "select;unidades_id;maestro_unidades_medidas_t;nombre;id;buscar por unidades";
$buscadores[] = "select;empresa_id;maestro_empresas_serv_t;nombre;id;buscar por empresas";
$buscadores[] = "select_input;producto_id;maestro_productos_albaran_t;nombre;id;buscar por producto";


//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";


// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

?>