<?php 
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
?>

<?php

   //include("obtener_miperfil.php");
   
   //include("obtener_opciones.php");

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "GESTION DE VACACIONES DEL USUARIO";
// Titulo que aparece en la pestaña del navegador
$titulo_pagina = "GESTION DE VACACIONES DEL USUARIO";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";
// Texto que aparece en el boton de crear
$titulo_boton_crear = "INCORPORAR UN NUEVO PERIODO DE VACACIONES";
// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.
// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 
$clase_boton_crear = " class='btn btn-success' ";
$clase_boton_buscar = " class='btn btn-mini btn-black' ";
$clase_boton_guardar = " class='btn btn-success' ";
$clase_boton_volver  = " class='btn btn-navi' ";
$clase_boton_confirmar_borrado  = " class='btn btn-danger' ";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 1;
// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// Nombre del script
$script = "index_usuarios_vacaciones_new";
// Nombre de la tabla
$tabla = "usuarios_vacaciones_t"; // OJO, la clave principal se debe llamar id

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
$campos_col1 = array('fecha_inicio','fecha_final', 'observaciones','fin_computo');

// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]
$ayudas_col1 = array();

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('on','on','','','');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('','','','','');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('','','','','');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php
$tipos_col1  = array('datetime3','datetime3','textarea;400;50','checkbox');

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
// $filtro_noc_para_insert = " noc='$noc', ";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = " fecha_creacion=now(), ";

// Campo para la busqueda
$campo_busqueda = "fecha_inicio desc";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript
$plantilla_insercion = "index_usuarios_vacaciones_new.plantilla.php";
if ($plantilla_insercion != "") {
  $fichero_absoluto =  "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}

// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 1;
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','fecha_inicio','fecha_final','observaciones','fin_computo');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','fecha_inicio','fecha_final','observaciones','fin_computo');
// Decodificacion si existiese de los campos
$campos_listado_decod = array ('','si;datetime','si;datetime','');
// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_listado = " and noc='$noc'";
// Para el paginado
$registros_por_pagina = "3000";


// acciones_por_registro: Es un array de botones u opciones que debe tener cada registro
// Cada registro tendra un conjunto de acciones. Fijarse como en el ID del registro, ponemos #ID#.
// maestro_formulario.php pondra el ID correcto.
$acciones_por_registro = array(); 
$condiciones_visibilidad_por_registro = array();

$acciones_por_registro[] = '<a class="smallmario green"  href="modules/gestproject/imprimir_doc_vacaciones.php?id=#ID#" target="_new"><i class="fugue-pencil" title="editar"></i> IMPRIMIR</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="smallmario green"  href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#&padre_id=#PADREID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="smallmario green"  href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#&padre_id=#PADREID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";


/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear
/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
*/
$proceso_post_accioncrear= "modules/gestproject/index_usuarios_vacaciones_new_proceso_post_accioncrearmodifificar.php";
//$proceso_post_accionmodificar= "modules/gestproject/proceso_post_accioncrearmodificar.php";


// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
 $campo_padre = "user_id";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
 $consulta_nombre_padre = " select login as nombre from usuarios_t where id=#PADREID#";
/*
// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 1;
$buscadores = array();
//$buscadores[] = "select;matricula;vehiculos_t;matricula;id;buscar por matricula";
$buscadores[] = "input;fecha";
*/

//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";


// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

// Calculamos los dias de vacaciones que ha tenido el usuario
$padre_id = base64_decode(base64_decode(($_REQUEST[padre_id])));

// Comprobamos si este usuario trabaja con dias habiles o dias naturales
$dias_habiles = 0; // si dias_habiles se mantiene a cero, estamos en dias_naturales
$consultamod = "select dias_vacaciones_habiles from usuarios_t where id='$padre_id'";
//echo "$consultamod<br>";
$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: $consultamod " . mysql_error());
while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC)) {
    if ($lineasmod[dias_vacaciones_habiles] == "on") { $dias_habiles = 1; }
}

// Vemos todos los registros hasta el on
$num_dias = 0;
$consultamod = "select to_days(fecha_final)-to_days(fecha_inicio) as dias, fecha_final, fecha_inicio, fin_computo from usuarios_vacaciones_t where user_id='$padre_id' order by fecha_inicio desc";
//echo "$consultamod<br>";
$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: $consultamod " . mysql_error());
while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC)) {
   if ($lineasmod[fin_computo] == "on") { break; }
   /*
   echo "fecha_final: $lineasmod[fecha_final]<br>";
   echo "fecha_inicio: $lineasmod[fecha_inicio]<br>";
   echo "dias: $lineasmod[dias]<br>";
   */
   //echo "dias_habiles: $dias_habiles<br>";
   if ($dias_habiles == 0) {
		$num_dias += $lineasmod[dias];
   } else {
		// Se trata de contar dias habiles. Tenemos que ir comprobando dia a dia viendo si no es ni sabado ni domingo y no esta en el periodo de dias de fiesta
	    // Desde la fecha_final hasta fecha_inicio, 
		// Para saber si es sabado o domingo 0: domingo 6: sabado
		// $fecha="2007-01-31";
		// $dia=date("w", strtotime($fecha));
		
		// Para sumar dias para ir recorriendo los dias
		list ($fecha_final, $n) = explode(' ', $lineasmod[fecha_final]);
		list ($fecha_inicio, $n) = explode(' ', $lineasmod[fecha_inicio]);
		//echo "fecha_inicio: $fecha_inicio<br>";
		//echo "fecha_final: $fecha_final<br>";
		/*
		$fecha = date('Y-m-j');
		$nuevafecha = strtotime ( '+2 day' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
		echo $nuevafecha;
		*/
		// Recorremos DESDE la fecha_inicio hasta la fecha_final
		$fecha_iteracion = $fecha_inicio;
		$salir = 0;
		while ($salir == 0) {
			//echo "fecha_iteracion: $fecha_iteracion<br>";
			// Vemos si es sabado o domingo
			$dia_semana = date("w", strtotime($fecha_iteracion));
			if ( ($dia_semana != 0) && ($dia_semana != 6) ) {
			
			    $es_fiesta = 0;
				$consultamod2 = "select id from maestro_dias_fiesta_t where dia_fiesta='$fecha_iteracion'";
				//echo "$consultamod2<br>";
				$resultadomod2 = mysql_query($consultamod2) or die("La consulta fall&oacute;: $consultamod " . mysql_error());
				while ($lineasmod2 = mysql_fetch_array($resultadomod2, MYSQL_ASSOC)) {
					$es_fiesta = 1;
				}
			
				if ($es_fiesta == 0) {
					$num_dias = $num_dias + 1;
				}
			}
			
			
			
			$nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha_iteracion ) ) ;
			$fecha_iteracion = date ( 'Y-m-d' , $nuevafecha );
			//echo "$fecha_iteracion<br>";
			
			
			if ("$fecha_iteracion" == "$fecha_final") { $salir = 1; }
			
		}
   }
   
}
// Fin Calculamos los dias de vacaciones que ha tenido el usuario

echo "<br><i>N&uacute;mero total de d&iacute;as satisfechos en el ultimo periodo: <b>$num_dias</b></i>";


?>



