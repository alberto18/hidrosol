<?php 
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
?>

<?php

	// prueba
   //include("obtener_miperfil.php");
   
   //include("obtener_opciones.php");

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "";
// Titulo que aparece en la pesta�a del navegador
$titulo_pagina = "Mis dias";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo '
        <section id="content">
          <section class="vbox">

            <header class="header bg-white b-b b-light">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="modules.php?mod=gestproject&file=index"><i class="fa fa-home"></i> Inicio</a></li>
                <li class="active">Mis d&iacute;as</li>
              </ul>
            </header>

            <section class="scrollable wrapper w-f">
              <p class="h4">Mis d&iacute;as</p>
';
			  
			  
// Texto que aparece en el boton de crear
$titulo_boton_crear = "CREAR NUEVO USUARIO";
// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.
// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 
$clase_boton_crear = " class='btn btn-success' ";
$clase_boton_buscar = " class='btn btn-mini btn-black' ";
$clase_boton_guardar = " class='btn btn-success' ";
$clase_boton_volver  = " class='btn btn-navi' ";
$clase_boton_confirmar_borrado  = " class='btn btn-danger' ";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 0;
// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// Nombre del script
$script = "index_misdias_new";
// Nombre de la tabla
$tabla = "usuarios_t"; // OJO, la clave principal se debe llamar id

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
$campos_col1 = array('login','grupo_id','nombre','email', 'movil', 'estado_id','cod','direccion','empresa_servicio_id','cargo_id');

// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]
$ayudas_col1 = array();

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('on','','on','','','');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('','','','','','');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('','','','','','');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php
$tipos_col1  = array('mostrar_dato_text','mostrar_dato_select;grupos_t;nombre;id;','mostrar_dato_text','mostrar_dato_text','mostrar_dato_text','mostrar_dato_select;usuarios_estados_t;nombre;id','mostrar_dato_text','mostrar_dato_text','mostrar_dato_select;maestro_empresas_serv_t;nombre;id;nombre','mostrar_dato_select;maestro_cargos_t;nombre;id;nombre');

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
// $filtro_noc_para_insert = " noc='$noc', ";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = "";

$ocultar_boton_guardar_en_formcrear_formmodificar = 1;

// Campo para la busqueda
$campo_busqueda = "login";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript
$plantilla_insercion = "index_miperfil_new.plantilla.php";
if ($plantilla_insercion != "") {
  $fichero_absoluto =  "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}


// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 0;
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','login','grupo_id','nombre','email', 'movil', 'estado_id');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','login','grupo_id','nombre','email', 'movil', 'estado_id');
// Decodificacion si existiese de los campos
$campos_listado_decod = array ('','','si;grupos_t;nombre;id','','','', 'si;usuarios_estados_t;nombre;id');
// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
$filtro_noc_para_listado = " and id='$user_id'";
// Para el paginado
$registros_por_pagina = "3000";


//$filtros_iniciales = " and ($tabla.empresa_servicio=1 or $tabla.empresa_servicio=2 or $tabla.empresa_servicio=6) and ($tabla.via_cobro=1 or $tabla.via_cobro=6) and user_destino_id='$user_id'";


//$consulta_inicial =  "select $string_para_select from $tabla left join recibos_gestiones_t on $tabla.id=recibos_gestiones_t.recibo_id where recibos_gestiones_t.user_destino_id='$user_id' and $tabla.id>0 $filtro_noc_para_listado $filtro_buscar $filtro_padre $filtros_iniciales";
$visualizar_num_registros = 1;

// 2. MODO PERSONALIZADO
// Nombre de la funcion que se ejecutara para cada registro. Si se deja blanco el modo personalizado NO ESTA ACTIVO

$nombre_funcion_acciones_por_registro = "funcion_acciones_registro";
function funcion_acciones_registro($valor_id)
{

	$id_encript = base64_encode(base64_encode($valor_id));

	/*
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_comerciales_kms_new&padre_id='.$id_encript.'&pag=0">KMS</a>';
		
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_areas_new&padre_id='.$id_encript.'&pag=0">AREAS</a>';
                
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_epi_new&padre_id='.$id_encript.'&pag=0">MATERIALES</a>';

		
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_vacaciones_new&padre_id='.$id_encript.'&pag=0">VACACIONES</a>';
	*/
     
    /*	 
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_bajas_new&padre_id='.$id_encript.'&pag=0">BAJAS</a>';
	
    echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_documentacion_new&padre_id='.$id_encript.'&pag=0">DOCUMENTOS</a>';
	*/
    echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_miperfil_documentacion_new&padre_id='.$id_encript.'&pag=0">DOCUMENTOS</a> ';
	
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_miperfil_new&accion=formmodificar&id='.$id_encript.'"><i class="fugue-pencil" title="editar"></i> VER DATOS</a>';
	
	
	
	
	//echo '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
}


/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear
/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";
$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";
*/

// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
// $campo_padre = "";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
// $consulta_nombre_padre = " select nombre as nombre from productos_t where id=#PADREID#";

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 0;
$buscadores = array();
//$buscadores[] = "select;matricula;vehiculos_t;matricula;id;buscar por matricula";
$buscadores[] = "input;nombre|login|id|cod";
$buscadores[] = "select;grupo_id;grupos_t;nombre;id;buscar por grupo";
$buscadores[] = "select;empresa_servicio_id;maestro_empresas_serv_t;nombre;id;buscar por empresa de servicio";
$buscadores[] = "select;estado_id;usuarios_estados_t;nombre;id;buscar por estado";


//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";


// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");


echo "<hr>";

/*
// El login es el nif
$dni = obtener_campo('login','usuarios_t','','id='.$user_id);

$dni = "43766733X";
//echo "dni: $dni";

echo "<table width=75% align=center>";

$consulta30  = "select * from nominas2_t where dni='$dni' order by ano desc, mes desc;";
$resultado30 = mysql_query($consulta30) or die("La consulta fall&oacute;: " . mysql_error());
while ($linea30 = mysql_fetch_array($resultado30, MYSQL_ASSOC)) {


	echo "<tr><td>Nomina del mes $linea30[mes] y a&ntilde;o $linea30[ano]</td><td>";
	echo "
	<form method=get action=leer_nomina.php name=form_volver>
	<input type=hidden name=id value=$linea30[id]>
	<input type=submit value=DESCARGAR>
	</form>
	";
	echo "</td></tr>";

}

echo "</table>";
*/

$anos = array();
$anos[3] = "2015";
$anos[2] = "2014";

$ano_ids = array("3","2"); // Los anos son un seleccionable cuyos ids corresponden con 3 y 2 para 2015 y 2014.

foreach ($ano_ids as $ano_id) {
?>
<p class="h4">A&ntilde;o <?= $anos[$ano_id] ?></p>
<table id="tabla_listado" tabindex="0" width="100%" class="table table-bordered table-striped table-condensed table-hover">
    <thead><tr><th scope="col"><b>Tipo permiso</b></th><th scope="col"><b>Solicitados</b></th><th scope="col"><b>Anulados</b></th><th scope="col"><b>Pdtes. Responsable</b></th><th scope="col"><b>Pdtes. Concejal</b></th><th scope="col"><b>Pdtes. RRHH</b></th><th scope="col"><b>Aceptados</b></th><th scope="col"><b>Restantes</b></th>
	
	<?php
	// Asuntos particulares
	// 2015 -> ano_permiso=3 tipo_permiso=9
	$tipo_permiso = 9;
	$solicitados = 0;
	$anulados = 0;
	$pdtes_responsable = 0;
	$pdtes_concejal = 0;
	$pdtes_rrhh = 0;
	$aceptadas_rrhh = 0;
	$cons = "select *, ((fecha_fin>=fecha_ini)) as mayor, (to_days(fecha_fin)-to_days(fecha_ini)+1) as dias from solicitud_t where tipo_permiso='$tipo_permiso' and user_id='$user_id' and ano_permiso='$ano_id'";
	//echo $cons;
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {
	
	   $dias = $lin['dias'];
	   // Deberiamos corregir para evitar sabados, domingos y dias no habiles.
	   // En asuntos propios no lo estamos haciendo. Ver en vacaciones para ver como se hace.
	
	   $solicitados += $dias;

	   if ($lin[anulada] == "on") { $anulados += $dias; }
	   
	   if ($lin[anulada] == "") { 
			if ($lin[estado_departamento_id] == "3") { $pdtes_responsable += $dias; }
			if ($lin[estado_concejal_id] == "3") { $pdtes_concejal += $dias; }
			if ($lin[estado_rrhh_id] == "3") { $pdtes_rrhh += $dias; }
			
			if ($lin[estado_rrhh_id] == "1") { $aceptadas_rrhh += $dias; }
	   }
	   
	}
	
	// Vemos cuantos dias disponibles hay en este ano
	$dias_disponibles = obtener_campo('num_maximo_dias','maestro_tipo_permisos_num_dias_t','','tipo_permiso_id='.$tipo_permiso.' and ano='.$anos[$ano_id]);
	
	$dias_restantes = $dias_disponibles - $aceptadas_rrhh;
	
	$nombre_permiso = obtener_campo('nombre','maestro_tipo_permisos_t','','id='.$tipo_permiso);
	
	?>
	<tr><td><?= $nombre_permiso ?></td><td><?= $solicitados ?></td><td><?= $anulados ?></td><td><?= $pdtes_responsable ?></td><td><?= $pdtes_concejal ?></td><td><?= $pdtes_rrhh ?></td><td><?= $aceptadas_rrhh ?></td><td><b><?= $dias_restantes ?></b></td></tr>
	
	
	<?php
	// Ausencias por enfermedad
	// 2015 -> ano_permiso=3 tipo_permiso=39
	$tipo_permiso = 39;
	$solicitados = 0;
	$anulados = 0;
	$pdtes_responsable = 0;
	$pdtes_concejal = 0;
	$pdtes_rrhh = 0;
	$aceptadas_rrhh = 0;
	$cons = "select *, ((fecha_fin>=fecha_ini)) as mayor, (to_days(fecha_fin)-to_days(fecha_ini)+1) as dias from solicitud_t where tipo_permiso='$tipo_permiso' and user_id='$user_id' and ano_permiso='$ano_id'";
	//echo $cons;
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {
	
	   $dias = $lin['dias'];
	   // Deberiamos corregir para evitar sabados, domingos y dias no habiles.
	   // En asuntos propios no lo estamos haciendo. Ver en vacaciones para ver como se hace.
	
	   $solicitados += $dias;

	   if ($lin[anulada] == "on") { $anulados += $dias; }
	   
	   if ($lin[anulada] == "") { 
			if ($lin[estado_departamento_id] == "3") { $pdtes_responsable += $dias; }
			if ($lin[estado_concejal_id] == "3") { $pdtes_concejal += $dias; }
			if ($lin[estado_rrhh_id] == "3") { $pdtes_rrhh += $dias; }
			
			if ($lin[estado_rrhh_id] == "1") { $aceptadas_rrhh += $dias; }
	   }
	   
	}
	
	// Vemos cuantos dias disponibles hay en este ano
	$dias_disponibles = obtener_campo('num_maximo_dias','maestro_tipo_permisos_num_dias_t','','tipo_permiso_id='.$tipo_permiso.' and ano='.$anos[$ano_id]);
	
	$dias_restantes = $dias_disponibles - $aceptadas_rrhh;
	
	$nombre_permiso = obtener_campo('nombre','maestro_tipo_permisos_t','','id='.$tipo_permiso);
	
	?>
	<tr><td><?= $nombre_permiso ?></td><td><?= $solicitados ?></td><td><?= $anulados ?></td><td><?= $pdtes_responsable ?></td><td><?= $pdtes_concejal ?></td><td><?= $pdtes_rrhh ?></td><td><?= $aceptadas_rrhh ?></td><td><b><?= $dias_restantes ?></b></td></tr>
	
	
	
	<?php
	// Vacaciones
	// 2015 -> ano_permiso=3 tipo_permiso=8
	$tipo_permiso = 8;
	$solicitados = 0;
	$anulados = 0;
	$pdtes_responsable = 0;
	$pdtes_concejal = 0;
	$pdtes_rrhh = 0;
	$aceptadas_rrhh = 0;
	$cons = "select *, ((fecha_fin>=fecha_ini)) as mayor, (to_days(fecha_fin)-to_days(fecha_ini)+1) as dias from solicitud_t where tipo_permiso='$tipo_permiso' and user_id='$user_id' and ano_permiso='$ano_id'";
	//echo $cons;
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {
	
	   list($fecha_fin,$n) = explode(' ',$lin['fecha_fin']);
	   list($fecha_ini,$n) = explode(' ',$lin['fecha_ini']);
	   
	   $dias = $lin['dias'];
	   // Deberiamos corregir para evitar sabados, domingos y dias no habiles
	   // a) evitar sabados y domingos. Entre la fecha_fin y fecha_ini debemos ir comprobando todos los dias.
	   // Primero comprobar que fecha_fin es mayor o igual que fecha_ini. Luego un bucle.
	   //echo "mayor: $lin[mayor] fecha_ini: $fecha_ini fecha_fin: $fecha_fin";
	   if ($lin['mayor'] == "1") {
			echo "dentro";
			// Hacemos un bucle
			$c = 0;
			while ($fecha_fin != $fecha_ini) {
				// Vamos incrementando fecha_ini hasta llegar a la fecha_fin
				$fecha_ini_tmp = strtotime ( '+1 day' , strtotime ( $fecha_ini ) ) ;
				$fecha_ini = date ( 'Y-m-d' , $fecha_ini_tmp );
				
				$dia_semana = date('w',strtotime($fecha_ini));
				if ($dia_semana == "0" || $dia_semana == "6") {
					// Este dia es una sabado o domingo por lo que restamos uno
					$dias = $dias - 1;
				}
				//echo "$fecha_ini: dia de la semana: $dia_semana<br>";
				
				// Por seguridad
				$c++;
				if ($c >= "365") { break; }
			}
	   
	   }
	   // Fin correccion

	   $solicitados += $dias;
	   if ($lin[anulada] == "on") { $anulados += $lin['dias']; }
	   
	   if ($lin[anulada] == "") { 
			if ($lin[estado_departamento_id] == "3") { $pdtes_responsable += $dias; }
			if ($lin[estado_concejal_id] == "3") { $pdtes_concejal += $dias; }
			if ($lin[estado_rrhh_id] == "3") { $pdtes_rrhh += $dias; }
			
			if ($lin[estado_rrhh_id] == "1") { $aceptadas_rrhh += $dias; }
	   }
	   
	}
	
	// Vemos cuantos dias disponibles hay en este ano
	$dias_disponibles = obtener_campo('num_maximo_dias','maestro_tipo_permisos_num_dias_t','','tipo_permiso_id='.$tipo_permiso.' and ano='.$anos[$ano_id]);
	
	$dias_restantes = $dias_disponibles - $aceptadas_rrhh;
	
	$nombre_permiso = obtener_campo('nombre','maestro_tipo_permisos_t','','id='.$tipo_permiso);
	
	?>
	<tr><td><?= $nombre_permiso ?></td><td><?= $solicitados ?></td><td><?= $anulados ?></td><td><?= $pdtes_responsable ?></td><td><?= $pdtes_concejal ?></td><td><?= $pdtes_rrhh ?></td><td><?= $aceptadas_rrhh ?></td><td><b><?= $dias_restantes ?></b></td></tr>
	
	
	
	
</tbody></table>

<?php
} // del foreach de los anos
?>



            </section>
