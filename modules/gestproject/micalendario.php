<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	
<?php
// filtro buscar_veterinario => b_comercial

if(file_exists("variables_globales_gestproject.php"))
{
	include_once("variables_globales_gestproject.php");
	include_once("funciones.php");
}
else
{
	include_once("../variables_globales_gestproject.php");
	include_once("../funciones.php");
}
if ($enventanita != "si")
{
	echo '
	<script src="dialogos/jquery.js" type="text/javascript"></script>
	<script src="dialogos/jquery.ui.draggable.js" type="text/javascript"></script>
	<script src="dialogos/jquery.alerts.js" type="text/javascript"></script>
	<link href="dialogos/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />';
}

/*
if ($user_id == "")// || $noc == "" )
{
  echo "DEBE INICIAR UNA SESION. <a href=http://www.tacnetting.com>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
*/
if (!isset($user_id)) { echo "*!*DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
?>
	<!-- MODAL !-->
	<!-- FIN MODAL !-->
	<link rel='stylesheet' type='text/css' href='modules/calendario/cupertino/theme.css' />
	<link rel='stylesheet' type='text/css' href='modules/calendario/fullcalendar/fullcalendar.css' />
	<link rel='stylesheet' type='text/css' href='modules/calendario/fullcalendar/fullcalendar.print.css' media='print' />
	<script type='text/javascript' src='modules/calendario/jquery/jquery-1.7.1.min.js'></script>
	<script type='text/javascript' src='modules/calendario/jquery/jquery-ui-1.8.17.custom.min.js'></script>
	<!-- <script type='text/javascript' src='modules/calendario/fullcalendar/fullcalendar.min.js'></script> !-->
	<script type='text/javascript' src='modules/calendario/fullcalendar/fullcalendar.js'></script>
	<script type='text/javascript' src='modules/calendario/fullcalendar/jquery.scrollTo-min.js'></script>
<script type='text/javascript'>
	function mostrar_alerta(mensaje)
	{
		document.getElementById("busquedas_mario2").innerHTML="mensaje:"+mensaje;
	}
	function revalidar()
	{
		$('#calendar').fullCalendar('refetchEvents');
	}
	function abrir_miventana(contenido,titulo,condic)
	{
		try {
			window.parent.abrir_miventana(contenido,titulo,condic);
		}catch (e) {
			alert("error");
		}
	}
<?php
if ($_REQUEST['cerrado_ventana'] == "Si")
{
	echo "
	window.parent.cerrar_ventana_volatil();";
}

// Variables del script
$parametros_nombres = array("b_tipo","b_cliente","b_comercial","b_prov","b_estado");
$parametros_formulario = array();
$parametros_filtro = array(); // parametros que estan en el filtro
// Tipos de parametros
//									Sin nada es que no sirven para los filtros o filtro distinto
// texto;campo_filtro							Es un filtro para texto
// texto								Es un filtro para texto, pero hay que desarrollar el filtrado
// select;campo_filtro							Es un filtro con select
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;desde		Es un filtro para fechas superiores a la introducida
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;hasta		Es un filtro para fechas inferiores a la introducida
$parametros_tipos = array("","","","","");

foreach($parametros_nombres as $indice_parametros => $nombre_param)
{
	list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
	if ($ele == "texto")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; $$nombre_param = str_replace('+',' ',$$nombre_param); }
		else { $$nombre_param = ""; }
	}
	if ($ele == "select")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
	if ($ele == "fecha")
	{
		if ($_REQUEST[$ele3] != "") { $$ele3 = $_REQUEST[$ele3]; }
		else { $$ele3 = ""; }
		if ($_REQUEST[$ele4] != "") { $$ele4 = $_REQUEST[$ele4]; }
		else { $$ele4 = ""; }
		if ($_REQUEST[$ele5] != "") { $$ele5 = $_REQUEST[$ele5]; }
		else { $$ele5 = ""; }
	}
	if ($ele == "")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		//else { $$nombre_param = ""; }
		else { if ($nombre_param == "b_cliente" || $nombre_param == "b_prov") { $$nombre_param = ""; } else { $$nombre_param = "0"; } }
	}
}
//if ($b_comercial == "") { $b_comercial = "0"; }

?>
$(document).ready(function()
{
	$('#calendar').fullCalendar({
/*eventAfterRender: function(event, element, view) {
$(element).css('width','10px');
},*/
//width: 3000,
//contentHeight: 1200,
		height: 750,
		viewDisplay: function(view) {
			//Se ejecuta despues de cargar el calendario.
			var scrollToHourMillis = 500; //tiempo de la animacion en milisegundos
			var viewName = view.name;
			if (viewName === "agendaWeek" || viewName === "agendaDay")
			{
				//ATENCION SI SE CAMBIA EL FORMATO DE 'timeFormat:' EL SIGUIENTE 'HH' TAMBIEN SE DEBE CAMBIAR
				var currentHour = $.fullCalendar.formatDate(new Date(), "HH"); 
				currentHour = ' ' + currentHour; //PARA NO BUSCAR EJ. 10:10AM 11:10AM y solo busque 10AM
				var $viewWrapper = $("div.fc-view-" + viewName + " div.scroll_slot");
				var currentHourLabel = $viewWrapper.find("table tbody tr th:contains('"+ currentHour +"')");
				var hora_actual = $("div.fc-view-" + viewName + " div.scroll_slot th:contains('"+ currentHour +"')").parent();
				$(hora_actual).find("td.fc-widget-content").addClass("div_hora_actual");
				$viewWrapper.animate({scrollTop: 0}, 0, function() {
					var targetOffset = currentHourLabel.offset().top - 70;//70 implica mostrar 60 minutos anteriores
					var scroll = targetOffset - $viewWrapper.offset().top - currentHourLabel.outerHeight();
					$viewWrapper.animate({scrollTop: scroll}, scrollToHourMillis);
				});
			}
		},
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		slotMinutes: 30,
		//defaultView: 'agendaWeek',
		defaultView: 'month',
		allDayText: "DIARIOS",
		timeFormat:  'H:mm {- H:mm}', // default
		agenda: 'hh(:mm)TT', //H implica 24horas hh(:mm)TT
		axisFormat: 'HH:mm', // hh(:mm)TT
		events: {
			url:'modules/calendario/json_eventos.php',
			cache:false,
			type: 'GET',
			allDay: false,
			data: {
				usuario_id: '<?php echo"$user_id"; ?>',
				b_comercial: $('#b_comercial').val()
				// hack en la linea 974 de fullcalendar.js donde cogemos la variable enviamos la variable
				// valor_select_filtro al json_eventos y la cogemos desde select_busquedas_mario2
				//select_busquedas_mario: document.getElementById("select_busquedas_mario2").value
				//select_busquedas_mario: $('#select_busquedas_mario2').val()
				//select_busquedas_mario: document.getElementById("select_busquedas_mario2").value
			},
			error: function() {
				alert('Ha habido un error en la carga de registros!');
			},
			color: 'blue',   // a non-ajax option
			textColor: 'white' // a non-ajax option
		},
		firstHour: 7, 			
		selectable: true,
		selectHelper: true,
		select: function(start, end, allDay) { //CUANDO SE HACE CLICK EN LA AGENDA Crear nuevo registro
			var mes = start.getMonth() + 1;
			mes = '' + mes;
			while (mes.length < 2)
			{
				mes = '0' + mes;
			}
			var tiempo = start.getHours() + ':' + start.getMinutes();// + ':' +start.getSeconds();
			var fecha1 = start.getFullYear() + '-' + mes + '-' + start.getDate();// + ' 00:00:00' ;
			//var tiempo1 =  end.getHours() + ':' + end.getMinutes() + ':' +end.getSeconds();
			//var url_crear = 'modules.php?mod=gestproject&file=form_nuevaentradaagenda2&accion=formcrear&user_id=<?= $user_id ?>&fecha=' + '&fecha=' + encodeURIComponent(fecha1) + '&hora=' + encodeURIComponent(tiempo)+ '&duracion=00:30:00'; // + encodeURIComponent(tiempo1);
			//$('#iframe_modal').attr('src',url_crear);
			//abrir_ventana('<iframe src=url_crear></iframe','AGENDA');
			window.parent.location.href= 'modules.php?mod=gestproject&file=index_agenda_new&accion=formcrearcli&decalendario=Si&fecha=' + encodeURIComponent(fecha1) + '&hora=' + encodeURIComponent(tiempo)+ '&enventanita=<?php echo $enventanita; ?>';
<?php
/*
echo "
			window.parent.location.href= 'modules.php?mod=gestproject&file=index_agenda_new&accion=formcrearcli&decalendario=Si&fecha=' + encodeURIComponent(fecha1) + '&hora=' + encodeURIComponent(tiempo)+ '&enventanita=$enventanita'; ";
*/
?>
/*$('#basic-modal-content_boton_desde_agenda').modal({onClose: function(){
$('#calendar').fullCalendar( 'refetchEvents' );		
$.modal.close();
}});*/
		},
		eventDrop: function(event, delta) {  //CUANDO SE MUEVE UN EVENTO Y SE DEJA EN LA NUEVA HORA
			var mes= event.start.getMonth() + 1;
			var tiempo = event.start.getHours() + ':' + event.start.getMinutes() + ':' +event.start.getSeconds();
			var fecha1 = event.start.getFullYear() + '-' + mes + '-' + event.start.getDate() + ' 00:00:00' ;
			var duracion  = event.end.getHours() + ':' + event.end.getMinutes() + ':' +event.end.getSeconds();
			var url_actualizar = 'modules.php?mod=gestproject&file=proceso_actualizar_evento_embebido&id=' + encodeURIComponent(event.id) + '&fecha=' + encodeURIComponent(fecha1) + '&hora=' + encodeURIComponent(tiempo) + '&duracion=' + encodeURIComponent(duracion);
			$('#calendar').append("<div style=display:none id='div_drop'></div>");
			$('#div_drop').load(url_actualizar, function(){
				$('#div_drop').remove();
				$('#calendar').fullCalendar( 'refetchEvents' );
				$('#calendar').fullCalendar( 'unselect' );
			});
		},
		eventClick: function(calEvent, jsEvent, view) { //CUANDO SE HACE CLICK EN UN EVENTO Modificar un registro
			var url_modificar = "";
			if (calEvent.tipo_agenda_id == 1)
			{
				// visita cliente
				url_modificar = 'modules.php?mod=gestproject&file=index_agenda_new&accion=formmodificarcli&decalendario=Si&enventanita=<?php echo $enventanita; ?>&id=' + calEvent.id;
			} else {
//	modules.php?mod=gestproject&file=index_prov_plan_logis_agenda_new&accion=formmodificar&id=32&pag=0&prov_plan_logis_id=10&agenda=1
				url_modificar = 'modules.php?mod=gestproject&file=index_prov_plan_logis_agenda_new&accion=formmodificar&pag=0&prov_plan_logis_id=' + calEvent.prov_plan_logis_id + '&agenda=1&decalendario=Si&enventanita=<?php echo $enventanita; ?>&id=' + calEvent.id;
			} // del else
			window.parent.location.href = url_modificar;
/*
			if(calEvent.title.indexOf('REUNION')!=-1)
			{
				url_modificar = "";
			} else if (calEvent.title.indexOf('TAREA')!=-1) {
				url_modificar = "";
			} else {
				url_modificar = 'modules.php?mod=gestproject&file=form_modentradaagenda&enventanita=<?php echo"$enventanita";?>&user=<?php echo"$user_id"; ?>&accion=formmodificar&id=' + calEvent.id;
			} // del else
*/
<?php
/*
if ($enventanita != "si")
{
	echo "
		abrir_miventana('<iframe src=modules.php?mod=gestproject&file=form_modentradaagenda&encarga=Si&decalendario=Si&accion=formmodificar&id=' + calEvent.id+'&enventanita=si  width=98% height=98% align=top style=background-color:#FFFFFF ></iframe>','Modificar cita  ','height:540px; width:850px; background-color:white; overflow:auto');";	    
}
else
{
	echo "
		window.location.href= 'modules.php?mod=gestproject&file=form_modentradaagenda&enventanita=$enventanita&decalendario=Si&user=$user_id&accion=formmodificar&id=' + calEvent.id; ";//// + encodeURIComponent(tiempo1);
}
*/
// visita cliente
//	modules.php?mod=gestproject&file=index_agenda_new&accion=formmodificarcli&id=1&pag=&b_mes=07&b_ano=2012
// visita proveedor
//	modules.php?mod=gestproject&file=index_prov_plan_logis_agenda_new&accion=formmodificar&id=32&pag=0&prov_plan_logis_id=10&agenda=1
/*
echo "
			window.parent.location.href= 'modules.php?mod=gestproject&file=index_agenda_new&accion=formmodificarcli&decalendario=Si&enventanita=$enventanita&id=' + calEvent.id;";
*/
?>
//$('#iframe_modal').attr('src',url_modificar);
/*$('#basic-modal-content_boton_desde_agenda').modal({onClose: function(){
$('#calendar').fullCalendar( 'refetchEvents' );
$.modal.close();
}});*/
		},
		eventResize: function(event,dayDelta,minuteDelta,revertFunc) { //CUANDO CAMBIA DE TAMA�O EL EVENTO
			var mes= event.start.getMonth() + 1;
			var tiempo = event.start.getHours() + ':' + event.start.getMinutes() + ':' +event.start.getSeconds();
			var fecha1 = event.start.getFullYear() + '-' + mes + '-' + event.start.getDate() + ' 00:00:00' ;
			var duracion  = event.end.getHours() + ':' + event.end.getMinutes() + ':' +event.end.getSeconds();
			var url_actualizar = 'modules.php?mod=gestproject&file=proceso_actualizar_evento_embebido&id=' + encodeURIComponent(event.id) + '&fecha=' + encodeURIComponent(fecha1) + '&hora=' + encodeURIComponent(tiempo) + '&duracion=' + encodeURIComponent(duracion);
			$('#calendar').append("<div style=display:none id='div_drop'></div>");
			$('#div_drop').load(url_actualizar, function(){
				$('#div_drop').remove();
				$('#calendar').fullCalendar( 'refetchEvents' );
				$('#calendar').fullCalendar( 'unselect' );
//alert('hola');
//$('#calendar').load('modules.php?mod=gestproject&file=micalendario');
			});
		},
		eventMouseover: function(event, domEvent) {
			var layercampana ='';
			if(event.con_campana){
				layercampana ='<div id="campana-layer'+event.id+'" class="fc-transparent" style="position:absolute; width:100%; height:100%; top:-1px; text-align:right; z-index:100" title="'+event.title+'"><img src="images/bell.png" width="14" id="edbut'+event.id+'" border="0" title="Con envio de mensajes" style="padding-right:5px; padding-top:2px;" /></div>';
			}else{
				layercampana ='<div id="campana-layer'+event.id+'" class="fc-transparent" style="position:absolute; width:100%; height:100%; top:-1px; text-align:right; z-index:100" title="'+event.title+'"> </div>';
			}
			$(this).append(layercampana);
			var layer1 = '<div id="events-layer'+event.id+'" class="fc-transparent" style="position:absolute; width:100%; height:100%; top:-1px; text-align:right; z-index:100"><a><img src="images/PNG22/edit22.png" title="editar" width="14" id="edbut'+event.id+'" border="0" style="padding-right:5px; padding-top:2px;" /></a>';
			var layer2 = '<a><img src="images/PNG22/delete22.png" title="borrar" width="14" id="delbut'+event.id+'" border="0" style="padding-right:5px; padding-top:2px;" /></a>';
			var layer3 = '</div>';
			if (!event.borrar) { layer2 = ''; }
//var layer = layer1 + layer2 + layer3;
//$(this).append(layer);
/*$("#delbut"+event.id).hide();
$("#delbut"+event.id).fadeIn(300);
$("#delbut"+event.id).click(function() {
//var url_borrar = 'modules.php?mod=gestproject&file=proceso_borrar_evento&id=' + encodeURIComponent(event.id) ;

var url_borrar = 'modules.php?mod=gestproject&file=form_borrarentradaagenda2&accion=formmodificar&id=' + event.id;
//$('#iframe_modal_borrar').attr('src',url_borrar);				
$('#basic-modal-content_boton_desde_agenda_para_borrar').modal({onClose: function(){
$('#calendar').fullCalendar( 'refetchEvents' );		
$.modal.close();
}});

//$('#basic-modal-content_boton_desde_agenda_para_borrar').load(url_borrar, function(){
//$.modal.close();
//$('#calendar').fullCalendar( 'refetchEvents' );
//});
});*/

/*$("#edbut"+event.id).hide();
$("#edbut"+event.id).fadeIn(300);
$("#edbut"+event.id).click(function() {
var url_modificar = 'modules.php?mod=gestproject&file=form_modentradaagenda2&accion=formmodificar&id=' + event.id;
//$('#iframe_modal').attr('src',url_modificar);
/*$('#basic-modal-content_boton_desde_agenda').modal({onClose: function(){
$('#calendar').fullCalendar( 'refetchEvents' );
$.modal.close();
}});
});*/
		},
		eventMouseout: function(event, jsEvent, view ) {
//$("#events-layer"+event.id+"").hide();
			$("#campana-layer"+event.id+"").remove();
		},
		loading: function(bool) {
			if (bool) $('#loading').show();
			else $('#loading').hide();
		}
	});
//$('#busquedas_mario').html('Visualizar: <select onchange="$(\'#calendar\').fullCalendar(\'refetchEvents\');" id=select_busquedas_mario2><option value=1>Todo</option><option value=2>Mis tareas planificadas</option><option value=3>Mis Reuniones</option><option value=4>Solo los eventos que yo he compartido</option><option value=5>Solo los eventos que me han compartido</option></select>');		
});
</script>
<style type='text/css'>
body {
	margin-top: 0px;
	text-align: center;
	font-size: 12px;
	font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
}
#loading {
	position: absolute;
	top: 30px;
	right: 0px;
}
#calendar {
	width: 100%;
	height: 100%;
	margin: 0 auto;
}
</style>
	</head>
<body>
<div id='loading' style='display:none'><b><font color=red>cargando...</font></b></div>
<br>
<div id='busquedas_mario2' style='font-size:10px;text-align:left'>
<?php

echo "
	<center><b>Buscar por</b></center>
	<table width='100%'>
		<tr style='text-align:center;'>
			<td><b>Tipo</b>:
<select name='b_tipo' id='b_tipo' onchange=\"$('#calendar').fullCalendar('refetchEvents');\">
	<option value='0'"; if($b_tipo == '0') { echo " selected"; } echo ">Todos</option>";
	$consulta_cat = "select * from maestro_tipos_agenda_t order by nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $b_tipo) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select></td>
			<td><b>Cliente</b>: ";
	echo "
<input type=text name='b_cliente' id='b_cliente' onkeyup=\"$('#calendar').fullCalendar('refetchEvents');\" value='".$b_cliente."'>";
/*
	$array_clientes = array();
	$cons = "select distinct cliente_id from agenda_t where cliente_id>0;";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$array_clientes[] = $lin['cliente_id'];
	}
	//echo print_r($array_clientes,true)."<br>";
	$condi1 = CrearCondicionConsulta($array_clientes,"agenda_t","cliente_visitado_id","<>","and");
	$cons = "select distinct cliente_visitado_id from agenda_t where cliente_visitado_id>0 and $condi1;";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$array_clientes[] = $lin['cliente_visitado_id'];
	}
	//echo print_r($array_clientes,true)."<br>";
	if (count($array_clientes) > 0)
	{
		$condi2 = CrearCondicionConsulta($array_clientes,"clientes_t","id","=","or");
	}
	else
	{
		$condi2 = "id='-1'";
	}
	$consulta_cat = "select clientes_t.id, clientes_t.nombre from clientes_t where $condi2 order by clientes_t.nombre;";
	//echo "$consulta_cat<br>";
	echo "
<select name='b_cliente' id='b_cliente' onchange=\"$('#calendar').fullCalendar('refetchEvents');\">
	<option value='0'"; if($b_cliente == '0') { echo " selected"; } echo ">Todos</option>";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $b_cliente) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select>";
*/
	echo "</td>
			<td>&nbsp;</td>
		</tr>
		<tr style='text-align:center;'>
			<td><b>Estado</b>:
<select name='b_estado' id='b_estado' onchange=\"$('#calendar').fullCalendar('refetchEvents');\">
	<option value='0'"; if($b_estado == '0') { echo " selected"; } echo ">Todos</option>";
	$consulta_cat = "select * from maestro_estados_agenda_t order by nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $b_estado) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select></td>
			<td><b>Proveedor</b>: ";
	echo "
<input type=text name='b_prov' id='b_prov' onkeyup=\"$('#calendar').fullCalendar('refetchEvents');\" value='".$b_prov."'>";
/*
	echo "
<select name='b_prov' id='b_prov' onchange=\"$('#calendar').fullCalendar('refetchEvents');\">
	<option value='0'"; if($b_prov == '0') { echo " selected"; } echo ">Todos</option>";
	$consulta_cat = "select proveedores_t.* from proveedores_t join agenda_t on agenda_t.proveedor_id=proveedores_t.id group by proveedores_t.id order by proveedores_t.nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $b_prov) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select>";
*/
	echo "</td>
			<td><b>Comercial</b>:
<select name='b_comercial' id='b_comercial' onchange=\"$('#calendar').fullCalendar('refetchEvents');\">
	<option value='0'"; if($b_comercial == '0') { echo " selected"; } echo ">Todos</option>";
	$consulta_cat = "select usuarios_t.* from usuarios_t join agenda_t on agenda_t.comercial_visita_id=usuarios_t.id group by usuarios_t.id order by usuarios_t.nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $b_comercial) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select></td>
		</tr>
	</table>";
	
	echo "
	<table align='center' cellpadding='5' cellspacing='1' border='0'>
		<tr>
			<td bgcolor='#$agenda_color_cliente_defecto'> <span style='font-weight:bold; color: #ffffff;'>VISITA CLIENTE</span> </td>
			<td bgcolor='#$agenda_color_cliente_cancelada'> <span style='font-weight:bold; color: #ffffff;'>CANCELADA</span> </td>
			<td bgcolor='#$agenda_color_cliente_bloqueada'> <span style='font-weight:bold; color: #ffffff;'>BLOQUEADA</span> </td>
			<td bgcolor='#$agenda_color_cliente_realizada'> <span style='font-weight:bold; color: #ffffff;'>REALIZADA</span> </td>
		</tr>
		<tr>
			<td bgcolor='#$agenda_color_proveedor_defecto'> <span style='font-weight:bold; color: #ffffff;'>VISITA PROVEEDOR</span> </td>
			<td bgcolor='#$agenda_color_proveedor_cancelada'> <span style='font-weight:bold; color: #ffffff;'>CANCELADA</span> </td>
			<td bgcolor='#$agenda_color_proveedor_bloqueada'> <span style='font-weight:bold; color: #ffffff;'>BLOQUEADA</span> </td>
			<td bgcolor='#$agenda_color_proveedor_realizada'> <span style='font-weight:bold; color: #ffffff;'>REALIZADA</span> </td>
		</tr>
	</table>";
?>
</div>

<div id='calendar'></div>

<script type='text/javascript' src='modules/gestproject/demos.js?ver=1.0.3'></script>

</body>
</html>