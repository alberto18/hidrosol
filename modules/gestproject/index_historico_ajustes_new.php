<?php
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=user.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $login</b></center><br>";

// CONFIGURACION
$titulo = "HISTORICO DE AJUSTES";
$titulo_pagina = "HISTORICO DE AJUSTES";
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_historico_ajustes_new";
$tabla = "historico_ajustes_t";
$tabla_articulos = "articulos_t";
$registros_por_pagina = 25;
$script_descarga = "";
$tamano_max_archivo = "500000";
if ($color_entorno != "") { $color_fondo = $color_entorno; $color_fondo_claro = $color_entorno_claro; }
else { $color_fondo = "348209"; $color_fondo_claro = "C3EBC2"; }//$color_fondo = "97c00e"; $color_fondo_claro = "dcfb73"; }

//titulo pagina
echo "
<script type=\"text/JavaScript\">document.title = '".$titulo_pagina."';</script>";

echo "
<table width='100%' height='700' border='0' cellpadding='0' cellspacing='0' class='text' align='center'>
	<tr valign='top'>
		<td width=100% align=left>
";

if (PermisosSecciones($user_id, $script, array()) == 1)
{
// textos de la pagina
$texto_crear = "Crear entrada";
$texto_listado_general = "Listar todos los ajustes";
$texto_creado = "Entrada creada";
$texto_modificado = "Entrada modificada";
$texto_borrado = "Entrada borrada";
$nombre_objeto = " UNA ENTRADA";

// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('fecha','almacen_id','articulo_id',
'unidades_antes','unidades_despues'
);
// indica la tabla de donde es el campo
$tablas_campos_listado = array($tabla,$tabla,$tabla,
$tabla,$tabla
);

$nombres_listado = array('Fecha','Almac&eacute;n','(Cod.) Art&iacute;culo',
'Unidades antes','Unidades despues'
);

$campos_necesarios_listado = array('id','estanteria_antes','hueco_antes','piso_antes','palet_antes',
'estanteria_despues','hueco_despues','piso_despues','palet_despues');
$tablas_campos_necesarios = array($tabla,$tabla,$tabla,$tabla,$tabla,
$tabla,$tabla,$tabla,$tabla);

// Si es necesaria una decodificacion en el listado
// '' no es necesaria
// si;tabla;campo_mostrar;campo_buscar
// si;date
// si;datetime
// si;checkbox
// si;time
$campos_listado_decod = array ('si;date','si;maestro_almacenes_t;nombre;id','si;articulos_t;nombre;id',
'',''
);

// Campo para la busqueda
$campo_busqueda = "fecha";

$campos_listado_sub = array ('ancho','largo','unidades_antes','unidades_despues','user_id');
$tablas_campos_listado_sub = array($tabla,$tabla,$tabla,$tabla,$tabla);
$nombres_listado_sub = array('Ancho','Largo','Unid. antes','Unid. desp','Usuario');

// Variables del script
$parametros_nombres = array("accion","pag","b_art","fecha1","fecha2",
"b_familia");
$parametros_formulario = array("pag","b_art","fecha1","fecha2",
"b_familia");
// Tipos de parametros
//									Sin nada es que no sirven para los filtros o filtro distinto
// texto;campo_filtro							Es un filtro para texto
// texto								Es un filtro para texto, pero hay que desarrollar el filtrado
// select;campo_filtro							Es un filtro con select
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;desde		Es un filtro para fechas superiores a la introducida
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;hasta		Es un filtro para fechas inferiores a la introducida
$parametros_tipos = array("","","select;articulo_id","fecha;fecha;ano1;mes1;dia1;desde","fecha;fecha;ano2;mes2;dia2;hasta",
"");
foreach($parametros_nombres as $indice_parametros => $nombre_param)
{
	$array_ele = explode(';', $parametros_tipos[$indice_parametros]);
	if ($array_ele[0] == "texto")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; $$nombre_param = str_replace('+',' ',$$nombre_param); }
		else { $$nombre_param = ""; }
	}
	if ($array_ele[0] == "select")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
	if ($array_ele[0] == "fecha")
	{
		if ($_REQUEST[$array_ele[2]] != "") { $$array_ele[2] = $_REQUEST[$array_ele[2]]; }
		else { $$array_ele[2] = ""; }
		if ($_REQUEST[$array_ele[3]] != "") { $$array_ele[3] = $_REQUEST[$array_ele[3]]; }
		else { $$array_ele[3] = ""; }
		if ($_REQUEST[$array_ele[4]] != "") { $$array_ele[4] = $_REQUEST[$array_ele[4]]; }
		else { $$array_ele[4] = ""; }
	}
	if ($array_ele[0] == "")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
}
if ($pag == "") { $pag = "0"; }

// COMIENZA EL SCRIPT

if ($accion != "formborrar" && $accion != "formcrear" && $accion != "formmodificar")
{
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			$array_ele = explode(';', $parametros_tipos[$indice_parametros]);
			if ($array_ele[0] == "fecha")
			{
				if ($_REQUEST[$array_ele[2]] != "") { $parametros .= "&".$array_ele[2]."=".$_REQUEST[$array_ele[2]]; }
				if ($_REQUEST[$array_ele[3]] != "") { $parametros .= "&".$array_ele[3]."=".$_REQUEST[$array_ele[3]]; }
				if ($_REQUEST[$array_ele[4]] != "") { $parametros .= "&".$array_ele[4]."=".$_REQUEST[$array_ele[4]]; }
			}
			else
			{
				if ($$nombre_param != "") { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "
<center><b>$titulo</b><br>";
//	echo "[<a href='$enlacevolver"."$script&accion=formcrear$parametros'>$texto_crear</a>]<br>";
	echo "
	<b>Buscar por</b>
	<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<form name=form_buscar method=post action='$enlacevolver"."$script'>
	<input type=hidden name=pag value=0>";
	echo "
	<tr align='center'>
		<td colspan='2'><b>Art&iacute;culo</b>: <select name=b_art><option value=''>Seleccione una opcion</option>";
	$consultamod = "select articulos_t.* from articulos_t join $tabla on $tabla.articulo_id=articulos_t.id order by nombre;";// where compuesto_id<>'1'
	$resultadomod = mysql_query($consultamod) or die("$consultamod<br>La consulta fall&oacute;: " . mysql_error());
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC))
	{
		echo "<option value='$lineasmod[id]'"; if ($lineasmod[id] == $b_art) { echo " selected"; }
		echo " title='$lineasmod[nombre]'>$lineasmod[codigo] ";
		if (strlen($lineasmod[nombre]) > 90) { echo substr($lineasmod[nombre],0,90)."..."; }
		else { echo "$lineasmod[nombre]"; }
		echo "</option>";
	}
	echo "</select></td>
	</tr>
	<tr align='center'>
		<td><b>Familia</b>: <select name=b_familia><option value=''>Seleccione una opcion</option>";
	$consultamod = "select id, nombre from maestro_familias_t order by nombre;";
	$resultadomod = mysql_query($consultamod) or die("$consultamod<br>La consulta fall&oacute;: " . mysql_error());
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC))
	{
		echo "<option value='$lineasmod[id]'"; if ($lineasmod[id] == $b_familia) { echo " selected"; }
		echo ">$lineasmod[nombre]</option>";
	}
	echo "</select></td>
		<td><input type=submit value='Filtrar'></td>
	</tr>
	<tr align='center'>
		<td><b>Fecha desde</b>: D: <input type='text' name='dia1' size='2' maxlength='2' value='$dia1' onkeyup='validar(dia1);'> 
M: <input type='text' name='mes1' size='2' maxlength='2' value='$mes1' onkeyup='validar(mes1);'> 
A: <input type='text' name='ano1' size='4' maxlength='4' value='$ano1' onkeyup='validar(ano1);'></td>
		<td><b>Fecha hasta</b>: D: <input type='text' name='dia2' size='2' maxlength='2' value='$dia2' onkeyup='validar(dia2);'> 
M: <input type='text' name='mes2' size='2' maxlength='2' value='$mes2' onkeyup='validar(mes2);'> 
A: <input type='text' name='ano2' size='4' maxlength='4' value='$ano2' onkeyup='validar(ano2);'></td>
		<td>&nbsp;</td>
	</tr>
	</form>
	</table>
	<a href='$enlacevolver"."$script&pag=0'>$texto_listado_general</a>";
	echo "
</center>";
}

// OBTENEMOS EL LISTADO DE REGISTROS 
if ($accion != "formborrar" && $accion != "formcrear" && $accion != "formmodificar")
{
	$condiciones = "";
	$parametros = "";
	$join = "";
	$group = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
		if ($ele == "texto" && $ele2 != "")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2 like '%".$$nombre_param."%' ";
				$$nombre_param = str_replace(' ','+',$$nombre_param); 
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "select")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2='".$$nombre_param."' ";
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "fecha")
		{
			if ($$ele3 != "" && $$ele4 != "" && $$ele5 != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				($ele6 == "desde" ? $condiciones .= " $tabla.$ele2>='".$$ele3."-".$$ele4."-".$$ele5." 00:00:00' " : $condiciones .= " $tabla.$ele2<='".$$ele3."-".$$ele4."-".$$ele5." 23:59:59' ");
				$parametros .= "&$ele3=".$$ele3."&$ele4=".$$ele4."&$ele5=".$$ele5;
			}
		}
	}
	if ($b_familia != "")
	{
		$join = " join $tabla_articulos on $tabla_articulos.id=$tabla.articulo_id ";
		$group = " group by $tabla.id ";
		if ($condiciones != "") { $condiciones .= " and "; }
		$condiciones .= " $tabla_articulos.familia_id='".$b_familia."' ";
		$parametros .= "&b_familia=".$b_familia;
	}
	$string_para_select = "";
	foreach($campos_necesarios_listado as $indice => $campo)
	{
		$string_para_select .= $tablas_campos_necesarios[$indice].".$campo,";
	}
 	echo "
<table width='100%'>
	<tr bgcolor='#$color_fondo'>";
	$columnas = 0;
	foreach ($campos_listado as $campo) {
		echo "<td><a class='listadoTabla' href='$enlacevolver"."$script&orderby=$campo&pag=$pag$parametros'>$nombres_listado[$columnas]</a></td>";
		$string_para_select .= " ".$tablas_campos_listado[$columnas].".$campo,";
		$columnas++;
	}
	// Eliminamos el ultimo caracter
	$string_para_select = substr($string_para_select,0,-1);
	//$columnas += 1;
	//echo "<td><font color='#ffffff'><b>Acciones</b></font></td>";
	echo "</tr>";

	$consulta  = "select $string_para_select from $tabla $join";
	if ($condiciones != "") { $consulta .= " where $condiciones "; }
	// Vemos si existe un orderby
	if ($_REQUEST[orderby] != "") { $order = " order by $tabla.$_REQUEST[orderby]"; $parametros .= "&orderby=".$_REQUEST[orderby]; }
	else { $order = " order by $tabla.$campo_busqueda desc";  }
	if ($pag != "") {
		// la primera pagina es la 0
		$inicio = $pag*$registros_por_pagina;
		$limit = " limit $inicio,$registros_por_pagina";
	}
	else { $limit = ""; }
/*
	$limit = "";
	$group = " group by $tabla.fecha, $tabla.almacen_id, $tabla.articulo_id";
*/
	$consulta .= "$group $order $limit;";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		//if ($_GET[id] != "" && $linea[id] == $_GET[id]) { $bgcolor = " bgcolor='#$color_fondo_claro'"; }
		//else { $bgcolor = ""; }
		
		echo "\n	<tr valign='top' $bgcolor>";
		foreach ($campos_listado as $cuenta_campos => $campo)
		{
			$nombre = "";
			list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod[$cuenta_campos]);
			if ($ele != 'si')
			{
				$nombre = "$linea[$campo]";
			}
			elseif ($ele2 == "date")
			{
				if ($linea[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
				else { $nombre = date("d-m-Y",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "datetime")
			{
				if ($linea[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
				else { $nombre = date("d-m-Y H:i",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "checkbox")
			{
				($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
			}
			elseif ($ele2 == "time")
			{
				list($temp1, $temp2, $temp3) = explode(":",$linea[$campo]);
				$nombre = $temp1.":".$temp2;
			}
			elseif ($linea[$campo] != "")
			{
				$consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre = $lineaselect[$ele3];
					if ($campo == "articulo_id")
					{
						$nombre = "($lineaselect[codigo]) ".$nombre;
					}
				}
			}
			if ($campo == "unidades_antes")
			{
				$nombre .= " <span title='Estanteria: $linea[estanteria_antes], Hueco: $linea[hueco_antes], Piso: $linea[piso_antes], Palet: $linea[palet_antes]'>(info)</span>";
			}
			elseif ($campo == "unidades_despues")
			{
				$nombre .= " <span title='Estanteria: $linea[estanteria_despues], Hueco: $linea[hueco_despues], Piso: $linea[piso_despues], Palet: $linea[palet_despues]'>(info)</span>";
			}
			echo "<td>$nombre</td>";
		}
/*
		echo "
		<td>
		</td>";
*/
		echo "
	</tr>";
		echo "\n	<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas'></td></tr>";
	}
	echo "
</table>";
	if ($pag != "")
	{
		$pag_visual = $pag+1;
		// Visualizamos las paginas existentes
		$consulta2  = "select count(distinct $tabla.id) as num from $tabla $join";
		if ($condiciones != "") { $consulta2 .= " where $condiciones;"; } else { $consulta2 .= ";"; }// $group
		$resultado2 = mysql_query($consulta2) or die("$consulta2, La consulta fall&oacute;: " . mysql_error());
		//echo "$consulta2<br>";
		while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC)) {
			$exp = "$linea2[num]";
		}
		if ($exp > $registros_por_pagina)
		{
			PaginadoListado($pag, $enlacevolver.$script, $parametros, $exp, $registros_por_pagina);
		}
	}
	echo "<hr>";
}
// FIN DE OBTENER EL LISTADO INICIAL

} // fin de se tiene permiso para este script
else
{
	echo "No se tiene permiso para ver esta seccion.<br><br>";
}
echo "
		</td>
	</tr>
</table>
";
?>