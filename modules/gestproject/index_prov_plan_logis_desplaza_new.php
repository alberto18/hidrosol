<?php 
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";

// CONFIGURACION
$titulo = "GESTION DE DESPLAZAMIENTOS DEL PLANNING DEL DIA ";
$titulo_pagina = "DESPLAZAMIENTOS DE PLANNING";
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_prov_plan_logis_desplaza_new";
$script_logistica = "index_prov_planning_logistica_new";
$tabla = "prov_plan_logis_desplaza_t";
$tabla_padre = "prov_planning_logistica_t";
$registros_por_pagina = 10;
$script_descarga = "";
$tamano_max_archivo = "16000000";
if ($color_entorno != "") { $color_fondo = $color_entorno; $color_fondo_claro = $color_entorno_claro; }
else { $color_fondo = "97c00e"; $color_fondo_claro = "dcfb73"; }

$texto_textarea = '<br type="_moz" />';

echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo "
<table width='100%' height='700' border='0' cellpadding='0' cellspacing='0' class='text' align='center'>
	<tr valign='top'>
		<td width=100% align=left>
";

if (PermisosSecciones($user_id, $script, array()) == 1)
{

// textos de la pagina
$texto_crear = "Crear desplazamiento";
$texto_listado_general = "Listar todos los desplazamiento";
$texto_creado = "Desplazamiento creado";
$texto_modificado = "Desplazamiento modificado";
$texto_borrado = "Desplazamiento borrado";
$nombre_objeto = " UN DESPLAZAMIENTO";

// Campos con los que se trabajara en el insert y modify
$campos_col1 = array('hora_inicio','tipo_transporte_id','hora_fin',
'avion_vuelo','avion_trayecto','barco_compania','barco_trayecto');
$campos_col2 = array('coche_compania','coche_modelo','coche_recogida');

// Nombres que apareceran en las columnas de los formularios
$nombres_col1 = array('Hora inicio','Tipo transporte','Hora fin (excepto coche)',
'Vuelo','Trayecto','Compa&ntilde;ia','Trayecto');
$nombres_col2 = array('Compa&ntilde;ia alquiler','Modelo alquilado','Direccion recogida');

// Tipos. Cada campo puede ser:
// text;readonly;size (60)
// password;readonly;size
// textarea;readonly;row;col (10;60)
// select;readonly;tabla;campo_mostrar;campo_para_value;campo_condicion
// checkbox;readonly
// date;readonly
// hidden;tabla;campo_mostrar;campo_para_value;campo_para_order;nuevo_nombre;campo_depende;campo_filtro mostrara un select que saltara y ademas tendra otro campo oculto con el valor
// dni;readonly
// telefono;readonly
// cuenta;readonly
// email;readonly;size
// float;readonly;size
// multiple;readonly;tabla;campo_mostrar;campo_para_value;campo_para_order;nombre_checkbox
// file poner el campo "nombre_fichero", actualiza nombre_fichero, tipo_fichero, peso_fichero, fichero_binario, fecha_subida, user_id
//   Los campos en la tabla a a�adir serian:
//  | nombre_fichero              | varchar(200)
//  | tipo_fichero                | varchar(20)
//  | peso_fichero                | varchar(20)
//  | fecha_subida                | datetime
//  | user_id                     | int(11)
//  | fichero_binario             | blob
// ss;readonly
// calendar;readonly
// time;readonly
// numerico;readonly;size
// fileCarp poner el campo "nombre_fichero", actualiza solo nombre_fichero y sube a la carpeta definida
$tipos_col1  = array('time;0','select;0;maestro_tipos_transportes_t;nombre;id;nombre','time;0',
'text;0;40','text;0;40','text;0;40','text;0;40');
$tipos_col2  = array('text;0;40','text;0;40','text;0;40');

// Separadores o titulos
$titulos_col1 = array('','','',
'Datos del vuelo','','Datos del barco','');
$titulos_col2 = array('Datos del coche alquilado','','');

// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('hora_inicio','tipo_transporte_id');
// indica la tabla de donde es el campo
$tablas_campos_listado = array($tabla,$tabla);

$nombres_listado = array('Hora inicio','Tipo transporte');

$campos_necesarios_listado = array('id');
$tablas_campos_necesarios = array($tabla);

// Si es necesaria una decodificacion en el listado
// '' no es necesaria
// si;tabla;campo_mostrar;campo_buscar
// si;date
// si;datetime
// si;checkbox
// si;time
$campos_listado_decod = array ('si;time','si;maestro_tipos_transportes_t;nombre;id');

// Campo para la busqueda
$campo_busqueda = "hora_inicio";

// Campo padre
$usa_padre = 1;
$campopadre = "prov_planning_logistica_id";

// Variables del script
$parametros_nombres = array("accion","pag",$campopadre,"b_nombre");
$parametros_formulario = array("pag",$campopadre,"b_nombre");
$parametros_filtro = array("b_nombre"); // parametros que estan en el filtro
// Tipos de parametros
//									Sin nada es que no sirven para los filtros o filtro distinto
// texto;campo_filtro							Es un filtro para texto
// texto								Es un filtro para texto, pero hay que desarrollar el filtrado
// select;campo_filtro							Es un filtro con select
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;desde		Es un filtro para fechas superiores a la introducida
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;hasta		Es un filtro para fechas inferiores a la introducida
$parametros_tipos = array("","","","texto;nombre");

foreach($parametros_nombres as $indice_parametros => $nombre_param)
{
	list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
	if ($ele == "texto")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; $$nombre_param = str_replace('+',' ',$$nombre_param); }
		else { $$nombre_param = ""; }
	}
	if ($ele == "select")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
	if ($ele == "fecha")
	{
		if ($_REQUEST[$ele3] != "") { $$ele3 = $_REQUEST[$ele3]; }
		else { $$ele3 = ""; }
		if ($_REQUEST[$ele4] != "") { $$ele4 = $_REQUEST[$ele4]; }
		else { $$ele4 = ""; }
		if ($_REQUEST[$ele5] != "") { $$ele5 = $_REQUEST[$ele5]; }
		else { $$ele5 = ""; }
	}
	if ($ele == "")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
}
if ($pag == "") { $pag = "0"; }

// ACCION CREAR
if ($accion == "accioncrear")
{
	$campo = "hora_inicio";
	$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
	$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
	$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
	if (($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
	{
		$valor_hora_inicio = $valor_hora;
		$valor_minutos_inicio = $valor_minutos;
		if ($_POST['tipo_transporte_id'] > 0)
		{
			if ($_POST['tipo_transporte_id'] == 1 || $_POST['tipo_transporte_id'] == 3)
			{
				// si es tipo transporte avion o barco la hora de fin es obligatoria
				$campo = "hora_fin";
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{
					$valor_hora_fin = $valor_hora;
					$valor_minutos_fin = $valor_minutos;
					if (strtotime(date("Y-m-d")." $valor_hora_fin:$valor_minutos_fin") <= strtotime(date("Y-m-d")." $valor_hora_inicio:$valor_minutos_inicio"))
					{
						echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora fin debe ser superior al de hora inicio.'); history.back(); </SCRIPT>";
						exit;
					}
				}
				else
				{
					echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora fin se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>";
					exit;
				}
			}
//--------------------------------------------------------
		if ($usa_padre != 0)
		{
			$consulta2  = "insert into $tabla set $campopadre='".$$campopadre."',";
		} else {
			$consulta2  = "insert into $tabla set ";
		}
		// Preparamos los campos a insertar
		foreach ($campos_col1 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$cuenta_campos]);
			$nombre_campo = $nombres_col1[$cuenta_campos];
			if ($ele == "date")
			{
				$campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
				if ((($_POST[$campo_fecha_dia] == "00" || $_POST[$campo_fecha_dia] == "") && ($_POST[$campo_fecha_mes] == "00" || $_POST[$campo_fecha_mes] == "") && ($_POST[$campo_fecha_ano] == "0000" || $_POST[$campo_fecha_ano] == "")) || ($_POST[$campo_fecha_dia] > 0 && $_POST[$campo_fecha_dia] < 32 && $_POST[$campo_fecha_mes] > 0 && $_POST[$campo_fecha_mes] < 13 && $_POST[$campo_fecha_ano] > 0))
				{ $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD-MM-AAAA.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "telefono")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg('([0-9]{9})', $_POST[$campo])) { $consulta2 .= " $campo=\"$_POST[$campo]\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo telefonico ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 123456789.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "dni")
			{
				if ($_POST[$campo] != "")
				{
					if (strlen($_POST[$campo]) == 9 && (ereg('([0-9]{8}[a-zA-Z]{1})', $_POST[$campo]) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtoupper($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de DNI ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12345678A o A123456B.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "cuenta")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo"; $campo4 = "cuatro$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "" && $_POST[$campo4] == "") || (strlen($_POST[$campo1]) == 4 && strlen($_POST[$campo2]) == 4 && strlen($_POST[$campo3]) == 2 && strlen($_POST[$campo4]) == 10))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]$_POST[$campo4]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de cuenta corriente ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 1234-5678-90-1234567890.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "email")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg($patron_comprobacion_email, strtolower($_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtolower($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de email ($nombre_campo) se ha introducido con un formato erroneo.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "multiple")
			{
				$array_multiple_ids = array();
				$consulta_elementos = "select * from $ele3;";
				$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
				{
					$array_multiple_ids[] = $linea_elementos[$ele5];
				}
				$valor_campo = "";
				foreach ($array_multiple_ids as $multiple_id)
				{
					$nombre_campo = $ele7.$multiple_id;
					if ($_POST[$nombre_campo] == "on")
					{
						if ($valor_campo != "") { $valor_campo .= ";"; }
						$valor_campo .= $multiple_id;
					}
				}
				$consulta2 .= " $campo=\"$valor_campo\",";
			}
			elseif ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						if ($extension == "jpg" || $extension == "png")
						{
							$binario_peso = $_FILES[$campo]['size']; 
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='#id#slider.$extension',";
							}
						}
						else
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) debe estar en formato jpg o png. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				} // fin if files name != ""
				else
				{
					$consulta2 .= " nombre_fichero='',";
				}
			}
			elseif ($ele == "file")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						$existe = 0;
						if (in_array($extension,$array_ext_excluidas)) { $existe = 1; }
						if ($existe == 1)
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) tiene un formato no permitido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
						else
						{
							$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
							$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
							
							$binario_peso = $_FILES[$campo]['size']; 
							$binario_tipo = $_FILES[$campo]['type']; 
							
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='$nombre_fichero', tipo_fichero='$binario_tipo', peso_fichero='$binario_peso', fichero_binario='$binario_contenido', fecha_subida=now(), user_id='$user_id',";
							}
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				}
			}
			elseif ($ele == "ss")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "") || (strlen($_POST[$campo1]) == 2 && strlen($_POST[$campo2]) == 8 && strlen($_POST[$campo3]) == 2))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de seguridad social ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12-34567890-12.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "calendar")
			{
				if ($_POST[$campo] != "")
				{
					list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
					if ($ele2 != 1)
					{
						if (($dia == 0 && $mes == 0 && $ano == 0) || ($dia != "" && strlen($dia) == 2 && $dia > 0 && $dia < 32 && $mes != "" && strlen($mes) == 2 && $mes > 0 && $mes < 13 && $ano != "" && strlen($ano) == 4 && $ano > 1900))
						{
							$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
						}
						else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD/MM/AAAA.'); history.back(); </SCRIPT>"; exit; }
					}
					else
					{
						$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
					}
				}
			}
			elseif ($ele == "time")
			{
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora == "00" && $valor_minutos == "00") || ($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{ $consulta2 .= " $campo=\"$valor_hora:$valor_minutos\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "textarea") { $consulta2 .= " $campo='".str_replace("'","",str_replace($texto_textarea,"",$_POST[$campo]))."',"; }
			else { $consulta2 .= " $campo='".str_replace("'","",$_POST[$campo])."',"; }
		}
		foreach ($campos_col2 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col2[$cuenta_campos]);
			$nombre_campo = $nombres_col2[$cuenta_campos];
			if ($ele == "date")
			{
				$campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
				if ((($_POST[$campo_fecha_dia] == "00" || $_POST[$campo_fecha_dia] == "") && ($_POST[$campo_fecha_mes] == "00" || $_POST[$campo_fecha_mes] == "") && ($_POST[$campo_fecha_ano] == "0000" || $_POST[$campo_fecha_ano] == "")) || ($_POST[$campo_fecha_dia] > 0 && $_POST[$campo_fecha_dia] < 32 && $_POST[$campo_fecha_mes] > 0 && $_POST[$campo_fecha_mes] < 13 && $_POST[$campo_fecha_ano] > 0))
				{ $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD-MM-AAAA.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "telefono")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg('([0-9]{9})', $_POST[$campo])) { $consulta2 .= " $campo=\"$_POST[$campo]\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo telefonico ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 123456789.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "dni")
			{
				if ($_POST[$campo] != "")
				{
					if (strlen($_POST[$campo]) == 9 && (ereg('([0-9]{8}[a-zA-Z]{1})', $_POST[$campo]) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtoupper($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de DNI ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12345678A o A123456B.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "cuenta")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo"; $campo4 = "cuatro$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "" && $_POST[$campo4] == "") || (strlen($_POST[$campo1]) == 4 && strlen($_POST[$campo2]) == 4 && strlen($_POST[$campo3]) == 2 && strlen($_POST[$campo4]) == 10))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]$_POST[$campo4]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de cuenta corriente ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 1234-5678-90-1234567890.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "email")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg($patron_comprobacion_email, strtolower($_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtolower($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de email ($nombre_campo) se ha introducido con un formato erroneo.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "multiple")
			{
				$array_multiple_ids = array();
				$consulta_elementos = "select * from $ele3;";
				$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
				{
					$array_multiple_ids[] = $linea_elementos[$ele5];
				}
				$valor_campo = "";
				foreach ($array_multiple_ids as $multiple_id)
				{
					$nombre_campo = $ele7.$multiple_id;
					if ($_POST[$nombre_campo] == "on")
					{
						if ($valor_campo != "") { $valor_campo .= ";"; }
						$valor_campo .= $multiple_id;
					}
				}
				$consulta2 .= " $campo=\"$valor_campo\",";
			}
			elseif ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						if ($extension == "jpg" || $extension == "png")
						{
							$binario_peso = $_FILES[$campo]['size']; 
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='#id#slider.$extension',";
							}
						}
						else
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) debe estar en formato jpg o png. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				} // fin if files name != ""
				else
				{
					$consulta2 .= " nombre_fichero='',";
				}
			}
			elseif ($ele == "file")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						$existe = 0;
						if (in_array($extension,$array_ext_excluidas)) { $existe = 1; }
						if ($existe == 1)
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) tiene un formato no permitido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
						else
						{
							$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
							$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
							
							$binario_peso = $_FILES[$campo]['size']; 
							$binario_tipo = $_FILES[$campo]['type']; 
							
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='$nombre_fichero', tipo_fichero='$binario_tipo', peso_fichero='$binario_peso', fichero_binario='$binario_contenido', fecha_subida=now(), user_id='$user_id',";
							}
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				}
			}
			elseif ($ele == "ss")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "") || (strlen($_POST[$campo1]) == 2 && strlen($_POST[$campo2]) == 8 && strlen($_POST[$campo3]) == 2))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de seguridad social ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12-34567890-12.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "calendar")
			{
				if ($_POST[$campo] != "")
				{
					list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
					if ($ele2 != 1)
					{
						if (($dia == 0 && $mes == 0 && $ano == 0) || ($dia != "" && strlen($dia) == 2 && $dia > 0 && $dia < 32 && $mes != "" && strlen($mes) == 2 && $mes > 0 && $mes < 13 && $ano != "" && strlen($ano) == 4 && $ano > 1900))
						{
							$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
						}
						else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD/MM/AAAA.'); history.back(); </SCRIPT>"; exit; }
					}
					else
					{
						$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
					}
				}
			}
			elseif ($ele == "time")
			{
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora == "00" && $valor_minutos == "00") || ($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{ $consulta2 .= " $campo=\"$valor_hora:$valor_minutos\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "textarea") { $consulta2 .= " $campo='".str_replace("'","",str_replace($texto_textarea,"",$_POST[$campo]))."',"; }
			else { $consulta2 .= " $campo='".str_replace("'","",$_POST[$campo])."',"; }
		}
		// Eliminamos la coma final
		$consulta2 = substr($consulta2, 0, strlen($consulta2)-1);
		$consulta2 .= ';';
		//echo "$consulta2";
		
		$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());
		
		// RENOMBRAR CORRECTAMENTE LA IMAGEN DE FONDO
		$ultima_coleccion_id = mysql_insert_id();
		foreach ($campos_col1 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$cuenta_campos]);
			if ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$consulta_datos = "select $campo, id from $tabla where id='$ultima_coleccion_id';";
					$resultado_datos = mysql_query($consulta_datos) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_datos = mysql_fetch_array($resultado_datos, MYSQL_ASSOC))
					{
						$nuevo_nombre = str_replace('#id#',$linea_datos['id'],$linea_datos[$campo]);
						
						$consulta_datos2 = "update $tabla set $campo='$nuevo_nombre' where id='$linea_datos[id]';";
						$resultado_datos2 = mysql_query($consulta_datos2) or die("La consulta fall&oacute;: " . mysql_error());
						
						$uploadfile = $uploaddir_contenidos.$nuevo_nombre;
						move_uploaded_file($_FILES[$campo]['tmp_name'], $uploadfile);
					}
				}
			}
		}
		foreach ($campos_col2 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col2[$cuenta_campos]);
			if ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$consulta_datos = "select $campo, id from $tabla where id='$ultima_coleccion_id';";
					$resultado_datos = mysql_query($consulta_datos) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_datos = mysql_fetch_array($resultado_datos, MYSQL_ASSOC))
					{
						$nuevo_nombre = str_replace('#id#',$linea_datos['id'],$linea_datos[$campo]);
						
						$consulta_datos2 = "update $tabla set $campo='$nuevo_nombre' where id='$linea_datos[id]';";
						$resultado_datos2 = mysql_query($consulta_datos2) or die("La consulta fall&oacute;: " . mysql_error());
						
						$uploadfile = $uploaddir_contenidos.$nuevo_nombre;
						move_uploaded_file($_FILES[$campo]['tmp_name'], $uploadfile);
					}
				}
			}
		}
		
		echo "<b>$texto_creado</b>";
		$accion = "formcrear";
//--------------------------------------------------------
		}
		else
		{
			echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo tipo transporte es obligatorio. No se puede continuar.'); history.back(); </SCRIPT>";
			exit;
		}
	}
	else
	{
		echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora inicio se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>";
		exit;
	}
}
// FIN ACCION CREAR

// ACCION MODIFICAR 
if ($accion == "accionmodificar")
{
	$campo = "hora_inicio";
	$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
	$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
	$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
	if (($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
	{
		$valor_hora_inicio = $valor_hora;
		$valor_minutos_inicio = $valor_minutos;
		if ($_POST['tipo_transporte_id'] > 0)
		{
			if ($_POST['tipo_transporte_id'] == 1 || $_POST['tipo_transporte_id'] == 3)
			{
				// si es tipo transporte avion o barco la hora de fin es obligatoria
				$campo = "hora_fin";
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{
					$valor_hora_fin = $valor_hora;
					$valor_minutos_fin = $valor_minutos;
					if (strtotime(date("Y-m-d")." $valor_hora_fin:$valor_minutos_fin") <= strtotime(date("Y-m-d")." $valor_hora_inicio:$valor_minutos_inicio"))
					{
						echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora fin debe ser superior al de hora inicio.'); history.back(); </SCRIPT>";
						exit;
					}
				}
				else
				{
					echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora fin se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>";
					exit;
				}
			}
//--------------------------------------------------------
		$consulta2  = "update $tabla set ";
		// Preparamos los campos a insertar
		foreach ($campos_col1 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$cuenta_campos]);
			$nombre_campo = $nombres_col1[$cuenta_campos];
			if ($ele == "date")
			{
				$campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
				if ((($_POST[$campo_fecha_dia] == "00" || $_POST[$campo_fecha_dia] == "") && ($_POST[$campo_fecha_mes] == "00" || $_POST[$campo_fecha_mes] == "") && ($_POST[$campo_fecha_ano] == "0000" || $_POST[$campo_fecha_ano] == "")) || ($_POST[$campo_fecha_dia] > 0 && $_POST[$campo_fecha_dia] < 32 && $_POST[$campo_fecha_mes] > 0 && $_POST[$campo_fecha_mes] < 13 && $_POST[$campo_fecha_ano] > 0))
				{ $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD-MM-AAAA.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "telefono")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg('([0-9]{9})', $_POST[$campo])) { $consulta2 .= " $campo=\"$_POST[$campo]\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo telefonico ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 123456789.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "dni")
			{
				if ($_POST[$campo] != "")
				{
					if (strlen($_POST[$campo]) == 9 && (ereg('([0-9]{8}[a-zA-Z]{1})', $_POST[$campo]) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtoupper($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de DNI ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12345678A o A123456B.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "cuenta")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo"; $campo4 = "cuatro$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "" && $_POST[$campo4] == "") || (strlen($_POST[$campo1]) == 4 && strlen($_POST[$campo2]) == 4 && strlen($_POST[$campo3]) == 2 && strlen($_POST[$campo4]) == 10))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]$_POST[$campo4]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de cuenta corriente ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 1234-5678-90-1234567890.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "email")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg($patron_comprobacion_email, strtolower($_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtolower($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de email ($nombre_campo) se ha introducido con un formato erroneo.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "multiple")
			{
				$array_multiple_ids = array();
				$consulta_elementos = "select * from $ele3;";
				$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
				{
					$array_multiple_ids[] = $linea_elementos[$ele5];
				}
				$valor_campo = "";
				foreach ($array_multiple_ids as $multiple_id)
				{
					$nombre_campo = $ele7.$multiple_id;
					if ($_POST[$nombre_campo] == "on")
					{
						if ($valor_campo != "") { $valor_campo .= ";"; }
						$valor_campo .= $multiple_id;
					}
				}
				$consulta2 .= " $campo=\"$valor_campo\",";
			}
			elseif ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						if ($extension == "jpg" || $extension == "png")
						{
							$binario_peso = $_FILES[$campo]['size']; 
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='#id#slider.$extension',";
							}
						}
						else
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) debe estar en formato jpg o png. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				} // fin if files name != ""
			}
			elseif ($ele == "file")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						$existe = 0;
						if (in_array($extension,$array_ext_excluidas)) { $existe = 1; }
						if ($existe == 1)
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) tiene un formato no permitido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
						else
						{
							$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
							$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
							
							$binario_peso = $_FILES[$campo]['size']; 
							$binario_tipo = $_FILES[$campo]['type']; 
							
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='$nombre_fichero', tipo_fichero='$binario_tipo', peso_fichero='$binario_peso', fichero_binario='$binario_contenido', fecha_subida=now(), user_id='$user_id',";
							}
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				}
			}
			elseif ($ele == "ss")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "") || (strlen($_POST[$campo1]) == 2 && strlen($_POST[$campo2]) == 8 && strlen($_POST[$campo3]) == 2))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de seguridad social ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12-34567890-12.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "calendar")
			{
				if ($_POST[$campo] != "")
				{
					list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
					if ($ele2 != 1)
					{
						if (($dia == 0 && $mes == 0 && $ano == 0) || ($dia != "" && strlen($dia) == 2 && $dia > 0 && $dia < 32 && $mes != "" && strlen($mes) == 2 && $mes > 0 && $mes < 13 && $ano != "" && strlen($ano) == 4 && $ano > 1900))
						{
							$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
						}
						else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD/MM/AAAA.'); history.back(); </SCRIPT>"; exit; }
					}
					else
					{
						$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
					}
				}
			}
			elseif ($ele == "time")
			{
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora == "00" && $valor_minutos == "00") || ($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{ $consulta2 .= " $campo=\"$valor_hora:$valor_minutos\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "textarea") { $consulta2 .= " $campo='".str_replace("'","",str_replace($texto_textarea,"",$_POST[$campo]))."',"; }
			else { $consulta2 .= " $campo='".str_replace("'","",$_POST[$campo])."',"; }
		}
		foreach ($campos_col2 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col2[$cuenta_campos]);
			$nombre_campo = $nombres_col2[$cuenta_campos];
			if ($ele == "date")
			{
				$campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
				if ((($_POST[$campo_fecha_dia] == "00" || $_POST[$campo_fecha_dia] == "") && ($_POST[$campo_fecha_mes] == "00" || $_POST[$campo_fecha_mes] == "") && ($_POST[$campo_fecha_ano] == "0000" || $_POST[$campo_fecha_ano] == "")) || ($_POST[$campo_fecha_dia] > 0 && $_POST[$campo_fecha_dia] < 32 && $_POST[$campo_fecha_mes] > 0 && $_POST[$campo_fecha_mes] < 13 && $_POST[$campo_fecha_ano] > 0))
				{ $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD-MM-AAAA.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "telefono")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg('([0-9]{9})', $_POST[$campo])) { $consulta2 .= " $campo=\"$_POST[$campo]\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo telefonico ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 123456789.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "dni")
			{
				if ($_POST[$campo] != "")
				{
					if (strlen($_POST[$campo]) == 9 && (ereg('([0-9]{8}[a-zA-Z]{1})', $_POST[$campo]) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtoupper($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de DNI ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12345678A o A123456B.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "cuenta")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo"; $campo4 = "cuatro$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "" && $_POST[$campo4] == "") || (strlen($_POST[$campo1]) == 4 && strlen($_POST[$campo2]) == 4 && strlen($_POST[$campo3]) == 2 && strlen($_POST[$campo4]) == 10))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]$_POST[$campo4]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de cuenta corriente ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 1234-5678-90-1234567890.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "email")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg($patron_comprobacion_email, strtolower($_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtolower($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de email ($nombre_campo) se ha introducido con un formato erroneo.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "multiple")
			{
				$array_multiple_ids = array();
				$consulta_elementos = "select * from $ele3;";
				$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
				{
					$array_multiple_ids[] = $linea_elementos[$ele5];
				}
				$valor_campo = "";
				foreach ($array_multiple_ids as $multiple_id)
				{
					$nombre_campo = $ele7.$multiple_id;
					if ($_POST[$nombre_campo] == "on")
					{
						if ($valor_campo != "") { $valor_campo .= ";"; }
						$valor_campo .= $multiple_id;
					}
				}
				$consulta2 .= " $campo=\"$valor_campo\",";
			}
			elseif ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						if ($extension == "jpg" || $extension == "png")
						{
							$binario_peso = $_FILES[$campo]['size']; 
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='#id#slider.$extension',";
							}
						}
						else
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) debe estar en formato jpg o png. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				} // fin if files name != ""
			}
			elseif ($ele == "file")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						$existe = 0;
						if (in_array($extension,$array_ext_excluidas)) { $existe = 1; }
						if ($existe == 1)
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) tiene un formato no permitido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
						else
						{
							$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
							$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
							
							$binario_peso = $_FILES[$campo]['size']; 
							$binario_tipo = $_FILES[$campo]['type']; 
							
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='$nombre_fichero', tipo_fichero='$binario_tipo', peso_fichero='$binario_peso', fichero_binario='$binario_contenido', fecha_subida=now(), user_id='$user_id',";
							}
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				}
			}
			elseif ($ele == "ss")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "") || (strlen($_POST[$campo1]) == 2 && strlen($_POST[$campo2]) == 8 && strlen($_POST[$campo3]) == 2))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de seguridad social ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12-34567890-12.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "calendar")
			{
				if ($_POST[$campo] != "")
				{
					list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
					if ($ele2 != 1)
					{
						if (($dia == 0 && $mes == 0 && $ano == 0) || ($dia != "" && strlen($dia) == 2 && $dia > 0 && $dia < 32 && $mes != "" && strlen($mes) == 2 && $mes > 0 && $mes < 13 && $ano != "" && strlen($ano) == 4 && $ano > 1900))
						{
							$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
						}
						else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD/MM/AAAA.'); history.back(); </SCRIPT>"; exit; }
					}
					else
					{
						$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
					}
				}
			}
			elseif ($ele == "time")
			{
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora == "00" && $valor_minutos == "00") || ($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{ $consulta2 .= " $campo=\"$valor_hora:$valor_minutos\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "textarea") { $consulta2 .= " $campo='".str_replace("'","",str_replace($texto_textarea,"",$_POST[$campo]))."',"; }
			else { $consulta2 .= " $campo='".str_replace("'","",$_POST[$campo])."',"; }
		}
		// Eliminamos la coma final
		$consulta2 = substr($consulta2, 0, strlen($consulta2)-1);
		$consulta2 .= " where id='$_POST[id]';";
		//echo "$consulta2<br>";
	
		$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());
		
		// RENOMBRAR CORRECTAMENTE LA IMAGEN DE FONDO
		foreach ($campos_col1 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$cuenta_campos]);
			if ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$consulta_datos = "select $campo, id from $tabla where id='$_POST[id]';";
					$resultado_datos = mysql_query($consulta_datos) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_datos = mysql_fetch_array($resultado_datos, MYSQL_ASSOC))
					{
						$nuevo_nombre = str_replace('#id#',$linea_datos['id'],$linea_datos[$campo]);
						
						$consulta_datos2 = "update $tabla set $campo='$nuevo_nombre' where id='$linea_datos[id]';";
						//echo "$consulta_datos2<br>";
						$resultado_datos2 = mysql_query($consulta_datos2) or die("La consulta fall&oacute;: " . mysql_error());
						
						$uploadfile = $uploaddir_contenidos.$nuevo_nombre;
						move_uploaded_file($_FILES[$campo]['tmp_name'], $uploadfile);
					}
				}
			}
		}
		foreach ($campos_col2 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col2[$cuenta_campos]);
			if ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$consulta_datos = "select $campo, id from $tabla where id='$_POST[id]';";
					$resultado_datos = mysql_query($consulta_datos) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_datos = mysql_fetch_array($resultado_datos, MYSQL_ASSOC))
					{
						$nuevo_nombre = str_replace('#id#',$linea_datos['id'],$linea_datos[$campo]);
						
						$consulta_datos2 = "update $tabla set $campo='$nuevo_nombre' where id='$linea_datos[id]';";
						//echo "$consulta_datos2<br>";
						$resultado_datos2 = mysql_query($consulta_datos2) or die("La consulta fall&oacute;: " . mysql_error());
						
						$uploadfile = $uploaddir_contenidos.$nuevo_nombre;
						move_uploaded_file($_FILES[$campo]['tmp_name'], $uploadfile);
					}
				}
			}
		}
		
		echo "<b>$texto_modificado</b>";
		$accion = "formcrear";
//--------------------------------------------------------
		}
		else
		{
			echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo tipo transporte es obligatorio. No se puede continuar.'); history.back(); </SCRIPT>";
			exit;
		}
	}
	else
	{
		echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora inicio se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>";
		exit;
	}
}
// FIN ACCION MODIFICAR 

// ACCION BORRAR
if ($accion == "accionborrar")
{
	$consulta2 = "delete from $tabla where id='$_POST[id]';";
	//echo "$consulta2";
	$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());
	echo "<b>$texto_borrado</b>";
	$accion = "formcrear";
}
// FIN ACCION BORRAR

// COMIENZA EL SCRIPT

if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$nombre_padre = "";
		$prov_planning_id = "";
		$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
		//echo "$consulta_padre";
		$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
		while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
		{
			$prov_planning_id = $linea_padre['prov_planning_id'];
			$nombre_padre = "<span style='color:#$color_fondo;'>".date("d-m-Y",strtotime($linea_padre['fecha']))."</span>";
			if ($linea_padre['isla_id'] != 0)
			{
				$cons = "select * from maestro_islas_t where id='".$linea_padre['isla_id']."';";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$nombre_padre .= " EN <span style='color:#$color_fondo;'>".$lin['nombre']."</span>";
				}
			}
		}
	}
        
	echo "
<center><b>$titulo $nombre_padre</b><br>";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != $campopadre && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "[<a href='$enlacevolver"."$script_logistica&prov_planning_id=$prov_planning_id&pag=0'>Volver a logistica</a>]";
	//echo "[<a href='$enlacevolver"."$script&accion=formcrear&pag=$pag'>$texto_crear</a>]";
/*
	echo "
	<!--
	<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<form name=form_buscar method=post action='$enlacevolver"."$script'>
	<input type=hidden name=pag value=0>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	echo "
		<tr>
			<td style='text-align:center;'><b>Buscar por nombre</b>: <input type='text' name='b_nombre' value='$b_nombre'>&nbsp;<input type=submit value='Filtrar'></td>
		</tr>
	</form>
	</table>";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "
	<a href='$enlacevolver"."$script&pag=0$parametros'>$texto_listado_general</a>
	-->";
*/
	echo "
</center>";
}

// OBTENEMOS EL LISTADO DE REGISTROS 
if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$condiciones = " $tabla.$campopadre='".$$campopadre."' ";
		$parametros = "&$campopadre=".$$campopadre;
        }
	else
	{
		$condiciones = "";
		$parametros = "";
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
		if ($ele == "texto" && $ele2 != "")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2 like '%".$$nombre_param."%' ";
				$$nombre_param = str_replace(' ','+',$$nombre_param); 
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "select")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2='".$$nombre_param."' ";
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "fecha")
		{
			if ($$ele3 != "" && $$ele4 != "" && $$ele5 != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				($ele6 == "desde" ? $condiciones .= " $tabla.$ele2>='".$$ele3."-".$$ele4."-".$$ele5." 00:00:00' " : $condiciones .= " $tabla.$ele2<='".$$ele3."-".$$ele4."-".$$ele5." 23:59:59' ");
				$parametros .= "&$ele3=".$$ele3."&$ele4=".$$ele4."&$ele5=".$$ele5;
			}
		}
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && $nombre_param != $campopadre && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	$string_para_select = "";
	foreach($campos_necesarios_listado as $indice => $campo)
	{
		$string_para_select .= $tablas_campos_necesarios[$indice].".$campo,";
	}
	echo "<table width=100%>
	<tr bgcolor='#$color_fondo'>";
	$columnas = 0;
	foreach ($campos_listado as $campo) {
//		echo "<td><b><a href='$enlacevolver"."$script&orderby=$campo&pag=$pag$parametros'><font color='#ffffff'>$nombres_listado[$columnas]</font></a></b></td>";
		echo "<td><a class='listadoTabla' href='$enlacevolver"."$script&orderby=$campo&pag=$pag$parametros'>$nombres_listado[$columnas]</a></td>";
		$string_para_select .= $tablas_campos_listado[$columnas].".$campo,";
		$columnas++;
	}
	$columnas += 1;
	// Eliminamos el ultimo caracter
	$string_para_select = substr($string_para_select,0,-1);
	echo "<td><font color='#ffffff'><b>Acciones</b></font></td>";
	echo "</tr>";

	$consulta  = "select $string_para_select from $tabla";
	if ($condiciones != "") { $consulta .= " where $condiciones "; }
	// Vemos si existe un orderby
	if ($_REQUEST[orderby] != "")
	{
		$order = " order by $tabla.$_REQUEST[orderby]";
		$parametros .= "&orderby=".$_REQUEST[orderby];
	}
	else { $order = " order by $tabla.$campo_busqueda";  }
	if ($pag != "") {
		// la primera pagina es la 0
		$inicio = $pag*$registros_por_pagina;
		$limit = " limit $inicio,$registros_por_pagina";
	}
	else { $limit = ""; }
	$consulta .= "$order $limit;";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: $consulta " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		if ($_GET[id] != "" && $linea[id] == $_GET[id]) { $bgcolor = " bgcolor='#$color_fondo_claro'"; }
		else { $bgcolor = ""; }
		
		echo "<tr $bgcolor>";
		foreach ($campos_listado as $cuenta_campos => $campo)
		{
			$nombre = "";
			list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod[$cuenta_campos]);
			if ($ele != 'si')
			{
				$nombre = "$linea[$campo]";
			}
			elseif ($ele2 == "date")
			{
				if ($linea[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
				else { $nombre = date("d-m-Y",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "datetime")
			{
				if ($linea[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
				else { $nombre = date("d-m-Y H:i",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "checkbox")
			{
				($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
			}
			elseif ($ele2 == "time")
			{
				list($temp1, $temp2, $temp3) = explode(':',$linea[$campo]);
				$nombre = $temp1.":".$temp2;
			}
			elseif ($linea[$campo] != "")
			{
				$consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre = $lineaselect[$ele3];
				}
			}
			echo "<td>$nombre</td>";
		}
		echo "<td>
			<a href='$enlacevolver"."$script&accion=formmodificar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
		$posibilidad = 0;
		//$posibilidad = ComprobarMaestroGrupo($linea['id']);
		if ($posibilidad == 0)
		{
			echo "
			<a href='$enlacevolver"."$script&accion=formborrar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_delete.png' alt='Borrar' title='Borrar' border='0' /></a> ";
		}
		echo "
		</td>";
		echo "</tr>";
		echo "<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas'></td></tr>";
	}
	echo "</table>";
	if ($pag != "")
	{
		$pag_visual = $pag+1;
		// Visualizamos las paginas existentes
		$consulta2  = "select count($tabla.id) as num from $tabla";
		if ($condiciones != "") { $consulta2 .= " where $condiciones;"; } else { $consulta2 .= ";"; }
		$resultado2 = mysql_query($consulta2) or die("$consulta2, La consulta fall&oacute;: " . mysql_error());
		//echo "$consulta2<br>";
		while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC)) {
			$exp = "$linea2[num]";
		}
		if ($exp > $registros_por_pagina)
		{
			echo PaginadoListado($pag, $enlacevolver.$script, $parametros, $exp, $registros_por_pagina);
		}
	}
	echo "<hr>";
}
// FIN DE OBTENER EL LISTADO INICIAL

// FORMULARIO PARA BORRAR UN REGISTRO
if ($accion == "formborrar")
{
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "") { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "") { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "") { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { $parametros .= "&orderby=".$_REQUEST[orderby]; }
	echo "<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<tr>
		<td><center><b>BORRADO DE $nombre_objeto</b> [<a href='$enlacevolver"."$script$parametros'>Volver sin cambios</a>]</center></td>
	</tr>
	<tr valign='top'>
		<td>
		<form name=form_buscar method=post action='$enlacevolver"."$script'>
		<input type=hidden name=accion value=accionborrar>
		<input type=hidden name=id value=$_GET[id]>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "") { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "") { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "") { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { echo "<input type=hidden name=orderby value='".$_REQUEST[orderby]."'>"; }
	// Obtenemos los valores actuales del registro que se esta modificando
	$consultamod = "select * from $tabla where id=$_GET[id];";
	$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: " . mysql_error());
	// El resultado lo metemos en un array asociativo
	$arraymod = array();
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC)) {
		foreach ($campos_col1 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
		foreach ($campos_col2 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
	} // del while

	echo "
		<table width='100%' border='0'>
			<tr><td colspan='2'><input type=submit value='Va usted a borrar el registro con los siguientes datos'></td></tr>
			<tr><td width='50%'><table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col1 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col1[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col1[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col1[$cuenta_campos]);
		$nombre_campo = $nombres_col1[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: $dia M: $mes A: $ano</td></tr>";
		}
		if ($ele == "checkbox") { echo "<tr><td><b>$nombre_campo</b></td><td>";if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; } echo "</td></tr>"; }
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele3 where $ele5='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele4]";
				}
			}
			echo "</td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele2 where $ele4='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele3]";
				}
			}
			echo "</td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3 - $valor4</td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "email") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "float") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
				echo "\n<td align='center' width='5%'>$nombre_elemento</td>";
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			if ($arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			$id_codificado = base64_encode($_GET[id]);
			echo "Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3</td></tr>";
		}
		if ($ele == "calendar")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>$dia/$mes/$ano</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>$hora : $minutos</td></tr>";
		}
		if ($ele == "numerico") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		$cuenta_campos++;
	} // del foreach
	echo "</table>";

	// Vamos a por la columna 2
	echo "</td><td>";

	echo "<table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col2 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col2[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col2[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col2[$cuenta_campos]);
		$nombre_campo = $nombres_col2[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: $dia M: $mes A: $ano</td></tr>";
		}
		if ($ele == "checkbox") { echo "<tr><td><b>$nombre_campo</b></td><td>";if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; } echo "</td></tr>"; }
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele3 where $ele5='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele4]";
				}
			}
			echo "</td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele2 where $ele4='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele3]";
				}
			}
			echo "</td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3 - $valor4</td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "email") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "float") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
				echo "\n<td align='center' width='5%'>$nombre_elemento</td>";
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			if ($arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			$id_codificado = base64_encode($_GET[id]);
			echo "Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3</td></tr>";
		}
		if ($ele == "calendar")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>$dia/$mes/$ano</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>$hora : $minutos</td></tr>";
		}
		if ($ele == "numerico") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		$cuenta_campos++;
	} // del foreach
	echo "</table>";
	echo "</td></tr></table>";
	echo "</form></td></tr></table>";
}
// FIN FORMULARIO BORRAR

// FORMULARIO PARA LA CREACION/MODIFICACION DE UN NUEVO REGISTRO
if ($accion == "formcrear" || $accion == "formmodificar" || $accion == "")
{
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "") { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "") { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "") { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { $parametros .= "&orderby=".$_REQUEST[orderby]; }
	echo "<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<tr>
		<td colspan='2'><center><b>";
	if ($accion == "formcrear" || $accion == "") { echo "CREACION"; }
	else { echo "MODIFICACION"; }
	echo " DE $nombre_objeto</b>";
	if ($accion == "formmodificar") { echo " [<a href='$enlacevolver"."$script$parametros'>Volver sin cambios</a>]"; }
	echo "</center></td>
	</tr>
	<form enctype='multipart/form-data' name=form_crear method=post action='$enlacevolver"."$script'>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "") { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "") { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "") { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { echo "<input type=hidden name=orderby value='".$_REQUEST[orderby]."'>"; }
	echo "
	<tr valign='top'>
		<td>";
	echo "<img src=images/p.jpg onload=document.form_crear.".$campos_col1[0].".focus();>";
	$arraymod = array();
	if ($accion == "formcrear" || $accion == "") {
		echo "<input type=hidden name=accion value=accioncrear>";
	}
	if ($accion == "formmodificar") {
		echo "<input type=hidden name=id value='$_GET[id]'>
		<input type=hidden name=accion value=accionmodificar>";
		// Obtenemos los valores actuales del registro que se esta modificando
		$string_para_select = "";
		foreach($campos_col1 as $campo)
		{
			if ($string_para_select != "") { $string_para_select .= ", "; }
			$string_para_select .= "$campo";
		}
		foreach($campos_col2 as $campo)
		{
			if ($string_para_select != "") { $string_para_select .= ", "; }
			$string_para_select .= "$campo";
		}
		$consultamod = "select $string_para_select from $tabla where id=$_GET[id];";
		//echo "$consultamod<br>";
		$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: " . mysql_error());
		// El resultado lo metemos en un array asociativo
		while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC)) {
			foreach ($campos_col1 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
			foreach ($campos_col2 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
		} // del while
	}

	$cuenta_campos = 0;
	foreach($campos_col1 as $campo)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $tipos_col1[$cuenta_campos]);
		if ($ele == "hidden" && $_GET[$ele6] != "") { $arraymod[$campo] = $_GET[$ele6]; }
		$cuenta_campos++;
	}
	$cuenta_campos = 0;
	foreach($campos_col2 as $campo)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $tipos_col2[$cuenta_campos]);
		if ($ele == "hidden" && $_GET[$ele6] != "") { $arraymod[$campo] = $_GET[$ele6]; }
		$cuenta_campos++;
	}

	echo "<table width='100%'>";
	foreach ($campos_col1 as $cuenta_campos => $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col1[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col1[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col1[$cuenta_campos]);
		$nombre_campo = $nombres_col1[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1 && $accion == "formmodificar") { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) {echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($ele2 == 1 && $accion == "formmodificar")
			{
				echo "$arraymod[$campo]<input type=hidden name='$campo' value='$arraymod[$campo]'>";
			}
			else
			{
				echo "<textarea rows='$ele3' cols='$ele4' name='$campo'>$arraymod[$campo]</textarea>";
			}
			echo "</td></tr>";
		}
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: <input type='text' name='dia$campo' size='2' maxlength='2' value='$dia' onkeyup='validar(dia$campo);'"; if ($ele2 == 1) { echo " readonly"; }
			echo "> M: <input type='text' name='mes$campo' size='2' maxlength='2' value='$mes' onkeyup='validar(mes$campo);'"; if ($ele2 == 1) { echo " readonly"; }
			echo "> A: <input type='text' name='ano$campo' size='4' maxlength='4' value='$ano' onkeyup='validar(ano$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "checkbox")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($ele2 == 1)
			{
				if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; }
				echo "<input type=hidden name='$campo' value='$arraymod[$campo]'>";
			}
			else
			{
				echo "<input type=checkbox name='$campo'"; if ($arraymod[$campo] == "on") { echo " checked"; } echo ">";
			}
			echo "</td></tr>";
		}
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><select name='$campo' id='$campo'>";
			echo "<option value='0'></option>";
			if ($accion == "formcrear" || $accion == "" || ($accion == "formmodificar" && $ele2 != 1))
			{
				$condi_select = "";
				if ($ele7 != "") { $condi_select .= " where $ele7"; }
				if ($ele6 != "") { $consultaselect = "select * from $ele3 $condi_select order by $ele6;"; }
				else { $consultaselect = "select * from $ele3 $condi_select;"; }
			}
			else
			{
				if ($ele2 == 1)
				{
					$consultaselect = "select * from $ele3 where $ele3.$ele5='$arraymod[$campo]';";
				}
			}
			//echo "<option>$consultaselect</option>";
			$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
				echo "<option value='$lineaselect[$ele5]'"; if ($lineaselect[$ele5] == "$arraymod[$campo]") { echo " selected"; } echo ">$lineaselect[$ele4]</option>";
			}
			echo "</select></td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>
			<select name='$ele6' onchange=\"saltoPagina('parent',this,0)\"><option value='$enlacevolver"."$script&accion=$accion&pag=$pag&$ele6=0";
			if ($usa_padre != 0)
			{
				$parametros_hidden = "&$campopadre=".$$campopadre;
			}
			else
			{
				$parametros_hidden = "";
			}
			if ($accion == "formmodificar") { echo "&id=$_GET[id]"; }
			$cuenta_hidden = 0;
			foreach ($campos_col1 as $valor_campo) {
				list ($valor, $valor2, $valor3, $valor4, $valor5, $valor6) = explode(';', $tipos_col1[$cuenta_hidden]);
				if ($valor == "hidden" && $valor_campo != $campo) { $parametros_hidden .= "&$valor6=$arraymod[$valor_campo]"; }
				$cuenta_hidden++;
			}
			$cuenta_hidden = 0;
			foreach ($campos_col2 as $valor_campo) {
				list ($valor, $valor2, $valor3, $valor4, $valor5, $valor6) = explode(';', $tipos_col2[$cuenta_hidden]);
				if ($valor == "hidden" && $valor_campo != $campo) { $parametros_hidden .= "&$valor6=$arraymod[$valor_campo]"; }
				$cuenta_hidden++;
			}
			echo "$parametros_hidden'></option>";
			if ($ele7 != "")
			{
				$valor_filtro = ""; $encontrado = 0;
				while ($encontrado == 0)
				{
					foreach ($campos_col1 as $valor_campo) { if ($valor_campo == $ele7) { $encontrado = 1; $valor_filtro = $arraymod[$valor_campo]; } }
				}
				while ($encontrado == 0)
				{
					foreach ($campos_col2 as $valor_campo) { if ($valor_campo == $ele7) { $encontrado = 1; $valor_filtro = $arraymod[$valor_campo]; } }
				}
				$consultaselect = "select $ele2.* from $ele2 where $ele8='$valor_filtro' order by $ele5;";
			}
			else 
			{
				if ($ele5 != "") { $consultaselect = "select * from $ele2 order by $ele5;"; }
				else { $consultaselect = "select * from $ele2;"; }
			}
			//echo "<option>$consultaselect</option>";
			$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
				echo "<option value='$enlacevolver"."$script&accion=$accion&pag=$pag&$ele6=$lineaselect[$ele4]$parametros_hidden";
				if ($accion == "formmodificar") { echo "&id=$_GET[id]'"; } else { echo "'"; }
				if ($lineaselect[$ele4] == $arraymod[$campo]) { echo " selected"; }
				echo ">$lineaselect[$ele3]</option>";
			}
			echo "</select><input type=hidden name=$campo value='$arraymod[$campo]'></td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>
		<input type='text' name='uno$campo' size='4' maxlength='4' value='$valor1' onkeyup='validar(uno$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='dos$campo' size='4' maxlength='4' value='$valor2' onkeyup='validar(dos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='tres$campo' size='2' maxlength='2' value='$valor3' onkeyup='validar(tres$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='cuatro$campo' size='10' maxlength='10' value='$valor4' onkeyup='validar(cuatro$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> <img src='images/money.png'></td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='9' maxlength='9' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='9' maxlength='9' value='$arraymod[$campo]' onkeyup='validar($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "email")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo ">";
			if ($arraymod[$campo] != "") { echo "&nbsp;&nbsp;<a href='mailto:$arraymod[$campo]'><img src='images/email.png' border='0' title='Enviar correo'></a>"; }
			echo "</td></tr>";
		}
		if ($ele == "float")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]' onkeyup='validarFloat($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> (Ej: 1234.56)</td></tr>";
		}
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				if ($ele2 == 1) // no modificable
				{
					($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
					echo "\n<td align='center' width='5%'>$nombre_elemento
					<input type=hidden name='$ele7$linea_elementos[$ele5]' value='".$array_multiple_valor[$linea_elementos[$ele5]]."'></td>";
				}
				else
				{
					echo "\n<td align='center' width='5%'><input type='checkbox' name='$ele7$linea_elementos[$ele5]'";
					if ($array_multiple_valor[$linea_elementos[$ele5]] == "on") { echo " checked"; }
					echo "></td>";
				}
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td><input type=hidden name=MAX_FILE_SIZE value='$tamano_max_archivo' /><input name='$campo' type=file /> <img src='images/camera.png'><br><span syle='font-size:10px;'>Tama&ntilde;o m&aacute;ximo: ".round($tamano_max_archivo/1024,2)." Kb</span><br>";
			if ($accion == "formmodificar" && $arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td><input type=hidden name=MAX_FILE_SIZE value='$tamano_max_archivo' /><input name='$campo' type=file />";
			if ($accion == "formmodificar" && $arraymod[$campo] != "")
			{
				$id_codificado = base64_encode($_GET[id]);
				echo "<br>Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>
		<input type='text' name='uno$campo' size='2' maxlength='2' value='$valor1' onkeyup='validar(uno$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='dos$campo' size='8' maxlength='8' value='$valor2' onkeyup='validar(dos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='tres$campo' size='2' maxlength='2' value='$valor3' onkeyup='validar(tres$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "calendar")
		{
			if ($accion == "formmodificar") { list($fecha, $reloj) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha); }
			else { $ano = "0000"; $mes = "00"; $dia = "00"; }
			echo "<tr><td><b>$nombre_campo</b></td><td>
<input type='text' name='$campo' id='$campo'"; if ($ele2 == 1) { echo " readonly='1'"; } echo " size='10' value='$dia/$mes/$ano'/>";
			if ($ele2 != 1)
			{
				echo "
<img src='images/calendar.gif' name='boton$campo' border='0' id='boton$campo' style='cursor: pointer; border: 0px solid blue;' title='Seleccione fecha entrada' />
<script type='text/javascript'>
	Calendar.setup({
		inputField	:	'$campo',		// id of the input field
		trigger		:	'boton$campo',	// trigger for the calendar (button ID)
		onSelect	:	function() { this.hide() }
	});
</script>";
			}
			echo "</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>
<input type='text' name='hora$campo' size='2' maxlength='2' value='$hora' onkeyup='validar(hora$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> : 
<input type='text' name='minutos$campo' size='2' maxlength='2' value='$minutos' onkeyup='validar(minutos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "numerico")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]' onkeyup='validar($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		echo "\n";
	} // del foreach
	echo "</table>";

	// Vamos a por la columna 2
	echo "</td><td>";

	echo "<table width='100%'>";
	foreach ($campos_col2 as $cuenta_campos => $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col2[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col2[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col2[$cuenta_campos]);
		$nombre_campo = $nombres_col2[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1 && $accion == "formmodificar") { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) {echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($ele2 == 1 && $accion == "formmodificar")
			{
				echo "$arraymod[$campo]<input type=hidden name='$campo' value='$arraymod[$campo]'>";
			}
			else
			{
				echo "<textarea rows='$ele3' cols='$ele4' name='$campo'>$arraymod[$campo]</textarea>";
			}
			echo "</td></tr>";
		}
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: <input type='text' name='dia$campo' size='2' maxlength='2' value='$dia' onkeyup='validar(dia$campo);'"; if ($ele2 == 1) { echo " readonly"; }
			echo "> M: <input type='text' name='mes$campo' size='2' maxlength='2' value='$mes' onkeyup='validar(mes$campo);'"; if ($ele2 == 1) { echo " readonly"; }
			echo "> A: <input type='text' name='ano$campo' size='4' maxlength='4' value='$ano' onkeyup='validar(ano$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "checkbox")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($ele2 == 1)
			{
				if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; }
				echo "<input type=hidden name='$campo' value='$arraymod[$campo]'>";
			}
			else
			{
				echo "<input type=checkbox name='$campo'"; if ($arraymod[$campo] == "on") { echo " checked"; } echo ">";
			}
			echo "</td></tr>";
		}
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><select name='$campo' id='$campo'>";
			echo "<option value='0'></option>";
			if ($accion == "formcrear" || $accion == "" || ($accion == "formmodificar" && $ele2 != 1))
			{
				if ($ele6 != "") { $consultaselect = "select * from $ele3 order by $ele6;"; }
				else { $consultaselect = "select * from $ele3;"; }
			}
			else
			{
				if ($ele2 == 1)
				{
					$consultaselect = "select * from $ele3 where $ele3.$ele5='$arraymod[$campo]';";
				}
			}
			$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
				echo "<option value='$lineaselect[$ele5]'"; if ($lineaselect[$ele5] == "$arraymod[$campo]") { echo " selected"; } echo ">$lineaselect[$ele4]</option>";
			}
			echo "</select></td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>
			<select name='$ele6' onchange=\"saltoPagina('parent',this,0)\"><option value='$enlacevolver"."$script&accion=$accion&pag=$pag&$ele6=0";
			if ($usa_padre != 0)
			{
				$parametros_hidden = "&$campopadre=".$$campopadre;
			}
			else
			{
				$parametros_hidden = "";
			}
			if ($accion == "formmodificar") { echo "&id=$_GET[id]"; }
			$cuenta_hidden = 0;
			foreach ($campos_col1 as $valor_campo) {
				list ($valor, $valor2, $valor3, $valor4, $valor5, $valor6) = explode(';', $tipos_col1[$cuenta_hidden]);
				if ($valor == "hidden" && $valor_campo != $campo) { $parametros_hidden .= "&$valor6=$arraymod[$valor_campo]"; }
				$cuenta_hidden++;
			}
			$cuenta_hidden = 0;
			foreach ($campos_col2 as $valor_campo) {
				list ($valor, $valor2, $valor3, $valor4, $valor5, $valor6) = explode(';', $tipos_col2[$cuenta_hidden]);
				if ($valor == "hidden" && $valor_campo != $campo) { $parametros_hidden .= "&$valor6=$arraymod[$valor_campo]"; }
				$cuenta_hidden++;
			}
			echo "$parametros_hidden'></option>";
			if ($ele7 != "")
			{
				$valor_filtro = ""; $encontrado = 0;
				while ($encontrado == 0)
				{
					foreach ($campos_col1 as $valor_campo) { if ($valor_campo == $ele7) { $encontrado = 1; $valor_filtro = $arraymod[$valor_campo]; } }
				}
				while ($encontrado == 0)
				{
					foreach ($campos_col2 as $valor_campo) { if ($valor_campo == $ele7) { $encontrado = 1; $valor_filtro = $arraymod[$valor_campo]; } }
				}
				$consultaselect = "select $ele2.* from $ele2 where $ele8='$valor_filtro' order by $ele5;";
			}
			else 
			{
				if ($ele5 != "") { $consultaselect = "select * from $ele2 order by $ele5;"; }
				else { $consultaselect = "select * from $ele2;"; }
			}
			//echo "<option>$consultaselect</option>";
			$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
				echo "<option value='$enlacevolver"."$script&accion=$accion&pag=$pag&$ele6=$lineaselect[$ele4]$parametros_hidden";
				if ($accion == "formmodificar") { echo "&id=$_GET[id]'"; } else { echo "'"; }
				if ($lineaselect[$ele4] == $arraymod[$campo]) { echo " selected"; }
				echo ">$lineaselect[$ele3]</option>";
			}
			echo "</select><input type=hidden name=$campo value='$arraymod[$campo]'></td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>
		<input type='text' name='uno$campo' size='4' maxlength='4' value='$valor1' onkeyup='validar(uno$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='dos$campo' size='4' maxlength='4' value='$valor2' onkeyup='validar(dos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='tres$campo' size='2' maxlength='2' value='$valor3' onkeyup='validar(tres$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='cuatro$campo' size='10' maxlength='10' value='$valor4' onkeyup='validar(cuatro$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> <img src='images/money.png'></td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='9' maxlength='9' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='9' maxlength='9' value='$arraymod[$campo]' onkeyup='validar($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "email")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo ">";
			if ($arraymod[$campo] != "") { echo "&nbsp;&nbsp;<a href='mailto:$arraymod[$campo]'><img src='images/email.png' border='0' title='Enviar correo'></a>"; }
			echo "</td></tr>";
		}
		if ($ele == "float")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]' onkeyup='validarFloat($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> (Ej: 1234.56)</td></tr>";
		}
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				if ($ele2 == 1) // no modificable
				{
					($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
					echo "\n<td align='center' width='5%'>$nombre_elemento
					<input type=hidden name='$ele7$linea_elementos[$ele5]' value='".$array_multiple_valor[$linea_elementos[$ele5]]."'></td>";
				}
				else
				{
					echo "\n<td align='center' width='5%'><input type='checkbox' name='$ele7$linea_elementos[$ele5]'";
					if ($array_multiple_valor[$linea_elementos[$ele5]] == "on") { echo " checked"; }
					echo "></td>";
				}
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td><input type=hidden name=MAX_FILE_SIZE value='$tamano_max_archivo' /><input name='$campo' type=file /> <img src='images/camera.png'><br><span syle='font-size:10px;'>Tama&ntilde;o m&aacute;ximo: ".round($tamano_max_archivo/1024,2)." Kb</span><br>";
			if ($accion == "formmodificar" && $arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td><input type=hidden name=MAX_FILE_SIZE value='$tamano_max_archivo' /><input name='$campo' type=file />";
			if ($accion == "formmodificar" && $arraymod[$campo] != "")
			{
				$id_codificado = base64_encode($_GET[id]);
				echo "<br>Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>
		<input type='text' name='uno$campo' size='2' maxlength='2' value='$valor1' onkeyup='validar(uno$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='dos$campo' size='8' maxlength='8' value='$valor2' onkeyup='validar(dos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='tres$campo' size='2' maxlength='2' value='$valor3' onkeyup='validar(tres$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "calendar")
		{
			if ($accion == "formmodificar") { list($fecha, $reloj) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha); }
			else { $ano = "0000"; $mes = "00"; $dia = "00"; }
			echo "<tr><td><b>$nombre_campo</b></td><td>
<input type='text' name='$campo' id='$campo'"; if ($ele2 == 1) { echo " readonly='1'"; } echo " size='10' value='$dia/$mes/$ano'/>";
			if ($ele2 != 1)
			{
				echo "
<img src='images/calendar.gif' name='boton$campo' border='0' id='boton$campo' style='cursor: pointer; border: 0px solid blue;' title='Seleccione fecha entrada' />
<script type='text/javascript'>
	Calendar.setup({
		inputField	:	'$campo',		// id of the input field
		trigger		:	'boton$campo',	// trigger for the calendar (button ID)
		onSelect	:	function() { this.hide() }
	});
</script>";
			}
			echo "</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>
<input type='text' name='hora$campo' size='2' maxlength='2' value='$hora' onkeyup='validar(hora$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> : 
<input type='text' name='minutos$campo' size='2' maxlength='2' value='$minutos' onkeyup='validar(minutos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "numerico")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]' onkeyup='validar($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		echo "\n";
	} // del foreach
	echo "</table>";
	echo "</td></tr>
	<tr><td colspan='2'><input type=submit value='Guardar'></td></tr></form></table>";
}
// FIN FORMULARIO CREACION DE UN NUEVO REGISTRO

} // fin de se tiene permiso para este script
else
{
	echo "No se tiene permiso para ver esta seccion.<br><br>";
}

echo "
		</td>
	</tr>
</table>
";
?>