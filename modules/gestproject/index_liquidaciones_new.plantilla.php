<table border=1 class='table table-bordered table-condensed table-hover' align=center>
<tr><td style='background-color: #e1e4ea !important;'><b>Fecha abono</b></td><td>[fecha_abono]</td></tr>
<tr><td style='background-color: #e1e4ea !important;'><b>Tal&oacute;n</b></td><td>[talon]</td></tr>
<tr><td style='background-color: #e1e4ea !important;'><b>Efectivo</b></td><td>[efectivo]</td></tr>
<tr><td style='background-color: #e1e4ea !important;'><b>Via de cobro</b></td><td>[via_cobro] (<b>Importante: Marcar si se desea modificar la via de cobro en Velazquez)</b>)</td></tr>
<tr><td style='background-color: #e1e4ea !important;'><b>Importe Bruto</b></td><td>[bruto] (<b>Importante: El separador de decimales es el PUNTO. Ejemplos: 2100.67 o 34.50</b>)</td></tr>
<tr><td style='background-color: #e1e4ea !important;'><b>Usuario gestor</b></td><td>[gestor_id] (<b>Si se deja en blanco se asume que su usuario es el gestor del recibo</b>)</td></tr>
</table>