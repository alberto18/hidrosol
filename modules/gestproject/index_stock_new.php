<?php
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=user.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $login</b></center><br>";

// CONFIGURACION
$titulo = "GESTION DEL STOCK";
$titulo_pagina = "STOCK";
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_stock_new";
$script_traspaso = "index_traspaso_almacen_new";
$script_imprimir = "listar_stock";
$script_historico_ajustes = "index_historico_ajustes_new";
$tabla = "stock_t";
$tabla_articulos = "articulos_t";
$registros_por_pagina = 25;
$script_descarga = "";
$tamano_max_archivo = "500000";
if ($color_entorno != "") { $color_fondo = $color_entorno; $color_fondo_claro = $color_entorno_claro; }
else { $color_fondo = "348209"; $color_fondo_claro = "C3EBC2"; }//$color_fondo = "97c00e"; $color_fondo_claro = "dcfb73"; }

//titulo pagina
echo "
<script type=\"text/JavaScript\">document.title = '".$titulo_pagina."';</script>";

echo "
<table width='100%' height='700' border='0' cellpadding='0' cellspacing='0' class='text' align='center'>
	<tr valign='top'>
		<td width=100% align=left>
";

//include ("modules/gestproject/obtener_opciones.php");

if (PermisosSecciones($user_id, $script) == 1)
{
// textos de la pagina
$texto_crear = "Crear stock";
$texto_listado_general = "Listar todo el stock";
$texto_creado = "Stock creado";
$texto_modificado = "Stock modificado";
$texto_borrado = "Stock borrado";
$nombre_objeto = " UN STOCK";

// Campos con los que se trabajara en el insert y modify
$campos_col1 = array('articulo_id');
$campos_col2 = array();

// Nombres que apareceran en las columnas de los formularios
$nombres_col1 = array('Articulo');
$nombres_col2 = array();

// Tipos. Cada campo puede ser:
// text;readonly;size
// password;readonly;size
// textarea;readonly;row;col
// select;readonly;tabla;campo_mostrar;campo_para_value;campo_para_order;condicion
// checkbox;readonly
// date;readonly
// hidden;tabla;campo_mostrar;campo_para_value;campo_para_order;nuevo_nombre;campo_depende;campo_filtro mostrara un select que saltara y ademas tendra otro campo oculto con el valor
// dni;readonly
// telefono;readonly
// cuenta;readonly
// email;readonly;size
// float;readonly;size
// multiple;readonly;tabla;campo_mostrar;campo_para_value;campo_para_order;nombre_checkbox
// file poner el campo "nombre_fichero", actualiza nombre_fichero, tipo_fichero, peso_fichero, fichero_binario, fecha_subida, user_id
// ss;readonly
// calendar;readonly
// time;readonly
// numerico;readonly;size
// fileCarp poner el campo "nombre_fichero1", actualiza solo nombre_fichero y sube a la carpeta definida
$tipos_col1  = array('select;0;articulos_t;nombre;id;nombre');
$tipos_col2  = array();

// Separadores o titulos
$titulos_col1 = array('');
$titulos_col2 = array('');

// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('almacen_id','articulo_id',
'proveedor_id','unidades','estanteria','hueco','piso','palet');
// indica la tabla de donde es el campo
$tablas_campos_listado = array($tabla,$tabla,
$tabla,$tabla,$tabla,$tabla,$tabla,$tabla);
$nombres_listado = array('Almac&eacute;n','(Ref.) Art&iacute;culo',
'Proveedor','Unidades','Estanteria','Hueco','Piso','Palet');
$campos_necesarios_listado = array('id','articulo_id');
$tablas_campos_necesarios = array($tabla,$tabla);

// Si es necesaria una decodificacion en el listado
// '' no es necesaria
// si;tabla;campo_mostrar;campo_buscar
// si;date
// si;datetime
// si;checkbox
// si;time
$campos_listado_decod = array ('si;maestro_almacenes_t;nombre;id',"si;$tabla_articulos;nombre;id",
'si;proveedores_t;nombre_corto;id','','','','','');

// Campo para la busqueda
$campo_busqueda = "almacen_id";

/*
$campos_listado_sub = array ('unidades','estanteria','hueco','piso','palet');
$tablas_campos_listado_sub = array($tabla,$tabla,$tabla,$tabla,$tabla);
$nombres_listado_sub = array('Unidades','Estanteria','Hueco','Piso','Palet');
*/

// Variables del script
$parametros_nombres = array("accion","pag","b_art","b_alm","b_nombre_art","b_prov");
$parametros_formulario = array("pag","b_art","b_alm","b_nombre_art","b_prov");
// Tipos de parametros
//									Sin nada es que no sirven para los filtros o filtro distinto
// texto;campo_filtro							Es un filtro para texto
// texto								Es un filtro para texto, pero hay que desarrollar el filtrado
// select;campo_filtro							Es un filtro con select
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;desde		Es un filtro para fechas superiores a la introducida
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;hasta		Es un filtro para fechas inferiores a la introducida
$parametros_tipos = array("","","select;articulo_id","select;almacen_id","","select;proveedor_id");
foreach($parametros_nombres as $indice_parametros => $nombre_param)
{
	$array_ele = explode(';', $parametros_tipos[$indice_parametros]);
	if ($array_ele[0] == "texto")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; $$nombre_param = str_replace('+',' ',$$nombre_param); }
		else { $$nombre_param = ""; }
	}
	if ($array_ele[0] == "select")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
	if ($array_ele[0] == "fecha")
	{
		if ($_REQUEST[$array_ele[2]] != "") { $$array_ele[2] = $_REQUEST[$array_ele[2]]; }
		else { $$array_ele[2] = ""; }
		if ($_REQUEST[$array_ele[3]] != "") { $$array_ele[3] = $_REQUEST[$array_ele[3]]; }
		else { $$array_ele[3] = ""; }
		if ($_REQUEST[$array_ele[4]] != "") { $$array_ele[4] = $_REQUEST[$array_ele[4]]; }
		else { $$array_ele[4] = ""; }
	}
	if ($array_ele[0] == "")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
}
if ($pag == "") { $pag = "0"; }

// ACCION AJUSTAR 
if ($accion == "accionajustar")
{
	if ($_POST['id'] > 0)
	{
		$nombre_unidadades = "unidades_despues_".$_POST['id'];
		if ($_POST[$nombre_unidadades] != "")
		{
			$consulta_histo = "";
			$nuevas_unidades = 0;
			$consultamod = "select * from $tabla where id='".$_POST['id']."';";
			$resultadomod = mysql_query($consultamod) or die("$consultamod<br>La consulta fall&oacute;: " . mysql_error());
			while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC))
			{
				$nuevas_unidades = $_POST[$nombre_unidad_embalaje]*$_POST[$nombre_num_embalaje];
				$consulta_histo = "insert into historico_ajustes_t set articulo_id='".$lineasmod['articulo_id']."', almacen_id='".$lineasmod['almacen_id']."', 
unidades_antes='".$lineasmod['unidades']."', estanteria_antes='".$lineasmod['estanteria']."', hueco_antes='".$lineasmod['hueco']."', piso_antes='".$lineasmod['piso']."', palet_antes='".$lineasmod['palet']."',
unidades_despues='".$_POST[$nombre_unidadades]."', estanteria_despues='".$_POST["estanteria_despues_".$_POST['id']]."', hueco_despues='".$_POST["hueco_despues_".$_POST['id']]."', piso_despues='".$_POST["piso_despues_".$_POST['id']]."', palet_despues='".$_POST["palet_despues_".$_POST['id']]."', fecha=now(), user_id='".$user_id."';";
			}
			if ($consulta_histo != "")
			{
				if ($_POST[$nombre_unidadades] > 0)
				{
					$consulta_ajus = "update $tabla set unidades='".$_POST[$nombre_unidadades]."', estanteria='".$_POST["estanteria_despues_".$_POST['id']]."', hueco='".$_POST["hueco_despues_".$_POST['id']]."', piso='".$_POST["piso_despues_".$_POST['id']]."', palet='".$_POST["palet_despues_".$_POST['id']]."' where id='".$_POST['id']."';";
				}
				else
				{
					$consulta_ajus = "delete from $tabla where id='".$_POST['id']."';";
				}
				//echo "$consulta_ajus<br>";
				$resultado_ajus = mysql_query($consulta_ajus) or die("$consulta_ajus<br>La consulta fall&oacute;: " . mysql_error());
				//echo "$consulta_histo<br>";
				$resulta_histo = mysql_query($consulta_histo) or die("$consulta_histo<br>La consulta fall&oacute;: " . mysql_error());
				echo "<b>Se ha ajustado el articulo elegido</b><br>";
			}
			else
			{
				echo "<SCRIPT language='JavaScript'> alert('Alerta. Falto algun campo y no se pudo ajustar. No se puede continuar.'); history.back(); </SCRIPT>";
			}
		}
		else
		{
			echo "<SCRIPT language='JavaScript'> alert('Alerta. No se relleno ningun valor para las unidades. No se puede continuar.'); history.back(); </SCRIPT>";
			exit;
		}
	}
	else
	{
		echo "<SCRIPT language='JavaScript'> alert('Alerta. Falto algun campo y no se pudo ajustar. No se puede continuar.'); history.back(); </SCRIPT>";
	}
	$accion = "";
}
// FIN ACCION AJUSTAR 

// COMIENZA EL SCRIPT

if ($accion != "formborrar" && $accion != "formcrear" && $accion != "formmodificar")
{
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			$array_ele = explode(';', $parametros_tipos[$indice_parametros]);
			if ($array_ele[0] == "fecha")
			{
				if ($_REQUEST[$array_ele[2]] != "") { $parametros .= "&".$array_ele[2]."=".$_REQUEST[$array_ele[2]]; }
				if ($_REQUEST[$array_ele[3]] != "") { $parametros .= "&".$array_ele[3]."=".$_REQUEST[$array_ele[3]]; }
				if ($_REQUEST[$array_ele[4]] != "") { $parametros .= "&".$array_ele[4]."=".$_REQUEST[$array_ele[4]]; }
			}
			else
			{
				if ($$nombre_param != "") { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "
<center><b>$titulo</b><br>";
	echo "[<a href='$enlacevolver"."$script_historico_ajustes&pag=0'>Ver el historico de ajustes</a>]<br>";
	//echo "[<a href='$enlacevolver"."$script&accion=formcrear$parametros'>$texto_crear</a>]<br>";
	//echo "[<a href='$enlacevolver"."$script_traspaso'>Traspaso entre almacenes</a>]<br>";
	//echo "[<a href='$enlacevolver"."$script_imprimir'>Imprimir stock</a>]<br>";

	echo "
	<b>Buscar por</b>
	<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<form name=form_buscar1 method=post action='$enlacevolver"."$script'>
	<input type=hidden name=pag value=0>";
	echo "<img src=images/p.jpg onload=document.form_buscar1.b_nombre_art.focus();>";
//		<td><b>Nombre</b>: <input name=buscar_nombre value='$buscar_nombre'></td>
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	echo "
	<tr align='center'>
		<td><b>Art&iacute;culo</b>: <input type='text' name=b_nombre_art value='$b_nombre_art'>";
/*
	echo "<select name=b_art><option value=''>Seleccione una opcion</option>";
	$consultamod = "select articulos_t.id, articulos_t.nombre, articulos_t.codigo from articulos_t join $tabla on $tabla.articulo_id=articulos_t.id group by articulos_t.id order by articulos_t.codigo;";
	$resultadomod = mysql_query($consultamod) or die("$consultamod<br>La consulta fall&oacute;: " . mysql_error());
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC))
	{
		echo "<option value='$lineasmod[id]'"; if ($lineasmod[id] == $b_art) { echo " selected"; }
		echo " title='$lineasmod[nombre]'>$lineasmod[codigo] ";
		if (strlen($lineasmod[nombre]) > 90) { echo substr($lineasmod[nombre],0,90)."..."; }
		else { echo "$lineasmod[nombre]"; }
		echo "</option>";
	}
	echo "</select>";
*/
	echo "</td>
		<td><b>Almac&eacute;n</b>: <select name=b_alm><option value=''>Seleccione una opcion</option>";
	$consultamod = "select id, nombre from maestro_almacenes_t order by nombre;";
	$resultadomod = mysql_query($consultamod) or die("$consultamod<br>La consulta fall&oacute;: " . mysql_error());
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC))
	{
		echo "<option value='$lineasmod[id]'"; if ($lineasmod[id] == $b_alm) { echo " selected"; }
		echo ">$lineasmod[nombre]</option>";
	}
	echo "</select></td>
		<td><b>Proveedor</b>: <select name=b_prov><option value=''>Seleccione una opcion</option>";
	$consultamod = "select proveedores_t.* from proveedores_t join $tabla on $tabla.proveedor_id=proveedores_t.id group by proveedores_t.id order by proveedores_t.nombre_corto;";
	$resultadomod = mysql_query($consultamod) or die("$consultamod<br>La consulta fall&oacute;: " . mysql_error());
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC))
	{
		echo "<option value='$lineasmod[id]'"; if ($lineasmod[id] == $b_prov) { echo " selected"; }
		echo ">$lineasmod[nombre_corto]</option>";
	}
	echo "</select></td>
	</tr>
	<tr align='center'>
		<td colspan='3'><input type=submit name=enviar value='Filtrar'></td>
	</tr>
	</form>
	</table>
	<a href='$enlacevolver"."$script&pag=0'>$texto_listado_general</a>";
	echo "
</center>";
}

// OBTENEMOS EL LISTADO DE REGISTROS 
if ($accion != "formborrar" && $accion != "formcrear" && $accion != "formmodificar")
{
	//$condiciones = "$tabla.reservado='' and $tabla.presupuesto_id='0'";
	$condiciones = "";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
		if ($ele == "texto" && $ele2 != "")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2 like '%".$$nombre_param."%' ";
				$$nombre_param = str_replace(' ','+',$$nombre_param); 
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "select")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2='".$$nombre_param."' ";
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "fecha")
		{
			if ($$ele3 != "" && $$ele4 != "" && $$ele5 != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				($ele6 == "desde" ? $condiciones .= " $tabla.$ele2>='".$$ele3."-".$$ele4."-".$$ele5." 00:00:00' " : $condiciones .= " $tabla.$ele2<='".$$ele3."-".$$ele4."-".$$ele5." 23:59:59' ");
				$parametros .= "&$ele3=".$$ele3."&$ele4=".$$ele4."&$ele5=".$$ele5;
			}
		}
	}
	if ($b_nombre_art != "")
	{
		$trozos = array();
		$trozos = explode(" ",$b_nombre_art);
		foreach ($trozos as $valor)
		{
			if ($condiciones != "") { $condiciones .= " and "; }
			$condiciones .= "(articulos_t.nombre like '%$valor%' or articulos_t.referencia like '%$valor%')";
		}
		$parametros .= "&b_nombre_art=".$b_nombre_art;
		$join = " join articulos_t on articulos_t.id=$tabla.articulo_id ";
		$group = " group by $tabla.id ";
	}
/*
	$string_para_select = "";
	foreach($campos_necesarios_listado as $indice => $campo)
	{
		$string_para_select .= $tablas_campos_necesarios[$indice].".$campo,";
	}
	echo "
<table width='100%'>
	<tr bgcolor='#$color_fondo'>";
	$columnas = 0;
	foreach ($campos_listado as $campo) {
		echo "<td><a class='listadoTabla' href='$enlacevolver"."$script&orderby=$campo&pag=$pag$parametros'>$nombres_listado[$columnas]</a></td>";
		$string_para_select .= $tablas_campos_listado[$columnas].".$campo,";
		$columnas++;
	}
	// Eliminamos el ultimo caracter
	$string_para_select = substr($string_para_select,0,-1);
	$columnas += 1;
	echo "<td><font color='#ffffff'><b>Cortes</b></font></td>";
	echo "</tr>";

	$consulta = "select $tabla.almacen_id, $tabla.articulo_id from $tabla join $tabla_articulos on $tabla_articulos.id=$tabla.articulo_id ";
	if ($condiciones != "") { $consulta .= " where $condiciones "; }
	// Vemos si existe un orderby
	if ($_REQUEST[orderby] != "") { $order = " order by $tabla.$_REQUEST[orderby]"; $parametros .= "&orderby=".$_REQUEST[orderby]; }
	else { $order = " order by $tabla.$campo_busqueda, $tabla_articulos.codigo";  }
	$limit = "";
	$group = " group by $tabla.almacen_id, $tabla.articulo_id";
	$consulta .= "$group $order $limit;";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		//if ($_GET[id] != "" && $linea[id] == $_GET[id]) { $bgcolor = " bgcolor='#$color_fondo_claro'"; }
		//else { $bgcolor = ""; }
		$bgcolor = "";
		
		echo "\n	<tr $bgcolor valign='top'>";
		foreach ($campos_listado as $cuenta_campos => $campo)
		{
			$nombre = "";
			list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod[$cuenta_campos]);
			if ($ele != 'si')
			{
				$nombre = "$linea[$campo]";
			}
			elseif ($ele2 == "date")
			{
				if ($linea[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
				else { $nombre = date("d-m-Y",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "datetime")
			{
				if ($linea[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
				else { $nombre = date("d-m-Y H:i",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "checkbox")
			{
				($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
			}
			elseif ($ele2 == "time")
			{
				list($temp1, $temp2, $temp3) = explode(":",$linea[$campo]);
				$nombre = $temp1.":".$temp2;
			}
			elseif ($linea[$campo] != "")
			{
				$consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre = $lineaselect[$ele3];
					if ($campo == "articulo_id")
					{
						$nombre = "($lineaselect[codigo]) ".$nombre;
					}
				}
			}
			echo "<td>$nombre</td>";
		}
		echo "
		<td>
		<table width='100%' cellpadding='0' cellspacing='0'>
			<tr bgcolor='#$color_fondo'>";
		$columnas_sub = 0;
		foreach ($campos_listado_sub as $campo)
		{
			echo "<td width='25%'><b><font color='#ffffff'>$nombres_listado_sub[$columnas_sub]</font></b></td>";
			$columnas_sub++;
		}
		$columnas_sub += 1;
		echo "
				<td width='25%'><b><font color='#ffffff'>Acciones</font></b></td>
			</tr>";

		$consulta_2  = "select * from $tabla where almacen_id='$linea[almacen_id]' and articulo_id='$linea[articulo_id]' ";
		if ($condiciones != "") { $consulta_2 .= " and $condiciones "; }
		$consulta_2 .= " order by $tabla.estanteria, $tabla.hueco, $tabla.piso, $tabla.palet";
		//echo "$consulta_2<br>";
		$resultado_2 = mysql_query($consulta_2) or die("La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{
			if ($_GET['id'] != "" && $linea_2['id'] == $_GET['id']) { $bgcolor = " bgcolor='#$color_fondo_claro'"; }
			else { $bgcolor = ""; }
			echo "\n			<tr $bgcolor valign='top' align='right'>";
			echo "<form name=form_ajustar_".$linea_2['id']." method=post action='$enlacevolver"."$script'><input type=hidden name=accion value='accionajustar'><input type=hidden name=id value='".$linea_2['id']."'>";
			foreach($parametros_nombres as $indice_parametros => $nombre_param)
			{
				if (in_array($nombre_param,$parametros_formulario))
				{
					list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
					if ($ele == "fecha")
					{
						if ($$ele3 != "") { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
						if ($$ele4 != "") { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
						if ($$ele5 != "") { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
					}
					else
					{
						if ($$nombre_param != "") { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
					}
				}
			}
			foreach ($campos_listado_sub as $campo) {
				$nombre_sub = "";
				if (in_array($campo, array('ancho','largo')))
				{
					$nombre_sub = number_format($linea_2[$campo],2,",","");
				}
				else
				{
					$nombre_sub = $linea_2[$campo];
				}
				echo "
				<td>".$nombre_sub."</td>";
			}
			echo "
				<td>";
			echo "<input name='unidades_reales_".$linea_2['id']."' size='4' onkeyup='validar(unidades_reales_".$linea_2['id'].");'> <input type=submit value='Ajustar'>";
			
			echo "
				</td>
				</form>
			</tr>";
			echo "\n	<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas_sub'></td></tr>";
		} // fin while linea_2
		echo "
		</table>
		</td>
	</tr>";
		echo "\n	<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas'></td></tr>";
	}
	echo "
</table>";
*/
	$string_para_select = "";
	foreach($campos_necesarios_listado as $indice => $campo)
	{
		$string_para_select .= $tablas_campos_necesarios[$indice].".$campo,";
	}
	echo "<center><b>Nota:</b> Si se define el numero de unidades a cero al ajustar el inventario se eliminara ese articulo</center>";
	echo "<table width='100%'>
	<tr bgcolor='#$color_fondo'>";
	$columnas = 0;
	foreach ($campos_listado as $campo) {
		if ($campo == "unidades")
		{
			echo "<td colspan='2'><a class='listadoTabla' href='$enlacevolver"."$script&orderby=$campo&pag=$pag$parametros'>$nombres_listado[$columnas]</a></td>";
		}
		else
		{
			echo "<td><a class='listadoTabla' href='$enlacevolver"."$script&orderby=$campo&pag=$pag$parametros'>$nombres_listado[$columnas]</a></td>";
		}
		$string_para_select .= $tablas_campos_listado[$columnas].".$campo,";
		$columnas++;
	}
	$columnas += 2;
	// Eliminamos el ultimo caracter
	$string_para_select = substr($string_para_select,0,-1);
	echo "<td><font color='#ffffff'><b>Acciones</b></font></td>";
	echo "</tr>";

	$consulta  = "select $string_para_select from $tabla $join ";
	if ($condiciones != "") { $consulta .= " where $condiciones "; }
	// Vemos si existe un orderby
	if ($_REQUEST[orderby] != "")
	{
		$order = " order by $tabla.$_REQUEST[orderby]";
		$parametros .= "&orderby=".$_REQUEST[orderby];
	}
	else { $order = " order by $tabla.$campo_busqueda, $tabla.articulo_id";  }
/*
	if ($pag != "") {
		// la primera pagina es la 0
		$inicio = $pag*$registros_por_pagina;
		$limit = " limit $inicio,$registros_por_pagina";
	}
	else { $limit = ""; }
*/
	$consulta .= "$group $order;";
	//echo "$consulta<br>";
	$almacen_anterior_id = 0;
	$articulo_anterior_id = 0;
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: $consulta " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		if ($_GET[id] != "" && $linea[id] == $_GET[id]) { $bgcolor = " bgcolor='#$color_fondo_claro'"; }
		else { $bgcolor = ""; }
		
		echo "<form name=form_ajustar_".$linea['id']." method=post action='$enlacevolver"."$script'>
	<input type=hidden name=accion value='accionajustar'>
	<input type=hidden name=id value='".$linea['id']."'>";
		foreach($parametros_nombres as $indice_parametros => $nombre_param)
		{
			if (in_array($nombre_param,$parametros_formulario))
			{
				list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
				if ($ele == "fecha")
				{
					if ($$ele3 != "") { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
					if ($$ele4 != "") { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
					if ($$ele5 != "") { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
				}
				else
				{
					if ($$nombre_param != "") { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
				}
			}
		}
		echo "
	<tr $bgcolor>";
		foreach ($campos_listado as $cuenta_campos => $campo)
		{
			$nombre = "";
			list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod[$cuenta_campos]);
			if ($ele != 'si')
			{
				$nombre = "$linea[$campo]";
			}
			elseif ($ele2 == "date")
			{
				if ($linea[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
				else { $nombre = date("d-m-Y",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "datetime")
			{
				if ($linea[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
				else { $nombre = date("d-m-Y H:i",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "checkbox")
			{
				($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
			}
			elseif ($ele2 == "time")
			{
				list($temp1, $temp2, $temp3) = explode(':',$linea[$campo]);
				$nombre = $temp1.":".$temp2;
			}
			elseif ($linea[$campo] != "")
			{
				$consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre = $lineaselect[$ele3];
					if ($campo == "articulo_id")
					{
						$nombre = "($lineaselect[referencia]) ".$nombre;
					}
				}
			}
			if ($campo == "unidades")
			{
				echo "<td align='right'>$nombre</td>
				<td><input type='text' name='unidades_despues_".$linea['id']."' size='5' onkeyup='validar(unidades_despues_".$linea['id'].");' /></td>"."\n";
			}
			elseif ($campo == "estanteria" || $campo == "hueco" || $campo == "piso" || $campo == "palet")
			{
				echo "<td><input type='text' name='".$campo."_despues_".$linea['id']."' size='5' value='$nombre' /></td>"."\n";
			}
			elseif ($campo == "proveedor_id")
			{
				echo "<td><img src=\"images/help.png\" onClick=\"abrirStock(".$linea['articulo_id'].", ".$linea['proveedor_id'].");\" /> $nombre</td>"."\n";
			}
			else
			{
				if (($campo == "almacen_id" && $linea[$campo] == $almacen_anterior_id) || ($campo == "articulo_id" && $linea[$campo] == $articulo_anterior_id))
				{
					echo "<td>&nbsp;</td>"."\n";
				}
				else { echo "<td>$nombre</td>"."\n"; }
			}
//			 

		}
		$almacen_anterior_id = $linea['almacen_id'];
		$articulo_anterior_id = $linea['articulo_id'];
		echo "<td>";
		/*
		echo "			<a href='$enlacevolver"."$script&accion=formmodificar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
		$posibilidad = 0;
		$posibilidad = ComprobarMaestroGrupo($linea['id']);
		if ($posibilidad == 0)
		{
			echo "
			<a href='$enlacevolver"."$script&accion=formborrar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_delete.png' alt='Borrar' title='Borrar' border='0' /></a> ";
		}
		*/
		//echo "<input name='unidades_reales_".$linea['id']."' size='4' onkeyup='validar(unidades_reales_".$linea['id'].");'> ";
		echo "<input type=submit value='Ajustar'>";
		echo "
		</td>";
		echo "</tr>";
		echo "</form>";
		echo "<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas'></td></tr>";
	}
	echo "</table>";
	echo "<hr>";
}
// FIN DE OBTENER EL LISTADO INICIAL

} // fin de se tiene permiso para este script
else
{
	echo "No se tiene permiso para ver esta seccion.<br><br>";
}
echo "
		</td>
	</tr>
</table>
";
?>