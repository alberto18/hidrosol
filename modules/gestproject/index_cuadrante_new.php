<?php 
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
?>

<?php

   //include("obtener_miperfil.php");
   
   //include("obtener_opciones.php");

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "CUADRANTE DE PERMISOS";
// Titulo que aparece en la pestana del navegador
$titulo_pagina = "CUADRANTE DE PERMISOS";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo '
        <section id="content">
          <section class="vbox">

            <header class="header bg-white b-b b-light">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="modules.php?mod=gestproject&file=index"><i class="fa fa-home"></i> Inicio</a></li>
                <li class="active">Cuadrante de permisos</li>
              </ul>
            </header>

            <section class="scrollable wrapper w-f">
              <p class="h4">'.$titulo.'</p>
';
			  
			  
// Texto que aparece en el boton de crear
$titulo_boton_crear = "CREAR NUEVO MENSAJE/NOTIFICACION";
// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.
// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 
$clase_boton_crear = " class='btn btn-success' ";
$clase_boton_buscar = " class='btn btn-mini btn-black' ";
$clase_boton_guardar = " class='btn btn-success' ";
$clase_boton_volver  = " class='btn btn-navi' ";
$clase_boton_confirmar_borrado  = " class='btn btn-danger' ";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
/*
if ($grupo == 3) {
	$permitir_creacion_de_registros = 1;
} else {
	$permitir_creacion_de_registros = 0;
}
*/
$permitir_creacion_de_registros = 0;

// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// Nombre del script
$script = "index_cuadrante_new";
// Nombre de la tabla
$tabla = "mensajes_t"; // OJO, la clave principal se debe llamar id

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
$campos_col1 = array('asunto','descripcion','user_id','departamento_id');

// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]
$ayudas_col1 = array();

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('on','','','');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('','','','','','');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('','','','','','');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php
$tipos_col1  = array('text;400','textarea;400;100','select;usuarios_t;nombre;id','select;maestro_departamentos_t;nombre;id');

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
// $filtro_noc_para_insert = " noc='$noc', ";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = " fecha=now(), remitente_id='$user_id', ";



// Campo para la busqueda
$campo_busqueda = " fecha desc ";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript
$plantilla_insercion = "index_cuadrante_new.plantilla.php";
if ($plantilla_insercion != "") {
  $fichero_absoluto =  "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}


// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 0;
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','fecha','remitente_id','asunto');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','Fecha','Remitente','Asunto');
// Decodificacion si existiese de los campos
$campos_listado_decod = array ('','si;datetime','si;usuarios_t;nombre;id','');
// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
$filtro_noc_para_listado = " and user_id='$user_id'";
// Para el paginado
$registros_por_pagina = "20";

//$filtros_iniciales = " and ($tabla.empresa_servicio=1 or $tabla.empresa_servicio=2 or $tabla.empresa_servicio=6) and ($tabla.via_cobro=1 or $tabla.via_cobro=6) and user_destino_id='$user_id'";

//$consulta_inicial =  "select $string_para_select from $tabla left join recibos_gestiones_t on $tabla.id=recibos_gestiones_t.recibo_id where recibos_gestiones_t.user_destino_id='$user_id' and $tabla.id>0 $filtro_noc_para_listado $filtro_buscar $filtro_padre $filtros_iniciales";
$visualizar_num_registros = 1;

// 2. MODO PERSONALIZADO
// Nombre de la funcion que se ejecutara para cada registro. Si se deja blanco el modo personalizado NO ESTA ACTIVO

$nombre_funcion_acciones_por_registro = "funcion_acciones_registro";
function funcion_acciones_registro($valor_id)
{

	$id_encript = base64_encode(base64_encode($valor_id));

	/*
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_comerciales_kms_new&padre_id='.$id_encript.'&pag=0">KMS</a>';
		
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_areas_new&padre_id='.$id_encript.'&pag=0">AREAS</a>';
                
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_epi_new&padre_id='.$id_encript.'&pag=0">MATERIALES</a>';

		
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_vacaciones_new&padre_id='.$id_encript.'&pag=0">VACACIONES</a>';
	*/
     
    /*	 
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_bajas_new&padre_id='.$id_encript.'&pag=0">BAJAS</a>';
	
    echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_documentacion_new&padre_id='.$id_encript.'&pag=0">DOCUMENTOS</a>';
	*/
    //echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_miperfil_documentacion_new&padre_id='.$id_encript.'&pag=0">DOCUMENTOS</a> ';
	
	// Vemos si esta leido o no
	$salida_array = obtener_multiples_campos(array('fecha_leido'),'mensajes_t','','id='.$valor_id,'','','');
	//print_r($salida_array);
	$fecha_leido = $salida_array[0][fecha_leido];
	
	if ($fecha_leido == "") {
		echo '<span class="label label-danger">NO LEIDO</span> ';
	} else {
		echo '<span class="label label-success">LEIDO</span> ';
	}
	// <span class="badge bg-danger">EN TRAMITE</span>
	
	echo '<a data-toggle="modal" class="smallmario green" href="modules.php?mod=gestproject&file=index_mensajes_lectura_new&accion=formmodificar&id='.$id_encript.'"><i class="fugue-pencil" title="editar"></i> VER MENSAJE</a>';
	

	
	
	
	
	//echo '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
}


/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear
/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";
$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";
*/

// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
// $campo_padre = "";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
// $consulta_nombre_padre = " select nombre as nombre from productos_t where id=#PADREID#";

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 1;
$buscadores = array();
$buscadores[] = "select;departamento_id;maestro_departamentos_t;nombre;id;buscar por departamento";

//$buscadores_extra_sin_efecto_unico[] = "<
 
//$buscadores[] = "input;asunto|descripcion";

//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";


// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

echo "<hr>";

?>

                <section class="scrollable wrapper">
                  <section class="panel panel-default">
                    <header class="panel-heading bg-light">
                      Cuadrante de d&iacute;as de permiso
                    </header>
                    <div class="calendar" id="calendar">

                    </div>
                  </section>
                </section>
				

            </section>
			




			  
	<script src="js/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.js"></script>
	<!-- App -->
	<script src="js/app.js"></script>
	<script src="js/app.plugin.js"></script>
	<script src="js/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="js/charts/easypiechart/jquery.easy-pie-chart.js"></script>
	<script src="js/charts/sparkline/jquery.sparkline.min.js"></script>
	<script src="js/charts/flot/jquery.flot.min.js"></script>
	<script src="js/charts/flot/jquery.flot.tooltip.min.js"></script>
	<script src="js/charts/flot/jquery.flot.resize.js"></script>
	<script src="js/charts/flot/jquery.flot.grow.js"></script>
	<script src="js/charts/flot/demo.js"></script>
	<script src="js/sortable/jquery.sortable.js"></script>
  
    <!-- Magnific Popup core JS file -->
    <script src="js/jquery.magnific-popup.min.js"></script>
	<!-- calendario -->
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
 

	<!-- fullcalendar -->
	<script src="js/jquery.ui.touch-punch.min.js"></script>
	<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="js/fullcalendar/fullcalendar.min.js"></script>
	<!-- <script src="js/fullcalendar/demo.js"></script> -->
	
<script>
	!function ($) {

  $(function(){

    // fullcalendar
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var addDragEvent = function($this){
      // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
      // it doesn't need to have a start or end
      var eventObject = {
        title: $.trim($this.text()), // use the element's text as the event title
        className: $this.attr('class').replace('label','')
      };
      
      // store the Event Object in the DOM element so we can get to it later
      $this.data('eventObject', eventObject);
      
      // make the event draggable using jQuery UI
      $this.draggable({
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
      });
    };
    $('.calendar').each(function() {
      $(this).fullCalendar({
        header: {
          left: 'prev,next',
          center: 'title',
          right: 'today,month,agendaWeek,agendaDay'
        },
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar !!!
        isRTL: false,

        firstDay: 1,

        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['En','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        buttonText: {
                prev: '&nbsp;&#9668;&nbsp;',
                next: '&nbsp;&#9658;&nbsp;',
                prevYear: '&nbsp;&lt;&lt;&nbsp;',
                nextYear: '&nbsp;&gt;&gt;&nbsp;',
                today: 'Hoy',
                month: 'Mensual',
                week: 'Semanal',
                day: 'Diario'
        },

        drop: function(date, allDay) { // this function is called when something is dropped
          
            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');
            
            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);
            
            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            
            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
            
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
              // if so, remove the element from the "Draggable Events" list
              $(this).remove();
            }
            
          }
        ,
        events: [
		
		<?php
			// Vemos los permisos aceptados
			$mensajes_sin_leer = 0;
			$contenido_mensajes = "";
			if ($_REQUEST[buscar_cliente_1] != "") { $filtrodepartamento = " and usuarios_t.departamento_id='$_REQUEST[buscar_cliente_1]'; "; }
			
			// En el caso de que este usuario no pertenezca a este dpto y no sea Admin cerramos el proceso
			$departamento_id = obtener_campo('departamento_id','usuarios_t','','id='.$user_id);
			if ($login != "Admin" && $departamento_id != $_REQUEST[buscar_cliente_1]) { exit; }
			
			$consulta6 = "select * from solicitud_t left join usuarios_t on solicitud_t.user_id=usuarios_t.id where solicitud_t.id>0 $filtrodepartamento";
			$resultado6 = mysql_query($consulta6) or die("La consulta fall&oacute;: $consulta6 " . mysql_error());
			while ($linea6 = mysql_fetch_array($resultado6, MYSQL_ASSOC)) {
					$mensajes_sin_leer++;
					list($mensaje_fecha_ini, $mensaje_hora) = explode(' ', $linea6[fecha_ini]);
					list($mensaje_ano, $mensaje_mes, $mensaje_dia) = explode('-', $mensaje_fecha_ini);
					
					list($mensaje_fecha_fin, $mensaje_hora) = explode(' ', $linea6[fecha_fin]);
					list($mensaje_ano_fin, $mensaje_mes_fin, $mensaje_dia_fin) = explode('-', $mensaje_fecha_fin);
					
					$nombre_user = obtener_campo('nombre','usuarios_t','','id='.$linea6[user_id]);
					
					$mensaje_mes--;
					$mensaje_mes_fin--;
					
					// Buscamos el nombre del permiso
					$nombre_permiso = obtener_campo('nombre','maestro_tipo_permisos_t','','id='.$linea6[tipo_permiso]);
					
					list($fecha_ini, $n) = explode(" ", $linea6[fecha_ini]);
					list($fecha_ini_ano, $fecha_ini_mes, $fecha_ini_dia) = explode ("-", $fecha_ini);
					
					list($fecha_fin, $n) = explode(" ", $linea6[fecha_fin]);
					list($fecha_fin_ano, $fecha_fin_mes, $fecha_fin_dia) = explode ("-", $fecha_fin);
					
					
					echo "
					  {
						title: '$nombre_permiso: $nombre_user $fecha_ini_dia/$fecha_ini_mes/$fecha_ini_ano -> $fecha_fin_dia/$fecha_fin_mes/$fecha_fin_ano',
						start: new Date($mensaje_ano, $mensaje_mes, $mensaje_dia),
						end: new Date($mensaje_ano_fin, $mensaje_mes_fin, $mensaje_dia_fin),
						className:'bg-primary',
						url: '#'
					  },
					";

			}
		
		
		?>

        ]
      });
    });
    $('#myEvents').on('change', function(e, item){
      addDragEvent($(item));
    });

    $('#myEvents li').each(function() {
      addDragEvent($(this));
    });

  });
}(window.jQuery);
	
</script>
	
			

