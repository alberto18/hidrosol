<script type="text/Javascript">
<!--
function expandir (nombrecampo)
{ 
	whichpost = document.getElementById(nombrecampo); 
	if (whichpost.className == "infoshown") { 
		whichpost.className = "infohidden"; 
	} 
	else { 
		whichpost.className = "infoshown"; 
	} 
} 

// selObj.name => etiqueta name
// selObj.selectedIndex => indice del array de opciones del select
// selObj.options[indice].value => valor de la opcion "indice" del select
function cambiarEmbalaje (selObj)
{
	// paso 1: cambiar el valor de las unidades por embalaje
	var nombre = selObj.name;// pilla el nombre
	var posBarraBaja = nombre.search("_"); // busco la barra baja en embalaje_X
	var numElemento = nombre.substring(posBarraBaja+1); // busco el articulo de todos los mostrados
	var opcion_elegida = selObj.options[selObj.selectedIndex].value;
	var nombre_dato = "ud_embalajes_"+numElemento+"_"+opcion_elegida;
	var nombre_destino = "ud_embalajes_"+numElemento;
	var objetoDato = document.getElementById(nombre_dato);
	var objetoDestino = document.getElementById(nombre_destino);
	objetoDestino.value = objetoDato.value;
	var nombre_num_embalajes = "num_embalajes_"+numElemento;
	var objetoNumEmbalajes = document.getElementById(nombre_num_embalajes);
	// paso 2: recalcular las unidades si es posible
	if (objetoNumEmbalajes.value > 0)
	{
		// si se han definido el numero de unidades se debe calcular las unidades totales
		var nuevas_unidades = objetoDestino.value*objetoNumEmbalajes.value;
		var nombre_unidades = "unidades_"+numElemento;
		var objetoUnidades = document.getElementById(nombre_unidades);
		objetoUnidades.value = nuevas_unidades;
		cambiarUnidades(numElemento);
	}
}
function cambiarUdEmbalajes (inputObj)
{
	var nombre = inputObj.name;// pilla el nombre
	validar(inputObj);
	if (inputObj.value > 0)
	{
		var posBarraBaja = nombre.indexOf("_"); // busco la barra baja en num_embalajes_X
		posBarraBaja = nombre.indexOf("_",posBarraBaja+1);
		var numElemento = nombre.substring(posBarraBaja+1); // busco el articulo de todos los mostrados
		var nombre_ud_embalajes = "ud_embalajes_"+numElemento;
		var objetoUdEmbalajes = document.getElementById(nombre_ud_embalajes);
		if (objetoUdEmbalajes.value > 0)
		{
			// si se han definido el numero de unidades se debe calcular las unidades totales
			var nombre_unidades = "unidades_"+numElemento;
			var nuevas_unidades = objetoUdEmbalajes.value*inputObj.value;
			var objetoUnidades = document.getElementById(nombre_unidades);
			objetoUnidades.value = nuevas_unidades;
			cambiarUnidades(numElemento);
		}
	}
}

function cambiarUnidades (numElemento)
{
	var nombre_unidades = "unidades_"+numElemento;
	var objetoUnidades = document.getElementById(nombre_unidades);
	validar(objetoUnidades);
	recalcularPrecioOferta(numElemento);
	calcularGratis(numElemento);
}

function recalcularPrecioOferta (numElemento)
{
	var nombre_unidades = "unidades_"+numElemento;
	var objetoUnidades = document.getElementById(nombre_unidades);
	var nombre_min_unidades_dto = "min_unidades_dto_"+numElemento;
	var objetoMinUnidadesDto = document.getElementById(nombre_min_unidades_dto);
	if (parseInt(objetoMinUnidadesDto.value) > 0)
	{
		var nuevo_valor_precio = 0;
		if (parseInt(objetoUnidades.value) >= parseInt(objetoMinUnidadesDto.value))
		{
			// si las unidades son igual o mayor que las de la oferta se pone el precio de oferta y se quita el dto1
			var nombre_precio_unidad_dto = "precio_unidad_dto_"+numElemento;
			var objetoPrecioUnidadDto = document.getElementById(nombre_precio_unidad_dto);
			nuevo_valor_precio = objetoPrecioUnidadDto.value;
			var nombre_dto1 = "dto1_"+numElemento;
			var objetoDto1 = document.getElementById(nombre_dto1);
			objetoDto1.value = "0.00";
		}
		else
		{
			// si no son suficientes unidades se pone el precio y el dto1 que tendria sin tener en cuenta esta oferta
			var nombre_precio_inicial_sin = "precio_inicial_sin_"+numElemento;
			var objetoPrecioInicialSin = document.getElementById(nombre_precio_inicial_sin);
			nuevo_valor_precio = objetoPrecioInicialSin.value;
			var nombre_dto1_sin = "dto1_sin_"+numElemento;
			var objetoDto1Sin = document.getElementById(nombre_dto1_sin);
			var nombre_dto1 = "dto1_"+numElemento;
			var objetoDto1 = document.getElementById(nombre_dto1);
			objetoDto1.value = objetoDto1Sin.value;
		}
		var nombre_precio_inicial = "precio_inicial_"+numElemento;
		var objetoPrecioInicial = document.getElementById(nombre_precio_inicial);
		objetoPrecioInicial.value = nuevo_valor_precio;
		recalcularPrecio(numElemento);
	}
}

function calcularGratis (numElemento)
{
	var nombre_min_unidades_gratis = "min_unidades_gratis_"+numElemento;
	var objetoMinUnidadesGratis = document.getElementById(nombre_min_unidades_gratis);
	if (parseInt(objetoMinUnidadesGratis.value) > 0)
	{
		var nombre_unidades = "unidades_"+numElemento;
		var objetoUnidades = document.getElementById(nombre_unidades);
		var num_unidades_regaladas = 0;
		if (parseInt(objetoUnidades.value) >= parseInt(objetoMinUnidadesGratis.value))
		{
			// se regalaran unidades gratis
			var nombre_num_unidades_gratis = "num_unidades_gratis_"+numElemento;
			var objetoNumUnidadesGratis = document.getElementById(nombre_num_unidades_gratis);
			var incremento = parseInt(objetoNumUnidadesGratis.value);
			var valor_inicial = parseInt(objetoUnidades.value);
			var valor_minimo = parseInt(objetoMinUnidadesGratis.value);
			while (valor_inicial >= valor_minimo)
			{
				valor_inicial -= valor_minimo;
				num_unidades_regaladas += incremento;
			}
		}
		var nombre_txt_gratis = "txt_gratis_"+numElemento;
		var objetoTxtGratis = document.getElementById(nombre_txt_gratis);
		var nombre_unidades_gratis = "unidades_gratis_"+numElemento;
		var objetoUnidadesGratis = document.getElementById(nombre_unidades_gratis);
		objetoUnidadesGratis.value = num_unidades_regaladas;
		if (num_unidades_regaladas == 0)
		{
			objetoTxtGratis.innerHTML="";
		}
		else
		{
			objetoTxtGratis.innerHTML="+ "+num_unidades_regaladas;
		}
		
	}
}

function cambiarDto (inputObj)
{
	var nombre = inputObj.name;// pilla el nombre
	validarFloat(inputObj);
	var posBarraBaja = nombre.indexOf("_"); // busco la barra baja en dto2_X
	var numElemento = nombre.substring(posBarraBaja+1); // busco el articulo de todos los mostrados
	var nombre_precio_inicial = "precio_inicial_"+numElemento;
	var objetoPrecioInicial = document.getElementById(nombre_precio_inicial);
	if (objetoPrecioInicial.value > 0)
	{
		recalcularPrecio(numElemento);
	}
}

function recalcularPrecio (numElemento)
{
	var numElemento;
	var nombre_precio_inicial = "precio_inicial_"+numElemento;
	var objetoPrecioInicial = document.getElementById(nombre_precio_inicial);
	// el articulo tiene definido un precio inicial
	var nuevo_precio = objetoPrecioInicial.value;
	var nombre_dto2 = "dto2_"+numElemento;
	var objetoDto2 = document.getElementById(nombre_dto2);
	if (objetoDto2.value > 0)
	{
		// calculo del descuento 2
		var dto_2 = (nuevo_precio*objetoDto2.value)/100;
		dto_2 = Math.round(dto_2*100)/100; // redondeo a dos decimales
		nuevo_precio -= dto_2;
		nuevo_precio = Math.round(nuevo_precio*100)/100;
	}
	var nombre_dto3 = "dto3_"+numElemento;
	var objetoDto3 = document.getElementById(nombre_dto3);
	if (objetoDto3.value > 0)
	{
		// calculo del descuento 3
		var dto_3 = (nuevo_precio*objetoDto3.value)/100;
		dto_3 = Math.round(dto_3*100)/100; // redondeo a dos decimales
		nuevo_precio -= dto_3;
		nuevo_precio = Math.round(nuevo_precio*100)/100;
	}
	var nombre_pronto_pago = "pronto_pago_"+numElemento;
	var objetoProntoPago = document.getElementById(nombre_pronto_pago);
	if (objetoProntoPago.value > 0)
	{
		var dto_pronto_pago = (nuevo_precio*objetoProntoPago.value)/100;
		dto_pronto_pago = Math.round(dto_pronto_pago*100)/100; // redondeo a dos decimales
		nuevo_precio -= dto_pronto_pago;
		nuevo_precio = Math.round(nuevo_precio*100)/100;
	}
	var nombre_precio_final = "precio_final_"+numElemento;
	var objetoPrecioFinal = document.getElementById(nombre_precio_final);
	objetoPrecioFinal.value = nuevo_precio;
}
-->
</script>

<?php 
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";

// CONFIGURACION
$titulo = "GESTION DE ARTICULOS DEL PRESUPUESTO ";
$titulo_pagina = "ARTICULOS DE PRESUPUESTO";
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_pre_articulos_new";
$script_presupuestos = "index_presupuestos_new";
$script_ver_correo = "ver_correo_presupuesto";
$tabla = "pre_articulos_t";
$tabla_padre = "presupuestos_t";
$tabla_stock = "stock_t";
$tabla_pre_portes = "pre_portes_t";
$tabla_reservas = "stock_reserva_t";
$tabla_clientes_direcciones = "clientes_direcciones_t";
$tabla_proveedores = "proveedores_t";
$tabla_articulos = "articulos_t";
$tabla_cli_prov = "clientes_prov_t";
$tabla_m_igic = "maestro_igic_t";
$tabla_m_embalajes = "maestro_tipos_embalajes_t";
$tabla_art_embalajes = "articulos_embalajes_t";
$tabla_prov_familias = "prov_familias_t";
$tabla_art_precios = "articulos_precios_t";
$tabla_cli_prov_fam = "clientes_prov_familias_t";
$tabla_cli_prov_fam_art = "clientes_prov_fam_articulos_t";
$tabla_prov_ofertas = "prov_ofertas_t";
$tabla_prov_ofertas_cli = "prov_ofertas_clientes_t";
$tabla_prov_oferta_fam = "prov_ofertas_familias_t";
$tabla_prov_oferta_fam_art = "prov_ofertas_fam_articulos_t";
$tabla_stock_reserva = "stock_reserva_t";
$tabla_m_ag_transportes = "maestro_agencias_transportes_t";
$registros_por_pagina = 10;
$script_descarga = "";
$tamano_max_archivo = "16000000";
if ($color_entorno != "") { $color_fondo = $color_entorno; $color_fondo_claro = $color_entorno_claro; }
else { $color_fondo = "97c00e"; $color_fondo_claro = "dcfb73"; }

$texto_textarea = '<br type="_moz" />';
$nombres_bloque_1 = array('Ref.','Nombre','Embalaje','Ud. embalaje','Ud.','Stock','Pedido','Prov.','PVP','Dto','Dto2','Dto3','Pronto pago','Precio final','IGIC','Reservar','Compromiso');
$texto_boton_bloque_1 = "A&ntilde;adir al presupuesto";

echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo "
<table width='100%' height='700' border='0' cellpadding='0' cellspacing='0' class='text' align='center'>
	<tr valign='top'>
		<td width=100% align=left>
";

if (PermisosSecciones($user_id, $script, array()) == 1)
{

// textos de la pagina
$texto_crear = "Crear articulo";
$texto_listado_general = "Listar todos los articulos";
$texto_creado = "Articulo creado";
$texto_modificado = "Articulo modificado";
$texto_borrado = "Articulo borrado";
$nombre_objeto = " UN ARTICULO";

// Campos con los que se trabajara en el insert y modify
$campos_col1 = array('articulo_id','unidades','proveedor_id','precio_unidad');
$campos_col2 = array();

// Nombres que apareceran en las columnas de los formularios
$nombres_col1 = array('Articulo','Unidades','Proveedor','Precio');
$nombres_col2 = array();

// Tipos. Cada campo puede ser:
// text;readonly;size (60)
// password;readonly;size
// textarea;readonly;row;col (10;60)
// select;readonly;tabla;campo_mostrar;campo_para_value;campo_condicion
// checkbox;readonly
// date;readonly
// hidden;tabla;campo_mostrar;campo_para_value;campo_para_order;nuevo_nombre;campo_depende;campo_filtro mostrara un select que saltara y ademas tendra otro campo oculto con el valor
// dni;readonly
// telefono;readonly
// cuenta;readonly
// email;readonly;size
// float;readonly;size
// multiple;readonly;tabla;campo_mostrar;campo_para_value;campo_para_order;nombre_checkbox
// file poner el campo "nombre_fichero", actualiza nombre_fichero, tipo_fichero, peso_fichero, fichero_binario, fecha_subida, user_id
//   Los campos en la tabla a a�adir serian:
//  | nombre_fichero              | varchar(200)
//  | tipo_fichero                | varchar(20)
//  | peso_fichero                | varchar(20)
//  | fecha_subida                | datetime
//  | user_id                     | int(11)
//  | fichero_binario             | blob
// ss;readonly
// calendar;readonly
// time;readonly
// numerico;readonly;size
// fileCarp poner el campo "nombre_fichero", actualiza solo nombre_fichero y sube a la carpeta definida
$tipos_col1  = array('select;1;articulos_t;nombre;id;nombre','numerico;1;5','select;1;proveedores_t;nombre_corto;id;nombre_corto','float;1;8');
$tipos_col2  = array();

// Separadores o titulos
$titulos_col1 = array('');
$titulos_col2 = array('');

// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('articulo_id','tipo_embalaje_id','unidades',
'proveedor_id','precio_unidad','dto_porc','dto2','dto3','pronto_pago','igic_id',
'importe_articulo','importe_igic','fecha_max_reserva','comprometido');
// indica la tabla de donde es el campo
$tablas_campos_listado = array($tabla,$tabla,$tabla,
$tabla,$tabla,$tabla,$tabla,$tabla,$tabla,$tabla,
$tabla,$tabla,$tabla,$tabla);

$nombres_listado = array('(Ref.) Articulo','Embalaje','Ud.',
'Prov.','Precio','Dto1','Dto2','Dto3','Pronto pago','Igic',
'Importe parcial','IGIC parcial','Reserva','Compromiso');

$campos_necesarios_listado = array('id','unidades_embalaje','fecha_max_reserva','reservado','articulo_id','proveedor_id','unidades_gratis');
$tablas_campos_necesarios = array($tabla,$tabla,$tabla,$tabla,$tabla,$tabla,$tabla);

// Si es necesaria una decodificacion en el listado
// '' no es necesaria
// si;tabla;campo_mostrar;campo_buscar
// si;date
// si;datetime
// si;checkbox
// si;time
$campos_listado_decod = array ('si;articulos_t;nombre;id','si;maestro_tipos_embalajes_t;nombre;id','',
'si;proveedores_t;nombre_corto;id','','','','','','si;maestro_igic_t;valor;id',
'','','si;date','');

// Campo para la busqueda
$campo_busqueda = "id";

// Campo padre
$usa_padre = 1;
$campopadre = "presupuesto_id";

// Variables del script
$parametros_nombres = array("accion","pag",$campopadre,"b_nombre","b_nombre2");
$parametros_formulario = array("pag",$campopadre,"b_nombre");
$parametros_filtro = array("b_nombre"); // parametros que estan en el filtro
// Tipos de parametros
//									Sin nada es que no sirven para los filtros o filtro distinto
// texto;campo_filtro							Es un filtro para texto
// texto								Es un filtro para texto, pero hay que desarrollar el filtrado
// select;campo_filtro							Es un filtro con select
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;desde		Es un filtro para fechas superiores a la introducida
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;hasta		Es un filtro para fechas inferiores a la introducida
$parametros_tipos = array("","","","texto","texto");

foreach($parametros_nombres as $indice_parametros => $nombre_param)
{
	list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
	if ($ele == "texto")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; $$nombre_param = str_replace('+',' ',$$nombre_param); }
		else { $$nombre_param = ""; }
	}
	if ($ele == "select")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
	if ($ele == "fecha")
	{
		if ($_REQUEST[$ele3] != "") { $$ele3 = $_REQUEST[$ele3]; }
		else { $$ele3 = ""; }
		if ($_REQUEST[$ele4] != "") { $$ele4 = $_REQUEST[$ele4]; }
		else { $$ele4 = ""; }
		if ($_REQUEST[$ele5] != "") { $$ele5 = $_REQUEST[$ele5]; }
		else { $$ele5 = ""; }
	}
	if ($ele == "")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
}
if ($pag == "") { $pag = "0"; }

// ACCION MODIFICAR 
if ($accion == "accionmodificar")
{
	$pre_estado_id = 0;
	$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
	//echo "$consulta_padre";
	$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
	while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
	{
		$pre_estado_id = $linea_padre['estado_id'];
	}
	$consulta_hijo = "select * from $tabla where $campopadre='".$$campopadre."' order by $tabla.$campo_busqueda;";
	//echo "$consulta_hijo<br>";
	$resultado_hijo = mysql_query($consulta_hijo) or die("$consulta_hijo, La consulta fall&oacute;: " . mysql_error());
	while ($linea_hijo = mysql_fetch_array($resultado_hijo, MYSQL_ASSOC))
	{
		// se ha enviado una id a modificar
		$existe_articulo = 0;
		$nombre_articulo = "";
		$nombre_proveedor = "";
		$valor_actual_dia_max_reserva = "";
		$valor_actual_mes_max_reserva = "";
		$valor_actual_ano_max_reserva = "";
		$valor_actual_comprometido = "";
		$valor_actual_unidades = "";
		$valor_actual_dto2 = "";
		$valor_actual_dto3 = "";
		$valor_actual_tipo_embalaje_id = "";
		$valor_actual_unidades_embalaje = "";
		$cons = "select * from $tabla where id='".$linea_hijo['id']."';";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$existe_articulo = 1;
			
			$consulta_cat = "select * from $tabla_articulos where $tabla_articulos.id='".$lin['articulo_id']."';";
			$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
			{
				$nombre_articulo = $linea_cat['nombre'];
			}
			
			$consulta_cat = "select * from $tabla_proveedores where $tabla_proveedores.id='".$lin['proveedor_id']."';";
			$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
			{
				$nombre_proveedor = $linea_cat['nombre_corto'];
			}
			
			list($valor_actual_ano_max_reserva, $valor_actual_mes_max_reserva, $valor_actual_dia_max_reserva) = explode('-',$lin['fecha_max_reserva']);
			$valor_actual_comprometido = $lin['comprometido'];
			$valor_actual_unidades = $lin['unidades'];
			$valor_actual_dto2 = $lin['dto2'];
			$valor_actual_dto3 = $lin['dto3'];
			$valor_actual_tipo_embalaje_id = $lin['tipo_embalaje_id'];
			$valor_actual_unidades_embalaje = $lin['unidades_embalaje'];
		}
		if ($pre_estado_id == $presu_estado_creado)
		{
			// modificacion del embalaje
			$campo_tipo_embalaje_id = "tipo_embalaje_id_".$linea_hijo['id'];
			if (isset($_REQUEST[$campo_tipo_embalaje_id]))
			{
				$nuevo_valor_unidades_embalaje = 0;
				$nuevo_valor_tipo_embalaje_id = 0;
				$consulta_cat = "select $tabla_art_embalajes.unidades, $tabla_art_embalajes.tipo_embalaje_id from $tabla_art_embalajes
where $tabla_art_embalajes.id='".$_REQUEST[$campo_tipo_embalaje_id]."';";
				$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
				{
					$nuevo_valor_unidades_embalaje = $linea_cat['unidades'];
					$nuevo_valor_tipo_embalaje_id = $linea_cat['tipo_embalaje_id'];
				}
				if ($nuevo_valor_unidades_embalaje > 0 && $nuevo_valor_tipo_embalaje_id > 0 && ($nuevo_valor_unidades_embalaje != $valor_actual_unidades_embalaje || $nuevo_valor_tipo_embalaje_id != $valor_actual_tipo_embalaje_id))
				{
					// inicio de actualizacion embalaje
					// Se modifica los campos del embalaje (tipo_embalaje_id y unidades_embalaje)
					$cons = "update $tabla set tipo_embalaje_id='".$nuevo_valor_tipo_embalaje_id."', unidades_embalaje='".$nuevo_valor_unidades_embalaje."' where id='".$linea_hijo['id']."';";
					//echo "cons: $cons<br>";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error());
					echo "<b>Embalaje modificado</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
					// fin de actualizacion embalaje
				}
			} // fin if (isset($_REQUEST[$campo_comprometido]))
			// modificacion de la cantidad comprometida
			$campo_comprometido = "comprometido_".$linea_hijo['id'];
			if (isset($_REQUEST[$campo_comprometido]))
			{
				if ($_REQUEST[$campo_comprometido] != $valor_actual_comprometido)
				{
					// inicio de actualizacion comprometido
					// Se modifica las unidades comprometidas
					$cons = "update $tabla set comprometido='".$_REQUEST[$campo_comprometido]."' where id='".$linea_hijo['id']."';";
					//echo "cons: $cons<br>";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error());
					if ($_REQUEST[$campo_comprometido] > 0)
					{
						echo "<b>Compromiso actualizado</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
					}
					else
					{
						echo "<b>Compromiso anulado</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
					}
					// fin de actualizacion comprometido
				}
			} // fin if (isset($_REQUEST[$campo_comprometido]))
			// modificacion de la fecha de reserva
			$campo_fecha = "fecha_max_reserva_".$linea_hijo['id'];
			if (isset($_REQUEST[$campo_fecha]))
			{
				if (strlen($_REQUEST[$campo_fecha]) == 10 && (preg_match('([0-9]{2}/[0-9]{2}/[0-9]{4})', $_REQUEST[$campo_fecha])))
				{
					// inicio cumple el formato
					list($dia, $mes, $ano) = explode('/', $_REQUEST[$campo_fecha]);
					$dia = str_pad($dia, 2, "0", STR_PAD_LEFT);
					$mes = str_pad($mes, 2, "0", STR_PAD_LEFT);
					$ano = str_pad($ano, 4, "0", STR_PAD_LEFT);
					
					if ($dia != $valor_actual_dia_max_reserva || $mes != $valor_actual_mes_max_reserva || $ano != $valor_actual_ano_max_reserva)
					{
						// inicio de actualizacion fecha de reserva
						if ($dia > 0 && $dia < 32 && $mes > 0 && $mes < 13 && $ano > 2000)
						{
							// Se modifica la fecha de reserva
							$cons = "update $tabla set fecha_max_reserva='$ano-$mes-$dia' where id='".$linea_hijo['id']."';";
							//echo "cons: $cons<br>";
							$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error());
							echo "<b>Fecha actualizada</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
						}
						elseif ($dia == "00" && $mes == "00" && $ano == "0000")
						{
							// si existe el campo de fecha y no tiene valor es que se quita la reserva
							// en pre_articulos_t se quita el reservado y la fecha_max_reserva
							$cons = "update $tabla set fecha_max_reserva='0000-00-00', reservado='' where id='".$linea_hijo['id']."';";
							//echo "cons: $cons<br>";
							$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error());
							// se devuelven las unidades al almacen
							$cons1 = "select * from $tabla_reservas where pre_articulo_id='".$linea_hijo['id']."';";
							$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
							while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
							{
								// se busca el articulo en el stock para insertar o actualizar
								$existe_stock = 0;
								$unidades_stock = 0;
								$cons2 = "select * from $tabla_stock where articulo_id='".$lin1['articulo_id']."' and almacen_id='".$lin1['almacen_id']."' and proveedor_id='".$lin1['proveedor_id']."' and estanteria='".$lin1['estanteria']."' and hueco='".$lin1['hueco']."' and piso='".$lin1['piso']."' and palet='".$lin1['palet']."';";
								//echo "cons2: $cons2<br>";
								$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
								while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
								{
									$existe_stock = $lin2['id'];
									$unidades_stock = $lin2['unidades'];
								}
								
								if ($existe_stock == 0)
								{
									// no existe ese articulo => se crea
									$cons3 = "insert into $tabla_stock set articulo_id='".$lin1['articulo_id']."', unidades='".$lin1['unidades']."', almacen_id='".$lin1['almacen_id']."', proveedor_id='".$lin1['proveedor_id']."', estanteria='".$lin1['estanteria']."', hueco='".$lin1['hueco']."', piso='".$lin1['piso']."', palet='".$lin1['palet']."';";
									//echo "cons3: $cons3<br>";
									$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
								}
								else
								{
									$nuevas_unidades = $unidades_stock + $lin1['unidades'];
									$cons3 = "update $tabla_stock set unidades='".$nuevas_unidades."' where id='".$existe_stock."';";
									//echo "cons3: $cons3<br>";
									$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3" . mysql_error());
								}
								
								// se borra el registro de la reserva
								$cons4 = "delete from $tabla_reservas where id='".$lin1['id']."';";
								//echo "cons4: $cons4<br>";
								$res4 = mysql_query($cons4) or die("La consulta fall&oacute;: $cons4 " . mysql_error());
							}
							
							// se actualizan los portes y los importes
							ActualiarPortesImportesPresupuesto ($$campopadre);
							echo "<b>Reserva eliminada</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
						}
						// fin de actualizacion fecha de reserva
					} // fin if ha cambiado la fecha
					// fin cumple el formato
				} // fin if formato correcto
				else
				{
					echo "<b>La fecha para el articulo $nombre_articulo (prov: $nombre_proveedor) se ha definido con un formato incorrecto. Formtato correcto DD/MM/AAAA.</b><br>";
				}
			} // fin if (isset($_REQUEST[$campo_fecha]))
			// modificacion de las unidades
			$campo_unidades = "unidades_".$linea_hijo['id'];
			if (isset($_REQUEST[$campo_unidades]))
			{
				if ($_REQUEST[$campo_unidades] != $valor_actual_unidades)
				{
					// inicio de actualizacion unidades
					if ($_REQUEST[$campo_unidades] > 0)
					{
						// inicio es mayor que cero
						$importe_articulo = 0;
						$importe_igic = 0;
						$valor_precio_neto = 0;
						$valor_dto_porc = 0;
						$valor_precio_unidad = 0;
						$valor_unidades_gratis = 0;
						$cons = "select * from $tabla where id='".$linea_hijo['id']."';";
						$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
						while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
						{
							$valor_igic = "";
							$consulta_cat = "select * from $tabla_m_igic where $tabla_m_igic.id='".$lin['igic_id']."';";
							$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
							while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
							{
								$valor_igic = $linea_cat['valor'];
							}
							if ($lin['min_unidades_dto'] == 0 && $lin['precio_unidad_dto'] == 0 && $lin['min_unidades_gratis'] == 0 && $lin['num_unidades_gratis'] == 0)
							{
								//echo "opcion 1<br>";
								// si no hay ninguna oferta => precio_unidad
								$importe_articulo = $_REQUEST[$campo_unidades]*$lin['precio_unidad'];
								$importe_igic = round(($importe_articulo*$valor_igic)/100, 2);
								
								// valores afectados en el update por los otros casos
								$valor_precio_neto = $lin['precio_neto'];
								$valor_dto_porc = $lin['dto_porc'];
								$valor_precio_unidad = $lin['precio_unidad'];
								$valor_unidades_gratis = $lin['unidades_gratis'];
							}
							elseif ($lin['min_unidades_dto'] > 0 && $lin['precio_unidad_dto'] > 0)
							{
								//echo "opcion 2<br>";
								// si hay una oferta del tipo [comprando X unidades la unidad sale a Y euros]
								if ($_REQUEST[$campo_unidades] >= $lin['min_unidades_dto'])
								{
									//echo "opcion 2.1<br>";
									// si se cumple: se actualiza precio_neto, precio_unidad, dto_porc
									$valor_precio_neto = $lin['precio_unidad_dto'];
									$valor_dto_porc = 0;
									$valor_precio_unidad = $lin['precio_unidad_dto'];
									// aplicar el dto2, dto3 y pronto_pago
									if ($lin['dto2'] > 0)
									{
										$dto_2 = round(($valor_precio_unidad*$lin['dto2'])/100, 2);
										$valor_precio_unidad = $valor_precio_unidad-$dto_2;
									}
									if ($lin['dto3'] > 0)
									{
										$dto_3 = round(($valor_precio_unidad*$lin['dto3'])/100, 2);
										$valor_precio_unidad = $valor_precio_unidad-$dto_3;
									}
									if ($lin['pronto_pago'] > 0)
									{
										$dto_pronto_pago = round(($valor_precio_unidad*$lin['pronto_pago'])/100, 2);
										$valor_precio_unidad = $valor_precio_unidad-$dto_pronto_pago;
									}
								}
								else
								{
									//echo "opcion 2.2<br>";
									// si no se cumple: se actualiza precio_neto, precio_unidad, dto_porc
									$valor_precio_neto = $lin['precio_neto_sin_oferta'];
									$valor_dto_porc = $lin['dto_porc_sin_oferta'];
									$valor_precio_inicial = 0;
									if ($lin['precio_neto_sin_oferta'] > 0)
									{
										// si se ha definido (antes de la oferta) un precio neto este sera el precio de partida
										$valor_precio_inicial = $lin['precio_neto_sin_oferta'];
									}
									elseif ($lin['dto_porc_sin_oferta'] > 0)
									{
										// si se ha definido (antes de la oferta) un dto % al art se calculara el precio del articulo menos ese dto %
										$valor_precio_inicial = $lin['precio_articulo']-round(($lin['precio_articulo']*$lin['dto_porc_sin_oferta'])/100, 2);
									}
									else
									{
										// si no se ha definido nada se coge el precio del articulo directamente
										$valor_precio_inicial = $lin['precio_articulo'];
									}
									// aplicacion del dto2
									$valor_precio_inicial -= round(($valor_precio_inicial*$lin['dto2'])/100, 2);
									// aplicacion del dto3
									$valor_precio_inicial -= round(($valor_precio_inicial*$lin['dto3'])/100, 2);
									// aplicacion del pronto_pago
									$valor_precio_inicial -= round(($valor_precio_inicial*$lin['pronto_pago'])/100, 2);
									$valor_precio_unidad = $valor_precio_inicial;
								}
								$importe_articulo = $_REQUEST[$campo_unidades]*$valor_precio_unidad;
								$importe_igic = round(($importe_articulo*$valor_igic)/100, 2);
								
								// valores afectados en el update por los otros casos
								$valor_unidades_gratis = $lin['unidades_gratis'];
							}
							elseif ($lin['min_unidades_gratis'] > 0 && $lin['num_unidades_gratis'] > 0)
							{
								//echo "opcion 3<br>";
								// si hay una oferta de tipo unidades gratis comprando un minimo
								if ($_REQUEST[$campo_unidades] >= $lin['min_unidades_gratis'])
								{
									//echo "opcion 3.1<br>";
									// si se cumple: se actualiza unidades_gratis
									$valor_inicial = $_REQUEST[$campo_unidades];
									$valor_unidades_gratis = 0;
									while ($valor_inicial >= $lin['min_unidades_gratis'])
									{
										$valor_inicial -= $lin['min_unidades_gratis'];
										$valor_unidades_gratis += $lin['num_unidades_gratis'];
									}
								}
								else
								{
									//echo "opcion 3.2<br>";
									// si no se cumple: se pone a cero las unidades_gratis
									$valor_unidades_gratis = 0;
								}
								
								// valores afectados en el update por los otros casos
								$valor_precio_neto = $lin['precio_neto'];
								$valor_dto_porc = $lin['dto_porc'];
								$valor_precio_unidad = $lin['precio_unidad'];
								$importe_articulo = $lin['importe_articulo'];
								$importe_igic = $lin['importe_igic'];
							}
						} // fin while $lin
						$cons = "update $tabla set unidades='".$_REQUEST[$campo_unidades]."', 
precio_neto='".$valor_precio_neto."', dto_porc='".$valor_dto_porc."', precio_unidad='".$valor_precio_unidad."', 
unidades_gratis='".$valor_unidades_gratis."', 
importe_articulo='".$importe_articulo."', importe_igic='".$importe_igic."'
where id='".$linea_hijo['id']."';";
						
						//echo "cons: $cons<br>";
						$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error());
						
						// se actualizan los portes y los importes
						ActualiarPortesImportesPresupuesto ($$campopadre);
						echo "<b>Unidades actualizadas</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
						// fin es mayor que cero
					}
					else
					{
						echo "<b>No se puede dejar a cero las unidades para el articulo $nombre_articulo (prov: $nombre_proveedor).</b><br>";
					}
					// fin de actualizacion unidades
				} // fin if ($_REQUEST[$campo_unidades] != $valor_actual_unidades)
			} // fin if (isset($_REQUEST[$campo_unidades]))
			// modificacion del dto2
			$campo_dto2 = "dto2_".$linea_hijo['id'];
			if (isset($_REQUEST[$campo_dto2]))
			{
				if ($_REQUEST[$campo_dto2] != $valor_actual_dto2)
				{
					// inicio de actualizacion dto2
					$importe_articulo = 0;
					$importe_igic = 0;
					$valor_precio_unidad = 0;
					$cons = "select * from $tabla where id='".$linea_hijo['id']."';";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
					while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
					{
						$valor_igic = "";
						$consulta_cat = "select * from $tabla_m_igic where $tabla_m_igic.id='".$lin['igic_id']."';";
						$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
						while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
						{
							$valor_igic = $linea_cat['valor'];
						}
						
						$valor_precio_inicial = 0;
						if ($lin['precio_neto'] > 0)
						{
							// si se ha definido un precio neto este sera el precio de partida
							$valor_precio_inicial = $lin['precio_neto'];
						}
						elseif ($lin['dto_porc'] > 0)
						{
							// si se ha definido un dto % al art se calculara el precio del articulo menos ese dto %
							$valor_precio_inicial = $lin['precio_articulo']-round(($lin['precio_articulo']*$lin['dto_porc'])/100, 2);
						}
						else
						{
							// si no se ha definido nada se coge el precio del articulo directamente
							$valor_precio_inicial = $lin['precio_articulo'];
						}
						// aplicacion del dto2
						$valor_precio_inicial -= round(($valor_precio_inicial*$_REQUEST[$campo_dto2])/100, 2);
						// aplicacion del dto3
						$valor_precio_inicial -= round(($valor_precio_inicial*$lin['dto3'])/100, 2);
						// aplicacion del pronto_pago
						$valor_precio_inicial -= round(($valor_precio_inicial*$lin['pronto_pago'])/100, 2);
						$valor_precio_unidad = $valor_precio_inicial;
						
						$importe_articulo = $lin['unidades']*$valor_precio_unidad;
						$importe_igic = round(($importe_articulo*$valor_igic)/100, 2);
					}
					$cons = "update $tabla set precio_unidad='".$valor_precio_unidad."', dto2='".$_REQUEST[$campo_dto2]."', importe_articulo='".$importe_articulo."', importe_igic='".$importe_igic."' where id='".$linea_hijo['id']."';";
					//echo "cons: $cons<br>";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error());
					
					// se actualizan los portes y los importes
					ActualiarPortesImportesPresupuesto ($$campopadre);
					echo "<b>Dto2 actualizado</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
					// fin de actualizacion dto2
				}
			} // fin if (isset($_REQUEST[$campo_dto2]))
			// modificacion del dto3
			$campo_dto3 = "dto3_".$linea_hijo['id'];
			if (isset($_REQUEST[$campo_dto3]))
			{
				if ($_REQUEST[$campo_dto3] != $valor_actual_dto3)
				{
					// inicio de actualizacion dto3
					$importe_articulo = 0;
					$importe_igic = 0;
					$valor_precio_unidad = 0;
					$cons = "select * from $tabla where id='".$linea_hijo['id']."';";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
					while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
					{
						$valor_igic = "";
						$consulta_cat = "select * from $tabla_m_igic where $tabla_m_igic.id='".$lin['igic_id']."';";
						$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
						while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
						{
							$valor_igic = $linea_cat['valor'];
						}
						
						$valor_precio_inicial = 0;
						if ($lin['precio_neto'] > 0)
						{
							// si se ha definido un precio neto este sera el precio de partida
							$valor_precio_inicial = $lin['precio_neto'];
						}
						elseif ($lin['dto_porc'] > 0)
						{
							// si se ha definido un dto % al art se calculara el precio del articulo menos ese dto %
							$valor_precio_inicial = $lin['precio_articulo']-round(($lin['precio_articulo']*$lin['dto_porc'])/100, 2);
						}
						else
						{
							// si no se ha definido nada se coge el precio del articulo directamente
							$valor_precio_inicial = $lin['precio_articulo'];
						}
						// aplicacion del dto2
						$valor_precio_inicial -= round(($valor_precio_inicial*$lin['dto2'])/100, 2);
						// aplicacion del dto3
						$valor_precio_inicial -= round(($valor_precio_inicial*$_REQUEST[$campo_dto3])/100, 2);
						// aplicacion del pronto_pago
						$valor_precio_inicial -= round(($valor_precio_inicial*$lin['pronto_pago'])/100, 2);
						$valor_precio_unidad = $valor_precio_inicial;
						
						$importe_articulo = $lin['unidades']*$valor_precio_unidad;
						$importe_igic = round(($importe_articulo*$valor_igic)/100, 2);
					}
					$cons = "update $tabla set precio_unidad='".$valor_precio_unidad."', dto3='".$_REQUEST[$campo_dto3]."', importe_articulo='".$importe_articulo."', importe_igic='".$importe_igic."' where id='".$linea_hijo['id']."';";
					//echo "cons: $cons<br>";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error());
					
					// se actualizan los portes y los importes
					ActualiarPortesImportesPresupuesto ($$campopadre);
					echo "<b>Dto3 actualizado</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
					// fin de actualizacion dto3
				}
			} // fin if (isset($_REQUEST[$campo_dto3]))
		}
	} // fin while $linea_hijo
	$accion = "";
}
// FIN ACCION MODIFICAR 

// ACCION BORRAR
if ($accion == "accionborrar")
{
	$cons = "select * from $tabla where id='".$_REQUEST['id']."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		if ($lin['reservado'] == "")
		{
			$consulta2 = "delete from $tabla where id='$_POST[id]';";
			//echo "$consulta2<br>";
			$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());
			$num_proveedores_art = 0;
			$cons3 = "select count(distinct proveedor_id) as total from $tabla where $tabla.$campopadre='".$$campopadre."';";
			$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
			while ($lin3 = mysql_fetch_array($res3, MYSQL_ASSOC))
			{
				$num_proveedores_art = $lin3['total'];
			}
			$num_proveedores_porte = 0;
			$cons4 = "select count(distinct proveedor_id) as total from $tabla_pre_portes where $tabla_pre_portes.$campopadre='".$$campopadre."';";
			$res4 = mysql_query($cons4) or die("La consulta fall&oacute;: $cons4 " . mysql_error());
			while ($lin4 = mysql_fetch_array($res4, MYSQL_ASSOC))
			{
				$num_proveedores_porte = $lin4['total'];
			}
			if ($num_proveedores_art < $num_proveedores_porte)
			{
				$cons4 = "select * from $tabla_pre_portes where $tabla_pre_portes.$campopadre='".$$campopadre."';";
				$res4 = mysql_query($cons4) or die("La consulta fall&oacute;: $cons4 " . mysql_error());
				while ($lin4 = mysql_fetch_array($res4, MYSQL_ASSOC))
				{
					$num_art = 0;
					$cons3 = "select count(id) as total from $tabla where $tabla.$campopadre='".$$campopadre."' and $tabla.proveedor_id='".$lin4['proveedor_id']."';";
					$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
					while ($lin3 = mysql_fetch_array($res3, MYSQL_ASSOC))
					{
						$num_art = $lin3['total'];
					}
					if ($num_art == 0)
					{
						$cons5 = "delete from $tabla_pre_portes where id='".$lin4['id']."';";
						//echo "cons5: $cons5<br>";
						$res5 = mysql_query($cons5) or die("La consulta fall&oacute;: $cons5 " . mysql_error());
					}
				}
			}
			// se actualizan los portes y los importes
			ActualiarPortesImportesPresupuesto ($$campopadre);
			echo "<b>$texto_borrado</b>";
		}
		else
		{
			echo "<b>No se puede borrar el articulo pedido porque esta reservado, primero quitar la reserva.</b>";
		}
	}
	$accion = "";
}
// FIN ACCION BORRAR

// ACCION A�ADIR
if ($accion == "actualizabloque2")
{
	if ($$campopadre > 0)
	{
		$pre_cliente_id = 0;
		$pre_estado_id = 0;
		$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
		//echo "$consulta_padre";
		$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
		while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
		{
			$pre_cliente_id = $linea_padre['cliente_id'];
			$pre_estado_id = $linea_padre['estado_id'];
		}
		
		if ($pre_estado_id == $presu_estado_creado)
		{
			$array_cesta = array();
			$array_cesta = $_SESSION['presupuestoAmpliado'.$$campopadre];
			//echo print_r($array_cesta,true);
			for ($i = 1; $i < $_REQUEST['cont']; $i++)
			{
				$campo_referencia = "referencia_".$i;
				$campo_proveedor_id = "proveedor_id_".$i;
				$campo_igic_id = "igic_id_".$i;
				$campo_embalaje = "embalaje_".$i;
				$campo_num_embalajes = "num_embalajes_".$i;
				$campo_unidades = "unidades_".$i;
				$campo_min_unidades_gratis = "min_unidades_gratis_".$i;
				$campo_num_unidades_gratis = "num_unidades_gratis_".$i;
				$campo_unidades_gratis = "unidades_gratis_".$i;
				$campo_min_unidades_dto = "min_unidades_dto_".$i;
				$campo_precio_unidad_dto = "precio_unidad_dto_".$i;
				$campo_precio_neto = "precio_neto_".$i;
				$campo_dto1 = "dto1_".$i;
				$campo_dto1_sin = "dto1_sin_".$i;
				$campo_dto2 = "dto2_".$i;
				$campo_dto3 = "dto3_".$i;
				$campo_pronto_pago = "pronto_pago_".$i;
				$campo_precio_final = "precio_final_".$i;
				$campo_fecha_reserva = "fecha_reserva_".$i;
				$campo_comprometido = "comprometido_".$i;
				
				//$_POST[$campo_num_embalajes] > 0 || 
				if ($_POST[$campo_embalaje] > 0 && ($_POST[$campo_unidades] > 0) && $_POST[$campo_proveedor_id] > 0 && $_POST[$campo_igic_id] > 0)
				{
					$tipo_embalaje_id = 0;
					$unidades_embalaje = 0;
					$consulta_cat = "select $tabla_art_embalajes.tipo_embalaje_id, $tabla_art_embalajes.unidades from $tabla_art_embalajes where $tabla_art_embalajes.id='".$_REQUEST[$campo_embalaje]."';";
					$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
					{
						$tipo_embalaje_id = $linea_cat['tipo_embalaje_id'];
						$unidades_embalaje = $linea_cat['unidades'];
					}
					
					$precio_inicial_articulo = 0;
					$cons_precio = "select * from $tabla_art_precios where articulo_id='".$_REQUEST[$campo_referencia]."' and proveedor_id='".$_REQUEST[$campo_proveedor_id]."';";
					$res_precio = mysql_query($cons_precio) or die("La consulta fall&oacute;: $cons_precio " . mysql_error());
					while ($lin_precio = mysql_fetch_array($res_precio, MYSQL_ASSOC))
					{
						$precio_inicial_articulo = $lin_precio['precio'];
					}
					
					$unidades_pedidas = $_REQUEST[$campo_unidades];
					// precio_neto
					// el precio neto sera el valor de:
					// - precio neto del articulo para la familia de una oferta de un proveedor (prov_ofertas_fam_articulos_t.precio_neto_oferta_art)
					// - en su defecto, si se supera las unidades minimas de la oferta el precio neto definido (prov_ofertas_fam_articulos_t.min_unidades_dto, prov_ofertas_fam_articulos_t.precio_unidad_dto)
					// - en su defecto, precio neto del articulo definido para la familia de un proveedor del cliente (clientes_prov_fam_articulos_t.precio_neto_cli_art_deposito)
					// dto1 (pre_articulos_t.dto_porc)
					// el dto1 sera el valor de:
					// - dto% del articulo para la familia de una oferta de un proveedor (prov_ofertas_fam_articulos_t.dto_porc_oferta_art)
					// - en su defecto, dto% del articulo definido para la familia de un proveedor del cliente (clientes_prov_fam_articulos_t.dto_porc_cli_art_deposito)
					// - en su defecto, dto% de la familia para un proveedor del cliente (clientes_prov_familias_t.dto_porc_cli_fam_deposito)
					// - en su defecto, dto% de la familia para un proveedor (prov_familias_t.dto_porc_prov_fam_deposito)
					$valor_precio_neto = "";
					$valor_dto1 = "";
					if ($_REQUEST[$campo_min_unidades_dto] > 0 && $_REQUEST[$campo_precio_unidad_dto] > 0 && $unidades_pedidas >= $_REQUEST[$campo_min_unidades_dto])
					{
						// se aplica la oferta
						$valor_precio_neto = $_REQUEST[$campo_precio_unidad_dto];
						$valor_dto1 = 0;
					}
					else
					{
						$valor_precio_neto = $_REQUEST[$campo_precio_neto];
						$valor_dto1 = $_REQUEST[$campo_dto1];
					}
					
					// precio_unidad (pre_articulos_t.precio_unidad)
					// el precio_unidad es el precio final del articulo, se calcula aplicando el dto2, despues el dto3 y despues el pronto_pago al precio definido al articulo segun el caso:
					// - caso 1: no se define ni dto% ni precios neto => coincide con articulos_precios_t.precio
					// - caso 2: se define algun dto% (no se contabiliza el dto2 y el dto3 para esto) => sera el resultado de aplicar el dto% a articulos_precios_t.precio
					// - caso 3: se define algun precio neto => se utilizara ese precio_neto
					
					$valor_igic = "";
					$consulta_cat = "select * from $tabla_m_igic where $tabla_m_igic.id='".$_REQUEST[$campo_igic_id]."';";
					$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
					{
						$valor_igic = $linea_cat['valor'];
					}
					$importe_art_parcial = $_REQUEST[$campo_precio_final]*$unidades_pedidas;
					$importe_igic_parcial = round(($importe_art_parcial*$valor_igic)/100, 2);
					
/*
			if ($valor["fecha_reserva"] == "0000-00-00" || ($valor["fecha_reserva"] != "0000-00-00" && $unidades_totales < $unidades_pedidas))
			{
				// si no esta reservado o si se intenta reservar mas del stock existente (que no se deja reservar): se suma para el calculo de los portes
				$array_importes_proveedores[$valor["proveedor_id"]]["importe_arti"] += $importe_art_parcial;
				$array_importes_proveedores[$valor["proveedor_id"]]["importe_igic"] += $importe_igic_parcial;
			}
*/
					
					$insercion_correcta = 1;
					$consulta_pre_art = "insert into $tabla set $campopadre='".$$campopadre."', articulo_id='".$_REQUEST[$campo_referencia]."', tipo_embalaje_id='".$tipo_embalaje_id."', unidades_embalaje='".$unidades_embalaje."', unidades='".$unidades_pedidas."', proveedor_id='".$_REQUEST[$campo_proveedor_id]."', precio_articulo='".$precio_inicial_articulo."', 
dto_porc='".$valor_dto1."', precio_neto='".$valor_precio_neto."', precio_unidad='".$_REQUEST[$campo_precio_final]."', dto2='".$_REQUEST[$campo_dto2]."', dto3='".$_REQUEST[$campo_dto3]."', pronto_pago='".$_REQUEST[$campo_pronto_pago]."', 
importe_articulo='".$importe_art_parcial."', igic_id='".$_REQUEST[$campo_igic_id]."', importe_igic='".$importe_igic_parcial."', 
min_unidades_dto='".$_REQUEST[$campo_min_unidades_dto]."', precio_unidad_dto='".$_REQUEST[$campo_precio_unidad_dto]."', precio_neto_sin_oferta='".$_REQUEST[$campo_precio_neto]."', dto_porc_sin_oferta='".$_REQUEST[$campo_dto1_sin]."', min_unidades_gratis='".$_REQUEST[$campo_min_unidades_gratis]."', num_unidades_gratis='".$_REQUEST[$campo_num_unidades_gratis]."', unidades_gratis='".$_REQUEST[$campo_unidades_gratis]."', ";
					$unidades_reservadas = $unidades_pedidas+$_REQUEST[$campo_unidades_gratis];
					if ($_REQUEST[$campo_fecha_reserva] != "00/00/0000")
					{
						// si hay una fecha de reserva se intenta reservar
						// comprobacion de si hay reserva
						list($dia, $mes, $ano) = explode('/', $_REQUEST[$campo_fecha_reserva]);
						$dia = str_pad($dia, 2, "0", STR_PAD_LEFT);
						$mes = str_pad($mes, 2, "0", STR_PAD_LEFT);
						$ano = str_pad($ano, 4, "0", STR_PAD_LEFT);
						if ($dia > 0 && $dia < 32 && $mes > 0 && $mes < 13 && $ano > 2000)
						{
							$nombre_articulo = "";
							$consulta_art = "select * from $tabla_articulos where id='".$_REQUEST[$campo_referencia]."';";
							$resultado_art = mysql_query($consulta_art) or die("La consulta fall�: $consulta_art" . mysql_error());
							while ($linea_art = mysql_fetch_array($resultado_art, MYSQL_ASSOC))
							{
								$nombre_articulo = $linea_art['nombre'];
							}
							// fecha correcta
							if (strtotime("$ano-$mes-$dia") >= strtotime(date("Y-m-d")))
							{
								// fecha minima hoy
								// calculo del stock disponible (no contar las reservas)
								$cantidad_stock = 0;
								$cantidad_stock = CantidadStock($_REQUEST[$campo_referencia], $_REQUEST[$campo_proveedor_id]);
								$cantidad_reservar = $unidades_reservadas;
								if ($cantidad_stock >= $unidades_reservadas)
								{
									// si se marco una fecha de reserva y hay suficentes unidades (se reservan tambien las unidades gratis si las hubiese)
									$consulta_pre_art .= "reservado='on', fecha_max_reserva='$ano-$mes-$dia', ";
								}
								else
								{
									// se quiere reservar una cantidad que no hay en stock
									$insercion_correcta = 0;
									$nombre_proveedor = "";
									$consulta_prov = "select * from $tabla_proveedores where id='".$_REQUEST[$campo_proveedor_id]."';";
									$resultado_prov = mysql_query($consulta_prov) or die("La consulta fall�: $consulta_prov" . mysql_error());
									while ($linea_prov = mysql_fetch_array($resultado_prov, MYSQL_ASSOC))
									{
										$nombre_proveedor = $linea_prov['nombre_corto'];
									}
									echo "<b>No se pudieron reservar ".$cantidad_reservar." unidades del articulo $nombre_articulo porque solo hay $cantidad_stock unidades en stock para el proveedor $nombre_proveedor.</b><br>";
									// se guardan datos en session
									$indice_array_cesta = $_REQUEST[$campo_referencia]."/".$_REQUEST[$campo_proveedor_id];
									$array_cesta[$indice_array_cesta]["referencia"] = $_REQUEST[$campo_referencia];
									$array_cesta[$indice_array_cesta]["embalaje"] = $_REQUEST[$campo_embalaje];
									$array_cesta[$indice_array_cesta]["unidades"] = $_REQUEST[$campo_unidades];
									$array_cesta[$indice_array_cesta]["min_unidades_gratis"] = $_REQUEST[$campo_min_unidades_gratis];
									$array_cesta[$indice_array_cesta]["num_unidades_gratis"] = $_REQUEST[$campo_num_unidades_gratis];
									$array_cesta[$indice_array_cesta]["unidades_gratis"] = $_REQUEST[$campo_unidades_gratis];
									$array_cesta[$indice_array_cesta]["proveedor_id"] = $_REQUEST[$campo_proveedor_id];
									$array_cesta[$indice_array_cesta]["min_unidades_dto"] = $_REQUEST[$campo_min_unidades_dto];
									$array_cesta[$indice_array_cesta]["precio_unidad_dto"] = $_REQUEST[$campo_precio_unidad_dto];
									$array_cesta[$indice_array_cesta]["precio_neto"] = $valor_precio_neto;
									$array_cesta[$indice_array_cesta]["precio_neto_sin_oferta"] = $_REQUEST[$campo_precio_neto];
									$array_cesta[$indice_array_cesta]["dto1"] = $valor_dto1;
									$array_cesta[$indice_array_cesta]["dto1_sin_oferta"] = $_REQUEST[$campo_dto1_sin];
									$array_cesta[$indice_array_cesta]["dto2"] = $_REQUEST[$campo_dto2];
									$array_cesta[$indice_array_cesta]["dto3"] = $_REQUEST[$campo_dto3];
									$array_cesta[$indice_array_cesta]["pronto_pago"] = $_REQUEST[$campo_pronto_pago];
									$array_cesta[$indice_array_cesta]["precio_unidad"] = $_REQUEST[$campo_precio_final];
									$array_cesta[$indice_array_cesta]["igic_id"] = $_REQUEST[$campo_igic_id];
									$array_cesta[$indice_array_cesta]["fecha_reserva"] = "0000-00-00";
								}
							}
							else
							{
								// se intenta reservar a una fecha inferior a hoy
								$insercion_correcta = 0;
								echo "<b>No se pudo reservar el articulo $nombre_articulo porque la fecha puesta es previa a hoy.</b><br>";
								// se guardan datos en session
								$indice_array_cesta = $_REQUEST[$campo_referencia]."/".$_REQUEST[$campo_proveedor_id];
								$array_cesta[$indice_array_cesta]["referencia"] = $_REQUEST[$campo_referencia];
								$array_cesta[$indice_array_cesta]["embalaje"] = $_REQUEST[$campo_embalaje];
								$array_cesta[$indice_array_cesta]["unidades"] = $_REQUEST[$campo_unidades];
								$array_cesta[$indice_array_cesta]["min_unidades_gratis"] = $_REQUEST[$campo_min_unidades_gratis];
								$array_cesta[$indice_array_cesta]["num_unidades_gratis"] = $_REQUEST[$campo_num_unidades_gratis];
								$array_cesta[$indice_array_cesta]["unidades_gratis"] = $_REQUEST[$campo_unidades_gratis];
								$array_cesta[$indice_array_cesta]["proveedor_id"] = $_REQUEST[$campo_proveedor_id];
								$array_cesta[$indice_array_cesta]["min_unidades_dto"] = $_REQUEST[$campo_min_unidades_dto];
								$array_cesta[$indice_array_cesta]["precio_unidad_dto"] = $_REQUEST[$campo_precio_unidad_dto];
								$array_cesta[$indice_array_cesta]["precio_neto"] = $valor_precio_neto;
								$array_cesta[$indice_array_cesta]["precio_neto_sin_oferta"] = $_REQUEST[$campo_precio_neto];
								$array_cesta[$indice_array_cesta]["dto1"] = $valor_dto1;
								$array_cesta[$indice_array_cesta]["dto1_sin_oferta"] = $_REQUEST[$campo_dto1_sin];
								$array_cesta[$indice_array_cesta]["dto2"] = $_REQUEST[$campo_dto2];
								$array_cesta[$indice_array_cesta]["dto3"] = $_REQUEST[$campo_dto3];
								$array_cesta[$indice_array_cesta]["pronto_pago"] = $_REQUEST[$campo_pronto_pago];
								$array_cesta[$indice_array_cesta]["precio_unidad"] = $_REQUEST[$campo_precio_final];
								$array_cesta[$indice_array_cesta]["igic_id"] = $_REQUEST[$campo_igic_id];
								$array_cesta[$indice_array_cesta]["fecha_reserva"] = "0000-00-00";
							}
						}
					} // fin if if ($_REQUEST[$campo_fecha_reserva]
					else
					{
						// no se reservo el articulo
						$consulta_pre_art .= "reservado='', fecha_max_reserva='0000-00-00', ";
					}
					$consulta_pre_art .= " comprometido='".$_REQUEST[$campo_comprometido]."';";
					if ($insercion_correcta == 1)
					{
						//echo "$consulta_pre_art<br>";
						$resultado_pre_art = mysql_query($consulta_pre_art) or die("$consulta_pre_art, La consulta fall�: " . mysql_error());
						$ultimo_articulo_id = mysql_insert_id();
						
						$cantidad_stock = 0;
						$cantidad_stock = CantidadStock($_REQUEST[$campo_referencia], $_REQUEST[$campo_proveedor_id]);
						// se retiran de stock los articulos reservados
						if ($_REQUEST[$campo_fecha_reserva] != "00/00/0000" && $cantidad_stock >= $unidades_reservadas)
						{
							// hay suficientes unidades en total
							//$unidades_reservadas => las que se reservan
							$cons_stock = "select * from $tabla_stock where articulo_id='".$_REQUEST[$campo_referencia]."' and proveedor_id='".$_REQUEST[$campo_proveedor_id]."' order by id;";
							//echo $cons_stock;
							$res_stock = mysql_query($cons_stock) or die("La consulta fall&oacute;: $cons_stock " . mysql_error());
							while ($lin_stock = mysql_fetch_array($res_stock, MYSQL_ASSOC))
							{
								// si quedan unidades por reservar
								if ($unidades_reservadas > 0)
								{
									if ($lin_stock['unidades'] > $unidades_reservadas)
									{
										// hay mas stock del que se va a reservar : se crea la reserva y se actualizan las unidades que quedan
										$consulta_art_reserva = "insert into $tabla_stock_reserva set unidades='".$unidades_reservadas."', articulo_id='".$lin_stock['articulo_id']."', proveedor_id='".$lin_stock['proveedor_id']."', almacen_id='".$lin_stock['almacen_id']."', estanteria='".$lin_stock['estanteria']."', hueco='".$lin_stock['hueco']."', piso='".$lin_stock['piso']."', palet='".$lin_stock['palet']."', pre_articulo_id='".$ultimo_articulo_id."';";
										$nuevo_stock = $lin_stock['unidades'] - $unidades_reservadas;
										// ya no quedan unidades que reservar
										$unidades_reservadas = 0;
										$cons_stock2 = "update $tabla_stock set unidades='$nuevo_stock' where id='".$lin_stock['id']."'";
										//echo "cons_stock2: $cons_stock2<br>";
										$res_stock2 = mysql_query($cons_stock2) or die("La consulta fall&oacute;: $cons_stock2" . mysql_error());
									}
									else
									{
										// hay menos o la misma cantidad de stock que lo que se va a reservar: se crea la reserva y se borra el registro en el stock
										$consulta_art_reserva = "insert into $tabla_stock_reserva set unidades='".$lin_stock['unidades']."', articulo_id='".$lin_stock['articulo_id']."', proveedor_id='".$lin_stock['proveedor_id']."', almacen_id='".$lin_stock['almacen_id']."', estanteria='".$lin_stock['estanteria']."', hueco='".$lin_stock['hueco']."', piso='".$lin_stock['piso']."', palet='".$lin_stock['palet']."', pre_articulo_id='".$ultimo_articulo_id."';";
										// se restan las unidades ya reservadas y se continua reservando
										$unidades_reservadas -= $lin_stock['unidades'];
										$cons_stock2 = "delete from $tabla_stock where id='".$lin_stock['id']."'";
										//echo "cons_stock2: $cons_stock2<br>";
										$res_stock2 = mysql_query($cons_stock2) or die("La consulta fall&oacute;: $cons_stock2" . mysql_error());
									}
									//echo "$consulta_art_reserva<br>";
									$resultado_art_reserva = mysql_query($consulta_art_reserva) or die("$consulta_art_reserva, La consulta fall�: " . mysql_error());
								}
							}
						} // fin if reserva
						
						// comprobacion de si hay que crear portes (la direccion es fuera de gran canaria y si no existe el proveedor)
						// calculo de portes
						$consulta_cat = "select $tabla_clientes_direcciones.municipio_id from $tabla_clientes_direcciones 
join $tabla_padre on $tabla_padre.cliente_direccion_id=$tabla_clientes_direcciones.id 
where $tabla_padre.id='".$$campopadre."';";
						$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
						while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
						{
							$cons1 = "select * from maestro_municipios_t where id='".$linea_cat['municipio_id']."';";
							$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
							while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
							{
								if ($lin1['isla_id'] != $identificador_gran_canaria)
								{
									$existe_pre_porte_id = 0;
									$cons_pre_porte = "select id from $tabla_pre_portes where $campopadre='".$$campopadre."' and proveedor_id='".$_REQUEST[$campo_proveedor_id]."';";
									$res_pre_porte = mysql_query($cons_pre_porte) or die("La consulta fall&oacute;: $cons_pre_porte " . mysql_error());
									while ($lin_pre_porte = mysql_fetch_array($res_pre_porte, MYSQL_ASSOC))
									{
										$existe_pre_porte_id = 1;
									}
									if ($existe_pre_porte_id == 0)
									{
										// es un proveedor nuevo en el presupuesto y hay que crearle la linea de porte
										// busqueda del minimo que hay que superar para que no pague el cliente (primero el definido en clientes_prov_t, despues en proveedores_t)
										$importe_min_portes_pagados = 0;
										// primero busco las condiciones del proveedor, si existen condiciones particulares para el cliente se machacaran los valores
										$cons_porte = "select * from $tabla_proveedores where id='".$_REQUEST[$campo_proveedor_id]."';";
										$res_porte = mysql_query($cons_porte) or die("La consulta fall&oacute;: $cons_porte " . mysql_error());
										while ($lin_porte = mysql_fetch_array($res_porte, MYSQL_ASSOC))
										{
											//$porc_portes = $lin_porte['prov_porc_portes']; => ya no se va a calcular
											$importe_min_portes_pagados = $lin_porte['prov_importe_min_portes_pagados'];
										}
										$cons_porte = "select * from $tabla_cli_prov where cliente_id='".$pre_cliente_id."' and proveedor_id='".$_REQUEST[$campo_proveedor_id]."';";
										$res_porte = mysql_query($cons_porte) or die("La consulta fall&oacute;: $cons_porte " . mysql_error());
										while ($lin_porte = mysql_fetch_array($res_porte, MYSQL_ASSOC))
										{
											if ($lin_porte['importe_min_portes_pagados'] > 0)//$lin_porte['porc_portes'] > 0 && 
											{
												//$porc_portes = $lin_porte['porc_portes']; => ya no se va a calcular
												$importe_min_portes_pagados = $lin_porte['importe_min_portes_pagados'];
											}
										}
										$ag_tansp_cli = 0;
										$cons_transporte = "select $tabla_cli_prov.agencia_transporte_id, $tabla_m_ag_transportes.nombre from $tabla_cli_prov join $tabla_m_ag_transportes on $tabla_m_ag_transportes.id=$tabla_cli_prov.agencia_transporte_id where $tabla_cli_prov.cliente_id='".$pre_cliente_id."' and $tabla_cli_prov.proveedor_id='".$_REQUEST[$campo_proveedor_id]."';";
										//echo "$cons_transporte<br>";
										$res_transporte = mysql_query($cons_transporte) or die("La consulta fall&oacute;: $cons_transporte " . mysql_error());
										while ($lin_transporte = mysql_fetch_array($res_transporte, MYSQL_ASSOC))
										{
											$ag_tansp_cli = $lin_transporte['agencia_transporte_id'];
										}
										$ag_tansp_prov = 0;
										$cons_transporte = "select prov_agencias_transportes_t.agencia_transporte_id, $tabla_m_ag_transportes.nombre from prov_agencias_transportes_t join $tabla_m_ag_transportes on $tabla_m_ag_transportes.id=prov_agencias_transportes_t.agencia_transporte_id where prov_agencias_transportes_t.proveedor_id='".$_REQUEST[$campo_proveedor_id]."' order by prov_agencias_transportes_t.id limit 1;";
										//echo "$cons_transporte<br>";
										$res_transporte = mysql_query($cons_transporte) or die("La consulta fall&oacute;: $cons_transporte " . mysql_error());
										while ($lin_transporte = mysql_fetch_array($res_transporte, MYSQL_ASSOC))
										{
											$ag_tansp_prov = $lin_transporte['agencia_transporte_id'];
										}
										$cons_insert_porte = "insert into $tabla_pre_portes set $campopadre='".$$campopadre."', proveedor_id='".$_REQUEST[$campo_proveedor_id]."', importe_min_portes_pagados='".$importe_min_portes_pagados."', cli_ag_transporte_id='".$ag_tansp_cli."', prov_ag_transporte_id='".$ag_tansp_prov."';";
										//echo "$cons_insert_porte<br>";
										$resultado_insert_porte = mysql_query($cons_insert_porte) or die("$cons_insert_porte, La consulta fall�: " . mysql_error());
									} // fin if $existe_pre_porte_id
								} // fin if distinto gran canaria
							} // fin while $lin1
						} // fin while $linea_cat
						// se actualizan los portes y los importes
						ActualiarPortesImportesPresupuesto ($$campopadre);
					} // fin if $insercion_correcta
				} // fin if unidades > 0
			} // fin foreach
			
			//vuelvo a guardar cesta en sesion
			$_SESSION['presupuestoAmpliado'.$$campopadre] = $array_cesta;
		} // fin pre_estado_id
	}// fin if campopadre
}
// FIN ACCION A�ADIR

// COMIENZA EL SCRIPT

if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$nombre_padre = "";
		$pre_importe_sin_igic = 0;
		$pre_importe_igic = 0;
		$pre_importe_total = 0;
		$pre_estado_id = 0;
		$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
		//echo "$consulta_padre";
		$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
		while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
		{
			$nombre_padre = "<a target='_blank' href='$enlacevolver"."$script_ver_correo&presupuesto_id=".$$campopadre."' style='color:#$color_fondo;'>".$linea_padre['numero']."</a>";
			$pre_importe_sin_igic = $linea_padre['importe_sin_igic'];
			$pre_importe_igic = $linea_padre['importe_igic'];
			$pre_importe_total = $linea_padre['importe_total'];
			$pre_estado_id = $linea_padre['estado_id'];
		}
	}
        
	echo "
<center><b>$titulo $nombre_padre</b><br>";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != $campopadre && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "[<a href='$enlacevolver"."$script_presupuestos&pag=0'>Volver a presupuestos</a>]";
	//echo "[<a href='$enlacevolver"."$script&accion=formcrear&pag=$pag'>$texto_crear</a>]";
	echo "
<table align='center'>
	<tr bgcolor='#$color_fondo'>
		<td><span class='listadoTabla'>Importe base</span></td>
		<td><span class='listadoTabla'>Importe IGIC</span></td>
		<td><span class='listadoTabla'>Importe total</span></td>
	</tr>
	<tr align='right'>
		<td>".number_format($pre_importe_sin_igic,2,",",".")." &euro;</td>
		<td>".number_format($pre_importe_igic,2,",",".")." &euro;</td>
		<td>".number_format($pre_importe_total,2,",",".")." &euro;</td>
	</tr>
</table>";
//jgs
	$cons = "select count(id) as total from $tabla_pre_portes where $campopadre='".$$campopadre."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		if ($lin['total'] > 0)
		{
			echo "
<table align='center'>
	<tr bgcolor='#$color_fondo'>
		<td><span class='listadoTabla'>Prov.</span></td>
		<td><span class='listadoTabla'>Ag. Transp.</span></td>
		<td><span class='listadoTabla'>Importe minimo</span></td>
		<td><span class='listadoTabla'>Importe articulos</span></td>
		<td><span class='listadoTabla'>Estado</span></td>
	</tr>";
			$importe_total_portes = 0;
			$cons2 = "select * from $tabla_pre_portes where $campopadre='".$$campopadre."' order by id;";
			$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
			while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
			{
				$importe_total_portes += $lin2['importe_portes'];
				$nombre_proveedor = "";
				$consultaselect = "select * from $tabla_proveedores where id='$lin2[proveedor_id]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre_proveedor = $lineaselect['nombre_corto'];
				}
				$nombre_ag_transp = NombreAgTransporte(AgTransporteProvPresupuesto($$campopadre, $lin2['proveedor_id']));
				$importe_total_sin = 0;
				$cons3 = "select sum(importe_articulo) as total from $tabla where $campopadre='".$$campopadre."' and reservado='' and proveedor_id='".$lin2['proveedor_id']."';";// and fecha_max_reserva='0000-00-00'
				//echo "$cons3<br>";
				$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
				while ($lin3 = mysql_fetch_array($res3, MYSQL_ASSOC))
				{
					$importe_total_sin = $lin3['total'];
				}
				echo "
	<tr align='right'>
		<td>$nombre_proveedor</td>
		<td>$nombre_ag_transp</td>
		<td>".number_format($lin2['importe_min_portes_pagados'],2,",",".")." &euro;</td>
		<td>".number_format($importe_total_sin,2,",",".")." &euro;</td>
		<td>";
				if ($lin2['usar_cliente'] == "on") { echo "Debidos"; }
				else { echo "Pagados"; }
				echo "</td>
	</tr>";
			}
/*
			echo "
	<tr align='right'>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><b>Total</b></td>
		<td>".number_format($importe_total_portes,2,",",".")." &euro;</td>
		<td>&nbsp;</td>
	</tr>";
*/
			echo "
</table>";
		}
	}
	echo "
	<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<form name=form_buscar method=post action='$enlacevolver"."$script'>
	<input type=hidden name=pag value=0>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
/*
	echo "
		<tr style='text-align:center;'>
			<td><b>Buscar por articulo</b>: <select name='b_arti'><option value=''>Todos</option>";
	$consulta_cat = "select articulos_t.* from articulos_t join $tabla on $tabla.articulo_id=articulos_t.id where $tabla.$campopadre='".$$campopadre."' order by articulos_t.nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $b_arti) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select> <input type=submit value='Filtrar'></td>
		</tr>";
*/
	echo "
		<tr>
			<td style='text-align:center;'><b>Buscar por articulo</b>: <input type='text' name='b_nombre' value='$b_nombre'>&nbsp;<input type=submit value='Filtrar'></td>
		</tr>
	</form>
	</table>";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "
	<a href='$enlacevolver"."$script&pag=0$parametros'>$texto_listado_general</a>
	<!--
	-->
</center>";
}

// OBTENEMOS EL LISTADO DE REGISTROS 
if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$condiciones = " $tabla.$campopadre='".$$campopadre."' ";
		$parametros = "&$campopadre=".$$campopadre;
        }
	else
	{
		$condiciones = "";
		$parametros = "";
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
		if ($ele == "texto" && $ele2 != "")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2 like '%".$$nombre_param."%' ";
				$$nombre_param = str_replace(' ','+',$$nombre_param); 
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "select")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2='".$$nombre_param."' ";
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "fecha")
		{
			if ($$ele3 != "" && $$ele4 != "" && $$ele5 != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				($ele6 == "desde" ? $condiciones .= " $tabla.$ele2>='".$$ele3."-".$$ele4."-".$$ele5." 00:00:00' " : $condiciones .= " $tabla.$ele2<='".$$ele3."-".$$ele4."-".$$ele5." 23:59:59' ");
				$parametros .= "&$ele3=".$$ele3."&$ele4=".$$ele4."&$ele5=".$$ele5;
			}
		}
	}
	if ($b_nombre != "")
	{
		$trozos = array();
		$trozos = explode(" ",$b_nombre);
		foreach ($trozos as $valor)
		{
			if ($condiciones != "") { $condiciones .= " and "; }
			$condiciones .= "(articulos_t.nombre like '%$valor%' or articulos_t.referencia like '%$valor%')";
		}
		$parametros .= "&b_nombre=".$b_nombre;
		$join .= " join articulos_t on articulos_t.id=$tabla.articulo_id ";
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && $nombre_param != $campopadre && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	$string_para_select = "";
	foreach($campos_necesarios_listado as $indice => $campo)
	{
		$string_para_select .= $tablas_campos_necesarios[$indice].".$campo,";
	}
	echo "<table width=100%>
	<tr bgcolor='#$color_fondo'>";
	$columnas = 0;
	foreach ($campos_listado as $campo) {
		echo "<td><a class='listadoTabla' href='$enlacevolver"."$script&orderby=$campo&pag=$pag$parametros'>$nombres_listado[$columnas]</a></td>";
		if ($campo == "unidades")
		{
			echo "<td><span class='listadoTabla'>Stock</span></td>";
			echo "<td><span class='listadoTabla'>Pedido</span></td>";
		}
		$string_para_select .= " ".$tablas_campos_listado[$columnas].".$campo,";
		$columnas++;
	}
	$columnas += 2;
	$columnas += 1;// acciones
	// Eliminamos el ultimo caracter
	$string_para_select = substr($string_para_select,0,-1);
	echo "<td><font color='#ffffff'><b>Acciones</b></font></td>";
	echo "</tr>";
	if ($pre_estado_id == $presu_estado_creado)
	{
		echo "
	<form name=form_mod_pre method=post action='$enlacevolver"."$script'>
	<input type=hidden name=accion value='accionmodificar'>
	<input type=hidden name=$campopadre value='".$$campopadre."'>";
	}	

	$consulta  = "select $string_para_select from $tabla $join";
	if ($condiciones != "") { $consulta .= " where $condiciones "; }
	// Vemos si existe un orderby
	if ($_REQUEST[orderby] != "")
	{
		$order = " order by $tabla.$_REQUEST[orderby]";
		$parametros .= "&orderby=".$_REQUEST[orderby];
	}
	else { $order = " order by $tabla.$campo_busqueda";  }
/*
	if ($pag != "") {
		// la primera pagina es la 0
		$inicio = $pag*$registros_por_pagina;
		$limit = " limit $inicio,$registros_por_pagina";
	}
	else { $limit = ""; }
*/
	$limit = "";
	$consulta .= " group by $tabla.id $order $limit;";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: $consulta " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		$mostrar_submit = 0;
		echo "
	<tr>";
		foreach ($campos_listado as $cuenta_campos => $campo)
		{
			$nombre = "";
			list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod[$cuenta_campos]);
			if ($ele != 'si')
			{
				$nombre = "$linea[$campo]";
			}
			elseif ($ele2 == "date")
			{
				if ($linea[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
				else { $nombre = date("d-m-Y",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "datetime")
			{
				if ($linea[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
				else { $nombre = date("d-m-Y H:i",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "checkbox")
			{
				($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
			}
			elseif ($ele2 == "time")
			{
				list($temp1, $temp2, $temp3) = explode(':',$linea[$campo]);
				$nombre = $temp1.":".$temp2;
			}
			elseif ($linea[$campo] != "")
			{
				$consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre = $lineaselect[$ele3];
					if ($campo == "articulo_id")
					{
						$nombre = "($lineaselect[referencia]) $lineaselect[$ele3]";
					}
				}
			}
			if ($campo == "tipo_embalaje_id")
			{
				if ($linea['reservado'] == "" && $pre_estado_id == $presu_estado_creado)
				{
					$nombre = "<select name='$campo"."_"."$linea[id]' id='$campo"."_"."$linea[id]'>";
					$consulta_cat = "select $tabla_art_embalajes.id, $tabla_art_embalajes.unidades, $tabla_m_embalajes.nombre, $tabla_art_embalajes.tipo_embalaje_id from $tabla_art_embalajes
join $tabla_m_embalajes on $tabla_m_embalajes.id=$tabla_art_embalajes.tipo_embalaje_id 
where $tabla_art_embalajes.articulo_id='".$linea['articulo_id']."' and $tabla_art_embalajes.inactivo=''
order by $tabla_art_embalajes.defecto_presupuesto desc, $tabla_art_embalajes.tipo_embalaje_id, $tabla_art_embalajes.unidades;";
					$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
					{
						$nombre .= "<option value='$linea_cat[id]'"; if ($linea_cat['tipo_embalaje_id'] == $linea['tipo_embalaje_id'] && $linea_cat['unidades'] == $linea['unidades_embalaje']) { $nombre .= " selected"; } $nombre .= ">$linea_cat[nombre] de $linea_cat[unidades]</option>";
					}
					$nombre .= "</select>";
				}
				else
				{
					$nombre .= " de ".$linea['unidades_embalaje'];
				}
			}
			if ($campo == "unidades")
			{
				if ($linea['reservado'] == "" && $pre_estado_id == $presu_estado_creado)
				{
					// solo se permitira modificar las unidades si el presupuesto esta en estado creado (antes de aceptarlo) y si el articulo no esta reservado
					$mostrar_submit = 1;
					$nombre = "
	<input type='text' name='$campo"."_"."$linea[id]' id='$campo"."_"."$linea[id]' size='5' value='$linea[$campo]' onkeyup='validar($campo"."_"."$linea[id]);' >";
				}
				if ($linea['unidades_gratis'] > 0)
				{
					$nombre .= " + ".$linea['unidades_gratis'];
				}
			}
			if (in_array($campo,array("precio_unidad","igic_id","importe_articulo","importe_igic")))
			{
				if (in_array($campo,array("importe_articulo","importe_igic")) && $linea['reservado'] == "on")
				{
					// si el articulo esta reservado (pre_articulos_t.reservado=on) no se visualiza el importe
					$nombre = 0;
				}
				$nombre = number_format($nombre, 2, ",",".");
				if (in_array($campo,array("precio_unidad","importe_articulo","importe_igic"))) { $nombre .= " &euro;"; }
				if ($campo == "igic_id") { $nombre .= " %"; }
			}
			if (in_array($campo,array("dto_porc","pronto_pago")))
			{
				if ($linea[$campo] > 0) { $nombre = number_format($linea[$campo], 2, ",",".")." %"; }
				else { $nombre = ""; }
			}
			if ($campo == "dto2" || $campo == "dto3")
			{
				if ($linea['reservado'] == "" && $pre_estado_id == $presu_estado_creado)
				{
					// solo se permitira modificar los descuentos si el presupuesto esta en estado creado (antes de aceptarlo) y si el articulo no esta reservado
					$mostrar_submit = 1;
					$nombre = "
	<input type='text' name='$campo"."_"."$linea[id]' id='$campo"."_"."$linea[id]' size='5' value='$linea[$campo]' onkeyup='validarFloat($campo"."_"."$linea[id]);' >";
				}
				else
				{
					if ($linea[$campo] > 0) { $nombre = number_format($linea[$campo], 2, ",",".")." %"; }
					else { $nombre = ""; }
				}
			}
			if ($campo == "fecha_max_reserva" && $linea[$campo] != "0000-00-00" && $linea['reservado'] == "on" && $pre_estado_id == $presu_estado_creado)
			{
				// si el articulo esta reservado se permite quitar la reserva
				$mostrar_submit = 1;
				list($fecha, $reloj) = explode(' ', $linea[$campo]);
				list($ano, $mes, $dia) = explode('-', $fecha);
				$nombre = "
<input type='text' name='$campo"."_"."$linea[id]' id='$campo"."_"."$linea[id]' size='10' value='$dia/$mes/$ano'/ >
<img src='images/calendar.gif' name='boton"."$campo"."_"."$linea[id]' border='0' id='boton$campo"."_"."$linea[id]' style='cursor: pointer; border: 0px solid blue;' title='Seleccione fecha' />
<script type='text/javascript'>
	Calendar.setup({
		inputField	:	'$campo"."_"."$linea[id]',		// id of the input field
		trigger		:	'boton"."$campo"."_"."$linea[id]',	// trigger for the calendar (button ID)
		onSelect	:	function() { this.hide() },
		min		:	".date("Ymd")."
	});
</script>";
			}
			if ($campo == "comprometido" && $linea[$campo] > 0 && $pre_estado_id == $presu_estado_creado)
			{
				// si el articulo tiene unidades comprometidas se permite modificar o quitar
				$mostrar_submit = 1;
				$nombre = "
<input type='text' name='$campo"."_"."$linea[id]' id='$campo"."_"."$linea[id]' size='5' value='$linea[$campo]' onkeyup='validar($campo"."_"."$linea[id]);' >";
			}
			if (in_array($campo,array("precio_unidad","dto_porc","dto2","dto3","importe_articulo","importe_igic")))
			{
				echo "
		<td align='right'>$nombre</td>";
			}
			else
			{
				echo "
		<td>$nombre</td>";
			}
			if ($campo == "unidades")
			{
				// calculo del stock disponible (no contar las reservas)
				$cantidad_stock = CantidadStock($linea['articulo_id'], $linea['proveedor_id']);
				
				// calculo de las unidades ya pedidas en otros pedidos (aceptados, no finalizados, no entregados y sin marcar como no preparable)
				$cantidad_pedido = CantidadPedido($linea['articulo_id'], $linea['proveedor_id']);
				
				echo "
		<td>$cantidad_stock</td>
		<td>$cantidad_pedido</td>";
			}
		}
		echo "<td>";
		//echo "			<a href='$enlacevolver"."$script&accion=formmodificar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
		if ($pre_estado_id == $presu_estado_creado && $linea['reservado'] != "on")
		{
			// inicio formulario de borrar
			echo "<a href='$enlacevolver"."$script&accion=formborrar&id=$linea[id]&pag=$pag$parametros' class='buttonmario smallmario red' style='color:#ffffff;'>eliminar</a>";
		}
		echo "
		</td>";
		echo "</tr>";
		echo "<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas'></td></tr>";
	}
	if ($pre_estado_id == $presu_estado_creado)
	{
		echo "<tr align='center'><td colspan='$columnas'><input type=submit value='Actualizar'></td></tr>";
		// fin formulario de modificar
		echo "
	</form>";
	}
	echo "</table>";
/*
	if ($pag != "")
	{
		$pag_visual = $pag+1;
		// Visualizamos las paginas existentes
		$consulta2  = "select count($tabla.id) as num from $tabla $join";
		if ($condiciones != "") { $consulta2 .= " where $condiciones;"; } else { $consulta2 .= ";"; }
		$resultado2 = mysql_query($consulta2) or die("$consulta2, La consulta fall&oacute;: " . mysql_error());
		//echo "$consulta2<br>";
		while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC)) {
			$exp = "$linea2[num]";
		}
		if ($exp > $registros_por_pagina)
		{
			echo PaginadoListado($pag, $enlacevolver.$script, $parametros, $exp, $registros_por_pagina);
		}
	}
*/
	echo "<hr>";
}
// FIN DE OBTENER EL LISTADO INICIAL

// FORMULARIO PARA BORRAR UN REGISTRO
if ($accion == "formborrar")
{
	$pre_estado_id = 0;
	$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
	//echo "$consulta_padre";
	$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
	while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
	{
		$pre_estado_id = $linea_padre['estado_id'];
	}
	if ($pre_estado_id == $presu_estado_creado)
	{
//----------------------------------
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "") { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "") { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "") { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { $parametros .= "&orderby=".$_REQUEST[orderby]; }
	echo "<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<tr>
		<td><center><b>BORRADO DE $nombre_objeto</b> [<a href='$enlacevolver"."$script$parametros'>Volver sin cambios</a>]</center></td>
	</tr>
	<tr valign='top'>
		<td>
		<form name=form_buscar method=post action='$enlacevolver"."$script'>
		<input type=hidden name=accion value=accionborrar>
		<input type=hidden name=id value=$_GET[id]>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "") { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "") { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "") { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { echo "<input type=hidden name=orderby value='".$_REQUEST[orderby]."'>"; }
	// Obtenemos los valores actuales del registro que se esta modificando
	$consultamod = "select * from $tabla where id=$_GET[id];";
	$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: " . mysql_error());
	// El resultado lo metemos en un array asociativo
	$arraymod = array();
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC)) {
		foreach ($campos_col1 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
		foreach ($campos_col2 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
	} // del while

	echo "
		<table width='100%' border='0'>
			<tr><td colspan='2'><input type=submit value='Va usted a borrar el registro con los siguientes datos'></td></tr>
			<tr><td width='50%'><table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col1 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col1[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col1[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col1[$cuenta_campos]);
		$nombre_campo = $nombres_col1[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: $dia M: $mes A: $ano</td></tr>";
		}
		if ($ele == "checkbox") { echo "<tr><td><b>$nombre_campo</b></td><td>";if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; } echo "</td></tr>"; }
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele3 where $ele5='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele4]";
				}
			}
			echo "</td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele2 where $ele4='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele3]";
				}
			}
			echo "</td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3 - $valor4</td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "email") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "float") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
				echo "\n<td align='center' width='5%'>$nombre_elemento</td>";
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			if ($arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			$id_codificado = base64_encode($_GET[id]);
			echo "Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3</td></tr>";
		}
		if ($ele == "calendar")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>$dia/$mes/$ano</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>$hora : $minutos</td></tr>";
		}
		if ($ele == "numerico") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		$cuenta_campos++;
	} // del foreach
	echo "</table>";

	// Vamos a por la columna 2
	echo "</td><td>";

	echo "<table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col2 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col2[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col2[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col2[$cuenta_campos]);
		$nombre_campo = $nombres_col2[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: $dia M: $mes A: $ano</td></tr>";
		}
		if ($ele == "checkbox") { echo "<tr><td><b>$nombre_campo</b></td><td>";if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; } echo "</td></tr>"; }
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele3 where $ele5='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele4]";
				}
			}
			echo "</td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele2 where $ele4='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele3]";
				}
			}
			echo "</td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3 - $valor4</td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "email") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "float") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
				echo "\n<td align='center' width='5%'>$nombre_elemento</td>";
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			if ($arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			$id_codificado = base64_encode($_GET[id]);
			echo "Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3</td></tr>";
		}
		if ($ele == "calendar")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>$dia/$mes/$ano</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>$hora : $minutos</td></tr>";
		}
		if ($ele == "numerico") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		$cuenta_campos++;
	} // del foreach
	echo "</table>";
	echo "</td></tr></table>";
	echo "</form></td></tr></table>";
//----------------------------------
	}
	else
	{
		echo "<b>No se puede borrar</b>";
	}
}
// FIN FORMULARIO BORRAR

// INICIO A�ADIR ARTICULO NUEVO
if ($accion != "formborrar")
{
	$pre_cliente_id = 0;
	$pre_estado_id = 0;
	$valor_fecha = "";
	$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
	//echo "$consulta_padre";
	$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
	while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
	{
		$pre_cliente_id = $linea_padre['cliente_id'];
		$pre_estado_id = $linea_padre['estado_id'];
		$valor_fecha = $linea_padre['fecha'];
	}
	
	if ($pre_estado_id == $presu_estado_creado)
	{
		// formulario de busqueda
		echo "
<table align='center'>
	<form name=\"buscar\" id=\"buscar\" method=\"post\" action=\"$enlacevolver"."$script\">
	<input type=hidden name=accion value='buscar'>
	<input type=hidden name=$campopadre value='".$$campopadre."'>
	<tr align='center'>
		<td>Buscar art&iacute;culo:</td>
		<td><input type='text' name=b_nombre2 value='$b_nombre2'></td>
		<td><input type=submit name='Buscar' value='Buscar'></td>
	</tr>
	</form>
</table>";
		
		// visualizacion de los resultados
		//si no activa busqueda no muestra articulos
		if ($b_nombre2 != "")
		{
$array_var_session = array('presupuestoAmpliado'.$$campopadre);
echo "inicio sesion<br>";
foreach ($array_var_session as $valor_session)
{
	if (is_array($_SESSION[$valor_session])) { echo $valor_session.": <pre>".print_r($_SESSION[$valor_session],true)."</pre><br>"; }
	else { echo $valor_session.": ".$_SESSION[$valor_session]."<br>"; }
}
echo "fin sesion<br>";
			echo "
<table width='100%' bgcolor='#$color_fondo_amarillo'>
	<tr align='center' bgcolor='#$color_fondo'>";
			foreach ($nombres_bloque_1 as $nombre_colum)
			{
				echo "
		<td><font color='#ffffff'><b>$nombre_colum</b></font></td>";
			}
			echo "
	</tr>";
			$condiciones = "$tabla_cli_prov.cliente_id='".$pre_cliente_id."' and $tabla_cli_prov.proveedor_id=$tabla_proveedores.id and $tabla_cli_prov.bloqueado='' and $tabla_proveedores.inactivo='' and $tabla_proveedores.tipo_servicio_id!='".$tipo_servicio_directo."' and $tabla_art_embalajes.inactivo='' and $tabla_art_precios.proveedor_id=$tabla_proveedores.id";
			$parametros = "&buscar_nombre=$b_nombre2";
			//desgloso buscar_nombre si tiene espacios en blanco
			$trozos = array();
			$trozos = explode(" ",$b_nombre2);
			foreach ($trozos as $valor)
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= "($tabla_articulos.nombre like '%$valor%' or $tabla_articulos.referencia like '%$valor%')";
			}
			echo "
	<form name=\"bloque1\" method=\"post\" action=\"$enlacevolver"."$script\">
	<input type=\"hidden\" name=\"accion\" value=\"actualizabloque2\">
	<input type=\"hidden\" name=\"$campopadre\" value=\"".$$campopadre."\">
	<input type=\"hidden\" name=\"b_nombre2\" value=\"$b_nombre2\">";
	// se buscaran aquellos articulos que:
	// - cumpla lo buscado en el filtro (nombre o referencia)
	// - tengan algun embalaje definido y no este inactivo
	// - lo trabaje algun proveedor que trabaje con el cliente del presupuesto (familia definida en prov_familias_t y clientes_prov_familias_t)
	// - el proveedor no este inactivo, no sea de tipo servico "directo"
	// - el proveedor no este bloqueado para ese cliente
	// - que tenga el precio definido para el proveedor
			
			$array_cesta = array();
			$array_cesta = $_SESSION['presupuestoAmpliado'.$$campopadre];
			$cont = 1;
			$consulta_bloque1 = "select $tabla_articulos.id, $tabla_articulos.nombre, $tabla_articulos.referencia, $tabla_articulos.familia_id, $tabla_cli_prov.proveedor_id, $tabla_cli_prov.dto_porc_pronto_pago, $tabla_prov_familias.igic_id 
from $tabla_articulos 
join $tabla_art_embalajes on $tabla_art_embalajes.articulo_id=$tabla_articulos.id 
join $tabla_art_precios on $tabla_art_precios.articulo_id=$tabla_articulos.id 
join $tabla_prov_familias on $tabla_prov_familias.familia_id=$tabla_articulos.familia_id 
join $tabla_proveedores on $tabla_proveedores.id=$tabla_prov_familias.proveedor_id 
join $tabla_cli_prov_fam on $tabla_cli_prov_fam.familia_id=$tabla_articulos.familia_id 
join $tabla_cli_prov on $tabla_cli_prov.id=$tabla_cli_prov_fam.cliente_prov_id 
where $condiciones and $tabla_prov_familias.igic_id>0 
group by $tabla_articulos.id, $tabla_cli_prov.proveedor_id 
order by $tabla_articulos.nombre;";
			//echo "$consulta_bloque1<br>";
			$res_bloque1 = mysql_query($consulta_bloque1) or die("$consulta_bloque1, La consulta fall�: $consulta_bloque1" . mysql_error());
			while ($lin_bloque1 = mysql_fetch_array($res_bloque1, MYSQL_ASSOC))
			{
				$existe_art = 0;
				$cons = "select id from $tabla where $campopadre='".$$campopadre."' and articulo_id='".$lin_bloque1['id']."' and proveedor_id='".$lin_bloque1['proveedor_id']."';";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$existe_art = 1;
				}
				if ($existe_art == 0)
				{
					// inicio articulo que no esta en presupuesto
					$ano = "0000"; $mes = "00"; $dia = "00";
					$color_fondo_articulo = "";
					$valor_existente_embalaje = "";
					$valor_existente_unidades = "";
					$valor_existente_dto2 = "";
					$valor_existente_dto3 = "";
					$valor_existente_comprometido = "";
					$indice_cesta = $lin_bloque1['id']."/".$lin_bloque1['proveedor_id'];
					if (array_key_exists($indice_cesta,$array_cesta))
					{
						// existe el articulo para este proveedor en la cesta
						$color_fondo_articulo = " bgcolor='#E8CD71'";
						$valor_existente_embalaje = $array_cesta[$indice_cesta]["embalaje"];
						$valor_existente_unidades = $array_cesta[$indice_cesta]["unidades"];
						$valor_existente_dto2 = $array_cesta[$indice_cesta]["dto2"];
						$valor_existente_dto3 = $array_cesta[$indice_cesta]["dto3"];
						list($ano,$mes,$dia) = explode("-",$array_cesta[$indice_cesta]["fecha_reserva"]);
						$valor_existente_comprometido = $array_cesta[$indice_cesta]["comprometido"];
					}
if (true){
					$nombre_proveedor = "";
					$consulta_cat = "select * from $tabla_proveedores where $tabla_proveedores.id='".$lin_bloque1['proveedor_id']."';";
					$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
					{
						$nombre_proveedor = $linea_cat['nombre_corto'];
					}
					$valor_igic = "";
					$consulta_cat = "select * from $tabla_m_igic where $tabla_m_igic.id='".$lin_bloque1['igic_id']."';";
					$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
					{
						$valor_igic = $linea_cat['valor'];
					}
					$valor_precio = "";
					$consulta_cat = "select * from $tabla_art_precios where $tabla_art_precios.articulo_id='".$lin_bloque1['id']."' and $tabla_art_precios.proveedor_id='".$lin_bloque1['proveedor_id']."';";
					$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
					{
						$valor_precio = $linea_cat['precio'];
					}
					
					// calculos para el precio de proveedor
					$precio_prov = $valor_precio;
					$dto_porc_prov_fam = "";
					$consulta1 = "select * from $tabla_prov_familias where proveedor_id='".$lin_bloque1['proveedor_id']."' and familia_id='".$lin_bloque1['familia_id']."';";
					//echo "$consulta1<br>";
					$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1" . mysql_error());
					while ($linea1 = mysql_fetch_array($resultado1, MYSQL_ASSOC))
					{
						$dto_porc_prov_fam = $linea1['dto_porc_prov_fam_deposito'];
					}
					if ($dto_porc_prov_fam > 0)
					{
						$precio_prov -= round(($precio_prov*$dto_porc_prov_fam)/100, 2);
					}
					// calculos para el precio de cliente
					$precio_cli = $valor_precio;
					$dto_porc_cli_fam = "";
					$dto_porc_cli_art = "";
					$precio_neto_cli_art = "";
					$consulta1 = "select $tabla_cli_prov_fam.* from $tabla_cli_prov_fam join $tabla_cli_prov on $tabla_cli_prov.id=$tabla_cli_prov_fam.cliente_prov_id where $tabla_cli_prov.cliente_id='".$pre_cliente_id."' and $tabla_cli_prov.proveedor_id='".$lin_bloque1['proveedor_id']."' and $tabla_cli_prov_fam.familia_id='".$lin_bloque1['familia_id']."';";
					//echo "$consulta1<br>";
					$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1" . mysql_error());
					while ($linea1 = mysql_fetch_array($resultado1, MYSQL_ASSOC))
					{
						$dto_porc_cli_fam = $linea1['dto_porc_cli_fam_deposito'];
						// busqueda de los descuentos del articulo para el cliente
						$consulta2 = "select * from $tabla_cli_prov_fam_art where cliente_prov_familia_id='$linea1[id]' and articulo_id='".$lin_bloque1['id']."';";
						//echo "$consulta2<br>";
						$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: $consulta2" . mysql_error());
						while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC))
						{
							$dto_porc_cli_art = $linea2['dto_porc_cli_art_deposito'];
							$precio_neto_cli_art = $linea2['precio_neto_cli_art_deposito'];
						}
					}
					$precio_cli = $valor_precio;
					if ($precio_neto_cli_art > 0)
					{
						$precio_cli = $precio_neto_cli_art;
					}
					elseif ($dto_porc_cli_art > 0)
					{
						$precio_cli -= round(($precio_cli*$dto_porc_cli_art)/100, 2);
					}
					elseif ($dto_porc_cli_fam > 0)
					{
						$precio_cli -= round(($precio_cli*$dto_porc_cli_fam)/100, 2);
					}
					
					
					// calculos para el precio por ofertas
					$precio_oferta = $valor_precio;
					$precio_neto_oferta_art = "";
					$dto_porc_oferta_art = "";
					$min_unidades_dto = "";
					$precio_unidad_dto = "";
					$min_unidades_gratis = "";
					$num_unidades_gratis = "";
					$dto_porc_oferta_fam = "";
					$texto_oferta_reduccion_por_cantidad = "";
					$texto_oferta_regalo_por_compra = "";
					$valor_prov_oferta_id = 0;
					// busqueda de ofertas:
					// - de este proveedor, que no esta inactivo
					// - de este cliente, que no este bloqueado
					// - que la fecha de hoy este entre sus fechas
					// - que se aplica en deposito
//join $tabla_prov_oferta_fam on $tabla_prov_oferta_fam.prov_oferta_id=$tabla_prov_ofertas.id 
					$consulta1 = "select $tabla_prov_ofertas.id from $tabla_prov_ofertas 
join $tabla_proveedores on $tabla_proveedores.id=$tabla_prov_ofertas.proveedor_id 
join $tabla_prov_ofertas_cli on $tabla_prov_ofertas_cli.prov_oferta_id=$tabla_prov_ofertas.id 
join $tabla_cli_prov on $tabla_cli_prov.proveedor_id=$tabla_proveedores.id 
join $tabla_prov_oferta_fam on $tabla_prov_oferta_fam.prov_oferta_id=$tabla_prov_ofertas.id 
where $tabla_prov_ofertas.proveedor_id='".$lin_bloque1['proveedor_id']."' and $tabla_prov_ofertas.fecha_inicio<='".$valor_fecha."' and $tabla_prov_ofertas.fecha_fin>='".$valor_fecha."' and $tabla_proveedores.inactivo='' and $tabla_prov_ofertas_cli.cliente_id='".$pre_cliente_id."' and $tabla_cli_prov.cliente_id='".$pre_cliente_id."' and $tabla_cli_prov.proveedor_id='".$lin_bloque1['proveedor_id']."' and $tabla_cli_prov.bloqueado='' and $tabla_prov_ofertas.aplica_deposito='on' and $tabla_prov_oferta_fam.familia_id='".$lin_bloque1['familia_id']."' order by $tabla_prov_ofertas.fecha_fin asc limit 1;";
// and $tabla_prov_oferta_fam.familia_id='".$familia_art_id."'
					//echo "$consulta1<br>";
					$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1 " . mysql_error());
					while ($linea1 = mysql_fetch_array($resultado1, MYSQL_ASSOC))
					{
						$valor_prov_oferta_id = $linea1['id'];
					}
					if ($valor_prov_oferta_id > 0)
					{
						$consulta1 = "select * from $tabla_prov_oferta_fam where prov_oferta_id='".$valor_prov_oferta_id."' and familia_id='".$lin_bloque1['familia_id']."';";
						//echo "$consulta1<br>";
						$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1 " . mysql_error());
						while ($linea1 = mysql_fetch_array($resultado1, MYSQL_ASSOC))
						{
							if ($linea1['dto_porc_oferta_fam'] > 0)
							{
								$dto_porc_oferta_fam = $linea1['dto_porc_oferta_fam'];
							}
							$consulta2 = "select * from $tabla_prov_oferta_fam_art where prov_oferta_familia_id='".$linea1['id']."' and articulo_id='".$lin_bloque1['id']."';";
							//echo "$consulta2<br>";
							$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: $consulta2 " . mysql_error());
							while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC))
							{
								if ($linea2['precio_neto_oferta_art'] > 0)
								{
									$precio_neto_oferta_art = $linea2['precio_neto_oferta_art'];
									$precio_oferta = $linea2['precio_neto_oferta_art'];
								}
								elseif ($linea2['dto_porc_oferta_art'] > 0)
								{
									$dto_porc_oferta_art = $linea2['dto_porc_oferta_art'];
									$dto_oferta = round(($valor_precio*$linea2['dto_porc_oferta_art'])/100, 2);
									$precio_oferta = $valor_precio-$dto_oferta;
								}
								elseif ($linea1['dto_porc_oferta_fam'] > 0)
								{
									$dto_porc_oferta_fam = $linea1['dto_porc_oferta_fam'];
									$dto_oferta = round(($valor_precio*$linea1['dto_porc_oferta_fam'])/100, 2);
									$precio_oferta = $valor_precio-$dto_oferta;
								}
//jgs => caso especial
								if ($linea2['min_unidades_dto'] > 0 && $linea2['precio_unidad_dto'] > 0)
								{
									$texto_oferta_reduccion_por_cantidad = "Comprando ".$linea2['min_unidades_dto']." unidades, la unidad sale a ".number_format($linea2['precio_unidad_dto'],2,",",".")." &euro;.";
									$min_unidades_dto = $linea2['min_unidades_dto'];
									$precio_unidad_dto = $linea2['precio_unidad_dto'];
								}
								if ($linea2['min_unidades_gratis'] > 0 && $linea2['num_unidades_gratis'] > 0)
								{
									$min_unidades_gratis = $linea2['min_unidades_gratis'];
									$num_unidades_gratis = $linea2['num_unidades_gratis'];
								}
							}
							if ($precio_neto_oferta_art > 0)
							{
								$precio_oferta = $precio_neto_oferta_art;
							}
							elseif ($dto_porc_oferta_art > 0)
							{
								$dto_oferta = round(($valor_precio*$dto_porc_oferta_art)/100, 2);
								$precio_oferta = $valor_precio-$dto_oferta;
							}
							elseif ($dto_porc_oferta_fam > 0)
							{
								$dto_oferta = round(($valor_precio*$dto_porc_oferta_fam)/100, 2);
								$precio_oferta = $valor_precio-$dto_oferta;
							}
						}
						$consulta1 = "select * from $tabla_prov_ofertas where id='".$valor_prov_oferta_id."';";
						//echo "$consulta1<br>";
						$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1 " . mysql_error());
						while ($linea1 = mysql_fetch_array($resultado1, MYSQL_ASSOC))
						{
							if ($linea1['min_importe_regalo'] > 0 && $linea1['regalo'] != "")
							{
								$texto_oferta_regalo_por_compra = "Si compra ".number_format($linea1['min_importe_regalo'],2,",",".")." &euro; le regalamos ".$linea1['regalo'].".";
							}
						}
					}
					$valor_precio_inicial = 0;
					$valor_dto1 = 0; // para guardarlo en pre_articulos_t.dto_porc
					$valor_precio_neto = 0; // para guardarlo en pre_articulos_t.precio_neto
					if ($valor_prov_oferta_id > 0 && $precio_oferta < $valor_precio)
					{
						// hay un descuento por oferta
						if ($precio_neto_oferta_art > 0)
						{
							$valor_precio_neto = $precio_neto_oferta_art;
						}
						elseif ($dto_porc_oferta_art > 0)
						{
							$valor_dto1 = $dto_porc_oferta_art;
						}
						elseif ($dto_porc_oferta_fam > 0)
						{
							$valor_dto1 = $dto_porc_oferta_fam;
						}
						$valor_precio_inicial = $precio_oferta;
					}
					elseif ($precio_cli < $valor_precio)
					{
						// hay un descuento por cliente
						if ($precio_neto_cli_art > 0)
						{
							$valor_precio_neto = $precio_neto_cli_art;
						}
						elseif ($dto_porc_cli_art > 0)
						{
							$valor_dto1 = $dto_porc_cli_art;
						}
						elseif ($dto_porc_cli_fam > 0)
						{
							$valor_dto1 = $dto_porc_cli_fam;
						}
						$valor_precio_inicial = $precio_cli;
					}
					elseif ($precio_prov < $valor_precio)
					{
						// hay un descuento por proveedor
						$valor_dto1 = $dto_porc_prov_fam;
						$valor_precio_inicial = $precio_prov;
					}
					else
					{
						$valor_precio_inicial = $valor_precio;
					}
					
					// en codigo solo se puede calcular el precio final aplicando el pronto pago
					// con javascript se calculara el dto2 y dto3
					$valor_precio_final = $valor_precio_inicial;
					if ($lin_bloque1['dto_porc_pronto_pago'] > 0)
					{
						$dto_pronto_pago = round(($valor_precio_inicial*$lin_bloque1['dto_porc_pronto_pago'])/100, 2);
						$valor_precio_final = $valor_precio_inicial-$dto_pronto_pago;
					}
					
					// calculo del stock disponible (no contar las reservas)
					$cantidad_stock = 0;
					$cantidad_stock = CantidadStock($lin_bloque1['id'], $lin_bloque1['proveedor_id']);
						
					// calculo de las unidades ya pedidas en otros pedidos (aceptados, no finalizados, no entregados y sin marcar como no preparable)
					$cantidad_pedido = CantidadPedido($lin_bloque1['id'], $lin_bloque1['proveedor_id']);
} // fin true
					
					// nombres campos
					$nombre_embalaje = "embalaje_".$cont;
					$nombre_num_embalajes = "num_embalajes_".$cont;
					$nombre_ud_embalajes = "ud_embalajes_".$cont;
					$nombre_unidades = "unidades_".$cont;
					$nombre_txt_gratis = "txt_gratis_".$cont;
					$nombre_precio_inicial = "precio_inicial_".$cont;
					$nombre_precio_inicial_sin_oferta = "precio_inicial_sin_".$cont;
					$nombre_min_unidades_dto = "min_unidades_dto_".$cont;
					$nombre_precio_unidad_dto = "precio_unidad_dto_".$cont;
					$nombre_min_unidades_gratis = "min_unidades_gratis_".$cont;
					$nombre_num_unidades_gratis = "num_unidades_gratis_".$cont;
					$nombre_unidades_gratis = "unidades_gratis_".$cont;
					$nombre_precio_final = "precio_final_".$cont;
					$nombre_precio_neto = "precio_neto_".$cont;
					$nombre_dto1 = "dto1_".$cont; // definido en la oferta, en el cliente o en el proveedor, puede ser por familia o por articulo
					$nombre_dto1_sin_oferta = "dto1_sin_".$cont;
					$nombre_dto2 = "dto2_".$cont;
					$nombre_dto3 = "dto3_".$cont;
					$nombre_pronto_pago = "pronto_pago_".$cont;
					$nombre_fecha_reserva = "fecha_reserva_".$cont;
					$nombre_comprometido = "comprometido_".$cont;
					echo "
	<tr valign='top' $color_fondo_articulo>
		<input type=\"hidden\" name=\"referencia_"."$cont\" value=\"".$lin_bloque1['id']."\">
		<input type=\"hidden\" name=\"proveedor_id_"."$cont\" value=\"".$lin_bloque1['proveedor_id']."\">
		<input type=\"hidden\" name=\"igic_id_"."$cont\" value=\"".$lin_bloque1['igic_id']."\">
		<input type=\"hidden\" name=\"precio_articulo_"."$cont\" value=\"".$valor_precio."\">
		<td>(".$lin_bloque1['referencia'].")</td>
		<td><img src=\"images/help.png\" onClick=\"abrirStock(".$lin_bloque1['id'].", ".$lin_bloque1['proveedor_id'].");\" onLoad=\"cambiarUnidades(".$cont.")\" />&nbsp;".$lin_bloque1['nombre']."</td>
		<td><select name=\"".$nombre_embalaje."\" onChange=\"cambiarEmbalaje(this)\">";
					$valor_inicial = "";
					$texto_opciones_select = "";
					$consulta_cat = "select $tabla_art_embalajes.id, $tabla_art_embalajes.unidades, $tabla_m_embalajes.nombre from $tabla_art_embalajes
join $tabla_m_embalajes on $tabla_m_embalajes.id=$tabla_art_embalajes.tipo_embalaje_id 
where $tabla_art_embalajes.articulo_id='".$lin_bloque1['id']."' and $tabla_art_embalajes.inactivo=''
order by $tabla_art_embalajes.defecto_presupuesto desc, $tabla_art_embalajes.tipo_embalaje_id, $tabla_art_embalajes.unidades;";
					$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
					{
						if ($valor_existente_embalaje == "")
						{
							if ($valor_inicial == "") { $valor_inicial = $linea_cat['unidades']; }
						}
						else
						{
							if ($valor_existente_embalaje == $linea_cat['id']) { $valor_inicial = $linea_cat['unidades']; }
						}
						echo "<option value='$linea_cat[id]'"; if ($valor_existente_embalaje != "" && $valor_existente_embalaje == $linea_cat['id']) { echo " selected"; }
						echo ">$linea_cat[nombre] de $linea_cat[unidades]</option>";
						$texto_opciones_select .= "
			<input type=\"hidden\" name=\"".$nombre_ud_embalajes."_".$linea_cat['id']."\" id=\"".$nombre_ud_embalajes."_".$linea_cat['id']."\" value=\"".$linea_cat['unidades']."\" >";
					}
					echo "</select>".$texto_opciones_select."
			<input type=\"hidden\" name=\"".$nombre_ud_embalajes."\" id=\"".$nombre_ud_embalajes."\" size=\"4\" value=\"".$valor_inicial."\" ></td>
		<td><input type=\"text\" name=\"".$nombre_num_embalajes."\" id=\"".$nombre_num_embalajes."\" size=\"4\" value=\"\" onkeyup='cambiarUdEmbalajes(this);'></td>
		<td><input type=\"text\" name=\"".$nombre_unidades."\" id=\"".$nombre_unidades."\" size=\"4\" value=\"".$valor_existente_unidades."\" onkeyup='cambiarUnidades(".$cont.");'>
			<span id=\"".$nombre_txt_gratis."\"></span>
		</td>
		<td>$cantidad_stock</td>
		<td>$cantidad_pedido</td>
		<td>$nombre_proveedor</td>
		<td><input type=\"text\" readonly=\"readonly\" name=\"".$nombre_precio_inicial."\" id=\"".$nombre_precio_inicial."\" size=\"4\" value=\"".number_format($valor_precio_inicial, 2)."\"> &euro;</b>
			<input type=\"hidden\" name=\"".$nombre_precio_inicial_sin_oferta."\" id=\"".$nombre_precio_inicial_sin_oferta."\" value=\"".number_format($valor_precio_inicial, 2)."\">
			<input type=\"hidden\" name=\"".$nombre_min_unidades_dto."\" id=\"".$nombre_min_unidades_dto."\" value=\"".$min_unidades_dto."\">
			<input type=\"hidden\" name=\"".$nombre_precio_unidad_dto."\" id=\"".$nombre_precio_unidad_dto."\" value=\"".number_format($precio_unidad_dto, 2)."\">
			<input type=\"hidden\" name=\"".$nombre_min_unidades_gratis."\" id=\"".$nombre_min_unidades_gratis."\" value=\"".$min_unidades_gratis."\">
			<input type=\"hidden\" name=\"".$nombre_num_unidades_gratis."\" id=\"".$nombre_num_unidades_gratis."\" value=\"".$num_unidades_gratis."\">
			<input type=\"hidden\" readonly=\"readonly\" name=\"".$nombre_unidades_gratis."\" id=\"".$nombre_unidades_gratis."\" value=\"\">
		</td>
		<td><input type=\"text\" readonly=\"readonly\" name=\"".$nombre_dto1."\" id=\"".$nombre_dto1."\" size=\"4\" value=\"".number_format($valor_dto1, 2)."\"> %
			<input type=\"hidden\" readonly=\"readonly\" name=\"".$nombre_dto1_sin_oferta."\" id=\"".$nombre_dto1_sin_oferta."\" value=\"".number_format($valor_dto1, 2)."\">
			<input type=\"hidden\" readonly=\"readonly\" name=\"".$nombre_precio_neto."\" id=\"".$nombre_precio_neto."\" value=\"".$valor_precio_neto."\"></td>
		<td><input type=\"text\" name=\"".$nombre_dto2."\" id=\"".$nombre_dto2."\" size=\"4\" value=\"".$valor_existente_dto2."\" onkeyup='cambiarDto(this);'> *</td>
		<td><input type=\"text\" name=\"".$nombre_dto3."\" id=\"".$nombre_dto3."\" size=\"4\" value=\"".$valor_existente_dto3."\" onkeyup='cambiarDto(this);'> *</td>
		<td><input type=\"text\" readonly=\"readonly\" name=\"".$nombre_pronto_pago."\" id=\"".$nombre_pronto_pago."\" size=\"4\" value=\"".number_format($lin_bloque1['dto_porc_pronto_pago'], 2)."\"> %</td>
		<td><input type=\"text\" readonly=\"readonly\" name=\"".$nombre_precio_final."\" id=\"".$nombre_precio_final."\" size=\"4\" value=\"".number_format($valor_precio_final, 2)."\"> &euro;</td>
		<td>";
					if ($lin_bloque1['igic_id'] > 0) { echo number_format($valor_igic, 2, ",", ".")." %"; }
					else { echo "No definido"; }
					echo "</td>";
					echo "<td><input type='text' name='$nombre_fecha_reserva' id='$nombre_fecha_reserva' size='10' value='$dia/$mes/$ano'/ readonly='readonly' >";
					echo "
<img src='images/calendar.gif' name='boton"."$nombre_fecha_reserva' border='0' id='boton"."$nombre_fecha_reserva' style='cursor: pointer; border: 0px solid blue;' title='Seleccione fecha' />
<script type='text/javascript'>
	Calendar.setup({
		inputField	:	'$nombre_fecha_reserva',		// id of the input field
		trigger		:	'boton"."$nombre_fecha_reserva',	// trigger for the calendar (button ID)
		onSelect	:	function() { this.hide() },
		min		:	".date("Ymd")."
	});
</script>";
					echo "</td>
		<td><input type=\"text\" name=\"".$nombre_comprometido."\" size=\"4\" value=\"".$valor_existente_comprometido."\" onkeyup='validar(".$nombre_comprometido.");'></td>
	</tr>";
					$cont ++;
					// fin articulo que no esta en presupuesto
				}
			} // fin del while
			echo "
	<tr>
		<td colspan='".count($nombres_bloque_1)."' align=\"center\">* Formato 1234.56</td>
	</tr>
	<tr>
		<td colspan='".count($nombres_bloque_1)."' align=\"center\">
		<input class='buttonmario smallmario green' type=submit name='Actualizar' value='$texto_boton_bloque_1'></td>
		<input type=\"hidden\" name=\"cont\" value=\"$cont\">
	</tr>
	</form>";
			echo "</table>";
		} // fin if buscar_nombre
	} // fin if estado

}
// FIN A�ADIR ARTICULO NUEVO

} // fin de se tiene permiso para este script
else
{
	echo "No se tiene permiso para ver esta seccion.<br><br>";
}

echo "
		</td>
	</tr>
</table>
";
?>