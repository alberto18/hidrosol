<?php 
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
?>

<?php

   //include("obtener_miperfil.php");
   
   //include("obtener_opciones.php");

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "GESTION DE SOLICITUDES";
// Titulo que aparece en la pestaña del navegador
$titulo_pagina = "GESTION DE SOLICITUDES";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo '
        <section id="content">
          <section class="vbox">

            <header class="header bg-white b-b b-light">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="modules.php?mod=gestproject&file=index"><i class="fa fa-home"></i> Inicio</a></li>
                <li class="active">Solicitudes</li>
              </ul>
            </header>

            <section class="scrollable wrapper w-f">
              <p class="h4">'.$titulo.'</p>
';
			  
			  
// Texto que aparece en el boton de crear
$titulo_boton_crear = "CREAR NUEVA SOLICITUD";
// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.
// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 
$clase_boton_crear = " class='btn btn-success' ";
$clase_boton_buscar = " class='btn btn-mini btn-black' ";
$clase_boton_guardar = " class='btn btn-success' ";
$clase_boton_volver  = " class='btn btn-navi' ";
$clase_boton_confirmar_borrado  = " class='btn btn-danger' ";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 0; // Creamos las solicitudes de otra forma
// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// Nombre del script
$script = "index_missolicitudes_new";
// Nombre de la tabla
$tabla = "solicitud_t"; // OJO, la clave principal se debe llamar id

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]

// VISITA MEDICA
if ($_REQUEST[tipo_permiso] == 1) {

	$campos_col1 = array('fecha_ini','fecha_fin','observaciones');
	$ayudas_col1 = array();
	$campos_col1_obligatorios = array('on','on','');
	$campos_col1_mascaras = array('','','','','','');
	$campos_col1_readonly = array('','','','','','');
	$tipos_col1  = array('datetime3','datetime3','textarea;300;100');
	$campos_automaticos_para_insert = " tipo_permiso=$_REQUEST[tipo_permiso], fecha_solicitud=now(), user_id='$user_id', estado_departamento_id=3, estado_concejal_id=3, ";
	
	$plantilla_insercion = "index_missolicitudes_visita_medica_new.plantilla.php";

} else if ($_REQUEST[tipo_permiso] == 1) {

	$campos_col1 = array('fecha_ini','fecha_fin','ano_permiso');
	$ayudas_col1 = array();
	$campos_col1_obligatorios = array('on','on','on','on');
	$campos_col1_mascaras = array('','','','','','');
	$campos_col1_readonly = array('','','','','','');
	$tipos_col1  = array('datetime3','datetime3','select;maestro_anos_t;nombre;id;nombre');
	$campos_automaticos_para_insert = " tipo_permiso_id=1, fecha_solicitud=now(), user_id='$user_id', estado_departamento_id=3, estado_concejal_id=3, ";
	
}


// Campo para la busqueda
$campo_busqueda = "fecha_ini desc";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript

if ($plantilla_insercion != "") {
  $fichero_absoluto =  "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}


// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 1;
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','tipo_permiso','fecha_solicitud','fecha_ini','fecha_fin','estado_departamento_id','estado_concejal_id','estado_rrhh_id');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','Tipo permiso','Fecha solicitud','Fecha inicial','Fecha fin','Estado Departamento','Estado Concejal','Estado RRHH');
// Decodificacion si existiese de los campos
$campos_listado_decod = array ('','si;maestro_tipo_permisos_t;nombre;id;nombre','si;datetime','si;datetime','si;datetime','si;maestro_estados_solicitud_t;nombre;id;nombre','si;maestro_estados_solicitud_t;nombre;id;nombre','si;maestro_estados_solicitud_t;nombre;id;nombre');
// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.

$filtro_noc_para_listado = " and user_id='$user_id'";
// Para el paginado
$registros_por_pagina = "30";


//$filtros_iniciales = " and ($tabla.empresa_servicio=1 or $tabla.empresa_servicio=2 or $tabla.empresa_servicio=6) and ($tabla.via_cobro=1 or $tabla.via_cobro=6) and user_destino_id='$user_id'";


//$consulta_inicial =  "select $string_para_select from $tabla left join recibos_gestiones_t on $tabla.id=recibos_gestiones_t.recibo_id where recibos_gestiones_t.user_destino_id='$user_id' and $tabla.id>0 $filtro_noc_para_listado $filtro_buscar $filtro_padre $filtros_iniciales";
$visualizar_num_registros = 1;

// 2. MODO PERSONALIZADO
// Nombre de la funcion que se ejecutara para cada registro. Si se deja blanco el modo personalizado NO ESTA ACTIVO

$nombre_funcion_acciones_por_registro = "funcion_acciones_registro";
function funcion_acciones_registro($valor_id)
{

	$id_encript = base64_encode(base64_encode($valor_id));

	/*
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_comerciales_kms_new&padre_id='.$id_encript.'&pag=0">KMS</a>';
		
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_areas_new&padre_id='.$id_encript.'&pag=0">AREAS</a>';
                
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_epi_new&padre_id='.$id_encript.'&pag=0">MATERIALES</a>';

		
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_vacaciones_new&padre_id='.$id_encript.'&pag=0">VACACIONES</a>';
	*/
	
	$salida_array = obtener_multiples_campos(array('estado_departamento_id','estado_concejal_id','estado_rrhh_id','responsable_razon_denegacion','concejal_razon_denegacion','rrhh_razon_denegacion','tipo_permiso'),'solicitud_t','','id='.$valor_id,'','','');
	$estado_departamento_id = $salida_array[0][estado_departamento_id];
	$estado_concejal_id = $salida_array[0][estado_concejal_id];
	$estado_rrhh_id = $salida_array[0][estado_rrhh_id];
	$tipo_permiso = $salida_array[0][tipo_permiso];
        
	$responsable_razon_denegacion = $salida_array[0][responsable_razon_denegacion];
	$concejal_razon_denegacion = $salida_array[0][concejal_razon_denegacion];
	$rrhh_razon_denegacion = $salida_array[0][rrhh_razon_denegacion];
		
	// Si ya esta aceptada por el responsable o por el concejal, no la podemos anular
	
	if ($estado_departamento_id==3 && $estado_concejal_id==3 && $estado_rrhh_id==3) {	
		echo '<a class="buttonmario smallmario red" onclick="return confirm(\'Esta usted seguro de anular el permiso?\')" href="modules.php?mod=gestproject&file=index_missolicitudes_new&accion=accionanular&id='.$id_encript.'&pag=0">ANULAR</a>';
	} else {
		echo '<a class="buttonmario smallmario red" onclick="return confirm(\'Esta seguro de solicitar la anulaci&oacute;n de est&aacute; solicitud?\')" href="modules.php?mod=gestproject&file=index_missolicitudes_new&accion=accionsolicitaranular&id='.$id_encript.'&pag=0">SOLICITAR ANULACION</a>';
	}
	
	if ($responsable_razon_denegacion != "") { echo "<b>Denegada por el responsable. Motivo: $responsable_razon_denegacion</b>"; }
	if ($concejal_razon_denegacion != "") { echo "<b>Denegada por el concejal. Motivo: $concejal_razon_denegacion</b>"; }
	if ($rrhh_razon_denegacion != "") { echo "<b>Denegada por RRHH. Motivo: $rrhh_razon_denegacion</b>"; }
	
	// Vemos si es necesario subir el justificante
	$obligatorio_justificante = obtener_campo('obligatorio_justificante','maestro_tipo_permisos_t','','id='.$tipo_permiso);
	
	//echo "$obligatorio_justificante";
	if ($obligatorio_justificante == "on") {
		
		// Asistencia consulta medica
		if ($tipo_permiso == "4") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_lactancia_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "5") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_cambiodomicilio_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "6") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_matrimonio_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "37") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_asistenciamedica_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "11") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_asistenciacursos_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "7") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_examenes_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "24") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_deberinexcusable_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "32") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_permisoparto_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		}  else if ($tipo_permiso == "33") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_adopcion_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		}  else if ($tipo_permiso == "34") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_permisopaternidad_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		}  else if ($tipo_permiso == "35") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_permisoviolenciagenero_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		}  else if ($tipo_permiso == "36") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_permisocuidadanosmenor_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		}  else if ($tipo_permiso == "38") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_flexibilidadhoraria_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "39") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_ausenciaenfermedad_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "40") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_reduccionjornadacuidadofamiliar_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "41") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_reduccionjornadacuidadomenor_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "42") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_lactanciaacumulacionjornadas_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "43") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_examenesprenatales_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "44") {
			// ESTE PERMISO SE HA ELIMINADO
			//echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_compensacion2431policia_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		} else if ($tipo_permiso == "45") {
			echo ' <a class="buttonmario smallmario green" href="modules.php?mod=gestproject&file=index_missolicitudes_compensacion2431_new&accion=formmodificar&id='.$id_encript.'">JUSTIFICANTE</a>';
		}
		
		
	}
	
	/*
    echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_documentacion_new&padre_id='.$id_encript.'&pag=0">DOCUMENTOS</a>';

	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_new&accion=formmodificar&id='.$id_encript.'"><i class="fugue-pencil" title="editar"></i> VER FICHA</a>';
	*/
	
	//echo '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
}


/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear
/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";
$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";
*/

// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
// $campo_padre = "";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
// $consulta_nombre_padre = " select nombre as nombre from productos_t where id=#PADREID#";

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 1;
$buscadores = array();
//$buscadores[] = "select;matricula;vehiculos_t;matricula;id;buscar por matricula";
$buscadores[] = "intervalo_fechas;fecha_ini;;;;Filtrar por fecha inicio ";
$buscadores[] = "select;tipo_permiso;maestro_tipo_permisos_t;nombre;id;buscar por tipo de permiso";
$buscadores[] = "select;estado_concejal_id;maestro_estados_solicitud_t;nombre;id;buscar por estado";
$buscadores[] = "checkbox;anulada;;;;Anulada";


//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";

?>

<div class="btn-group">
  <button class="btn btn-success dropdown-toggle" data-toggle="dropdown">CREAR NUEVA SOLICITUD <span class="caret"></span></button>
  <ul class="dropdown-menu">
  <?php
    $consulta = "select id, nombre from maestro_tipo_permisos_t order by nombre asc";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("$consulta, La consulta fall&oacute;: " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC)) {
	
	   if ($linea[id] == 1) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_visitamedica_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 3) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_funcionessindicales_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 4) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_lactancia_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   	   
	   if ($linea[id] == 5) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_cambiodomicilio_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 6) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_matrimonio_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   	   
	   if ($linea[id] == 7) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_examenes_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 8) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_vacaciones_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 9) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_asuntospropios_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 11) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_asistenciacursos_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 12) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_enfermedadfamiliar_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 13) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_fallecimientofamiliar_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }

	   if ($linea[id] == 26) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_fallecimientofamiliar_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	   
	   
	   if ($linea[id] == 14) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_hijos_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	   
	   
	   if ($linea[id] == 15) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_compensacion_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	  
	   
	   if ($linea[id] == 18) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_visitamedicafamiliar_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	   
	   
	   if ($linea[id] == 19) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_recuperacionhorasporcursos_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	   
	   
	   if ($linea[id] == 20) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_compensacionhorasextra_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	  
	   
	   if ($linea[id] == 21) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_reconocimientohorasextra_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	 
	   
	   if ($linea[id] == 22) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_miembrotribunaloposicion_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	
	   
	   if ($linea[id] == 23) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_cesionhorassindicales_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	  
	   
	   if ($linea[id] == 24) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_deberinexcusable_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	   
	   
	   if ($linea[id] == 25) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_recuperacionhorasporjuicio_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	
	   
	   if ($linea[id] == 27) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_nacimientoprematuro_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	

	   if ($linea[id] == 32) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_permisoparto_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 33) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_adopcion_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 34) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_permisopaternidad_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 35) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_permisoviolenciagenero_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 36) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_permisocuidadosmenor_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	  if ($linea[id] == 37) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_asistenciamedica_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	
	   
	   if ($linea[id] == 38) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_flexibilidadhoraria_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 39) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_ausenciaenfermedad_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	

	   if ($linea[id] == 40) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_reduccionjornadacuidadofamiliar_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	
	   
	   if ($linea[id] == 41) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_reduccionjornadacuidadomenor_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	
	   
	   if ($linea[id] == 42) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_lactanciaacumulacionjornadas_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	
	   
	   if ($linea[id] == 43) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_examenesprenatales_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	
	   
	   if ($linea[id] == 44) {
			// Este permiso se ha eliminado
			//echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_compensacion2431policia_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	
	   
	   if ($linea[id] == 45) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_compensacion2431_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   if ($linea[id] == 46) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_fallecimientofamiliar_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	
	   
	   if ($linea[id] == 49) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_enfermedadfamiliar_2grado_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	
	   
	   if ($linea[id] == 48) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_enfermedadfamiliar_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	
	   
	   if ($linea[id] == 47) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_fallecimientofamiliar_2grado_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }
	   
	   
	   if ($linea[id] == 28 || $linea[id] == 28 || $linea[id] == 29 || $linea[id] == 30 || $linea[id] == 31) {
			echo '<li><a href="modules.php?mod=gestproject&file=index_missolicitudes_recuperacionhorasporjuicio_new&accion=formcrear">'.$linea[nombre].'</a></li>';
	   }	
	}
  ?>
  </ul>
</div>
						
<?php

if ($_REQUEST[accion] == "accionanular") {

 $solicitud_id = base64_decode(base64_decode($_REQUEST[id]));
 
 $cons_update = "update solicitud_t set anulada='on' where id='$solicitud_id'";
 //echo "$cons_update<br>";
 $res = mysql_query($cons_update) or die("La consulta fall&oacute;: $cons_update " . mysql_error());
}

// accionsolicitaranulacion
if ($_REQUEST[accion] == "accionsolicitaranular") {

$solicitud_id = base64_decode(base64_decode($_REQUEST[id]));

$user_solicitante_id = obtener_campo('user_id','solicitud_t','','id='.$solicitud_id);
$tipo_permiso = obtener_campo('tipo_permiso','solicitud_t','','id='.$solicitud_id);
$fecha_ini = obtener_campo('fecha_ini','solicitud_t','','id='.$solicitud_id);

$nombre_solicitante = obtener_campo('nombre','usuarios_t','','id='.$user_solicitante_id);
$nombre_tipo_permiso = obtener_campo('nombre','maestro_tipo_permisos_t','','id='.$tipo_permiso);


$plantilla_correo = '
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td height="0" align="center"><div>
</div>
<table border="0" cellspacing="0" cellpadding="0" width="650">
<tbody>
<tr>
<td valign="top" align="left"><img src="http://casiopea.tac7.es/tacgestorcontenidos_empleado_stabrigida/images/header_envio_email.jpg" width="650" height="153"></td>
</tr>
<tr>
<td bgcolor="#e5e5e5" valign="top" align="left"><table border="0" cellspacing="10" cellpadding="0" width="100%">
<tbody>
<tr>
<td bgcolor="#ffffff" valign="top" width="600" align="center"><table border="0" cellspacing="0" cellpadding="30" width="100%">
<tbody>
<tr>
<td bgcolor="#ffffff" valign="top" align="left"><p>Estimado usuario, se ha solicitado la anulaci&oacute;n una solicitud de permiso con los siguientes datos:<br>
<b>Solicitante:</b> '.$nombre_solicitante.'<br>
<b>Tipo de permiso solicitado:</b> '.$nombre_tipo_permiso.'<br>
<b>Fecha solicitada:</b> '.$fecha_ini.'<br>
</p>
</td>
</tr>
</tbody>
</table>
Gracias.</td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table></td>
</tr>

</tbody>
</table></td>
</tr>
</tbody>
</table>
<p>Antes de imprimir este documento, por favor, compruebe que es verdaderamente necesario.</p>
';

 // Enviamos un email con la noticia a todos los empleados. Existe una direccion de email unica para los mismos.
$correo_from = "personal@santabrigida.es";
$nombre_from = "Portal del Empleado Santa Brigida";
$array_direcciones = array();
if ($_REQUEST[user_id] != "") {
	$email_destino = obtener_campo('email','usuarios_t','','id='.$_REQUEST[user_id]); 
	if ($email_destino != "") {
			$array_direcciones[] = $email_destino;
	}
}

if ($_REQUEST[departamento_id] != "") {
	$cons = "select email from usuarios_t where departamento_id = '$_REQUEST[departamento_id]'";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {
		if ($lin['email'] != "") {
			$array_direcciones[] = $lin['email'];
		}
	}
}


$array_direcciones[] = "mario@tac7.com";
//$array_direcciones[] = "enrique@tac7.com";
$array_direcciones[] = "alberto@tac7.com";
$array_direcciones[] = "personal@santabrigida.es";



$correo_replay = $correo_from;
$nombre_replay = $nombre_from;

$asunto = "[PORTALEMPLEADO] Anulacion solicitud de permiso";
$mensaje = $plantilla_correo;

$array_archivos = array();

//print_r($array_direcciones);

$resultado_email = EnviarEmail($correo_from, $nombre_from, $array_direcciones, $correo_replay, $nombre_replay, $asunto, $mensaje, $array_archivos);


echo " $resultado_email";


}

// fin accionsolicitaranulacion






// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

$departamento_id = obtener_campo('departamento_id','usuarios_t','','id='.$user_id);

echo "<a  class='btn btn-success' target=_blank href=modules.php?mod=gestproject&file=index_cuadrante_new&buscar_cliente_1=$departamento_id>Cuadrante de permisos de mi departamento</a>";

?>


            </section>

