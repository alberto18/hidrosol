  <div class="row">
  	<div class="col-sm-12">
	  <section class="panel panel-default">
		<header class="panel-heading font-bold">Creaci&oacute;n de una solicitud: Enfermedad de familiar primer grado</header>
		
			<table border=1   class='table table-bordered table-condensed table-hover' align=center>
			<tr><td style='background-color: #e1e4ea !important;'><b>Fecha inicial</b></td><td>[fecha_ini]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Fecha final</b></td><td>[fecha_fin]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Grado de parentezco</b></td><td>[grado_parentezco_id]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Informe m&eacute;dico familiar</b></td><td>[nombre_fichero]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Observaciones y descripci&oacute;n del motivo</b></td><td>[observaciones]</td></tr>
			</table>

	  </section>
	</div>
   </div>