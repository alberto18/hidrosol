<?php 
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";

// CONFIGURACION
$titulo = "HISTORICO DE VISITAS DEL CLIENTE ";
$titulo_pagina = "HISTORICO VISITAS CLIENTE";
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_historico_visitas_clientes_new";
$script_clientes = "index_clientes_new";
$tabla = "agenda_t";
$tabla_detalles = "detalle_agenda_t";
$tabla_padre = "clientes_t";
$registros_por_pagina = 10;
$script_descarga = "";
$tamano_max_archivo = "16000000";
if ($color_entorno != "") { $color_fondo = $color_entorno; $color_fondo_claro = $color_entorno_claro; }
else { $color_fondo = "97c00e"; $color_fondo_claro = "dcfb73"; }

$texto_textarea = '<br type="_moz" />';

echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo "
<table width='100%' height='700' border='0' cellpadding='0' cellspacing='0' class='text' align='center'>
	<tr valign='top'>
		<td width=100% align=left>
";

if (PermisosSecciones($user_id, $script, array()) == 1)
{

// textos de la pagina
$texto_crear = "Crear grupo";
$texto_listado_general = "Listar todas las visitas";
$texto_creado = "Grupo creado";
$texto_modificado = "Grupo modificado";
$texto_borrado = "Grupo borrado";
$nombre_objeto = " UN GRUPO";

// Campos con los que se trabajara en el insert y modify
$campos_col1 = array();
$campos_col2 = array();

/*
motivo_cancelacion
estado_tarea_id
tarea
bloqueada
motivo_bloqueada
observaciones
*/
/*
proveedor_id
estado_tarea_id
tarea
observaciones
*/

// Nombres que apareceran en las columnas de los formularios
$nombres_col1 = array();
$nombres_col2 = array();

// Tipos. Cada campo puede ser:
// text;readonly;size (60)
// password;readonly;size
// textarea;readonly;row;col (10;60)
// select;readonly;tabla;campo_mostrar;campo_para_value;campo_condicion
// checkbox;readonly
// date;readonly
// hidden;tabla;campo_mostrar;campo_para_value;campo_para_order;nuevo_nombre;campo_depende;campo_filtro mostrara un select que saltara y ademas tendra otro campo oculto con el valor
// dni;readonly
// telefono;readonly
// cuenta;readonly
// email;readonly;size
// float;readonly;size
// multiple;readonly;tabla;campo_mostrar;campo_para_value;campo_para_order;nombre_checkbox
// file poner el campo "nombre_fichero", actualiza nombre_fichero, tipo_fichero, peso_fichero, fichero_binario, fecha_subida, user_id
//   Los campos en la tabla a a�adir serian:
//  | nombre_fichero              | varchar(200)
//  | tipo_fichero                | varchar(20)
//  | peso_fichero                | varchar(20)
//  | fecha_subida                | datetime
//  | user_id                     | int(11)
//  | fichero_binario             | blob
// ss;readonly
// calendar;readonly
// time;readonly
// numerico;readonly;size
// fileCarp poner el campo "nombre_fichero", actualiza solo nombre_fichero y sube a la carpeta definida
$tipos_col1  = array();
$tipos_col2  = array();

// Separadores o titulos
$titulos_col1 = array('');
$titulos_col2 = array('');

// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('fecha','hora_inicio','hora_fin','comercial_visita_id','proveedor_id','estado_agenda_id');
// indica la tabla de donde es el campo
$tablas_campos_listado = array($tabla,$tabla,$tabla,$tabla,$tabla,$tabla);

$nombres_listado = array('Fecha','Hora inicio','Hora fin','Comercial','Proveedor','Estado');

$campos_necesarios_listado = array('id','tipo_agenda_id');
$tablas_campos_necesarios = array($tabla,$tabla);

// Si es necesaria una decodificacion en el listado
// '' no es necesaria
// si;tabla;campo_mostrar;campo_buscar
// si;date
// si;datetime
// si;checkbox
// si;time
$campos_listado_decod = array ('si;date','si;time','si;time','si;usuarios_t;nombre;id','si;proveedores_t;nombre;id','si;maestro_estados_agenda_t;nombre;id');

// Campo para la busqueda
$campo_busqueda = "fecha";

// Campo padre
$usa_padre = 1;
$campopadre = "cliente_id";

// Variables del script
$parametros_nombres = array("accion","pag",$campopadre,"b_prov",
"fecha1","fecha2");
$parametros_formulario = array("pag",$campopadre,"b_prov",
"fecha1","fecha2");
$parametros_filtro = array("ano1","mes1","dia1","ano2","mes2","dia2","b_prov"); // parametros que estan en el filtro
// Tipos de parametros
//									Sin nada es que no sirven para los filtros o filtro distinto
// texto;campo_filtro							Es un filtro para texto
// texto								Es un filtro para texto, pero hay que desarrollar el filtrado
// select;campo_filtro							Es un filtro con select
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;desde		Es un filtro para fechas superiores a la introducida
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;hasta		Es un filtro para fechas inferiores a la introducida
$parametros_tipos = array("","","","texto",
"fecha;fecha;ano1;mes1;dia1;desde","fecha;fecha;ano2;mes2;dia2;hasta");

foreach($parametros_nombres as $indice_parametros => $nombre_param)
{
	list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
	if ($ele == "texto")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; $$nombre_param = str_replace('+',' ',$$nombre_param); }
		else { $$nombre_param = ""; }
	}
	if ($ele == "select")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
	if ($ele == "fecha")
	{
		if ($_REQUEST[$ele3] != "") { $$ele3 = $_REQUEST[$ele3]; }
		else { $$ele3 = ""; }
		if ($_REQUEST[$ele4] != "") { $$ele4 = $_REQUEST[$ele4]; }
		else { $$ele4 = ""; }
		if ($_REQUEST[$ele5] != "") { $$ele5 = $_REQUEST[$ele5]; }
		else { $$ele5 = ""; }
	}
	if ($ele == "")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
}
if ($pag == "") { $pag = "0"; }

// COMIENZA EL SCRIPT

if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$nombre_padre = "";
		$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
		//echo "$consulta_padre";
		$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
		while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
		{
			$nombre_padre = "<span style='color:#$color_fondo;'>".$linea_padre['nombre_corto']."</span>";
		}
	}
        
	echo "
<center><b>$titulo $nombre_padre</b><br>";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != $campopadre && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "[<a href='$enlacevolver"."$script_clientes&pag=0'>Volver a clientes</a>]";
	OpcionesClientes($$campopadre);
	//echo "[<a href='$enlacevolver"."$script&accion=formcrear&pag=$pag'>$texto_crear</a>]";
	echo "<br><b>Buscar por</b>
	<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<form name=form_buscar1 method=post action='$enlacevolver"."$script'>
	<input type=hidden name=pag value=0>";
	echo "<img src=images/p.jpg onload=document.form_buscar1.dia1.focus();>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	echo "
		<tr style='text-align:center;'>
			<td><b>Fecha desde</b>: D: <input type='text' name='dia1' size='2' maxlength='2' value='$dia1' onkeyup='validar(dia1);'> 
M: <input type='text' name='mes1' size='2' maxlength='2' value='$mes1' onkeyup='validar(mes1);'> 
A: <input type='text' name='ano1' size='4' maxlength='4' value='$ano1' onkeyup='validar(ano1);'></td>
			<td><b>Fecha hasta</b>: D: <input type='text' name='dia2' size='2' maxlength='2' value='$dia2' onkeyup='validar(dia2);'> 
M: <input type='text' name='mes2' size='2' maxlength='2' value='$mes2' onkeyup='validar(mes2);'> 
A: <input type='text' name='ano2' size='4' maxlength='4' value='$ano2' onkeyup='validar(ano2);'></td>
		</tr>
		<tr style='text-align:center;'>
			<td><b>Proveedor</b>: <input type=text name='b_prov' value='".$b_prov."'></td>
			<td><input type=submit value='Filtrar'></td>
		</tr>
	</form>
	</table>";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "
	<a href='$enlacevolver"."$script&pag=0$parametros'>$texto_listado_general</a>
	<!--
	-->
</center>";
}

// OBTENEMOS EL LISTADO DE REGISTROS 
if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$condiciones = " ($tabla.$campopadre='".$$campopadre."' or $tabla.cliente_visitado_id='".$$campopadre."') ";
		$parametros = "&$campopadre=".$$campopadre;
        }
	else
	{
		$condiciones = "";
		$parametros = "";
	}
	//if ($condiciones != "") { $condiciones .= " and "; }
	//$condiciones .= " $tabla.tipo_agenda_id='1' ";
	$join = "";
	$group = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
		if ($ele == "texto" && $ele2 != "")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2 like '%".$$nombre_param."%' ";
				$$nombre_param = str_replace(' ','+',$$nombre_param); 
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "select")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2='".$$nombre_param."' ";
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "fecha")
		{
			if ($$ele3 != "" && $$ele4 != "" && $$ele5 != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				($ele6 == "desde" ? $condiciones .= " $tabla.$ele2>='".$$ele3."-".$$ele4."-".$$ele5." 00:00:00' " : $condiciones .= " $tabla.$ele2<='".$$ele3."-".$$ele4."-".$$ele5." 23:59:59' ");
				$parametros .= "&$ele3=".$$ele3."&$ele4=".$$ele4."&$ele5=".$$ele5;
			}
		}
	}
	if ($b_prov != "")
	{
		//desgloso b_prov si tiene espacios en blanco
		$trozos = array();
		$trozos = explode(" ",$b_prov);
		foreach ($trozos as $valor)
		{
			if ($condiciones != "") { $condiciones .= " and "; }
			$condiciones .= "(proveedores_t.nombre like '%$valor%' or proveedores_t.nombre_corto like '%$valor%')";
		}
		$parametros .= "&b_prov=".$b_prov;
		$join = " join $tabla_detalles on $tabla_detalles.agenda_id=$tabla.id join proveedores_t on proveedores_t.id=$tabla_detalles.proveedor_id ";
		$group = " group by $tabla.id ";
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && $nombre_param != $campopadre && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	$string_para_select = "";
	foreach($campos_necesarios_listado as $indice => $campo)
	{
		$string_para_select .= $tablas_campos_necesarios[$indice].".$campo,";
	}
	echo "<table width=100%>
	<tr bgcolor='#$color_fondo'>";
	$columnas = 0;
	foreach ($campos_listado as $campo) {
		echo "<td><a class='listadoTabla' href='$enlacevolver"."$script&orderby=$campo&pag=$pag$parametros'>$nombres_listado[$columnas]</a></td>";
		$string_para_select .= $tablas_campos_listado[$columnas].".$campo,";
		$columnas++;
	}
	$columnas += 1;
	// Eliminamos el ultimo caracter
	$string_para_select = substr($string_para_select,0,-1);
	echo "<td><font color='#ffffff'><b>Acciones</b></font></td>";
	echo "</tr>";

	$consulta  = "select $string_para_select from $tabla $join";
	if ($condiciones != "") { $consulta .= " where $condiciones "; }
	// Vemos si existe un orderby
	if ($_REQUEST[orderby] != "")
	{
		$order = " order by $tabla.$_REQUEST[orderby]";
		$parametros .= "&orderby=".$_REQUEST[orderby];
	}
	else { $order = " order by $tabla.$campo_busqueda desc";  }
	if ($pag != "") {
		// la primera pagina es la 0
		$inicio = $pag*$registros_por_pagina;
		$limit = " limit $inicio,$registros_por_pagina";
	}
	else { $limit = ""; }
	$consulta .= " $group $order $limit;";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: $consulta " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		if ($_GET[id] != "" && $linea[id] == $_GET[id]) { $bgcolor = " bgcolor='#$color_fondo_claro'"; }
		else { $bgcolor = ""; }
		
		echo "<tr $bgcolor>";
		foreach ($campos_listado as $cuenta_campos => $campo)
		{
			$nombre = "";
			list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod[$cuenta_campos]);
			if ($ele != 'si')
			{
				$nombre = "$linea[$campo]";
			}
			elseif ($ele2 == "date")
			{
				if ($linea[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
				else { $nombre = date("d-m-Y",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "datetime")
			{
				if ($linea[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
				else { $nombre = date("d-m-Y H:i",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "checkbox")
			{
				($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
			}
			elseif ($ele2 == "time")
			{
				list($temp1, $temp2, $temp3) = explode(':',$linea[$campo]);
				$nombre = $temp1.":".$temp2;
			}
			elseif ($linea[$campo] != "")
			{
				$consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre = $lineaselect[$ele3];
				}
			}
			echo "<td>$nombre</td>";
		}
		echo "<td>";
		echo " [<a href='$enlacevolver"."$script&accion=verdatos&id=$linea[id]&pag=$pag$parametros'>Datos</a>] ";
		if ($linea['tipo_agenda_id'] == 1)
		{
			$num_detalles = 0;
			$cons = "select count(id) as total from detalle_agenda_t where agenda_id='".$linea[id]."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$num_detalles = $lin['total'];
			}
			echo " [<a href='$enlacevolver"."$script&accion=verdetalle&id=$linea[id]&pag=$pag$parametros'>Detalles ($num_detalles)</a>] ";
		}
		echo "
		</td>";
		echo "</tr>";
		echo "<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas'></td></tr>";
	}
	echo "</table>";
	if ($pag != "")
	{
		$pag_visual = $pag+1;
		// Visualizamos las paginas existentes
		$consulta2  = "select count(distinct $tabla.id) as num from $tabla $join";
		if ($condiciones != "") { $consulta2 .= " where $condiciones;"; } else { $consulta2 .= ";"; }
		$resultado2 = mysql_query($consulta2) or die("$consulta2, La consulta fall&oacute;: " . mysql_error());
		//echo "$consulta2<br>";
		while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC)) {
			$exp = "$linea2[num]";
		}
		if ($exp > $registros_por_pagina)
		{
			echo PaginadoListado($pag, $enlacevolver.$script, $parametros, $exp, $registros_por_pagina);
		}
	}
	echo "<hr>";
}
// FIN DE OBTENER EL LISTADO INICIAL

if ($accion == "verdatos")
{
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "") { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "") { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "") { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { $parametros .= "&orderby=".$_REQUEST[orderby]; }
	echo "<table width='100%'>
	<tr>
		<td><center><b>DATOS DE LA VISITA</b> [<a href='$enlacevolver"."$script$parametros'>Volver sin cambios</a>]</center></td>
	</tr>
</table>
<table>";
	$campos_col1 = array();
	$nombres_col1 = array();
	$tipos_col1  = array();
	$titulos_col1 = array();
	$arraymod = array();
	// Obtenemos los valores actuales del registro que se esta modificando
	$consultamod = "select * from $tabla where id=$_GET[id];";
	$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: " . mysql_error());
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC))
	{
		if ($lineasmod['tipo_agenda_id'] == 1)
		{
			$campos_col1 = array('estado_agenda_id','observaciones','motivo_cancelacion','comercial_visita_id','motivo_bloqueada');
			$nombres_col1 = array('Estado','Observaciones','Motivo cancelacion','Comercial','Motivo bloqueada');
			$tipos_col1  = array('select;0;maestro_estados_agenda_t;nombre;id;nombre','textarea;0;10;60','textarea;0;10;60','select;0;usuarios_t;nombre;id;nombre','textarea;0;10;60');
			$titulos_col1 = array('','','','');
		}
		elseif ($lineasmod['tipo_agenda_id'] == 2)
		{
			$campos_col1 = array('estado_agenda_id','observaciones','comercial_visita_id');
			$nombres_col1 = array('Estado','Observaciones','Comercial');
			$tipos_col1  = array('select;0;maestro_estados_agenda_t;nombre;id;nombre','textarea;0;10;60','select;0;usuarios_t;nombre;id;nombre');
			$titulos_col1 = array('','','','');
		}
		foreach ($campos_col1 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
	} // del while
	foreach ($campos_col1 as $cuenta_campos => $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col1[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col1[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col1[$cuenta_campos]);
		$nombre_campo = $nombres_col1[$cuenta_campos];
		if ($ele == "checkbox") { echo "<tr><td><b>$nombre_campo</b></td><td>";if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; } echo "</td></tr>"; }
		elseif ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele3 where $ele5='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele4]";
				}
			}
			echo "</td></tr>";
		} // del tipo select
		else { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		echo "
";
	}
	echo "
	</table>";
}

if ($accion == "verdetalle")
{
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "") { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "") { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "") { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { $parametros .= "&orderby=".$_REQUEST[orderby]; }
	echo "<table width='100%'>
	<tr>
		<td><center><b>DETALLES DE LA VISITA</b> [<a href='$enlacevolver"."$script$parametros'>Volver sin cambios</a>]</center></td>
	</tr>
</table>
<table>";
	$campos_col1 = array('proveedor_id','fecha_fin','estado_tarea_id','tarea','observaciones');
	$nombres_col1 = array('Proveedor','Fecha fin','Estado tarea','Tarea','Observaciones');
	$tipos_col1  = array('select;0;proveedores_t;nombre;id;nombre','calendar;0','select;0;maestro_estados_tareas_t;nombre;id;nombre','textarea;0;10;60','textarea;0;10;60');
	$titulos_col1 = array('','','','','');
	
	$condiciones = "$tabla_detalles.agenda_id='".$_GET['id']."'";
	$join = "";
	$group = "";
	if ($b_prov != "")
	{
		if ($condiciones != "") { $condiciones .= " and "; }
		$condiciones .= "(proveedores_t.nombre like '%".$b_prov."%' or proveedores_t.nombre_corto like '%".$b_prov."%')";
		$join = " join proveedores_t on proveedores_t.id=$tabla_detalles.proveedor_id ";
		$group = " group by $tabla_detalles.id ";
	}
	// Obtenemos los valores actuales del registro que se esta modificando
	$consultamod = "select $tabla_detalles.* from $tabla_detalles $join ";
	if ($condiciones != "") { $consultamod .= " where $condiciones "; }
	$consultamod .= " $group order by $tabla_detalles.id;";
	//echo "$consultamod<br>";
	$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: " . mysql_error());
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC))
	{
		$arraymod = array();
		foreach ($campos_col1 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
		
		// informacion del tipo de detalle
		echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>";
		if ($arraymod['proveedor_id'] == 0) { echo "Tarea cliente"; }
		elseif ($arraymod['proveedor_id'] > 0 && $arraymod['estado_tarea_id'] > 0) { echo "Tarea proveedor"; }
		else { echo "Observacion proveedor"; }
		echo "</b></font><hr></td></tr>";
		
		foreach ($campos_col1 as $cuenta_campos => $campo)
		{
			//Vemos si existe un titulo
			if (
$campo == "observaciones" ||
(in_array($campo,array('estado_tarea_id','tarea','fecha_fin')) && (($arraymod['proveedor_id'] == 0) || ($arraymod['proveedor_id'] > 0 && $arraymod['estado_tarea_id'] > 0))) || 
($campo == "proveedor_id" && $arraymod['proveedor_id'] > 0)
			)
			{
				// si el campo es observaciones
				// si es el campo estado_tarea_id, tarea o fecha_fin
				//	si proveedor_id=0 => se muestra (tarea cliente)
				//	si proveedor_id>0 y estado_tarea_id>0 => se muestra (tarea proveedor)
				// si el campo es proveedor_id y es >0 => se muestra (no es tarea cliente)
				if ($titulos_col1[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col1[$cuenta_campos]</b></font><hr></td></tr>"; }
				list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col1[$cuenta_campos]);
				$nombre_campo = $nombres_col1[$cuenta_campos];
				if ($ele == "checkbox") { echo "<tr><td><b>$nombre_campo</b></td><td>";if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; } echo "</td></tr>"; }
				elseif ($ele == "select")
				{
					echo "<tr><td><b>$nombre_campo</b></td><td>";
					if ($arraymod[$campo] > 0)
					{
						$consultaselect = "select * from $ele3 where $ele5='$arraymod[$campo]';";
						$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
						while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
							echo "$lineaselect[$ele4]";
						}
					}
					echo "</td></tr>";
				} // del tipo select
				elseif ($ele == "calendar")
				{
					list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
					echo "<tr><td><b>$nombre_campo</b></td><td>$dia/$mes/$ano</td></tr>";
				}
				else { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
				echo "
";
			}
		}
		echo "<tr><td colspan='2'><hr></td></tr>";
	} // del while
	echo "
	</table>";
}

} // fin de se tiene permiso para este script
else
{
	echo "No se tiene permiso para ver esta seccion.<br><br>";
}

echo "
		</td>
	</tr>
</table>
";
?>