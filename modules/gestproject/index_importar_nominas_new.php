<?php
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";

$titulo = "Importar n&oacute;minas de los empleados";
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_importar_nominas_new";


echo '
        <section id="content">
          <section class="vbox">

            <header class="header bg-white b-b b-light">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="modules.php?mod=gestproject&file=index"><i class="fa fa-home"></i> Inicio</a></li>
                <li class="active">Importar n&oacute;minas de los empleados</li>
              </ul>
            </header>

            <section class="scrollable wrapper w-f">
              <p class="h4">'.$titulo.'</p>
';



echo "<br>
<p><b>PASOS PARA UN CORRECTO FUNCIONAMIENTO DE LA IMPORTACI&Oacute;N DE N&Oacute;MINAS DE LOS EMPLEADOS</b></p>

<ol>
<li>Trocear el fichero donde se encuentran todas las n&oacute;minas, el objetivo es tener una n&oacute;mina por cada empleado del Ayuntamiento. <a target=\"_blank\" class=\"smallmario green\" href=\"Guia_cargar_nominas_en_portalempleado.pdf\"><i class=\"fugue-pencil\" title=\"guia\"></i> DESCARGAR GU&Iacute;A</a></li>
<li>Subir todas las n&oacute;minas al servidor a la carpeta correspondiente seg&uacute;n el a&ntilde;o y mes que corresponda.</li>
<li>Ejecutar la importaci&oacute;n seg&uacute;n el a&ntilde;o y mes que corresponda.  </li>
</ol><hr>
<p>Indique el mes y a&ntilde;o de las n&oacute;minas que va a subir al tacportal del empleado.  (Ejemplo: Mes: 3 A&ntilde;o: 2014).</p>

<form method=get action=procesar_nominas_a_db.php>
	
 Mes:   <input name=mes value='$_GET[mes]'> A&ntilde;o: <input name=ano value='$_GET[ano]'> 
		<input type=submit value='IMPORTAR NOMINAS'> (puede tardar 60 segundos aprox,)
</form>


";

?>

            </section>


			