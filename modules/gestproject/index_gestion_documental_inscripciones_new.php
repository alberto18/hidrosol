<?php 
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
?>

<?php

   //include("obtener_miperfil.php");
   
   //include("obtener_opciones.php");

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "GESTI&Oacute;N DOCUMENTAL";
// Titulo que aparece en la pesta�a del navegador
$titulo_pagina = "GESTION DE BOLSA 2015";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo '
        <section id="content">
          <section class="vbox">

            <header class="header bg-white b-b b-light">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="modules.php?mod=gestproject&file=index"><i class="fa fa-home"></i> Inicio</a></li>
                <li class="active">Gesti&oacute;n documental para las inscripciones</li>
              </ul>
            </header>

            <section class="scrollable wrapper w-f">
              <p class="h4">'.$titulo.'</p>
';





// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW

// Es un script publico (que no requiere control de user_id por parte de maestro_formulario.php)

$maestro_formulario_publico = 0;

// Titulo que aparece en la parte superior del script

//$titulo = "GESTION DOCUMENTAL DE LA FACTURA";

// Titulo que aparece en la pesta�a del navegador

$titulo_pagina = "GESTION DOCUMENTAL INSCRIPCIONES BOLSA";

echo "<script>document.title = \"".$titulo_pagina."\";</script>";

// Texto que aparece en el boton de crear

$titulo_boton_crear = "CREAR NUEVO DOCUMENTO";

// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.

// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 

//$clase_boton_crear = " class='btn btn-success' ";
$clase_boton_crear = " class='btn btn-success'";

$clase_boton_buscar = " class='btn btn-mini btn-black' ";

$clase_boton_guardar = " class='btn btn-success' ";

$clase_boton_volver  = " class='btn btn-navi' ";

$clase_boton_confirmar_borrado  = " class='btn btn-danger' ";

// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear

$permitir_creacion_de_registros = 1;

$modulo_script = "gestproject";

// Si no permitimos la creacion de registros o la modificacion, pero vamos a hacer llamadas con accion=formmodificar (por ejemplo para ver algun detalle), quizas nos interese ocultar el boton de GUARDAR

$ocultar_boton_guardar_en_formcrear_formmodificar = 0;

// Direccion en la que se encuentra el script

$enlacevolver = "modules.php?mod=".$modulo_script."&file=";

// Nombre del script

$script = "index_gestion_documental_inscripciones_new";

// Nombre de la tabla

$tabla = "gestion_documental_bolsa2015_t"; // OJO, la clave principal se debe llamar id



// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS

// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]

$campos_col1 = array('nombre_fichero','nombre_fichero2','nombre_fichero3','nombre_fichero4','nombre_fichero5');



// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos

//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');



// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]

$ayudas_col1 = array();



// Definir que campos son onbligatorios (colocando 'on')

$campos_col1_obligatorios = array('on','on','','on','','');



// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 

$campos_col1_mascaras = array('');



// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura

$campos_col1_readonly = array('','','','','','','','','');



// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php

$tipos_col1  = array('file;8388608;descargar_archivo_gestiondocumental_inscripciones;tipo_fichero;peso_fichero;inscripciones;descargar_archivo_gestiondocumental_inscripciones','file;8388608;descargar_archivo_gestiondocumental_inscripciones2;tipo_fichero2;peso_fichero2;inscripciones;descargar_archivo_gestiondocumental_inscripciones2','file;8388608;descargar_archivo_gestiondocumental_inscripciones3;tipo_fichero3;peso_fichero3;inscripciones;descargar_archivo_gestiondocumental_inscripciones3','file;8388608;descargar_archivo_gestiondocumental_inscripciones4;tipo_fichero4;peso_fichero4;inscripciones;descargar_archivo_gestiondocumental_inscripciones4','file;8388608;descargar_archivo_gestiondocumental_inscripciones5;tipo_fichero5;peso_fichero5;inscripciones;descargar_archivo_gestiondocumental_inscripciones5');



// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.

$filtro_noc_para_insert = "";

// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()

// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";

$campos_automaticos_para_insert = "";



// Campo para la busqueda

$campo_busqueda = "id";



// PLANTILLAS VISUALES

// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript

$plantilla_insercion = "index_gestion_documental_inscripciones_new.plantilla.php";

if ($plantilla_insercion != "") {

  $fichero_absoluto =  "modules/gestproject/" . $plantilla_insercion;

  //echo $fichero_absoluto;

  if (file_exists($fichero_absoluto)) {

   $gestor = fopen($fichero_absoluto, "r");

   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));

   fclose($gestor);

  }

}



// CONFIGURACION DEL LISTADO DE REGISTRO

// Si se desea visualizar el listado o no (poner 1 o 0)

$visualizar_listado = 1;

// Campos, por orden, para el listado inicial de registros

$campos_listado = array ('id','nombre_fichero','nombre_fichero2','nombre_fichero3','nombre_fichero4','nombre_fichero5');

// Nombres para el encabezado de la tabla del listado de registros

$nombres_listado = array ('','Aportar DNI','Aportar titulaci&oacute;n','Aportar certificado minusval&iacute;a','Aportar justificante abono','Aportar presentaci&oacute;n f&iacute;sica');

// Decodificacion si existiese de los campos

$campos_listado_decod = array ('','si;file;../documentos/inscripciones;descargar_archivo_gestiondocumental_inscripciones;tipo_fichero;peso_fichero;inscripciones;descargar_archivo_gestiondocumental_inscripciones','si;file;../documentos/inscripciones;descargar_archivo_gestiondocumental_inscripciones2;tipo_fichero2;peso_fichero2;inscripciones;descargar_archivo_gestiondocumental_inscripciones2','si;file;../documentos/inscripciones;descargar_archivo_gestiondocumental_inscripciones3;tipo_fichero3;peso_fichero3;inscripciones;descargar_archivo_gestiondocumental_inscripciones3','si;file;../documentos/inscripciones;descargar_archivo_gestiondocumental_inscripciones4;tipo_fichero4;peso_fichero4;inscripciones;descargar_archivo_gestiondocumental_inscripciones4','si;file;../documentos/inscripciones;descargar_archivo_gestiondocumental_inscripciones5;tipo_fichero5;peso_fichero5;inscripciones;descargar_archivo_gestiondocumental_inscripciones5');



// Hoja de estilos para la tabla

$clase_tabla_listado = "class='table table-bordered table-striped table-condensed table-hover'";

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.

$filtro_noc_para_listado = " ";

// Para el paginado

$registros_por_pagina = "30";

// Estilo del paginado

$clase_paginado = "style='color:#000000; text-decoration:none;'";

// Formato paginado: reducido o no (Ej reducido: � [1] ...[3] [4] [5] ...[10] �)

$paginado_reducido = 1;



// Permitir la exportacion a excel

$permitir_creacion_de_registros_excel = 0;

$titulo_boton_crear_excel = "EXPORTAR A EXCEL";



// Color de los registros

/*

$nombre_funcion_bgcolor_por_registro = "funcion_bgcolor_registro";

function funcion_bgcolor_registro($valor_id) {

   $color_naranja = "#EFB334";

   $color_gris_oscuro = "#C1BFBB";

   // Vemos el estado del recibo

   $tipo_resultado_ultima_gestion_id = obtener_campo('tipo_resultado','recibos_gestiones_t','left join recibos_t on recibos_gestiones_t.recibo_id=recibos_t.id','recibos_t.id='.$valor_id.' order by recibos_gestiones_t.fecha_alta desc limit 1');

   if ($tipo_resultado_ultima_gestion_id != "") { return  $color_naranja; }

   //echo "tipo_resultado: $tipo_resultado_ultima_gestion_id";

}

*/





// Filtros iniciales al listado de registros 

// Puedes definir una consulta inicial para el listado de registros, de forma que se apliquen filtros

// para que no se vean todos los registros que existen en la tabla en funcion de cualquier condicion que definas aqui.

// PONER LA VIA DE COBRO QUE CORRESPONDA:  and via_cobro=

//$filtros_iniciales = " and user_id='$user_id'";



// Otro Ejemplo

/*

if ($hoja_ruta == 1) {

	$filtros_iniciales = " and (empresa_servicio=1 or empresa_servicio=2 or empresa_servicio=6) and (via_cobro=1) and recibos_gestiones_t.user_destino_id='$user_id' ";

	$left_join_inicial = " left join recibos_gestiones_t on $tabla.id=recibos_gestiones_t.recibo_id  ";

} else {

	$filtros_iniciales = " and (empresa_servicio=1 or empresa_servicio=2 or empresa_servicio=6) and (via_cobro=1) ";

}

*/





// Cada linea de registro en la tabla del listado podra tener un conjunto de acciones. Las Mas normales son

// MODIFICAR y BORRAR. En determinado momento nos interesara que esas acciones aparezcan o no (en funcion de una

// condicion que se aplique a cada registro). En otros casos, las opciones son iguales para todos los registros.

// Por lo tanto, tenemos dos modos de trabajo.

// 1. MODO GENERICO: Las acciones son iguales para todos los registros.

// 2. MODO PERSONALIZADO: Las acciones dependen de la ejecucion de una funcion para cada registro.



// 2. MODO PERSONALIZADO

// Nombre de la funcion que se ejecutara para cada registro. Si se deja blanco el modo personalizado NO ESTA ACTIVO

$nombre_funcion_acciones_por_registro = "funcion_acciones_registro";

function funcion_acciones_registro($valor_id)

{

    global $tabla, $enlacevolver, $script;

    // me llega decodificado el id del registro en cuestion (en valor_id) pero tengo que codificarlo para ponerlo

    // en los enlaces

    $id_encript = base64_encode(base64_encode($valor_id));

    

    // Aqui se incluye el codigo necesario sobre el registro id_encript con el objetivo de sacar tantos echos

    // como acciones sean necesarias para este registro

    /*

	echo '<a class="smallmario green" href="'.$enlacevolver.$script.'&accion=formmodificar&id='.$id_encript.'&padre_id='.$_REQUEST[padre_id].'">MODIFICAR</a> ';

	echo '<a class="smallmario red" href="'.$enlacevolver.$script.'&accion=formborrar&id='.$id_encript.'&padre_id='.$_REQUEST[padre_id].'">BORRAR</a> ';

	*/

			

    // Lo siguiente es un ejemplo de un proyecto determinado

    /*

    $array_datos_registro = obtener_multiples_campos(array("ano"),$tabla,""," $tabla.id='".$valor_id."' ","","","");

    // compruebo que la funcion solo me devuelve los datos de un registro

    if (count($array_datos_registro) == 1 && $array_datos_registro[0]['ano'] == date("Y"))

    {

            echo '

            <a href="'.$enlacevolver.$script.'&accion=formmodificar&id='.$id_encript.'"><img src="images/table_edit.png" title="Modificar" border="0" /></a> ';

    }

    */

}

// FIN 2. MODO PERSONALIZADO





// 1. MODO GENERICO

// acciones_por_registro: Es un array de botones u opciones que debe tener cada registro

// Cada registro tendra un conjunto de acciones. Fijarse como en el ID del registro, ponemos #ID#.

// maestro_formulario.php pondra el ID correcto.

$acciones_por_registro = array(); 

$condiciones_visibilidad_por_registro = array();



/*

$acciones_por_registro[] = '<a class="icon" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';

$condiciones_visibilidad_por_registro[] = "";



$acciones_por_registro[] = '<a class="icon" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';

*/

$condiciones_visibilidad_por_registro[] = "";

// FIN 1. MODO GENERICO



// Procesos PRE y POST de las acciones formcrear, formmodificar, etc

// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario

// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado

//          el insert de accioncrear

/*

$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";

$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";

$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";

$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";

$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";

$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";

$proceso_pre_listado = "modules/gestproject/index_cobros_facturas_new_proceso_pre_listado.php";

$proceso_post_listado = "modules/gestproject/index_cobros_facturas_new_proceso_pre_listado.php";

$proceso_post_form_modificar = "modules/gestproject/index_cobros_facturas_new_proceso_post_form_modificar.php";

*/



// CONFIGURACION DEL PADRE

// Si este script no tiene padre, dejar el resto de los campos en blanco

// IMPORTANTE: el padre debe venir desde el script anterior en la forma: &padre_id=XYZ

//             donde XYZ es el valor con doble codificacion base64

//             Por ejemplo, si un script index_facturas_new es el padre de index_facturas_cobros_new

//             dentro de index_facturas_new hay que hacer un:

//             $id_encript = base64_encode(base64_encode($linea[id]));

//             En este caso, el index_facturas_new tendria un enlace a index_facturas_cobros_new de

//             la siguiente forma:

//             <a href=\"modules.php?mod=gestproject&file=index_cobros_facturas_new&padre_id=$id_encript\">COBROS</a>

//$proceso_pre_accioncrear= "modules/gestproject/index_facturas_lineas_new2_pre_accioncrearmodifificar.php";

//$proceso_pre_accionmodificar= "modules/gestproject/index_facturas_lineas_new2_pre_accioncrearmodifificar.php";



//$proceso_post_accioncrear= "modules/gestproject/index_facturas_lineas_new2_post_accioncrearmodificar.php";

//$proceso_post_accionmodificar= "modules/gestproject/index_facturas_lineas_new2_post_accioncrearmodificar.php";

//$proceso_post_accionborrar= "modules/gestproject/index_facturas_lineas_new2_post_accioncrearmodifificar.php";



// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc

$campo_padre = "inscripcion_id";

// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'

$consulta_nombre_padre = "select concat(num_instancia,'- ', nombre,' ', apellidos) as nombre from registro_convocatorias2015_t where id=#PADREID#;";

// Enlace al que volver al padre

$enlace_volver_a_padre = "modules.php?mod=gestproject&file=index_inscripcionesbolsa2015_new";

// Texto del enlace volver al padre

$texto_volver_a_padre = "Volver a inscripciones";



// CONFIGURACION DEL BUSCADOR

$habilitar_buscador = 0;

// estilos de los buscadores

$clase_buscador_input = "";

$clase_buscador_select = "";

$clase_buscador_checkbox = "";



$buscadores = array();

$buscadores[] = "input;num_factura;;;;buscar por num_factura";

$buscadores[] = "select;tipo_factura;maestro_tipos_facturas_t;nombre;id;buscar por tipo;noc='$noc'";

$buscadores[] = "select;cliente_id;clientes_t;nombre;id;buscar por cliente;noc='$noc'";

$buscadores[] = "intervalo_fechas;fecha_factura;;;;<br>Buscar por fecha";

//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";



/*

$tipos_col1  = array('select;maestro_tipos_facturas_t;nombre;id;nombre;noc="'.$noc.'"','select;clientes_t;nombre;id;nombre;noc="'.$noc.'"','select;proyectos_t;nombre;id;nombre;noc="'.$noc.'"','datetime3_calendario','select;maestro_forma_pago_t;nombre;id;nombre;noc="'.$noc.'"','textarea;300;100');

*/



/*

if ($login == "mmartin" || $login == "rsosa") {

	$buscadores[] = "intervalo_fechas;fecha_gestion;;;;<br><b>Fecha de gestion.</b>:";

}

*/

// VISUALIZAR SUMATORIOS

// Permite indicar un nombre de campo el cual se sumara en todos los registros de la rejilla y aparecera al final de la misma

//$campo_a_sumar = "total_recibo";



// INCLUSION DEL MAESTRO_FORMULARIO.PHP





// OJO!! hay que hacer un: ln -s ../maestro_formulario.php maestro_formulario.php

// dentro de la carpeta del noc correspondiente

$padre_id = base64_decode(base64_decode($_REQUEST[padre_id]));



/*

echo "<br><center><b>DATOS DE LA FACTURA</b>  (<a href=modules.php?mod=gestproject&file=index_facturas_new2&pag=0>Volver a facturaci&oacute;n</a>)<hr><br>";

$consulta  = "select * from facturas_new_t  where id='$padre_id'";

$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: " . mysql_error());

while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC)) {

    $nombre_cliente = obtener_campo('nombre','clientes_t','','id='.$linea[cliente_id]);

    $nombre_facturador = obtener_campo('nombre','maestro_pagadores_t','','id='.$linea[facturador_id]);

    $facturador_id = $linea[facturador_id];

	list ($fecha_factura, $n) = explode(' ', $linea[fecha_factura]);

	list ($fecha_factura_ano, $fecha_factura_mes, $fecha_factura_dia) = explode('-', $fecha_factura);

	

	$total_factura = 0;

	$consulta2  = "select sum(total_linea) as total from facturas_lineas_new_t where factura_id='$padre_id'";

	$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());

	while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC)) {

	  $total_factura = $linea2[total];

	}



	echo "Num factura: <b>$linea[num_factura]</b> | Cliente: <b>$nombre_cliente</b> | Facturador: <b>$nombre_facturador</b> <br>Fecha: <b>$fecha_factura_dia/$fecha_factura_mes/$fecha_factura_ano</b> | TOTAL: <b>$total_factura &euro;</b>";	

}



// Inicio  del SELECT de los conceptos que existen en el maestro por si queremos copiar:

echo "<br><br>Si desea recuperar un concepto del maestro, seleccione: <select id=conceptos_del_maestro><option value=''></option>";

$cons = "select * from maestro_conceptos2_t where facturador_id = '$facturador_id' order by concepto asc";

$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());

while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {

   echo "<option value=".$lin['id'].">".substr($lin['concepto'],0,50)."</option>";

}

echo "</select>";

// Fin del SELECT de los conceptos que existen en el maestro por si queremos copiar



echo "</center>";



*/



include ("maestro_formulario.php");





// Para desactivar el  el caracter obligatorio de total_parcial y tipo_impuesto_id en el caso de un prodcuto_id seleccionado

?>



<script>

$( document ).ready(function() {



	var update_pizza = function () {



		$( "#producto_id" ).change(function() {

			

			$( "#producto_id option:selected" ).each(function() {

				//alert($(this).val());

				if ($(this).val() != "") {

					  // alert($(this).val());

					  // Quitamos el caracter obligatorio de total_parcial y tipo_impuesto_id

					  $("#total_parcial").removeAttr("required");

					  $("#tipo_impuesto_id").removeAttr("required");

				} 

			});

		 }).trigger( "change" );

	};



	$(update_pizza);

	

	$( "#conceptos_del_maestro" ).change(function() {

		$( "#conceptos_del_maestro option:selected" ).each(function() {

			//alert($(this).val());

			if ($(this).val() != "") {

				    

					//alert($(this).val());

					

					var data = "";

					data = "id=" + $(this).val();

					//alert(data);

				  

					$.ajax({

						type: "GET",

						url: 'http://www.formacioncip.com/intranet/modules/gestproject/json_maestro_conceptos.php',

						async: false,

						dataType: "json",

						data: data,

						success: function(data){

							//alert(data);

							$.each(data,function(key, registro) {

								var concepto = "";

								var anexo = "";

								

								$.each(registro, function(campo, valor) {

									//alert(campo + ": " + valor);

									if (campo == "concepto") { concepto = valor; }

									if (campo == "anexo") { anexo = valor; }							

								});

								

								//alert(anexo);

								// Incluimos el concepto y el anexo los campos correspondientes

								$('#concepto').val(concepto);

								CKEDITOR.instances.anexo.insertHtml(anexo);

								

							});

							

									   

							$("#capa_num_eventos_disponibles").html('<span class="label bg-dark">\

							'+num_eventos+'</span>\

							');

									   

						},

						error: function(data) {

							alert('error');

						}

					});

			} 

		});

	 }).trigger( "change" );

		 

		 

});

//$("#pizza").change(update_pizza);

</script>

