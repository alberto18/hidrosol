
<?php 
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
?>
      
            
<?php
 
   include("obtener_opciones.php");
?>
            
 
<?php

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "MAESTRO SECTORES PLAGA";
// Titulo que aparece en la pestaña del navegador
$titulo_pagina = "MAESTRO SECTORES PLAGA";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";
// Texto que aparece en el boton de crear
$titulo_boton_crear = "CREAR NUEVO SECTOR PLAGA";
// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.
// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 
$clase_boton_crear = " class='btn btn-success' ";
$clase_boton_buscar = " class='btn btn-mini btn-black' ";
$clase_boton_guardar = " class='btn btn-success' ";
$clase_boton_volver  = " class='btn btn-navi' ";
$clase_boton_confirmar_borrado  = " class='btn btn-danger' ";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 1;
// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// Nombre del script
$script = "index_maestro_sectores_plaga_new";
// Nombre de la tabla
$tabla = "sectores_plagas_t"; // OJO, la clave principal se debe llamar id

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
$campos_col1 = array('sector_id','plaga');

// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]
$ayudas_col1 = array('');

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('on','on');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php
$tipos_col1  = array('select;sectores_t;nombre;id;nombre','text;30');

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
$filtro_noc_para_insert = "";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = "";

// Campo para la busqueda
$campo_busqueda = "sector_id";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript
$plantilla_insercion = "index_maestro_sectores_plaga_new.plantilla.php";
if ($plantilla_insercion != "") {
  $fichero_absoluto =  "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}

// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 1;
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','sector_id','plaga');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','Sector','Plaga');
// Decodificacion si existiese de los campos
$campos_listado_decod = array ('','si;sectores_t;nombre;id;nombre','');
// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-striped table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
$filtro_noc_para_listado = "";
// Para el paginado
$registros_por_pagina = "1000";

// acciones_por_registro: Es un array de botones u opciones que debe tener cada registro
// Cada registro tendra un conjunto de acciones. Fijarse como en el ID del registro, ponemos #ID#.
// maestro_formulario.php pondra el ID correcto.

// 2. MODO PERSONALIZADO
// Nombre de la funcion que se ejecutara para cada registro. Si se deja blanco el modo personalizado NO ESTA ACTIVO

$nombre_funcion_acciones_por_registro = "funcion_acciones_registro";
function funcion_acciones_registro($valor_id)
{
    global $tabla, $enlacevolver, $script;
    // me llega decodificado el id del registro en cuestion (en valor_id) pero tengo que codificarlo para ponerlo
    // en los enlaces
    $id_encript = base64_encode(base64_encode($valor_id));
    
	$instrucciones = "";
	$cons2 = "select maestro_instrucciones_t.nombre from maestro_instrucciones_t left join sectores_plagas_planificaciones_t on maestro_instrucciones_t.id=sectores_plagas_planificaciones_t.instruccion_id where sector_plaga_id = '$valor_id'";
       	$res = mysql_query($cons2) or die("La consulta fall&oacute;: $cons " . mysql_error());
        while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {
           $instrucciones .=  " ". $lin['nombre'] . "<br>";
        }
	echo "$instrucciones";
	
	echo '<a href="modules.php?mod=gestproject&file=index_maestro_sectores_plaga_planificacion&padre_id='.$id_encript.'&pag=0">PLANIFICACION BASE</a><br>';
	echo '<a href="'.$enlacevolver.$script.'&accion=formmodificar&id='.$id_encript.'">MODIFICAR</a> ';
			

}

// FIN 2. MODO PERSONALIZADO



$acciones_por_registro = array(); 
$condiciones_visibilidad_por_registro = array();

//$acciones_por_registro[] = '<a class="btn btn-mini btn-sky" href="modules.php?mod=gestproject&file=index_productos_fotos_new&padre_id=#ID#&pag=0">FOTOS</a>';
$condiciones_visibilidad_por_registro[] = "";
// Ejemplo: $condiciones_visibilidad_por_registro[] = "select user_id as visualizar_opcion from usuarios_t where user_id=#ID#";

//$acciones_por_registro[] = '<a class="btn btn-mini btn-sky" href="modules.php?mod=gestproject&file=index_productos_variantes_asignadas_new&padre_id=#ID#&pag=0">VARIANTES DEL PRODUCTO</a>';
$condiciones_visibilidad_por_registro[] = "";

//$acciones_por_registro[] = '<a class="btn btn-mini btn-sky" href="modules.php?mod=gestproject&file=index_productos_variantes_stocks_new&padre_id=#ID#&pag=0">STOCKS</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="icon" href="modules.php?mod=gestproject&file=index_maestro_sectores_plaga_planificacion&padre_id=#ID#&pag=0"><i class="fugue-pencil" title="editar"></i> PLANIFICACION BASE</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="icon" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="icon" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear
/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";
$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";
*/
// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
// $campo_padre = "";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
// $consulta_nombre_padre = " select nombre as nombre from productos_t where id=#PADREID#";

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 1;
$buscadores = array();
$buscadores[] = "input;plaga";
$buscadores[] = "select;sector_id;sectores_t;nombre;id;buscar por sector";
//$buscadores[] = "intervalo_fechas;fecha_creacion;;;;Fecha de creaci&oacute;n";
//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";


// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

?>