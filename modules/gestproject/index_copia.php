        <!-- /.aside -->
        <section id="content">
          <section class="vbox">
            <header class="header bg-white b-b b-light">
              <p>Cuadro <small>Global</small></p>
            </header>
			

			  
            <section class="scrollable wrapper">              
              <div class="row">
                <div class="col-lg-8">
                  <div class="row">
				  
				  
						<section class="panel panel-default">
						<div class="row m-l-none m-r-none bg-light lter">
						
						  
						  <div class="col-sm-6 col-md-3 padder-v b-r b-light lt">                     
							<span class="fa-stack fa-2x pull-left m-r-sm">
							  <i class="fa fa-circle fa-stack-2x text-info"></i>
							  <i class="fa fa-medkit fa-stack-1x text-white"></i>
							  <span class="easypiechart pos-abt easyPieChart" data-percent="100" data-line-width="4" data-track-color="#f5f5f5" data-scale-color="false" data-size="50" data-line-cap="butt" data-animate="3000" data-target="#firers" data-update="5000" style="width: 50px; height: 50px; line-height: 50px;"><canvas width="50" height="50"></canvas></span>
							</span>
							<a class="clear" href="modules.php?mod=gestproject&file=index_vigilanciasalud_new">
							  <span class="h5 block m-t-xs"><strong>VIGILANCIA SALUD</strong></span>
							  <small class="text-muted text-uc">ACCESO</small>
							</a>
						  </div>
						  
						  <div class="col-sm-6 col-md-3 padder-v b-r b-light">                     
							<span class="fa-stack fa-2x pull-left m-r-sm">
							  <i class="fa fa-circle fa-stack-2x text-warning"></i>
							  <i class="fa fa-check-square-o fa-stack-1x text-white"></i>
							  <span class="easypiechart pos-abt easyPieChart" data-percent="100" data-line-width="4" data-track-color="#f5f5f5" data-scale-color="false" data-size="50" data-line-cap="butt" data-animate="3000" data-target="#firers" data-update="5000" style="width: 50px; height: 50px; line-height: 50px;"><canvas width="50" height="50"></canvas></span>
							</span>
							<a class="clear" href="modules.php?mod=gestproject&file=index_formulariossolicitudes_new">
							  <span class="h5 block m-t-xs"><strong>FORMULARIOS Y SOLICITUDES</strong></span>
							  <small class="text-muted text-uc">ACCESO</small>
							</a>
						  </div>
						  
						  
						  <div class="col-sm-6 col-md-3 padder-v b-r b-light lt">                     
							<span class="fa-stack fa-2x pull-left m-r-sm">
							  <i class="fa fa-circle fa-stack-2x text-danger"></i>
							  <i class="fa fa-fire-extinguisher fa-stack-1x text-white"></i>
							  <span class="easypiechart pos-abt easyPieChart" data-percent="100" data-line-width="4" data-track-color="#f5f5f5" data-scale-color="false" data-size="50" data-line-cap="butt" data-animate="3000" data-target="#firers" data-update="5000" style="width: 50px; height: 50px; line-height: 50px;"><canvas width="50" height="50"></canvas></span>
							</span>
							<a class="clear" href="modules.php?mod=gestproject&file=index_prevencionriesgoslaborales_new">
							  <span class="h5 block m-t-xs"><strong>PREVENCION RIESGOS LABORALES</strong></span>
							  <small class="text-muted text-uc">ACCESO</small>
							</a>
						  </div>
						  
						  <div class="col-sm-6 col-md-3 padder-v b-r b-light">                     
							<span class="fa-stack fa-2x pull-left m-r-sm">
							  <i class="fa fa-circle fa-stack-2x text-gray"></i>
							  <i class="fa fa-book fa-stack-1x text-white"></i>
							  <span class="easypiechart pos-abt easyPieChart" data-percent="100" data-line-width="4" data-track-color="#f5f5f5" data-scale-color="false" data-size="50" data-line-cap="butt" data-animate="3000" data-target="#firers" data-update="5000" style="width: 50px; height: 50px; line-height: 50px;"><canvas width="50" height="50"></canvas></span>
							</span>
							<a class="clear" href="modules.php?mod=gestproject&file=index_normativainteres_new">
							  <span class="h5 block m-t-xs"><strong>NORMATIVA DE INTERES</strong></span>
							  <small class="text-muted text-uc">ACCESO</small>
							</a>
						  </div>
						  
						  
						</div>
					  </section>

				  
						<section class="panel panel-default">
						<div class="row m-l-none m-r-none bg-light lter">
						
						  
						  <div class="col-sm-6 col-md-3 padder-v b-r b-light lt">                     
							<span class="fa-stack fa-2x pull-left m-r-sm">
							  <i class="fa fa-circle fa-stack-2x text-green"></i>
							  <i class="fa fa-user fa-stack-1x text-white"></i>
							  <span class="easypiechart pos-abt easyPieChart" data-percent="100" data-line-width="4" data-track-color="#f5f5f5" data-scale-color="false" data-size="50" data-line-cap="butt" data-animate="3000" data-target="#firers" data-update="5000" style="width: 50px; height: 50px; line-height: 50px;"><canvas width="50" height="50"></canvas></span>
							</span>
							<a class="clear" href="modules.php?mod=gestproject&file=index_procesosselectivos_new">
							  <span class="h5 block m-t-xs"><strong>PROCESOS SELECTIVOS</strong></span>
							  <small class="text-muted text-uc">ACCESO</small>
							</a>
						  </div>
						  
						  <div class="col-sm-6 col-md-3 padder-v b-r b-light">                     
							<span class="fa-stack fa-2x pull-left m-r-sm">
							  <i class="fa fa-circle fa-stack-2x text-info"></i>
							  <i class="fa fa-users fa-stack-1x text-white"></i>
							  <span class="easypiechart pos-abt easyPieChart" data-percent="100" data-line-width="4" data-track-color="#f5f5f5" data-scale-color="false" data-size="50" data-line-cap="butt" data-animate="3000" data-target="#firers" data-update="5000" style="width: 50px; height: 50px; line-height: 50px;"><canvas width="50" height="50"></canvas></span>
							</span>
							<a class="clear" href="modules.php?mod=gestproject&file=index_relacionessindicales_new">
							  <span class="h5 block m-t-xs"><strong>RELACIONES SINDICALES</strong></span>
							  <small class="text-muted text-uc">ACCESO</small>
							</a>
						  </div>
						  
						  
						  <div class="col-sm-6 col-md-3 padder-v b-r b-light lt">                     
							<span class="fa-stack fa-2x pull-left m-r-sm">
							  <i class="fa fa-circle fa-stack-2x text-warning"></i>
							  <i class="fa fa-euro fa-stack-1x text-white"></i>
							  <span class="easypiechart pos-abt easyPieChart" data-percent="100" data-line-width="4" data-track-color="#f5f5f5" data-scale-color="false" data-size="50" data-line-cap="butt" data-animate="3000" data-target="#firers" data-update="5000" style="width: 50px; height: 50px; line-height: 50px;"><canvas width="50" height="50"></canvas></span>
							</span>
							<a class="clear" href="modules.php?mod=gestproject&file=index_ofertasinteres_new">
							  <span class="h5 block m-t-xs"><strong>OFERTAS DE INTERES</strong></span>
							  <small class="text-muted text-uc">ACCESO</small>
							</a>
						  </div>
						  
						  <div class="col-sm-6 col-md-3 padder-v b-r b-light">                     
							<span class="fa-stack fa-2x pull-left m-r-sm">
							  <i class="fa fa-circle fa-stack-2x text-danger"></i>
							  <i class="fa fa-table fa-stack-1x text-white"></i>
							  <span class="easypiechart pos-abt easyPieChart" data-percent="100" data-line-width="4" data-track-color="#f5f5f5" data-scale-color="false" data-size="50" data-line-cap="butt" data-animate="3000" data-target="#firers" data-update="5000" style="width: 50px; height: 50px; line-height: 50px;"><canvas width="50" height="50"></canvas></span>
							</span>
							<a class="clear" href="modules.php?mod=gestproject&file=index_formacion_new">
							  <span class="h5 block m-t-xs"><strong>FORMACION</strong></span>
							  <small class="text-muted text-uc">ACCESO</small>
							</a>
						  </div>
						  
						  
						</div>
					  </section>
			  
				  
				  
                    <div class="col-sm-6">
                      <section class="panel panel-default">
                        <header class="panel-heading bg-danger lt no-border">
                          <div class="clearfix">
                            <a href="#" class="pull-left thumb avatar b-3x m-r">
                              <img src="images/avatar.jpg" class="img-circle">
                            </a>
                            <div class="clear">
                              <div class="h3 m-t-xs m-b-xs text-white">
                                <?= $nombre_empleado ?> 
                                <i class="fa fa-circle text-white pull-right text-xs m-t-sm"></i>
                              </div>
                              <small class="text-muted"><?= $cargo_empleado ?> </small>
                            </div>                
                          </div>
                        </header>
                        <div class="list-group no-radius alt">
                          <a class="list-group-item" href="modules.php?mod=gestproject&file=index_mensajes_new">
                            <span class="badge bg-success"><?= $mensajes_sin_leer ?></span>
                            <i class="fa fa-envelope icon-muted"></i> 
                            Notificaciones sin leer
                          </a>
                        </div>
                      </section>
                    </div>
					
					<!--
					<div class="col-md-6">
					  <section class="panel b-light">
						<header class="panel-heading bg-primary dker no-border"><strong>Calendario de solicitudes</strong></header>
						<div id="calendar" class="bg-primary m-l-n-xxs m-r-n-xxs"></div>
						<div class="list-group">
						  <a href="#" class="list-group-item text-ellipsis">
							<span class="badge bg-danger">EN TRAMITE</span> 
							Vacaciones 05/01/2014 - 10/01/2014
						  </a>
						  <a href="#" class="list-group-item text-ellipsis"> 
							<span class="badge bg-success">ACEPTADA</span> 
							Asuntos propios 15/01/2014 - 15/01/2014
						  </a>
						  <a href="#" class="list-group-item text-ellipsis">
							<span class="badge bg-light">1</span>
							Festividad de San Antonio
						  </a>
						</div>
					  </section>                  
					</div>
					-->
					
                  </div>
                </div>
				
                <div class="col-lg-4">
                  <section class="panel panel-default">
                    <div class="panel-body">
                      <div class="clearfix text-center m-t">
                        <div class="inline">
                          <div class="easypiechart" data-percent="100" data-line-width="5" data-bar-color="#4cc0c1" data-track-Color="#f5f5f5" data-scale-Color="false" data-size="130" data-line-cap='butt' data-animate="1000">
                            <div class="thumb-lg">
                              <img src="images/avatar.jpg" class="img-circle">
                            </div>
                          </div>
                          <div class="h4 m-t m-b-xs"><?= $nombre_empleado ?></div>
                          <small class="text-muted m-b"><?= $cargo_empleado ?></small>
                        </div>                      
                      </div>
                    </div>
                    <footer class="panel-footer bg-info text-center">
                      <div class="row pull-out">
                        <div class="col-xs-6">
                          <div class="padder-v">
                            <span class="m-b-xs h3 block text-white">22</span>
                            <small class="text-muted">Vacaciones disponibles</small>
                          </div>
                        </div>
                        <div class="col-xs-6 dk">
                          <div class="padder-v">
                            <span class="m-b-xs h3 block text-white"><?= $dias_asuntos_propios ?></span>
                            <small class="text-muted">Asuntos propios disponibles</small>
                          </div>
                        </div>
                      </div>
                    </footer>
                  </section>
                </div>
              </div>
			  
	  <?php
		// Vemos el numero de mensajes/notificaciones no leidas
		$consulta6 = "select * from noticias_t order by fecha desc;";
		//echo "$consulta6";
		$noticias = 0;
		$contenido_noticias = "";
		$resultado6 = mysql_query($consulta6) or die("La consulta fall&oacute;: $consulta6 " . mysql_error());
		while ($linea6 = mysql_fetch_array($resultado6, MYSQL_ASSOC)) {
				$noticias++;
				list($mensaje_fecha, $mensaje_hora) = explode(' ', $linea6[fecha]);
				list($mensaje_ano, $mensaje_mes, $mensaje_dia) = explode('-', $mensaje_fecha);
				list($mensaje_hora, $mensaje_min) = explode(':', $mensaje_hora);
				$nombre_remitente = obtener_campo('nombre','usuarios_t','','id='.$linea6[user_id]);
				//$noticia_id_encript = base64_encode(base64_encode($linea6[id]));
				
				$contenido_noticias .= '
                      <article class="media">
                        <span class="pull-left thumb-sm"><i class="fa fa-file-o fa-3x icon-muted"></i></span>          
                        <div class="media-body">
                          <div class="pull-right media-xs text-center text-muted">
                            <strong class="h4">'.$mensaje_dia.'/'.$mensaje_mes.'/'.$mensaje_ano.'</strong><br>
                            <small class="label bg-light">'.$mensaje_hora.':'.$mensaje_min.'</small>
                          </div>
                          <a href="#" class="h4">'.$linea6[asunto].'</a>
                          <small class="block"><a href="#" class="">'.$nombre_remitente.'</a></small>
                          <small class="block m-t-sm">'.$linea6[descripcion].'</small>
                        </div>
                      </article>

					  <div class="line pull-in"></div>
				';
		}
	  ?>
	  
              <div class="row">
                <div class="col-lg-12">
                  <section class="panel panel-default">
                    <header class="panel-heading">                    
                      <span class="label bg-dark"><?= $noticias ?></span> RESOLUCIONES, COMUNICACIONES, CIRCULARES, INSTRUCCION Y ANUNCIOS AL PERSONAL
                    </header>
                    <section class="panel-body slim-scroll" data-height="230px">
					
					  <?= $contenido_noticias ?>
                      
                    </section>
                  </section>
                </div>

              </div>
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>
  </section>