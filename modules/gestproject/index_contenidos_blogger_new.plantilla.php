  <div class="row">
  	<div class="col-sm-12">
	  <section class="panel panel-default">
		<header class="panel-heading font-bold">Crear | Modificar un contenido</b></header>
<table border=1   class='table table-bordered table-condensed table-hover' align=center>
        <tr>
        <td>T&iacute;tulo:</td><td>[pn_title]</td>
        </tr>
		<tr>
        <td>T&iacute;tulo en ingl&eacute;s:</td><td>[pn_title_en]</td>
        </tr>
        <tr>
        <td>T&oacute;pico principal:</td><td>[pn_topic]</td>       
        </tr>
		<tr>
        <td>T&oacute;pico secundario:</td><td>[pn_topic2]</td>       
        </tr>
		<tr>
        <td>Tercer T&oacute;pico:</td><td>[pn_topic3]</td>       
        </tr>
		<tr>
        <td>Categor&iacute;a:</td><td>[categoria_id]</td>       
        </tr>
		<tr>
        <td>Firma:</td><td>[firma_id]</td>       
        </tr>
         <tr>
         <td>Fecha contenido:</td><td>[fecha_contenido]</td>    
        </tr>
       <!-- <tr>
        <td>Fecha caducidad:</td><td>[fecha_caducidad]</td>
        </tr>-->
      <tr>
         <td>Contenido reducido:</td><td>[pn_hometext]</td>
                
        </tr>
		<tr>
         <td>Contenido reducido en ingl&eacute;s:</td><td>[pn_hometext_en]</td>
      </tr>
	  <tr><td>Contenido completo en ingl&eacute;s:</td><td>[pn_bodytext_en]</td></tr>
      <tr><td>Contenido completo:</td><td>[pn_bodytext]</td></tr>
     <!--   <tr><td>Orden:</td><td>[orden]</td>
        </tr>
        <tr><td>Notas:</td><td>[pn_notes]</td></tr>-->
      <tr><td>Fotograf&iacute;a principal:</td><td>[nombre_fichero]</td></tr>
      <tr><td>Imagen 2:</td><td>[nombre_fichero2]</td></tr>
	  <tr><td>Imagen 3:</td><td>[nombre_fichero3]</td></tr>	
      <tr><td>Imagen 4:</td><td>[nombre_fichero4]</td></tr>	
      <tr><td>Imagen 5:</td><td>[nombre_fichero5]</td></tr>
      <tr><td>T&iacute;tulo audio:</td><td>[titulo_audio]</td></tr>	  
      <tr><td>Fichero audio:</td><td>[nombre_fichero6]</td></tr>
      <tr><td>T&iacute;tulo video:</td><td>[titulo_video]</td></tr>		  
      <tr><td>Url v&iacute;deo:</td><td>[url_video]</td></tr>	
	  <tr><td>Documento con nota de prensa (PRESS ROOM) : <br>
	  <font size=-2>Formatos recomendados: .doc  .pdf</font></td><td>[nombre_fichero7]</td></tr>	
	   <tr><td>Nombre archivo ZIP con fotograf&iacute;as subido previamente al servidor:</td><td>[nombre_fichero8]</td></tr>	
	   <tr><td>Nombre archivo ZIP con videos subido previamente al servidor:</td><td>[nombre_fichero9]</td></tr>	
       <tr><td>Nombre archivo ZIP con audios subido previamente al servidor:</td><td>[nombre_fichero10]</td></tr>		  
</table>
<br><br>



	  </section>
	</div>
   </div>


