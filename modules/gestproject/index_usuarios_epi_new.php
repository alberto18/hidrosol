<?php 
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
?>

<?php

   //include("obtener_miperfil.php");
   
   //include("obtener_opciones.php");

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "GESTION MATERIAL PRESTADO AL USUARIO";
// Titulo que aparece en la pestaña del navegador
$titulo_pagina = "GESTION MATERIAL PRESTADO AL USUARIO";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";
// Texto que aparece en el boton de crear
$titulo_boton_crear = "INCORPORAR NUEVO MATERIAL PRESTADO";
// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.
// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 
$clase_boton_crear = " class='btn btn-success' ";
$clase_boton_buscar = " class='btn btn-mini btn-black' ";
$clase_boton_guardar = " class='btn btn-success' ";
$clase_boton_volver  = " class='btn btn-navi' ";
$clase_boton_confirmar_borrado  = " class='btn btn-danger' ";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 1;
// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// Nombre del script
$script = "index_usuarios_epi_new";
// Nombre de la tabla
$tabla = "usuarios_material_t"; // OJO, la clave principal se debe llamar id

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
$campos_col1 = array('fecha','producto','unidades');

// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]
$ayudas_col1 = array();

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('on','on');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('','');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('','');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php
$tipos_col1  = array('datetime3','select;maestro_productos_t;producto;id;producto;;habilitado_popup|modules.php?mod=gestproject&file=index_productos_busquedas_new|BUSCAR;sin_precarga','text');
        //SOLAMENTE LOS QUE TENGAN FAMILIA EPI QUE ES LA 10
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
// $filtro_noc_para_insert = " noc='$noc', ";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
//$campos_automaticos_para_insert = " user_id=$user_id ,";

// Campo para la busqueda
$campo_busqueda = "fecha desc";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript
$plantilla_insercion = "index_usuarios_epi_new.plantilla.php";
if ($plantilla_insercion != "") {
  $fichero_absoluto =  "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}

// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 1;
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','fecha','referencia','producto','unidades','estado');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','fecha','referencia','producto','unidades','estado');
// Decodificacion si existiese de los campos
$campos_listado_decod = array ('','si;datetime','','si;maestro_productos_t;producto;id','','');
// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_listado = " and noc='$noc'";
// Para el paginado
$registros_por_pagina = "3000";

$ocultar_imprimir_listado_final_rejilla = 1;

// Si permitimos una rejilla editable, aparecera en la izquierda un checkbox que se podra seleccionar desde jquery
$listado_editable = 1;
// A la url siguiente se le pasara via GET los check_123, check_124, etc con los ids seleccionados
$accion_a_realizar_con_campos_marcados = "IMPRIMIR|modules.php?mod=gestproject&file=informe_materiales";


// acciones_por_registro: Es un array de botones u opciones que debe tener cada registro
// Cada registro tendra un conjunto de acciones. Fijarse como en el ID del registro, ponemos #ID#.
// maestro_formulario.php pondra el ID correcto.
$acciones_por_registro = array(); 
$condiciones_visibilidad_por_registro = array();

$acciones_por_registro[] = '<a class="smallmario green"  href="modules/gestproject/imprimir_doc_productos.php?id=#ID#" target="_new"><i class="fugue-pencil" title="editar"></i> IMPRIMIR</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="smallmario green"  href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#&padre_id=#PADREID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="smallmario green"  href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#&padre_id=#PADREID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";


/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear
/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";
$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";
*/

// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
 $campo_padre = "user_id";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
 $consulta_nombre_padre = " select login as nombre from usuarios_t where id=#PADREID#";

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 1;
$buscadores = array();
//$buscadores[] = "input;fecha";
//$buscadores[] = "select;producto;maestro_productos_t;producto;id;buscar por producto";
$buscadores[] = "intervalo_fechas;fecha|fecha;;;;Buscar entre fechas";
//$buscadores[] = "input;fecha";


//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";
// VISUALIZAR SUMATORIOS
// Permite indicar un nombre de campo el cual se sumara en todos los registros de la rejilla y aparecera al final de la misma
//$campo_a_sumar = "";


// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

//echo $cons;
?>



