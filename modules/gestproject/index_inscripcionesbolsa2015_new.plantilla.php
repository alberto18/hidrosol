  <div class="row">
  	<div class="col-sm-12">
	  <section class="panel panel-default">
		<header class="panel-heading font-bold">Bolsa 2015 |  <b>Num Instancia: [num_instancia]</b></header>
<table border=1   class='table table-bordered table-condensed table-hover' align=center>
        <tr>
        <td>Nombre:</td><td>[nombre]</td> <td><b>Observaciones sobre la incripci&oacute;n: </b><hr>
		Aportar DNI:</td><td>&nbsp;<hr>[aportar_dni]</td>
        </tr>
        <tr>
        <td>Apellidos:</td><td>[apellidos]</td>   <td>Aportar DNI proximo a caducar:</td><td>[aportar_dni_proximo_a_caducar]</td>    
        </tr>
		 <tr>
        <td>Fecha nacimiento:</td><td>[fecnac]</td>  <td>Extranjero no comunitario:</td><td>[extranjero_no_comunitario]</td>
		</tr>
		<tr>
        <td>DNI:</td><td>[dni]</td>  <td><b>Motivos de exclusi&oacute;n:</b><hr>
		Motivo exclusion 1:</td><td>&nbsp;<hr>[motivo_exclusion_1]</td>
		</tr>
		<tr>
        <td>Direcci&oacute;n:</td><td> [tipo_via_id] [direccion]</td>  <td>Motivo exclusion 2:</td><td>[motivo_exclusion_2]</td>
		</tr>
		<tr>
        <td>Tel&eacute;fono:</td><td>[tlf]</td>  <td>Motivo exclusion 3:</td><td>[motivo_exclusion_3]</td>
		</tr>
		<tr>
        <td>Localidad:</td><td>[localidad]</td>  <td>Motivo exclusion 4:</td><td>[motivo_exclusion_4]</td>
		</tr>
		<tr>
        <td>Provincia:</td><td>[provincia]</td>  <td>Motivo exclusion 5:</td><td>[motivo_exclusion_5]</td>
		</tr>
		<tr>
        <td>CP:</td><td>[cp]</td>  <td>Motivo exclusion 6:</td><td>[motivo_exclusion_6]</td>
		</tr>
		<tr>
        <td>Plaza convocada:</td><td>[plaza_convocada]</td>  <td><b>Estado de la inscripci&oacute;n:</b><hr>Admitido provisional:</td><td>&nbsp;<hr>[admitido_provisional]</td>
		</tr>
		<tr>
        <td>Titulo academico:</td><td>[titulo_academico2015]</td>  <td>Admitido definitivo:</td><td>[admitido_definitivo]</td>
		</tr>
		<tr>
        <td>Sexo:</td><td>[sexo]</td>  <td></td><td></td>
		</tr>
		<tr>
        <td>Email:</td><td>[email]</td>  <td></td><td></td>
		</tr>
		<tr>
        <td>Discapacidad:</td><td>[discapacidad]</td>  <td></td><td></td>
		</tr>
		<tr>
        <td>Fecha solicitud:</td><td>[fecha_registro]</td>  <td></td><td></td>
		</tr>
</table>
<br><br>



	  </section>
	</div>
   </div>
