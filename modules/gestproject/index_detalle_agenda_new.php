<?php 
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";

// EXPLICACIONES
// ----------------
// id
// agenda_id
// proveedor_id
// estado_tarea_id
// tarea
// observaciones
// fecha_fin

// si es tarea de cliente => proveedor_id=0
// si es tarea de prov => proveedor_id>0 y estado_tarea_id>0
// si es observacion de prov => proveedor_id>0 y estado_tarea_id=0

// CONFIGURACION
$titulo = "GESTION DE DETALLES DE UNA ENTRADA DE AGENDA";
$titulo_pagina = "DETALLES DE AGENDA";
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_detalle_agenda_new";
$script_agenda = "index_agenda_new";
$script_calendario = "calendario_new";
$tabla = "detalle_agenda_t";
$tabla_padre = "agenda_t";
$registros_por_pagina = 10;
$script_descarga = "";
$tamano_max_archivo = "16000000";
if ($color_entorno != "") { $color_fondo = $color_entorno; $color_fondo_claro = $color_entorno_claro; }
else { $color_fondo = "97c00e"; $color_fondo_claro = "dcfb73"; }

$texto_textarea = '<br type="_moz" />';

echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo "
<table width='100%' height='700' border='0' cellpadding='0' cellspacing='0' class='text' align='center'>
	<tr valign='top'>
		<td width=100% align=left>
";

if (PermisosSecciones($user_id, $script, array()) == 1)
{

// textos de la pagina
$texto_crear = "Crear detalle";
$texto_listado_general = "Listar todos los detalles";
$texto_creado = "Detalle creado";
$texto_modificado = "Detalle modificado";
$texto_borrado = "Detalle borrado";
$nombre_objeto = " UN DETALLE";

// Campos con los que se trabajara en el insert y modify
$campos_col1 = array();
$campos_col2 = array();

// Nombres que apareceran en las columnas de los formularios
$nombres_col1 = array();
$nombres_col2 = array();

// Tipos. Cada campo puede ser:
// text;readonly;size (60)
// password;readonly;size
// textarea;readonly;row;col (10;60)
// select;readonly;tabla;campo_mostrar;campo_para_value;campo_condicion
// checkbox;readonly
// date;readonly
// hidden;tabla;campo_mostrar;campo_para_value;campo_para_order;nuevo_nombre;campo_depende;campo_filtro mostrara un select que saltara y ademas tendra otro campo oculto con el valor
// dni;readonly
// telefono;readonly
// cuenta;readonly
// email;readonly;size
// float;readonly;size
// multiple;readonly;tabla;campo_mostrar;campo_para_value;campo_para_order;nombre_checkbox
// file poner el campo "nombre_fichero", actualiza nombre_fichero, tipo_fichero, peso_fichero, fichero_binario, fecha_subida, user_id
//   Los campos en la tabla a a�adir serian:
//  | nombre_fichero              | varchar(200)
//  | tipo_fichero                | varchar(20)
//  | peso_fichero                | varchar(20)
//  | fecha_subida                | datetime
//  | user_id                     | int(11)
//  | fichero_binario             | blob
// ss;readonly
// calendar;readonly
// time;readonly
// numerico;readonly;size
// fileCarp poner el campo "nombre_fichero", actualiza solo nombre_fichero y sube a la carpeta definida
$tipos_col1  = array();
$tipos_col2  = array();

// Separadores o titulos
$titulos_col1 = array('');
$titulos_col2 = array('');

// INICIO DATOS TAREA PROVEEDOR
$campos_col1_Prov = array(
'proveedor_id','fecha_fin','estado_tarea_id','tarea','observaciones'
);
$campos_col2_Prov = array(
);

$nombres_col1_Prov = array(
'Proveedor','Fecha fin','Estado tarea','Tarea','Observaciones'
);
$nombres_col2_Prov = array(
);

$tipos_col1_Prov = array(
'select;0;proveedores_t;nombre;id;nombre','calendar;0','select;0;maestro_estados_tareas_t;nombre;id;nombre','textarea;0;10;60','textarea;0;10;60'
);
$tipos_col2_Prov = array(
);

$titulos_col1_Prov = array(
'','','','','',
);
$titulos_col2_Prov = array(
);
// FIN DATOS TAREA PROVEEDOR

// INICIO DATOS TAREA CLIENTE
$campos_col1_Cli = array(
'fecha_fin','estado_tarea_id','tarea','observaciones'
);
$campos_col2_Cli = array(
);

$nombres_col1_Cli = array(
'Fecha fin','Estado tarea','Tarea','Observaciones'
);
$nombres_col2_Cli = array(
);

$tipos_col1_Cli = array(
'calendar;0','select;0;maestro_estados_tareas_t;nombre;id;nombre','textarea;0;10;60','textarea;0;10;60'
);
$tipos_col2_Cli = array(
);

$titulos_col1_Cli = array(
'','','',''
);
$titulos_col2_Cli = array(
);
// FIN DATOS TAREA CLIENTE

// INICIO DATOS OBSERVACIONES PROVEEDOR
$campos_col1_Prov_Obs = array(
'proveedor_id','observaciones','prov_bloqueado'
);
//,'fecha_fin'
$campos_col2_Prov_Obs = array(
);

$nombres_col1_Prov_Obs = array(
'Proveedor','Observaciones','Bloqueado'
);
//,'Fecha fin'
$nombres_col2_Prov_Obs = array(
);

$tipos_col1_Prov_Obs = array(
'select;0;proveedores_t;nombre;id;nombre','textarea;0;10;60','checkbox;0'
);
//,'calendar;0'
$tipos_col2_Prov_Obs = array(
);

$titulos_col1_Prov_Obs = array(
'','',''
);
//,''
$titulos_col2_Prov_Obs = array(
);
// FIN DATOS OBSERVACIONES PROVEEDOR

// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('fecha_fin','estado_tarea_id');
// indica la tabla de donde es el campo
$tablas_campos_listado = array($tabla,$tabla);

$nombres_listado = array('Fecha fin','Estado tarea');

$campos_necesarios_listado = array('id','proveedor_id','estado_tarea_id');
$tablas_campos_necesarios = array($tabla,$tabla,$tabla);

// Si es necesaria una decodificacion en el listado
// '' no es necesaria
// si;tabla;campo_mostrar;campo_buscar
// si;date
// si;datetime
// si;checkbox
// si;time
$campos_listado_decod = array ('si;date','si;maestro_estados_tareas_t;nombre;id');

// Campo para la busqueda
$campo_busqueda = "fecha_fin";

// tarea de proveedores
$campos_listado1 = array ('fecha_fin','proveedor_id','estado_tarea_id');
$tablas_campos_listado1 = array($tabla,$tabla,$tabla);
$nombres_listado1 = array('Fecha fin','Proveedor','Estado tarea');

$campos_necesarios_listado1 = array('id','proveedor_id','estado_tarea_id');
$tablas_campos_necesarios1 = array($tabla,$tabla,$tabla);

$campos_listado_decod1 = array ('si;date','si;proveedores_t;nombre;id','si;maestro_estados_tareas_t;nombre;id');

$campo_busqueda1 = "fecha_fin";

// observaciones de proveedores
$campos_listado2 = array ('proveedor_id');
$tablas_campos_listado2 = array($tabla);
$nombres_listado2 = array('Proveedor');

$campos_necesarios_listado2 = array('id','proveedor_id','estado_tarea_id');
$tablas_campos_necesarios2 = array($tabla,$tabla,$tabla);

$campos_listado_decod2 = array ('si;proveedores_t;nombre;id');

$campo_busqueda2 = "id";

// Campo padre
$usa_padre = 1;
$campopadre = "agenda_id";

// Variables del script
$parametros_nombres = array("accion","pag","pag1","pag2",$campopadre);
$parametros_formulario = array("pag","pag1","pag2",$campopadre);
$parametros_filtro = array(); // parametros que estan en el filtro
// Tipos de parametros
//									Sin nada es que no sirven para los filtros o filtro distinto
// texto;campo_filtro							Es un filtro para texto
// texto								Es un filtro para texto, pero hay que desarrollar el filtrado
// select;campo_filtro							Es un filtro con select
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;desde		Es un filtro para fechas superiores a la introducida
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;hasta		Es un filtro para fechas inferiores a la introducida
$parametros_tipos = array("","","");

foreach($parametros_nombres as $indice_parametros => $nombre_param)
{
	list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
	if ($ele == "texto")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; $$nombre_param = str_replace('+',' ',$$nombre_param); }
		else { $$nombre_param = ""; }
	}
	if ($ele == "select")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
	if ($ele == "fecha")
	{
		if ($_REQUEST[$ele3] != "") { $$ele3 = $_REQUEST[$ele3]; }
		else { $$ele3 = ""; }
		if ($_REQUEST[$ele4] != "") { $$ele4 = $_REQUEST[$ele4]; }
		else { $$ele4 = ""; }
		if ($_REQUEST[$ele5] != "") { $$ele5 = $_REQUEST[$ele5]; }
		else { $$ele5 = ""; }
	}
	if ($ele == "")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
}
if ($pag == "") { $pag = "0"; }
if ($pag1 == "") { $pag1 = "0"; }
if ($pag2 == "") { $pag2 = "0"; }

// ACCION CREAR
if ($accion == "accioncrearcli" || $accion == "accioncrearprov" || $accion == "accioncrearprovobs")
{
	// esta variable controlara si se hace el insert o no
	$datos_correctos = 0;
	if ($accion == "accioncrearcli")
	{
		// todos los datos son correctos
		$datos_correctos = 1;
		
		$campos_col1 = $campos_col1_Cli;
		$campos_col2 = $campos_col2_Cli;
		
		$nombres_col1 = $nombres_col1_Cli;
		$nombres_col2 = $nombres_col2_Cli;
		
		$tipos_col1 = $tipos_col1_Cli;
		$tipos_col2 = $tipos_col2_Cli;
		
		$titulos_col1 = $titulos_col1_Cli;
		$titulos_col2 = $titulos_col2_Cli;
	}
	if ($accion == "accioncrearprov")
	{
		if ($_POST['proveedor_id'] > 0)
		{
			// todos los datos son correctos
			$datos_correctos = 1;
		}
		else
		{
			echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo proveedor es obligatorio. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
		}
		
		$campos_col1 = $campos_col1_Prov;
		$campos_col2 = $campos_col2_Prov;
		
		$nombres_col1 = $nombres_col1_Prov;
		$nombres_col2 = $nombres_col2_Prov;
		
		$tipos_col1 = $tipos_col1_Prov;
		$tipos_col2 = $tipos_col2_Prov;
		
		$titulos_col1 = $titulos_col1_Prov;
		$titulos_col2 = $titulos_col2_Prov;
	}
	if ($accion == "accioncrearprovobs")
	{
		if ($_POST['proveedor_id'] > 0)
		{
			// todos los datos son correctos
			$datos_correctos = 1;
		}
		else
		{
			echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo proveedor es obligatorio. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
		}
		
		$campos_col1 = $campos_col1_Prov_Obs;
		$campos_col2 = $campos_col2_Prov_Obs;
		
		$nombres_col1 = $nombres_col1_Prov_Obs;
		$nombres_col2 = $nombres_col2_Prov_Obs;
		
		$tipos_col1 = $tipos_col1_Prov_Obs;
		$tipos_col2 = $tipos_col2_Prov_Obs;
		
		$titulos_col1 = $titulos_col1_Prov_Obs;
		$titulos_col2 = $titulos_col2_Prov_Obs;
	}
//echo "accion=> $accion, campos: ".print_r($campos_col1,true);//.", tipos: ".print_r($tipos_col1,true);
	if ($datos_correctos == 1)
	{
		if ($usa_padre != 0)
		{
			$consulta2 = "insert into $tabla set $campopadre='".$$campopadre."',";
		} else {
			$consulta2 = "insert into $tabla set ";
		}
		if ($accion == "accioncrearcli")
		{
			$consulta2 .= " proveedor_id=0,";
		}
		if ($accion == "accioncrearprovobs")
		{
			$consulta2 .= " estado_tarea_id=0,";
		}
		// Preparamos los campos a insertar
		foreach ($campos_col1 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$cuenta_campos]);
			$nombre_campo = $nombres_col1[$cuenta_campos];
			if ($ele == "date")
			{
				$campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
				if ((($_POST[$campo_fecha_dia] == "00" || $_POST[$campo_fecha_dia] == "") && ($_POST[$campo_fecha_mes] == "00" || $_POST[$campo_fecha_mes] == "") && ($_POST[$campo_fecha_ano] == "0000" || $_POST[$campo_fecha_ano] == "")) || ($_POST[$campo_fecha_dia] > 0 && $_POST[$campo_fecha_dia] < 32 && $_POST[$campo_fecha_mes] > 0 && $_POST[$campo_fecha_mes] < 13 && $_POST[$campo_fecha_ano] > 0))
				{ $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD-MM-AAAA.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "telefono")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg('([0-9]{9})', $_POST[$campo])) { $consulta2 .= " $campo=\"$_POST[$campo]\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo telefonico ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 123456789.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "dni")
			{
				if ($_POST[$campo] != "")
				{
					if (strlen($_POST[$campo]) == 9 && (ereg('([0-9]{8}[a-zA-Z]{1})', $_POST[$campo]) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtoupper($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de DNI ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12345678A o A123456B.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "cuenta")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo"; $campo4 = "cuatro$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "" && $_POST[$campo4] == "") || (strlen($_POST[$campo1]) == 4 && strlen($_POST[$campo2]) == 4 && strlen($_POST[$campo3]) == 2 && strlen($_POST[$campo4]) == 10))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]$_POST[$campo4]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de cuenta corriente ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 1234-5678-90-1234567890.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "email")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg($patron_comprobacion_email, strtolower($_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtolower($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de email ($nombre_campo) se ha introducido con un formato erroneo.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "multiple")
			{
				$array_multiple_ids = array();
				$consulta_elementos = "select * from $ele3;";
				$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
				{
					$array_multiple_ids[] = $linea_elementos[$ele5];
				}
				$valor_campo = "";
				foreach ($array_multiple_ids as $multiple_id)
				{
					$nombre_campo = $ele7.$multiple_id;
					if ($_POST[$nombre_campo] == "on")
					{
						if ($valor_campo != "") { $valor_campo .= ";"; }
						$valor_campo .= $multiple_id;
					}
				}
				$consulta2 .= " $campo=\"$valor_campo\",";
			}
			elseif ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						if ($extension == "jpg" || $extension == "png")
						{
							$binario_peso = $_FILES[$campo]['size']; 
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='#id#slider.$extension',";
							}
						}
						else
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) debe estar en formato jpg o png. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				} // fin if files name != ""
				else
				{
					$consulta2 .= " nombre_fichero='',";
				}
			}
			elseif ($ele == "file")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						$existe = 0;
						if (in_array($extension,$array_ext_excluidas)) { $existe = 1; }
						if ($existe == 1)
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) tiene un formato no permitido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
						else
						{
							$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
							$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
							
							$binario_peso = $_FILES[$campo]['size']; 
							$binario_tipo = $_FILES[$campo]['type']; 
							
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='$nombre_fichero', tipo_fichero='$binario_tipo', peso_fichero='$binario_peso', fichero_binario='$binario_contenido', fecha_subida=now(), user_id='$user_id',";
							}
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				}
			}
			elseif ($ele == "ss")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "") || (strlen($_POST[$campo1]) == 2 && strlen($_POST[$campo2]) == 8 && strlen($_POST[$campo3]) == 2))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de seguridad social ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12-34567890-12.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "calendar")
			{
				if ($_POST[$campo] != "")
				{
					list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
					if ($ele2 != 1)
					{
						if (($dia == 0 && $mes == 0 && $ano == 0) || ($dia != "" && strlen($dia) == 2 && $dia > 0 && $dia < 32 && $mes != "" && strlen($mes) == 2 && $mes > 0 && $mes < 13 && $ano != "" && strlen($ano) == 4 && $ano > 1900))
						{
							$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
						}
						else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD/MM/AAAA.'); history.back(); </SCRIPT>"; exit; }
					}
					else
					{
						$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
					}
				}
			}
			elseif ($ele == "time")
			{
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora == "00" && $valor_minutos == "00") || ($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{ $consulta2 .= " $campo=\"$valor_hora:$valor_minutos\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "textarea") { $consulta2 .= " $campo='".str_replace("'","",str_replace($texto_textarea,"",$_POST[$campo]))."',"; }
			else { $consulta2 .= " $campo='".str_replace("'","",$_POST[$campo])."',"; }
		}
		foreach ($campos_col2 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col2[$cuenta_campos]);
			$nombre_campo = $nombres_col2[$cuenta_campos];
			if ($ele == "date")
			{
				$campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
				if ((($_POST[$campo_fecha_dia] == "00" || $_POST[$campo_fecha_dia] == "") && ($_POST[$campo_fecha_mes] == "00" || $_POST[$campo_fecha_mes] == "") && ($_POST[$campo_fecha_ano] == "0000" || $_POST[$campo_fecha_ano] == "")) || ($_POST[$campo_fecha_dia] > 0 && $_POST[$campo_fecha_dia] < 32 && $_POST[$campo_fecha_mes] > 0 && $_POST[$campo_fecha_mes] < 13 && $_POST[$campo_fecha_ano] > 0))
				{ $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD-MM-AAAA.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "telefono")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg('([0-9]{9})', $_POST[$campo])) { $consulta2 .= " $campo=\"$_POST[$campo]\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo telefonico ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 123456789.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "dni")
			{
				if ($_POST[$campo] != "")
				{
					if (strlen($_POST[$campo]) == 9 && (ereg('([0-9]{8}[a-zA-Z]{1})', $_POST[$campo]) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtoupper($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de DNI ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12345678A o A123456B.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "cuenta")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo"; $campo4 = "cuatro$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "" && $_POST[$campo4] == "") || (strlen($_POST[$campo1]) == 4 && strlen($_POST[$campo2]) == 4 && strlen($_POST[$campo3]) == 2 && strlen($_POST[$campo4]) == 10))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]$_POST[$campo4]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de cuenta corriente ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 1234-5678-90-1234567890.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "email")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg($patron_comprobacion_email, strtolower($_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtolower($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de email ($nombre_campo) se ha introducido con un formato erroneo.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "multiple")
			{
				$array_multiple_ids = array();
				$consulta_elementos = "select * from $ele3;";
				$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
				{
					$array_multiple_ids[] = $linea_elementos[$ele5];
				}
				$valor_campo = "";
				foreach ($array_multiple_ids as $multiple_id)
				{
					$nombre_campo = $ele7.$multiple_id;
					if ($_POST[$nombre_campo] == "on")
					{
						if ($valor_campo != "") { $valor_campo .= ";"; }
						$valor_campo .= $multiple_id;
					}
				}
				$consulta2 .= " $campo=\"$valor_campo\",";
			}
			elseif ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						if ($extension == "jpg" || $extension == "png")
						{
							$binario_peso = $_FILES[$campo]['size']; 
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='#id#slider.$extension',";
							}
						}
						else
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) debe estar en formato jpg o png. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				} // fin if files name != ""
				else
				{
					$consulta2 .= " nombre_fichero='',";
				}
			}
			elseif ($ele == "file")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						$existe = 0;
						if (in_array($extension,$array_ext_excluidas)) { $existe = 1; }
						if ($existe == 1)
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) tiene un formato no permitido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
						else
						{
							$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
							$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
							
							$binario_peso = $_FILES[$campo]['size']; 
							$binario_tipo = $_FILES[$campo]['type']; 
							
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='$nombre_fichero', tipo_fichero='$binario_tipo', peso_fichero='$binario_peso', fichero_binario='$binario_contenido', fecha_subida=now(), user_id='$user_id',";
							}
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				}
			}
			elseif ($ele == "ss")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "") || (strlen($_POST[$campo1]) == 2 && strlen($_POST[$campo2]) == 8 && strlen($_POST[$campo3]) == 2))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de seguridad social ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12-34567890-12.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "calendar")
			{
				if ($_POST[$campo] != "")
				{
					list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
					if ($ele2 != 1)
					{
						if (($dia == 0 && $mes == 0 && $ano == 0) || ($dia != "" && strlen($dia) == 2 && $dia > 0 && $dia < 32 && $mes != "" && strlen($mes) == 2 && $mes > 0 && $mes < 13 && $ano != "" && strlen($ano) == 4 && $ano > 1900))
						{
							$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
						}
						else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD/MM/AAAA.'); history.back(); </SCRIPT>"; exit; }
					}
					else
					{
						$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
					}
				}
			}
			elseif ($ele == "time")
			{
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora == "00" && $valor_minutos == "00") || ($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{ $consulta2 .= " $campo=\"$valor_hora:$valor_minutos\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "textarea") { $consulta2 .= " $campo='".str_replace("'","",str_replace($texto_textarea,"",$_POST[$campo]))."',"; }
			else { $consulta2 .= " $campo='".str_replace("'","",$_POST[$campo])."',"; }
		}
		// Eliminamos la coma final
		$consulta2 = substr($consulta2, 0, strlen($consulta2)-1);
		$consulta2 .= ';';
		//echo "$consulta2<br>";
		
		$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: $consulta2 " . mysql_error());
		
		// RENOMBRAR CORRECTAMENTE LA IMAGEN DE FONDO
		$ultima_coleccion_id = mysql_insert_id();
		foreach ($campos_col1 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$cuenta_campos]);
			if ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$consulta_datos = "select $campo, id from $tabla where id='$ultima_coleccion_id';";
					$resultado_datos = mysql_query($consulta_datos) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_datos = mysql_fetch_array($resultado_datos, MYSQL_ASSOC))
					{
						$nuevo_nombre = str_replace('#id#',$linea_datos['id'],$linea_datos[$campo]);
						
						$consulta_datos2 = "update $tabla set $campo='$nuevo_nombre' where id='$linea_datos[id]';";
						$resultado_datos2 = mysql_query($consulta_datos2) or die("La consulta fall&oacute;: " . mysql_error());
						
						$uploadfile = $uploaddir_contenidos.$nuevo_nombre;
						move_uploaded_file($_FILES[$campo]['tmp_name'], $uploadfile);
					}
				}
			}
		}
		foreach ($campos_col2 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col2[$cuenta_campos]);
			if ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$consulta_datos = "select $campo, id from $tabla where id='$ultima_coleccion_id';";
					$resultado_datos = mysql_query($consulta_datos) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_datos = mysql_fetch_array($resultado_datos, MYSQL_ASSOC))
					{
						$nuevo_nombre = str_replace('#id#',$linea_datos['id'],$linea_datos[$campo]);
						
						$consulta_datos2 = "update $tabla set $campo='$nuevo_nombre' where id='$linea_datos[id]';";
						$resultado_datos2 = mysql_query($consulta_datos2) or die("La consulta fall&oacute;: " . mysql_error());
						
						$uploadfile = $uploaddir_contenidos.$nuevo_nombre;
						move_uploaded_file($_FILES[$campo]['tmp_name'], $uploadfile);
					}
				}
			}
		}
		
		
		if ($accion == "accioncrearprovobs")
		{
			$valor_cliente = "";
			$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
			//echo "$consulta_padre";
			$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
			while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
			{
				$valor_cliente = $linea_padre['cliente_id'];
			}
			$es_potencial = 0;
			$cons = "select * from clientes_prov_potenciales_t where cliente_id='".$valor_cliente."' and proveedor_id='".$_POST['proveedor_id']."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$es_potencial = 1;
			}
			// hay un cambio de estado
			if ($_POST['prov_bloqueado'] == "on")
			{
				// hay que bloquear al proveedor
				if ($es_potencial == 1)
				{
					$cons = "update clientes_prov_potenciales_t set bloqueado='".$_POST['prov_bloqueado']."', detalle_agenda_id='".$ultima_coleccion_id."' where cliente_id='".$valor_cliente."' and proveedor_id='".$_POST['proveedor_id']."';";
					//echo "cons: $cons<br>";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error()); 
				}
				else
				{
					$cons = "update clientes_prov_t set bloqueado='".$_POST['prov_bloqueado']."', detalle_agenda_id='".$ultima_coleccion_id."' where cliente_id='".$valor_cliente."' and proveedor_id='".$_POST['proveedor_id']."';";
					//echo "cons: $cons<br>";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error()); 
				}
			}
		}
		
		echo "<b>$texto_creado</b>";
		$accion = "formcrear";
	}
}
// FIN ACCION CREAR

// ACCION MODIFICAR 
if ($accion == "accionmodificarcli" || $accion == "accionmodificarprov" || $accion == "accionmodificarprovobs")
{
	// esta variable controlara si se hace el insert o no
	$datos_correctos = 0;
	if ($accion == "accionmodificarcli")
	{
		// todos los datos son correctos
		$datos_correctos = 1;
		
		$campos_col1 = $campos_col1_Cli;
		$campos_col2 = $campos_col2_Cli;
		
		$nombres_col1 = $nombres_col1_Cli;
		$nombres_col2 = $nombres_col2_Cli;
		
		$tipos_col1 = $tipos_col1_Cli;
		$tipos_col2 = $tipos_col2_Cli;
		
		$titulos_col1 = $titulos_col1_Cli;
		$titulos_col2 = $titulos_col2_Cli;
	}
	if ($accion == "accionmodificarprov")
	{
		if ($_POST['proveedor_id'] > 0)
		{
			// todos los datos son correctos
			$datos_correctos = 1;
		}
		else
		{
			echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo proveedor es obligatorio. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
		}
		
		$campos_col1 = $campos_col1_Prov;
		$campos_col2 = $campos_col2_Prov;
		
		$nombres_col1 = $nombres_col1_Prov;
		$nombres_col2 = $nombres_col2_Prov;
		
		$tipos_col1 = $tipos_col1_Prov;
		$tipos_col2 = $tipos_col2_Prov;
		
		$titulos_col1 = $titulos_col1_Prov;
		$titulos_col2 = $titulos_col2_Prov;
	}
	$prov_bloqueado_antes = "";
	if ($accion == "accionmodificarprovobs")
	{
		if ($_POST['proveedor_id'] > 0)
		{
			// todos los datos son correctos
			$datos_correctos = 1;
			$cons = "select * from $tabla where id='".$_POST['id']."';";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$prov_bloqueado_antes = $lin['prov_bloqueado'];
			}
		}
		else
		{
			echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo proveedor es obligatorio. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
		}
		
		$campos_col1 = $campos_col1_Prov_Obs;
		$campos_col2 = $campos_col2_Prov_Obs;
		
		$nombres_col1 = $nombres_col1_Prov_Obs;
		$nombres_col2 = $nombres_col2_Prov_Obs;
		
		$tipos_col1 = $tipos_col1_Prov_Obs;
		$tipos_col2 = $tipos_col2_Prov_Obs;
		
		$titulos_col1 = $titulos_col1_Prov_Obs;
		$titulos_col2 = $titulos_col2_Prov_Obs;
	}
//echo "accion=> $accion, campos: ".print_r($campos_col1,true);//.", tipos: ".print_r($tipos_col1,true);
	if ($datos_correctos == 1)
	{
		$consulta2  = "update $tabla set ";
		// Preparamos los campos a insertar
		foreach ($campos_col1 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$cuenta_campos]);
			$nombre_campo = $nombres_col1[$cuenta_campos];
			if ($ele == "date")
			{
				$campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
				if ((($_POST[$campo_fecha_dia] == "00" || $_POST[$campo_fecha_dia] == "") && ($_POST[$campo_fecha_mes] == "00" || $_POST[$campo_fecha_mes] == "") && ($_POST[$campo_fecha_ano] == "0000" || $_POST[$campo_fecha_ano] == "")) || ($_POST[$campo_fecha_dia] > 0 && $_POST[$campo_fecha_dia] < 32 && $_POST[$campo_fecha_mes] > 0 && $_POST[$campo_fecha_mes] < 13 && $_POST[$campo_fecha_ano] > 0))
				{ $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD-MM-AAAA.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "telefono")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg('([0-9]{9})', $_POST[$campo])) { $consulta2 .= " $campo=\"$_POST[$campo]\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo telefonico ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 123456789.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "dni")
			{
				if ($_POST[$campo] != "")
				{
					if (strlen($_POST[$campo]) == 9 && (ereg('([0-9]{8}[a-zA-Z]{1})', $_POST[$campo]) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtoupper($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de DNI ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12345678A o A123456B.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "cuenta")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo"; $campo4 = "cuatro$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "" && $_POST[$campo4] == "") || (strlen($_POST[$campo1]) == 4 && strlen($_POST[$campo2]) == 4 && strlen($_POST[$campo3]) == 2 && strlen($_POST[$campo4]) == 10))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]$_POST[$campo4]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de cuenta corriente ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 1234-5678-90-1234567890.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "email")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg($patron_comprobacion_email, strtolower($_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtolower($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de email ($nombre_campo) se ha introducido con un formato erroneo.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "multiple")
			{
				$array_multiple_ids = array();
				$consulta_elementos = "select * from $ele3;";
				$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
				{
					$array_multiple_ids[] = $linea_elementos[$ele5];
				}
				$valor_campo = "";
				foreach ($array_multiple_ids as $multiple_id)
				{
					$nombre_campo = $ele7.$multiple_id;
					if ($_POST[$nombre_campo] == "on")
					{
						if ($valor_campo != "") { $valor_campo .= ";"; }
						$valor_campo .= $multiple_id;
					}
				}
				$consulta2 .= " $campo=\"$valor_campo\",";
			}
			elseif ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						if ($extension == "jpg" || $extension == "png")
						{
							$binario_peso = $_FILES[$campo]['size']; 
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='#id#slider.$extension',";
							}
						}
						else
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) debe estar en formato jpg o png. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				} // fin if files name != ""
			}
			elseif ($ele == "file")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						$existe = 0;
						if (in_array($extension,$array_ext_excluidas)) { $existe = 1; }
						if ($existe == 1)
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) tiene un formato no permitido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
						else
						{
							$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
							$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
							
							$binario_peso = $_FILES[$campo]['size']; 
							$binario_tipo = $_FILES[$campo]['type']; 
							
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='$nombre_fichero', tipo_fichero='$binario_tipo', peso_fichero='$binario_peso', fichero_binario='$binario_contenido', fecha_subida=now(), user_id='$user_id',";
							}
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				}
			}
			elseif ($ele == "ss")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "") || (strlen($_POST[$campo1]) == 2 && strlen($_POST[$campo2]) == 8 && strlen($_POST[$campo3]) == 2))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de seguridad social ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12-34567890-12.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "calendar")
			{
				if ($_POST[$campo] != "")
				{
					list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
					if ($ele2 != 1)
					{
						if (($dia == 0 && $mes == 0 && $ano == 0) || ($dia != "" && strlen($dia) == 2 && $dia > 0 && $dia < 32 && $mes != "" && strlen($mes) == 2 && $mes > 0 && $mes < 13 && $ano != "" && strlen($ano) == 4 && $ano > 1900))
						{
							$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
						}
						else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD/MM/AAAA.'); history.back(); </SCRIPT>"; exit; }
					}
					else
					{
						$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
					}
				}
			}
			elseif ($ele == "time")
			{
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora == "00" && $valor_minutos == "00") || ($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{ $consulta2 .= " $campo=\"$valor_hora:$valor_minutos\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "textarea") { $consulta2 .= " $campo='".str_replace("'","",str_replace($texto_textarea,"",$_POST[$campo]))."',"; }
			else { $consulta2 .= " $campo='".str_replace("'","",$_POST[$campo])."',"; }
		}
		foreach ($campos_col2 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col2[$cuenta_campos]);
			$nombre_campo = $nombres_col2[$cuenta_campos];
			if ($ele == "date")
			{
				$campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
				if ((($_POST[$campo_fecha_dia] == "00" || $_POST[$campo_fecha_dia] == "") && ($_POST[$campo_fecha_mes] == "00" || $_POST[$campo_fecha_mes] == "") && ($_POST[$campo_fecha_ano] == "0000" || $_POST[$campo_fecha_ano] == "")) || ($_POST[$campo_fecha_dia] > 0 && $_POST[$campo_fecha_dia] < 32 && $_POST[$campo_fecha_mes] > 0 && $_POST[$campo_fecha_mes] < 13 && $_POST[$campo_fecha_ano] > 0))
				{ $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD-MM-AAAA.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "telefono")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg('([0-9]{9})', $_POST[$campo])) { $consulta2 .= " $campo=\"$_POST[$campo]\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo telefonico ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 123456789.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "dni")
			{
				if ($_POST[$campo] != "")
				{
					if (strlen($_POST[$campo]) == 9 && (ereg('([0-9]{8}[a-zA-Z]{1})', $_POST[$campo]) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtoupper($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de DNI ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12345678A o A123456B.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "cuenta")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo"; $campo4 = "cuatro$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "" && $_POST[$campo4] == "") || (strlen($_POST[$campo1]) == 4 && strlen($_POST[$campo2]) == 4 && strlen($_POST[$campo3]) == 2 && strlen($_POST[$campo4]) == 10))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]$_POST[$campo4]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de cuenta corriente ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 1234-5678-90-1234567890.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "email")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg($patron_comprobacion_email, strtolower($_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtolower($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de email ($nombre_campo) se ha introducido con un formato erroneo.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "multiple")
			{
				$array_multiple_ids = array();
				$consulta_elementos = "select * from $ele3;";
				$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
				{
					$array_multiple_ids[] = $linea_elementos[$ele5];
				}
				$valor_campo = "";
				foreach ($array_multiple_ids as $multiple_id)
				{
					$nombre_campo = $ele7.$multiple_id;
					if ($_POST[$nombre_campo] == "on")
					{
						if ($valor_campo != "") { $valor_campo .= ";"; }
						$valor_campo .= $multiple_id;
					}
				}
				$consulta2 .= " $campo=\"$valor_campo\",";
			}
			elseif ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						if ($extension == "jpg" || $extension == "png")
						{
							$binario_peso = $_FILES[$campo]['size']; 
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='#id#slider.$extension',";
							}
						}
						else
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) debe estar en formato jpg o png. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				} // fin if files name != ""
			}
			elseif ($ele == "file")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						$existe = 0;
						if (in_array($extension,$array_ext_excluidas)) { $existe = 1; }
						if ($existe == 1)
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) tiene un formato no permitido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
						else
						{
							$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
							$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
							
							$binario_peso = $_FILES[$campo]['size']; 
							$binario_tipo = $_FILES[$campo]['type']; 
							
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='$nombre_fichero', tipo_fichero='$binario_tipo', peso_fichero='$binario_peso', fichero_binario='$binario_contenido', fecha_subida=now(), user_id='$user_id',";
							}
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				}
			}
			elseif ($ele == "ss")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "") || (strlen($_POST[$campo1]) == 2 && strlen($_POST[$campo2]) == 8 && strlen($_POST[$campo3]) == 2))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de seguridad social ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12-34567890-12.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "calendar")
			{
				if ($_POST[$campo] != "")
				{
					list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
					if ($ele2 != 1)
					{
						if (($dia == 0 && $mes == 0 && $ano == 0) || ($dia != "" && strlen($dia) == 2 && $dia > 0 && $dia < 32 && $mes != "" && strlen($mes) == 2 && $mes > 0 && $mes < 13 && $ano != "" && strlen($ano) == 4 && $ano > 1900))
						{
							$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
						}
						else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD/MM/AAAA.'); history.back(); </SCRIPT>"; exit; }
					}
					else
					{
						$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
					}
				}
			}
			elseif ($ele == "time")
			{
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora == "00" && $valor_minutos == "00") || ($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{ $consulta2 .= " $campo=\"$valor_hora:$valor_minutos\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "textarea") { $consulta2 .= " $campo='".str_replace("'","",str_replace($texto_textarea,"",$_POST[$campo]))."',"; }
			else { $consulta2 .= " $campo='".str_replace("'","",$_POST[$campo])."',"; }
		}
		// Eliminamos la coma final
		$consulta2 = substr($consulta2, 0, strlen($consulta2)-1);
		$consulta2 .= " where id='$_POST[id]';";
		//echo "$consulta2<br>";
	
		$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());
		// RENOMBRAR CORRECTAMENTE LA IMAGEN DE FONDO
		foreach ($campos_col1 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$cuenta_campos]);
			if ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$consulta_datos = "select $campo, id from $tabla where id='$_POST[id]';";
					$resultado_datos = mysql_query($consulta_datos) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_datos = mysql_fetch_array($resultado_datos, MYSQL_ASSOC))
					{
						$nuevo_nombre = str_replace('#id#',$linea_datos['id'],$linea_datos[$campo]);
						
						$consulta_datos2 = "update $tabla set $campo='$nuevo_nombre' where id='$linea_datos[id]';";
						//echo "$consulta_datos2<br>";
						$resultado_datos2 = mysql_query($consulta_datos2) or die("La consulta fall&oacute;: " . mysql_error());
						
						$uploadfile = $uploaddir_contenidos.$nuevo_nombre;
						move_uploaded_file($_FILES[$campo]['tmp_name'], $uploadfile);
					}
				}
			}
		}
		foreach ($campos_col2 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col2[$cuenta_campos]);
			if ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$consulta_datos = "select $campo, id from $tabla where id='$_POST[id]';";
					$resultado_datos = mysql_query($consulta_datos) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_datos = mysql_fetch_array($resultado_datos, MYSQL_ASSOC))
					{
						$nuevo_nombre = str_replace('#id#',$linea_datos['id'],$linea_datos[$campo]);
						
						$consulta_datos2 = "update $tabla set $campo='$nuevo_nombre' where id='$linea_datos[id]';";
						//echo "$consulta_datos2<br>";
						$resultado_datos2 = mysql_query($consulta_datos2) or die("La consulta fall&oacute;: " . mysql_error());
						
						$uploadfile = $uploaddir_contenidos.$nuevo_nombre;
						move_uploaded_file($_FILES[$campo]['tmp_name'], $uploadfile);
					}
				}
			}
		}
		
		
		// bloqueo de un proveedor
		if ($accion == "accionmodificarprovobs")
		{
			if ($_POST['prov_bloqueado'] != $prov_bloqueado_antes)
			{
				$valor_cliente = "";
				$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
				//echo "$consulta_padre";
				$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
				while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
				{
					$valor_cliente = $linea_padre['cliente_id'];
				}
				$es_potencial = 0;
				$cons = "select * from clientes_prov_potenciales_t where cliente_id='".$valor_cliente."' and proveedor_id='".$_POST['proveedor_id']."';";
				$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
				while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
				{
					$es_potencial = 1;
				}
				// hay un cambio de estado
				if ($_POST['prov_bloqueado'] == "on")
				{
					// hay que bloquear al proveedor
					if ($es_potencial == 1)
					{
						$cons = "update clientes_prov_potenciales_t set bloqueado='".$_POST['prov_bloqueado']."', detalle_agenda_id='".$_POST['id']."' where cliente_id='".$valor_cliente."' and proveedor_id='".$_POST['proveedor_id']."';";
						//echo "cons: $cons<br>";
						$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error()); 
					}
					else
					{
						$cons = "update clientes_prov_t set bloqueado='".$_POST['prov_bloqueado']."', detalle_agenda_id='".$_POST['id']."' where cliente_id='".$valor_cliente."' and proveedor_id='".$_POST['proveedor_id']."';";
						//echo "cons: $cons<br>";
						$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error()); 
					}
				}
				else
				{
					// hay que desbloquear al proveedor
					if ($es_potencial == 1)
					{
						$cons = "update clientes_prov_potenciales_t set bloqueado='".$_POST['prov_bloqueado']."', detalle_agenda_id='0' where cliente_id='".$valor_cliente."' and proveedor_id='".$_POST['proveedor_id']."';";
						//echo "cons: $cons<br>";
						$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error()); 
					}
					else
					{
						$cons = "update clientes_prov_t set bloqueado='".$_POST['prov_bloqueado']."', detalle_agenda_id='0' where cliente_id='".$valor_cliente."' and proveedor_id='".$_POST['proveedor_id']."';";
						//echo "cons: $cons<br>";
						$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error()); 
					}
				}
			}
		}
		
		echo "<b>$texto_modificado</b>";
		$accion = "formcrear";
	}
}
// FIN ACCION MODIFICAR 

// ACCION BORRAR
if ($accion == "accionborrar")
{
	$consulta2 = "delete from $tabla where id='$_POST[id]';";
	//echo "$consulta2";
	$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());
	echo "<b>$texto_borrado</b>";
	$accion = "formcrear";
}
// FIN ACCION BORRAR

// COMIENZA EL SCRIPT

if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$nombre_padre = "";
		$fecha_padre = "";
		$hora_inicio_padre = "";
		$hora_fin_padre = "";
		$tipo_padre = "";
		$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
		//echo "$consulta_padre";
		$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
		while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
		{
			$nombre_padre = "<span style='color:#$color_fondo;'>".$linea_padre['nombre']."</span>";
			$fecha_padre = date("d-m-Y",strtotime($linea_padre['fecha']));
			$hora_inicio_padre = substr($linea_padre['hora_inicio'],0,5);
			$hora_fin_padre = substr($linea_padre['hora_fin'],0,5);
			if ($linea_padre['tipo_agenda_id'] > 0)
			{
				$consulta_cat = "select * from maestro_tipos_agenda_t where id='".$linea_padre['tipo_agenda_id']."';";
				$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
				{
					$tipo_padre = $linea_cat['nombre'];
				}
				if ($linea_padre['tipo_agenda_id'] == 1 && $linea_padre['cliente_id'] > 0)
				{
					$consulta_cat = "select * from clientes_t where id='".$linea_padre['cliente_id']."';";
					$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
					{
						$tipo_padre .= " (<span style='color:#$color_fondo;'>$linea_cat[nombre_corto]</span>)";
					}
				}
			}
		}
	}
        
	echo "
<center><b>$titulo $nombre_padre</b><br>";
	echo "
<table align='center'>
	<tr>
		<td><b>Tipo</b> $tipo_padre<br>
<b>Fecha</b> $fecha_padre<br>
<b>Hora inicio</b> $hora_inicio_padre<br>
<b>Hora fin</b> $hora_fin_padre<br>
		</td>
	</tr>
</table>";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				// && $nombre_param != $campopadre
				if ($$nombre_param != "" && $nombre_param != "pag" && $nombre_param != "pag1" && $nombre_param != "pag2" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "[<a href='$enlacevolver"."$script_calendario'>Volver a la agenda</a>]<br>";
	echo "[<a href='$enlacevolver"."$script&accion=formcrearcli&pag=$pag&pag1=$pag1&pag2=$pag2"."$parametros'>Crear tarea de cliente</a>] ";
	echo "[<a href='$enlacevolver"."$script&accion=formcrearprov&pag=$pag&pag1=$pag1&pag2=$pag2"."$parametros'>Crear tarea de proveedor</a>] ";
	echo "[<a href='$enlacevolver"."$script&accion=formcrearprovobs&pag=$pag&pag1=$pag1&pag2=$pag2"."$parametros'>Crear observacion de proveedor</a>] ";
/*
	echo "
	<table width='100%'>
	<form name=form_buscar method=post action='$enlacevolver"."$script'>
	<input type=hidden name=pag value=0>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	echo "
		<tr>
			<td style='text-align:center;'><b>Buscar por nombre</b>: <input type='text' name='b_nombre' value='$b_nombre'>&nbsp;<input type=submit value='Filtrar'></td>
		</tr>
	</form>
	</table>";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "
	<a href='$enlacevolver"."$script&pag=0$parametros'>$texto_listado_general</a>
	<!--
	-->";
*/
	echo "
</center>";
}

// OBTENEMOS EL LISTADO DE REGISTROS 
if ($accion != "formborrar")
{
	// TAREAS DEL CLIENTE (pag, orderby) = > proveedor_id=0
	if ($usa_padre != 0)
	{
		$condiciones = "$tabla.$campopadre='".$$campopadre."'";
		$parametros = "&$campopadre=".$$campopadre;
        }
	else
	{
		$condiciones = "";
		$parametros = "";
	}
	if ($condiciones != "") { $condiciones .= " and "; }
	$condiciones .= "$tabla.proveedor_id=0";
	if ($_REQUEST['orderby1'] != "") { $parametros .= "&orderby1=".$_REQUEST['orderby1']; }
	if ($_REQUEST['orderby2'] != "") { $parametros .= "&orderby2=".$_REQUEST['orderby2']; }
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
		if ($ele == "texto" && $ele2 != "")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2 like '%".$$nombre_param."%' ";
				$$nombre_param = str_replace(' ','+',$$nombre_param); 
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "select")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2='".$$nombre_param."' ";
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "fecha")
		{
			if ($$ele3 != "" && $$ele4 != "" && $$ele5 != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				($ele6 == "desde" ? $condiciones .= " $tabla.$ele2>='".$$ele3."-".$$ele4."-".$$ele5." 00:00:00' " : $condiciones .= " $tabla.$ele2<='".$$ele3."-".$$ele4."-".$$ele5." 23:59:59' ");
				$parametros .= "&$ele3=".$$ele3."&$ele4=".$$ele4."&$ele5=".$$ele5;
			}
		}
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && $nombre_param != $campopadre && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "<center><b>TAREAS DEL CLIENTE</b></center>";
	$string_para_select = "";
	foreach($campos_necesarios_listado as $indice => $campo)
	{
		$string_para_select .= $tablas_campos_necesarios[$indice].".$campo,";
	}
	echo "<table width='100%'>
	<tr bgcolor='#$color_fondo'>";
	$columnas = 0;
	foreach ($campos_listado as $campo) {
		echo "<td><a class='listadoTabla' href='$enlacevolver"."$script&orderby=$campo&pag=$pag$parametros'>$nombres_listado[$columnas]</a></td>";
		$string_para_select .= $tablas_campos_listado[$columnas].".$campo,";
		$columnas++;
	}
	$columnas += 1;
	// Eliminamos el ultimo caracter
	$string_para_select = substr($string_para_select,0,-1);
	echo "<td><font color='#ffffff'><b>Acciones</b></font></td>";
	echo "</tr>";

	$consulta  = "select $string_para_select from $tabla";
	if ($condiciones != "") { $consulta .= " where $condiciones "; }
	// Vemos si existe un orderby
	if ($_REQUEST[orderby] != "")
	{
		$order = " order by $tabla.$_REQUEST[orderby]";
		$parametros .= "&orderby=".$_REQUEST[orderby];
	}
	else { $order = " order by $tabla.$campo_busqueda";  }
	if ($pag != "") {
		// la primera pagina es la 0
		$inicio = $pag*$registros_por_pagina;
		$limit = " limit $inicio,$registros_por_pagina";
	}
	else { $limit = ""; }
	$consulta .= "$order $limit;";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: $consulta " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		if ($_GET[id] != "" && $linea[id] == $_GET[id]) { $bgcolor = " bgcolor='#$color_fondo_claro'"; }
		else { $bgcolor = ""; }
		
		echo "<tr $bgcolor>";
		foreach ($campos_listado as $cuenta_campos => $campo)
		{
			$nombre = "";
			list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod[$cuenta_campos]);
			if ($ele != 'si')
			{
				$nombre = "$linea[$campo]";
			}
			elseif ($ele2 == "date")
			{
				if ($linea[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
				else { $nombre = date("d-m-Y",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "datetime")
			{
				if ($linea[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
				else { $nombre = date("d-m-Y H:i",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "checkbox")
			{
				($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
			}
			elseif ($ele2 == "time")
			{
				list($temp1, $temp2, $temp3) = explode(':',$linea[$campo]);
				$nombre = $temp1.":".$temp2;
			}
			elseif ($linea[$campo] != "")
			{
				$consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre = $lineaselect[$ele3];
				}
			}
			if ($campo == "proveedor_id" && $linea[$campo] == 0)
			{
				$nombre = "<i>Tarea de cliente</i>";
			}
			echo "<td>$nombre</td>";
		}
		echo "<td>";
		echo "
			<a href='$enlacevolver"."$script&accion=";
		if ($linea['proveedor_id'] == 0)
		{
			// tarea de cliente
			echo "formmodificarcli";
		}
		else
		{
			if ($linea['estado_tarea_id'] == 0)
			{
				// observacion de proveedor
				echo "formmodificarprovobs";
			}
			else
			{
				// tarea de proveedor
				echo "formmodificarprov";
			}
		}
		echo "&id=$linea[id]&pag=$pag$parametros'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
		$posibilidad = 0;
		$posibilidad = ComprobarDetalleAgenda($linea['id']);
		if ($posibilidad == 0)
		{
			echo "
			<a href='$enlacevolver"."$script&accion=formborrar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_delete.png' alt='Borrar' title='Borrar' border='0' /></a> ";
		}
		echo "
		</td>";
		echo "</tr>";
		echo "<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas'></td></tr>";
	}
	echo "</table>";
	if ($pag != "")
	{
		$pag_visual = $pag+1;
		// Visualizamos las paginas existentes
		$consulta2  = "select count($tabla.id) as num from $tabla";
		if ($condiciones != "") { $consulta2 .= " where $condiciones;"; } else { $consulta2 .= ";"; }
		$resultado2 = mysql_query($consulta2) or die("$consulta2, La consulta fall&oacute;: " . mysql_error());
		//echo "$consulta2<br>";
		while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC)) {
			$exp = "$linea2[num]";
		}
		if ($exp > $registros_por_pagina)
		{
			echo PaginadoListado($pag, $enlacevolver.$script, $parametros, $exp, $registros_por_pagina, "pag");
		}
	}
	// TAREAS DEL PROVEEDOR (pag1, orderby1) => proveedor_id>0 y estado_tarea_id>0
	if ($usa_padre != 0)
	{
		$condiciones = "$tabla.$campopadre='".$$campopadre."'";
		$parametros = "&$campopadre=".$$campopadre;
        }
	else
	{
		$condiciones = "";
		$parametros = "";
	}
	if ($condiciones != "") { $condiciones .= " and "; }
	$condiciones .= "$tabla.proveedor_id>0 and $tabla.estado_tarea_id>0";
	if ($_REQUEST['orderby'] != "") { $parametros .= "&orderby=".$_REQUEST['orderby']; }
	if ($_REQUEST['orderby2'] != "") { $parametros .= "&orderby2=".$_REQUEST['orderby2']; }
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
		if ($ele == "texto" && $ele2 != "")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2 like '%".$$nombre_param."%' ";
				$$nombre_param = str_replace(' ','+',$$nombre_param); 
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "select")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2='".$$nombre_param."' ";
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "fecha")
		{
			if ($$ele3 != "" && $$ele4 != "" && $$ele5 != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				($ele6 == "desde" ? $condiciones .= " $tabla.$ele2>='".$$ele3."-".$$ele4."-".$$ele5." 00:00:00' " : $condiciones .= " $tabla.$ele2<='".$$ele3."-".$$ele4."-".$$ele5." 23:59:59' ");
				$parametros .= "&$ele3=".$$ele3."&$ele4=".$$ele4."&$ele5=".$$ele5;
			}
		}
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag1" && $nombre_param != $campopadre && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "<center><b>TAREAS DE LOS PROVEEDORES</b></center>";
	$string_para_select1 = "";
	foreach($campos_necesarios_listado1 as $indice => $campo)
	{
		$string_para_select1 .= $tablas_campos_necesarios1[$indice].".$campo,";
	}
	echo "<table width='100%'>
	<tr bgcolor='#$color_fondo'>";
	$columnas1 = 0;
	foreach ($campos_listado1 as $campo) {
		echo "<td><a class='listadoTabla' href='$enlacevolver"."$script&orderby1=$campo&pag=$pag$parametros'>$nombres_listado1[$columnas1]</a></td>";
		$string_para_select1 .= $tablas_campos_listado1[$columnas1].".$campo,";
		$columnas1++;
	}
	$columnas1 += 1;
	// Eliminamos el ultimo caracter
	$string_para_select1 = substr($string_para_select1,0,-1);
	echo "<td><font color='#ffffff'><b>Acciones</b></font></td>";
	echo "</tr>";

	$consulta  = "select $string_para_select1 from $tabla";
	if ($condiciones != "") { $consulta .= " where $condiciones "; }
	// Vemos si existe un orderby
	if ($_REQUEST['orderby1'] != "")
	{
		$order = " order by $tabla.$_REQUEST[orderby1]";
		$parametros .= "&orderby1=".$_REQUEST['orderby1'];
	}
	else { $order = " order by $tabla.$campo_busqueda1";  }
	if ($pag != "") {
		// la primera pagina es la 0
		$inicio = $pag*$registros_por_pagina;
		$limit = " limit $inicio,$registros_por_pagina";
	}
	else { $limit = ""; }
	$consulta .= "$order $limit;";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: $consulta " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		if ($_GET[id] != "" && $linea[id] == $_GET[id]) { $bgcolor = " bgcolor='#$color_fondo_claro'"; }
		else { $bgcolor = ""; }
		
		echo "<tr $bgcolor>";
		foreach ($campos_listado1 as $cuenta_campos => $campo)
		{
			$nombre = "";
			list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod1[$cuenta_campos]);
			if ($ele != 'si')
			{
				$nombre = "$linea[$campo]";
			}
			elseif ($ele2 == "date")
			{
				if ($linea[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
				else { $nombre = date("d-m-Y",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "datetime")
			{
				if ($linea[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
				else { $nombre = date("d-m-Y H:i",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "checkbox")
			{
				($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
			}
			elseif ($ele2 == "time")
			{
				list($temp1, $temp2, $temp3) = explode(':',$linea[$campo]);
				$nombre = $temp1.":".$temp2;
			}
			elseif ($linea[$campo] != "")
			{
				$consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre = $lineaselect[$ele3];
				}
			}
			if ($campo == "proveedor_id" && $linea[$campo] == 0)
			{
				$nombre = "<i>Tarea de cliente</i>";
			}
			echo "<td>$nombre</td>";
		}
		echo "<td>";
		echo "
			<a href='$enlacevolver"."$script&accion=";
		if ($linea['proveedor_id'] == 0)
		{
			// tarea de cliente
			echo "formmodificarcli";
		}
		else
		{
			if ($linea['estado_tarea_id'] == 0)
			{
				// observacion de proveedor
				echo "formmodificarprovobs";
			}
			else
			{
				// tarea de proveedor
				echo "formmodificarprov";
			}
		}
		echo "&id=$linea[id]&pag=$pag$parametros'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
		$posibilidad = 0;
		$posibilidad = ComprobarDetalleAgenda($linea['id']);
		if ($posibilidad == 0)
		{
			echo "
			<a href='$enlacevolver"."$script&accion=formborrar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_delete.png' alt='Borrar' title='Borrar' border='0' /></a> ";
		}
		echo "
		</td>";
		echo "</tr>";
		echo "<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas1'></td></tr>";
	}
	echo "</table>";
	if ($pag1 != "")
	{
		$pag_visual = $pag1+1;
		// Visualizamos las paginas existentes
		$consulta2  = "select count($tabla.id) as num from $tabla";
		if ($condiciones != "") { $consulta2 .= " where $condiciones;"; } else { $consulta2 .= ";"; }
		$resultado2 = mysql_query($consulta2) or die("$consulta2, La consulta fall&oacute;: " . mysql_error());
		//echo "$consulta2<br>";
		while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC)) {
			$exp = "$linea2[num]";
		}
		if ($exp > $registros_por_pagina)
		{
			echo PaginadoListado($pag, $enlacevolver.$script, $parametros, $exp, $registros_por_pagina, "pag1");
		}
	}
	// OBSERVACIONES DE PROVEEDOR (pag3, orderby3) => proveedor_id>0 y estado_tarea_id=0
	if ($usa_padre != 0)
	{
		$condiciones = "$tabla.$campopadre='".$$campopadre."'";
		$parametros = "&$campopadre=".$$campopadre;
        }
	else
	{
		$condiciones = "";
		$parametros = "";
	}
	if ($condiciones != "") { $condiciones .= " and "; }
	$condiciones .= "$tabla.proveedor_id>0 and $tabla.estado_tarea_id=0";
	if ($_REQUEST['orderby'] != "") { $parametros .= "&orderby=".$_REQUEST['orderby']; }
	if ($_REQUEST['orderby1'] != "") { $parametros .= "&orderby1=".$_REQUEST['orderby1']; }
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
		if ($ele == "texto" && $ele2 != "")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2 like '%".$$nombre_param."%' ";
				$$nombre_param = str_replace(' ','+',$$nombre_param); 
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "select")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2='".$$nombre_param."' ";
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "fecha")
		{
			if ($$ele3 != "" && $$ele4 != "" && $$ele5 != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				($ele6 == "desde" ? $condiciones .= " $tabla.$ele2>='".$$ele3."-".$$ele4."-".$$ele5." 00:00:00' " : $condiciones .= " $tabla.$ele2<='".$$ele3."-".$$ele4."-".$$ele5." 23:59:59' ");
				$parametros .= "&$ele3=".$$ele3."&$ele4=".$$ele4."&$ele5=".$$ele5;
			}
		}
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag2" && $nombre_param != $campopadre && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "<center><b>OBSERVACIONES DE LOS PROVEEDORES</b></center>";
	$string_para_select2 = "";
	foreach($campos_necesarios_listado2 as $indice => $campo)
	{
		$string_para_select2 .= $tablas_campos_necesarios2[$indice].".$campo,";
	}
	echo "<table width='100%'>
	<tr bgcolor='#$color_fondo'>";
	$columnas2 = 0;
	foreach ($campos_listado2 as $campo) {
		echo "<td><a class='listadoTabla' href='$enlacevolver"."$script&orderby2=$campo&pag=$pag$parametros'>$nombres_listado2[$columnas2]</a></td>";
		$string_para_select2 .= $tablas_campos_listado2[$columnas2].".$campo,";
		$columnas2++;
	}
	$columnas2 += 1;
	// Eliminamos el ultimo caracter
	$string_para_select2 = substr($string_para_select2,0,-1);
	echo "<td><font color='#ffffff'><b>Acciones</b></font></td>";
	echo "</tr>";

	$consulta  = "select $string_para_select2 from $tabla";
	if ($condiciones != "") { $consulta .= " where $condiciones "; }
	// Vemos si existe un orderby
	if ($_REQUEST['orderby2'] != "")
	{
		$order = " order by $tabla.$_REQUEST[orderby2]";
		$parametros .= "&orderby2=".$_REQUEST['orderby2'];
	}
	else { $order = " order by $tabla.$campo_busqueda2";  }
	if ($pag != "") {
		// la primera pagina es la 0
		$inicio = $pag*$registros_por_pagina;
		$limit = " limit $inicio,$registros_por_pagina";
	}
	else { $limit = ""; }
	$consulta .= "$order $limit;";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: $consulta " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		if ($_GET[id] != "" && $linea[id] == $_GET[id]) { $bgcolor = " bgcolor='#$color_fondo_claro'"; }
		else { $bgcolor = ""; }
		
		echo "<tr $bgcolor>";
		foreach ($campos_listado2 as $cuenta_campos => $campo)
		{
			$nombre = "";
			list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod2[$cuenta_campos]);
			if ($ele != 'si')
			{
				$nombre = "$linea[$campo]";
			}
			elseif ($ele2 == "date")
			{
				if ($linea[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
				else { $nombre = date("d-m-Y",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "datetime")
			{
				if ($linea[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
				else { $nombre = date("d-m-Y H:i",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "checkbox")
			{
				($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
			}
			elseif ($ele2 == "time")
			{
				list($temp1, $temp2, $temp3) = explode(':',$linea[$campo]);
				$nombre = $temp1.":".$temp2;
			}
			elseif ($linea[$campo] != "")
			{
				$consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre = $lineaselect[$ele3];
				}
			}
			if ($campo == "proveedor_id" && $linea[$campo] == 0)
			{
				$nombre = "<i>Tarea de cliente</i>";
			}
			echo "<td>$nombre</td>";
		}
		echo "<td>";
		echo "
			<a href='$enlacevolver"."$script&accion=";
		if ($linea['proveedor_id'] == 0)
		{
			// tarea de cliente
			echo "formmodificarcli";
		}
		else
		{
			if ($linea['estado_tarea_id'] == 0)
			{
				// observacion de proveedor
				echo "formmodificarprovobs";
			}
			else
			{
				// tarea de proveedor
				echo "formmodificarprov";
			}
		}
		echo "&id=$linea[id]&pag=$pag$parametros'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
		$posibilidad = 0;
		$posibilidad = ComprobarDetalleAgenda($linea['id']);
		if ($posibilidad == 0)
		{
			echo "
			<a href='$enlacevolver"."$script&accion=formborrar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_delete.png' alt='Borrar' title='Borrar' border='0' /></a> ";
		}
		echo "
		</td>";
		echo "</tr>";
		echo "<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas2'></td></tr>";
	}
	echo "</table>";
	if ($pag2 != "")
	{
		$pag_visual = $pag2+1;
		// Visualizamos las paginas existentes
		$consulta2  = "select count($tabla.id) as num from $tabla";
		if ($condiciones != "") { $consulta2 .= " where $condiciones;"; } else { $consulta2 .= ";"; }
		$resultado2 = mysql_query($consulta2) or die("$consulta2, La consulta fall&oacute;: " . mysql_error());
		//echo "$consulta2<br>";
		while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC)) {
			$exp = "$linea2[num]";
		}
		if ($exp > $registros_por_pagina)
		{
			echo PaginadoListado($pag, $enlacevolver.$script, $parametros, $exp, $registros_por_pagina, "pag2");
		}
	}
	echo "<hr>";
}
// FIN DE OBTENER EL LISTADO INICIAL

// FORMULARIO PARA BORRAR UN REGISTRO
if ($accion == "formborrar")
{
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "") { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "") { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "") { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { $parametros .= "&orderby=".$_REQUEST[orderby]; }
	echo "<table width='100%'>
	<tr>
		<td><center><b>BORRADO DE $nombre_objeto</b> [<a href='$enlacevolver"."$script$parametros'>Volver sin cambios</a>]</center></td>
	</tr>
	<tr valign='top'>
		<td>
		<form name=form_buscar method=post action='$enlacevolver"."$script'>
		<input type=hidden name=accion value=accionborrar>
		<input type=hidden name=id value=$_GET[id]>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "") { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "") { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "") { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { echo "<input type=hidden name=orderby value='".$_REQUEST[orderby]."'>"; }
	// Obtenemos los valores actuales del registro que se esta modificando
	$consultamod = "select * from $tabla where id=$_GET[id];";
	$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: " . mysql_error());
	// El resultado lo metemos en un array asociativo
	$arraymod = array();
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC)) {
		foreach ($campos_col1 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
		foreach ($campos_col2 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
	} // del while

	echo "
		<table width='100%' border='0'>
			<tr><td colspan='2'><input type=submit value='Va usted a borrar el registro con los siguientes datos'></td></tr>
			<tr><td width='50%'><table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col1 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col1[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col1[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col1[$cuenta_campos]);
		$nombre_campo = $nombres_col1[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: $dia M: $mes A: $ano</td></tr>";
		}
		if ($ele == "checkbox") { echo "<tr><td><b>$nombre_campo</b></td><td>";if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; } echo "</td></tr>"; }
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele3 where $ele5='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele4]";
				}
			}
			echo "</td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele2 where $ele4='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele3]";
				}
			}
			echo "</td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3 - $valor4</td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "email") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "float") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
				echo "\n<td align='center' width='5%'>$nombre_elemento</td>";
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			if ($arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			$id_codificado = base64_encode($_GET[id]);
			echo "Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3</td></tr>";
		}
		if ($ele == "calendar")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>$dia/$mes/$ano</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>$hora : $minutos</td></tr>";
		}
		if ($ele == "numerico") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		$cuenta_campos++;
		echo "\n";
	} // del foreach
	echo "</table>";

	// Vamos a por la columna 2
	echo "</td><td>";

	echo "<table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col2 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col2[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col2[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col2[$cuenta_campos]);
		$nombre_campo = $nombres_col2[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: $dia M: $mes A: $ano</td></tr>";
		}
		if ($ele == "checkbox") { echo "<tr><td><b>$nombre_campo</b></td><td>";if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; } echo "</td></tr>"; }
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele3 where $ele5='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele4]";
				}
			}
			echo "</td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele2 where $ele4='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele3]";
				}
			}
			echo "</td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3 - $valor4</td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "email") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "float") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
				echo "\n<td align='center' width='5%'>$nombre_elemento</td>";
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			if ($arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			$id_codificado = base64_encode($_GET[id]);
			echo "Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3</td></tr>";
		}
		if ($ele == "calendar")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>$dia/$mes/$ano</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>$hora : $minutos</td></tr>";
		}
		if ($ele == "numerico") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		$cuenta_campos++;
		echo "\n";
	} // del foreach
	echo "</table>";
	echo "</td></tr></table>";
	echo "</form></td></tr></table>";
}
// FIN FORMULARIO BORRAR

// FORMULARIO PARA LA CREACION/MODIFICACION DE UN NUEVO REGISTRO

if ($accion == "formcrearcli" || $accion == "formmodificarcli" || $accion == "formcrearprov" || $accion == "formmodificarprov" || $accion == "formcrearprovobs" || $accion == "formmodificarprovobs")
{
	if ($accion == "formcrearcli" || $accion == "formmodificarcli")
	{
		$campos_col1 = $campos_col1_Cli;
		$campos_col2 = $campos_col2_Cli;
		
		$nombres_col1 = $nombres_col1_Cli;
		$nombres_col2 = $nombres_col2_Cli;
		
		$tipos_col1 = $tipos_col1_Cli;
		$tipos_col2 = $tipos_col2_Cli;
		
		$titulos_col1 = $titulos_col1_Cli;
		$titulos_col2 = $titulos_col2_Cli;
	}
	if ($accion == "formcrearprov" || $accion == "formmodificarprov")
	{
		$campos_col1 = $campos_col1_Prov;
		$campos_col2 = $campos_col2_Prov;
		
		$nombres_col1 = $nombres_col1_Prov;
		$nombres_col2 = $nombres_col2_Prov;
		
		$tipos_col1 = $tipos_col1_Prov;
		$tipos_col2 = $tipos_col2_Prov;
		
		$titulos_col1 = $titulos_col1_Prov;
		$titulos_col2 = $titulos_col2_Prov;
	}
	if ($accion == "formcrearprovobs" || $accion == "formmodificarprovobs")
	{
		$campos_col1 = $campos_col1_Prov_Obs;
		$campos_col2 = $campos_col2_Prov_Obs;
		
		$nombres_col1 = $nombres_col1_Prov_Obs;
		$nombres_col2 = $nombres_col2_Prov_Obs;
		
		$tipos_col1 = $tipos_col1_Prov_Obs;
		$tipos_col2 = $tipos_col2_Prov_Obs;
		
		$titulos_col1 = $titulos_col1_Prov_Obs;
		$titulos_col2 = $titulos_col2_Prov_Obs;
	}
//echo "accion=> $accion, campos: ".print_r($campos_col1,true);//.", tipos: ".print_r($tipos_col1,true);

	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "") { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "") { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "") { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { $parametros .= "&orderby=".$_REQUEST[orderby]; }
	echo "<table width='100%'>
	<tr>
		<td colspan='2'><center><b>";
	if ($accion == "formcrearcli" || $accion == "formcrearprov" || $accion == "formcrearprovobs") { echo "CREACION"; }
	elseif ($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs") { echo "MODIFICACION"; }
	echo " DE ";
	if ($accion == "formcrearcli" || $accion == "formmodificarcli") { echo "UNA TAREA DEL CLIENTE"; }
	elseif ($accion == "formcrearprov" || $accion == "formmodificarprov") { echo "UNA TAREA DEL PROVEEDOR"; }
	elseif ($accion == "formcrearprovobs" || $accion == "formmodificarprovobs") { echo "UNA OBSERVACION DEL PROVEEDOR"; }
	echo "</b> [<a href='$enlacevolver"."$script$parametros'>Volver sin cambios</a>]</center></td>
	</tr>
	<form enctype='multipart/form-data' name=form_crear method=post action='$enlacevolver"."$script'>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "") { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "") { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "") { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { echo "<input type=hidden name=orderby value='".$_REQUEST[orderby]."'>"; }
	echo "
	<tr valign='top'>
		<td>";
	echo "<img src=images/p.jpg onload=document.form_crear.".$campos_col1[0].".focus();>";
	$arraymod = array();
	if ($accion == "formcrearcli") {
		echo "<input type=hidden name=accion value=accioncrearcli>";
	}
	if ($accion == "formcrearprov") {
		echo "<input type=hidden name=accion value=accioncrearprov>";
	}
	if ($accion == "formcrearprovobs") {
		echo "<input type=hidden name=accion value=accioncrearprovobs>";
	}
	if ($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs")
	{
		echo "<input type=hidden name=id value='$_GET[id]'>";
		if ($accion == "formmodificarcli")
		{
			echo "<input type=hidden name=accion value=accionmodificarcli>";
		}
		if ($accion == "formmodificarprov")
		{
			echo "<input type=hidden name=accion value=accionmodificarprov>";
		}
		if ($accion == "formmodificarprovobs")
		{
			echo "<input type=hidden name=accion value=accionmodificarprovobs>";
		}
		// Obtenemos los valores actuales del registro que se esta modificando
		$string_para_select = "";
		foreach($campos_col1 as $campo)
		{
			if ($string_para_select != "") { $string_para_select .= ", "; }
			$string_para_select .= "$campo";
		}
		foreach($campos_col2 as $campo)
		{
			if ($string_para_select != "") { $string_para_select .= ", "; }
			$string_para_select .= "$campo";
		}
		$consultamod = "select $string_para_select from $tabla where id=$_GET[id];";
		//echo "$consultamod<br>";
		$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: $consultamod " . mysql_error());
		// El resultado lo metemos en un array asociativo
		while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC)) {
			foreach ($campos_col1 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
			foreach ($campos_col2 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
		} // del while
	}

	$cuenta_campos = 0;
	foreach($campos_col1 as $campo)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $tipos_col1[$cuenta_campos]);
		if ($ele == "hidden" && $_GET[$ele6] != "") { $arraymod[$campo] = $_GET[$ele6]; }
		$cuenta_campos++;
	}
	$cuenta_campos = 0;
	foreach($campos_col2 as $campo)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $tipos_col2[$cuenta_campos]);
		if ($ele == "hidden" && $_GET[$ele6] != "") { $arraymod[$campo] = $_GET[$ele6]; }
		$cuenta_campos++;
	}

	echo "<table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col1 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col1[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col1[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col1[$cuenta_campos]);
		$nombre_campo = $nombres_col1[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1 && ($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs")) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) {echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($ele2 == 1 && ($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs"))
			{
				echo "$arraymod[$campo]<input type=hidden name='$campo' value='$arraymod[$campo]'>";
			}
			else
			{
				echo "<textarea rows='$ele3' cols='$ele4' name='$campo'>$arraymod[$campo]</textarea>";
			}
			echo "</td></tr>";
		}
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: <input type='text' name='dia$campo' size='2' maxlength='2' value='$dia' onkeyup='validar(dia$campo);'"; if ($ele2 == 1) { echo " readonly"; }
			echo "> M: <input type='text' name='mes$campo' size='2' maxlength='2' value='$mes' onkeyup='validar(mes$campo);'"; if ($ele2 == 1) { echo " readonly"; }
			echo "> A: <input type='text' name='ano$campo' size='4' maxlength='4' value='$ano' onkeyup='validar(ano$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "checkbox")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($ele2 == 1)
			{
				if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; }
				echo "<input type=hidden name='$campo' value='$arraymod[$campo]'>";
			}
			else
			{
				echo "<input type=checkbox name='$campo'"; if ($arraymod[$campo] == "on") { echo " checked"; } echo ">";
			}
			echo "</td></tr>";
		}
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if (($accion == "formcrearcli" || $accion == "formcrearprov" || $accion == "formcrearprovobs") || (($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs") && $ele2 != 1))
			{
				$condi_select = "";
				if ($ele7 != "") { $condi_select .= " where $ele7"; }
				if ($ele6 != "") { $consultaselect = "select * from $ele3 $condi_select order by $ele6;"; }
				else { $consultaselect = "select * from $ele3 $condi_select;"; }
			}
			else
			{
				if ($ele2 == 1)
				{
					$consultaselect = "select * from $ele3 where $ele3.$ele5='$arraymod[$campo]';";
				}
			}
			if ($campo == "proveedor_id")
			{
				$join_select = " join clientes_prov_t on clientes_prov_t.proveedor_id=$ele3.id join $tabla_padre on $tabla_padre.cliente_id=clientes_prov_t.cliente_id ";
				$group_select = " group by $ele3.id ";
				$condi_select = "$tabla_padre.id='".$$campopadre."'";
				if ($ele7 != "") { if ($condi_select != "") { $condi_select .= " and "; } $condi_select .= " $ele3.$ele7"; }
				if ($accion == "formcrearprov" || $accion == "formcrearprovobs")
				{
					if ($condi_select != "") { $condi_select .= " and "; } $condi_select .= " clientes_prov_t.bloqueado=''";
				}
				// condicion inactivo=''
				if ($accion == "formmodificarprov" || $accion == "formmodificarprovobs")
				{
					if ($condi_select != "") { $condi_select .= " and "; }
					$condi_select .= "($ele3.inactivo='' or $ele3.id='".$arraymod[$campo]."')";
				}
				else
				{
					if ($condi_select != "") { $condi_select .= " and "; }
					$condi_select .= "$ele3.inactivo=''";
				}
				if ($condi_select != "") { $condi_select = " where ".$condi_select; }
				if ($ele6 != "") { $consultaselect = "select $ele3.* from $ele3 $join_select $condi_select $group_select order by $ele3.$ele6;"; }
				else { $consultaselect = "select $ele3.* from $ele3 $join_select $condi_select $group_select;"; }
				//echo "$consultaselect<br>";
			}
			echo "<select name='$campo' id='$campo'>";
			if (!(($accion == "formcrearprov" || $accion == "formmodificarprov" || $accion == "formcrearprovobs" || $accion == "formmodificarprovobs") && ($campo == "proveedor_id" || $campo == "estado_tarea_id"))) { echo "<option value='0'></option>"; }
			//echo "<option>$consultaselect</option>";
			$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: $consultaselect " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
				echo "<option value='$lineaselect[$ele5]'"; if ($lineaselect[$ele5] == "$arraymod[$campo]") { echo " selected"; } echo ">$lineaselect[$ele4]</option>";
			}
			if ($campo == "proveedor_id")
			{
				$join_select = " join clientes_prov_potenciales_t on clientes_prov_potenciales_t.proveedor_id=$ele3.id join $tabla_padre on $tabla_padre.cliente_id=clientes_prov_potenciales_t.cliente_id ";
				$group_select = " group by $ele3.id ";
				$condi_select = "$tabla_padre.id='".$$campopadre."'";
				if ($ele7 != "") { if ($condi_select != "") { $condi_select .= " and "; } $condi_select .= " $ele3.$ele7"; }
				if ($accion == "formcrearprov" || $accion == "formcrearprovobs")
				{
					if ($condi_select != "") { $condi_select .= " and "; } $condi_select .= " clientes_prov_potenciales_t.bloqueado=''";
				}
				if ($condi_select != "") { $condi_select = " where ".$condi_select; }
				if ($ele6 != "") { $consultaselect = "select $ele3.* from $ele3 $join_select $condi_select $group_select order by $ele3.$ele6;"; }
				else { $consultaselect = "select $ele3.* from $ele3 $join_select $condi_select $group_select;"; }
				//echo "<option>$consultaselect</option>";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: $consultaselect " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "<option value='$lineaselect[$ele5]'"; if ($lineaselect[$ele5] == "$arraymod[$campo]") { echo " selected"; } echo ">$lineaselect[$ele4]</option>";
				}
			}
			echo "</select></td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>
			<select name='$ele6' onchange=\"saltoPagina('parent',this,0)\"><option value='$enlacevolver"."$script&accion=$accion&pag=$pag&$ele6=0";
			if ($usa_padre != 0)
			{
				$parametros_hidden = "&$campopadre=".$$campopadre;
			}
			else
			{
				$parametros_hidden = "";
			}
			if ($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs") { echo "&id=$_GET[id]"; }
			$cuenta_hidden = 0;
			foreach ($campos_col1 as $valor_campo) {
				list ($valor, $valor2, $valor3, $valor4, $valor5, $valor6) = explode(';', $tipos_col1[$cuenta_hidden]);
				if ($valor == "hidden" && $valor_campo != $campo) { $parametros_hidden .= "&$valor6=$arraymod[$valor_campo]"; }
				$cuenta_hidden++;
			}
			$cuenta_hidden = 0;
			foreach ($campos_col2 as $valor_campo) {
				list ($valor, $valor2, $valor3, $valor4, $valor5, $valor6) = explode(';', $tipos_col2[$cuenta_hidden]);
				if ($valor == "hidden" && $valor_campo != $campo) { $parametros_hidden .= "&$valor6=$arraymod[$valor_campo]"; }
				$cuenta_hidden++;
			}
			echo "$parametros_hidden'></option>";
			if ($ele7 != "")
			{
				$valor_filtro = ""; $encontrado = 0;
				while ($encontrado == 0)
				{
					foreach ($campos_col1 as $valor_campo) { if ($valor_campo == $ele7) { $encontrado = 1; $valor_filtro = $arraymod[$valor_campo]; } }
				}
				while ($encontrado == 0)
				{
					foreach ($campos_col2 as $valor_campo) { if ($valor_campo == $ele7) { $encontrado = 1; $valor_filtro = $arraymod[$valor_campo]; } }
				}
				$consultaselect = "select $ele2.* from $ele2 where $ele8='$valor_filtro' order by $ele5;";
			}
			else 
			{
				if ($ele5 != "") { $consultaselect = "select * from $ele2 order by $ele5;"; }
				else { $consultaselect = "select * from $ele2;"; }
			}
			//echo "<option>$consultaselect</option>";
			$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
				echo "<option value='$enlacevolver"."$script&accion=$accion&pag=$pag&$ele6=$lineaselect[$ele4]$parametros_hidden";
				if ($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs") { echo "&id=$_GET[id]'"; } else { echo "'"; }
				if ($lineaselect[$ele4] == $arraymod[$campo]) { echo " selected"; }
				echo ">$lineaselect[$ele3]</option>";
			}
			echo "</select><input type=hidden name=$campo value='$arraymod[$campo]'></td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>
		<input type='text' name='uno$campo' size='4' maxlength='4' value='$valor1' onkeyup='validar(uno$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='dos$campo' size='4' maxlength='4' value='$valor2' onkeyup='validar(dos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='tres$campo' size='2' maxlength='2' value='$valor3' onkeyup='validar(tres$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='cuatro$campo' size='10' maxlength='10' value='$valor4' onkeyup='validar(cuatro$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> <img src='images/money.png'></td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='9' maxlength='9' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='9' maxlength='9' value='$arraymod[$campo]' onkeyup='validar($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "email")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo ">";
			if ($arraymod[$campo] != "") { echo "&nbsp;&nbsp;<a href='mailto:$arraymod[$campo]'><img src='images/email.png' border='0' title='Enviar correo'></a>"; }
			echo "</td></tr>";
		}
		if ($ele == "float")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]' onkeyup='validarFloat($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> (Ej: 1234.56)</td></tr>";
		}
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				if ($ele2 == 1) // no modificable
				{
					($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
					echo "\n<td align='center' width='5%'>$nombre_elemento
					<input type=hidden name='$ele7$linea_elementos[$ele5]' value='".$array_multiple_valor[$linea_elementos[$ele5]]."'></td>";
				}
				else
				{
					echo "\n<td align='center' width='5%'><input type='checkbox' name='$ele7$linea_elementos[$ele5]'";
					if ($array_multiple_valor[$linea_elementos[$ele5]] == "on") { echo " checked"; }
					echo "></td>";
				}
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td><input type=hidden name=MAX_FILE_SIZE value='$tamano_max_archivo' /><input name='$campo' type=file /> <img src='images/camera.png'><br><span syle='font-size:10px;'>Tama&ntilde;o m&aacute;ximo: ".round($tamano_max_archivo/1024,2)." Kb</span><br>";
			if (($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs") && $arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td><input type=hidden name=MAX_FILE_SIZE value='$tamano_max_archivo' /><input name='$campo' type=file />";
			if (($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs") && $arraymod[$campo] != "")
			{
				$id_codificado = base64_encode($_GET[id]);
				echo "<br>Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>
		<input type='text' name='uno$campo' size='2' maxlength='2' value='$valor1' onkeyup='validar(uno$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='dos$campo' size='8' maxlength='8' value='$valor2' onkeyup='validar(dos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='tres$campo' size='2' maxlength='2' value='$valor3' onkeyup='validar(tres$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "calendar")
		{
			if ($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs") { list($fecha, $reloj) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha); }
			else { $ano = "0000"; $mes = "00"; $dia = "00"; }
			echo "<tr><td><b>$nombre_campo</b></td><td>
<input type='text' name='$campo' id='$campo'"; if ($ele2 == 1) { echo " readonly='1'"; } echo " size='10' value='$dia/$mes/$ano'/>";
			if ($ele2 != 1)
			{
				echo "
<img src='images/calendar.gif' name='boton$campo' border='0' id='boton$campo' style='cursor: pointer; border: 0px solid blue;' title='Seleccione fecha entrada' />
<script type='text/javascript'>
	Calendar.setup({
		inputField	:	'$campo',		// id of the input field
		trigger		:	'boton$campo',	// trigger for the calendar (button ID)
		onSelect	:	function() { this.hide() }
	});
</script>";
			}
			echo "</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>
<input type='text' name='hora$campo' size='2' maxlength='2' value='$hora' onkeyup='validar(hora$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> : 
<input type='text' name='minutos$campo' size='2' maxlength='2' value='$minutos' onkeyup='validar(minutos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "numerico")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]' onkeyup='validar($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		$cuenta_campos++;
		echo "\n";
	} // del foreach
	echo "</table>";

	// Vamos a por la columna 2
	echo "</td><td>";

	echo "<table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col2 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col2[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col2[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col2[$cuenta_campos]);
		$nombre_campo = $nombres_col2[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1 && ($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs")) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) {echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($ele2 == 1 && ($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs"))
			{
				echo "$arraymod[$campo]<input type=hidden name='$campo' value='$arraymod[$campo]'>";
			}
			else
			{
				echo "<textarea rows='$ele3' cols='$ele4' name='$campo'>$arraymod[$campo]</textarea>";
			}
			echo "</td></tr>";
		}
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: <input type='text' name='dia$campo' size='2' maxlength='2' value='$dia' onkeyup='validar(dia$campo);'"; if ($ele2 == 1) { echo " readonly"; }
			echo "> M: <input type='text' name='mes$campo' size='2' maxlength='2' value='$mes' onkeyup='validar(mes$campo);'"; if ($ele2 == 1) { echo " readonly"; }
			echo "> A: <input type='text' name='ano$campo' size='4' maxlength='4' value='$ano' onkeyup='validar(ano$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "checkbox")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($ele2 == 1)
			{
				if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; }
				echo "<input type=hidden name='$campo' value='$arraymod[$campo]'>";
			}
			else
			{
				echo "<input type=checkbox name='$campo'"; if ($arraymod[$campo] == "on") { echo " checked"; } echo ">";
			}
			echo "</td></tr>";
		}
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><select name='$campo' id='$campo'>";
			echo "<option value='0'></option>";
			if (($accion == "formcrearcli" || $accion == "formcrearprov" || $accion == "formcrearprovobs") || (($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs") && $ele2 != 1))
			{
				if ($ele6 != "") { $consultaselect = "select * from $ele3 order by $ele6;"; }
				else { $consultaselect = "select * from $ele3;"; }
			}
			else
			{
				if ($ele2 == 1)
				{
					$consultaselect = "select * from $ele3 where $ele3.$ele5='$arraymod[$campo]';";
				}
			}
			$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
				echo "<option value='$lineaselect[$ele5]'"; if ($lineaselect[$ele5] == "$arraymod[$campo]") { echo " selected"; } echo ">$lineaselect[$ele4]</option>";
			}
			echo "</select></td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>
			<select name='$ele6' onchange=\"saltoPagina('parent',this,0)\"><option value='$enlacevolver"."$script&accion=$accion&pag=$pag&$ele6=0";
			if ($usa_padre != 0)
			{
				$parametros_hidden = "&$campopadre=".$$campopadre;
			}
			else
			{
				$parametros_hidden = "";
			}
			if ($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs") { echo "&id=$_GET[id]"; }
			$cuenta_hidden = 0;
			foreach ($campos_col1 as $valor_campo) {
				list ($valor, $valor2, $valor3, $valor4, $valor5, $valor6) = explode(';', $tipos_col1[$cuenta_hidden]);
				if ($valor == "hidden" && $valor_campo != $campo) { $parametros_hidden .= "&$valor6=$arraymod[$valor_campo]"; }
				$cuenta_hidden++;
			}
			$cuenta_hidden = 0;
			foreach ($campos_col2 as $valor_campo) {
				list ($valor, $valor2, $valor3, $valor4, $valor5, $valor6) = explode(';', $tipos_col2[$cuenta_hidden]);
				if ($valor == "hidden" && $valor_campo != $campo) { $parametros_hidden .= "&$valor6=$arraymod[$valor_campo]"; }
				$cuenta_hidden++;
			}
			echo "$parametros_hidden'></option>";
			if ($ele7 != "")
			{
				$valor_filtro = ""; $encontrado = 0;
				while ($encontrado == 0)
				{
					foreach ($campos_col1 as $valor_campo) { if ($valor_campo == $ele7) { $encontrado = 1; $valor_filtro = $arraymod[$valor_campo]; } }
				}
				while ($encontrado == 0)
				{
					foreach ($campos_col2 as $valor_campo) { if ($valor_campo == $ele7) { $encontrado = 1; $valor_filtro = $arraymod[$valor_campo]; } }
				}
				$consultaselect = "select $ele2.* from $ele2 where $ele8='$valor_filtro' order by $ele5;";
			}
			else 
			{
				if ($ele5 != "") { $consultaselect = "select * from $ele2 order by $ele5;"; }
				else { $consultaselect = "select * from $ele2;"; }
			}
			//echo "<option>$consultaselect</option>";
			$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
				echo "<option value='$enlacevolver"."$script&accion=$accion&pag=$pag&$ele6=$lineaselect[$ele4]$parametros_hidden";
				if ($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs") { echo "&id=$_GET[id]'"; } else { echo "'"; }
				if ($lineaselect[$ele4] == $arraymod[$campo]) { echo " selected"; }
				echo ">$lineaselect[$ele3]</option>";
			}
			echo "</select><input type=hidden name=$campo value='$arraymod[$campo]'></td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>
		<input type='text' name='uno$campo' size='4' maxlength='4' value='$valor1' onkeyup='validar(uno$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='dos$campo' size='4' maxlength='4' value='$valor2' onkeyup='validar(dos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='tres$campo' size='2' maxlength='2' value='$valor3' onkeyup='validar(tres$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='cuatro$campo' size='10' maxlength='10' value='$valor4' onkeyup='validar(cuatro$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> <img src='images/money.png'></td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='9' maxlength='9' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='9' maxlength='9' value='$arraymod[$campo]' onkeyup='validar($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "email")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo ">";
			if ($arraymod[$campo] != "") { echo "&nbsp;&nbsp;<a href='mailto:$arraymod[$campo]'><img src='images/email.png' border='0' title='Enviar correo'></a>"; }
			echo "</td></tr>";
		}
		if ($ele == "float")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]' onkeyup='validarFloat($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> (Ej: 1234.56)</td></tr>";
		}
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				if ($ele2 == 1) // no modificable
				{
					($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
					echo "\n<td align='center' width='5%'>$nombre_elemento
					<input type=hidden name='$ele7$linea_elementos[$ele5]' value='".$array_multiple_valor[$linea_elementos[$ele5]]."'></td>";
				}
				else
				{
					echo "\n<td align='center' width='5%'><input type='checkbox' name='$ele7$linea_elementos[$ele5]'";
					if ($array_multiple_valor[$linea_elementos[$ele5]] == "on") { echo " checked"; }
					echo "></td>";
				}
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td><input type=hidden name=MAX_FILE_SIZE value='$tamano_max_archivo' /><input name='$campo' type=file /> <img src='images/camera.png'><br><span syle='font-size:10px;'>Tama&ntilde;o m&aacute;ximo: ".round($tamano_max_archivo/1024,2)." Kb</span><br>";
			if (($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs") && $arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td><input type=hidden name=MAX_FILE_SIZE value='$tamano_max_archivo' /><input name='$campo' type=file />";
			if (($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs") && $arraymod[$campo] != "")
			{
				$id_codificado = base64_encode($_GET[id]);
				echo "<br>Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>
		<input type='text' name='uno$campo' size='2' maxlength='2' value='$valor1' onkeyup='validar(uno$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='dos$campo' size='8' maxlength='8' value='$valor2' onkeyup='validar(dos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='tres$campo' size='2' maxlength='2' value='$valor3' onkeyup='validar(tres$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "calendar")
		{
			if ($accion == "formmodificarcli" || $accion == "formmodificarprov" || $accion == "formmodificarprovobs") { list($fecha, $reloj) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha); }
			else { $ano = "0000"; $mes = "00"; $dia = "00"; }
			echo "<tr><td><b>$nombre_campo</b></td><td>
<input type='text' name='$campo' id='$campo'"; if ($ele2 == 1) { echo " readonly='1'"; } echo " size='10' value='$dia/$mes/$ano'/>";
			if ($ele2 != 1)
			{
				echo "
<img src='images/calendar.gif' name='boton$campo' border='0' id='boton$campo' style='cursor: pointer; border: 0px solid blue;' title='Seleccione fecha entrada' />
<script type='text/javascript'>
	Calendar.setup({
		inputField	:	'$campo',		// id of the input field
		trigger		:	'boton$campo',	// trigger for the calendar (button ID)
		onSelect	:	function() { this.hide() }
	});
</script>";
			}
			echo "</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>
<input type='text' name='hora$campo' size='2' maxlength='2' value='$hora' onkeyup='validar(hora$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> : 
<input type='text' name='minutos$campo' size='2' maxlength='2' value='$minutos' onkeyup='validar(minutos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "numerico")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]' onkeyup='validar($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		$cuenta_campos++;
		echo "\n";
	} // del foreach
	echo "</table>";
	echo "</td></tr>
	<tr><td colspan='2'><input type=submit value='Guardar'></td></tr></form></table>";
}
// FIN FORMULARIO CREACION DE UN NUEVO REGISTRO

} // fin de se tiene permiso para este script
else
{
	echo "No se tiene permiso para ver esta seccion.<br><br>";
}

echo "
		</td>
	</tr>
</table>
";
?>