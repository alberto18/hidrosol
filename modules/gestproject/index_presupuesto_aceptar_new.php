<?php
exit;
/*
<script type="text/Javascript">
<!--
function cambiarUnidades (numElemento)
{
	var nombre_unidades = "unidades_"+numElemento;
	var objetoUnidades = document.getElementById(nombre_unidades);
	validar(objetoUnidades);
	//recalcularPrecioOferta(numElemento);
	//calcularGratis(numElemento);
}

function recalcularPrecioOferta (numElemento)
{
	var nombre_unidades = "unidades_"+numElemento;
	var objetoUnidades = document.getElementById(nombre_unidades);
	var nombre_min_unidades_dto = "min_unidades_dto_"+numElemento;
	var objetoMinUnidadesDto = document.getElementById(nombre_min_unidades_dto);
	if (parseInt(objetoMinUnidadesDto.value) > 0)
	{
		var nuevo_valor_precio = 0;
		if (parseInt(objetoUnidades.value) >= parseInt(objetoMinUnidadesDto.value))
		{
			// si las unidades son igual o mayor que las de la oferta se pone el precio de oferta y se quita el dto1
			var nombre_precio_unidad_dto = "precio_unidad_dto_"+numElemento;
			var objetoPrecioUnidadDto = document.getElementById(nombre_precio_unidad_dto);
			nuevo_valor_precio = objetoPrecioUnidadDto.value;
			var nombre_dto1 = "dto1_"+numElemento;
			var objetoDto1 = document.getElementById(nombre_dto1);
			objetoDto1.value = "0.00";
		}
		else
		{
			// si no son suficientes unidades se pone el precio y el dto1 que tendria sin tener en cuenta esta oferta
			var nombre_precio_inicial_sin = "precio_inicial_sin_"+numElemento;
			var objetoPrecioInicialSin = document.getElementById(nombre_precio_inicial_sin);
			nuevo_valor_precio = objetoPrecioInicialSin.value;
			var nombre_dto1_sin = "dto1_sin_"+numElemento;
			var objetoDto1Sin = document.getElementById(nombre_dto1_sin);
			var nombre_dto1 = "dto1_"+numElemento;
			var objetoDto1 = document.getElementById(nombre_dto1);
			objetoDto1.value = objetoDto1Sin.value;
		}
		var nombre_precio_inicial = "precio_inicial_"+numElemento;
		var objetoPrecioInicial = document.getElementById(nombre_precio_inicial);
		objetoPrecioInicial.value = nuevo_valor_precio;
		recalcularPrecio(numElemento);
	}
}

function calcularGratis (numElemento)
{
	var nombre_min_unidades_gratis = "min_unidades_gratis_"+numElemento;
	var objetoMinUnidadesGratis = document.getElementById(nombre_min_unidades_gratis);
	if (parseInt(objetoMinUnidadesGratis.value) > 0)
	{
		var nombre_unidades = "unidades_"+numElemento;
		var objetoUnidades = document.getElementById(nombre_unidades);
		var num_unidades_regaladas = 0;
		if (parseInt(objetoUnidades.value) >= parseInt(objetoMinUnidadesGratis.value))
		{
			// se regalaran unidades gratis
			var nombre_num_unidades_gratis = "num_unidades_gratis_"+numElemento;
			var objetoNumUnidadesGratis = document.getElementById(nombre_num_unidades_gratis);
			var incremento = parseInt(objetoNumUnidadesGratis.value);
			var valor_inicial = parseInt(objetoUnidades.value);
			var valor_minimo = parseInt(objetoMinUnidadesGratis.value);
			while (valor_inicial >= valor_minimo)
			{
				valor_inicial -= valor_minimo;
				num_unidades_regaladas += incremento;
			}
		}
		var nombre_txt_gratis = "txt_gratis_"+numElemento;
		var objetoTxtGratis = document.getElementById(nombre_txt_gratis);
		var nombre_unidades_gratis = "unidades_gratis_"+numElemento;
		var objetoUnidadesGratis = document.getElementById(nombre_unidades_gratis);
		objetoUnidadesGratis.value = num_unidades_regaladas;
		if (num_unidades_regaladas == 0)
		{
			objetoTxtGratis.innerHTML="";
		}
		else
		{
			objetoTxtGratis.innerHTML="+ "+num_unidades_regaladas;
		}
		
	}
}

function cambiarDto (inputObj)
{
	var nombre = inputObj.name;// pilla el nombre
	validarFloat(inputObj);
	var posBarraBaja = nombre.indexOf("_"); // busco la barra baja en dto2_X
	var numElemento = nombre.substring(posBarraBaja+1); // busco el articulo de todos los mostrados
	var nombre_precio_inicial = "precio_inicial_"+numElemento;
	var objetoPrecioInicial = document.getElementById(nombre_precio_inicial);
	if (objetoPrecioInicial.value > 0)
	{
		recalcularPrecio(numElemento);
	}
}

function recalcularPrecio (numElemento)
{
	var numElemento;
	var nombre_precio_inicial = "precio_inicial_"+numElemento;
	var objetoPrecioInicial = document.getElementById(nombre_precio_inicial);
	// el articulo tiene definido un precio inicial
	var nuevo_precio = objetoPrecioInicial.value;
	var nombre_dto2 = "dto2_"+numElemento;
	var objetoDto2 = document.getElementById(nombre_dto2);
	if (objetoDto2.value > 0)
	{
		// calculo del descuento 2
		var dto_2 = (nuevo_precio*objetoDto2.value)/100;
		dto_2 = Math.round(dto_2*100)/100; // redondeo a dos decimales
		nuevo_precio -= dto_2;
		nuevo_precio = Math.round(nuevo_precio*100)/100;
	}
	var nombre_dto3 = "dto3_"+numElemento;
	var objetoDto3 = document.getElementById(nombre_dto3);
	if (objetoDto3.value > 0)
	{
		// calculo del descuento 3
		var dto_3 = (nuevo_precio*objetoDto3.value)/100;
		dto_3 = Math.round(dto_3*100)/100; // redondeo a dos decimales
		nuevo_precio -= dto_3;
		nuevo_precio = Math.round(nuevo_precio*100)/100;
	}
	var nombre_pronto_pago = "pronto_pago_"+numElemento;
	var objetoProntoPago = document.getElementById(nombre_pronto_pago);
	if (objetoProntoPago.value > 0)
	{
		var dto_pronto_pago = (nuevo_precio*objetoProntoPago.value)/100;
		dto_pronto_pago = Math.round(dto_pronto_pago*100)/100; // redondeo a dos decimales
		nuevo_precio -= dto_pronto_pago;
		nuevo_precio = Math.round(nuevo_precio*100)/100;
	}
	var nombre_precio_final = "precio_final_"+numElemento;
	var objetoPrecioFinal = document.getElementById(nombre_precio_final);
	objetoPrecioFinal.value = nuevo_precio;
}
-->
</script>
*/
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";

// CONFIGURACION
$titulo = "ACEPTACION DEL PRESUPUESTO ";
$titulo_pagina = "ACEPTACION DE PRESUPUESTO";
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_presupuesto_aceptar_new";
$script_presupuestos = "index_presupuestos_new";
$script_pedidos = "index_pedidos_new";
$script_ver_correo = "ver_correo_presupuesto";
$tabla = "pre_articulos_t";
$tabla_padre = "presupuestos_t";
$tabla_stock = "stock_t";
$tabla_pre_portes = "pre_portes_t";
$tabla_reservas = "stock_reserva_t";
$tabla_clientes_direcciones = "clientes_direcciones_t";
$tabla_proveedores = "proveedores_t";
$tabla_articulos = "articulos_t";
$tabla_cli_prov = "clientes_prov_t";
$tabla_m_embalajes = "maestro_tipos_embalajes_t";
$tabla_art_embalajes = "articulos_embalajes_t";
$tabla_pedidos = "pedidos_t";
$tabla_ped_articulos = "ped_articulos_t";
$tabla_ped_portes = "ped_portes_t";
$tabla_m_igic = "maestro_igic_t";
$registros_por_pagina = 10;
$script_descarga = "";
$tamano_max_archivo = "16000000";
if ($color_entorno != "") { $color_fondo = $color_entorno; $color_fondo_claro = $color_entorno_claro; }
else { $color_fondo = "97c00e"; $color_fondo_claro = "dcfb73"; }

$texto_textarea = '<br type="_moz" />';

echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo "
<table width='100%' height='700' border='0' cellpadding='0' cellspacing='0' class='text' align='center'>
	<tr valign='top'>
		<td width=100% align=left>
";

if (PermisosSecciones($user_id, $script, array()) == 1)
{

// textos de la pagina
$texto_crear = "Crear articulo";
$texto_listado_general = "Listar todos los articulos";
$texto_creado = "Articulo creado";
$texto_modificado = "Articulo modificado";
$texto_borrado = "Articulo borrado";
$nombre_objeto = " UN ARTICULO";

// Campos con los que se trabajara en el insert y modify
$campos_col1 = array('articulo_id','unidades','proveedor_id','precio_unidad');
$campos_col2 = array();

// Nombres que apareceran en las columnas de los formularios
$nombres_col1 = array('Articulo','Unidades','Proveedor','Precio');
$nombres_col2 = array();

// Tipos. Cada campo puede ser:
// text;readonly;size (60)
// password;readonly;size
// textarea;readonly;row;col (10;60)
// select;readonly;tabla;campo_mostrar;campo_para_value;campo_condicion
// checkbox;readonly
// date;readonly
// hidden;tabla;campo_mostrar;campo_para_value;campo_para_order;nuevo_nombre;campo_depende;campo_filtro mostrara un select que saltara y ademas tendra otro campo oculto con el valor
// dni;readonly
// telefono;readonly
// cuenta;readonly
// email;readonly;size
// float;readonly;size
// multiple;readonly;tabla;campo_mostrar;campo_para_value;campo_para_order;nombre_checkbox
// file poner el campo "nombre_fichero", actualiza nombre_fichero, tipo_fichero, peso_fichero, fichero_binario, fecha_subida, user_id
//   Los campos en la tabla a a�adir serian:
//  | nombre_fichero              | varchar(200)
//  | tipo_fichero                | varchar(20)
//  | peso_fichero                | varchar(20)
//  | fecha_subida                | datetime
//  | user_id                     | int(11)
//  | fichero_binario             | blob
// ss;readonly
// calendar;readonly
// time;readonly
// numerico;readonly;size
// fileCarp poner el campo "nombre_fichero", actualiza solo nombre_fichero y sube a la carpeta definida
$tipos_col1  = array('select;1;articulos_t;nombre;id;nombre','numerico;1;5','select;1;proveedores_t;nombre_corto;id;nombre_corto','float;1;8');
$tipos_col2  = array();

// Separadores o titulos
$titulos_col1 = array('');
$titulos_col2 = array('');

// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('articulo_id','tipo_embalaje_id','unidades',
'proveedor_id','precio_unidad','dto_porc','dto2','dto3','igic_id',
'fecha_max_reserva','comprometido');
//'importe_articulo','importe_igic',
// indica la tabla de donde es el campo
$tablas_campos_listado = array($tabla,$tabla,$tabla,
$tabla,$tabla,$tabla,$tabla,$tabla,$tabla,
$tabla,$tabla);
//$tabla,$tabla,

$nombres_listado = array('(Ref.) Articulo','Embalaje','Ud.',
'Prov.','Precio','Dto1','Dto2','Dto3','Igic',
'Reserva','Compromiso');
//'Importe parcial','IGIC parcial',

$campos_necesarios_listado = array('id','unidades_embalaje','reservado','articulo_id','proveedor_id','unidades_gratis');
$tablas_campos_necesarios = array($tabla,$tabla,$tabla,$tabla,$tabla,$tabla);

// Si es necesaria una decodificacion en el listado
// '' no es necesaria
// si;tabla;campo_mostrar;campo_buscar
// si;date
// si;datetime
// si;checkbox
// si;time
$campos_listado_decod = array ('si;articulos_t;nombre;id','si;maestro_tipos_embalajes_t;nombre;id','',
'si;proveedores_t;nombre_corto;id','','','','','si;maestro_igic_t;valor;id',
'si;date','');
//'','',

// Campo para la busqueda
$campo_busqueda = "id";

// Campo padre
$usa_padre = 1;
$campopadre = "presupuesto_id";

// Variables del script
$parametros_nombres = array("accion","pag",$campopadre);
$parametros_formulario = array("pag",$campopadre);
$parametros_filtro = array(); // parametros que estan en el filtro
// Tipos de parametros
//									Sin nada es que no sirven para los filtros o filtro distinto
// texto;campo_filtro							Es un filtro para texto
// texto								Es un filtro para texto, pero hay que desarrollar el filtrado
// select;campo_filtro							Es un filtro con select
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;desde		Es un filtro para fechas superiores a la introducida
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;hasta		Es un filtro para fechas inferiores a la introducida
$parametros_tipos = array("","","");

foreach($parametros_nombres as $indice_parametros => $nombre_param)
{
	list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
	if ($ele == "texto")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; $$nombre_param = str_replace('+',' ',$$nombre_param); }
		else { $$nombre_param = ""; }
	}
	if ($ele == "select")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
	if ($ele == "fecha")
	{
		if ($_REQUEST[$ele3] != "") { $$ele3 = $_REQUEST[$ele3]; }
		else { $$ele3 = ""; }
		if ($_REQUEST[$ele4] != "") { $$ele4 = $_REQUEST[$ele4]; }
		else { $$ele4 = ""; }
		if ($_REQUEST[$ele5] != "") { $$ele5 = $_REQUEST[$ele5]; }
		else { $$ele5 = ""; }
	}
	if ($ele == "")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
}
if ($pag == "") { $pag = "0"; }


// ACCION ACEPTAR PRESUPUESTO
if ($accion == "aceptar")
{
	// comprobacion de que se ha seleccionado al menos alguna unidad de algun articulo
	$existen_articulos = 0;
	$consulta  = "select * from $tabla where $tabla.$campopadre='".$$campopadre."' order by $tabla.id;";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: $consulta " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		$nombre_checkbox = "check_".$linea['id'];
		$nombre_unidades = "unidades_".$linea['id'];
		$nombre_comprometido = "comprometido_".$linea['id'];
		$nombre_embalaje = "embalaje_".$linea['id'];
		//if ($_REQUEST[$nombre_checkbox] == "on" && $_REQUEST[$nombre_unidades] > 0)
		if ($linea['unidades'] > 0) // 2012-10-29: pedido=presupuesto
		{
			$existen_articulos++;
		}
	}
	if ($existen_articulos > 0)
	{
		// se ha seleccionado algun articulo
		$dia = date("d");
		$mes = date("m");
		$ano = date("Y");
		
		$num_pedido_ultimo = "";
		$consulta1  = "select numero from $tabla_pedidos where ano='".$ano."' order by id desc limit 1;";
		//echo "$consulta1<br>";
		$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: " . mysql_error());
		while ($linea1 = mysql_fetch_array($resultado1)) {
			$num_pedido_ultimo = $linea1['numero'];
		}
		list($tmp, $num_pedido_ultimo) = explode('/',$num_pedido_ultimo);
		$num_pedido_ultimo++;
		$num_pedido = "P/".str_pad($num_pedido_ultimo, 4, "0", STR_PAD_LEFT)."/".$ano;
		
		$importe_sin_igic = 0;
		$importe_igic = 0;
		$importe_total = 0;
		$consulta2 = "select * from $tabla_padre where id='".$$campopadre."';";
		//echo "$consulta2<br>";
		$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: ".$consulta2 . mysql_error());
		while ($linea2 = mysql_fetch_array($resultado2))
		{
			$importe_sin_igic = $linea2['importe_sin_igic'];
			$importe_igic = $linea2['importe_igic'];
			$importe_total = $linea2['importe_total'];
		}
		// 2012-10-29: pedido=presupuesto
		//$consulta_pedido = "insert into $tabla_pedidos set numero=\"".$num_pedido."\", fecha='$ano-$mes-$dia', ano='".$ano."', $campopadre='".$$campopadre."', estado_id='".$pedido_estado_creado."', importe_sin_igic='0', importe_igic='0', importe_total='0';";
		$consulta_pedido = "insert into $tabla_pedidos set numero=\"".$num_pedido."\", fecha='$ano-$mes-$dia', ano='".$ano."', $campopadre='".$$campopadre."', estado_id='".$pedido_estado_pendiente_servir."', importe_sin_igic='".$importe_sin_igic."', importe_igic='".$importe_igic."', importe_total='".$importe_total."';";
		//echo "$consulta_pedido<br>";
		$res_pedido = mysql_query($consulta_pedido) or die("$consulta_pedido, La consulta fall�: " . mysql_error());
		$ultimo_pedido_id = mysql_insert_id();
		
		//$array_importes_proveedores = array();
		$consulta  = "select * from $tabla where $tabla.$campopadre='".$$campopadre."' order by $tabla.id;";
		//echo "$consulta<br>";
		$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: $consulta " . mysql_error());
		while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
		{
/*
			$nombre_checkbox = "check_".$linea['id'];
			$nombre_unidades = "unidades_".$linea['id'];
			$nombre_comprometido = "comprometido_".$linea['id'];
			$nombre_embalaje = "embalaje_".$linea['id'];
			if ($_REQUEST[$nombre_checkbox] == "on" && $_REQUEST[$nombre_unidades] > 0)
			{
				$tipo_embalaje_id = 0;
				$unidades_embalaje = 0;
				$consulta_cat = "select $tabla_art_embalajes.tipo_embalaje_id, $tabla_art_embalajes.unidades from $tabla_art_embalajes where $tabla_art_embalajes.id='".$_REQUEST[$nombre_embalaje]."';";
				$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
				{
					$tipo_embalaje_id = $linea_cat['tipo_embalaje_id'];
					$unidades_embalaje = $linea_cat['unidades'];
				}
				$valor_igic = "";
				$consulta_cat = "select * from $tabla_m_igic where $tabla_m_igic.id='".$linea['igic_id']."';";
				$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
				{
					$valor_igic = $linea_cat['valor'];
				}
				
				$importe_art_parcial = $_REQUEST[$nombre_unidades]*$linea['precio_unidad'];
				$importe_igic_parcial = round(($importe_art_parcial*$valor_igic)/100, 2);
*/
// 2012-10-29: pedido=presupuesto
				//$consulta_ped_art = "insert into $tabla_ped_articulos set pedido_id='".$ultimo_pedido_id."', articulo_id='".$linea['articulo_id']."', tipo_embalaje_id='".$tipo_embalaje_id."', unidades_embalaje='".$unidades_embalaje."', unidades='".$_REQUEST[$nombre_unidades]."', proveedor_id='".$linea['proveedor_id']."', precio_articulo='".$linea['precio_articulo']."', dto_porc='".$linea['dto_porc']."', precio_neto='".$linea['precio_neto']."', num_unidades_gratis='0', precio_unidad='".$linea['precio_unidad']."', dto2='".$linea['dto2']."', dto3='".$linea['dto3']."', pronto_pago='".$linea['pronto_pago']."', importe_articulo='".$importe_art_parcial."', igic_id='".$linea['igic_id']."', importe_igic='".$importe_igic_parcial."', reservado='".$linea['reservado']."', fecha_max_reserva='".$linea['fecha_max_reserva']."', comprometido='".$_REQUEST[$nombre_comprometido]."';";
				//min_unidades_dto: int(11)
				//precio_unidad_dto: float(50,2)
				//precio_neto_sin_oferta: float(50,2)
				//dto_porc_sin_oferta: float(50,2)
				//min_unidades_gratis: int(11)
				//num_unidades_gratis: int(11)
				//unidades_gratis: int(11)
				$consulta_ped_art = "insert into $tabla_ped_articulos set pedido_id='".$ultimo_pedido_id."', articulo_id='".$linea['articulo_id']."', tipo_embalaje_id='".$linea['tipo_embalaje_id']."', unidades_embalaje='".$linea['unidades_embalaje']."', unidades='".$linea['unidades']."', proveedor_id='".$linea['proveedor_id']."', precio_articulo='".$linea['precio_articulo']."', dto_porc='".$linea['dto_porc']."', precio_neto='".$linea['precio_neto']."', precio_unidad='".$linea['precio_unidad']."', dto2='".$linea['dto2']."', dto3='".$linea['dto3']."', pronto_pago='".$linea['pronto_pago']."', 
importe_articulo='".$linea['importe_articulo']."', igic_id='".$linea['igic_id']."', importe_igic='".$linea['importe_igic']."', reservado='".$linea['reservado']."', fecha_max_reserva='".$linea['fecha_max_reserva']."', comprometido='".$linea['comprometido']."', min_unidades_dto='".$linea['min_unidades_dto']."', precio_unidad_dto='".$linea['precio_unidad_dto']."', precio_neto_sin_oferta='".$linea['precio_neto_sin_oferta']."',dto_porc_sin_oferta='".$linea['dto_porc_sin_oferta']."', min_unidades_gratis='".$linea['min_unidades_gratis']."', num_unidades_gratis='".$linea['num_unidades_gratis']."', unidades_gratis='".$linea['unidades_gratis']."';";
				//echo "$consulta_ped_art<br>";
				$resultado_ped_art = mysql_query($consulta_ped_art) or die("$consulta_ped_art, La consulta fall�: " . mysql_error());
				$ultimo_articulo_id = mysql_insert_id();
				
				// si habia una reserva en el presupuesto se pasa al pedido
				if ($linea['reservado'] == "on")
				{
					$cons_reserva = "update $tabla_reservas set ped_articulo_id='".$ultimo_articulo_id."', pre_articulo_id='0' where pre_articulo_id='".$linea['id']."';";
					//echo "cons_reserva: $cons_reserva<br>";
					$res_reserva = mysql_query($cons_reserva) or die("La consulta fall&oacute;: $cons_reserva" . mysql_error());
				}
				else
				{
					// si no esta reservado o si se intenta reservar mas del stock existente (que no se deja reservar): se suma para el calculo de los portes
					$array_importes_proveedores[$linea["proveedor_id"]]["importe_arti"] += $importe_art_parcial;
					$array_importes_proveedores[$linea["proveedor_id"]]["importe_igic"] += $importe_igic_parcial;
				}
			//} // fin if checkbox
		} // fin while $linea
		//echo print_r($array_importes_proveedores,true);
		
/*
// 2012-10-29: pedido=presupuesto
		// calculo de portes
		$consulta_cat = "select $tabla_clientes_direcciones.municipio_id from $tabla_clientes_direcciones where $tabla_clientes_direcciones.id='".$_SESSION['presupuestoDireccion']."';";
		$consulta_cat = "select $tabla_clientes_direcciones.municipio_id, $tabla_padre.cliente_id from $tabla_clientes_direcciones join $tabla_padre on $tabla_padre.cliente_direccion_id=$tabla_clientes_direcciones.id where $tabla_padre.id='".$$campopadre."';";
		$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
		while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
		{
			$cons1 = "select * from maestro_municipios_t where id='".$linea_cat['municipio_id']."';";
			$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
			while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
			{
				if ($lin1['isla_id'] != $identificador_gran_canaria)
				{
					// hay que calcular portes
					foreach ($array_importes_proveedores as $valor_prov => $array_datos)
					{
						// busqueda de los portes definidos para este proveedor en el presupuesto origen
						$existe_portes = 0;
						$ped_porc_portes = 0;
						$ped_importe_min_portes_pagados = 0;
						$ped_importe_portes = 0;
						$ped_cli_ag_transporte_id = 0;
						$ped_prov_ag_transporte_id = 0;
						$cons_porte = "select * from $tabla_pre_portes where $campopadre='".$$campopadre."' and proveedor_id='".$valor_prov."';";
						$res_porte = mysql_query($cons_porte) or die("La consulta fall&oacute;: $cons_porte " . mysql_error());
						while ($lin_porte = mysql_fetch_array($res_porte, MYSQL_ASSOC))
						{
							$existe_portes = 1;
							$ped_porc_portes = $lin_porte['porc_portes'];
							$ped_importe_min_portes_pagados = $lin_porte['importe_min_portes_pagados'];
							$ped_importe_portes = $lin_porte['importe_portes'];
							$ped_cli_ag_transporte_id = $lin_porte['cli_ag_transporte_id'];
							$ped_prov_ag_transporte_id = $lin_porte['prov_ag_transporte_id'];
						}
						if ($existe_portes == 1)
						{
							$cons_insert_porte = "insert into $tabla_ped_portes set pedido_id='".$ultimo_pedido_id."', proveedor_id='".$valor_prov."', importe_min_portes_pagados='".$ped_importe_min_portes_pagados."', cli_ag_transporte_id='".$ped_cli_ag_transporte_id."', prov_ag_transporte_id='".$ped_prov_ag_transporte_id."'";
							// contabilizo el importe sin igic
							$importe_total_prov = $array_datos["importe_arti"];//$array_datos["importe_igic"]
							if ($importe_total_prov <= $ped_importe_min_portes_pagados)
							{
								// no supera el minimo para que sea el proveedor quien pague los portes: se marca que se debe usar la ag transportes del cliente
								$cons_insert_porte .= ", usar_cliente='on'";
							}
							else
							{
								// si se supera el minimo: como lo paga el proveedor el importe es cero y se marca que se debe usar la ag transportes del proveedor
								$cons_insert_porte .= ", usar_cliente=''";
							}
							//echo "$cons_insert_porte<br>";
							$resultado_insert_porte = mysql_query($cons_insert_porte) or die("$cons_insert_porte, La consulta fall�: " . mysql_error());
						}
						else
						{
							// en el presupuesto no existian portes para este proveedor
						}
					} // fin foreach
				} // fin if es fuera gran canaria
			} // fin if $lin1
		} // fin if $linea_cat
*/
		$cons_portes = "select * from $tabla_pre_portes where presupuesto_id='".$$campopadre."';";
		$res_portes = mysql_query($cons_portes) or die("La consulta fall&oacute;: $cons_portes " . mysql_error());
		while ($lin_portes = mysql_fetch_array($res_portes, MYSQL_ASSOC))
		{
			$cons_portes2 = "insert into $tabla_ped_portes set pedido_id='".$ultimo_pedido_id."', proveedor_id='".$lin_portes['proveedor_id']."', importe_min_portes_pagados='".$lin_portes['importe_min_portes_pagados']."', cli_ag_transporte_id='".$lin_portes['cli_ag_transporte_id']."', prov_ag_transporte_id='".$lin_portes['prov_ag_transporte_id']."', usar_cliente='".$lin_portes['usar_cliente']."';";
			//echo "cons_portes2: $cons_portes2<br>";
			$res_portes2 = mysql_query($cons_portes2) or die("La consulta fall&oacute;: $cons_portes2 " . mysql_error());
			$ultimo_id =  mysql_insert_id();
		}
/*
// 2012-10-29: pedido=presupuesto
		// calculo importes
		$array_resul = CalcularPedido ($ultimo_pedido_id);
		$importe_sin_igic = $array_resul[0];
		$importe_igic = $array_resul[1];
		$importe_total = $array_resul[2];
		$consulta_pedido2 = "update $tabla_pedidos set importe_sin_igic=\"".$importe_sin_igic."\", importe_igic=\"".$importe_igic."\", importe_total=\"".$importe_total."\" where id='".$ultimo_pedido_id."';";
		//echo "$consulta_pedido2<br>";
		$res_pedido2 = mysql_query($consulta_pedido2) or die("$consulta_pedido2, La consulta fall�: " . mysql_error());
		// actualizo estado de presupuesto
*/
//jgs
		$consulta_presu = "update $tabla_padre set estado_id=\"".$presu_estado_aceptado."\" where id='".$$campopadre."';";
		//echo "$consulta_presu<br>";
		$res_presu = mysql_query($consulta_presu) or die("$consulta_presu, La consulta fall�: " . mysql_error());
		echo "<b>Presupuesto aceptado y pedido creado</b>";
		echo "<form name=form_volver1 method=get action='modules.php'>
<input type=hidden name=mod value=gestproject>
<input type=hidden name=file value=$script_pedidos>
<input type=hidden name=pag value=0>
</form>
<script>document.form_volver1.submit();</script>";
	}
	else
	{
		echo "<b>No se puede aceptar el presupuesto y crear el pedido porque no se ha seleccionado ningun articulo.</b><br>";
	}
}
// FIN ACCION ACEPTAR PRESUPUESTO

// COMIENZA EL SCRIPT

if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$nombre_padre = "";
		$pre_importe_sin_igic = 0;
		$pre_importe_igic = 0;
		$pre_importe_total = 0;
		$pre_estado_id = 0;
		$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
		//echo "$consulta_padre";
		$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
		while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
		{
			$nombre_padre = "<a target='_blank' href='$enlacevolver"."$script_ver_correo&presupuesto_id=".$$campopadre."' style='color:#$color_fondo;'>".$linea_padre['numero']."</a>";
			$pre_importe_sin_igic = $linea_padre['importe_sin_igic'];
			$pre_importe_igic = $linea_padre['importe_igic'];
			$pre_importe_total = $linea_padre['importe_total'];
			$pre_estado_id = $linea_padre['estado_id'];
		}
	}
        
	echo "
<center><b>$titulo $nombre_padre</b><br>";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != $campopadre && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "[<a href='$enlacevolver"."$script_presupuestos&pag=0'>Volver a presupuestos</a>]";
	echo "
</center>";
}

// OBTENEMOS EL LISTADO DE REGISTROS 
if ($pre_estado_id == $presu_estado_creado)
{
	if ($usa_padre != 0)
	{
		$condiciones = " $tabla.$campopadre='".$$campopadre."' ";
		$parametros = "&$campopadre=".$$campopadre;
        }
	else
	{
		$condiciones = "";
		$parametros = "";
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
		if ($ele == "texto" && $ele2 != "")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2 like '%".$$nombre_param."%' ";
				$$nombre_param = str_replace(' ','+',$$nombre_param); 
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "select")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2='".$$nombre_param."' ";
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "fecha")
		{
			if ($$ele3 != "" && $$ele4 != "" && $$ele5 != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				($ele6 == "desde" ? $condiciones .= " $tabla.$ele2>='".$$ele3."-".$$ele4."-".$$ele5." 00:00:00' " : $condiciones .= " $tabla.$ele2<='".$$ele3."-".$$ele4."-".$$ele5." 23:59:59' ");
				$parametros .= "&$ele3=".$$ele3."&$ele4=".$$ele4."&$ele5=".$$ele5;
			}
		}
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && $nombre_param != $campopadre && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	$string_para_select = "";
	foreach($campos_necesarios_listado as $indice => $campo)
	{
		$string_para_select .= $tablas_campos_necesarios[$indice].".$campo,";
	}
	echo "<table width=100%>
	<tr bgcolor='#$color_fondo'>";
	echo "<td><font color='#ffffff'><b>Incluir</b></font></td>";
	$columnas = 0;
	foreach ($campos_listado as $campo) {
		echo "<td><span class='listadoTabla'>$nombres_listado[$columnas]</span></td>";
		if ($campo == "tipo_embalaje_id")
		{
			echo "<td><span class='listadoTabla'>Embalaje pedido</span></td>";
		}
		if ($campo == "unidades")
		{
			echo "<td><span class='listadoTabla'>Stock</span></td>";
			echo "<td><span class='listadoTabla'>Pedido</span></td>";
		}
		$string_para_select .= " ".$tablas_campos_listado[$columnas].".$campo,";
		$columnas++;
	}
	$columnas += 3;
	$columnas += 1;// Incluir
	// Eliminamos el ultimo caracter
	$string_para_select = substr($string_para_select,0,-1);
	echo "</tr>";
	
	$string_para_select = "*";
	$consulta  = "select $string_para_select from $tabla";
	if ($condiciones != "") { $consulta .= " where $condiciones "; }
	// Vemos si existe un orderby
	if ($_REQUEST[orderby] != "")
	{
		$order = " order by $tabla.$_REQUEST[orderby]";
		$parametros .= "&orderby=".$_REQUEST[orderby];
	}
	else { $order = " order by $tabla.$campo_busqueda";  }
/*
	if ($pag != "") {
		// la primera pagina es la 0
		$inicio = $pag*$registros_por_pagina;
		$limit = " limit $inicio,$registros_por_pagina";
	}
	else { $limit = ""; }
*/
	$limit = "";
	$consulta .= " group by $tabla.id $order $limit;";
	//echo "$consulta<br>";
	echo "
	<form name=form_aceptar method=post action='$enlacevolver"."$script'>
	<input type=hidden name=accion value='aceptar'>
	<input type=hidden name=$campopadre value='".$$campopadre."'>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: $consulta " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		$nombre_checkbox = "check_".$linea['id'];
		$nombre_unidades = "unidades_".$linea['id'];
		$nombre_comprometido = "comprometido_".$linea['id'];
		$nombre_embalaje = "embalaje_".$linea['id'];
		
		$nombre_min_unidades_dto = "min_unidades_dto_".$linea['id'];
		$nombre_precio_unidad_dto = "precio_unidad_dto_".$linea['id'];
		$nombre_dto1 = "dto1_".$linea['id'];
		$nombre_precio_inicial_sin = "precio_inicial_sin_".$linea['id'];
		$nombre_dto1_sin = "dto1_sin_".$linea['id'];
		$nombre_precio_inicial = "precio_inicial_".$linea['id'];
		$nombre_min_unidades_gratis = "min_unidades_gratis_".$linea['id'];
		$nombre_num_unidades_gratis = "num_unidades_gratis_".$linea['id'];
		$nombre_unidades_gratis = "unidades_gratis_".$linea['id'];
		$nombre_txt_gratis = "txt_gratis_".$linea['id'];
		$nombre_dto2 = "dto2_".$linea['id'];
		$nombre_dto3 = "dto3_".$linea['id'];
		$nombre_pronto_pago = "pronto_pago_".$linea['id'];
		$nombre_precio_final = "precio_final_".$linea['id'];
//jgs
		//$nombre_dto1_sin = "dto1_sin_".$linea['id'];
/*
		echo "
	<tr>
		<td colspan='$columnas'>
			$nombre_min_unidades_dto<input type='text' readonly='readonly' name='$nombre_min_unidades_dto' id='$nombre_min_unidades_dto' size='5' value='$linea[min_unidades_dto]' />
			$nombre_precio_unidad_dto<input type='text' readonly='readonly' name='$nombre_precio_unidad_dto' id='$nombre_precio_unidad_dto' size='5' value='$linea[precio_unidad_dto]' />
			$nombre_dto1<input type='text' readonly='readonly' name='$nombre_dto1' id='$nombre_dto1' size='5' value='$linea[dto_porc]' />
			$nombre_precio_inicial_sin<input type='text' readonly='readonly' name='$nombre_precio_inicial_sin' id='$nombre_precio_inicial_sin' size='5' value='$linea[precio_neto_sin_oferta]' />
			$nombre_dto1_sin<input type='text' readonly='readonly' name='$nombre_dto1_sin' id='$nombre_dto1_sin' size='5' value='$linea[dto_porc_sin_oferta]' />
			$nombre_min_unidades_gratis<input type='text' readonly='readonly' name='$nombre_min_unidades_gratis' id='$nombre_min_unidades_gratis' size='5' value='$linea[min_unidades_gratis]' />
			$nombre_num_unidades_gratis<input type='text' readonly='readonly' name='$nombre_num_unidades_gratis' id='$nombre_num_unidades_gratis' size='5' value='$linea[num_unidades_gratis]' />
			$nombre_unidades_gratis<input type='text' readonly='readonly' name='$nombre_unidades_gratis' id='$nombre_unidades_gratis' size='5' value='$linea[unidades_gratis]' />
			<br>
			<!--precio_inicial-->
		</td>
	</tr>";
*/
		echo "
	<tr>
		<td><!--<input type=checkbox name='$nombre_checkbox' checked>--></td>";
		foreach ($campos_listado as $cuenta_campos => $campo)
		{
			$nombre = "";
			list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod[$cuenta_campos]);
			if ($ele != 'si')
			{
				$nombre = "$linea[$campo]";
			}
			elseif ($ele2 == "date")
			{
				if ($linea[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
				else { $nombre = date("d-m-Y",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "datetime")
			{
				if ($linea[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
				else { $nombre = date("d-m-Y H:i",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "checkbox")
			{
				($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
			}
			elseif ($ele2 == "time")
			{
				list($temp1, $temp2, $temp3) = explode(':',$linea[$campo]);
				$nombre = $temp1.":".$temp2;
			}
			elseif ($linea[$campo] != "")
			{
				$consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre = $lineaselect[$ele3];
					if ($campo == "articulo_id")
					{
						$nombre = "($lineaselect[referencia]) $lineaselect[$ele3]";
					}
				}
			}
			if ($campo == "tipo_embalaje_id")
			{
				$nombre .= " de ".$linea['unidades_embalaje'];
			}
			if ($campo == "unidades")
			{
				if ($linea['reservado'] == "on")
				{
					$nombre = "<input type='hidden' name='$nombre_unidades' id='$nombre_unidades' size='5' value='$linea[$campo]' />$linea[$campo]";
				}
				else
				{
					$nombre = "<input type='text' readonly='readonly' name='$nombre_unidades' id='$nombre_unidades' size='5' value='$linea[$campo]' onkeyup='cambiarUnidades(".$linea['id'].");' />";
				}
				$nombre .= "<span id=\"".$nombre_txt_gratis."\">";
				if ($linea['unidades_gratis'] > 0)
				{
					$nombre .= " + ".$linea['unidades_gratis'];
				}
				$nombre .= "</span>";
			}
			if ($campo == "precio_unidad")
			{
				$nombre = "<input type='text' readonly='readonly' name='$nombre_precio_inicial' id='$nombre_precio_inicial' size='5' value='$linea[$campo]' /> &euro;";
			}
			if (in_array($campo,array("igic_id","importe_articulo","importe_igic")))
			{
				if (in_array($campo,array("importe_articulo","importe_igic")) && $linea['reservado'] == "on")
				{
					// si el articulo esta reservado (pre_articulos_t.reservado=on) no se visualiza el importe
					$nombre = 0;
				}
				$nombre = number_format($nombre, 2, ",",".");
				if (in_array($campo,array("importe_articulo","importe_igic"))) { $nombre .= " &euro;"; }
				if ($campo == "igic_id") { $nombre .= " %"; }
			}
			if (in_array($campo,array("dto_porc","dto2","dto3")))
			{
				if ($linea[$campo] > 0) { $nombre = number_format($linea[$campo], 2, ",",".")." %"; }
				else { $nombre = ""; }
			}
			if ($campo == "comprometido")
			{
				// si el articulo tiene unidades comprometidas se permite modificar o quitar
				$nombre = "
<input type='text' name='$nombre_comprometido' id='$nombre_comprometido' size='5' value='$linea[$campo]' onkeyup='validar($nombre_comprometido);'";
				if ($linea[$campo] == 0) { $nombre .= " readonly='readonly' "; }
				$nombre .= " >";
			}
			if (in_array($campo,array("precio_unidad","dto_porc","dto2","importe_articulo","importe_igic")))
			{
				echo "
		<td align='right'>$nombre</td>";
			}
			else
			{
				echo "
		<td>$nombre</td>";
			}
			if ($campo == "tipo_embalaje_id")
			{
				echo "
		<td><select name=\"".$nombre_embalaje."\">";
				$consulta_cat = "select $tabla_art_embalajes.id, $tabla_art_embalajes.unidades, $tabla_m_embalajes.nombre from $tabla_art_embalajes
join $tabla_m_embalajes on $tabla_m_embalajes.id=$tabla_art_embalajes.tipo_embalaje_id 
where $tabla_art_embalajes.articulo_id='".$linea['articulo_id']."' and $tabla_art_embalajes.inactivo=''
order by $tabla_art_embalajes.defecto_pedido desc, $tabla_art_embalajes.tipo_embalaje_id, $tabla_art_embalajes.unidades;";
				$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
				{
					echo "<option value='$linea_cat[id]'>$linea_cat[nombre] de $linea_cat[unidades]</option>";
				}
				echo "</select></td>";
			}
			if ($campo == "unidades")
			{
				// calculo del stock disponible (no contar las reservas)
				$cantidad_stock = CantidadStock($linea['articulo_id'], $linea['proveedor_id']);
				
				// calculo de las unidades ya pedidas en otros pedidos (aceptados, no finalizados, no entregados y sin marcar como no preparable)
				$cantidad_pedido = CantidadPedido($linea['articulo_id'], $linea['proveedor_id']);
				
				echo "
		<td>$cantidad_stock</td>
		<td>$cantidad_pedido</td>";
			}
		}
		echo "<td>";
		//echo "			<a href='$enlacevolver"."$script&accion=formmodificar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
/*
		$posibilidad = 0;
		//$posibilidad = ComprobarMaestroGrupo($linea['id']);
		if ($posibilidad == 0)
		{
			echo "
			<a href='$enlacevolver"."$script&accion=formborrar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_delete.png' alt='Borrar' title='Borrar' border='0' /></a> ";
		}
*/
		echo "
		</td>";
		echo "</tr>";
		echo "<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas'></td></tr>";
	}
	echo "<tr align='center'><td colspan='$columnas'><input type=submit value='Aceptar presupuesto y crear pedido' class='reduced'></td></tr>";
	echo "
	</form>";
	echo "</table>";
/*
	if ($pag != "")
	{
		$pag_visual = $pag+1;
		// Visualizamos las paginas existentes
		$consulta2  = "select count($tabla.id) as num from $tabla";
		if ($condiciones != "") { $consulta2 .= " where $condiciones;"; } else { $consulta2 .= ";"; }
		$resultado2 = mysql_query($consulta2) or die("$consulta2, La consulta fall&oacute;: " . mysql_error());
		//echo "$consulta2<br>";
		while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC)) {
			$exp = "$linea2[num]";
		}
		if ($exp > $registros_por_pagina)
		{
			echo PaginadoListado($pag, $enlacevolver.$script, $parametros, $exp, $registros_por_pagina);
		}
	}
*/
	echo "<hr>";
}
// FIN DE OBTENER EL LISTADO INICIAL


} // fin de se tiene permiso para este script
else
{
	echo "No se tiene permiso para ver esta seccion.<br><br>";
}

echo "
		</td>
	</tr>
</table>
";
?>