<?php 
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";

// CONFIGURACION
$titulo = "GESTION DE INCIDENCIAS";
$titulo_pagina = "INCIDENCIAS";
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_incidencias_new";
$script_detalle = "index_detalle_incidencias_new";
$script_log = "index_log_incidencias_new";
//$script_provincias = "index_maestro_provincias_new";
$tabla = "incidencias_t";
$tabla_detalles = "detalle_incidencias_t";
$tabla_padre = "maestro_provincias_t";
$registros_por_pagina = 10;
$script_descarga = "";
$tamano_max_archivo = "16000000";
if ($color_entorno != "") { $color_fondo = $color_entorno; $color_fondo_claro = $color_entorno_claro; }
else { $color_fondo = "97c00e"; $color_fondo_claro = "dcfb73"; }

$texto_textarea = '<br type="_moz" />';

echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo "
<table width='100%' height='700' border='0' cellpadding='0' cellspacing='0' class='text' align='center'>
	<tr valign='top'>
		<td width=100% align=left>
";

if (PermisosSecciones($user_id, $script, array()) == 1)
{

// textos de la pagina
$texto_crear = "Crear incidencia";
$texto_listado_general = "Listar todas las incidencias";
$texto_creado = "Incidencia creada";
$texto_modificado = "Incidencia modificada";
$texto_borrado = "Incidencia borrada";
$nombre_objeto = " UNA INCIDENCIA";

// Campos con los que se trabajara en el insert y modify
$campos_col1 = array('origen_cliente_id','origen_proveedor_id','origen_interna',
'destino_cliente_id','destino_proveedor_id','destino_interna',
'tipo_incidencia_id','usuario_gestion_id','estado_inci_id'
);
$campos_col2 = array();

// Nombres que apareceran en las columnas de los formularios
$nombres_col1 = array('Cliente','Proveedor','Interna',
'Cliente','Proveedor','Interna',
'Tipo incidencia','Usuario que gestiona','Estado'
);
$nombres_col2 = array();

// Tipos. Cada campo puede ser:
// text;readonly;size (60)
// password;readonly;size
// textarea;readonly;row;col (10;60)
// select;readonly;tabla;campo_mostrar;campo_para_value;campo_condicion
// checkbox;readonly
// date;readonly
// hidden;tabla;campo_mostrar;campo_para_value;campo_para_order;nuevo_nombre;campo_depende;campo_filtro mostrara un select que saltara y ademas tendra otro campo oculto con el valor
// dni;readonly
// telefono;readonly
// cuenta;readonly
// email;readonly;size
// float;readonly;size
// multiple;readonly;tabla;campo_mostrar;campo_para_value;campo_para_order;nombre_checkbox
// file poner el campo "nombre_fichero", actualiza nombre_fichero, tipo_fichero, peso_fichero, fichero_binario, fecha_subida, user_id
//   Los campos en la tabla a a�adir serian:
//  | nombre_fichero              | varchar(200)
//  | tipo_fichero                | varchar(20)
//  | peso_fichero                | varchar(20)
//  | fecha_subida                | datetime
//  | user_id                     | int(11)
//  | fichero_binario             | blob
// ss;readonly
// calendar;readonly
// time;readonly
// numerico;readonly;size
// fileCarp poner el campo "nombre_fichero", actualiza solo nombre_fichero y sube a la carpeta definida
$tipos_col1  = array('select;0;clientes_t;nombre;id;nombre','select;0;proveedores_t;nombre;id;nombre','checkbox;0',
'select;0;clientes_t;nombre;id;nombre','select;0;proveedores_t;nombre;id;nombre','checkbox;0',
'select;0;maestro_tipos_incidencias_t;nombre;id;nombre','select;0;usuarios_t;nombre;id;nombre;grupo_id=2','select;0;maestro_estados_inci_t;nombre;id;id'
);
$tipos_col2  = array();

// Separadores o titulos
$titulos_col1 = array('Origen','','',
'Destino','','',
'Datos','',''
);
$titulos_col2 = array('');

// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('fecha_inicio','hora_inicio','tipo_incidencia_id','estado_inci_id','fecha_fin','hora_fin');
// indica la tabla de donde es el campo
$tablas_campos_listado = array($tabla,$tabla,$tabla,$tabla,$tabla,$tabla);

$nombres_listado = array('Fecha inicio','Hora inicio','Tipo incidencia','Estado','Fecha fin','Hora fin');

$campos_necesarios_listado = array('id','origen_cliente_id','origen_proveedor_id','origen_interna','destino_cliente_id','destino_proveedor_id','destino_interna');
$tablas_campos_necesarios = array($tabla,$tabla,$tabla,$tabla,$tabla,$tabla,$tabla);

// Si es necesaria una decodificacion en el listado
// '' no es necesaria
// si;tabla;campo_mostrar;campo_buscar
// si;date
// si;datetime
// si;checkbox
// si;time
$campos_listado_decod = array ('si;date','si;time','si;maestro_tipos_incidencias_t;nombre;id','si;maestro_estados_inci_t;nombre;id','si;date','si;time');

// Campo para la busqueda
$campo_busqueda = "fecha_inicio";

// Campo padre
$usa_padre = 0;
$campopadre = "provincia_id";

// Variables del script
$parametros_nombres = array("accion","pag",$campopadre,
"b_or_cli","b_or_prov","b_or_int",
"b_de_cli","b_de_prov","b_de_int",
"fecha1","fecha2","b_estado","b_tipo",
"b_usu_ges"
);
$parametros_formulario = array("pag",$campopadre,
"b_or_cli","b_or_prov","b_or_int",
"b_de_cli","b_de_prov","b_de_int",
"fecha1","fecha2","b_estado","b_tipo",
"b_usu_ges"
);
$parametros_filtro = array("b_or_cli","b_or_prov","b_or_int","b_de_cli","b_de_prov","b_de_int","b_usu_ges","ano1","mes1","dia1","ano2","mes2","dia2"); // parametros que estan en el filtro
// Tipos de parametros
//									Sin nada es que no sirven para los filtros o filtro distinto
// texto;campo_filtro							Es un filtro para texto
// texto								Es un filtro para texto, pero hay que desarrollar el filtrado
// select;campo_filtro							Es un filtro con select
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;desde		Es un filtro para fechas superiores a la introducida
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;hasta		Es un filtro para fechas inferiores a la introducida
$parametros_tipos = array("","","",
"","","texto;origen_interna",
"","","texto;destino_interna",
"fecha;fecha_inicio;ano1;mes1;dia1;desde","fecha;fecha_inicio;ano2;mes2;dia2;hasta","select;estado_inci_id","select;tipo_incidencia_id",
"select;usuario_gestion_id"
);

foreach($parametros_nombres as $indice_parametros => $nombre_param)
{
	list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
	if ($ele == "texto")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; $$nombre_param = str_replace('+',' ',$$nombre_param); }
		else { $$nombre_param = ""; }
	}
	if ($ele == "select")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
	if ($ele == "fecha")
	{
		if ($_REQUEST[$ele3] != "") { $$ele3 = $_REQUEST[$ele3]; }
		else { $$ele3 = ""; }
		if ($_REQUEST[$ele4] != "") { $$ele4 = $_REQUEST[$ele4]; }
		else { $$ele4 = ""; }
		if ($_REQUEST[$ele5] != "") { $$ele5 = $_REQUEST[$ele5]; }
		else { $$ele5 = ""; }
	}
	if ($ele == "")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
}
if ($pag == "") { $pag = "0"; }

if ($b_or_cli != "") { $b_or_prov = ""; $b_or_int = ""; }
if ($b_or_prov != "") { $b_or_int = ""; }
if ($b_de_cli != "") { $b_de_prov = ""; $b_de_int = ""; }
if ($b_de_prov != "") { $b_de_int = ""; }

// ACCION CREAR
if ($accion == "accioncrear")
{
	if (($_POST['origen_cliente_id'] > 0 && $_POST['origen_proveedor_id'] == 0 && $_POST['origen_interna'] == "") || ($_POST['origen_cliente_id'] == 0 && $_POST['origen_proveedor_id'] > 0 && $_POST['origen_interna'] == "") || ($_POST['origen_cliente_id'] == 0 && $_POST['origen_proveedor_id'] == 0 && $_POST['origen_interna'] == "on"))
	{
		if (($_POST['destino_cliente_id'] > 0 && $_POST['destino_proveedor_id'] == 0 && $_POST['destino_interna'] == "") || ($_POST['destino_cliente_id'] == 0 && $_POST['destino_proveedor_id'] > 0 && $_POST['destino_interna'] == "") || ($_POST['destino_cliente_id'] == 0 && $_POST['destino_proveedor_id'] == 0 && $_POST['destino_interna'] == "on"))
		{
//--------------------------------------------
		$campos_fin_incidencia = "";
		if ($_POST['estado_inci_id'] == 3)
		{
			$campos_fin_incidencia = " fecha_fin=now(), hora_fin=now(), usuario_fin_id='$user_id', ";
		}
		if ($usa_padre != 0)
		{
			$consulta2  = "insert into $tabla set $campopadre='".$$campopadre."', fecha_inicio=now(), hora_inicio=now(), usuario_inicio_id='$user_id', $campos_fin_incidencia";
		} else {
			$consulta2  = "insert into $tabla set fecha_inicio=now(), hora_inicio=now(), usuario_inicio_id='$user_id', $campos_fin_incidencia";
		}
		// Preparamos los campos a insertar
		foreach ($campos_col1 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$cuenta_campos]);
			$nombre_campo = $nombres_col1[$cuenta_campos];
			if ($ele == "date")
			{
				$campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
				if ((($_POST[$campo_fecha_dia] == "00" || $_POST[$campo_fecha_dia] == "") && ($_POST[$campo_fecha_mes] == "00" || $_POST[$campo_fecha_mes] == "") && ($_POST[$campo_fecha_ano] == "0000" || $_POST[$campo_fecha_ano] == "")) || ($_POST[$campo_fecha_dia] > 0 && $_POST[$campo_fecha_dia] < 32 && $_POST[$campo_fecha_mes] > 0 && $_POST[$campo_fecha_mes] < 13 && $_POST[$campo_fecha_ano] > 0))
				{ $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD-MM-AAAA.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "telefono")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg('([0-9]{9})', $_POST[$campo])) { $consulta2 .= " $campo=\"$_POST[$campo]\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo telefonico ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 123456789.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "dni")
			{
				if ($_POST[$campo] != "")
				{
					if (strlen($_POST[$campo]) == 9 && (ereg('([0-9]{8}[a-zA-Z]{1})', $_POST[$campo]) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtoupper($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de DNI ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12345678A o A123456B.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "cuenta")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo"; $campo4 = "cuatro$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "" && $_POST[$campo4] == "") || (strlen($_POST[$campo1]) == 4 && strlen($_POST[$campo2]) == 4 && strlen($_POST[$campo3]) == 2 && strlen($_POST[$campo4]) == 10))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]$_POST[$campo4]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de cuenta corriente ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 1234-5678-90-1234567890.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "email")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg($patron_comprobacion_email, strtolower($_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtolower($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de email ($nombre_campo) se ha introducido con un formato erroneo.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "multiple")
			{
				$array_multiple_ids = array();
				$consulta_elementos = "select * from $ele3;";
				$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
				{
					$array_multiple_ids[] = $linea_elementos[$ele5];
				}
				$valor_campo = "";
				foreach ($array_multiple_ids as $multiple_id)
				{
					$nombre_campo = $ele7.$multiple_id;
					if ($_POST[$nombre_campo] == "on")
					{
						if ($valor_campo != "") { $valor_campo .= ";"; }
						$valor_campo .= $multiple_id;
					}
				}
				$consulta2 .= " $campo=\"$valor_campo\",";
			}
			elseif ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						if ($extension == "jpg" || $extension == "png")
						{
							$binario_peso = $_FILES[$campo]['size']; 
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='#id#slider.$extension',";
							}
						}
						else
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) debe estar en formato jpg o png. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				} // fin if files name != ""
				else
				{
					$consulta2 .= " nombre_fichero='',";
				}
			}
			elseif ($ele == "file")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						$existe = 0;
						if (in_array($extension,$array_ext_excluidas)) { $existe = 1; }
						if ($existe == 1)
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) tiene un formato no permitido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
						else
						{
							$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
							$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
							
							$binario_peso = $_FILES[$campo]['size']; 
							$binario_tipo = $_FILES[$campo]['type']; 
							
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='$nombre_fichero', tipo_fichero='$binario_tipo', peso_fichero='$binario_peso', fichero_binario='$binario_contenido', fecha_subida=now(), user_id='$user_id',";
							}
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				}
			}
			elseif ($ele == "ss")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "") || (strlen($_POST[$campo1]) == 2 && strlen($_POST[$campo2]) == 8 && strlen($_POST[$campo3]) == 2))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de seguridad social ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12-34567890-12.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "calendar")
			{
				if ($_POST[$campo] != "")
				{
					list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
					if ($ele2 != 1)
					{
						if (($dia == 0 && $mes == 0 && $ano == 0) || ($dia != "" && strlen($dia) == 2 && $dia > 0 && $dia < 32 && $mes != "" && strlen($mes) == 2 && $mes > 0 && $mes < 13 && $ano != "" && strlen($ano) == 4 && $ano > 1900))
						{
							$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
						}
						else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD/MM/AAAA.'); history.back(); </SCRIPT>"; exit; }
					}
					else
					{
						$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
					}
				}
			}
			elseif ($ele == "time")
			{
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora == "00" && $valor_minutos == "00") || ($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{ $consulta2 .= " $campo=\"$valor_hora:$valor_minutos\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "textarea") { $consulta2 .= " $campo='".str_replace("'","",str_replace($texto_textarea,"",$_POST[$campo]))."',"; }
			else { $consulta2 .= " $campo='".str_replace("'","",$_POST[$campo])."',"; }
		}
		foreach ($campos_col2 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col2[$cuenta_campos]);
			$nombre_campo = $nombres_col2[$cuenta_campos];
			if ($ele == "date")
			{
				$campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
				if ((($_POST[$campo_fecha_dia] == "00" || $_POST[$campo_fecha_dia] == "") && ($_POST[$campo_fecha_mes] == "00" || $_POST[$campo_fecha_mes] == "") && ($_POST[$campo_fecha_ano] == "0000" || $_POST[$campo_fecha_ano] == "")) || ($_POST[$campo_fecha_dia] > 0 && $_POST[$campo_fecha_dia] < 32 && $_POST[$campo_fecha_mes] > 0 && $_POST[$campo_fecha_mes] < 13 && $_POST[$campo_fecha_ano] > 0))
				{ $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD-MM-AAAA.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "telefono")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg('([0-9]{9})', $_POST[$campo])) { $consulta2 .= " $campo=\"$_POST[$campo]\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo telefonico ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 123456789.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "dni")
			{
				if ($_POST[$campo] != "")
				{
					if (strlen($_POST[$campo]) == 9 && (ereg('([0-9]{8}[a-zA-Z]{1})', $_POST[$campo]) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtoupper($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de DNI ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12345678A o A123456B.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "cuenta")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo"; $campo4 = "cuatro$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "" && $_POST[$campo4] == "") || (strlen($_POST[$campo1]) == 4 && strlen($_POST[$campo2]) == 4 && strlen($_POST[$campo3]) == 2 && strlen($_POST[$campo4]) == 10))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]$_POST[$campo4]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de cuenta corriente ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 1234-5678-90-1234567890.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "email")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg($patron_comprobacion_email, strtolower($_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtolower($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de email ($nombre_campo) se ha introducido con un formato erroneo.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "multiple")
			{
				$array_multiple_ids = array();
				$consulta_elementos = "select * from $ele3;";
				$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
				{
					$array_multiple_ids[] = $linea_elementos[$ele5];
				}
				$valor_campo = "";
				foreach ($array_multiple_ids as $multiple_id)
				{
					$nombre_campo = $ele7.$multiple_id;
					if ($_POST[$nombre_campo] == "on")
					{
						if ($valor_campo != "") { $valor_campo .= ";"; }
						$valor_campo .= $multiple_id;
					}
				}
				$consulta2 .= " $campo=\"$valor_campo\",";
			}
			elseif ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						if ($extension == "jpg" || $extension == "png")
						{
							$binario_peso = $_FILES[$campo]['size']; 
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='#id#slider.$extension',";
							}
						}
						else
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) debe estar en formato jpg o png. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				} // fin if files name != ""
				else
				{
					$consulta2 .= " nombre_fichero='',";
				}
			}
			elseif ($ele == "file")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						$existe = 0;
						if (in_array($extension,$array_ext_excluidas)) { $existe = 1; }
						if ($existe == 1)
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) tiene un formato no permitido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
						else
						{
							$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
							$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
							
							$binario_peso = $_FILES[$campo]['size']; 
							$binario_tipo = $_FILES[$campo]['type']; 
							
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='$nombre_fichero', tipo_fichero='$binario_tipo', peso_fichero='$binario_peso', fichero_binario='$binario_contenido', fecha_subida=now(), user_id='$user_id',";
							}
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				}
			}
			elseif ($ele == "ss")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "") || (strlen($_POST[$campo1]) == 2 && strlen($_POST[$campo2]) == 8 && strlen($_POST[$campo3]) == 2))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de seguridad social ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12-34567890-12.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "calendar")
			{
				if ($_POST[$campo] != "")
				{
					list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
					if ($ele2 != 1)
					{
						if (($dia == 0 && $mes == 0 && $ano == 0) || ($dia != "" && strlen($dia) == 2 && $dia > 0 && $dia < 32 && $mes != "" && strlen($mes) == 2 && $mes > 0 && $mes < 13 && $ano != "" && strlen($ano) == 4 && $ano > 1900))
						{
							$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
						}
						else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD/MM/AAAA.'); history.back(); </SCRIPT>"; exit; }
					}
					else
					{
						$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
					}
				}
			}
			elseif ($ele == "time")
			{
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora == "00" && $valor_minutos == "00") || ($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{ $consulta2 .= " $campo=\"$valor_hora:$valor_minutos\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "textarea") { $consulta2 .= " $campo='".str_replace("'","",str_replace($texto_textarea,"",$_POST[$campo]))."',"; }
			else { $consulta2 .= " $campo='".str_replace("'","",$_POST[$campo])."',"; }
		}
		// Eliminamos la coma final
		$consulta2 = substr($consulta2, 0, strlen($consulta2)-1);
		$consulta2 .= ';';
		//echo "$consulta2";
		
		$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());
		
		// RENOMBRAR CORRECTAMENTE LA IMAGEN DE FONDO
		$ultima_coleccion_id = mysql_insert_id();
		foreach ($campos_col1 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$cuenta_campos]);
			if ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$consulta_datos = "select $campo, id from $tabla where id='$ultima_coleccion_id';";
					$resultado_datos = mysql_query($consulta_datos) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_datos = mysql_fetch_array($resultado_datos, MYSQL_ASSOC))
					{
						$nuevo_nombre = str_replace('#id#',$linea_datos['id'],$linea_datos[$campo]);
						
						$consulta_datos2 = "update $tabla set $campo='$nuevo_nombre' where id='$linea_datos[id]';";
						$resultado_datos2 = mysql_query($consulta_datos2) or die("La consulta fall&oacute;: " . mysql_error());
						
						$uploadfile = $uploaddir_contenidos.$nuevo_nombre;
						move_uploaded_file($_FILES[$campo]['tmp_name'], $uploadfile);
					}
				}
			}
		}
		foreach ($campos_col2 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col2[$cuenta_campos]);
			if ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$consulta_datos = "select $campo, id from $tabla where id='$ultima_coleccion_id';";
					$resultado_datos = mysql_query($consulta_datos) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_datos = mysql_fetch_array($resultado_datos, MYSQL_ASSOC))
					{
						$nuevo_nombre = str_replace('#id#',$linea_datos['id'],$linea_datos[$campo]);
						
						$consulta_datos2 = "update $tabla set $campo='$nuevo_nombre' where id='$linea_datos[id]';";
						$resultado_datos2 = mysql_query($consulta_datos2) or die("La consulta fall&oacute;: " . mysql_error());
						
						$uploadfile = $uploaddir_contenidos.$nuevo_nombre;
						move_uploaded_file($_FILES[$campo]['tmp_name'], $uploadfile);
					}
				}
			}
		}
		
		echo "<b>$texto_creado</b>";
		$accion = "formcrear";
//--------------------------------------------
		}
		else
		{
			echo "<SCRIPT language='JavaScript'> alert('Alerta. El dato destino es obligatorio, solo se puede elegir uno de los tres. No se puede continuar.'); history.back(); </SCRIPT>";
			exit;
		}
	}
	else
	{
		echo "<SCRIPT language='JavaScript'> alert('Alerta. El dato origen es obligatorio, solo se puede elegir uno de los tres. No se puede continuar.'); history.back(); </SCRIPT>";
		exit;
	}
}
// FIN ACCION CREAR

// ACCION MODIFICAR 
if ($accion == "accionmodificar")
{
	if (($_POST['origen_cliente_id'] > 0 && $_POST['origen_proveedor_id'] == 0 && $_POST['origen_interna'] == "") || ($_POST['origen_cliente_id'] == 0 && $_POST['origen_proveedor_id'] > 0 && $_POST['origen_interna'] == "") || ($_POST['origen_cliente_id'] == 0 && $_POST['origen_proveedor_id'] == 0 && $_POST['origen_interna'] == "on"))
	{
		if (($_POST['destino_cliente_id'] > 0 && $_POST['destino_proveedor_id'] == 0 && $_POST['destino_interna'] == "") || ($_POST['destino_cliente_id'] == 0 && $_POST['destino_proveedor_id'] > 0 && $_POST['destino_interna'] == "") || ($_POST['destino_cliente_id'] == 0 && $_POST['destino_proveedor_id'] == 0 && $_POST['destino_interna'] == "on"))
		{
//--------------------------------------------
		$campos_fin_incidencia = "";
		if ($_POST['estado_inci_id'] == 3)
		{
			$campos_fin_incidencia = " fecha_fin=now(), hora_fin=now(), usuario_fin_id='$user_id', ";
		}
		$consulta2  = "update $tabla set $campos_fin_incidencia";
		// Preparamos los campos a insertar
		foreach ($campos_col1 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$cuenta_campos]);
			$nombre_campo = $nombres_col1[$cuenta_campos];
			if ($ele == "date")
			{
				$campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
				if ((($_POST[$campo_fecha_dia] == "00" || $_POST[$campo_fecha_dia] == "") && ($_POST[$campo_fecha_mes] == "00" || $_POST[$campo_fecha_mes] == "") && ($_POST[$campo_fecha_ano] == "0000" || $_POST[$campo_fecha_ano] == "")) || ($_POST[$campo_fecha_dia] > 0 && $_POST[$campo_fecha_dia] < 32 && $_POST[$campo_fecha_mes] > 0 && $_POST[$campo_fecha_mes] < 13 && $_POST[$campo_fecha_ano] > 0))
				{ $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD-MM-AAAA.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "telefono")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg('([0-9]{9})', $_POST[$campo])) { $consulta2 .= " $campo=\"$_POST[$campo]\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo telefonico ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 123456789.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "dni")
			{
				if ($_POST[$campo] != "")
				{
					if (strlen($_POST[$campo]) == 9 && (ereg('([0-9]{8}[a-zA-Z]{1})', $_POST[$campo]) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtoupper($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de DNI ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12345678A o A123456B.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "cuenta")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo"; $campo4 = "cuatro$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "" && $_POST[$campo4] == "") || (strlen($_POST[$campo1]) == 4 && strlen($_POST[$campo2]) == 4 && strlen($_POST[$campo3]) == 2 && strlen($_POST[$campo4]) == 10))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]$_POST[$campo4]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de cuenta corriente ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 1234-5678-90-1234567890.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "email")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg($patron_comprobacion_email, strtolower($_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtolower($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de email ($nombre_campo) se ha introducido con un formato erroneo.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "multiple")
			{
				$array_multiple_ids = array();
				$consulta_elementos = "select * from $ele3;";
				$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
				{
					$array_multiple_ids[] = $linea_elementos[$ele5];
				}
				$valor_campo = "";
				foreach ($array_multiple_ids as $multiple_id)
				{
					$nombre_campo = $ele7.$multiple_id;
					if ($_POST[$nombre_campo] == "on")
					{
						if ($valor_campo != "") { $valor_campo .= ";"; }
						$valor_campo .= $multiple_id;
					}
				}
				$consulta2 .= " $campo=\"$valor_campo\",";
			}
			elseif ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						if ($extension == "jpg" || $extension == "png")
						{
							$binario_peso = $_FILES[$campo]['size']; 
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='#id#slider.$extension',";
							}
						}
						else
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) debe estar en formato jpg o png. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				} // fin if files name != ""
			}
			elseif ($ele == "file")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						$existe = 0;
						if (in_array($extension,$array_ext_excluidas)) { $existe = 1; }
						if ($existe == 1)
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) tiene un formato no permitido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
						else
						{
							$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
							$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
							
							$binario_peso = $_FILES[$campo]['size']; 
							$binario_tipo = $_FILES[$campo]['type']; 
							
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='$nombre_fichero', tipo_fichero='$binario_tipo', peso_fichero='$binario_peso', fichero_binario='$binario_contenido', fecha_subida=now(), user_id='$user_id',";
							}
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				}
			}
			elseif ($ele == "ss")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "") || (strlen($_POST[$campo1]) == 2 && strlen($_POST[$campo2]) == 8 && strlen($_POST[$campo3]) == 2))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de seguridad social ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12-34567890-12.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "calendar")
			{
				if ($_POST[$campo] != "")
				{
					list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
					if ($ele2 != 1)
					{
						if (($dia == 0 && $mes == 0 && $ano == 0) || ($dia != "" && strlen($dia) == 2 && $dia > 0 && $dia < 32 && $mes != "" && strlen($mes) == 2 && $mes > 0 && $mes < 13 && $ano != "" && strlen($ano) == 4 && $ano > 1900))
						{
							$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
						}
						else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD/MM/AAAA.'); history.back(); </SCRIPT>"; exit; }
					}
					else
					{
						$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
					}
				}
			}
			elseif ($ele == "time")
			{
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora == "00" && $valor_minutos == "00") || ($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{ $consulta2 .= " $campo=\"$valor_hora:$valor_minutos\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "textarea") { $consulta2 .= " $campo='".str_replace("'","",str_replace($texto_textarea,"",$_POST[$campo]))."',"; }
			else { $consulta2 .= " $campo='".str_replace("'","",$_POST[$campo])."',"; }
		}
		foreach ($campos_col2 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col2[$cuenta_campos]);
			$nombre_campo = $nombres_col2[$cuenta_campos];
			if ($ele == "date")
			{
				$campo_fecha_dia = "dia$campo"; $campo_fecha_mes = "mes$campo"; $campo_fecha_ano = "ano$campo";
				if ((($_POST[$campo_fecha_dia] == "00" || $_POST[$campo_fecha_dia] == "") && ($_POST[$campo_fecha_mes] == "00" || $_POST[$campo_fecha_mes] == "") && ($_POST[$campo_fecha_ano] == "0000" || $_POST[$campo_fecha_ano] == "")) || ($_POST[$campo_fecha_dia] > 0 && $_POST[$campo_fecha_dia] < 32 && $_POST[$campo_fecha_mes] > 0 && $_POST[$campo_fecha_mes] < 13 && $_POST[$campo_fecha_ano] > 0))
				{ $consulta2 .= " $campo=\"$_POST[$campo_fecha_ano]-$_POST[$campo_fecha_mes]-$_POST[$campo_fecha_dia]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD-MM-AAAA.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "telefono")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg('([0-9]{9})', $_POST[$campo])) { $consulta2 .= " $campo=\"$_POST[$campo]\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo telefonico ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 123456789.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "dni")
			{
				if ($_POST[$campo] != "")
				{
					if (strlen($_POST[$campo]) == 9 && (ereg('([0-9]{8}[a-zA-Z]{1})', $_POST[$campo]) || ereg('([a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1})', $_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtoupper($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de DNI ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12345678A o A123456B.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "cuenta")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo"; $campo4 = "cuatro$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "" && $_POST[$campo4] == "") || (strlen($_POST[$campo1]) == 4 && strlen($_POST[$campo2]) == 4 && strlen($_POST[$campo3]) == 2 && strlen($_POST[$campo4]) == 10))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]$_POST[$campo4]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de cuenta corriente ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 1234-5678-90-1234567890.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "email")
			{
				if ($_POST[$campo] != "")
				{
					if (ereg($patron_comprobacion_email, strtolower($_POST[$campo])))
					{ $consulta2 .= " $campo=\"".strtolower($_POST[$campo])."\","; }
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de email ($nombre_campo) se ha introducido con un formato erroneo.'); history.back(); </SCRIPT>"; exit; }
				}
				else { $consulta2 .= " $campo='',"; }
			}
			elseif ($ele == "multiple")
			{
				$array_multiple_ids = array();
				$consulta_elementos = "select * from $ele3;";
				$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
				{
					$array_multiple_ids[] = $linea_elementos[$ele5];
				}
				$valor_campo = "";
				foreach ($array_multiple_ids as $multiple_id)
				{
					$nombre_campo = $ele7.$multiple_id;
					if ($_POST[$nombre_campo] == "on")
					{
						if ($valor_campo != "") { $valor_campo .= ";"; }
						$valor_campo .= $multiple_id;
					}
				}
				$consulta2 .= " $campo=\"$valor_campo\",";
			}
			elseif ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						if ($extension == "jpg" || $extension == "png")
						{
							$binario_peso = $_FILES[$campo]['size']; 
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='#id#slider.$extension',";
							}
						}
						else
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) debe estar en formato jpg o png. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				} // fin if files name != ""
			}
			elseif ($ele == "file")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					// Obtener del array FILES (superglobal) los datos del binario .. nombre, tamano y tipo. 
					$nombre_fichero = ComprobarNombreArchivo($_FILES[$campo]['name']);
					$posicion_punto = strrpos($nombre_fichero,".");
					if (is_bool($posicion_punto) == false)
					{
						$extension = strtolower(substr($nombre_fichero, $posicion_punto+1));
						
						$existe = 0;
						if (in_array($extension,$array_ext_excluidas)) { $existe = 1; }
						if ($existe == 1)
						{
							echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo ($nombre_campo) tiene un formato no permitido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
						}
						else
						{
							$binario_nombre_temporal = $_FILES[$campo]['tmp_name'] ; 
							$binario_contenido = addslashes(fread(fopen($binario_nombre_temporal, "rb"), filesize($binario_nombre_temporal))); 
							
							$binario_peso = $_FILES[$campo]['size']; 
							$binario_tipo = $_FILES[$campo]['type']; 
							
							if ($binario_peso < 1)
							{
								echo "<SCRIPT language='JavaScript'> alert('Alerta. Se ha sobrepasado el tama�o maximo del archivo a subir. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
							}
							else
							{
								$consulta2 .= " nombre_fichero='$nombre_fichero', tipo_fichero='$binario_tipo', peso_fichero='$binario_peso', fichero_binario='$binario_contenido', fecha_subida=now(), user_id='$user_id',";
							}
						}
					}
					else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El archivo del campo ($nombre_campo) no tiene extension. No se puede continuar.'); history.back(); </SCRIPT>"; exit; }
				}
			}
			elseif ($ele == "ss")
			{
				$campo1 = "uno$campo"; $campo2 = "dos$campo"; $campo3 = "tres$campo";
				if (($_POST[$campo1] == "" && $_POST[$campo2] == "" && $_POST[$campo3] == "") || (strlen($_POST[$campo1]) == 2 && strlen($_POST[$campo2]) == 8 && strlen($_POST[$campo3]) == 2))
				{ $consulta2 .= " $campo=\"$_POST[$campo1]$_POST[$campo2]$_POST[$campo3]\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de seguridad social ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto 12-34567890-12.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "calendar")
			{
				if ($_POST[$campo] != "")
				{
					list($dia, $mes, $ano) = explode('/', $_POST[$campo]);
					if ($ele2 != 1)
					{
						if (($dia == 0 && $mes == 0 && $ano == 0) || ($dia != "" && strlen($dia) == 2 && $dia > 0 && $dia < 32 && $mes != "" && strlen($mes) == 2 && $mes > 0 && $mes < 13 && $ano != "" && strlen($ano) == 4 && $ano > 1900))
						{
							$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
						}
						else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de fecha ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto DD/MM/AAAA.'); history.back(); </SCRIPT>"; exit; }
					}
					else
					{
						$consulta2 .= " $campo=\"$ano-$mes-$dia\",";
					}
				}
			}
			elseif ($ele == "time")
			{
				$campo_hora = "hora$campo"; $campo_minutos = "minutos$campo";
				$valor_hora = str_pad($_POST[$campo_hora], 2, "0", STR_PAD_LEFT);
				$valor_minutos = str_pad($_POST[$campo_minutos], 2, "0", STR_PAD_LEFT);
				if (($valor_hora == "00" && $valor_minutos == "00") || ($valor_hora > -1 && $valor_hora < 25 && $valor_minutos > -1 && $valor_minutos < 59))
				{ $consulta2 .= " $campo=\"$valor_hora:$valor_minutos\","; }
				else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de hora ($nombre_campo) se ha introducido con un formato erroneo. Formato correcto hh:mm.'); history.back(); </SCRIPT>"; exit; }
			}
			elseif ($ele == "textarea") { $consulta2 .= " $campo='".str_replace("'","",str_replace($texto_textarea,"",$_POST[$campo]))."',"; }
			else { $consulta2 .= " $campo='".str_replace("'","",$_POST[$campo])."',"; }
		}
		// Eliminamos la coma final
		$consulta2 = substr($consulta2, 0, strlen($consulta2)-1);
		$consulta2 .= " where id='$_POST[id]';";
		//echo "$consulta2<br>";
	
		$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());
		
		// RENOMBRAR CORRECTAMENTE LA IMAGEN DE FONDO
		foreach ($campos_col1 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col1[$cuenta_campos]);
			if ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$consulta_datos = "select $campo, id from $tabla where id='$_POST[id]';";
					$resultado_datos = mysql_query($consulta_datos) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_datos = mysql_fetch_array($resultado_datos, MYSQL_ASSOC))
					{
						$nuevo_nombre = str_replace('#id#',$linea_datos['id'],$linea_datos[$campo]);
						
						$consulta_datos2 = "update $tabla set $campo='$nuevo_nombre' where id='$linea_datos[id]';";
						//echo "$consulta_datos2<br>";
						$resultado_datos2 = mysql_query($consulta_datos2) or die("La consulta fall&oacute;: " . mysql_error());
						
						$uploadfile = $uploaddir_contenidos.$nuevo_nombre;
						move_uploaded_file($_FILES[$campo]['tmp_name'], $uploadfile);
					}
				}
			}
		}
		foreach ($campos_col2 as $cuenta_campos => $campo)
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7) = explode(';', $tipos_col2[$cuenta_campos]);
			if ($ele == "fileCarp")
			{
				if ($_FILES[$campo]['name'] != "")
				{
					$consulta_datos = "select $campo, id from $tabla where id='$_POST[id]';";
					$resultado_datos = mysql_query($consulta_datos) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_datos = mysql_fetch_array($resultado_datos, MYSQL_ASSOC))
					{
						$nuevo_nombre = str_replace('#id#',$linea_datos['id'],$linea_datos[$campo]);
						
						$consulta_datos2 = "update $tabla set $campo='$nuevo_nombre' where id='$linea_datos[id]';";
						//echo "$consulta_datos2<br>";
						$resultado_datos2 = mysql_query($consulta_datos2) or die("La consulta fall&oacute;: " . mysql_error());
						
						$uploadfile = $uploaddir_contenidos.$nuevo_nombre;
						move_uploaded_file($_FILES[$campo]['tmp_name'], $uploadfile);
					}
				}
			}
		}
		
		echo "<b>$texto_modificado</b>";
		$accion = "formcrear";
//--------------------------------------------
		}
		else
		{
			echo "<SCRIPT language='JavaScript'> alert('Alerta. El dato destino es obligatorio, solo se puede elegir uno de los tres. No se puede continuar.'); history.back(); </SCRIPT>";
			exit;
		}
	}
	else
	{
		echo "<SCRIPT language='JavaScript'> alert('Alerta. El dato origen es obligatorio, solo se puede elegir uno de los tres. No se puede continuar.'); history.back(); </SCRIPT>";
		exit;
	}
}
// FIN ACCION MODIFICAR 

// ACCION BORRAR
if ($accion == "accionborrar")
{
	$consulta2 = "delete from $tabla where id='$_POST[id]';";
	//echo "$consulta2";
	$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());
	echo "<b>$texto_borrado</b>";
	$accion = "formcrear";
}
// FIN ACCION BORRAR

// ACCION ENVIAR
if ($accion == "accionenviar")
{
	//jgs
	$valor_email = strtolower($_POST['email']);
	if ($valor_email != "" && ereg($patron_comprobacion_email, $valor_email))
	{
		$array_datos = array();
		$array_datos = DatosIncidencia($_REQUEST['id']);
		
		$array_archivos = array();
		$mensaje_correo = "En la incidencia con los siguientes datos:<br>
<b>Origen</b> ".utf8_decode($array_datos[0])."<br>
<b>Destino</b> ".utf8_decode($array_datos[1])."<br>
<b>Fecha</b> ".$array_datos[2]."<br>
<b>Tipo</b> ".utf8_decode($array_datos[3])."<br>
<b>Estado</b> ".utf8_decode($array_datos[4])."<br>
Se ha a�adido el siguiente comentario:<br><br>
";
		
		$array_detalles = array();
		$consulta2 = "select * from $tabla_detalles where $tabla_detalles.incidencia_id='$_REQUEST[id]' order by id;";
		$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: $consulta2 " . mysql_error());
		while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC))
		{
			$array_detalles[] = $linea2['id'];
			$fecha_detalle = "";
			if ($linea2['fecha'] != "0000-00-00")
			{
				$dato_fecha = $linea2['fecha'];
				if ($linea2['hora'] != "00:00:00")
				{
					$dato_fecha .= " ".$linea2['hora'];
				}
				$fecha_detalle = date("d-m-Y H:i:s",strtotime($dato_fecha));
			}
			$mensaje_correo .= "<b>$fecha_detalle</b>: ".utf8_decode($linea2['observaciones'])."<br>";
			$nombre_archivo = $linea2['id']."_".$linea2['nombre_fichero'];
			if ($nombre_archivo != "")
			{
				$direccion_archivo = $uploaddir_incidencias.$nombre_archivo;
				$archivo_envio = fopen($direccion_archivo, "w") or die("can't open file"); // con el parametro w si existe el fichero lo borra y sino lo crea
				if ($archivo_envio)
				{
					fwrite($archivo_envio, $linea2['fichero_binario']);
					fclose($archivo_envio);
					$array_archivos[] = array($uploaddir_incidencias, $nombre_archivo);
				}
			}
		}
		
		$resultado_envio = EnviarEmail($usuario_correo, "JOSE AYALA E HIJO", array($valor_email), "", "", "Actualizacion incidencia", $mensaje_correo, $array_archivos);
		$posicion_error = strrpos($resultado_envio,"error");
		if (is_bool($posicion_error) == false)
		{
			echo "<!--$resultado_envio<br>-->";
			echo "<b>No se pudo enviar la notificacion</b>";
		}
		else
		{
			$consulta_basica = "insert into log_incidencias_t set user_id='$user_id', fecha='".date('Y-m-d')."', hora='".date('H:i:s')."', email_envio='$valor_email'";
			foreach ($array_detalles as $detalle_id)
			{
				$consulta_2 = $consulta_basica.", detalle_incidencia_id='$detalle_id';";
				//echo "$consulta_2<br>";
				$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
			}
			echo "<b>Notificacion enviada con exito</b>";
		}
		// borrado de archivos adjuntos en caso de existir
		if (count($array_archivos) > 0)
		{
			foreach ($array_archivos as $array_datos_archi)
			{
				$direccion_borrado = $carpeta_incidencias.$separador_carpeta.$array_datos_archi[1];
				$resultado_borrado = unlink($direccion_borrado);
			}
		}
		
		$accion = "formcrear";
	}
	else { echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo de email se ha introducido con un formato erroneo.'); history.back(); </SCRIPT>"; exit; }
}
// FIN ACCION ENVIAR

// COMIENZA EL SCRIPT

if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$nombre_padre = "";
		$pais_id = "";
		$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
		//echo "$consulta_padre";
		$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
		while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
		{
			$nombre_padre = $linea_padre['nombre']; $pais_id = $linea_padre['pais_id'];
		}
	}
        
	echo "
<center><b>$titulo $nombre_padre</b><br>";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != $campopadre && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	//echo "[<a href='$enlacevolver"."$script_provincias&pais_id=$pais_id$parametros"."&id=".$$campopadre."'>Volver a provincias</a>]";
	//echo "[<a href='$enlacevolver"."$script&accion=formcrear&pag=$pag'>$texto_crear</a>]";
	echo "
	<br><b>Buscar por</b>
	<form name=form_buscar1 method=post action='$enlacevolver"."$script'>
	<input type=hidden name=pag value=0>";
	echo "<img src=images/p.jpg onload=document.form_buscar1.b_or_cli.focus();>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	echo "
	<table width='100%' style='background-color:#$color_fondo_amarillo; border: 1px solid #$color_fondo_amarillo;'>
		<tr>
			<td rowspan='3' style='border-right: 1px solid #000000; width:100px;' style='text-align:center;'><i><b>Origen</b></i></td>
			<td><b>Cliente</b>: <input type='text' name='b_or_cli' value='".$b_or_cli."'>";
	echo "</td>
			<td rowspan='3' style='border-right: 1px solid #000000; width:100px;' style='text-align:center;'><i><b>Destino</b></i></td>
			<td><b>Cliente</b>: <input type='text' name='b_de_cli' value='".$b_de_cli."'>";
	echo "</td>
		</tr>
		<tr>
			<td><b>Proveedor</b>: <input type='text' name='b_or_prov' value='".$b_or_prov."'>";
	echo "</td>
			<td><b>Proveedor</b>: <input type='text' name='b_de_prov' value='".$b_de_prov."'>";
	echo "</td>
		</tr>
		<tr>
			<td><b>Interna</b> <input type=checkbox name='b_or_int'"; if ($b_or_int == "on") { echo " checked"; } echo "></td>
			<td><b>Interna</b> <input type=checkbox name='b_de_int'"; if ($b_de_int == "on") { echo " checked"; } echo "></td>
		</tr>
	</table>
	<table width='100%' style='background-color:#$color_fondo_amarillo;'>
		<tr style='text-align:center;'>
			<td><b>Estado</b>: <select name='b_estado'><option value=''>Todos</option>";
	$consulta_cat = "select * from maestro_estados_inci_t order by nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $b_estado) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select></td>
			<td><b>Fecha desde</b>: D: <input type='text' name='dia1' size='2' maxlength='2' value='$dia1' onkeyup='validar(dia1);'> 
M: <input type='text' name='mes1' size='2' maxlength='2' value='$mes1' onkeyup='validar(mes1);'> 
A: <input type='text' name='ano1' size='4' maxlength='4' value='$ano1' onkeyup='validar(ano1);'></td>
			<td><b>Fecha hasta</b>: D: <input type='text' name='dia2' size='2' maxlength='2' value='$dia2' onkeyup='validar(dia2);'> 
M: <input type='text' name='mes2' size='2' maxlength='2' value='$mes2' onkeyup='validar(mes2);'> 
A: <input type='text' name='ano2' size='4' maxlength='4' value='$ano2' onkeyup='validar(ano2);'></td>
			<td><b>Tipo</b>: <select name='b_tipo'><option value=''>Todos</option>";
	$consulta_cat = "select * from maestro_tipos_incidencias_t order by nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $b_tipo) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select></td>
		</tr>
		<tr style='text-align:center;'>
			<td colspan='2'><b>Usuario gestiona</b>: <select name='b_usu_ges'><option value=''>Todos</option>";
	$consulta_cat = "select usuarios_t.id, usuarios_t.nombre from usuarios_t join $tabla on $tabla.usuario_gestion_id=usuarios_t.id group by usuarios_t.id order by usuarios_t.nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $b_estado) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select></td>
			<td colspan='2'><input type=submit value='Filtrar'></td>
		</tr>
	</table>
	</form>
";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "
	<a href='$enlacevolver"."$script&pag=0$parametros'>$texto_listado_general</a>
</center>";
}

// OBTENEMOS EL LISTADO DE REGISTROS 
if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$condiciones = " $tabla.$campopadre='".$$campopadre."' ";
		$parametros = "&$campopadre=".$$campopadre;
        }
	else
	{
		$condiciones = "";
		$parametros = "";
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
		if ($ele == "texto" && $ele2 != "")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2 like '%".$$nombre_param."%' ";
				$$nombre_param = str_replace(' ','+',$$nombre_param); 
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "select")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2='".$$nombre_param."' ";
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "fecha")
		{
			if ($$ele3 != "" && $$ele4 != "" && $$ele5 != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				($ele6 == "desde" ? $condiciones .= " $tabla.$ele2>='".$$ele3."-".$$ele4."-".$$ele5." 00:00:00' " : $condiciones .= " $tabla.$ele2<='".$$ele3."-".$$ele4."-".$$ele5." 23:59:59' ");
				$parametros .= "&$ele3=".$$ele3."&$ele4=".$$ele4."&$ele5=".$$ele5;
			}
		}
	}
	if ($b_or_cli != "")
	{
		$array_clientes_ids = array();
		//desgloso b_or_cli si tiene espacios en blanco
		$trozos = array();
		$trozos = explode(" ",$b_or_cli);
		$condi_cli = "";
		foreach ($trozos as $valor)
		{
			if ($condi_cli != "") { $condi_cli .= " and "; }
			$condi_cli .= "(clientes_t.nombre like '%$valor%' or clientes_t.nombre_corto like '%$valor%')";
		}
		if ($condi_cli != "")
		{
			$cons = "select id from clientes_t where $condi_cli;";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$array_clientes_ids[] = $lin['id'];
			}
		}
		if ($condiciones != "") { $condiciones .= " and "; }
		if (count($array_clientes_ids) > 0)
		{
			$temp = CrearCondicionConsulta($array_clientes_ids,$tabla,"origen_cliente_id","=","or");
			$condiciones .= "($temp)";
		}
		else
		{
			$condiciones .= "$tabla.origen_cliente_id='-1'";
		}
		$parametros .= "&b_or_cli=".$b_or_cli;
	}
	if ($b_de_cli != "")
	{
		$array_clientes_ids = array();
		//desgloso b_de_cli si tiene espacios en blanco
		$trozos = array();
		$trozos = explode(" ",$b_de_cli);
		$condi_cli = "";
		foreach ($trozos as $valor)
		{
			if ($condi_cli != "") { $condi_cli .= " and "; }
			$condi_cli .= "(clientes_t.nombre like '%$valor%' or clientes_t.nombre_corto like '%$valor%')";
		}
		if ($condi_cli != "")
		{
			$cons = "select id from clientes_t where $condi_cli;";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$array_clientes_ids[] = $lin['id'];
			}
		}
		if ($condiciones != "") { $condiciones .= " and "; }
		if (count($array_clientes_ids) > 0)
		{
			$temp = CrearCondicionConsulta($array_clientes_ids,$tabla,"destino_cliente_id","=","or");
			$condiciones .= "($temp)";
		}
		else
		{
			$condiciones .= "$tabla.destino_cliente_id='-1'";
		}
		$parametros .= "&b_de_cli=".$b_de_cli;
	}
	if ($b_or_prov != "")
	{
		$array_prov_ids = array();
		//desgloso b_or_prov si tiene espacios en blanco
		$trozos = array();
		$trozos = explode(" ",$b_or_prov);
		$condi_prov = "";
		foreach ($trozos as $valor)
		{
			if ($condi_prov != "") { $condi_prov .= " and "; }
			$condi_prov .= "(proveedores_t.nombre like '%$valor%' or proveedores_t.nombre_corto like '%$valor%')";
		}
		if ($condi_prov != "")
		{
			$cons = "select id from proveedores_t where proveedores_t.inactivo='' and $condi_prov;";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$array_prov_ids[] = $lin['id'];
			}
		}
		if ($condiciones != "") { $condiciones .= " and "; }
		if (count($array_prov_ids) > 0)
		{
			$temp = CrearCondicionConsulta($array_prov_ids,$tabla,"origen_proveedor_id","=","or");
			$condiciones .= "($temp)";
		}
		else
		{
			$condiciones .= "$tabla.origen_proveedor_id='-1'";
		}
		$parametros .= "&b_or_prov=".$b_or_prov;
	}
	if ($b_de_prov != "")
	{
		$array_prov_ids = array();
		//desgloso b_de_prov si tiene espacios en blanco
		$trozos = array();
		$trozos = explode(" ",$b_de_prov);
		$condi_prov = "";
		foreach ($trozos as $valor)
		{
			if ($condi_prov != "") { $condi_prov .= " and "; }
			$condi_prov .= "(proveedores_t.nombre like '%$valor%' or proveedores_t.nombre_corto like '%$valor%')";
		}
		if ($condi_prov != "")
		{
			$cons = "select id from proveedores_t where proveedores_t.inactivo='' and $condi_prov;";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$array_prov_ids[] = $lin['id'];
			}
		}
		if ($condiciones != "") { $condiciones .= " and "; }
		if (count($array_prov_ids) > 0)
		{
			$temp = CrearCondicionConsulta($array_prov_ids,$tabla,"destino_proveedor_id","=","or");
			$condiciones .= "($temp)";
		}
		else
		{
			$condiciones .= "$tabla.destino_proveedor_id='-1'";
		}
		$parametros .= "&b_de_prov=".$b_de_prov;
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && $nombre_param != $campopadre && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	$string_para_select = "";
	foreach($campos_necesarios_listado as $indice => $campo)
	{
		$string_para_select .= $tablas_campos_necesarios[$indice].".$campo,";
	}
	echo "<table width=100%>
	<tr bgcolor='#$color_fondo'>";
	echo "<td><font color='#ffffff'><b>Origen</b></font></td>";
	echo "<td><font color='#ffffff'><b>Destino</b></font></td>";
	$columnas = 0;
	foreach ($campos_listado as $campo) {
		echo "<td><a class='listadoTabla' href='$enlacevolver"."$script&orderby=$campo&pag=$pag$parametros'>$nombres_listado[$columnas]</a></td>";
		$string_para_select .= " ".$tablas_campos_listado[$columnas].".$campo,";
		$columnas++;
	}
	$columnas += 3;
	// Eliminamos el ultimo caracter
	$string_para_select = substr($string_para_select,0,-1);
	echo "<td><font color='#ffffff'><b>Acciones</b></font></td>";
	echo "</tr>";

	$consulta  = "select $string_para_select from $tabla";
	if ($condiciones != "") { $consulta .= " where $condiciones "; }
	// Vemos si existe un orderby
	if ($_REQUEST[orderby] != "")
	{
		$order = " order by $tabla.$_REQUEST[orderby]";
		$parametros .= "&orderby=".$_REQUEST[orderby];
	}
	else { $order = " order by $tabla.$campo_busqueda desc, $tabla.hora_inicio desc";  }
	if ($pag != "") {
		// la primera pagina es la 0
		$inicio = $pag*$registros_por_pagina;
		$limit = " limit $inicio,$registros_por_pagina";
	}
	else { $limit = ""; }
	$consulta .= "$order $limit;";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: $consulta " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		if ($_GET[id] != "" && $linea[id] == $_GET[id]) { $bgcolor = " bgcolor='#$color_fondo_claro'"; }
		else { $bgcolor = ""; }
		
		echo "<tr $bgcolor>";
		// origen
		$dato_origen = "";
		if ($linea['origen_cliente_id'] > 0)
		{
			$consultaselect = "select * from clientes_t where id='$linea[origen_cliente_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_origen = $lineaselect['nombre_corto'];
			}
			$dato_origen = "(C) ".$dato_origen;
		}
		elseif ($linea['origen_proveedor_id'] > 0)
		{
			$consultaselect = "select * from proveedores_t where id='$linea[origen_proveedor_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_origen = $lineaselect['nombre_corto'];
			}
			$dato_origen = "(P) ".$dato_origen;
		}
		elseif ($linea['origen_interna'] == "on")
		{
			$dato_origen = "(I) Interna";
		}
		// destino
		$dato_destino = "";
		if ($linea['destino_cliente_id'] > 0)
		{
			$consultaselect = "select * from clientes_t where id='$linea[destino_cliente_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_destino = $lineaselect['nombre_corto'];
			}
			$dato_destino = "(C) ".$dato_destino;
		}
		elseif ($linea['destino_proveedor_id'] > 0)
		{
			$consultaselect = "select * from proveedores_t where id='$linea[destino_proveedor_id]';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$dato_destino = $lineaselect['nombre_corto'];
			}
			$dato_destino = "(P) ".$dato_destino;
		}
		elseif ($linea['destino_interna'] == "on")
		{
			$dato_destino = "(I) Interna";
		}
		echo "<td>$dato_origen</td>";
		echo "<td>$dato_destino</td>";
		foreach ($campos_listado as $cuenta_campos => $campo)
		{
			$nombre = "";
			list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod[$cuenta_campos]);
			if ($ele != 'si')
			{
				$nombre = "$linea[$campo]";
			}
			elseif ($ele2 == "date")
			{
				if ($linea[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
				else { $nombre = date("d-m-Y",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "datetime")
			{
				if ($linea[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
				else { $nombre = date("d-m-Y H:i",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "checkbox")
			{
				($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
			}
			elseif ($ele2 == "time")
			{
				list($temp1, $temp2, $temp3) = explode(':',$linea[$campo]);
				$nombre = $temp1.":".$temp2;
			}
			elseif ($linea[$campo] != "")
			{
				$consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre = $lineaselect[$ele3];
				}
			}
			echo "<td>$nombre</td>";
		}
		echo "<td>
			<a href='$enlacevolver"."$script_detalle&incidencia_id=$linea[id]&pag=0'><img src='images/paste_plain.png' alt='Detalles' title='Detalles' border='0' /></a> ";
		$cuenta_detalles = 0;
		$cons = "select count($tabla_detalles.id) as total from $tabla_detalles where $tabla_detalles.incidencia_id='".$linea['id']."';";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$cuenta_detalles = $lin['total'];
		}
		echo "($cuenta_detalles) ";
		echo "
			<a href='$enlacevolver"."$script&accion=formenviar&id=$linea[id]&pag=$pag$parametros'><img src='images/email.png' alt='Notificar todos los detalles' title='Notificar todos los detalles' border='0' /></a> ";
		echo "
			<a href='$enlacevolver"."$script_log&incidencia_id=$linea[id]&pag=0'><img src='images/email_go.png' alt='Historial envios' title='Historial envios' border='0' /></a> ";
		$cuenta_envios = 0;
		$cons = "select count(log_incidencias_t.id) as total from log_incidencias_t join $tabla_detalles on $tabla_detalles.id=log_incidencias_t.detalle_incidencia_id where $tabla_detalles.incidencia_id='".$linea['id']."';";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			$cuenta_envios = $lin['total'];
		}
		echo "($cuenta_envios) ";
		echo "
			<a href='$enlacevolver"."$script&accion=formmodificar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
		$posibilidad = 0;
		$posibilidad = ComprobarIncidencias($linea['id']);
		if ($posibilidad == 0)
		{
			echo "
			<a href='$enlacevolver"."$script&accion=formborrar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_delete.png' alt='Borrar' title='Borrar' border='0' /></a> ";
		}
		echo "
		</td>";
		echo "</tr>";
		echo "<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas'></td></tr>";
	}
	echo "</table>";
	if ($pag != "")
	{
		$pag_visual = $pag+1;
		// Visualizamos las paginas existentes
		$consulta2  = "select count($tabla.id) as num from $tabla";
		if ($condiciones != "") { $consulta2 .= " where $condiciones;"; } else { $consulta2 .= ";"; }
		$resultado2 = mysql_query($consulta2) or die("$consulta2, La consulta fall&oacute;: " . mysql_error());
		//echo "$consulta2<br>";
		while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC)) {
			$exp = "$linea2[num]";
		}
		if ($exp > $registros_por_pagina)
		{
			echo PaginadoListado($pag, $enlacevolver.$script, $parametros, $exp, $registros_por_pagina);
		}
	}
	echo "<hr>";
}
// FIN DE OBTENER EL LISTADO INICIAL

// FORMULARIO PARA BORRAR UN REGISTRO
if ($accion == "formborrar")
{
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "") { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "") { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "") { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { $parametros .= "&orderby=".$_REQUEST[orderby]; }
	echo "<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<tr>
		<td><center><b>BORRADO DE $nombre_objeto</b> [<a href='$enlacevolver"."$script$parametros'>Volver sin cambios</a>]</center></td>
	</tr>
	<tr valign='top'>
		<td>
		<form name=form_buscar method=post action='$enlacevolver"."$script'>
		<input type=hidden name=accion value=accionborrar>
		<input type=hidden name=id value=$_GET[id]>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "") { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "") { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "") { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { echo "<input type=hidden name=orderby value='".$_REQUEST[orderby]."'>"; }
	// Obtenemos los valores actuales del registro que se esta modificando
	$consultamod = "select * from $tabla where id=$_GET[id];";
	$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: " . mysql_error());
	// El resultado lo metemos en un array asociativo
	$arraymod = array();
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC)) {
		foreach ($campos_col1 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
		foreach ($campos_col2 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
	} // del while

	echo "
		<table width='100%' border='0'>
			<tr><td colspan='2'><input type=submit value='Va usted a borrar el registro con los siguientes datos'></td></tr>
			<tr><td width='50%'><table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col1 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col1[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col1[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col1[$cuenta_campos]);
		$nombre_campo = $nombres_col1[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: $dia M: $mes A: $ano</td></tr>";
		}
		if ($ele == "checkbox") { echo "<tr><td><b>$nombre_campo</b></td><td>";if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; } echo "</td></tr>"; }
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele3 where $ele5='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele4]";
				}
			}
			echo "</td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele2 where $ele4='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele3]";
				}
			}
			echo "</td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3 - $valor4</td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "email") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "float") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
				echo "\n<td align='center' width='5%'>$nombre_elemento</td>";
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			if ($arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			$id_codificado = base64_encode($_GET[id]);
			echo "Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3</td></tr>";
		}
		if ($ele == "calendar")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>$dia/$mes/$ano</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>$hora : $minutos</td></tr>";
		}
		if ($ele == "numerico") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		$cuenta_campos++;
		echo "\n";
	} // del foreach
	echo "</table>";

	// Vamos a por la columna 2
	echo "</td><td>";

	echo "<table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col2 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col2[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col2[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col2[$cuenta_campos]);
		$nombre_campo = $nombres_col2[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: $dia M: $mes A: $ano</td></tr>";
		}
		if ($ele == "checkbox") { echo "<tr><td><b>$nombre_campo</b></td><td>";if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; } echo "</td></tr>"; }
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele3 where $ele5='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele4]";
				}
			}
			echo "</td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele2 where $ele4='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele3]";
				}
			}
			echo "</td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3 - $valor4</td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "email") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "float") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
				echo "\n<td align='center' width='5%'>$nombre_elemento</td>";
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			if ($arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			$id_codificado = base64_encode($_GET[id]);
			echo "Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3</td></tr>";
		}
		if ($ele == "calendar")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>$dia/$mes/$ano</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>$hora : $minutos</td></tr>";
		}
		if ($ele == "numerico") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		$cuenta_campos++;
		echo "\n";
	} // del foreach
	echo "</table>";
	echo "</td></tr></table>";
	echo "</form></td></tr></table>";
}
// FIN FORMULARIO BORRAR

// FORMULARIO PARA LA CREACION/MODIFICACION DE UN NUEVO REGISTRO
if ($accion == "formcrear" || $accion == "formmodificar" || $accion == "")
{
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "") { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "") { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "") { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { $parametros .= "&orderby=".$_REQUEST[orderby]; }
	echo "<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<tr>
		<td colspan='2'><center><b>";
	if ($accion == "formcrear" || $accion == "") { echo "CREACION"; }
	else { echo "MODIFICACION"; }
	echo " DE $nombre_objeto</b>";
	if ($accion == "formmodificar") { echo " [<a href='$enlacevolver"."$script$parametros'>Volver sin cambios</a>]"; }
	echo "</center></td>
	</tr>
	<form enctype='multipart/form-data' name=form_crear method=post action='$enlacevolver"."$script'>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "") { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "") { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "") { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { echo "<input type=hidden name=orderby value='".$_REQUEST[orderby]."'>"; }
	echo "
	<tr valign='top'>
		<td>";
	//echo "<img src=images/p.jpg onload=document.form_crear.".$campos_col1[0].".focus();>";
	$arraymod = array();
	if ($accion == "formcrear" || $accion == "") {
		echo "<input type=hidden name=accion value=accioncrear>";
	}
	if ($accion == "formmodificar") {
		echo "<input type=hidden name=id value='$_GET[id]'>
		<input type=hidden name=accion value=accionmodificar>";
		// Obtenemos los valores actuales del registro que se esta modificando
		$string_para_select = "";
		foreach($campos_col1 as $campo)
		{
			if ($string_para_select != "") { $string_para_select .= ", "; }
			$string_para_select .= "$campo";
		}
		foreach($campos_col2 as $campo)
		{
			if ($string_para_select != "") { $string_para_select .= ", "; }
			$string_para_select .= "$campo";
		}
		$consultamod = "select $string_para_select from $tabla where id=$_GET[id];";
		$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: " . mysql_error());
		// El resultado lo metemos en un array asociativo
		while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC)) {
			foreach ($campos_col1 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
			foreach ($campos_col2 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
		} // del while
	}

	$cuenta_campos = 0;
	foreach($campos_col1 as $campo)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $tipos_col1[$cuenta_campos]);
		if ($ele == "hidden" && $_GET[$ele6] != "") { $arraymod[$campo] = $_GET[$ele6]; }
		$cuenta_campos++;
	}
	$cuenta_campos = 0;
	foreach($campos_col2 as $campo)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $tipos_col2[$cuenta_campos]);
		if ($ele == "hidden" && $_GET[$ele6] != "") { $arraymod[$campo] = $_GET[$ele6]; }
		$cuenta_campos++;
	}

	echo "<table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col1 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col1[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col1[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col1[$cuenta_campos]);
		$nombre_campo = $nombres_col1[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1 && $accion == "formmodificar") { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) {echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($ele2 == 1 && $accion == "formmodificar")
			{
				echo "$arraymod[$campo]<input type=hidden name='$campo' value='$arraymod[$campo]'>";
			}
			else
			{
				echo "<textarea rows='$ele3' cols='$ele4' name='$campo'>$arraymod[$campo]</textarea>";
			}
			echo "</td></tr>";
		}
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: <input type='text' name='dia$campo' size='2' maxlength='2' value='$dia' onkeyup='validar(dia$campo);'"; if ($ele2 == 1) { echo " readonly"; }
			echo "> M: <input type='text' name='mes$campo' size='2' maxlength='2' value='$mes' onkeyup='validar(mes$campo);'"; if ($ele2 == 1) { echo " readonly"; }
			echo "> A: <input type='text' name='ano$campo' size='4' maxlength='4' value='$ano' onkeyup='validar(ano$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "checkbox")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($ele2 == 1)
			{
				if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; }
				echo "<input type=hidden name='$campo' value='$arraymod[$campo]'>";
			}
			else
			{
				echo "<input type=checkbox name='$campo'"; if ($arraymod[$campo] == "on") { echo " checked"; } echo ">";
			}
			echo "</td></tr>";
		}
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($accion == "formcrear" || $accion == "" || ($accion == "formmodificar" && $ele2 != 1))
			{
				$condi_select = "";
				if ($ele7 != "") { $condi_select .= " $ele7"; }
				if ($campo == "origen_proveedor_id" || $campo == "destino_proveedor_id")
				{
					// condicion inactivo=''
					if ($accion == "formmodificar")
					{
						if ($condi_select != "") { $condi_select .= " and "; }
						$condi_select .= "($ele3.inactivo='' or $ele3.id='".$arraymod[$campo]."')";
					}
					else
					{
						if ($condi_select != "") { $condi_select .= " and "; }
						$condi_select .= "$ele3.inactivo=''";
					}
				}
				if ($condi_select != "") { $condi_select = " where ".$condi_select; }
				if ($ele6 != "") { $consultaselect = "select * from $ele3 $condi_select order by $ele6;"; }
				else { $consultaselect = "select * from $ele3 $condi_select;"; }
				if ($campo == "origen_proveedor_id" || $campo == "destino_proveedor_id")
				{
					//echo "$consultaselect<br>";
				}
			}
			else
			{
				if ($ele2 == 1)
				{
					$consultaselect = "select * from $ele3 where $ele3.$ele5='$arraymod[$campo]';";
				}
			}
			echo "<select name='$campo' id='$campo'>";
			if ($campo != "estado_inci_id") { echo "<option value='0'></option>"; }
			//echo "<option>$consultaselect</option>";
			$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
				echo "<option value='$lineaselect[$ele5]'"; if ($lineaselect[$ele5] == "$arraymod[$campo]") { echo " selected"; } echo ">$lineaselect[$ele4]</option>";
			}
			echo "</select></td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>
			<select name='$ele6' onchange=\"saltoPagina('parent',this,0)\"><option value='$enlacevolver"."$script&accion=$accion&pag=$pag&$ele6=0";
			if ($usa_padre != 0)
			{
				$parametros_hidden = "&$campopadre=".$$campopadre;
			}
			else
			{
				$parametros_hidden = "";
			}
			if ($accion == "formmodificar") { echo "&id=$_GET[id]"; }
			$cuenta_hidden = 0;
			foreach ($campos_col1 as $valor_campo) {
				list ($valor, $valor2, $valor3, $valor4, $valor5, $valor6) = explode(';', $tipos_col1[$cuenta_hidden]);
				if ($valor == "hidden" && $valor_campo != $campo) { $parametros_hidden .= "&$valor6=$arraymod[$valor_campo]"; }
				$cuenta_hidden++;
			}
			$cuenta_hidden = 0;
			foreach ($campos_col2 as $valor_campo) {
				list ($valor, $valor2, $valor3, $valor4, $valor5, $valor6) = explode(';', $tipos_col2[$cuenta_hidden]);
				if ($valor == "hidden" && $valor_campo != $campo) { $parametros_hidden .= "&$valor6=$arraymod[$valor_campo]"; }
				$cuenta_hidden++;
			}
			echo "$parametros_hidden'></option>";
			if ($ele7 != "")
			{
				$valor_filtro = ""; $encontrado = 0;
				while ($encontrado == 0)
				{
					foreach ($campos_col1 as $valor_campo) { if ($valor_campo == $ele7) { $encontrado = 1; $valor_filtro = $arraymod[$valor_campo]; } }
				}
				while ($encontrado == 0)
				{
					foreach ($campos_col2 as $valor_campo) { if ($valor_campo == $ele7) { $encontrado = 1; $valor_filtro = $arraymod[$valor_campo]; } }
				}
				$consultaselect = "select $ele2.* from $ele2 where $ele8='$valor_filtro' order by $ele5;";
			}
			else 
			{
				if ($ele5 != "") { $consultaselect = "select * from $ele2 order by $ele5;"; }
				else { $consultaselect = "select * from $ele2;"; }
			}
			//echo "<option>$consultaselect</option>";
			$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
				echo "<option value='$enlacevolver"."$script&accion=$accion&pag=$pag&$ele6=$lineaselect[$ele4]$parametros_hidden";
				if ($accion == "formmodificar") { echo "&id=$_GET[id]'"; } else { echo "'"; }
				if ($lineaselect[$ele4] == $arraymod[$campo]) { echo " selected"; }
				echo ">$lineaselect[$ele3]</option>";
			}
			echo "</select><input type=hidden name=$campo value='$arraymod[$campo]'></td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>
		<input type='text' name='uno$campo' size='4' maxlength='4' value='$valor1' onkeyup='validar(uno$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='dos$campo' size='4' maxlength='4' value='$valor2' onkeyup='validar(dos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='tres$campo' size='2' maxlength='2' value='$valor3' onkeyup='validar(tres$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='cuatro$campo' size='10' maxlength='10' value='$valor4' onkeyup='validar(cuatro$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> <img src='images/money.png'></td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='9' maxlength='9' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='9' maxlength='9' value='$arraymod[$campo]' onkeyup='validar($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "email")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo ">";
			if ($arraymod[$campo] != "") { echo "&nbsp;&nbsp;<a href='mailto:$arraymod[$campo]'><img src='images/email.png' border='0' title='Enviar correo'></a>"; }
			echo "</td></tr>";
		}
		if ($ele == "float")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]' onkeyup='validarFloat($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> (Ej: 1234.56)</td></tr>";
		}
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				if ($ele2 == 1) // no modificable
				{
					($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
					echo "\n<td align='center' width='5%'>$nombre_elemento
					<input type=hidden name='$ele7$linea_elementos[$ele5]' value='".$array_multiple_valor[$linea_elementos[$ele5]]."'></td>";
				}
				else
				{
					echo "\n<td align='center' width='5%'><input type='checkbox' name='$ele7$linea_elementos[$ele5]'";
					if ($array_multiple_valor[$linea_elementos[$ele5]] == "on") { echo " checked"; }
					echo "></td>";
				}
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td><input type=hidden name=MAX_FILE_SIZE value='$tamano_max_archivo' /><input name='$campo' type=file /> <img src='images/camera.png'><br><span syle='font-size:10px;'>Tama&ntilde;o m&aacute;ximo: ".round($tamano_max_archivo/1024,2)." Kb</span><br>";
			if ($accion == "formmodificar" && $arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td><input type=hidden name=MAX_FILE_SIZE value='$tamano_max_archivo' /><input name='$campo' type=file />";
			if ($accion == "formmodificar" && $arraymod[$campo] != "")
			{
				$id_codificado = base64_encode($_GET[id]);
				echo "<br>Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>
		<input type='text' name='uno$campo' size='2' maxlength='2' value='$valor1' onkeyup='validar(uno$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='dos$campo' size='8' maxlength='8' value='$valor2' onkeyup='validar(dos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='tres$campo' size='2' maxlength='2' value='$valor3' onkeyup='validar(tres$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "calendar")
		{
			if ($accion == "formmodificar") { list($fecha, $reloj) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha); }
			else { $ano = "0000"; $mes = "00"; $dia = "00"; }
			echo "<tr><td><b>$nombre_campo</b></td><td>
<input type='text' name='$campo' id='$campo'"; if ($ele2 == 1) { echo " readonly='1'"; } echo " size='10' value='$dia/$mes/$ano'/>";
			if ($ele2 != 1)
			{
				echo "
<img src='images/calendar.gif' name='boton$campo' border='0' id='boton$campo' style='cursor: pointer; border: 0px solid blue;' title='Seleccione fecha entrada' />
<script type='text/javascript'>
	Calendar.setup({
		inputField	:	'$campo',		// id of the input field
		trigger		:	'boton$campo',	// trigger for the calendar (button ID)
		onSelect	:	function() { this.hide() }
	});
</script>";
			}
			echo "</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>
<input type='text' name='hora$campo' size='2' maxlength='2' value='$hora' onkeyup='validar(hora$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> : 
<input type='text' name='minutos$campo' size='2' maxlength='2' value='$minutos' onkeyup='validar(minutos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "numerico")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]' onkeyup='validar($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		$cuenta_campos++;
		echo "\n";
	} // del foreach
	echo "</table>";

	// Vamos a por la columna 2
	echo "</td><td>";

	echo "<table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col2 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col2[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col2[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col2[$cuenta_campos]);
		$nombre_campo = $nombres_col2[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1 && $accion == "formmodificar") { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) {echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($ele2 == 1 && $accion == "formmodificar")
			{
				echo "$arraymod[$campo]<input type=hidden name='$campo' value='$arraymod[$campo]'>";
			}
			else
			{
				echo "<textarea rows='$ele3' cols='$ele4' name='$campo'>$arraymod[$campo]</textarea>";
			}
			echo "</td></tr>";
		}
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: <input type='text' name='dia$campo' size='2' maxlength='2' value='$dia' onkeyup='validar(dia$campo);'"; if ($ele2 == 1) { echo " readonly"; }
			echo "> M: <input type='text' name='mes$campo' size='2' maxlength='2' value='$mes' onkeyup='validar(mes$campo);'"; if ($ele2 == 1) { echo " readonly"; }
			echo "> A: <input type='text' name='ano$campo' size='4' maxlength='4' value='$ano' onkeyup='validar(ano$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "checkbox")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($ele2 == 1)
			{
				if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; }
				echo "<input type=hidden name='$campo' value='$arraymod[$campo]'>";
			}
			else
			{
				echo "<input type=checkbox name='$campo'"; if ($arraymod[$campo] == "on") { echo " checked"; } echo ">";
			}
			echo "</td></tr>";
		}
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><select name='$campo' id='$campo'>";
			echo "<option value='0'></option>";
			if ($accion == "formcrear" || $accion == "" || ($accion == "formmodificar" && $ele2 != 1))
			{
				if ($ele6 != "") { $consultaselect = "select * from $ele3 order by $ele6;"; }
				else { $consultaselect = "select * from $ele3;"; }
			}
			else
			{
				if ($ele2 == 1)
				{
					$consultaselect = "select * from $ele3 where $ele3.$ele5='$arraymod[$campo]';";
				}
			}
			$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
				echo "<option value='$lineaselect[$ele5]'"; if ($lineaselect[$ele5] == "$arraymod[$campo]") { echo " selected"; } echo ">$lineaselect[$ele4]</option>";
			}
			echo "</select></td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>
			<select name='$ele6' onchange=\"saltoPagina('parent',this,0)\"><option value='$enlacevolver"."$script&accion=$accion&pag=$pag&$ele6=0";
			if ($usa_padre != 0)
			{
				$parametros_hidden = "&$campopadre=".$$campopadre;
			}
			else
			{
				$parametros_hidden = "";
			}
			if ($accion == "formmodificar") { echo "&id=$_GET[id]"; }
			$cuenta_hidden = 0;
			foreach ($campos_col1 as $valor_campo) {
				list ($valor, $valor2, $valor3, $valor4, $valor5, $valor6) = explode(';', $tipos_col1[$cuenta_hidden]);
				if ($valor == "hidden" && $valor_campo != $campo) { $parametros_hidden .= "&$valor6=$arraymod[$valor_campo]"; }
				$cuenta_hidden++;
			}
			$cuenta_hidden = 0;
			foreach ($campos_col2 as $valor_campo) {
				list ($valor, $valor2, $valor3, $valor4, $valor5, $valor6) = explode(';', $tipos_col2[$cuenta_hidden]);
				if ($valor == "hidden" && $valor_campo != $campo) { $parametros_hidden .= "&$valor6=$arraymod[$valor_campo]"; }
				$cuenta_hidden++;
			}
			echo "$parametros_hidden'></option>";
			if ($ele7 != "")
			{
				$valor_filtro = ""; $encontrado = 0;
				while ($encontrado == 0)
				{
					foreach ($campos_col1 as $valor_campo) { if ($valor_campo == $ele7) { $encontrado = 1; $valor_filtro = $arraymod[$valor_campo]; } }
				}
				while ($encontrado == 0)
				{
					foreach ($campos_col2 as $valor_campo) { if ($valor_campo == $ele7) { $encontrado = 1; $valor_filtro = $arraymod[$valor_campo]; } }
				}
				$consultaselect = "select $ele2.* from $ele2 where $ele8='$valor_filtro' order by $ele5;";
			}
			else 
			{
				if ($ele5 != "") { $consultaselect = "select * from $ele2 order by $ele5;"; }
				else { $consultaselect = "select * from $ele2;"; }
			}
			//echo "<option>$consultaselect</option>";
			$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
				echo "<option value='$enlacevolver"."$script&accion=$accion&pag=$pag&$ele6=$lineaselect[$ele4]$parametros_hidden";
				if ($accion == "formmodificar") { echo "&id=$_GET[id]'"; } else { echo "'"; }
				if ($lineaselect[$ele4] == $arraymod[$campo]) { echo " selected"; }
				echo ">$lineaselect[$ele3]</option>";
			}
			echo "</select><input type=hidden name=$campo value='$arraymod[$campo]'></td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>
		<input type='text' name='uno$campo' size='4' maxlength='4' value='$valor1' onkeyup='validar(uno$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='dos$campo' size='4' maxlength='4' value='$valor2' onkeyup='validar(dos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='tres$campo' size='2' maxlength='2' value='$valor3' onkeyup='validar(tres$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='cuatro$campo' size='10' maxlength='10' value='$valor4' onkeyup='validar(cuatro$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> <img src='images/money.png'></td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='9' maxlength='9' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='9' maxlength='9' value='$arraymod[$campo]' onkeyup='validar($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "email")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo ">";
			if ($arraymod[$campo] != "") { echo "&nbsp;&nbsp;<a href='mailto:$arraymod[$campo]'><img src='images/email.png' border='0' title='Enviar correo'></a>"; }
			echo "</td></tr>";
		}
		if ($ele == "float")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]' onkeyup='validarFloat($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> (Ej: 1234.56)</td></tr>";
		}
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				if ($ele2 == 1) // no modificable
				{
					($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
					echo "\n<td align='center' width='5%'>$nombre_elemento
					<input type=hidden name='$ele7$linea_elementos[$ele5]' value='".$array_multiple_valor[$linea_elementos[$ele5]]."'></td>";
				}
				else
				{
					echo "\n<td align='center' width='5%'><input type='checkbox' name='$ele7$linea_elementos[$ele5]'";
					if ($array_multiple_valor[$linea_elementos[$ele5]] == "on") { echo " checked"; }
					echo "></td>";
				}
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td><input type=hidden name=MAX_FILE_SIZE value='$tamano_max_archivo' /><input name='$campo' type=file /> <img src='images/camera.png'><br><span syle='font-size:10px;'>Tama&ntilde;o m&aacute;ximo: ".round($tamano_max_archivo/1024,2)." Kb</span><br>";
			if ($accion == "formmodificar" && $arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td><input type=hidden name=MAX_FILE_SIZE value='$tamano_max_archivo' /><input name='$campo' type=file />";
			if ($accion == "formmodificar" && $arraymod[$campo] != "")
			{
				$id_codificado = base64_encode($_GET[id]);
				echo "<br>Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>
		<input type='text' name='uno$campo' size='2' maxlength='2' value='$valor1' onkeyup='validar(uno$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='dos$campo' size='8' maxlength='8' value='$valor2' onkeyup='validar(dos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> - 
		<input type='text' name='tres$campo' size='2' maxlength='2' value='$valor3' onkeyup='validar(tres$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "calendar")
		{
			if ($accion == "formmodificar") { list($fecha, $reloj) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha); }
			else { $ano = "0000"; $mes = "00"; $dia = "00"; }
			echo "<tr><td><b>$nombre_campo</b></td><td>
<input type='text' name='$campo' id='$campo'"; if ($ele2 == 1) { echo " readonly='1'"; } echo " size='10' value='$dia/$mes/$ano'/>";
			if ($ele2 != 1)
			{
				echo "
<img src='images/calendar.gif' name='boton$campo' border='0' id='boton$campo' style='cursor: pointer; border: 0px solid blue;' title='Seleccione fecha entrada' />
<script type='text/javascript'>
	Calendar.setup({
		inputField	:	'$campo',		// id of the input field
		trigger		:	'boton$campo',	// trigger for the calendar (button ID)
		onSelect	:	function() { this.hide() }
	});
</script>";
			}
			echo "</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>
<input type='text' name='hora$campo' size='2' maxlength='2' value='$hora' onkeyup='validar(hora$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "> : 
<input type='text' name='minutos$campo' size='2' maxlength='2' value='$minutos' onkeyup='validar(minutos$campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		if ($ele == "numerico")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td><input type='text' name='$campo' size='$ele3' value='$arraymod[$campo]' onkeyup='validar($campo);'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>";
		}
		$cuenta_campos++;
		echo "\n";
	} // del foreach
	echo "</table>";
	echo "</td></tr>
	<tr><td colspan='2'><input type=submit value='Guardar'></td></tr></form></table>";
}
// FIN FORMULARIO CREACION DE UN NUEVO REGISTRO

// FORMULARIO PARA LA NOTIFICACION
if ($accion == "formenviar")
{
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "") { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "") { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "") { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { $parametros .= "&orderby=".$_REQUEST[orderby]; }
	echo "<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<tr>
		<td colspan='2'><center><b>NOTIFICACI&Oacute;N DE $nombre_objeto</b> [<a href='$enlacevolver"."$script$parametros'>Volver sin cambios</a>]</center></td>
	</tr>
	<form enctype='multipart/form-data' name=form_crear method=post action='$enlacevolver"."$script'>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "") { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "") { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "") { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { echo "<input type=hidden name=orderby value='".$_REQUEST[orderby]."'>"; }
	$valor_email = "";
	$consulta_padre = "select * from $tabla where id='".$_REQUEST['id']."';";
	//echo "$consulta_padre";
	$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
	while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
	{
		// origen
		if ($linea_padre['origen_cliente_id'] > 0)
		{
			$consultaselect = "select * from clientes_contactos_t where cliente_id='$linea_padre[origen_cliente_id]' and principal='on' and email<>'';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$valor_email = $lineaselect['email'];
			}
		}
		elseif ($linea_padre['origen_proveedor_id'] > 0)
		{
			$consultaselect = "select * from prov_contactos_t where proveedor_id='$linea_padre[origen_proveedor_id]' and principal='on' and email<>'';";
			//echo "$consultaselect";
			$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
			while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
			{
				$valor_email = $lineaselect['email'];
			}
		}
	}
	echo "
	<tr valign='top'>
		<td>";
	//echo "<img src=images/p.jpg onload=document.form_crear."."email".".focus();>";
	echo "<input type=hidden name=accion value=accionenviar>";
	echo "<input type=hidden name=id value='$_GET[id]'>";
	echo "<b>Email</b> <input type='text' name='email' size='40' value='$valor_email'></td>
	</tr>
	<tr><td colspan='2'><input type=submit value='Notificar'></td></tr></form></table>";
}
// FIN FORMULARIO PARA LA NOTIFICACION

} // fin de se tiene permiso para este script
else
{
	echo "No se tiene permiso para ver esta seccion.<br><br>";
}

echo "
		</td>
	</tr>
</table>
";
?>