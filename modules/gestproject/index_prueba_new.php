<?php
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $login</b></center><br>";

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// titulo que aparece en la parte superior del script
$titulo = "GESTION DE LOS USUARIOS";
// titulo que aparece en la pesta�a del navegador
$titulo_pagina = "USUARIOS";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";
// texto que aparece en el boton de crear
$titulo_boton_crear = "CREAR NUEVO USUARIO";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 1;
// direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// nombre del fichero
$script = "index_prueba_new";
// nombre de la tabla
$tabla = "usuarios_t";


// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify
// en la plantilla apareceran como [campo]
$campos_col1 = array('login','nombre');


// Ayuda para que el usuario tenga mas informacion sobre que poner o como ponerlo
// en la plantilla apareceran como [campo_ayuda]
$ayudas_col1 = array('No introduzca espacios','Puede introducir espacios');

// Marcar los campos que son obligatorios
// poner on o dejar vacio
$campos_col1_obligatorios = array('','on');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
// Aplicar comprobaciones de tipos de campo
// se tiene que poner => 'pattern="comprobacion_elegida"'
// enteros con o sin decimales (con punto para decimales, con signo, numero indeterminado de decimales)  =>  \-?\d+(\.\d{0,})?
// enteros con o sin decimales (con punto para decimales y dos decimales) (MYSQL float) =>  \d+(\.\d{2})?
// fecha en formato DD/MM/YYYY  =>  (0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d
//--------------------------------------------------------------
// REVISAR: se deberia revisar que al guardar se trocee y se guarde correctamente, en antiguos index_new el tipo calendar lo hacia (de DD/MM/YYYY -> YYYY-MM-DD)
//--------------------------------------------------------------
$campos_col1_mascaras = array('','');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
// Marca los campos de solo lectura
// poner on o dejar vacio
$campos_col1_readonly = array('','');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php
$tipos_col1  = array('text;class="span2"','text;class="span6"');

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
$filtro_noc_para_insert = "";//" noc='$noc', ";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = "";

// Campo para la busqueda
$campo_busqueda = "nombre";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript
$plantilla_insercion = $script.".plantilla.php";
if ($plantilla_insercion != "")
{
	$fichero_absoluto = $dir_raiz . "modules/gestproject/" . $plantilla_insercion;
//--------------------------------------------------------------
// REVISAR: donde se define $dir_raiz
//--------------------------------------------------------------
	//echo $fichero_absoluto;
	if (file_exists($fichero_absoluto))
	{
		$gestor = fopen($fichero_absoluto, "r");
		$contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
		fclose($gestor);
	}
}

// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 1;

// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','login','nombre');
//--------------------------------------------------------------
// REVISAR: en el index new generico actual hay unos array para sacar datos en la consulta sin tener que visualizarlos (tipo id, o que se utilicen despues para mostrar mas o menos opciones)
//--------------------------------------------------------------

// Nombre de los campos del listado
$nombres_listado = array ('','Login','Nombre');

// Decodificacion de los campos del listado
$campos_listado_decod = array ('','','');

//--------------------------------------------------------------
// REVISAR: en el listado inicial:
//  - en index new generico hay mas decodificares de campos genericos (foreach dentro del while)
//  - �como se personalizan las acciones de cada registro segun sus valores?
//  - hay una funcion de paginado en el funciones.php generico
//--------------------------------------------------------------

// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-striped table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
$filtro_noc_para_listado = "";//" and noc='$noc'";
// Para el paginado
$registros_por_pagina = "30";

// acciones_por_registro: Es un array de botones u opciones que debe tener cada registro
// Cada registro tendra un conjunto de acciones. Fijarse como en el ID del registro, ponemos #ID#.
// maestro_formulario.php pondra el ID correcto.
$acciones_por_registro = array();
$condiciones_visibilidad_por_registro = array();

$acciones_por_registro[] = '<a class="btn btn-mini btn-sky" href="modules.php?mod=gestproject&file=index_productos_fotos_new&padre_id=#ID#&pag=0">FOTOS</a>';
$condiciones_visibilidad_por_registro[] = "";
// Ejemplo: $condiciones_visibilidad_por_registro[] = "select user_id as visualizar_opcion from usuarios_t where user_id=#ID#";

$acciones_por_registro[] = '<a class="btn btn-mini btn-sky" href="modules.php?mod=gestproject&file=index_productos_variantes_asignadas_new&padre_id=#ID#&pag=0">VARIANTES DEL PRODUCTO</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="btn btn-mini btn-sky" href="modules.php?mod=gestproject&file=index_productos_variantes_stocks_new&padre_id=#ID#&pag=0">STOCKS</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="icon" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="icon" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";
$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";

// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
//$campo_padre = "";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
//$consulta_nombre_padre = " select nombre as nombre from productos_t where id=#PADREID#";



// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

?>