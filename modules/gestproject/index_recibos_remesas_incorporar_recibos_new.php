<?php 

//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
 


// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "INCOPORAR LIQUIDACIONES DE RECIBOS A REMESAS<br>(<a href=modules.php?mod=gestproject&file=index_recibos_remesas_new&pag=0&padre_id=>VOLVER A LISTADO DE REMESAS</a>)";
// Titulo que aparece en la pestna del navegador
$titulo_pagina = "INCOPORAR LIQUIDACIONES DE RECIBOS A REMESAS";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";
// Texto que aparece en el boton de crear
$titulo_boton_crear = "CREAR NUEVA LIQUIDACION DEL RECIBO";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 0;

// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
$modulo_script="gestproject";

// Nombre del script
$script = "index_recibos_remesas_incorporar_recibos_new";
// Nombre de la tabla
$tabla = "recibos_liquidaciones_t"; // OJO, la clave principal se debe llamar id
$clase_boton_crear = " class='buttonmario mediummario green' ";
$clase_boton_buscar = " class='buttonmario mediummario green' ";
$clase_boton_guardar = " class='buttonmario mediummario green' ";
$clase_boton_volver  = " class='buttonmario mediummario green' ";
$clase_boton_confirmar_borrado  = " class='buttonmario mediummario red' ";

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
$campos_col1 = array('fecha_abono','talon','efectivo','bruto','via_cobro','gestor_id');

// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]
$ayudas_col1 = array();

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('on','','','on');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('','','','pattern="\d+(\.\d*)?"');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php

/*
if ($login != "Admin" && $login != "mteresa") {
	$tipos_col1  = array('textarea;200;40','select;maestro_recibos_tipo_gestion_solo_cobrador_t;nombre;id','select;usuarios_t;login;id','datetime3');
} else {
	$tipos_col1  = array('textarea;200;40','select;maestro_recibos_tipo_gestion_t;nombre;id','select;usuarios_t;login;id','datetime3');
}
*/
$tipos_col1  = array('datetime3','text;100','checkbox','text;10','select;maestro_via_cobro_t;nombre;id;nombre','select;usuarios_t;login;id;login');

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_insert = " noc='$noc', ";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = " fecha_alta=now(), user_id='$user_id', ";

// Campo para la busqueda
$campo_busqueda = "fecha_abono desc ";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript
$plantilla_insercion = "index_liquidaciones_informe_new.plantilla.php";


if ($plantilla_insercion != "") {
  $fichero_absoluto = $dir_raiz . "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}

// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 1;
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','remesa_id','recibo_id','fecha_abono','talon','efectivo','bruto','user_id');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','Remesa','N recibo','Fecha de abono','Tal&oacute;n','Efectivo','Bruto','Usuario gestor');
// Decodificacion si existiese de los campos
//echo "login: $login";
$filtros_iniciales = " and (remesa_id='' or remesa_id is null) ";

/*
if ($login != "Admin" && $login != "mteresa") {
	$campos_listado_decod = array ('','','','si;maestro_recibos_tipo_gestion_solo_cobrador_t;nombre;id','si;usuarios_t;login;id','si;usuarios_t;login;id','');
} else {
	$campos_listado_decod = array ('','','','si;maestro_recibos_tipo_gestion_t;nombre;id','si;usuarios_t;login;id','si;usuarios_t;login;id','');
}
*/
$campos_listado_decod = array ('','si;recibos_remesas_t;fecha;id','si;recibos_t;num_recibo;id','si;datetime','','','','si;usuarios_t;login;id');


// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-striped table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_listado = " and noc='$noc'";
//$filtros_iniciales = " and (remesa_id = '' or remesa_id is null) ";
// Para el paginado
$registros_por_pagina = "100";
$ocultar_imprimir_listado_final_rejilla = 1;

if ($_REQUEST["anobuscar_cliente_2_desde"] != "" || $_REQUEST["anobuscar_cliente_2_hasta"] != "" || $_REQUEST["buscar_cliente_1"] != "" ) {
  
   //$opciones_adicionales_final_listado = "<a href=modules.php?mod=gestproject&file=informe_recibos&buscar_cliente_1=$_REQUEST[buscar_cliente_1]&anobuscar_cliente_2_desde=$_REQUEST[anobuscar_cliente_2_desde]&mesbuscar_cliente_2_desde=$_REQUEST[mesbuscar_cliente_2_desde]&diabuscar_cliente_2_desde=$_REQUEST[diabuscar_cliente_2_desde]&anobuscar_cliente_2_hasta=$_REQUEST[anobuscar_cliente_2_hasta]&mesbuscar_cliente_2_hasta=$_REQUEST[mesbuscar_cliente_2_hasta]&diabuscar_cliente_2_hasta=$_REQUEST[diabuscar_cliente_2_hasta] target='_blank'>IMPRIMIR INFORME</a>";

}

// Si permitimos una rejilla editable, aparecera en la izquierda un checkbox que se podra seleccionar desde jquery
$listado_editable = 1;
// A la url siguiente se le pasara via GET los check_123, check_124, etc con los ids seleccionados
$accion_a_realizar_con_campos_marcados = "INCORPORAR RECIBOS MARCADOS EN REMESA|modules.php?mod=informes&file=informe_liquidaciones_remesas_accion_incorporar_recibos_en_remesa";



// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 1;
$buscadores = array();
$buscadores[] = "select_input;recibo_id;recibos_t;num_recibo;id;buscar por num recibo";
$buscadores[] = "select;gestor_id;usuarios_t;login;id;buscar gestor";
$buscadores[] = "intervalo_fechas;fecha_abono;;;;Fecha de abono";
$buscadores[] = "select;remesa_id;recibos_remesas_t;fecha;id;remesa";
$buscadores_extra_sin_efecto[] = "<input name=remesa_id id=remesa_id value='$_REQUEST[remesa_id]'>";
$buscadores_extra_sin_efecto_solo_campo[] = "remesa_id";

//$buscadores[] = "input;efectivo;;;;Buscar por efectivo = on";
//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";


// acciones_por_registro: Es un array de botones u opciones que debe tener cada registro
// Cada registro tendra un conjunto de acciones. Fijarse como en el ID del registro, ponemos #ID#.
// maestro_formulario.php pondra el ID correcto.
/*$acciones_por_registro = array();
$condiciones_visibilidad_por_registro = array();*/
/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#&padre_id=#PADREID#">MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#&padre_id=#PADREID#">BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear

/*
$proceso_pre_accioncrear= "modules/gestproject/proceso_pre_gestioncobros_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/gestproject/proceso_post_gestioncobros_accioncrearmodificar.php";
$proceso_post_accionmodificar= "modules/gestproject/proceso_post_gestioncobros_accioncrearmodificar.php";
*/

// Despues de insertar o modificar un registro, se lo enviamos a VELAZQUEZ
/*$proceso_post_accioncrear= "modules/gestproject/index_liquidaciones_new_proceso_post_gestioncobros_accioncrearmodificar.php";
$proceso_post_accionmodificar= "modules/gestproject/index_liquidaciones_new_proceso_post_gestioncobros_accioncrearmodificar.php";*/

/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";
$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";
*/

// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
//$campo_padre = "recibo_id";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
//$consulta_nombre_padre = " select num_recibo as nombre from recibos_t where id=#PADREID#";

$campo_a_sumar = "bruto";

// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");





?>

