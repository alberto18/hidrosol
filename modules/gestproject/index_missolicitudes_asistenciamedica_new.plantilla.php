  <div class="row">
  	<div class="col-sm-12">
	  <section class="panel panel-default">
		<header class="panel-heading font-bold">Creaci&oacute;n de una solicitud: Asistencia a Consulta m&eacute;dica</header>
		
			<table border=1   class='table table-bordered table-condensed table-hover' align=center>
			<tr><td style='background-color: #e1e4ea !important;'><b>Fecha inicial</b></td><td>[fecha_ini]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Fecha final</b></td><td>[fecha_fin]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Observaciones</b></td><td>[observaciones]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Justificante</b></td><td>[nombre_fichero] <i>(debe incorporar el justificante tan pronto lo tenga)</i></td></tr>
			</table>

	  </section>
	</div>
   </div>