  <div class="row">
  	<div class="col-sm-12">
	  <section class="panel panel-default">
		<header class="panel-heading font-bold">Creaci&oacute;n de una solicitud: Reconocimiento de horas extra</header>
		
			<table border=1   class='table table-bordered table-condensed table-hover' align=center>
			<tr><td style='background-color: #e1e4ea !important;'><b>Fecha inicial</b></td><td>[fecha_ini]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Fecha final</b></td><td>[fecha_fin]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Horas y minutos extra</b></td><td>[horas]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Observaciones y descripci&oacute;n del motivo</b></td><td>[observaciones]</td></tr>
			</table>

	  </section>
	</div>
   </div>