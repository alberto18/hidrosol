<?php 
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";

// CONFIGURACION
$titulo = "GESTION DE ARTICULOS DEL PEDIDO ";
$titulo_pagina = "ARTICULOS DE PEDIDO";
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_ped_articulos_new";
$script_pedidos = "index_pedidos_new";
$script_ver_correo = "ver_correo_pedido";
$tabla = "ped_articulos_t";
$tabla_padre = "pedidos_t";
$tabla_stock = "stock_t";
$tabla_ped_portes = "ped_portes_t";
$tabla_reservas = "stock_reserva_t";
$tabla_clientes_direcciones = "clientes_direcciones_t";
$tabla_proveedores = "proveedores_t";
$tabla_articulos = "articulos_t";
$tabla_cli_prov = "clientes_prov_t";
$tabla_ped_preparado = "ped_art_preparado_t";
$tabla_albaranes = "albaranes_t";
$tabla_m_embalajes = "maestro_tipos_embalajes_t";
$tabla_art_embalajes = "articulos_embalajes_t";
$tabla_m_igic = "maestro_igic_t";
$registros_por_pagina = 10;
$script_descarga = "";
$tamano_max_archivo = "16000000";
if ($color_entorno != "") { $color_fondo = $color_entorno; $color_fondo_claro = $color_entorno_claro; }
else { $color_fondo = "97c00e"; $color_fondo_claro = "dcfb73"; }

$texto_textarea = '<br type="_moz" />';

echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo "
<table width='100%' height='700' border='0' cellpadding='0' cellspacing='0' class='text' align='center'>
	<tr valign='top'>
		<td width=100% align=left>
";

if (PermisosSecciones($user_id, $script, array()) == 1)
{

// textos de la pagina
$texto_crear = "Crear articulo";
$texto_listado_general = "Listar todos los articulos";
$texto_creado = "Articulo creado";
$texto_modificado = "Articulo modificado";
$texto_borrado = "Articulo borrado";
$nombre_objeto = " UN ARTICULO";

// Campos con los que se trabajara en el insert y modify
$campos_col1 = array('articulo_id','unidades','proveedor_id','precio_unidad');
$campos_col2 = array();

// Nombres que apareceran en las columnas de los formularios
$nombres_col1 = array('Articulo','Unidades','Proveedor','Precio');
$nombres_col2 = array();

// Tipos. Cada campo puede ser:
// text;readonly;size (60)
// password;readonly;size
// textarea;readonly;row;col (10;60)
// select;readonly;tabla;campo_mostrar;campo_para_value;campo_condicion
// checkbox;readonly
// date;readonly
// hidden;tabla;campo_mostrar;campo_para_value;campo_para_order;nuevo_nombre;campo_depende;campo_filtro mostrara un select que saltara y ademas tendra otro campo oculto con el valor
// dni;readonly
// telefono;readonly
// cuenta;readonly
// email;readonly;size
// float;readonly;size
// multiple;readonly;tabla;campo_mostrar;campo_para_value;campo_para_order;nombre_checkbox
// file poner el campo "nombre_fichero", actualiza nombre_fichero, tipo_fichero, peso_fichero, fichero_binario, fecha_subida, user_id
//   Los campos en la tabla a a�adir serian:
//  | nombre_fichero              | varchar(200)
//  | tipo_fichero                | varchar(20)
//  | peso_fichero                | varchar(20)
//  | fecha_subida                | datetime
//  | user_id                     | int(11)
//  | fichero_binario             | blob
// ss;readonly
// calendar;readonly
// time;readonly
// numerico;readonly;size
// fileCarp poner el campo "nombre_fichero", actualiza solo nombre_fichero y sube a la carpeta definida
$tipos_col1  = array('select;1;articulos_t;nombre;id;nombre','numerico;1;5','select;1;proveedores_t;nombre_corto;id;nombre_corto','float;1;8');
$tipos_col2  = array();

// Separadores o titulos
$titulos_col1 = array('');
$titulos_col2 = array('');

// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('articulo_id','tipo_embalaje_id','unidades','unidades_entregadas',
'proveedor_id','precio_unidad','dto_porc','dto2','dto3','pronto_pago','igic_id',
'importe_articulo','importe_igic','comprometido');
//,'fecha_max_reserva'
// indica la tabla de donde es el campo
$tablas_campos_listado = array($tabla,$tabla,$tabla,$tabla,
$tabla,$tabla,$tabla,$tabla,$tabla,$tabla,$tabla,
$tabla,$tabla,$tabla);
//,$tabla

$nombres_listado = array('(Ref.) Articulo','Embalaje','Ud.','Ud. entregadas',
'Prov.','Precio','Dto1','Dto2','Dto3','Pronto pago','Igic',
'Importe parcial','IGIC parcial','Compromiso');
//,'Reserva'

$campos_necesarios_listado = array('id','unidades_embalaje','articulo_id','proveedor_id','fecha_modificacion','unidades_gratis');
//,'fecha_max_reserva','reservado'
$tablas_campos_necesarios = array($tabla,$tabla,$tabla,$tabla,$tabla,$tabla);
//,$tabla,$tabla

// Si es necesaria una decodificacion en el listado
// '' no es necesaria
// si;tabla;campo_mostrar;campo_buscar
// si;date
// si;datetime
// si;checkbox
// si;time
$campos_listado_decod = array ('si;articulos_t;nombre;id','si;maestro_tipos_embalajes_t;nombre;id','','',
'si;proveedores_t;nombre_corto;id','','','','','si;maestro_igic_t;valor;id',
'','','');
//,'si;date'

// Campo para la busqueda
$campo_busqueda = "id";

// Campo padre
$usa_padre = 1;
$campopadre = "pedido_id";

// Variables del script
$parametros_nombres = array("accion","pag",$campopadre,"b_nombre");
$parametros_formulario = array("pag",$campopadre,"b_nombre");
$parametros_filtro = array("b_nombre"); // parametros que estan en el filtro
// Tipos de parametros
//									Sin nada es que no sirven para los filtros o filtro distinto
// texto;campo_filtro							Es un filtro para texto
// texto								Es un filtro para texto, pero hay que desarrollar el filtrado
// select;campo_filtro							Es un filtro con select
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;desde		Es un filtro para fechas superiores a la introducida
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;hasta		Es un filtro para fechas inferiores a la introducida
$parametros_tipos = array("","","","texto");

foreach($parametros_nombres as $indice_parametros => $nombre_param)
{
	list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
	if ($ele == "texto")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; $$nombre_param = str_replace('+',' ',$$nombre_param); }
		else { $$nombre_param = ""; }
	}
	if ($ele == "select")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
	if ($ele == "fecha")
	{
		if ($_REQUEST[$ele3] != "") { $$ele3 = $_REQUEST[$ele3]; }
		else { $$ele3 = ""; }
		if ($_REQUEST[$ele4] != "") { $$ele4 = $_REQUEST[$ele4]; }
		else { $$ele4 = ""; }
		if ($_REQUEST[$ele5] != "") { $$ele5 = $_REQUEST[$ele5]; }
		else { $$ele5 = ""; }
	}
	if ($ele == "")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
}
if ($pag == "") { $pag = "0"; }


// ACCION MODIFICAR 
if ($accion == "accionmodificar")
{
	$ped_estado_id = 0;
	$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
	//echo "$consulta_padre";
	$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
	while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
	{
		$ped_estado_id = $linea_padre['estado_id'];
	}
	$num_albaranes = 0;
	$cons_alba = "select count(id) as total from $tabla_albaranes where $campopadre='".$$campopadre."';";
	$res_alba = mysql_query($cons_alba) or die("La consulta fall&oacute;: $cons_alba " . mysql_error());
	while ($lin_alba = mysql_fetch_array($res_alba, MYSQL_ASSOC))
	{
		$num_albaranes = $lin_alba['total'];
	}
	if (($ped_estado_id == $pedido_estado_creado || $ped_estado_id == $pedido_estado_pendiente_servir) && $num_albaranes == 0)
	{
		// inicio modificaciones de los pedidos
		$consulta_hijo = "select * from $tabla where $campopadre='".$$campopadre."' order by $tabla.$campo_busqueda;";
		//echo "$consulta_hijo<br>";
		$resultado_hijo = mysql_query($consulta_hijo) or die("$consulta_hijo, La consulta fall&oacute;: " . mysql_error());
		while ($linea_hijo = mysql_fetch_array($resultado_hijo, MYSQL_ASSOC))
		{
			$valor_actual_unidades_embalaje = $linea_hijo['unidades_embalaje'];
			$valor_actual_tipo_embalaje_id = $linea_hijo['tipo_embalaje_id'];
			$valor_actual_comprometido = $linea_hijo['comprometido'];
			$valor_actual_unidades = $linea_hijo['unidades'];
			$valor_actual_dto2 = $linea_hijo['dto2'];
			$valor_actual_dto3 = $linea_hijo['dto3'];
			
			$nombre_articulo = "";
			$nombre_proveedor = "";
			$consulta_cat = "select * from $tabla_articulos where $tabla_articulos.id='".$linea_hijo['articulo_id']."';";
			$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
			{
				$nombre_articulo = $linea_cat['nombre'];
			}
			$consulta_cat = "select * from $tabla_proveedores where $tabla_proveedores.id='".$linea_hijo['proveedor_id']."';";
			$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
			{
				$nombre_proveedor = $linea_cat['nombre_corto'];
			}
			// modificacion del embalaje
			$campo_tipo_embalaje_id = "tipo_embalaje_id_".$linea_hijo['id'];
			if (isset($_REQUEST[$campo_tipo_embalaje_id]))
			{
				$nuevo_valor_unidades_embalaje = 0;
				$nuevo_valor_tipo_embalaje_id = 0;
				$consulta_cat = "select $tabla_art_embalajes.unidades, $tabla_art_embalajes.tipo_embalaje_id from $tabla_art_embalajes
where $tabla_art_embalajes.id='".$_REQUEST[$campo_tipo_embalaje_id]."';";
				$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
				{
					$nuevo_valor_unidades_embalaje = $linea_cat['unidades'];
					$nuevo_valor_tipo_embalaje_id = $linea_cat['tipo_embalaje_id'];
				}
				if ($nuevo_valor_unidades_embalaje > 0 && $nuevo_valor_tipo_embalaje_id > 0 && ($nuevo_valor_unidades_embalaje != $valor_actual_unidades_embalaje || $nuevo_valor_tipo_embalaje_id != $valor_actual_tipo_embalaje_id))
				{
					// inicio de actualizacion embalaje
					// Se modifica los campos del embalaje (tipo_embalaje_id y unidades_embalaje)
					$cons = "update $tabla set tipo_embalaje_id='".$nuevo_valor_tipo_embalaje_id."', unidades_embalaje='".$nuevo_valor_unidades_embalaje."', fecha_modificacion=now() where id='".$linea_hijo['id']."';";
					//echo "cons: $cons<br>";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error());
					echo "<b>Embalaje modificado</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
					// fin de actualizacion embalaje
				}
			} // fin if (isset($_REQUEST[$campo_comprometido]))
			// modificacion de la cantidad comprometida
			$campo_comprometido = "comprometido_".$linea_hijo['id'];
			if (isset($_REQUEST[$campo_comprometido]))
			{
				if ($_REQUEST[$campo_comprometido] != $valor_actual_comprometido)
				{
					// inicio de actualizacion comprometido
					// Se modifica las unidades comprometidas
					$cons = "update $tabla set comprometido='".$_REQUEST[$campo_comprometido]."', fecha_modificacion=now() where id='".$linea_hijo['id']."';";
					//echo "cons: $cons<br>";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error());
					if ($_REQUEST[$campo_comprometido] > 0)
					{
						echo "<b>Compromiso actualizado</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
					}
					else
					{
						echo "<b>Compromiso anulado</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
					}
					// fin de actualizacion comprometido
				}
			} // fin if (isset($_REQUEST[$campo_comprometido]))
			// modificacion de las unidades
			$campo_unidades = "unidades_".$linea_hijo['id'];
			if (isset($_REQUEST[$campo_unidades]))
			{
				if ($_REQUEST[$campo_unidades] != $valor_actual_unidades)
				{
					// inicio de actualizacion unidades
					if ($_REQUEST[$campo_unidades] > 0)
					{
						// inicio es mayor que cero
						$importe_articulo = 0;
						$importe_igic = 0;
						$valor_precio_neto = 0;
						$valor_dto_porc = 0;
						$valor_precio_unidad = 0;
						$valor_unidades_gratis = 0;
						$cons = "select * from $tabla where id='".$linea_hijo['id']."';";
						$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
						while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
						{
							$valor_igic = "";
							$consulta_cat = "select * from $tabla_m_igic where $tabla_m_igic.id='".$lin['igic_id']."';";
							$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
							while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
							{
								$valor_igic = $linea_cat['valor'];
							}
							if ($lin['min_unidades_dto'] == 0 && $lin['precio_unidad_dto'] == 0 && $lin['min_unidades_gratis'] == 0 && $lin['num_unidades_gratis'] == 0)
							{
								//echo "opcion 1<br>";
								// si no hay ninguna oferta => precio_unidad
								$importe_articulo = $_REQUEST[$campo_unidades]*$lin['precio_unidad'];
								$importe_igic = round(($importe_articulo*$valor_igic)/100, 2);
								
								// valores afectados en el update por los otros casos
								$valor_precio_neto = $lin['precio_neto'];
								$valor_dto_porc = $lin['dto_porc'];
								$valor_precio_unidad = $lin['precio_unidad'];
								$valor_unidades_gratis = $lin['unidades_gratis'];
							}
							elseif ($lin['min_unidades_dto'] > 0 && $lin['precio_unidad_dto'] > 0)
							{
								//echo "opcion 2<br>";
								// si hay una oferta del tipo [comprando X unidades la unidad sale a Y euros]
								if ($_REQUEST[$campo_unidades] >= $lin['min_unidades_dto'])
								{
									//echo "opcion 2.1<br>";
									// si se cumple: se actualiza precio_neto, precio_unidad, dto_porc
									$valor_precio_neto = $lin['precio_unidad_dto'];
									$valor_dto_porc = 0;
									$valor_precio_unidad = $lin['precio_unidad_dto'];
									// aplicar el dto2, dto3 y pronto_pago
									if ($lin['dto2'] > 0)
									{
										$dto_2 = round(($valor_precio_unidad*$lin['dto2'])/100, 2);
										$valor_precio_unidad = $valor_precio_unidad-$dto_2;
									}
									if ($lin['dto3'] > 0)
									{
										$dto_3 = round(($valor_precio_unidad*$lin['dto3'])/100, 2);
										$valor_precio_unidad = $valor_precio_unidad-$dto_3;
									}
									if ($lin['pronto_pago'] > 0)
									{
										$dto_pronto_pago = round(($valor_precio_unidad*$lin['pronto_pago'])/100, 2);
										$valor_precio_unidad = $valor_precio_unidad-$dto_pronto_pago;
									}
								}
								else
								{
									//echo "opcion 2.2<br>";
									// si no se cumple: se actualiza precio_neto, precio_unidad, dto_porc
									$valor_precio_neto = $lin['precio_neto_sin_oferta'];
									$valor_dto_porc = $lin['dto_porc_sin_oferta'];
									$valor_precio_inicial = 0;
									if ($lin['precio_neto_sin_oferta'] > 0)
									{
										// si se ha definido (antes de la oferta) un precio neto este sera el precio de partida
										$valor_precio_inicial = $lin['precio_neto_sin_oferta'];
									}
									elseif ($lin['dto_porc_sin_oferta'] > 0)
									{
										// si se ha definido (antes de la oferta) un dto % al art se calculara el precio del articulo menos ese dto %
										$valor_precio_inicial = $lin['precio_articulo']-round(($lin['precio_articulo']*$lin['dto_porc_sin_oferta'])/100, 2);
									}
									else
									{
										// si no se ha definido nada se coge el precio del articulo directamente
										$valor_precio_inicial = $lin['precio_articulo'];
									}
									// aplicacion del dto2
									$valor_precio_inicial -= round(($valor_precio_inicial*$lin['dto2'])/100, 2);
									// aplicacion del dto3
									$valor_precio_inicial -= round(($valor_precio_inicial*$lin['dto3'])/100, 2);
									// aplicacion del pronto_pago
									$valor_precio_inicial -= round(($valor_precio_inicial*$lin['pronto_pago'])/100, 2);
									$valor_precio_unidad = $valor_precio_inicial;
								}
								$importe_articulo = $_REQUEST[$campo_unidades]*$valor_precio_unidad;
								$importe_igic = round(($importe_articulo*$valor_igic)/100, 2);
								
								// valores afectados en el update por los otros casos
								$valor_unidades_gratis = $lin['unidades_gratis'];
							}
							elseif ($lin['min_unidades_gratis'] > 0 && $lin['num_unidades_gratis'] > 0)
							{
								//echo "opcion 3<br>";
								// si hay una oferta de tipo unidades gratis comprando un minimo
								if ($_REQUEST[$campo_unidades] >= $lin['min_unidades_gratis'])
								{
									//echo "opcion 3.1<br>";
									// si se cumple: se actualiza unidades_gratis
									$valor_inicial = $_REQUEST[$campo_unidades];
									$valor_unidades_gratis = 0;
									while ($valor_inicial >= $lin['min_unidades_gratis'])
									{
										$valor_inicial -= $lin['min_unidades_gratis'];
										$valor_unidades_gratis += $lin['num_unidades_gratis'];
									}
								}
								else
								{
									//echo "opcion 3.2<br>";
									// si no se cumple: se pone a cero las unidades_gratis
									$valor_unidades_gratis = 0;
								}
								
								// valores afectados en el update por los otros casos
								$valor_precio_neto = $lin['precio_neto'];
								$valor_dto_porc = $lin['dto_porc'];
								$valor_precio_unidad = $lin['precio_unidad'];
								$importe_articulo = $lin['importe_articulo'];
								$importe_igic = $lin['importe_igic'];
							}
						} // fin while $lin
						$cons = "update $tabla set fecha_modificacion=now(), unidades='".$_REQUEST[$campo_unidades]."', 
precio_neto='".$valor_precio_neto."', dto_porc='".$valor_dto_porc."', precio_unidad='".$valor_precio_unidad."', 
unidades_gratis='".$valor_unidades_gratis."', 
importe_articulo='".$importe_articulo."', importe_igic='".$importe_igic."'
where id='".$linea_hijo['id']."';";
						
						//echo "cons: $cons<br>";
						$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error());
						
						// se actualizan los portes y los importes
						ActualiarPortesImportesPedido ($$campopadre);
						echo "<b>Unidades actualizadas</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
						// fin es mayor que cero
					}
					else
					{
						echo "<b>No se puede dejar a cero las unidades para el articulo $nombre_articulo (prov: $nombre_proveedor).</b><br>";
					}
					// fin de actualizacion unidades
				} // fin if ($_REQUEST[$campo_unidades] != $valor_actual_unidades)
			} // fin if (isset($_REQUEST[$campo_unidades]))
			// modificacion del dto2
			$campo_dto2 = "dto2_".$linea_hijo['id'];
			if (isset($_REQUEST[$campo_dto2]))
			{
				if ($_REQUEST[$campo_dto2] != $valor_actual_dto2)
				{
					// inicio de actualizacion dto2
					$importe_articulo = 0;
					$importe_igic = 0;
					$valor_precio_unidad = 0;
					$cons = "select * from $tabla where id='".$linea_hijo['id']."';";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
					while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
					{
						$valor_igic = "";
						$consulta_cat = "select * from $tabla_m_igic where $tabla_m_igic.id='".$lin['igic_id']."';";
						$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
						while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
						{
							$valor_igic = $linea_cat['valor'];
						}
						
						$valor_precio_inicial = 0;
						if ($lin['precio_neto'] > 0)
						{
							// si se ha definido un precio neto este sera el precio de partida
							$valor_precio_inicial = $lin['precio_neto'];
						}
						elseif ($lin['dto_porc'] > 0)
						{
							// si se ha definido un dto % al art se calculara el precio del articulo menos ese dto %
							$valor_precio_inicial = $lin['precio_articulo']-round(($lin['precio_articulo']*$lin['dto_porc'])/100, 2);
						}
						else
						{
							// si no se ha definido nada se coge el precio del articulo directamente
							$valor_precio_inicial = $lin['precio_articulo'];
						}
						// aplicacion del dto2
						$valor_precio_inicial -= round(($valor_precio_inicial*$_REQUEST[$campo_dto2])/100, 2);
						// aplicacion del dto3
						$valor_precio_inicial -= round(($valor_precio_inicial*$lin['dto3'])/100, 2);
						// aplicacion del pronto_pago
						$valor_precio_inicial -= round(($valor_precio_inicial*$lin['pronto_pago'])/100, 2);
						$valor_precio_unidad = $valor_precio_inicial;
						
						$importe_articulo = $lin['unidades']*$valor_precio_unidad;
						$importe_igic = round(($importe_articulo*$valor_igic)/100, 2);
					}
					$cons = "update $tabla set precio_unidad='".$valor_precio_unidad."', dto2='".$_REQUEST[$campo_dto2]."', importe_articulo='".$importe_articulo."', importe_igic='".$importe_igic."', fecha_modificacion=now() where id='".$linea_hijo['id']."';";
					//echo "cons: $cons<br>";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error());
					
					// se actualizan los portes y los importes
					ActualiarPortesImportesPedido ($$campopadre);
					echo "<b>Dto2 actualizado</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
					// fin de actualizacion dto2
				}
			} // fin if (isset($_REQUEST[$campo_dto2]))
			// modificacion del dto3
			$campo_dto3 = "dto3_".$linea_hijo['id'];
			if (isset($_REQUEST[$campo_dto3]))
			{
				if ($_REQUEST[$campo_dto3] != $valor_actual_dto3)
				{
					// inicio de actualizacion dto3
					$importe_articulo = 0;
					$importe_igic = 0;
					$valor_precio_unidad = 0;
					$cons = "select * from $tabla where id='".$linea_hijo['id']."';";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
					while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
					{
						$valor_igic = "";
						$consulta_cat = "select * from $tabla_m_igic where $tabla_m_igic.id='".$lin['igic_id']."';";
						$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
						while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
						{
							$valor_igic = $linea_cat['valor'];
						}
						
						$valor_precio_inicial = 0;
						if ($lin['precio_neto'] > 0)
						{
							// si se ha definido un precio neto este sera el precio de partida
							$valor_precio_inicial = $lin['precio_neto'];
						}
						elseif ($lin['dto_porc'] > 0)
						{
							// si se ha definido un dto % al art se calculara el precio del articulo menos ese dto %
							$valor_precio_inicial = $lin['precio_articulo']-round(($lin['precio_articulo']*$lin['dto_porc'])/100, 2);
						}
						else
						{
							// si no se ha definido nada se coge el precio del articulo directamente
							$valor_precio_inicial = $lin['precio_articulo'];
						}
						// aplicacion del dto2
						$valor_precio_inicial -= round(($valor_precio_inicial*$lin['dto2'])/100, 2);
						// aplicacion del dto3
						$valor_precio_inicial -= round(($valor_precio_inicial*$_REQUEST[$campo_dto3])/100, 2);
						// aplicacion del pronto_pago
						$valor_precio_inicial -= round(($valor_precio_inicial*$lin['pronto_pago'])/100, 2);
						$valor_precio_unidad = $valor_precio_inicial;
						
						$importe_articulo = $lin['unidades']*$valor_precio_unidad;
						$importe_igic = round(($importe_articulo*$valor_igic)/100, 2);
					}
					$cons = "update $tabla set precio_unidad='".$valor_precio_unidad."', dto3='".$_REQUEST[$campo_dto3]."', importe_articulo='".$importe_articulo."', importe_igic='".$importe_igic."', fecha_modificacion=now() where id='".$linea_hijo['id']."';";
					//echo "cons: $cons<br>";
					$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons" . mysql_error());
					
					// se actualizan los portes y los importes
					ActualiarPortesImportesPedido ($$campopadre);
					echo "<b>Dto3 actualizado</b> (Articulo: $nombre_articulo, Prov: $nombre_proveedor)<br>";
					// fin de actualizacion dto3
				}
			} // fin if (isset($_REQUEST[$campo_dto3]))
		} // fin while $linea_hijo
		// fin modificaciones de los pedidos
	}
	else
	{
		echo "<b>No se permite modificar este pedido</b>";
	}
	$accion = "";
}
// FIN ACCION MODIFICAR 

// ACCION BORRAR
if ($accion == "accionborrar")
{
	$cons = "select * from $tabla where id='".$_REQUEST['id']."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		// si tiene hecha alguna reserva se borra primero
		$cons1 = "select * from $tabla_reservas where ped_articulo_id='".$_REQUEST['id']."';";
		$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
		while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
		{
			// se busca el articulo en el stock para insertar o actualizar
			$existe_stock = 0;
			$unidades_stock = 0;
			$cons2 = "select * from $tabla_stock where articulo_id='".$lin1['articulo_id']."' and almacen_id='".$lin1['almacen_id']."' and proveedor_id='".$lin1['proveedor_id']."' and estanteria='".$lin1['estanteria']."' and hueco='".$lin1['hueco']."' and piso='".$lin1['piso']."' and palet='".$lin1['palet']."';";
			//echo "cons2: $cons2<br>";
			$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
			while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
			{
				$existe_stock = $lin2['id'];
				$unidades_stock = $lin2['unidades'];
			}
			
			if ($existe_stock == 0)
			{
				// no existe ese articulo => se crea
				$cons3 = "insert into $tabla_stock set articulo_id='".$lin1['articulo_id']."', unidades='".$lin1['unidades']."', almacen_id='".$lin1['almacen_id']."', proveedor_id='".$lin1['proveedor_id']."', estanteria='".$lin1['estanteria']."', hueco='".$lin1['hueco']."', piso='".$lin1['piso']."', palet='".$lin1['palet']."';";
				//echo "cons3: $cons3<br>";
				$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
				$existe_stock =  mysql_insert_id();
			}
			else
			{
				$nuevas_unidades = $unidades_stock + $lin1['unidades'];
				$cons3 = "update $tabla_stock set unidades='".$nuevas_unidades."' where id='".$existe_stock."';";
				//echo "cons3: $cons3<br>";
				$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3" . mysql_error());
			}
			
			// si estaba preparado el articulo habra que asociar el stock resevado al stock ya devuelto (stock_reservado_id, stock_id)
			$existe_prepa = 0;
			$cons2 = "select * from $tabla_ped_preparado where stock_reservado_id='".$lin1['id']."';";
			//echo "cons2: $cons2<br>";
			$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
			while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
			{
				$cons3 = "update $tabla_ped_preparado set stock_id='".$existe_stock."', stock_reservado_id='0' where id='".$lin2['id']."';";
				//echo "cons3: $cons3<br>";
				$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3" . mysql_error());
			}
			
			// se borra el registro de la reserva
			$cons4 = "delete from $tabla_reservas where id='".$lin1['id']."';";
			//echo "cons4: $cons4<br>";
			$res4 = mysql_query($cons4) or die("La consulta fall&oacute;: $cons4 " . mysql_error());
		} // fin while $lin1
		// eliminacion del articulo
		$consulta2 = "delete from $tabla where id='$_POST[id]';";
		//echo "$consulta2<br>";
		$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: " . mysql_error());
		
		// numero de proveedores en el pedido
		$num_proveedores_art = 0;
		$cons3 = "select count(distinct proveedor_id) as total from $tabla where $tabla.$campopadre='".$$campopadre."';";
		$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
		while ($lin3 = mysql_fetch_array($res3, MYSQL_ASSOC))
		{
			$num_proveedores_art = $lin3['total'];
		}
		// numero de proveedores en los portes
		$num_proveedores_porte = 0;
		$cons4 = "select count(distinct proveedor_id) as total from $tabla_ped_portes where $tabla_ped_portes.$campopadre='".$$campopadre."';";
		$res4 = mysql_query($cons4) or die("La consulta fall&oacute;: $cons4 " . mysql_error());
		while ($lin4 = mysql_fetch_array($res4, MYSQL_ASSOC))
		{
			$num_proveedores_porte = $lin4['total'];
		}
		if ($num_proveedores_art < $num_proveedores_porte)
		{
			// si hay mas proveedores en los portes que en el pedido hay que borrar aquellos que sobren
			$cons4 = "select * from $tabla_ped_portes where $tabla_ped_portes.$campopadre='".$$campopadre."';";
			$res4 = mysql_query($cons4) or die("La consulta fall&oacute;: $cons4 " . mysql_error());
			while ($lin4 = mysql_fetch_array($res4, MYSQL_ASSOC))
			{
				$num_art = 0;
				$cons3 = "select count(id) as total from $tabla where $tabla.$campopadre='".$$campopadre."' and $tabla.proveedor_id='".$lin4['proveedor_id']."';";
				$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
				while ($lin3 = mysql_fetch_array($res3, MYSQL_ASSOC))
				{
					$num_art = $lin3['total'];
				}
				if ($num_art == 0)
				{
					// si no hay articulos del proveedor del porte => se borra el porte
					$cons5 = "delete from $tabla_ped_portes where id='".$lin4['id']."';";
					//echo "cons5: $cons5<br>";
					$res5 = mysql_query($cons5) or die("La consulta fall&oacute;: $cons5 " . mysql_error());
				}
			}
		}
		// se actualizan los portes y los importes
		ActualiarPortesImportesPedido ($$campopadre);
		echo "<b>$texto_borrado</b>";
	} // fin while $lin
	$accion = "";
}
// FIN ACCION BORRAR

// COMIENZA EL SCRIPT

if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$nombre_padre = "";
		$ped_importe_sin_igic = 0;
		$ped_importe_igic = 0;
		$ped_importe_total = 0;
		$ped_estado_id = 0;
		$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
		//echo "$consulta_padre";
		$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
		while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
		{
			$nombre_padre = "<a target='_blank' href='$enlacevolver"."$script_ver_correo&pedido_id=".$$campopadre."' style='color:#$color_fondo;'>".$linea_padre['numero']."</a>";
			$ped_importe_sin_igic = $linea_padre['importe_sin_igic'];
			$ped_importe_igic = $linea_padre['importe_igic'];
			$ped_importe_total = $linea_padre['importe_total'];
			$ped_estado_id = $linea_padre['estado_id'];
		}
		$num_albaranes = 0;
		$cons_alba = "select count(id) as total from $tabla_albaranes where $campopadre='".$$campopadre."';";
		$res_alba = mysql_query($cons_alba) or die("La consulta fall&oacute;: $cons_alba " . mysql_error());
		while ($lin_alba = mysql_fetch_array($res_alba, MYSQL_ASSOC))
		{
			$num_albaranes = $lin_alba['total'];
		}
	}
        
	echo "
<center><b>$titulo $nombre_padre</b><br>";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != $campopadre && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "[<a href='$enlacevolver"."$script_pedidos&pag=0'>Volver a pedidos</a>]";
	//echo "[<a href='$enlacevolver"."$script&accion=formcrear&pag=$pag'>$texto_crear</a>]";
	echo "
<table align='center'>
	<tr bgcolor='#$color_fondo'>
		<td><span class='listadoTabla'>Importe base</span></td>
		<td><span class='listadoTabla'>Importe IGIC</span></td>
		<td><span class='listadoTabla'>Importe total</span></td>
	</tr>
	<tr align='right'>
		<td>".number_format($ped_importe_sin_igic,2,",",".")." &euro;</td>
		<td>".number_format($ped_importe_igic,2,",",".")." &euro;</td>
		<td>".number_format($ped_importe_total,2,",",".")." &euro;</td>
	</tr>
</table>";
//jgs
	$cons = "select count(id) as total from $tabla_ped_portes where $campopadre='".$$campopadre."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		if ($lin['total'] > 0)
		{
			echo "
<table align='center'>
	<tr bgcolor='#$color_fondo'>
		<td><span class='listadoTabla'>Prov.</span></td>
		<td><span class='listadoTabla'>Ag. Transp.</span></td>
		<td><span class='listadoTabla'>Importe minimo</span></td>
		<td><span class='listadoTabla'>Importe articulos</span></td>
		<td><span class='listadoTabla'>Estado</span></td>
	</tr>";
			$importe_total_portes = 0;
			$cons2 = "select * from $tabla_ped_portes where $campopadre='".$$campopadre."' order by id;";
			$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
			while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
			{
				$importe_total_portes += $lin2['importe_portes'];
				$nombre_proveedor = "";
				$consultaselect = "select * from $tabla_proveedores where id='$lin2[proveedor_id]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre_proveedor = $lineaselect['nombre_corto'];
				}
				$nombre_ag_transp = NombreAgTransporte(AgTransporteProvPedido($$campopadre, $lin2['proveedor_id']));
				$importe_total_sin = 0;
				$cons3 = "select sum(importe_articulo) as total from $tabla where $campopadre='".$$campopadre."' and proveedor_id='".$lin2['proveedor_id']."';";// and reservado='' and fecha_max_reserva='0000-00-00'
				//echo "$cons3<br>";
				$res3 = mysql_query($cons3) or die("La consulta fall&oacute;: $cons3 " . mysql_error());
				while ($lin3 = mysql_fetch_array($res3, MYSQL_ASSOC))
				{
					$importe_total_sin = $lin3['total'];
				}
				echo "
	<tr align='right'>
		<td>$nombre_proveedor</td>
		<td>$nombre_ag_transp</td>
		<td>".number_format($lin2['importe_min_portes_pagados'],2,",",".")." &euro;</td>
		<td>".number_format($importe_total_sin,2,",",".")." &euro;</td>
		<td>";
				if ($lin2['usar_cliente'] == "on") { echo "Debidos"; }
				else { echo "Pagados"; }
				echo "</td>
	</tr>";
			}
/*
			echo "
	<tr align='right'>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><b>Total</b></td>
		<td>".number_format($importe_total_portes,2,",",".")." &euro;</td>
		<td>&nbsp;</td>
	</tr>";
*/
			echo "
</table>";
		}
	}
	echo "
	<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<form name=form_buscar method=post action='$enlacevolver"."$script'>
	<input type=hidden name=pag value=0>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
/*
	echo "
		<tr style='text-align:center;'>
			<td><b>Buscar por articulo</b>: <select name='b_arti'><option value=''>Todos</option>";
	$consulta_cat = "select articulos_t.* from articulos_t join $tabla on $tabla.articulo_id=articulos_t.id where $tabla.$campopadre='".$$campopadre."' order by articulos_t.nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $b_arti) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select> <input type=submit value='Filtrar'></td>
		</tr>";
*/
	echo "
		<tr>
			<td style='text-align:center;'><b>Buscar por articulo</b>: <input type='text' name='b_nombre' value='$b_nombre'>&nbsp;<input type=submit value='Filtrar'></td>
		</tr>
	</form>
	</table>";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "
	<a href='$enlacevolver"."$script&pag=0$parametros'>$texto_listado_general</a>
	<!--
	-->
</center>";
}

// OBTENEMOS EL LISTADO DE REGISTROS 
if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$condiciones = " $tabla.$campopadre='".$$campopadre."' ";
		$parametros = "&$campopadre=".$$campopadre;
        }
	else
	{
		$condiciones = "";
		$parametros = "";
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
		if ($ele == "texto" && $ele2 != "")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2 like '%".$$nombre_param."%' ";
				$$nombre_param = str_replace(' ','+',$$nombre_param); 
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "select")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2='".$$nombre_param."' ";
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "fecha")
		{
			if ($$ele3 != "" && $$ele4 != "" && $$ele5 != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				($ele6 == "desde" ? $condiciones .= " $tabla.$ele2>='".$$ele3."-".$$ele4."-".$$ele5." 00:00:00' " : $condiciones .= " $tabla.$ele2<='".$$ele3."-".$$ele4."-".$$ele5." 23:59:59' ");
				$parametros .= "&$ele3=".$$ele3."&$ele4=".$$ele4."&$ele5=".$$ele5;
			}
		}
	}
	if ($b_nombre != "")
	{
		$trozos = array();
		$trozos = explode(" ",$b_nombre);
		foreach ($trozos as $valor)
		{
			if ($condiciones != "") { $condiciones .= " and "; }
			$condiciones .= "(articulos_t.nombre like '%$valor%' or articulos_t.referencia like '%$valor%')";
		}
		$parametros .= "&b_nombre=".$b_nombre;
		$join .= " join articulos_t on articulos_t.id=$tabla.articulo_id ";
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && $nombre_param != $campopadre && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	$string_para_select = "";
	foreach($campos_necesarios_listado as $indice => $campo)
	{
		$string_para_select .= $tablas_campos_necesarios[$indice].".$campo,";
	}
	echo "<table width=100%>
	<tr bgcolor='#$color_fondo'>";
	$columnas = 0;
	foreach ($campos_listado as $campo) {
		echo "<td><a class='listadoTabla' href='$enlacevolver"."$script&orderby=$campo&pag=$pag$parametros'>$nombres_listado[$columnas]</a></td>";
		if ($campo == "unidades_entregadas")
		{
			echo "<td><span class='listadoTabla'>Stock</span></td>";
			echo "<td><span class='listadoTabla'>Pedido</span></td>";
		}
		if ($campo == "comprometido")
		{
			echo "<td><span class='listadoTabla'>Reservado</span></td>";
		}
		$string_para_select .= " ".$tablas_campos_listado[$columnas].".$campo,";
		$columnas++;
	}
	$columnas += 3;
	$columnas += 1;// acciones
	// Eliminamos el ultimo caracter
	$string_para_select = substr($string_para_select,0,-1);
	echo "<td><font color='#ffffff'><b>Acciones</b></font></td>";
	echo "</tr>";
	$permiso_modificar = 0;
	if (($ped_estado_id == $pedido_estado_creado || $ped_estado_id == $pedido_estado_pendiente_servir) && $num_albaranes == 0)
	{
		$permiso_modificar = 1;
	}
	if ($permiso_modificar == 1)
	{
		echo "
	<form name=form_mod_ped method=post action='$enlacevolver"."$script'>
	<input type=hidden name=accion value='accionmodificar'>
	<input type=hidden name=$campopadre value='".$$campopadre."'>";
	}	

	$consulta  = "select $string_para_select from $tabla $join";
	if ($condiciones != "") { $consulta .= " where $condiciones "; }
	// Vemos si existe un orderby
	if ($_REQUEST[orderby] != "")
	{
		$order = " order by $tabla.$_REQUEST[orderby]";
		$parametros .= "&orderby=".$_REQUEST[orderby];
	}
	else { $order = " order by $tabla.$campo_busqueda";  }
/*
	if ($pag != "") {
		// la primera pagina es la 0
		$inicio = $pag*$registros_por_pagina;
		$limit = " limit $inicio,$registros_por_pagina";
	}
	else { $limit = ""; }
*/
	$limit = "";
	$consulta .= " group by $tabla.id $order $limit;";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: $consulta " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		$existe_reserva = 0;
		$cons_reserva = "select id from $tabla_reservas where ped_articulo_id='".$linea['id']."' limit 1;";
		$res_reserva = mysql_query($cons_reserva) or die("La consulta fall&oacute;: $cons_reserva " . mysql_error());
		while ($lin_reserva = mysql_fetch_array($res_reserva, MYSQL_ASSOC))
		{
			if ($lin_reserva['id'] > 0) { $existe_reserva = 1; }
		}
		$mostrar_submit = 0;
		echo "
	<tr>";
		foreach ($campos_listado as $cuenta_campos => $campo)
		{
			$nombre = "";
			list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod[$cuenta_campos]);
			if ($ele != 'si')
			{
				$nombre = "$linea[$campo]";
			}
			elseif ($ele2 == "date")
			{
				if ($linea[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
				else { $nombre = date("d-m-Y",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "datetime")
			{
				if ($linea[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
				else { $nombre = date("d-m-Y H:i",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "checkbox")
			{
				($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
			}
			elseif ($ele2 == "time")
			{
				list($temp1, $temp2, $temp3) = explode(':',$linea[$campo]);
				$nombre = $temp1.":".$temp2;
			}
			elseif ($linea[$campo] != "")
			{
				$consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre = $lineaselect[$ele3];
					if ($campo == "articulo_id")
					{
						$nombre = "($lineaselect[referencia]) $lineaselect[$ele3]";
					}
				}
			}
			if ($campo == "tipo_embalaje_id")
			{
				if ($existe_reserva == 0 && $permiso_modificar == 1)
				{
					// si no hay reserva y se puede modificar el pedido se permite cambiar el embalaje
					$nombre = "<select name='$campo"."_"."$linea[id]' id='$campo"."_"."$linea[id]'>";
					$consulta_cat = "select $tabla_art_embalajes.id, $tabla_art_embalajes.unidades, $tabla_m_embalajes.nombre, $tabla_art_embalajes.tipo_embalaje_id from $tabla_art_embalajes
join $tabla_m_embalajes on $tabla_m_embalajes.id=$tabla_art_embalajes.tipo_embalaje_id 
where $tabla_art_embalajes.articulo_id='".$linea['articulo_id']."' and $tabla_art_embalajes.inactivo=''
order by $tabla_art_embalajes.defecto_presupuesto desc, $tabla_art_embalajes.tipo_embalaje_id, $tabla_art_embalajes.unidades;";
					$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
					{
						$nombre .= "<option value='$linea_cat[id]'"; if ($linea_cat['tipo_embalaje_id'] == $linea['tipo_embalaje_id'] && $linea_cat['unidades'] == $linea['unidades_embalaje']) { $nombre .= " selected"; } $nombre .= ">$linea_cat[nombre] de $linea_cat[unidades]</option>";
					}
					$nombre .= "</select>";
				}
				else
				{
					$nombre .= " de ".$linea['unidades_embalaje'];
				}
			}
			if ($campo == "unidades")
			{
				if ($existe_reserva == 0 && $permiso_modificar == 1)
				{
					// solo se permitira modificar las unidades si el pedido esta en estado creado o pendiente de servir, no hay ningun albaran y si el articulo no esta reservado
					$mostrar_submit = 1;
					$nombre = "
	<input type='text' name='$campo"."_"."$linea[id]' id='$campo"."_"."$linea[id]' size='5' value='$linea[$campo]' onkeyup='validar($campo"."_"."$linea[id]);' >";
				}
				if ($linea['unidades_gratis'] > 0)
				{
					$nombre .= " + ".$linea['unidades_gratis'];
				}
			}
			if (in_array($campo,array("precio_unidad","igic_id","importe_articulo","importe_igic")))
			{
				//if (in_array($campo,array("importe_articulo","importe_igic")) && $linea['reservado'] == "on")
				//{
					// si el articulo esta reservado (ped_articulos_t.reservado=on) no se visualiza el importe
				//	$nombre = 0;
				//}
				$nombre = number_format($nombre, 2, ",",".");
				if (in_array($campo,array("precio_unidad","importe_articulo","importe_igic"))) { $nombre .= " &euro;"; }
				if ($campo == "igic_id") { $nombre .= " %"; }
			}
			if (in_array($campo,array("dto_porc","pronto_pago")))
			{
				if ($linea[$campo] > 0) { $nombre = number_format($linea[$campo], 2, ",",".")." %"; }
				else { $nombre = ""; }
			}
			if ($campo == "dto2" || $campo == "dto3")
			{
				if ($existe_reserva == 0 && $permiso_modificar == 1)
				{
					// solo se permitira modificar los descuentos si el pedido esta en estado creado o pendiente de servir, no hay ningun albaran y si el articulo no esta reservado
					$mostrar_submit = 1;
					$nombre = "
	<input type='text' name='$campo"."_"."$linea[id]' id='$campo"."_"."$linea[id]' size='5' value='$linea[$campo]' onkeyup='validarFloat($campo"."_"."$linea[id]);' >";
				}
				else
				{
					if ($linea[$campo] > 0) { $nombre = number_format($linea[$campo], 2, ",",".")." %"; }
					else { $nombre = ""; }
				}
			}
			if ($campo == "comprometido" && $linea[$campo] > 0 && $permiso_modificar == 1)
			{
				// si el articulo tiene unidades comprometidas se permite modificar o quitar
				$mostrar_submit = 1;
				$nombre = "
<input type='text' name='$campo"."_"."$linea[id]' id='$campo"."_"."$linea[id]' size='5' value='$linea[$campo]' onkeyup='validar($campo"."_"."$linea[id]);' >";
			}
			if (in_array($campo,array("precio_unidad","dto_porc","dto2","importe_articulo","importe_igic")))
			{
				echo "
		<td align='right'>$nombre</td>";
			}
			else
			{
				echo "
		<td>$nombre</td>";
			}
			if ($campo == "unidades_entregadas")
			{
				// calculo del stock disponible (no contar las reservas)
				$cantidad_stock = CantidadStock($linea['articulo_id'], $linea['proveedor_id']);
				
				// calculo de las unidades ya pedidas en otros pedidos (aceptados, no finalizados, no entregados y sin marcar como no preparable)
				$cantidad_pedido = CantidadPedido($linea['articulo_id'], $linea['proveedor_id']);
				
				echo "
		<td>$cantidad_stock</td>
		<td>$cantidad_pedido</td>";
			}
			if ($campo == "comprometido")
			{
				$texto_reservado = "No";
				if ($existe_reserva == 1) { $texto_reservado = "Si"; }
				echo "
		<td>$texto_reservado</td>";
			}
		}
		echo "<td>";
		$fecha_preparado = "";
		$cons_prepa = "select fecha from $tabla_ped_preparado where ped_articulo_id='".$linea['id']."' order by fecha desc limit 1;";
		$res_prepa = mysql_query($cons_prepa) or die("La consulta fall&oacute;: $cons_prepa " . mysql_error());
		while ($lin_prepa = mysql_fetch_array($res_prepa, MYSQL_ASSOC))
		{
			$fecha_preparado = $lin_prepa['fecha'];
		}
		if ($fecha_preparado != "")
		{
			echo " <img src='images/error3.png' alt='Referencia en preparacion' title='Referencia en preparacion' border='0' />";
		}
		
		//if ($mostrar_submit == 1) { echo "<input type=submit value='Actualizar' class='reduced'>"; }
		//echo "			<a href='$enlacevolver"."$script&accion=formmodificar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
/*
		$posibilidad = 0;
		//$posibilidad = ComprobarMaestroGrupo($linea['id']);
		if ($posibilidad == 0)
		{
			echo "
			<a href='$enlacevolver"."$script&accion=formborrar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_delete.png' alt='Borrar' title='Borrar' border='0' /></a> ";
		}
*/
		if ($permiso_modificar == 1)
		{
			// inicio formulario de borrar
			echo "<a href='$enlacevolver"."$script&accion=formborrar&id=$linea[id]&pag=$pag$parametros' class='buttonmario smallmario red' style='color:#ffffff;'>eliminar</a>";
		}
		echo "
		</td>";
		echo "</tr>";
		echo "<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas'></td></tr>";
	}
	if ($permiso_modificar == 1)
	{
		echo "<tr align='center'><td colspan='$columnas'><input type=submit value='Actualizar'></td></tr>";
		// fin formulario de modificar
		echo "
	</form>";
	}
	echo "</table>";
/*
	if ($pag != "")
	{
		$pag_visual = $pag+1;
		// Visualizamos las paginas existentes
		$consulta2  = "select count($tabla.id) as num from $tabla $join";
		if ($condiciones != "") { $consulta2 .= " where $condiciones;"; } else { $consulta2 .= ";"; }
		$resultado2 = mysql_query($consulta2) or die("$consulta2, La consulta fall&oacute;: " . mysql_error());
		//echo "$consulta2<br>";
		while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC)) {
			$exp = "$linea2[num]";
		}
		if ($exp > $registros_por_pagina)
		{
			echo PaginadoListado($pag, $enlacevolver.$script, $parametros, $exp, $registros_por_pagina);
		}
	}
*/
	echo "<hr>";
}
// FIN DE OBTENER EL LISTADO INICIAL

// FORMULARIO PARA BORRAR UN REGISTRO
if ($accion == "formborrar")
{
	$ped_estado_id = 0;
	$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
	//echo "$consulta_padre";
	$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
	while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
	{
		$ped_estado_id = $linea_padre['estado_id'];
	}
	$num_albaranes = 0;
	$cons_alba = "select count(id) as total from $tabla_albaranes where $campopadre='".$$campopadre."';";
	$res_alba = mysql_query($cons_alba) or die("La consulta fall&oacute;: $cons_alba " . mysql_error());
	while ($lin_alba = mysql_fetch_array($res_alba, MYSQL_ASSOC))
	{
		$num_albaranes = $lin_alba['total'];
	}
	if (($ped_estado_id == $pedido_estado_creado || $ped_estado_id == $pedido_estado_pendiente_servir) && $num_albaranes == 0)
	{
//----------------------------------
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "") { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "") { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "") { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { $parametros .= "&orderby=".$_REQUEST[orderby]; }
	echo "<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<tr>
		<td><center><b>BORRADO DE $nombre_objeto</b> [<a href='$enlacevolver"."$script$parametros'>Volver sin cambios</a>]</center></td>
	</tr>
	<tr valign='top'>
		<td>
		<form name=form_buscar method=post action='$enlacevolver"."$script'>
		<input type=hidden name=accion value=accionborrar>
		<input type=hidden name=id value=$_GET[id]>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "") { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "") { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "") { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "") { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	if ($_REQUEST[orderby] != "") { echo "<input type=hidden name=orderby value='".$_REQUEST[orderby]."'>"; }
	// Obtenemos los valores actuales del registro que se esta modificando
	$consultamod = "select * from $tabla where id=$_GET[id];";
	$resultadomod = mysql_query($consultamod) or die("La consulta fall&oacute;: " . mysql_error());
	// El resultado lo metemos en un array asociativo
	$arraymod = array();
	while ($lineasmod = mysql_fetch_array($resultadomod, MYSQL_ASSOC)) {
		foreach ($campos_col1 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
		foreach ($campos_col2 as $campo) { $arraymod[$campo] = $lineasmod[$campo]; }
	} // del while

	echo "
		<table width='100%' border='0'>
			<tr><td colspan='2'><input type=submit value='Va usted a borrar el registro con los siguientes datos'></td></tr>
			<tr><td width='50%'><table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col1 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col1[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col1[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col1[$cuenta_campos]);
		$nombre_campo = $nombres_col1[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: $dia M: $mes A: $ano</td></tr>";
		}
		if ($ele == "checkbox") { echo "<tr><td><b>$nombre_campo</b></td><td>";if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; } echo "</td></tr>"; }
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele3 where $ele5='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele4]";
				}
			}
			echo "</td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele2 where $ele4='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele3]";
				}
			}
			echo "</td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3 - $valor4</td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "email") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "float") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
				echo "\n<td align='center' width='5%'>$nombre_elemento</td>";
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			if ($arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			$id_codificado = base64_encode($_GET[id]);
			echo "Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3</td></tr>";
		}
		if ($ele == "calendar")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>$dia/$mes/$ano</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>$hora : $minutos</td></tr>";
		}
		if ($ele == "numerico") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		$cuenta_campos++;
	} // del foreach
	echo "</table>";

	// Vamos a por la columna 2
	echo "</td><td>";

	echo "<table width='100%'>";
	$cuenta_campos = 0;
	foreach ($campos_col2 as $campo)
	{
		//Vemos si existe un titulo
		if ($titulos_col2[$cuenta_campos] != '') { echo "<tr><td colspan='2' bgcolor='#$color_fondo'><font color='#ffffff'><b>$titulos_col2[$cuenta_campos]</b></font><hr></td></tr>"; }
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6, $ele7, $ele8) = explode(';', $tipos_col2[$cuenta_campos]);
		$nombre_campo = $nombres_col2[$cuenta_campos];
		if ($ele == "text") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "password") { echo "<tr><td><b>$nombre_campo</b></td><td><input type=password name='$campo' size='$ele3' value='$arraymod[$campo]'"; if ($ele2 == 1) { echo " readonly"; } echo "></td></tr>"; }
		if ($ele == "textarea") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "date")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>D: $dia M: $mes A: $ano</td></tr>";
		}
		if ($ele == "checkbox") { echo "<tr><td><b>$nombre_campo</b></td><td>";if ($arraymod[$campo] == "on") { echo " Si"; } else { echo " No"; } echo "</td></tr>"; }
		if ($ele == "select")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele3 where $ele5='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele4]";
				}
			}
			echo "</td></tr>";
		} // del tipo select
		if ($ele == "hidden")
		{
			echo "<tr><td><b>$nombre_campo</b></td><td>";
			if ($arraymod[$campo] > 0)
			{
				$consultaselect = "select * from $ele2 where $ele4='$arraymod[$campo]';";
				$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC)) {
					echo "$lineaselect[$ele3]";
				}
			}
			echo "</td></tr>";
		} // del tipo hidden
		if ($ele == "cuenta")
		{
			$valor1 = substr($arraymod[$campo],0,4); $valor2 = substr($arraymod[$campo],4,4); $valor3 = substr($arraymod[$campo],8,2); $valor4 = substr($arraymod[$campo],10,10);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3 - $valor4</td></tr>";
		}
		if ($ele == "dni") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</tr>"; }
		if ($ele == "telefono") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "email") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "float") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		if ($ele == "multiple")
		{
			echo "<tr valign='top'><td><b>$nombre_campo</b></td><td>";
			$array_multiple_ids = array();
			$array_multiple_ids = explode(';', $arraymod[$campo]);
			$array_multiple_valor = array();
			foreach ($array_multiple_ids as $multiple_id)
			{
				$array_multiple_valor[$multiple_id] = "on";
			}
			echo "<table width='100%' border='0'>
			<tr>";
			if ($ele6 != "") { $consulta_elementos = "select * from $ele3 order by $ele6;"; }
			else { $consulta_elementos = "select * from $ele3;"; }
			$cuenta_elementos = 0;
			$resultado_elementos = mysql_query($consulta_elementos) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_elementos = mysql_fetch_array($resultado_elementos, MYSQL_ASSOC))
			{
				($array_multiple_valor[$linea_elementos[$ele5]] == "on" ? $nombre_elemento = "Si" : $nombre_elemento = "No");
				echo "\n<td align='center' width='5%'>$nombre_elemento</td>";
				echo "\n<td width='15%'>$linea_elementos[$ele4]</td>";
				$cuenta_elementos++;
				if ($cuenta_elementos == 5)
				{
					echo "</tr><tr>";
					$cuenta_elementos = 0;
				}
			}
			while ($cuenta_elementos < 5)
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
				$cuenta_elementos++;
			}
			echo "
					</tr>
				</table>
				</td>
			</tr>";
		} // del tipo multiple
		if ($ele == "fileCarp")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			if ($arraymod[$campo] != "")
			{
				echo "Archivo existente: <a target=new href='".str_replace("\\","/",$carpeta_contenidos)."/".$arraymod[$campo]."'>".$arraymod[$campo]."</a>";
			}
			echo "</td></tr>";
		}
		if ($ele == "file")
		{
			echo "<tr><td><b>$nombre_campo</b></td>
			<td>";
			$id_codificado = base64_encode($_GET[id]);
			echo "Archivo existente: <a target=new href='$script_descarga.php?id=$id_codificado'>".$arraymod[$campo]."</a>";
			echo "</td></tr>";
		} // del tipo file
		if ($ele == "ss")
		{
			$valor1 = substr($arraymod[$campo],0,2); $valor2 = substr($arraymod[$campo],2,8); $valor3 = substr($arraymod[$campo],10,2);
			echo "<tr><td><b>$nombre_campo</b></td><td>$valor1 - $valor2 - $valor3</td></tr>";
		}
		if ($ele == "calendar")
		{
			list($fecha, $hora) = explode(' ', $arraymod[$campo]); list($ano, $mes, $dia) = explode('-', $fecha);
			echo "<tr><td><b>$nombre_campo</b></td><td>$dia/$mes/$ano</td></tr>";
		}
		if ($ele == "time")
		{
			list($hora, $minutos) = explode(':', $arraymod[$campo]);
			echo "<tr><td><b>$nombre_campo</b></td><td>$hora : $minutos</td></tr>";
		}
		if ($ele == "numerico") { echo "<tr><td><b>$nombre_campo</b></td><td>$arraymod[$campo]</td></tr>"; }
		$cuenta_campos++;
	} // del foreach
	echo "</table>";
	echo "</td></tr></table>";
	echo "</form></td></tr></table>";
//----------------------------------
	}
	else
	{
		echo "<b>No se puede borrar</b>";
	}
}
// FIN FORMULARIO BORRAR

} // fin de se tiene permiso para este script
else
{
	echo "No se tiene permiso para ver esta seccion.<br><br>";
}

echo "
		</td>
	</tr>
</table>
";
?>