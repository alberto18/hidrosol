<?php 
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";

// CONFIGURACION
$titulo = "TAREAS OTROS";
$titulo_pagina = "TAREAS OTROS";
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_tareas_otros_new";
//$script_provincias = "index_maestro_provincias_new";
$script_agenda = "index_agenda_new";
$script_detalle_agenda = "index_detalle_agenda_new";
$tabla = "";
$tabla_padre = "maestro_provincias_t";
$tabla_agenda = "agenda_t";
$tabla_detalle_agenda = "detalle_agenda_t";
$registros_por_pagina = 10;
$script_descarga = "";
$tamano_max_archivo = "16000000";
if ($color_entorno != "") { $color_fondo = $color_entorno; $color_fondo_claro = $color_entorno_claro; }
else { $color_fondo = "97c00e"; $color_fondo_claro = "dcfb73"; }

$texto_textarea = '<br type="_moz" />';

echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo "
<table width='100%' height='700' border='0' cellpadding='0' cellspacing='0' class='text' align='center'>
	<tr valign='top'>
		<td width=100% align=left>
";

if (PermisosSecciones($user_id, $script, array()) == 1)
{

// textos de la pagina
$texto_crear = "Crear grupo";
$texto_listado_general = "Listar todas las tareas";
$texto_creado = "Grupo creado";
$texto_modificado = "Grupo modificado";
$texto_borrado = "Grupo borrado";
$nombre_objeto = " UN GRUPO";

// Campos con los que se trabajara en el insert y modify
$campos_col1 = array('nombre');
$campos_col2 = array();

// Nombres que apareceran en las columnas de los formularios
$nombres_col1 = array('Nombre');
$nombres_col2 = array();

// Tipos. Cada campo puede ser:
// text;readonly;size (60)
// password;readonly;size
// textarea;readonly;row;col (10;60)
// select;readonly;tabla;campo_mostrar;campo_para_value;campo_condicion
// checkbox;readonly
// date;readonly
// hidden;tabla;campo_mostrar;campo_para_value;campo_para_order;nuevo_nombre;campo_depende;campo_filtro mostrara un select que saltara y ademas tendra otro campo oculto con el valor
// dni;readonly
// telefono;readonly
// cuenta;readonly
// email;readonly;size
// float;readonly;size
// multiple;readonly;tabla;campo_mostrar;campo_para_value;campo_para_order;nombre_checkbox
// file poner el campo "nombre_fichero", actualiza nombre_fichero, tipo_fichero, peso_fichero, fichero_binario, fecha_subida, user_id
//   Los campos en la tabla a a�adir serian:
//  | nombre_fichero              | varchar(200)
//  | tipo_fichero                | varchar(20)
//  | peso_fichero                | varchar(20)
//  | fecha_subida                | datetime
//  | user_id                     | int(11)
//  | fichero_binario             | blob
// ss;readonly
// calendar;readonly
// time;readonly
// numerico;readonly;size
// fileCarp poner el campo "nombre_fichero", actualiza solo nombre_fichero y sube a la carpeta definida
$tipos_col1  = array('text;0;40');
$tipos_col2  = array();

// Separadores o titulos
$titulos_col1 = array('');
$titulos_col2 = array('');

// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('cliente_id','fecha','fecha_fin','proveedor_id','estado_tarea_id','tarea','comercial_visita_id');
// indica la tabla de donde es el campo
$tablas_campos_listado = array($tabla_agenda,$tabla_agenda,$tabla_detalle_agenda,$tabla_detalle_agenda,$tabla_detalle_agenda,$tabla_detalle_agenda,$tabla_agenda);

$nombres_listado = array('Cliente','Fecha visita','Fecha fin tarea','Proveedor','Estado','Tarea','Comercial');

$campos_necesarios_listado = array('id','agenda_id','proveedor_id');
$tablas_campos_necesarios = array($tabla_detalle_agenda,$tabla_detalle_agenda,$tabla_detalle_agenda);

// Si es necesaria una decodificacion en el listado
// '' no es necesaria
// si;tabla;campo_mostrar;campo_buscar
// si;date
// si;datetime
// si;checkbox
// si;time
$campos_listado_decod = array ('si;clientes_t;nombre_corto;id','si;date','si;date','si;proveedores_t;nombre_corto;id','si;maestro_estados_tareas_t;nombre;id','','si;usuarios_t;nombre;id');

// Campo para la busqueda
$campo_busqueda = $tabla_agenda.".fecha";

// Campo padre
$usa_padre = 0;
$campopadre = "provincia_id";

// Variables del script
$parametros_nombres = array("accion","pag","pag2",$campopadre,"b_cliente","b_prov","b_estado",
"fecha1","fecha2");
$parametros_formulario = array("pag","pag2",$campopadre,"b_cliente","b_prov","b_estado",
"fecha1","fecha2");
$parametros_filtro = array("b_cliente","b_prov","ano1","mes1","dia1","ano2","mes2","dia2","b_estado"); // parametros que estan en el filtro
// Tipos de parametros
//									Sin nada es que no sirven para los filtros o filtro distinto
// texto;campo_filtro							Es un filtro para texto
// texto								Es un filtro para texto, pero hay que desarrollar el filtrado
// select;campo_filtro							Es un filtro con select
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;desde		Es un filtro para fechas superiores a la introducida
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;hasta		Es un filtro para fechas inferiores a la introducida
$parametros_tipos = array("","","","","","","texto",
"fecha;fecha;ano1;mes1;dia1;desde","fecha;fecha;ano2;mes2;dia2;hasta");

foreach($parametros_nombres as $indice_parametros => $nombre_param)
{
	list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
	if ($ele == "texto")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; $$nombre_param = str_replace('+',' ',$$nombre_param); }
		else { $$nombre_param = ""; }
	}
	if ($ele == "select")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
	if ($ele == "fecha")
	{
		if ($_REQUEST[$ele3] != "") { $$ele3 = $_REQUEST[$ele3]; }
		else { $$ele3 = ""; }
		if ($_REQUEST[$ele4] != "") { $$ele4 = $_REQUEST[$ele4]; }
		else { $$ele4 = ""; }
		if ($_REQUEST[$ele5] != "") { $$ele5 = $_REQUEST[$ele5]; }
		else { $$ele5 = ""; }
	}
	if ($ele == "")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
}
if ($pag == "") { $pag = "0"; }
if ($b_estado == "") { $b_estado = "1"; }

// COMIENZA EL SCRIPT

if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$nombre_padre = "";
		$pais_id = "";
		$consulta_padre = "select * from $tabla_padre where id='".$$campopadre."';";
		//echo "$consulta_padre";
		$resultado_padre = mysql_query($consulta_padre) or die("$consulta_padre, La consulta fall&oacute;: " . mysql_error());
		while ($linea_padre = mysql_fetch_array($resultado_padre, MYSQL_ASSOC))
		{
			$nombre_padre = "<span style='color:#$color_fondo;'>".$linea_padre['nombre']."</span>"; $pais_id = $linea_padre['pais_id'];
		}
	}
        
	echo "
<center><b>$titulo $nombre_padre</b><br>";
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != $campopadre && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	//echo "[<a href='$enlacevolver"."$script_provincias&pais_id=$pais_id$parametros"."&id=".$$campopadre."'>Volver a provincias</a>]";
	//echo "[<a href='$enlacevolver"."$script&accion=formcrear&pag=$pag'>$texto_crear</a>]";
	echo "<br><b>Buscar por</b>
	<table width='100%' style='background-color:#$color_fondo_amarillo;'>
	<form name=form_buscar1 method=post action='$enlacevolver"."$script'>
	<input type=hidden name=pag value=0>";
	echo "<img src=images/p.jpg onload=document.form_buscar1.b_cliente.focus();>";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele3 value='".$$ele3."'>"; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele4 value='".$$ele4."'>"; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { echo "<input type=hidden name=$ele5 value='".$$ele5."'>"; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && $nombre_param != "pag2" && !(in_array($nombre_param,$parametros_filtro))) { echo "<input type=hidden name=$nombre_param value='".$$nombre_param."'>"; }
			}
		}
	}
	echo "
		<tr style='text-align:center;'>
			<td><b>Cliente</b>: <input type='text' name='b_cliente' value='".$b_cliente."'>";
/*
	echo "<select name='b_cliente'><option value=''>Seleccione</option>";
	$consulta_cat = "select * from clientes_t order by nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $b_cliente) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select>";
*/
	echo "</td>
			<td><b>Proveedor</b>: <input type='text' name='b_prov' value='".$b_prov."'>";
/*
	echo "<select name='b_prov'><option value=''>Seleccione</option>";
	$consulta_cat = "select * from proveedores_t order by nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $b_prov) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select>";
*/
	echo "</td>
		</tr>
		<tr style='text-align:center;'>
			<td><b>Fecha desde</b>: D: <input type='text' name='dia1' size='2' maxlength='2' value='$dia1' onkeyup='validar(dia1);'> 
M: <input type='text' name='mes1' size='2' maxlength='2' value='$mes1' onkeyup='validar(mes1);'> 
A: <input type='text' name='ano1' size='4' maxlength='4' value='$ano1' onkeyup='validar(ano1);'></td>
			<td><b>Fecha hasta</b>: D: <input type='text' name='dia2' size='2' maxlength='2' value='$dia2' onkeyup='validar(dia2);'> 
M: <input type='text' name='mes2' size='2' maxlength='2' value='$mes2' onkeyup='validar(mes2);'> 
A: <input type='text' name='ano2' size='4' maxlength='4' value='$ano2' onkeyup='validar(ano2);'></td>
		</tr>
		<tr style='text-align:center;'>
			<td><b>Estado</b>: <select name='b_estado'><option value='-1'>Todos los estados</option>";
	$consulta_cat = "select * from maestro_estados_tareas_t order by nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $b_estado) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select></td>
			<td><input type=submit value='Filtrar'></td>
		</tr>
	</form>
	</table>";
//			<td><b>Nombre</b>: <input type='text' name='b_nombre' value='$b_nombre'></td>
	$parametros = "";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	echo "
	<a href='$enlacevolver"."$script&pag=0$parametros'>$texto_listado_general</a>
	<!--
	-->
</center>";
}

// OBTENEMOS EL LISTADO DE REGISTROS 
if ($accion != "formborrar")
{
	if ($usa_padre != 0)
	{
		$condiciones = " $tabla.$campopadre='".$$campopadre."' ";
		$parametros = "&$campopadre=".$$campopadre;
		$join = " join $tabla_agenda on $tabla_agenda.id=$tabla_detalle_agenda.agenda_id ";
	}
	else
	{
		$condiciones = "";
		$parametros = "";
		$join = " join $tabla_agenda on $tabla_agenda.id=$tabla_detalle_agenda.agenda_id ";
	}
	if ($condiciones != "") { $condiciones .= " and "; }
	$condiciones .= " $tabla_agenda.comercial_visita_id<>'".$user_id."' ";
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
		if ($ele == "texto" && $ele2 != "")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2 like '%".$$nombre_param."%' ";
				$$nombre_param = str_replace(' ','+',$$nombre_param); 
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "select")
		{
			if ($$nombre_param != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				$condiciones .= " $tabla.$ele2='".$$nombre_param."' ";
				$parametros .= "&$nombre_param=".$$nombre_param;
			}
		}
		if ($ele == "fecha")
		{
			if ($$ele3 != "" && $$ele4 != "" && $$ele5 != "")
			{
				if ($condiciones != "") { $condiciones .= " and "; }
				//($ele6 == "desde" ? $condiciones .= " $tabla.$ele2>='".$$ele3."-".$$ele4."-".$$ele5." 00:00:00' " : $condiciones .= " $tabla.$ele2<='".$$ele3."-".$$ele4."-".$$ele5." 23:59:59' ");
				($ele6 == "desde" ? $condiciones .= " $tabla_agenda.$ele2>='".$$ele3."-".$$ele4."-".$$ele5." 00:00:00' " : $condiciones .= " $tabla_agenda.$ele2<='".$$ele3."-".$$ele4."-".$$ele5." 23:59:59' ");
				$parametros .= "&$ele3=".$$ele3."&$ele4=".$$ele4."&$ele5=".$$ele5;
			}
		}
	}
	if ($b_estado != "-1")
	{
		if ($condiciones != "") { $condiciones .= " and "; }
		$condiciones .= " $tabla_detalle_agenda.estado_tarea_id='".$b_estado."' ";
		$parametros .= "&b_estado=".$b_estado;
	}
	if ($b_cliente != "")
	{
		$array_clientes_ids = array();
		//desgloso b_cliente si tiene espacios en blanco
		$trozos = array();
		$trozos = explode(" ",$b_cliente);
		$condi_cli = "";
		foreach ($trozos as $valor)
		{
			if ($condi_cli != "") { $condi_cli .= " and "; }
			$condi_cli .= "(clientes_t.nombre like '%$valor%' or clientes_t.nombre_corto like '%$valor%')";
		}
		if ($condi_cli != "")
		{
			$cons = "select id from clientes_t where $condi_cli;";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$array_clientes_ids[] = $lin['id'];
			}
		}
		if ($condiciones != "") { $condiciones .= " and "; }
		if (count($array_clientes_ids) > 0)
		{
			$temp = CrearCondicionConsulta($array_clientes_ids,$tabla_agenda,"cliente_id","=","or");
			$condiciones .= "($temp)";
		}
		else
		{
			$condiciones .= "$tabla_agenda.cliente_id='-1'";
		}
		$parametros .= "&b_cliente=".$b_cliente;
	}
	if ($b_prov != "")
	{
		$array_prov_ids = array();
		//desgloso b_prov si tiene espacios en blanco
		$trozos = array();
		$trozos = explode(" ",$b_prov);
		$condi_prov = "";
		foreach ($trozos as $valor)
		{
			if ($condi_prov != "") { $condi_prov .= " and "; }
			$condi_prov .= "(proveedores_t.nombre like '%$valor%' or proveedores_t.nombre_corto like '%$valor%')";
		}
		if ($condi_prov != "")
		{
			$cons = "select id from proveedores_t where proveedores_t.inactivo='' and $condi_prov;";
			$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
			while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
			{
				$array_prov_ids[] = $lin['id'];
			}
		}
		if ($condiciones != "") { $condiciones .= " and "; }
		if (count($array_prov_ids) > 0)
		{
			$temp = CrearCondicionConsulta($array_prov_ids,$tabla_detalle_agenda,"proveedor_id","=","or");
			$condiciones .= "($temp)";
		}
		else
		{
			$condiciones .= "$tabla_detalle_agenda.proveedor_id='-1'";
		}
		$parametros .= "&b_prov=".$b_prov;
	}
	foreach($parametros_nombres as $indice_parametros => $nombre_param)
	{
		if (in_array($nombre_param,$parametros_formulario))
		{
			list ($ele, $ele2, $ele3, $ele4, $ele5, $ele6) = explode(';', $parametros_tipos[$indice_parametros]);
			if ($ele == "fecha")
			{
				if ($$ele3 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele3=".$$ele3; }
				if ($$ele4 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele4=".$$ele4; }
				if ($$ele5 != "" && !(in_array($ele3,$parametros_filtro))) { $parametros .= "&$ele5=".$$ele5; }
			}
			else
			{
				if ($$nombre_param != "" && $nombre_param != "pag" && $nombre_param != $campopadre && !(in_array($nombre_param,$parametros_filtro))) { $parametros .= "&$nombre_param=".$$nombre_param; }
			}
		}
	}
	$string_para_select = "";
	foreach($campos_necesarios_listado as $indice => $campo)
	{
		$string_para_select .= $tablas_campos_necesarios[$indice].".$campo,";
	}
	echo "<table width=100%>
	<tr bgcolor='#$color_fondo'>";
	$columnas = 0;
	foreach ($campos_listado as $campo) {
//		echo "<td><b><a href='$enlacevolver"."$script&orderby=$campo&pag=$pag$parametros'><font color='#ffffff'>$nombres_listado[$columnas]</font></a></b></td>";
		echo "<td><a class='listadoTabla' href='$enlacevolver"."$script&orderby=$campo&pag=$pag$parametros'>$nombres_listado[$columnas]</a></td>";
		$string_para_select .= $tablas_campos_listado[$columnas].".$campo,";
		$columnas++;
	}
	$columnas += 1;
	// Eliminamos el ultimo caracter
	$string_para_select = substr($string_para_select,0,-1);
	echo "<td><font color='#ffffff'><b>Acciones</b></font></td>";
	echo "</tr>";

	$consulta  = "select $string_para_select from $tabla_detalle_agenda $join";
	if ($condiciones != "") { $consulta .= " where $condiciones "; }
	// Vemos si existe un orderby
	if ($_REQUEST[orderby] != "")
	{
		$order = " order by $tabla.$_REQUEST[orderby]";
		$parametros .= "&orderby=".$_REQUEST[orderby];
	}
	else { $order = " order by $tabla.$campo_busqueda";  }
	if ($pag != "") {
		// la primera pagina es la 0
		$inicio = $pag*$registros_por_pagina;
		$limit = " limit $inicio,$registros_por_pagina";
	}
	else { $limit = ""; }
	$consulta .= " group by $tabla_detalle_agenda.id $order $limit;";
	//echo "$consulta<br>";
	$resultado = mysql_query($consulta) or die("La consulta fall&oacute;: $consulta " . mysql_error());
	while ($linea = mysql_fetch_array($resultado, MYSQL_ASSOC))
	{
		if ($_GET[id] != "" && $linea[id] == $_GET[id]) { $bgcolor = " bgcolor='#$color_fondo_claro'"; }
		else { $bgcolor = ""; }
		
		echo "<tr $bgcolor>";
		foreach ($campos_listado as $cuenta_campos => $campo)
		{
			$nombre = "";
			list ($ele, $ele2, $ele3, $ele4, $ele5) = explode(';', $campos_listado_decod[$cuenta_campos]);
			if ($ele != 'si')
			{
				$nombre = "$linea[$campo]";
			}
			elseif ($ele2 == "date")
			{
				if ($linea[$campo] == "0000-00-00") { $nombre = "00-00-0000"; }
				else { $nombre = date("d-m-Y",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "datetime")
			{
				if ($linea[$campo] == "0000-00-00 00:00:00") { $nombre = "00-00-0000 00:00"; }
				else { $nombre = date("d-m-Y H:i",strtotime($linea[$campo])); }
			}
			elseif ($ele2 == "checkbox")
			{
				($linea[$campo] == "on" ? $nombre = "Si" : $nombre = "No");
			}
			elseif ($ele2 == "time")
			{
				list($temp1, $temp2, $temp3) = explode(':',$linea[$campo]);
				$nombre = $temp1.":".$temp2;
			}
			elseif ($linea[$campo] != "")
			{
				$consultaselect = "select * from $ele2 where $ele4='$linea[$campo]';";
				//echo "$consultaselect";
				$resultadoselect = mysql_query($consultaselect) or die("9 La consulta fall&oacute;: " . mysql_error());
				while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
				{
					$nombre = $lineaselect[$ele3];
				}
			}
			echo "<td>$nombre</td>";
		}
		echo "<td>";
		if ($linea['proveedor_id'] == 0)
		{
			echo "
			<a href='$enlacevolver"."$script_detalle_agenda&accion=formmodificarcli&id=$linea[id]&pag=0&agenda_id=$linea[agenda_id]'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
		}
		else
		{
			echo "
			<a href='$enlacevolver"."$script_detalle_agenda&accion=formmodificarprov&id=$linea[id]&pag=0&agenda_id=$linea[agenda_id]'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
		}
/*
		echo "
			<a href='$enlacevolver"."$script&accion=formmodificar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_edit.png' alt='Modificar' title='Modificar' border='0' /></a> ";
		$posibilidad = 0;
		$posibilidad = ComprobarMaestroGrupo($linea['id']);
		if ($posibilidad == 0)
		{
			echo "
			<a href='$enlacevolver"."$script&accion=formborrar&id=$linea[id]&pag=$pag$parametros'><img src='images/table_delete.png' alt='Borrar' title='Borrar' border='0' /></a> ";
		}
*/
		echo "
		</td>";
		echo "</tr>";
		echo "<tr><td height='1' bgcolor='#$color_fondo_claro' colspan='$columnas'></td></tr>";
	}
	echo "</table>";
	if ($pag != "")
	{
		$pag_visual = $pag+1;
		// Visualizamos las paginas existentes
		$consulta2  = "select count($tabla_detalle_agenda.id) as num from $tabla_detalle_agenda $join";
		if ($condiciones != "") { $consulta2 .= " where $condiciones;"; } else { $consulta2 .= ";"; }
		$resultado2 = mysql_query($consulta2) or die("$consulta2, La consulta fall&oacute;: " . mysql_error());
		//echo "$consulta2<br>";
		while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC)) {
			$exp = "$linea2[num]";
		}
		if ($exp > $registros_por_pagina)
		{
			echo PaginadoListado($pag, $enlacevolver.$script, $parametros, $exp, $registros_por_pagina);
		}
	}
	echo "<hr>";
}
// FIN DE OBTENER EL LISTADO INICIAL

} // fin de se tiene permiso para este script
else
{
	echo "No se tiene permiso para ver esta seccion.<br><br>";
}

echo "
		</td>
	</tr>
</table>
";
?>