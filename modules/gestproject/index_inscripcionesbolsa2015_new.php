<?php 
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
?>

<?php

   //include("obtener_miperfil.php");
   
   //include("obtener_opciones.php");

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "GESTION DE BOLSA 2015";
// Titulo que aparece en la pestaña del navegador
$titulo_pagina = "GESTION DE BOLSA 2015";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo '
        <section id="content">
          <section class="vbox">

            <header class="header bg-white b-b b-light">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="modules.php?mod=gestproject&file=index"><i class="fa fa-home"></i> Inicio</a></li>
                <li class="active">Inscripciones Bolsa de Empleo P&uacute;blico 2015</li>
              </ul>
            </header>

            <section class="scrollable wrapper w-f">
              <p class="h4">'.$titulo.'</p>
';
			


// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "GESTION DE BOLSA 2015";
// Titulo que aparece en la pestaña del navegador
$titulo_pagina = "GESTION DE BOLSA 2015";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";
// Texto que aparece en el boton de crear
$titulo_boton_crear = "CREAR NUEVO REGISTRO BOLSA 2015";
// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.
// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 
$clase_boton_crear = " class='btn btn-success' ";
$clase_boton_buscar = " class='btn btn-mini btn-black' ";
$clase_boton_guardar = " class='btn btn-success' ";
$clase_boton_volver  = " class='btn btn-navi' ";
$clase_boton_confirmar_borrado  = " class='btn btn-danger' ";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 1;
// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// Nombre del script
$script = "index_inscripcionesbolsa2015_new";
$script_gestion = "index_gestion_documental_inscripciones_new";
$script_asientos = "index_gestion_asientos_inscripciones_new";
// Nombre de la tabla
$tabla = "registro_convocatorias2015_t"; // OJO, la clave principal se debe llamar id

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
$campos_col1 = array('nombre','apellidos','fecnac','dni','direccion','tlf','localidad','provincia','cp','plaza_convocada','titulo_academico2015','sexo','fecha_registro','aportar_dni','aportar_dni_proximo_a_caducar','extranjero_no_comunitario','motivo_exclusion_1','motivo_exclusion_2','motivo_exclusion_3','motivo_exclusion_4','motivo_exclusion_5','motivo_exclusion_6','admitido_provisional','admitido_definitivo','email','num_instancia','discapacidad','tipo_via_id');

// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]
$ayudas_col1 = array('');

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('on','on','on','on','on','on','on','on','on','on','on','on','','','','','','','','','','','','','on','','');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php
$tipos_col1  = array('text;200','text;200','datetime3_calendario','text;200','text;200','text;100','text;200','text;200','text;50','select;maestro_plazas_convocatoriasofertadas2015_t;nombre;id','text;200','select;sexo_t;nombre;id','datetime3_calendario','checkbox','checkbox','checkbox','checkbox','checkbox','checkbox','checkbox','checkbox','checkbox','checkbox','checkbox','text;200','mostrar_dato_text;200','checkbox','select;maestro_tipos_vias_t;nombre;id');

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
$filtro_noc_para_insert = "";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = "";

// Campo para la busqueda
$campo_busqueda = "id desc";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript
$plantilla_insercion = "index_inscripcionesbolsa2015_new.plantilla.php";
if ($plantilla_insercion != "") {
  $fichero_absoluto =  "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}

// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 1;
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','num_instancia','fecha_registro','nombre','apellidos','dni','plaza_convocada','email');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','Num. Instancia','Fecha solicitud','Nombre','Apellidos','DNI','PLAZA CONVOCADA','EMAIL');
// Decodificacion si existiese de los campos
$campos_listado_decod = array ('','','si;datetime_hora','','','','si;maestro_plazas_convocatoriasofertadas2015_t;nombre;id','');
// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-striped table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
$filtro_noc_para_listado = "";
// Para el paginado
if ($_REQUEST[accion] == "crearexcel"){
$registros_por_pagina = "30000";
}else{
$registros_por_pagina = "100";	
	
}
// Permitir la exportacion a excel

$permitir_creacion_de_registros_excel = 1;

$titulo_boton_crear_excel = "&nbsp;&nbsp; | EXPORTAR A EXCEL";




// 2. MODO PERSONALIZADO
// Nombre de la funcion que se ejecutara para cada registro. Si se deja blanco el modo personalizado NO ESTA ACTIVO

$nombre_funcion_acciones_por_registro = "funcion_acciones_registro";
function funcion_acciones_registro($valor_id)
{
global $grupo;
	$id_encript = base64_encode(base64_encode($valor_id));

	/*
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_comerciales_kms_new&padre_id='.$id_encript.'&pag=0">KMS</a>';
		
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_areas_new&padre_id='.$id_encript.'&pag=0">AREAS</a>';
                
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_epi_new&padre_id='.$id_encript.'&pag=0">MATERIALES</a>';

		
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_vacaciones_new&padre_id='.$id_encript.'&pag=0">VACACIONES</a>';
	*/
     
    /*	 
	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_bajas_new&padre_id='.$id_encript.'&pag=0">BAJAS</a>';
	
    echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_usuarios_documentacion_new&padre_id='.$id_encript.'&pag=0">DOCUMENTOS</a>';
	*/
	
	//echo "($valor_id)";
	
	$cons44 = "select * from registro_convocatorias2015_t where id = '$valor_id'";
	$res44 = mysql_query($cons44) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin44 = mysql_fetch_array($res44, MYSQL_ASSOC)) {
	   $tipo_fisica = $lin44['tipo_fisica'];
	   
		 if ($lin44[motivo_exclusion_1] == "on" || $lin44[motivo_exclusion_2] == "on" || $lin44[motivo_exclusion_3] == "on" || $lin44[motivo_exclusion_4] == "on" || $lin44[motivo_exclusion_5] == "on" || $lin44[motivo_exclusion_6] == "on" || $lin44[motivo_exclusion_7] == "on" || $lin44[motivo_exclusion_8] == "on" || $lin44[motivo_exclusion_9] == "on" || $lin44[motivo_exclusion_10] == "on" || $lin44[motivo_exclusion_11] == "on" || $lin44[motivo_exclusion_12] == "on") {
	   
	   //echo " <a title=Excluido class=\"smallmario red\"><i class=\"fa fa-check-square-o\"></i></a>  ";
		echo "<img src=\"images/excluido.png\" width=\"16\" height=\"16\" title=Excluido alt=Excluido  /> ";
		}
		
		 if ($lin44[admitido_provisional] == "on" || $lin44[admitido_definitivo] == "on") {
		  // echo "<a title=Admitido class=\"smallmario green\"><i class=\"fa fa-check-square-o\"></i></a> ";
		   echo "<img src=\"images/admitido.png\" width=\"16\" height=\"16\" title=Admitido alt=Admitido  /> ";
		 } 
	   
	}
	
	//localizamos los registros del logs
	
	$cons444 = "select * from gestion_documental_asientoregistrar_t where inscripcion_id = '$valor_id'";
	$res444 = mysql_query($cons444) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin444 = mysql_fetch_array($res444, MYSQL_ASSOC)) {
	   $asiento_id = $lin444['id'];
	}
	
	$cons445 = "select count(*) as num from historial_envio_asientos_t where asiento_id = '$asiento_id'";
	$res445 = mysql_query($cons445) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin445 = mysql_fetch_array($res445, MYSQL_ASSOC)) {
	   $num_envios = $lin445['num'];
	}
	
	/*
	echo " <a title='Enviar Asiento Registral' class=\"btn btn-default btn-xs\" href=\"modules.php?mod=gestproject&file=index_enviar_email_asiento&id=$id_encript\"><i class=\"fa fa-envelope\" ></i> ($num_envios)</a>  ";
	*/
	
	echo '<a  title="Enviar email con el Asiento Registral al usuario" class="btn btn-default btn-xs"  onclick="return confirm(\'Confirmas que quieres enviar el documento de registro al usuario?\')" href="modules.php?mod=gestproject&file=index_enviar_email_asiento&id='.$id_encript.'"> <i class="fa fa-envelope" ></i> ('.$num_envios.')</a> ';
	
	
	/*
	if ($bienvenida_enviada == "on"){
		
	echo '&nbsp;<a style="margin-top: 2px;" class="btn btn-xs btn-danger">Bienvenida enviada</a>&nbsp;';	
	}else{
	echo '<a style="margin-top: 2px; margin-right: 2px;" class="btn btn-xs btn-info" href="modules_new.php?mod=gestproject_new&file=index_email_bienvenida_new&padre_id='.$id_encript.'"><i class="fugue-pencil" title="editar"></i> Bienvenida</a>';
	}
    echo '<a style="margin-top: 2px; margin-right: 2px;" class="btn btn-xs btn-info" href="'.$enlacevolver.$script.'&accion=formmodificar&id='.$id_encript.'"><i class="icon-pencil2" title="editar"></i></a>';
	echo '<a style="margin-top: 2px;" class="btn btn-xs btn-danger" href="'.$enlacevolver.$script.'&accion=formborrar&id='.$id_encript.'"><i class="icon-cancel-circle2" title="borrar"></i></a>';

}
*/
		
	$cons442 = "select count(*) as num from gestion_documental_bolsa2015_t where inscripcion_id = '$valor_id'";
	$res442 = mysql_query($cons442) or die("La consulta fall&oacute;: $cons442 " . mysql_error());
	while ($lin442 = mysql_fetch_array($res442, MYSQL_ASSOC)) {
	   $num_registros = $lin442['num'];
	}
	
	if($num_registros > "0"){
	$tiene_documentos = "(*)";	
	}else{
	$tiene_documentos = "";	
	}
	
	
	$cons45 = "select * from registrobolsa2015_certificados_t where inscripcion_id='$valor_id'";
	$res45 = mysql_query($cons45) or die("La consulta fall&oacute;: $cons45 " . mysql_error());
	while ($lin45 = mysql_fetch_array($res45, MYSQL_ASSOC)) {
	   $csv = $lin45['csv'];
	}	
	
	//echo "($tipo_fisica)";
	
	if($tipo_fisica == "on"){
		
	echo ' (PRESENTACI&Oacute;N FISICA) ';	
		
		
	}else{
		
	$inscripcion_id = base64_encode(base64_encode($valor_id));
	
	echo '<a target="_blank" class="btn btn-default btn-xs" href="http://casiopea.tac7.com/tacsistemaconvocatorias_stalucia/ver_instancia_pdf.php?accion=descargar&inscripcion='.$inscripcion_id.'"><i class=\"fa fa-folder\"></i>GENERAR INSTANCIA</a> ';
	
	
	echo '<a target="_blank" class="btn btn-default btn-xs" href="http://casiopea.tac7.com/tacsistemaconvocatorias_stalucia/documentos/inscripciones/instancias/'.$csv.'.pdf"><i class=\"fa fa-folder\"></i>INSTANCIA</a> ';
	
	}
	
	
    if($grupo == "1"){

	echo '<a class="btn btn-default btn-xs" href="modules.php?mod=gestproject&file=index_gestion_documental_inscripciones_new&padre_id='.$id_encript.'&pag=0"><i class=\"fa fa-folder\"></i>DOCS '.$tiene_documentos.'</a> ';
	
    }// del if
    
	echo '<a class="btn btn-default btn-xs" href="modules.php?mod=gestproject&file=index_gestion_asientos_inscripciones_new&padre_id='.$id_encript.'&pag=0">REGISTRO</a> ';
	
	 if($grupo == "1"){
    echo '<a class="btn btn-default btn-xs" href="modules.php?mod=gestproject&file=index_inscripcionesbolsa2015_new&accion=formmodificar&id='.$id_encript.'&pag=0"><i class="fugue-pencil" title="editar"></i>VER / MODIFICAR</a> ';
	    }// del if
	
	/*
	echo '<a class="smallmario red" href="modules.php?mod=gestproject&file=index_inscripcionesbolsa2015_new&accion=formborrar&id='.$id_encript.'"><i class="fugue-pencil" title="editar"></i> BORRAR</a>';
	
	*/
	
	
	//echo '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
}




// acciones_por_registro: Es un array de botones u opciones que debe tener cada registro
// Cada registro tendra un conjunto de acciones. Fijarse como en el ID del registro, ponemos #ID#.
// maestro_formulario.php pondra el ID correcto.
$acciones_por_registro = array(); 
$condiciones_visibilidad_por_registro = array();

//$acciones_por_registro[] = '<a class="btn btn-mini btn-sky" href="modules.php?mod=gestproject&file=index_productos_fotos_new&padre_id=#ID#&pag=0">FOTOS</a>';
$condiciones_visibilidad_por_registro[] = "";
// Ejemplo: $condiciones_visibilidad_por_registro[] = "select user_id as visualizar_opcion from usuarios_t where user_id=#ID#";

//$acciones_por_registro[] = '<a class="btn btn-mini btn-sky" href="modules.php?mod=gestproject&file=index_productos_variantes_asignadas_new&padre_id=#ID#&pag=0">VARIANTES DEL PRODUCTO</a>';
$condiciones_visibilidad_por_registro[] = "";

//$acciones_por_registro[] = '<a class="btn btn-mini btn-sky" href="modules.php?mod=gestproject&file=index_productos_variantes_stocks_new&padre_id=#ID#&pag=0">STOCKS</a>';
$condiciones_visibilidad_por_registro[] = "";




$acciones_por_registro[] = '<a class="icon" href="modules.php?mod=gestproject&file='.$script_gestion.'&padre_id=#ID#"><i class="fugue-pencil" title="editar"></i> DOCUMENTOS</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="icon" href="modules.php?mod=gestproject&file='.$script_asientos.'&padre_id=#ID#"><i class="fugue-pencil" title="editar"></i> ASIENTO REGISTRAL</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="icon" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="icon" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear

$proceso_pre_accioncrear= "modules/gestproject/index_inscripcionesbolsa2015_new_proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/gestproject/index_inscripcionesbolsa2015_new_proceso_post_accioncrearmodificar.php";
/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";
$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";
*/
// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
// $campo_padre = "";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
// $consulta_nombre_padre = " select nombre as nombre from productos_t where id=#PADREID#";

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 1;
$buscadores = array();
$buscadores[] = "input;nombre|apellidos;;;;buscar por nombre, apellidos;";
$buscadores[] = "input;dni;;;;buscar por DNI;";
$buscadores[] = "select;tipo_presentacion;maestro_tipos_presentacion_t;nombre;id;buscar por tipo";
$buscadores[] = "select;plaza_convocada;maestro_plazas_convocatoriasofertadas2015_t;nombre;id;buscar por plaza";
$buscadores[] = "intervalo_fechas;fecha_registro;;;;Fecha de solicitud";
//$buscadores[] = "intervalo_fechas;fecha_creacion;;;;Fecha de creaci&oacute;n";
//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";

$buscadores[] = "input_personalizado;id;select r.id from gestion_documental_asientoregistrar_t as g 
left join registro_convocatorias2015_t as r on r.id = g.inscripcion_id where r.plaza_convocada='$_REQUEST[buscar_cliente_4]';buscar por asiento registral;;Asiento registral escribir 1;";

$buscadores[] = "input_personalizado;id;SELECT id FROM registro_convocatorias2015_t WHERE  registro_convocatorias2015_t.plaza_convocada='$_REQUEST[buscar_cliente_4]' and NOT EXISTS 
 (SELECT * FROM gestion_documental_asientoregistrar_t WHERE gestion_documental_asientoregistrar_t.inscripcion_id = registro_convocatorias2015_t.id);buscar por asiento registral;;Sin Asiento registral escribir 1;";

// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

?>

 

            </section>

