  <div class="row">
  	<div class="col-sm-12">
	  <section class="panel panel-default">
		<header class="panel-heading font-bold">Creaci&oacute;n de una solicitud: Asistencia a Cursos</header>
		
			<table border=1   class='table table-bordered table-condensed table-hover' align=center>
			<tr><td style='background-color: #e1e4ea !important;'><b>Fecha inicial</b></td><td>[fecha_ini]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Fecha final</b></td><td>[fecha_fin]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Duraci&oacute;n</b></td><td>[manana_tarde_id]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>T&iacute;tulo del curso</b></td><td>[titulo_curso]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Relaci&oacute;n con el puesto de trabajo</b></td><td>[relacion_puesto_trabajo]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Observaciones</b></td><td>[observaciones]</td></tr>
			<tr><td style='background-color: #e1e4ea !important;'><b>Programa actividad formativa</b></td><td>[nombre_fichero]</td></tr>
			</table>

	  </section>
	</div>
   </div>