<?php 
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
?>

<?php

   //include("obtener_miperfil.php");
   
   //include("obtener_opciones.php");

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "CONTROL DEL KILOMETRAJE (Usuario: $user_id)";
// Titulo que aparece en la pestaña del navegador
$titulo_pagina = "CONTROL DEL KILOMETRAJE";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";
// Texto que aparece en el boton de crear
$titulo_boton_crear = "CREAR NUEVO KILOMETRAJE";
// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.
// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 
$clase_boton_crear = " class='btn btn-success' ";
$clase_boton_buscar = " class='btn btn-mini btn-black' ";
$clase_boton_guardar = " class='btn btn-success' ";
$clase_boton_volver  = " class='btn btn-navi' ";
$clase_boton_confirmar_borrado  = " class='btn btn-danger' ";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 1;
// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// Nombre del script
$script = "index_vehiculos_kilometraje_new";
// Nombre de la tabla
$tabla = "vehiculos_registro_t"; // OJO, la clave principal se debe llamar id

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
$campos_col1 = array('km');


// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('on');


// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('');


// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('');



// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php
$tipos_col1  = array('text;100');
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
// $filtro_noc_para_insert = " noc='$noc', ";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = " usuario_id='$user_id' , fecha=now() , ";

// Campo para la busqueda
$campo_busqueda = "km";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript
$plantilla_insercion = "index_vehiculos_kilometraje_new.plantilla.php";

if ($plantilla_insercion != "") {
  $fichero_absoluto =  "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}

// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 1;
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','usuario_id','km','fecha' );
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','Usuario','Kilometros','Fecha');
// Decodificacion si existiese de los campos
$campos_listado_decod = array ('','si;usuarios_t;login;id','text;100','datetime3');
// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_listado = " and noc='$noc'";
// Para el paginado
$registros_por_pagina = "3000";


//$filtros_iniciales = " and ($tabla.empresa_servicio=1 or $tabla.empresa_servicio=2 or $tabla.empresa_servicio=6) and ($tabla.via_cobro=1 or $tabla.via_cobro=6) and user_destino_id='$user_id'";


//$consulta_inicial =  "select $string_para_select from $tabla left join recibos_gestiones_t on $tabla.id=recibos_gestiones_t.recibo_id where recibos_gestiones_t.user_destino_id='$user_id' and $tabla.id>0 $filtro_noc_para_listado $filtro_buscar $filtro_padre $filtros_iniciales";
$visualizar_num_registros = 1;

// 2. MODO PERSONALIZADO
// Nombre de la funcion que se ejecutara para cada registro. Si se deja blanco el modo personalizado NO ESTA ACTIVO

$acciones_por_registro = array();
$condiciones_visibilidad_por_registro = array();
if ($grupo==5) {

$acciones_por_registro[] = '<a class="smallmario green"  href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#&padre_id=#PADREID#&pag=0"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";
	
		
}


/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear
/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";
$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";
*/

// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
 $campo_padre = "vehiculo_id";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
 $consulta_nombre_padre = " select nombre as nombre from vehiculos_t where id=#PADREID#";

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 0;
$buscadores = array();
//$buscadores[] = "select;matricula;vehiculos_t;matricula;id;buscar por matricula";
$buscadores[] = "input;nombre|modelo";
$buscadores[] = "select;estado_id;maestro_estado_vehiculos_t;nombre;id;buscar por estado";


//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";


// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

?>


