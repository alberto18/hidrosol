<?php 
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
?>

<?php

   //include("obtener_miperfil.php");
   
   //include("obtener_opciones.php");

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "EMPRESAS CLIENTES";
// Titulo que aparece en la pestaña del navegador
$titulo_pagina = "";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";
// Texto que aparece en el boton de crear
$titulo_boton_crear = "CREAR NUEVA EMPRESA/CLIENTE";
// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.
// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 
$clase_boton_crear = " class='buttonmario mediummario green' ";
$clase_boton_buscar = " class='buttonmario mediummario green' ";
$clase_boton_guardar = " class='buttonmario mediummario green' ";
$clase_boton_volver  = " class='buttonmario mediummario green' ";
$clase_boton_confirmar_borrado  = " class='buttonmario mediummario red' ";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear

// solo permitido a la direccion
if ($grupo == 5) {
	$permitir_creacion_de_registros = 1;
}


// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// Nombre del script
$script = "index_empresas_new";
// Nombre de la tabla
$tabla = "empresas_t"; // OJO, la clave principal se debe llamar id

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
$campos_col1 = array('nombre','nombrecomercial','nif','direccion','numero');

// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]
$ayudas_col1 = array();

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('on','','on','','','','','');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('','','','','','','','');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('','','','','','','','','');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php
$tipos_col1  = array('text;400','text;400','text;100','text;500','text;20');

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
// $filtro_noc_para_insert = " noc='$noc', ";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = " ";

// Campo para la busqueda
$campo_busqueda = "nombre asc";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript
$plantilla_insercion = "index_empresas_new.plantilla.php";
if ($plantilla_insercion != "") {
  $fichero_absoluto =  "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}



// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
if ($_REQUEST[accion] != "formcrear" && $_REQUEST[accion] != "formmodificar" && $_REQUEST[accion] != "formborrar") {
	$visualizar_listado = 1;
}
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','nombre','nif');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','Nombre empresa','NIF/CIF');
// Decodificacion si existiese de los campos
$campos_listado_decod = array ('','','');

// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-striped table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_listado = " and noc='$noc'";
// Para el paginado
$registros_por_pagina = "20";
$paginado_reducido = 1;

// filtros iniciales para el listado NUEVO 08032013
// Puedes definir una consulta inicial para el listado de registros, de forma que se apliquen filtros
// para que no se vean todos los registros que existen en la tabla
// PONER LA VIA DE COBRO QUE CORRESPONDA:  and via_cobro=

//$filtros_iniciales = " and (user_id='$user_id')";

//echo "(filtros: $filtros_iniciales)";

//$filtros_iniciales = " and ($tabla.empresa_servicio=1 or $tabla.empresa_servicio=2 or $tabla.empresa_servicio=6) and ($tabla.via_cobro=1 or $tabla.via_cobro=6) and user_destino_id='$user_id'";


//$consulta_inicial =  "select $string_para_select from $tabla left join recibos_gestiones_t on $tabla.id=recibos_gestiones_t.recibo_id where recibos_gestiones_t.user_destino_id='$user_id' and $tabla.id>0 $filtro_noc_para_listado $filtro_buscar $filtro_padre $filtros_iniciales";
$visualizar_num_registros = 1;

// Color de los registros
/*
$nombre_funcion_bgcolor_por_registro = "funcion_bgcolor_registro";
function funcion_bgcolor_registro($valor_id) {
  
  global $user_id;
  
   $color_naranja = "#EFB334";
   $color_gris_oscuro = "#C1BFBB";
   // Vemos el estado del recibo
   $tipo_resultado_ultima_gestion_id = obtener_campo('tipo_resultado','recibos_gestiones_t','left join recibos_t on recibos_gestiones_t.recibo_id=recibos_t.id','recibos_t.id='.$valor_id.' order by recibos_gestiones_t.fecha_alta desc limit 1');
  $tipo_resultado_ultima_gestion_usuario_id = obtener_campo('id','recibos_t','','user_hizo_gestion_id='.$user_id.' order by fecha_gestion desc limit 1');
   if ($tipo_resultado_ultima_gestion_usuario_id == $valor_id) { return  $color_gris_oscuro; } //COLOREAMOS EN GRIS LA ULTIMA GESTION DE USUARIO
   if ($tipo_resultado_ultima_gestion_id != "") { return  $color_naranja; }
}
*/

// acciones_por_registro: Es un array de botones u opciones que debe tener cada registro
// Cada registro tendra un conjunto de acciones. Fijarse como en el ID del registro, ponemos #ID#.
// maestro_formulario.php pondra el ID correcto.

// 2. MODO PERSONALIZADO
// Nombre de la funcion que se ejecutara para cada registro. Si se deja blanco el modo personalizado NO ESTA ACTIVO

/*
$nombre_funcion_acciones_por_registro = "funcion_acciones_registro";
function funcion_acciones_registro($valor_id)
{
    global $tabla, $enlacevolver, $script;
    // me llega decodificado el id del registro en cuestion (en valor_id) pero tengo que codificarlo para ponerlo
    // en los enlaces
    $id_encript = base64_encode(base64_encode($valor_id));
    
    // Aqui se incluye el codigo necesario sobre el registro id_encript con el objetivo de sacar tantos echos
    // como acciones sean necesarias para este registro
    
    // Lo siguiente es un ejemplo de un proyecto determinado

	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_gestioncobros_agenda_new&padre_id='.$id_encript.'&pag=0">AGENDA</a>';	
        // Prueba dibujamos el numero de contrato
	$cons2 = "select contrato,codigo from recibos_t where id = '$valor_id'";
       	$res = mysql_query($cons2) or die("La consulta fall&oacute;: $cons " . mysql_error());
        while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {
           $contrato = $lin['contrato'];
           $codigo = $lin['codigo'];
        }
	echo '<a target="_blank" class="smallmario green" href="modules.php?mod=gestproject&file=ver_contactos&contrato_id='.$contrato.'&pag=0">CONTAC</a>';


	echo '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_gestioncobros_gestiones_new&padre_id='.$id_encript.'&pag=0">GESTIONES</a>';
	
	// Calculamos el numero de gestiones que tiene pendiente
	$cons = "select count(*) as num from recibos_gestiones_t where recibo_id = '$valor_id'";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {
	   $valor = $lin['num'];
	}
        
	//echo '<a target="_blank" class="smallmario green" href="modules.php?mod=gestproject&file=imprimir_extracto&codigo='.$codigo.'&contrato_id='.$contrato.'&pag=0">IMPR</a>';
	echo '<a target="_blank" class="smallmario green" href="http://www.faycanes.es/WEB/extranet/modules.php?op=modload&name=administrativa&file=imprimir_extracto&CODWEB='.$codigo.'&MAESTWEB='.$contrato.'&NUMWEB=1">IMPR</a>';
	
	echo '<a target="_blank" class="smallmario green" href="http://www.faycanes.es/WEB/intranet/modules.php?op=modload&name=interior&file=imprimir_contrato&incluirnetos=1&CODWEB='.$contrato.'&NUMWEB=1">CONTR</a>';
	

	echo "<b>$valor</b>";
}
*/
// FIN 2. MODO PERSONALIZADO

$acciones_por_registro = array(); 
$condiciones_visibilidad_por_registro = array();
$ejecuciones_post_boton = array();

/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_gestioncobros_gestiones_new&padre_id=#ID#&pag=0">GESTIONES</a>';
$condiciones_visibilidad_por_registro[] = "";
// num_registros: Obtenemos el numero de registros vinculado al script hijo del boton
$ejecuciones_post_boton[] = "num_registros;recibos_gestiones_t;recibo_id";
*/

// Solo si la fecha de lo que estamos viendo es distinta al pasado
// Esta parte tenemos que verla

/*
$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#&select_campo_origen='.$_REQUEST[select_campo_origen].'"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

/*
$acciones_por_registro[] = '<a class="smallmario green" href="#" onclick="parent.document.getElementById(\''.$_REQUEST[select_campo_origen].'\').value=#ID_DECOD#;alert(\'ID: #ID_DECOD#\' + \'campo_origen: '.$_REQUEST[select_campo_origen].'\');"><i class="fugue-pencil" title="editar"></i> SELECCIONAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/

/*
$acciones_por_registro[] = '<a class="smallmario green" href="#" onclick="parent.document.getElementById(\''.$_REQUEST[select_campo_origen].'\').value=#ID_DECOD#; $( document ).ready(function() { $(\'.popup-text\').cssaddClass(\'mfp-hide\'); });"><i class="fugue-pencil" title="editar"></i> SELECCIONAR</a>';
$condiciones_visibilidad_por_registro[] = "";
*/


// La accion de CONTACTOS queremos que sea via popup
$url .= "modules.php?mod=gestproject&file=index_empresas_contactos_new&padre_id=#ID#";

$popup = '
<style>
/* para los popups */
.white-popup {
  position: relative;
  background: #FFF;
  padding: 20px;
  width: auto;
  max-width: 700px;
  margin: 20px auto;
}
</style>
<!-- POPUP -->
<div id="login-dialog#ID_DECOD#" class="mfp-with-anim mfp-hide mfp-dialog clearfix white-popup">
	<!-- <h3>Entrar</h3> -->
	<iframe width=700 height=300 src='.$url.'></iframe>
</div>
';
			
$acciones_por_registro[] = $popup . '<a class="smallmario green popup-text" href="#login-dialog#ID_DECOD#" data-effect="mfp-move-from-top"><i class="fugue-cross-circle" title="contactos"></i> CONTACTOS</a>';
$condiciones_visibilidad_por_registro[] = "";


// La accion de LOCALES queremos que sea via popup
$url .= "modules.php?mod=gestproject&file=index_empresas_locales_new&padre_id=#ID#";

$popup = '
<style>
/* para los popups */
.white-popup {
  position: relative;
  background: #FFF;
  padding: 20px;
  width: auto;
  max-width: 700px;
  margin: 20px auto;
}
</style>
<!-- POPUP -->
<div id="login-dialog-local#ID_DECOD#" class="mfp-with-anim mfp-hide mfp-dialog clearfix white-popup">
	<!-- <h3>Entrar</h3> -->
	<iframe width=700 height=500 src='.$url.'></iframe>
</div>
';
			
$acciones_por_registro[] = $popup . '<a class="smallmario green popup-text" href="#login-dialog-local#ID_DECOD#" data-effect="mfp-move-from-top"><i class="fugue-cross-circle" title="contactos"></i> LOCALES</a>';
$condiciones_visibilidad_por_registro[] = "";


// La accion de CONTRATOS queremos que sea via popup
$url .= "modules.php?mod=gestproject&file=index_contratos_new&padre_id=#ID#";

$popup = '
<style>
/* para los popups */
.white-popup-contratos {
  position: relative;
  background: #FFF;
  padding: 20px;
  width: auto;
  max-width: 950px;
  margin: 20px auto;
}
</style>
<!-- POPUP -->
<div id="login-dialog-contrato#ID_DECOD#" class="mfp-with-anim mfp-hide mfp-dialog clearfix white-popup-contratos">
	<!-- <h3>Entrar</h3> -->
	<iframe width=950 height=550 src='.$url.'></iframe>
</div>
';
			
$acciones_por_registro[] = $popup . '<a class="smallmario green popup-text" href="#login-dialog-contrato#ID_DECOD#" data-effect="mfp-move-from-top"><i class="fugue-cross-circle" title="contratos"></i> CONTRATOS</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_empresas_noconformidades_new&padre_id=#ID#"><i class="fugue-cross-circle" title="contactos"></i> NO CONFORMIDADES</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";

if ($grupo == "5") {
	$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
	$condiciones_visibilidad_por_registro[] = "";
}


// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear
/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_post_accioncrear= "modules/contratos/procesos/proceso_post_accioncrearmodifificar.php";
$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";
*/

$proceso_post_accioncrear= "modules/gestproject/index_empresas_new_proceso_post_accioncrear.php";

// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
// $campo_padre = "";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
// $consulta_nombre_padre = " select nombre as nombre from productos_t where id=#PADREID#";

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 1;
$buscadores = array();
$buscadores[] = "input;id;;;;Buscar por codigo";
$buscadores[] = "input_sin_trocear;nombre|nif;;;;Buscar por nombre/nif";
/*
$buscadores[] = "intervalo_fechas;fecha_ruta;;;;<b>Fecha de ruta</b>:";
$buscadores[] = "select;resultado_id;maestro_rutascomerciales_resultado_t;nombre;id;buscar por resultado";
$buscadores[] = "select;empresa_id;empresas_t;nombre;id;buscar por empresa/cliente";
if ($login == "mmartin" || $login == "rsosa") {
	$buscadores[] = "select;user_id;usuarios_t;login;id;buscar por usuario";
}
*/
//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";

// VISUALIZAR SUMATORIOS
// Permite indicar un nombre de campo el cual se sumara en todos los registros de la rejilla y aparecera al final de la misma
//$campo_a_sumar = "total_recibo";

?>


<?php

// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

?>




