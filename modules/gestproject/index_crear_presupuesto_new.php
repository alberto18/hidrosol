<style type="text/css">
<!--
.infohidden {
	display: none;
}
.infoshown {
	display: inline;
}
-->
</style>

<script type="text/Javascript">
<!--
function expandir (nombrecampo)
{ 
	whichpost = document.getElementById(nombrecampo); 
	if (whichpost.className == "infoshown") { 
		whichpost.className = "infohidden"; 
	} 
	else { 
		whichpost.className = "infoshown"; 
	} 
} 

// selObj.name => etiqueta name
// selObj.selectedIndex => indice del array de opciones del select
// selObj.options[indice].value => valor de la opcion "indice" del select
function cambiarEmbalaje (selObj)
{
	// paso 1: cambiar el valor de las unidades por embalaje
	var nombre = selObj.name;// pilla el nombre
	var posBarraBaja = nombre.search("_"); // busco la barra baja en embalaje_X
	var numElemento = nombre.substring(posBarraBaja+1); // busco el articulo de todos los mostrados
	var opcion_elegida = selObj.options[selObj.selectedIndex].value;
	var nombre_dato = "ud_embalajes_"+numElemento+"_"+opcion_elegida;
	var nombre_destino = "ud_embalajes_"+numElemento;
	var objetoDato = document.getElementById(nombre_dato);
	var objetoDestino = document.getElementById(nombre_destino);
	objetoDestino.value = objetoDato.value;
	var nombre_num_embalajes = "num_embalajes_"+numElemento;
	var objetoNumEmbalajes = document.getElementById(nombre_num_embalajes);
	// paso 2: recalcular las unidades si es posible
	if (objetoNumEmbalajes.value > 0)
	{
		// si se han definido el numero de unidades se debe calcular las unidades totales
		var nuevas_unidades = objetoDestino.value*objetoNumEmbalajes.value;
		var nombre_unidades = "unidades_"+numElemento;
		var objetoUnidades = document.getElementById(nombre_unidades);
		objetoUnidades.value = nuevas_unidades;
		cambiarUnidades(numElemento);
	}
}
function cambiarUdEmbalajes (inputObj)
{
	var nombre = inputObj.name;// pilla el nombre
	validar(inputObj);
	if (inputObj.value > 0)
	{
		var posBarraBaja = nombre.indexOf("_"); // busco la barra baja en num_embalajes_X
		posBarraBaja = nombre.indexOf("_",posBarraBaja+1);
		var numElemento = nombre.substring(posBarraBaja+1); // busco el articulo de todos los mostrados
		var nombre_ud_embalajes = "ud_embalajes_"+numElemento;
		var objetoUdEmbalajes = document.getElementById(nombre_ud_embalajes);
		if (objetoUdEmbalajes.value > 0)
		{
			// si se han definido el numero de unidades se debe calcular las unidades totales
			var nombre_unidades = "unidades_"+numElemento;
			var nuevas_unidades = objetoUdEmbalajes.value*inputObj.value;
			var objetoUnidades = document.getElementById(nombre_unidades);
			objetoUnidades.value = nuevas_unidades;
			cambiarUnidades(numElemento);
		}
	}
}

function cambiarUnidades (numElemento)
{
	var nombre_unidades = "unidades_"+numElemento;
	var objetoUnidades = document.getElementById(nombre_unidades);
	validar(objetoUnidades);
	recalcularPrecioOferta(numElemento);
	calcularGratis(numElemento);
}

function recalcularPrecioOferta (numElemento)
{
	var nombre_unidades = "unidades_"+numElemento;
	var objetoUnidades = document.getElementById(nombre_unidades);
	var nombre_min_unidades_dto = "min_unidades_dto_"+numElemento;
	var objetoMinUnidadesDto = document.getElementById(nombre_min_unidades_dto);
	if (parseInt(objetoMinUnidadesDto.value) > 0)
	{
		var nuevo_valor_precio = 0;
		if (parseInt(objetoUnidades.value) >= parseInt(objetoMinUnidadesDto.value))
		{
			// si las unidades son igual o mayor que las de la oferta se pone el precio de oferta y se quita el dto1
			var nombre_precio_unidad_dto = "precio_unidad_dto_"+numElemento;
			var objetoPrecioUnidadDto = document.getElementById(nombre_precio_unidad_dto);
			nuevo_valor_precio = objetoPrecioUnidadDto.value;
			var nombre_dto1 = "dto1_"+numElemento;
			var objetoDto1 = document.getElementById(nombre_dto1);
			objetoDto1.value = "0.00";
		}
		else
		{
			// si no son suficientes unidades se pone el precio y el dto1 que tendria sin tener en cuenta esta oferta
			var nombre_precio_inicial_sin = "precio_inicial_sin_"+numElemento;
			var objetoPrecioInicialSin = document.getElementById(nombre_precio_inicial_sin);
			nuevo_valor_precio = objetoPrecioInicialSin.value;
			var nombre_dto1_sin = "dto1_sin_"+numElemento;
			var objetoDto1Sin = document.getElementById(nombre_dto1_sin);
			var nombre_dto1 = "dto1_"+numElemento;
			var objetoDto1 = document.getElementById(nombre_dto1);
			objetoDto1.value = objetoDto1Sin.value;
		}
		var nombre_precio_inicial = "precio_inicial_"+numElemento;
		var objetoPrecioInicial = document.getElementById(nombre_precio_inicial);
		objetoPrecioInicial.value = nuevo_valor_precio;
		recalcularPrecio(numElemento);
	}
}

function calcularGratis (numElemento)
{
	var nombre_min_unidades_gratis = "min_unidades_gratis_"+numElemento;
	var objetoMinUnidadesGratis = document.getElementById(nombre_min_unidades_gratis);
	if (parseInt(objetoMinUnidadesGratis.value) > 0)
	{
		var nombre_unidades = "unidades_"+numElemento;
		var objetoUnidades = document.getElementById(nombre_unidades);
		var num_unidades_regaladas = 0;
		if (parseInt(objetoUnidades.value) >= parseInt(objetoMinUnidadesGratis.value))
		{
			// se regalaran unidades gratis
			var nombre_num_unidades_gratis = "num_unidades_gratis_"+numElemento;
			var objetoNumUnidadesGratis = document.getElementById(nombre_num_unidades_gratis);
			var incremento = parseInt(objetoNumUnidadesGratis.value);
			var valor_inicial = parseInt(objetoUnidades.value);
			var valor_minimo = parseInt(objetoMinUnidadesGratis.value);
			while (valor_inicial >= valor_minimo)
			{
				valor_inicial -= valor_minimo;
				num_unidades_regaladas += incremento;
			}
		}
		var nombre_txt_gratis = "txt_gratis_"+numElemento;
		var objetoTxtGratis = document.getElementById(nombre_txt_gratis);
		var nombre_unidades_gratis = "unidades_gratis_"+numElemento;
		var objetoUnidadesGratis = document.getElementById(nombre_unidades_gratis);
		objetoUnidadesGratis.value = num_unidades_regaladas;
		if (num_unidades_regaladas == 0)
		{
			objetoTxtGratis.innerHTML="";
		}
		else
		{
			objetoTxtGratis.innerHTML="+ "+num_unidades_regaladas;
		}
		
	}
}

function cambiarDto (inputObj)
{
	var nombre = inputObj.name;// pilla el nombre
	validarFloat(inputObj);
	var posBarraBaja = nombre.indexOf("_"); // busco la barra baja en dto2_X
	var numElemento = nombre.substring(posBarraBaja+1); // busco el articulo de todos los mostrados
	var nombre_precio_inicial = "precio_inicial_"+numElemento;
	var objetoPrecioInicial = document.getElementById(nombre_precio_inicial);
	if (objetoPrecioInicial.value > 0)
	{
		recalcularPrecio(numElemento);
	}
}

function recalcularPrecio (numElemento)
{
	var numElemento;
	var nombre_precio_inicial = "precio_inicial_"+numElemento;
	var objetoPrecioInicial = document.getElementById(nombre_precio_inicial);
	// el articulo tiene definido un precio inicial
	var nuevo_precio = objetoPrecioInicial.value;
	var nombre_dto2 = "dto2_"+numElemento;
	var objetoDto2 = document.getElementById(nombre_dto2);
	if (objetoDto2.value > 0)
	{
		// calculo del descuento 2
		var dto_2 = (nuevo_precio*objetoDto2.value)/100;
		dto_2 = Math.round(dto_2*100)/100; // redondeo a dos decimales
		nuevo_precio -= dto_2;
		nuevo_precio = Math.round(nuevo_precio*100)/100;
	}
	var nombre_dto3 = "dto3_"+numElemento;
	var objetoDto3 = document.getElementById(nombre_dto3);
	if (objetoDto3.value > 0)
	{
		// calculo del descuento 3
		var dto_3 = (nuevo_precio*objetoDto3.value)/100;
		dto_3 = Math.round(dto_3*100)/100; // redondeo a dos decimales
		nuevo_precio -= dto_3;
		nuevo_precio = Math.round(nuevo_precio*100)/100;
	}
	var nombre_pronto_pago = "pronto_pago_"+numElemento;
	var objetoProntoPago = document.getElementById(nombre_pronto_pago);
	if (objetoProntoPago.value > 0)
	{
		var dto_pronto_pago = (nuevo_precio*objetoProntoPago.value)/100;
		dto_pronto_pago = Math.round(dto_pronto_pago*100)/100; // redondeo a dos decimales
		nuevo_precio -= dto_pronto_pago;
		nuevo_precio = Math.round(nuevo_precio*100)/100;
	}
	var nombre_precio_final = "precio_final_"+numElemento;
	var objetoPrecioFinal = document.getElementById(nombre_precio_final);
	objetoPrecioFinal.value = nuevo_precio;
}
-->
</script>

<?php
if (!isset($user_id)) { echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente su nombre de usuario y password</a>"; include ("footer.php"); exit; }
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";

$titulo = "CREAR PRESUPUESTO";
$titulo_pagina = "CREAR PRESUPUESTO";
$enlacevolver = "modules.php?mod=gestproject&file=";
$script = "index_crear_presupuesto_new";
$script_presupuestos = "index_presupuestos_new";

$tabla_usuarios = "usuarios_t";
$tabla_m_ag_transportes = "maestro_agencias_transportes_t";
$tabla_articulos = "articulos_t";
$tabla_m_embalajes = "maestro_tipos_embalajes_t";
$tabla_art_embalajes = "articulos_embalajes_t";
$tabla_clientes = "clientes_t";
$tabla_clientes_comerciales = "clientes_comerciales_t";
$tabla_clientes_contactos = "clientes_contactos_t";
$tabla_clientes_direcciones = "clientes_direcciones_t";
$tabla_proveedores = "proveedores_t";
$tabla_prov_familias = "prov_familias_t";
$tabla_m_igic = "maestro_igic_t";
$tabla_cli_prov = "clientes_prov_t";
$tabla_cli_prov_fam = "clientes_prov_familias_t";
$tabla_cli_prov_fam_art = "clientes_prov_fam_articulos_t";
$tabla_prov_ofertas = "prov_ofertas_t";
$tabla_prov_ofertas_cli = "prov_ofertas_clientes_t";
$tabla_prov_oferta_fam = "prov_ofertas_familias_t";
$tabla_prov_oferta_fam_art = "prov_ofertas_fam_articulos_t";
$tabla_presupuestos = "presupuestos_t";
$tabla_pre_articulos = "pre_articulos_t";
$tabla_stock = "stock_t";
$tabla_stock_reserva = "stock_reserva_t";
$tabla_pre_portes = "pre_portes_t";
$tabla_art_precios = "articulos_precios_t";
$tabla_pedidos = "pedidos_t";
$tabla_ped_art = "ped_articulos_t";
/*
$tabla_almacenes = "maestro_almacenes_t";
$tabla_historico = "historico_entradas_t";
*/
if ($color_entorno != "") { $color_fondo = $color_entorno; $color_fondo_claro = $color_entorno_claro; }
else { $color_fondo = "348209"; $color_fondo_claro = "C3EBC2"; }//$color_fondo = "97c00e"; $color_fondo_claro = "dcfb73"; }
$color_fondo_tabla = "FFFFCC";
$color_fondo_tabla = $color_fondo_amarillo;

$tipo_servicio_directo = 3;

$titulo_bloque_1 = "Listado de art&iacute;culos:";
$nombres_bloque_1 = array('Ref.','Nombre','Embalaje','Ud. embalaje','Ud.','Stock','Pedido','Prov.','PVP','Dto','Dto2','Dto3','Pronto pago','Precio final','IGIC','Reservar','Compromiso');
$texto_boton_bloque_1 = "A&ntilde;adir al presupuesto";

$titulo_listado = "LISTADO DE ART&Iacute;CULOS PARA EL PRESUPUESTO";
$texto_boton_limpiar = "Limpiar listado de art&iacute;culos";
$nombres_bloque_2 = array('Ref.','Nombre','Embalaje','Ud.','Stock','Pedido','Prov.','Precio','Dto1','Dto2','Dto3','Pronto pago','IGIC','Importe parcial','IGIC parcial','Reserva','Compromiso');

$texto_boton_registro = "Generar presupuesto";
$texto_registrado = "Se ha creado el presupuesto";

//titulo pagina
echo "
<script type=\"text/JavaScript\">document.title = '".$titulo_pagina."';</script>";


echo "
<table width='100%' height='550' border='0' cellpadding='0' cellspacing='0' class='text' align='center'>
	<tr valign='top'>
		<td width=100% align=left>
";
echo "
<center><b>$titulo</b><br></center>";
echo "
<center><a href='$enlacevolver"."$script_presupuestos&pag=0'>Volver a presupuestos</a><br></center>";

if (PermisosSecciones($user_id, $script, array()) == 1)
{

// Campo para la busqueda
$campo_busqueda = "nombre";
// Variables del script
$parametros_nombres = array("accion","buscar_nombre");
$parametros_formulario = array();
// Tipos de parametros
//									Sin nada es que no sirven para los filtros o filtro distinto
// texto;campo_filtro							Es un filtro para texto
// texto								Es un filtro para texto, pero hay que desarrollar el filtrado
// select;campo_filtro							Es un filtro con select
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;desde		Es un filtro para fechas superiores a la introducida
// fecha;campo_filtro;nombre_ano;nombre_mes;nombre_dia;hasta		Es un filtro para fechas inferiores a la introducida
$parametros_tipos = array("","");
foreach($parametros_nombres as $indice_parametros => $nombre_param)
{
	$array_ele = explode(';', $parametros_tipos[$indice_parametros]);
	if ($array_ele[0] == "texto")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; $$nombre_param = str_replace('+',' ',$$nombre_param); }
		else { $$nombre_param = ""; }
	}
	if ($array_ele[0] == "select")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
	if ($array_ele[0] == "fecha")
	{
		if ($_REQUEST[$array_ele[2]] != "") { $$array_ele[2] = $_REQUEST[$array_ele[2]]; }
		else { $$array_ele[2] = ""; }
		if ($_REQUEST[$array_ele[3]] != "") { $$array_ele[3] = $_REQUEST[$array_ele[3]]; }
		else { $$array_ele[3] = ""; }
		if ($_REQUEST[$array_ele[4]] != "") { $$array_ele[4] = $_REQUEST[$array_ele[4]]; }
		else { $$array_ele[4] = ""; }
	}
	if ($array_ele[0] == "")
	{
		if ($_REQUEST[$nombre_param] != "") { $$nombre_param = $_REQUEST[$nombre_param]; }
		else { $$nombre_param = ""; }
	}
}

// INICIO ASIGNAR CLIENTE
if ($accion == "accioncliente")
{
	$_SESSION['presupuestoCliente'] = $_REQUEST['cliente_id'];
	// datos secundarios presupuesto
	$_SESSION['presupuestoComercial'] = "";
	$_SESSION['presupuestoContacto'] = "";
	$_SESSION['presupuestoDireccion'] = "";
	$_SESSION['presupuestoFecha'] = "";
	$_SESSION['presupuestoContactoNuevo'] = "";
	$_SESSION['presupuestoOrigen'] = "";
	// presupuesto
	$_SESSION['presupuestoNuevo'] = array();
}
// FIN ASIGNAR CLIENTE

// INICIO BORRAR CLIENTE
if ($accion == "accionquitar")
{
	$_SESSION['presupuestoCliente'] = "";
	// datos secundarios presupuesto
	$_SESSION['presupuestoComercial'] = "";
	$_SESSION['presupuestoContacto'] = "";
	$_SESSION['presupuestoDireccion'] = "";
	$_SESSION['presupuestoFecha'] = "";
	$_SESSION['presupuestoContactoNuevo'] = "";
	$_SESSION['presupuestoOrigen'] = "";
	// presupuesto
	$_SESSION['presupuestoNuevo'] = array();
}
// FIN BORRAR CLIENTE

// INICIO AÑADIR DATOS PASO 2
if ($accion == "acciondatos")
{
	if ($_POST["comercial_id"] > 0)
	{
		if ($_POST["direccion_id"] > 0)
		{
			if ($_POST["fecha"] != "")
			{
				list($dia, $mes, $ano) = explode('/', $_POST["fecha"]);
				$dia = str_pad($dia, 2, "0", STR_PAD_LEFT);
				$mes = str_pad($mes, 2, "0", STR_PAD_LEFT);
				$ano = str_pad($ano, 4, "0", STR_PAD_LEFT);
				if ($dia > 0 && $dia < 32 && $mes > 0 && $mes < 13 && $ano > 2000)
				{
					if ($_POST["contacto_id"] > 0 || $_POST["contacto_nuevo"] != "")
					{
						$_SESSION['presupuestoComercial'] = $_POST["comercial_id"];
						$_SESSION['presupuestoContacto'] = $_POST["contacto_id"];
						$_SESSION['presupuestoDireccion'] = $_POST["direccion_id"];
						$_SESSION['presupuestoFecha'] = "$dia/$mes/$ano";
						$_SESSION['presupuestoContactoNuevo'] = $_POST["contacto_nuevo"];
						$_SESSION['presupuestoOrigen'] = $_POST["origen_presupuesto_id"];
					}
					else
					{
						echo "<SCRIPT language='JavaScript'> alert('Alerta. Debe elegir un contacto existente o escribir el nombre de uno nuevo. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
					}
				}
				else
				{
					echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo fecha tiene un formato erroneo. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
				}
			}
			else
			{
				echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo fecha es obligatorio. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
			}
		}
		else
		{
			echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo direccion es obligatorio. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
		}
	}
	else
	{
		echo "<SCRIPT language='JavaScript'> alert('Alerta. El campo comercial es obligatorio. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
	}
}
// FIN AÑADIR DATOS PASO 2

// INICIO AÑADIR ARTICULOS
if ($accion == "actualizabloque2")
{
	//leo cesta de la sesión
	$array_cesta = array();
	$array_cesta = $_SESSION['presupuestoNuevo'];
	
	for ($i = 1; $i < $_REQUEST['cont']; $i++)
	{
		$campo_referencia = "referencia_".$i;
		$campo_proveedor_id = "proveedor_id_".$i;
		$campo_igic_id = "igic_id_".$i;
		$campo_embalaje = "embalaje_".$i;
		$campo_num_embalajes = "num_embalajes_".$i;
		$campo_unidades = "unidades_".$i;
		$campo_min_unidades_gratis = "min_unidades_gratis_".$i;
		$campo_num_unidades_gratis = "num_unidades_gratis_".$i;
		$campo_unidades_gratis = "unidades_gratis_".$i;
		$campo_min_unidades_dto = "min_unidades_dto_".$i;
		$campo_precio_unidad_dto = "precio_unidad_dto_".$i;
		$campo_precio_neto = "precio_neto_".$i;
		$campo_dto1 = "dto1_".$i;
		$campo_dto1_sin = "dto1_sin_".$i;
		$campo_dto2 = "dto2_".$i;
		$campo_dto3 = "dto3_".$i;
		$campo_pronto_pago = "pronto_pago_".$i;
		$campo_precio_final = "precio_final_".$i;
		$campo_fecha_reserva = "fecha_reserva_".$i;
		$campo_comprometido = "comprometido_".$i;
		
		//$_POST[$campo_num_embalajes] > 0 || 
		if ($_POST[$campo_embalaje] > 0 && ($_POST[$campo_unidades] > 0) && $_POST[$campo_proveedor_id] > 0 && $_POST[$campo_igic_id] > 0)
		{
			$indice_array_cesta = $_REQUEST[$campo_referencia]."/".$_REQUEST[$campo_proveedor_id];
			//actualizo cesta
			// referencia = articulos_t.id
			$array_cesta[$indice_array_cesta]["referencia"] = $_REQUEST[$campo_referencia];
			// embalaje = articulos_embalajes_t.id
			$array_cesta[$indice_array_cesta]["embalaje"] = $_REQUEST[$campo_embalaje];
			// unidades (input unidades)
			$array_cesta[$indice_array_cesta]["unidades"] = $_REQUEST[$campo_unidades];
			// min_unidades_gratis = prov_ofertas_fam_articulos_t.min_unidades_gratis
			$array_cesta[$indice_array_cesta]["min_unidades_gratis"] = $_REQUEST[$campo_min_unidades_gratis];
			// num_unidades_gratis = prov_ofertas_fam_articulos_t.num_unidades_gratis
			$array_cesta[$indice_array_cesta]["num_unidades_gratis"] = $_REQUEST[$campo_num_unidades_gratis];
			// unidades_gratis (pre_articulos_t.unidades_gratis)
			// las unidades gratis se calculan segun lo puesto en la oferta
			$array_cesta[$indice_array_cesta]["unidades_gratis"] = $_REQUEST[$campo_unidades_gratis];
			// proveedor_id = proveedores_t.id
			$array_cesta[$indice_array_cesta]["proveedor_id"] = $_REQUEST[$campo_proveedor_id];
			// min_unidades_dto = prov_ofertas_fam_articulos_t.min_unidades_dto
			$array_cesta[$indice_array_cesta]["min_unidades_dto"] = $_REQUEST[$campo_min_unidades_dto];
			// precio_unidad_dto = prov_ofertas_fam_articulos_t.precio_unidad_dto
			$array_cesta[$indice_array_cesta]["precio_unidad_dto"] = $_REQUEST[$campo_precio_unidad_dto];
			// precio_neto
			// el precio neto sera el valor de:
			// - precio neto del articulo para la familia de una oferta de un proveedor (prov_ofertas_fam_articulos_t.precio_neto_oferta_art)
			// - en su defecto, si se supera las unidades minimas de la oferta el precio neto definido (prov_ofertas_fam_articulos_t.min_unidades_dto, prov_ofertas_fam_articulos_t.precio_unidad_dto)
			// - en su defecto, precio neto del articulo definido para la familia de un proveedor del cliente (clientes_prov_fam_articulos_t.precio_neto_cli_art_deposito)
			$valor_precio_neto = "";
			$valor_dto1 = "";
			if ($_REQUEST[$campo_min_unidades_dto] > 0 && $_REQUEST[$campo_precio_unidad_dto] > 0 && $_REQUEST[$campo_unidades] >= $_REQUEST[$campo_min_unidades_dto])
			{
				// se aplica la oferta
				$valor_precio_neto = $_REQUEST[$campo_precio_unidad_dto];
				$valor_dto1 = 0;
			}
			else
			{
				$valor_precio_neto = $_REQUEST[$campo_precio_neto];
				$valor_dto1 = $_REQUEST[$campo_dto1];
			}
			$array_cesta[$indice_array_cesta]["precio_neto"] = $valor_precio_neto;
			// precio_neto_sin_oferta = en caso de no cumplir la oferta de [comprando X unidades la unidad sale a Y euros] el campo precio_neto debe coger este valor
			$array_cesta[$indice_array_cesta]["precio_neto_sin_oferta"] = $_REQUEST[$campo_precio_neto];
			// dto1 (pre_articulos_t.dto_porc)
			// el dto1 sera el valor de:
			// - dto% del articulo para la familia de una oferta de un proveedor (prov_ofertas_fam_articulos_t.dto_porc_oferta_art)
			// - en su defecto, dto% del articulo definido para la familia de un proveedor del cliente (clientes_prov_fam_articulos_t.dto_porc_cli_art_deposito)
			// - en su defecto, dto% de la familia para un proveedor del cliente (clientes_prov_familias_t.dto_porc_cli_fam_deposito)
			// - en su defecto, dto% de la familia para un proveedor (prov_familias_t.dto_porc_prov_fam_deposito)
			$array_cesta[$indice_array_cesta]["dto1"] = $valor_dto1;
			// dto1_sin_oferta = en caso de no cumplir la oferta de [comprando X unidades la unidad sale a Y euros] el campo dto1 debe coger este valor
			$array_cesta[$indice_array_cesta]["dto1_sin_oferta"] = $_REQUEST[$campo_dto1_sin];
			// dto2 (pre_articulos_t.dto2)
			$array_cesta[$indice_array_cesta]["dto2"] = $_REQUEST[$campo_dto2];
			// dto3 (pre_articulos_t.dto3)
			$array_cesta[$indice_array_cesta]["dto3"] = $_REQUEST[$campo_dto3];
			// pronto_pago = clientes_prov_t.dto_porc_pronto_pago
			$array_cesta[$indice_array_cesta]["pronto_pago"] = $_REQUEST[$campo_pronto_pago];
			// precio_unidad (pre_articulos_t.precio_unidad)
			// el precio_unidad es el precio final del articulo, se calcula aplicando el dto2, despues el dto3 y despues el pronto_pago al precio definido al articulo segun el caso:
			// - caso 1: no se define ni dto% ni precios neto => coincide con articulos_precios_t.precio
			// - caso 2: se define algun dto% (no se contabiliza el dto2 y el dto3 para esto) => sera el resultado de aplicar el dto% a articulos_precios_t.precio
			// - caso 3: se define algun precio neto => se utilizara ese precio_neto
			$array_cesta[$indice_array_cesta]["precio_unidad"] = $_REQUEST[$campo_precio_final];
			$array_cesta[$indice_array_cesta]["igic_id"] = $_REQUEST[$campo_igic_id];
			
			list($dia, $mes, $ano) = explode('/', $_POST[$campo_fecha_reserva]);
			$dia = str_pad($dia, 2, "0", STR_PAD_LEFT);
			$mes = str_pad($mes, 2, "0", STR_PAD_LEFT);
			$ano = str_pad($ano, 4, "0", STR_PAD_LEFT);
			if ($dia > 0 && $dia < 32 && $mes > 0 && $mes < 13 && $ano > 2000)
			{
				$nombre_articulo = "";
				$consulta_art = "select * from $tabla_articulos where id='".$_REQUEST[$campo_referencia]."';";
				$resultado_art = mysql_query($consulta_art) or die("La consulta falló: $consulta_art" . mysql_error());
				while ($linea_art = mysql_fetch_array($resultado_art, MYSQL_ASSOC))
				{
					$nombre_articulo = $linea_art['nombre'];
				}
				// fecha correcta
				if (strtotime("$ano-$mes-$dia") >= strtotime(date("Y-m-d")))
				{
					// fecha minima hoy
					// calculo del stock disponible (no contar las reservas)
					$cantidad_stock = 0;
					$cantidad_stock = CantidadStock($_REQUEST[$campo_referencia], $_REQUEST[$campo_proveedor_id]);
					$cantidad_reservar = $_REQUEST[$campo_unidades] + $_REQUEST[$campo_unidades_gratis];
					if ($cantidad_stock >= $cantidad_reservar)
					{
						$array_cesta[$indice_array_cesta]["fecha_reserva"] = "$ano-$mes-$dia";
					}
					else
					{
						$array_cesta[$indice_array_cesta]["fecha_reserva"] = "0000-00-00";
						$nombre_proveedor = "";
						$consulta_prov = "select * from $tabla_proveedores where id='".$_REQUEST[$campo_proveedor_id]."';";
						$resultado_prov = mysql_query($consulta_prov) or die("La consulta falló: $consulta_prov" . mysql_error());
						while ($linea_prov = mysql_fetch_array($resultado_prov, MYSQL_ASSOC))
						{
							$nombre_proveedor = $linea_prov['nombre_corto'];
						}
						echo "<b>No se pudieron reservar ".$cantidad_reservar." unidades del articulo $nombre_articulo porque solo hay $cantidad_stock unidades en stock para el proveedor $nombre_proveedor.</b><br>";
					}
				}
				else
				{
					$array_cesta[$indice_array_cesta]["fecha_reserva"] = "0000-00-00";
					echo "<b>No se pudo reservar el articulo $nombre_articulo porque la fecha puesta es previa a hoy.</b><br>";
				}
			}
			else
			{
				$array_cesta[$indice_array_cesta]["fecha_reserva"] = "0000-00-00";
			}
			
			$array_cesta[$indice_array_cesta]["comprometido"] = $_REQUEST[$campo_comprometido];
		}
	}
	
	//vuelvo a guardar cesta en sesion
	$_SESSION['presupuestoNuevo'] = $array_cesta;
}
// FIN AÑADIR ARTICULOS

// INICIO BORRAR TODOS LOS ARTICULOS
if ($accion == "borrartodo")
{
	$_SESSION['presupuestoNuevo'] = array();
}
// FIN BORRAR TODOS LOS ARTICULOS

// INICIO BORRAR UN ARTICULO
if ($accion == "eliminarlinea")
{
	//recupero de la sesión
	$array_cesta = array();
	$array_cesta = $_SESSION['presupuestoNuevo'];
	//array_splice($cesta[$ref],'0',1);
	unset($array_cesta[$_REQUEST['referencia']]);
	
	$_SESSION['presupuestoNuevo'] = $array_cesta;
}
// FIN BORRAR UN ARTICULO

// INICIO REGISTRAR ARTICULOS
if ($accion == "registrar")
{
	// comprobacion de los datos del presupuesto
	$correcto = 0;
	if ($_SESSION["presupuestoComercial"] > 0)
	{
		if ($_SESSION["presupuestoDireccion"] > 0)
		{
			if ($_SESSION["presupuestoFecha"] != "")
			{
				list($dia, $mes, $ano) = explode('/', $_SESSION["presupuestoFecha"]);
				$dia = str_pad($dia, 2, "0", STR_PAD_LEFT);
				$mes = str_pad($mes, 2, "0", STR_PAD_LEFT);
				$ano = str_pad($ano, 4, "0", STR_PAD_LEFT);
				if ($dia > 0 && $dia < 32 && $mes > 0 && $mes < 13 && $ano > 2000)
				{
					if ($_SESSION["presupuestoContacto"] > 0 || $_SESSION["presupuestoContactoNuevo"] != "")
					{
						$correcto = 1;
					}
					else
					{
						echo "<SCRIPT language='JavaScript'> alert('Alerta. No hay contacto elegido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
					}
				}
				else
				{
					echo "<SCRIPT language='JavaScript'> alert('Alerta. No hay fecha elegido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
				}
			}
			else
			{
				echo "<SCRIPT language='JavaScript'> alert('Alerta. No hay fecha elegido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
			}
		}
		else
		{
			echo "<SCRIPT language='JavaScript'> alert('Alerta. No hay direccion elegido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
		}
	}
	else
	{
		echo "<SCRIPT language='JavaScript'> alert('Alerta. No hay comercial elegido. No se puede continuar.'); history.back(); </SCRIPT>"; exit;
	}
	
	if ($correcto == 1)
	{
		$array_cesta = array();
		$array_cesta = $_SESSION['presupuestoNuevo'];
		//echo print_r($array_cesta,true);
		
		if (count($array_cesta) > 0)
		{
			list($dia,$mes,$ano) = explode('/', $_SESSION['presupuestoFecha']);
			
			$num_presupuesto_ultimo = "";
			$consulta1  = "select numero from $tabla_presupuestos where ano='".$ano."' order by id desc limit 1;";
			//echo "$consulta1<br>";
			$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea1 = mysql_fetch_array($resultado1)) {
				$num_presupuesto_ultimo = $linea1['numero'];
			}
			list($tmp, $num_presupuesto_ultimo) = explode('/',$num_presupuesto_ultimo);
			$num_presupuesto_ultimo++;
			$num_presupuesto = "PR/".str_pad($num_presupuesto_ultimo, 4, "0", STR_PAD_LEFT)."/".$ano;
			
			// comprobacion del contacto del presupuesto
			$valor_contacto_cliente = 0;
			if ($_SESSION["presupuestoContacto"] > 0) { $valor_contacto_cliente = $_SESSION["presupuestoContacto"]; }
			elseif ($_SESSION["presupuestoContacto"] == 0 && $_SESSION["presupuestoContactoNuevo"] != "")
			{
				// insertar el nuevo contacto del cliente
				$cons_nuevo_contacto = "insert into $tabla_clientes_contactos set cliente_id='".$_SESSION['presupuestoCliente']."', nombre='".$_SESSION['presupuestoContactoNuevo']."';";
				//echo "cons_nuevo_contacto: $cons_nuevo_contacto<br>";
				$res_nuevo_contacto = mysql_query($cons_nuevo_contacto) or die("La consulta fall&oacute;: $cons_nuevo_contacto" . mysql_error());
				$valor_contacto_cliente =  mysql_insert_id();
			}
			
			// inicialmente se crea el presupuesto en estado creado (1)
			$consulta_presupuesto = "insert into $tabla_presupuestos set numero=\"".$num_presupuesto."\", fecha='$ano-$mes-$dia', ano='".$ano."', cliente_id='".$_SESSION['presupuestoCliente']."', comercial_id='".$_SESSION['presupuestoComercial']."', contacto_id='".$valor_contacto_cliente."', cliente_direccion_id='".$_SESSION['presupuestoDireccion']."', origen_presupuesto_id='".$_SESSION['presupuestoOrigen']."', observaciones_publico='".str_replace("'","",$_REQUEST['obs_publico'])."', observaciones_privado='".str_replace("'","",$_REQUEST['obs_privado'])."', observaciones_aviso='".str_replace("'","",$_REQUEST['obs_aviso'])."', importe_sin_igic='0', importe_igic='0', importe_total='0', estado_id='".$presu_estado_creado."';";
			//echo "$consulta_presupuesto<br>";
			$res_presupuesto = mysql_query($consulta_presupuesto) or die("$consulta_presupuesto, La consulta falló: " . mysql_error());
			$ultimo_presupuesto_id = mysql_insert_id();
			
			$valor_fecha_calculo = "$ano-$mes-$dia";
			$importe_total = 0;
			$array_importes_proveedores = array();
			foreach ($array_cesta as $valor)
			{
				$unidades_pedidas = $valor['unidades'];
				$tipo_embalaje_id = 0;
				$unidades_embalaje = 0;
				$consulta_cat = "select $tabla_art_embalajes.tipo_embalaje_id, $tabla_art_embalajes.unidades from $tabla_art_embalajes where $tabla_art_embalajes.id='".$valor['embalaje']."';";
				$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
				while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
				{
					$tipo_embalaje_id = $linea_cat['tipo_embalaje_id'];
					$unidades_embalaje = $linea_cat['unidades'];
				}
				
				$unidades_totales = 0;
				$cons_stock = "select sum(unidades) as total from $tabla_stock where articulo_id='".$valor["referencia"]."' and proveedor_id='".$valor["proveedor_id"]."';";
				//echo $cons_stock;
				$res_stock = mysql_query($cons_stock) or die("La consulta fall&oacute;: $cons_stock " . mysql_error());
				while ($lin_stock = mysql_fetch_array($res_stock, MYSQL_ASSOC))
				{
					$unidades_totales = (int) $lin_stock['total'];
				}
				
				$dto_unidad = 0;
				$precio_inicial_articulo = 0;
				$precio_articulo = 0;
				$dto_articulo = 0;
				$precio_neto = 0;
				$importe_art_parcial = 0;
				$importe_igic_parcial = 0;
				$consulta_art = "select * from $tabla_articulos where id='".$valor['referencia']."';";
				$resultado_art = mysql_query($consulta_art) or die("La consulta falló: $consulta_art" . mysql_error());
				while ($linea_art = mysql_fetch_array($resultado_art, MYSQL_ASSOC))
				{
					$cons_precio = "select * from $tabla_art_precios where articulo_id='".$valor['referencia']."' and proveedor_id='".$valor['proveedor_id']."';";
					$res_precio = mysql_query($cons_precio) or die("La consulta fall&oacute;: $cons_precio " . mysql_error());
					while ($lin_precio = mysql_fetch_array($res_precio, MYSQL_ASSOC))
					{
						$precio_inicial_articulo = $lin_precio['precio'];
					}
					$valor_igic = "";
					$consulta_cat = "select * from $tabla_m_igic where $tabla_m_igic.id='".$valor['igic_id']."';";
					$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
					while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
					{
						$valor_igic = $linea_cat['valor'];
					}
					//$importe_art_parcial = $precio_articulo*$unidades_pedidas;
					$importe_art_parcial = $valor["precio_unidad"]*$unidades_pedidas;
					$importe_igic_parcial = round(($importe_art_parcial*$valor_igic)/100, 2);
					
					if ($valor["fecha_reserva"] == "0000-00-00" || ($valor["fecha_reserva"] != "0000-00-00" && $unidades_totales < $unidades_pedidas))
					{
						// si no esta reservado o si se intenta reservar mas del stock existente (que no se deja reservar): se suma para el calculo de los portes
						$array_importes_proveedores[$valor["proveedor_id"]]["importe_arti"] += $importe_art_parcial;
						$array_importes_proveedores[$valor["proveedor_id"]]["importe_igic"] += $importe_igic_parcial;
					}
				}
/*
presupuesto_id, articulo_id, tipo_embalaje_id, unidades_embalaje, unidades, proveedor_id, precio_articulo
dto_porc, precio_neto, precio_unidad, dto2, dto3, pronto_pago,
importe_articulo, igic_id, importe_igic,
reservado
fecha_max_reserva
comprometido

min_unidades_dto
precio_unidad_dto
precio_neto_sin_oferta
dto_porc_sin_oferta
min_unidades_gratis
num_unidades_gratis
unidades_gratis
*/
				$consulta_pre_art = "insert into $tabla_pre_articulos set presupuesto_id='".$ultimo_presupuesto_id."', articulo_id='".$valor["referencia"]."', tipo_embalaje_id='".$tipo_embalaje_id."', unidades_embalaje='".$unidades_embalaje."', unidades='".$unidades_pedidas."', proveedor_id='".$valor["proveedor_id"]."', precio_articulo='".$precio_inicial_articulo."', 
dto_porc='".$valor["dto1"]."', precio_neto='".$valor["precio_neto"]."', precio_unidad='".$valor["precio_unidad"]."', dto2='".$valor['dto2']."', dto3='".$valor['dto3']."', pronto_pago='".$valor['pronto_pago']."', 
importe_articulo='".$importe_art_parcial."', igic_id='".$valor['igic_id']."', importe_igic='".$importe_igic_parcial."', 
min_unidades_dto='".$valor['min_unidades_dto']."', precio_unidad_dto='".$valor['precio_unidad_dto']."', precio_neto_sin_oferta='".$valor['precio_neto_sin_oferta']."', dto_porc_sin_oferta='".$valor['dto1_sin_oferta']."', min_unidades_gratis='".$valor['min_unidades_gratis']."', num_unidades_gratis='".$valor['num_unidades_gratis']."', unidades_gratis='".$valor['unidades_gratis']."', ";
				$unidades_reservadas = $unidades_pedidas+$valor['unidades_gratis'];
				if ($valor["fecha_reserva"] != "0000-00-00" && $unidades_totales >= $unidades_reservadas)
				{
					// si se marco una fecha de reserva y hay suficentes unidades (se reservan tambien las unidades gratis si las hubiese)
					$consulta_pre_art .= "reservado='on', fecha_max_reserva='".$valor["fecha_reserva"]."', ";
				}
				else
				{
					$consulta_pre_art .= "reservado='', fecha_max_reserva='0000-00-00', ";
				}
				$consulta_pre_art .= " comprometido='".$valor["comprometido"]."';";
				//echo "$consulta_pre_art<br>";
				$resultado_pre_art = mysql_query($consulta_pre_art) or die("$consulta_pre_art, La consulta falló: " . mysql_error());
				$ultimo_articulo_id = mysql_insert_id();
				
				// se retiran de stock los articulos reservados
				if ($valor["fecha_reserva"] != "0000-00-00" && $unidades_totales >= $unidades_reservadas)
				{
					// hay suficientes unidades en total
					//$unidades_reservadas => las que se reservan
					$cons_stock = "select * from $tabla_stock where articulo_id='".$valor["referencia"]."' and proveedor_id='".$valor["proveedor_id"]."' order by id;";
					//echo $cons_stock;
					$res_stock = mysql_query($cons_stock) or die("La consulta fall&oacute;: $cons_stock " . mysql_error());
					while ($lin_stock = mysql_fetch_array($res_stock, MYSQL_ASSOC))
					{
						// si quedan unidades por reservar
						if ($unidades_reservadas > 0)
						{
							if ($lin_stock['unidades'] > $unidades_reservadas)
							{
								// hay mas stock del que se va a reservar : se crea la reserva y se actualizan las unidades que quedan
								$consulta_art_reserva = "insert into $tabla_stock_reserva set unidades='".$unidades_reservadas."', articulo_id='".$lin_stock['articulo_id']."', proveedor_id='".$lin_stock['proveedor_id']."', almacen_id='".$lin_stock['almacen_id']."', estanteria='".$lin_stock['estanteria']."', hueco='".$lin_stock['hueco']."', piso='".$lin_stock['piso']."', palet='".$lin_stock['palet']."', pre_articulo_id='".$ultimo_articulo_id."';";
								$nuevo_stock = $lin_stock['unidades'] - $unidades_reservadas;
								// ya no quedan unidades que reservar
								$unidades_reservadas = 0;
								$cons_stock2 = "update $tabla_stock set unidades='$nuevo_stock' where id='".$lin_stock['id']."'";
								//echo "cons_stock2: $cons_stock2<br>";
								$res_stock2 = mysql_query($cons_stock2) or die("La consulta fall&oacute;: $cons_stock2" . mysql_error());
							}
							else
							{
								// hay menos o la misma cantidad de stock que lo que se va a reservar: se crea la reserva y se borra el registro en el stock
								$consulta_art_reserva = "insert into $tabla_stock_reserva set unidades='".$lin_stock['unidades']."', articulo_id='".$lin_stock['articulo_id']."', proveedor_id='".$lin_stock['proveedor_id']."', almacen_id='".$lin_stock['almacen_id']."', estanteria='".$lin_stock['estanteria']."', hueco='".$lin_stock['hueco']."', piso='".$lin_stock['piso']."', palet='".$lin_stock['palet']."', pre_articulo_id='".$ultimo_articulo_id."';";
								// se restan las unidades ya reservadas y se continua reservando
								$unidades_reservadas -= $lin_stock['unidades'];
								$cons_stock2 = "delete from $tabla_stock where id='".$lin_stock['id']."'";
								//echo "cons_stock2: $cons_stock2<br>";
								$res_stock2 = mysql_query($cons_stock2) or die("La consulta fall&oacute;: $cons_stock2" . mysql_error());
							}
							//echo "$consulta_art_reserva<br>";
							$resultado_art_reserva = mysql_query($consulta_art_reserva) or die("$consulta_art_reserva, La consulta falló: " . mysql_error());
						}
					}
				}
			}
			
			// calculo de portes
			$consulta_cat = "select $tabla_clientes_direcciones.municipio_id from $tabla_clientes_direcciones where $tabla_clientes_direcciones.id='".$_SESSION['presupuestoDireccion']."';";
			$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
			{
				$cons1 = "select * from maestro_municipios_t where id='".$linea_cat['municipio_id']."';";
				$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
				while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
				{
					if ($lin1['isla_id'] != $identificador_gran_canaria)
					{
						// hay que calcular portes
						foreach ($array_importes_proveedores as $valor_prov => $array_datos)
						{
							// busqueda del minimo que hay que superar para que no pague el cliente (primero el definido en clientes_prov_t, despues en proveedores_t)
							//$porc_portes = 0; => ya no se va a calcular
							$importe_min_portes_pagados = 0;
							// primero busco las condiciones del proveedor, si existen condiciones particulares para el cliente se machacaran los valores
							$cons_porte = "select * from $tabla_proveedores where id='".$valor_prov."';";
							$res_porte = mysql_query($cons_porte) or die("La consulta fall&oacute;: $cons_porte " . mysql_error());
							while ($lin_porte = mysql_fetch_array($res_porte, MYSQL_ASSOC))
							{
								//$porc_portes = $lin_porte['prov_porc_portes']; => ya no se va a calcular
								$importe_min_portes_pagados = $lin_porte['prov_importe_min_portes_pagados'];
							}
							$cons_porte = "select * from $tabla_cli_prov where cliente_id='".$_SESSION['presupuestoCliente']."' and proveedor_id='".$valor_prov."';";
							$res_porte = mysql_query($cons_porte) or die("La consulta fall&oacute;: $cons_porte " . mysql_error());
							while ($lin_porte = mysql_fetch_array($res_porte, MYSQL_ASSOC))
							{
								if ($lin_porte['importe_min_portes_pagados'] > 0)//$lin_porte['porc_portes'] > 0 && 
								{
									//$porc_portes = $lin_porte['porc_portes']; => ya no se va a calcular
									$importe_min_portes_pagados = $lin_porte['importe_min_portes_pagados'];
								}
							}
							// contabilizo el importe sin igic
							$importe_total_prov = $array_datos["importe_arti"];//$array_datos["importe_igic"]
							//, porc_portes='".$porc_portes."' => ya no se va a calcular
							$cons_insert_porte = "insert into $tabla_pre_portes set presupuesto_id='".$ultimo_presupuesto_id."', proveedor_id='".$valor_prov."', importe_min_portes_pagados='".$importe_min_portes_pagados."'";
							if ($importe_total_prov <= $importe_min_portes_pagados)
							{
								// no supera el minimo para que sea el proveedor quien pague los portes: se marca que se debe usar la ag transportes del cliente
								//$importe_portes = round(($importe_total_prov*$porc_portes)/100, 2); => ya no se va a calcular
								//, importe_portes='".$importe_portes."'
								$cons_insert_porte .= ", usar_cliente='on'";
							}
							else
							{
								// si se supera el minimo: como lo paga el proveedor el importe es cero y se marca que se debe usar la ag transportes del proveedor
								//, importe_portes='0' => ya no se va a calcular
								$cons_insert_porte .= ", usar_cliente=''";
							}
							$ag_tansp_cli = 0;
							$cons_transporte = "select $tabla_cli_prov.agencia_transporte_id, $tabla_m_ag_transportes.nombre from $tabla_cli_prov join $tabla_m_ag_transportes on $tabla_m_ag_transportes.id=$tabla_cli_prov.agencia_transporte_id where $tabla_cli_prov.cliente_id='".$_SESSION['presupuestoCliente']."' and $tabla_cli_prov.proveedor_id='".$valor_prov."';";
							//echo "$cons_transporte<br>";
							$res_transporte = mysql_query($cons_transporte) or die("La consulta fall&oacute;: $cons_transporte " . mysql_error());
							while ($lin_transporte = mysql_fetch_array($res_transporte, MYSQL_ASSOC))
							{
								$ag_tansp_cli = $lin_transporte['agencia_transporte_id'];
							}
							$ag_tansp_prov = 0;
							$cons_transporte = "select prov_agencias_transportes_t.agencia_transporte_id, $tabla_m_ag_transportes.nombre from prov_agencias_transportes_t join $tabla_m_ag_transportes on $tabla_m_ag_transportes.id=prov_agencias_transportes_t.agencia_transporte_id where prov_agencias_transportes_t.proveedor_id='".$valor_prov."' order by prov_agencias_transportes_t.id limit 1;";
							//echo "$cons_transporte<br>";
							$res_transporte = mysql_query($cons_transporte) or die("La consulta fall&oacute;: $cons_transporte " . mysql_error());
							while ($lin_transporte = mysql_fetch_array($res_transporte, MYSQL_ASSOC))
							{
								$ag_tansp_prov = $lin_transporte['agencia_transporte_id'];
							}
							$cons_insert_porte .= ", cli_ag_transporte_id='".$ag_tansp_cli."', prov_ag_transporte_id='".$ag_tansp_prov."';";
							//echo "$cons_insert_porte<br>";
							$resultado_insert_porte = mysql_query($cons_insert_porte) or die("$cons_insert_porte, La consulta falló: " . mysql_error());
						}
					}
				}
			}
			
			$array_resul = CalcularPresupuesto($ultimo_presupuesto_id);
			$importe_sin_igic = $array_resul[0];
			$importe_igic = $array_resul[1];
			$importe_total = $array_resul[2];
			$consulta_presupuesto2 = "update $tabla_presupuestos set importe_sin_igic=\"".$importe_sin_igic."\", importe_igic=\"".$importe_igic."\", importe_total=\"".$importe_total."\" where id='".$ultimo_presupuesto_id."';";
			//echo "$consulta_presupuesto2<br>";
			$res_presupuesto2 = mysql_query($consulta_presupuesto2) or die("$consulta_presupuesto2, La consulta falló: " . mysql_error());
			$_SESSION['presupuestoCliente'] = "";
			$_SESSION['presupuestoComercial'] = "";
			$_SESSION['presupuestoContacto'] = "";
			$_SESSION['presupuestoDireccion'] = "";
			$_SESSION['presupuestoFecha'] = "";
			$_SESSION['presupuestoContactoNuevo'] = "";
			$_SESSION['presupuestoOrigen'] = "";
			$_SESSION['presupuestoNuevo'] = array();
			echo "<br><b>$texto_registrado</b><br><br>";
			
			echo "<form name=form_volver1 method=get action='modules.php'>
<input type=hidden name=mod value=gestproject>
<input type=hidden name=file value=$script_presupuestos>
<input type=hidden name=pag value=0>
</form>
<script>document.form_volver1.submit();</script>";
		}
		else
		{
			echo "<br><b>No se puede crear un presupuesto sin seleccionar art&iacute;culos</b><br><br>";
		}
	}
}
// FIN REGISTRAR ARTICULOS

//jgs
//'presupuestoCliente','presupuestoComercial','presupuestoContacto','presupuestoDireccion','presupuestoFecha','presupuestoContactoNuevo','presupuestoOrigen',
/*
$array_var_session = array('presupuestoNuevo');
echo "inicio sesion<br>";
foreach ($array_var_session as $valor_session)
{
	if (is_array($_SESSION[$valor_session])) { echo $valor_session.": <pre>".print_r($_SESSION[$valor_session],true)."</pre><br>"; }
	else { echo $valor_session.": ".$_SESSION[$valor_session]."<br>"; }
}
echo "fin sesion<br>";
*/
//echo print_r($nombres_bloque_1,true)."<br>";
//echo print_r($nombres_bloque_2,true)."<br>";

//******************************* PASO 1: SELECCIONAR CLIENTE
echo "
<table align='center' style='background-color:#$color_fondo_amarillo; border: 1px solid #000000;'>
	<form name=\"buscar1\" id=\"buscar1\" method=\"post\" action=\"$enlacevolver"."$script\">
	<tr align='center'>
		<td colspan='6'>Cliente: <select name='select1' onchange=\"saltoPagina('parent',this,0)\">
		<option value='$enlacevolver"."$script&accion=accionquitar'>Seleccionar un cliente</option>";
$consultaselect = "select id, nombre from $tabla_clientes where $tabla_clientes.tipo_cliente_id='1' order by nombre;";
//echo "<option>$consultaselect</option>";
$resultadoselect = mysql_query($consultaselect) or die("La consulta fall&oacute;: " . mysql_error());
while ($lineaselect = mysql_fetch_array($resultadoselect, MYSQL_ASSOC))
{
	echo "<option value='$enlacevolver"."$script&accion=accioncliente&cliente_id=$lineaselect[id]'";
	if ($lineaselect['id'] == $_SESSION['presupuestoCliente']) { echo " selected"; }
	echo ">$lineaselect[nombre]</option>";
}
echo "</select></td>
	</tr>
	</form>";
//echo "</table>";

//******************************* PASO 2: OTROS DATOS ANTES DE LOS ARTICULOS
if ($_SESSION['presupuestoCliente'] > 0)
{
//echo "<table align='center' style='background-color:#$color_fondo_amarillo; border: 1px solid #000000;'>";
echo "
	<form name=\"buscar2\" id=\"buscar2\" method=\"post\" action=\"$enlacevolver"."$script\">
	<input type=hidden name=accion value='acciondatos'>
	<tr>
		<td>Comercial:</td>
		<td><select name='comercial_id'>";
	$num_comerciales = 0;
	$consulta_cat = "select count($tabla_usuarios.id) as total from $tabla_usuarios join $tabla_clientes_comerciales on $tabla_clientes_comerciales.usuario_id=$tabla_usuarios.id where $tabla_clientes_comerciales.cliente_id='".$_SESSION['presupuestoCliente']."';";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		$num_comerciales = $linea_cat['total'];
	}
	if ($num_comerciales == 0)
	{
		echo "<option value='0'>Elegir uno</option>";
	}
	$consulta_cat = "select $tabla_usuarios.id, $tabla_usuarios.nombre from $tabla_usuarios join $tabla_clientes_comerciales on $tabla_clientes_comerciales.usuario_id=$tabla_usuarios.id where $tabla_clientes_comerciales.cliente_id='".$_SESSION['presupuestoCliente']."' group by $tabla_usuarios.id order by $tabla_usuarios.nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $_SESSION['presupuestoComercial']) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select></td>
		<td>Encargado por:</td>
		<td><select name='contacto_id'><option value='0'>Elegir uno</option>";
	$consulta_cat = "select $tabla_clientes_contactos.* from $tabla_clientes_contactos where $tabla_clientes_contactos.cliente_id='".$_SESSION['presupuestoCliente']."' order by $tabla_clientes_contactos.principal desc, $tabla_clientes_contactos.nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $_SESSION['presupuestoContacto']) { echo " selected"; } echo ">$linea_cat[nombre] $linea_cat[apellidos]</option>";
	}
	echo "</select></td>
		<td>Direccion:</td>
		<td><select name='direccion_id'>";
	$num_direcciones = 0;
	$consulta_cat = "select count($tabla_clientes_direcciones.id) as total from $tabla_clientes_direcciones where $tabla_clientes_direcciones.cliente_id='".$_SESSION['presupuestoCliente']."' and $tabla_clientes_direcciones.municipio_id>0;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		$num_direcciones = $linea_cat['total'];
	}
	if ($num_direcciones == 0)
	{
		echo "<option value='0'>Elegir una</option>";
	}
	$consulta_cat = "select $tabla_clientes_direcciones.* from $tabla_clientes_direcciones where $tabla_clientes_direcciones.cliente_id='".$_SESSION['presupuestoCliente']."' and $tabla_clientes_direcciones.municipio_id>0 order by $tabla_clientes_direcciones.principal desc, $tabla_clientes_direcciones.direccion;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		$nombre_municipio = "";
		$cons1 = "select * from maestro_municipios_t where id='".$linea_cat['municipio_id']."';";
		$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
		while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
		{
			$nombre_municipio = $lin1['nombre'];
			if ($lin1['isla_id'] > 0)
			{
				$cons2 = "select * from maestro_islas_t where id='".$lin1['isla_id']."';";
				$res2 = mysql_query($cons2) or die("La consulta fall&oacute;: $cons2 " . mysql_error());
				while ($lin2 = mysql_fetch_array($res2, MYSQL_ASSOC))
				{
					$nombre_municipio .= " ($lin2[nombre])";
				}
			}
		}
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $_SESSION['presupuestoDireccion']) { echo " selected"; } echo ">$linea_cat[direccion] $linea_cat[cp] - $nombre_municipio</option>";
	}
	echo "</select></td>
	</tr>
	<tr>
		<td>Fecha:</td>
		<td>";
	if ($_SESSION['presupuestoFecha'] != "")
	{
		list($dia, $mes, $ano) = explode('/', $_SESSION['presupuestoFecha']);
	}
	else { $ano = date("Y"); $mes = date("m"); $dia = date("d"); }//$ano = "0000"; $mes = "00"; $dia = "00"; }
	echo "<input type='text' name='fecha' id='fecha' size='10' value='$dia/$mes/$ano'/ readonly='readonly' >";
	echo "
<img src='images/calendar.gif' name='boton"."fecha' border='0' id='boton"."fecha' style='cursor: pointer; border: 0px solid blue;' title='Seleccione fecha entrada' />
<script type='text/javascript'>
	Calendar.setup({
		inputField	:	'fecha',		// id of the input field
		trigger		:	'boton"."fecha',	// trigger for the calendar (button ID)
		onSelect	:	function() { this.hide() },
		min		:	".date("Ymd")."
	});
</script>";
	echo "</td>
		<td>Nuevo contacto:</td>
		<td><input type='text' name=contacto_nuevo value='".$_SESSION['presupuestoContactoNuevo']."'></td>
		<td>Mediante:</td>
		<td><select name='origen_presupuesto_id'><option value='0'>Elegir uno</option>";
	$consulta_cat = "select maestro_origen_presupuesto_t.* from maestro_origen_presupuesto_t order by maestro_origen_presupuesto_t.nombre;";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		echo "<option value='$linea_cat[id]'"; if ($linea_cat['id'] == $_SESSION['presupuestoOrigen']) { echo " selected"; } echo ">$linea_cat[nombre]</option>";
	}
	echo "</select></td>
	</tr>
	<tr align='center'>
		<td colspan='6'><input type=submit name='Siguiente' value='Siguiente'></td>
	</tr>
	</form>";
}
echo "
</table>";

//******************************* FILTRADO ARTICULOS
if ($_SESSION['presupuestoCliente'] > 0 && $_SESSION['presupuestoDireccion'] > 0 && $_SESSION['presupuestoFecha'] != "")
{
	echo "
<table align='center'>
	<form name=\"buscar\" id=\"buscar\" method=\"post\" action=\"$enlacevolver"."$script\">
	<input type=hidden name=accion value='buscar'>
	<tr align='center'>
		<td>Buscar art&iacute;culo:</td>
		<td><input type='text' name=buscar_nombre value='$buscar_nombre'></td>
		<td><input type=submit name='Buscar' value='Buscar'></td>
	</tr>
	</form>
</table>";
}

//***************************** BLOQUE 1: SELECCIONAR ARTICULOS
echo "
$titulo_bloque_1";
if ($buscar_nombre != "")
{
	// inicio aviso del numero de articulos que coinciden en nombre nada mas
	$condiciones = "$tabla_proveedores.inactivo='' and $tabla_proveedores.tipo_servicio_id!='".$tipo_servicio_directo."'";
	//desgloso buscar_nombre si tiene espacios en blanco
	$trozos = array();
	$trozos = explode(" ",$buscar_nombre);
	foreach ($trozos as $valor)
	{
		if ($condiciones != "") { $condiciones .= " and "; }
		$condiciones .= "($tabla_articulos.nombre like '%$valor%' or $tabla_articulos.referencia like '%$valor%')";
	}
	$total_articulos = 0;
	$cons = "select count(distinct $tabla_articulos.id) as total from $tabla_articulos 
join $tabla_prov_familias on $tabla_prov_familias.familia_id=$tabla_articulos.familia_id 
join $tabla_proveedores on $tabla_proveedores.id=$tabla_prov_familias.proveedor_id 
where $condiciones;";
	//echo "$cons<br>";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$total_articulos = $lin['total'];
	}
	echo "<br>Existen <b>$total_articulos</b> articulos que coincidan con los datos buscados.";
	if ($total_articulos > 0)
	{
		echo "<a href=\"javascript:expandir('info_articulos')\" style='text-decoration:none; color: #000000; font-weight: bold;'>[+]</a><br>";
		echo "
	<span class='infohidden' id='info_articulos' name='info_articulos'>
	<table>
		<tr align='center' bgcolor='#$color_fondo'>
			<td><font color='#ffffff'><b>Referencia</b></font></td>
			<td><font color='#ffffff'><b>Nombre</b></font></td>
		</tr>";
		$cons = "select distinct $tabla_articulos.id, $tabla_articulos.nombre, $tabla_articulos.referencia from $tabla_articulos 
join $tabla_prov_familias on $tabla_prov_familias.familia_id=$tabla_articulos.familia_id 
join $tabla_proveedores on $tabla_proveedores.id=$tabla_prov_familias.proveedor_id 
where $condiciones 
order by $tabla_articulos.nombre;";
		//echo "$cons<br>";
		$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
		while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
		{
			echo "
		<tr>
			<td>$lin[referencia]</td>
			<td>$lin[nombre]</td>
		</tr>";
		}
		echo "
	</table>
	</span>";
	}
	// fin aviso del numero de articulos que coinciden en nombre nada mas
	// inicio aviso del numero de articulos que coinciden en nombre pero de proveedores bloqueados al cliente
	$condiciones = "$tabla_cli_prov.cliente_id='".$_SESSION['presupuestoCliente']."' and $tabla_cli_prov.proveedor_id=$tabla_proveedores.id and $tabla_cli_prov.bloqueado='on' and $tabla_proveedores.inactivo='' and $tabla_proveedores.tipo_servicio_id!='".$tipo_servicio_directo."' and $tabla_art_embalajes.inactivo='' and $tabla_art_precios.proveedor_id=$tabla_proveedores.id";
	//desgloso buscar_nombre si tiene espacios en blanco
	$trozos = array();
	$trozos = explode(" ",$buscar_nombre);
	foreach ($trozos as $valor)
	{
		if ($condiciones != "") { $condiciones .= " and "; }
		$condiciones .= "($tabla_articulos.nombre like '%$valor%' or $tabla_articulos.referencia like '%$valor%')";
	}
	$total_bloqueados = 0;
	$contenido_bloqueado = "";
	$consulta_bloqueado = "select $tabla_articulos.id, $tabla_articulos.nombre, $tabla_articulos.referencia, $tabla_articulos.familia_id, $tabla_cli_prov.proveedor_id, $tabla_cli_prov.dto_porc_pronto_pago, $tabla_prov_familias.igic_id 
from $tabla_articulos 
join $tabla_art_embalajes on $tabla_art_embalajes.articulo_id=$tabla_articulos.id 
join $tabla_art_precios on $tabla_art_precios.articulo_id=$tabla_articulos.id 
join $tabla_prov_familias on $tabla_prov_familias.familia_id=$tabla_articulos.familia_id 
join $tabla_proveedores on $tabla_proveedores.id=$tabla_prov_familias.proveedor_id 
join $tabla_cli_prov_fam on $tabla_cli_prov_fam.familia_id=$tabla_articulos.familia_id 
join $tabla_cli_prov on $tabla_cli_prov.id=$tabla_cli_prov_fam.cliente_prov_id 
where $condiciones and $tabla_prov_familias.igic_id>0 
group by $tabla_articulos.id, $tabla_cli_prov.proveedor_id 
order by $tabla_articulos.nombre;";
	//echo "$consulta_bloqueado<br>";
	$res_bloqueado = mysql_query($consulta_bloqueado) or die("$consulta_bloqueado, La consulta falló: $consulta_bloqueado" . mysql_error());
	while ($lin_bloqueado = mysql_fetch_array($res_bloqueado, MYSQL_ASSOC))
	{
		$total_bloqueados++;
		$nombre_proveedor = "";
		$consulta_cat = "select * from $tabla_proveedores where $tabla_proveedores.id='".$lin_bloqueado['proveedor_id']."';";
		$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
		while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
		{
			$nombre_proveedor = $linea_cat['nombre_corto'];
		}
		$contenido_bloqueado .= "
		<tr>
			<td>$lin_bloqueado[referencia]</td>
			<td>$lin_bloqueado[nombre]</td>
			<td>$nombre_proveedor</td>
		</tr>";
	}
	if ($total_bloqueados > 0)
	{
		echo "<br>Existen <b>$total_bloqueados</b> articulos que coincidan con los datos buscados pero que son de proveedores bloqueados. <a href=\"javascript:expandir('info_bloqueados')\" style='text-decoration:none; color: #000000; font-weight: bold;'>[+]</a><br>
	<span class='infohidden' id='info_bloqueados' name='info_bloqueados'>
	<table>
		<tr align='center' bgcolor='#$color_fondo'>
			<td><font color='#ffffff'><b>Referencia</b></font></td>
			<td><font color='#ffffff'><b>Nombre</b></font></td>
			<td><font color='#ffffff'><b>Prov.</b></font></td>
		</tr>".$contenido_bloqueado."
	</table>
	</span>";
	}
	// fin aviso del numero de articulos que coinciden en nombre pero de proveedores bloqueados al cliente
}
echo "
<table width='100%' bgcolor='#$color_fondo_tabla'>";
echo "
	<tr align='center' bgcolor='#$color_fondo'>";
foreach ($nombres_bloque_1 as $nombre_colum)
{
	echo "
		<td><font color='#ffffff'><b>$nombre_colum</b></font></td>";
}
echo "
	</tr>";

//si no activa busqueda no muestra articulos
if ($buscar_nombre != "")
{
	$condiciones = "$tabla_cli_prov.cliente_id='".$_SESSION['presupuestoCliente']."' and $tabla_cli_prov.proveedor_id=$tabla_proveedores.id and $tabla_cli_prov.bloqueado='' and $tabla_proveedores.inactivo='' and $tabla_proveedores.tipo_servicio_id!='".$tipo_servicio_directo."' and $tabla_art_embalajes.inactivo='' and $tabla_art_precios.proveedor_id=$tabla_proveedores.id";
	$parametros = "&buscar_nombre=$buscar_nombre";
	//desgloso buscar_nombre si tiene espacios en blanco
	$trozos = array();
	$trozos = explode(" ",$buscar_nombre);
	foreach ($trozos as $valor)
	{
		if ($condiciones != "") { $condiciones .= " and "; }
		$condiciones .= "($tabla_articulos.nombre like '%$valor%' or $tabla_articulos.referencia like '%$valor%')";
	}
	echo "
	<form name=\"bloque1\" method=\"post\" action=\"$enlacevolver"."$script\">
	<input type=\"hidden\" name=\"accion\" value=\"actualizabloque2\">
	<input type=\"hidden\" name=\"buscar_nombre\" value=\"$buscar_nombre\">";
	// se buscaran aquellos articulos que:
	// - cumpla lo buscado en el filtro (nombre o referencia)
	// - tengan algun embalaje definido y no este inactivo
	// - lo trabaje algun proveedor que trabaje con el cliente del presupuesto (familia definida en prov_familias_t y clientes_prov_familias_t)
	// - el proveedor no este inactivo, no sea de tipo servico "directo"
	// - el proveedor no este bloqueado para ese cliente
	// - que tenga el precio definido para el proveedor
	
	$array_cesta = array();
	$array_cesta = $_SESSION['presupuestoNuevo'];
	$cont = 1;
	$consulta_bloque1 = "select $tabla_articulos.id, $tabla_articulos.nombre, $tabla_articulos.referencia, $tabla_articulos.familia_id, $tabla_cli_prov.proveedor_id, $tabla_cli_prov.dto_porc_pronto_pago, $tabla_prov_familias.igic_id 
from $tabla_articulos 
join $tabla_art_embalajes on $tabla_art_embalajes.articulo_id=$tabla_articulos.id 
join $tabla_art_precios on $tabla_art_precios.articulo_id=$tabla_articulos.id 
join $tabla_prov_familias on $tabla_prov_familias.familia_id=$tabla_articulos.familia_id 
join $tabla_proveedores on $tabla_proveedores.id=$tabla_prov_familias.proveedor_id 
join $tabla_cli_prov_fam on $tabla_cli_prov_fam.familia_id=$tabla_articulos.familia_id 
join $tabla_cli_prov on $tabla_cli_prov.id=$tabla_cli_prov_fam.cliente_prov_id 
where $condiciones and $tabla_prov_familias.igic_id>0 
group by $tabla_articulos.id, $tabla_cli_prov.proveedor_id 
order by $tabla_articulos.nombre;";
	//echo "$consulta_bloque1<br>";
	$res_bloque1 = mysql_query($consulta_bloque1) or die("$consulta_bloque1, La consulta falló: $consulta_bloque1" . mysql_error());
	while ($lin_bloque1 = mysql_fetch_array($res_bloque1, MYSQL_ASSOC))
	{
		$ano = "0000"; $mes = "00"; $dia = "00";
		$color_fondo_articulo = "";
		$valor_existente_embalaje = "";
		$valor_existente_unidades = "";
		$valor_existente_dto2 = "";
		$valor_existente_dto3 = "";
		$valor_existente_comprometido = "";
		$indice_cesta = $lin_bloque1['id']."/".$lin_bloque1['proveedor_id'];
		if (array_key_exists($indice_cesta,$array_cesta))
		{
			// existe el articulo para este proveedor en la cesta
			$color_fondo_articulo = " bgcolor='#E8CD71'";
			$valor_existente_embalaje = $array_cesta[$indice_cesta]["embalaje"];
			$valor_existente_unidades = $array_cesta[$indice_cesta]["unidades"];
			$valor_existente_dto2 = $array_cesta[$indice_cesta]["dto2"];
			$valor_existente_dto3 = $array_cesta[$indice_cesta]["dto3"];
			list($ano,$mes,$dia) = explode("-",$array_cesta[$indice_cesta]["fecha_reserva"]);
			$valor_existente_comprometido = $array_cesta[$indice_cesta]["comprometido"];
		}
if (true)
{
		$nombre_proveedor = "";
		$consulta_cat = "select * from $tabla_proveedores where $tabla_proveedores.id='".$lin_bloque1['proveedor_id']."';";
		$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
		while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
		{
			$nombre_proveedor = $linea_cat['nombre_corto'];
		}
		$valor_igic = "";
		$consulta_cat = "select * from $tabla_m_igic where $tabla_m_igic.id='".$lin_bloque1['igic_id']."';";
		$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
		while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
		{
			$valor_igic = $linea_cat['valor'];
		}
		$valor_precio = "";
		$consulta_cat = "select * from $tabla_art_precios where $tabla_art_precios.articulo_id='".$lin_bloque1['id']."' and $tabla_art_precios.proveedor_id='".$lin_bloque1['proveedor_id']."';";
		$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
		while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
		{
			$valor_precio = $linea_cat['precio'];
		}
		
		// calculos para el precio de proveedor
		$precio_prov = $valor_precio;
		$dto_porc_prov_fam = "";
		$consulta1 = "select * from $tabla_prov_familias where proveedor_id='".$lin_bloque1['proveedor_id']."' and familia_id='".$lin_bloque1['familia_id']."';";
		//echo "$consulta1<br>";
		$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1" . mysql_error());
		while ($linea1 = mysql_fetch_array($resultado1, MYSQL_ASSOC))
		{
			$dto_porc_prov_fam = $linea1['dto_porc_prov_fam_deposito'];
		}
		if ($dto_porc_prov_fam > 0)
		{
			$precio_prov -= round(($precio_prov*$dto_porc_prov_fam)/100, 2);
		}
		// calculos para el precio de cliente
		$precio_cli = $valor_precio;
		$dto_porc_cli_fam = "";
		$dto_porc_cli_art = "";
		$precio_neto_cli_art = "";
		$consulta1 = "select $tabla_cli_prov_fam.* from $tabla_cli_prov_fam join $tabla_cli_prov on $tabla_cli_prov.id=$tabla_cli_prov_fam.cliente_prov_id where $tabla_cli_prov.cliente_id='".$_SESSION['presupuestoCliente']."' and $tabla_cli_prov.proveedor_id='".$lin_bloque1['proveedor_id']."' and $tabla_cli_prov_fam.familia_id='".$lin_bloque1['familia_id']."';";
		//echo "$consulta1<br>";
		$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1" . mysql_error());
		while ($linea1 = mysql_fetch_array($resultado1, MYSQL_ASSOC))
		{
			$dto_porc_cli_fam = $linea1['dto_porc_cli_fam_deposito'];
			// busqueda de los descuentos del articulo para el cliente
			$consulta2 = "select * from $tabla_cli_prov_fam_art where cliente_prov_familia_id='$linea1[id]' and articulo_id='".$lin_bloque1['id']."';";
			//echo "$consulta2<br>";
			$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: $consulta2" . mysql_error());
			while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC))
			{
				$dto_porc_cli_art = $linea2['dto_porc_cli_art_deposito'];
				$precio_neto_cli_art = $linea2['precio_neto_cli_art_deposito'];
			}
		}
		$precio_cli = $valor_precio;
		if ($precio_neto_cli_art > 0)
		{
			$precio_cli = $precio_neto_cli_art;
		}
		elseif ($dto_porc_cli_art > 0)
		{
			$precio_cli -= round(($precio_cli*$dto_porc_cli_art)/100, 2);
		}
		elseif ($dto_porc_cli_fam > 0)
		{
			$precio_cli -= round(($precio_cli*$dto_porc_cli_fam)/100, 2);
		}
		
		
		// calculos para el precio por ofertas
		$precio_oferta = $valor_precio;
		$precio_neto_oferta_art = "";
		$dto_porc_oferta_art = "";
		$min_unidades_dto = "";
		$precio_unidad_dto = "";
		$min_unidades_gratis = "";
		$num_unidades_gratis = "";
		$dto_porc_oferta_fam = "";
		$texto_oferta_reduccion_por_cantidad = "";
		$texto_oferta_regalo_por_compra = "";
		$valor_prov_oferta_id = 0;
		$valor_fecha = date("Y-m-d");
		// busqueda de ofertas:
		// - de este proveedor, que no esta inactivo
		// - de este cliente, que no este bloqueado
		// - que la fecha de hoy este entre sus fechas
		// - que se aplica en deposito
//join $tabla_prov_oferta_fam on $tabla_prov_oferta_fam.prov_oferta_id=$tabla_prov_ofertas.id 
		$consulta1 = "select $tabla_prov_ofertas.id from $tabla_prov_ofertas 
join $tabla_proveedores on $tabla_proveedores.id=$tabla_prov_ofertas.proveedor_id 
join $tabla_prov_ofertas_cli on $tabla_prov_ofertas_cli.prov_oferta_id=$tabla_prov_ofertas.id 
join $tabla_cli_prov on $tabla_cli_prov.proveedor_id=$tabla_proveedores.id 
join $tabla_prov_oferta_fam on $tabla_prov_oferta_fam.prov_oferta_id=$tabla_prov_ofertas.id 
where $tabla_prov_ofertas.proveedor_id='".$lin_bloque1['proveedor_id']."' and $tabla_prov_ofertas.fecha_inicio<='".$valor_fecha."' and $tabla_prov_ofertas.fecha_fin>='".$valor_fecha."' and $tabla_proveedores.inactivo='' and $tabla_prov_ofertas_cli.cliente_id='".$_SESSION['presupuestoCliente']."' and $tabla_cli_prov.cliente_id='".$_SESSION['presupuestoCliente']."' and $tabla_cli_prov.proveedor_id='".$lin_bloque1['proveedor_id']."' and $tabla_cli_prov.bloqueado='' and $tabla_prov_ofertas.aplica_deposito='on' and $tabla_prov_oferta_fam.familia_id='".$lin_bloque1['familia_id']."' order by $tabla_prov_ofertas.fecha_fin asc limit 1;";
// and $tabla_prov_oferta_fam.familia_id='".$familia_art_id."'
		//echo "$consulta1<br>";
		$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1 " . mysql_error());
		while ($linea1 = mysql_fetch_array($resultado1, MYSQL_ASSOC))
		{
			$valor_prov_oferta_id = $linea1['id'];
		}
		if ($valor_prov_oferta_id > 0)
		{
			$consulta1 = "select * from $tabla_prov_oferta_fam where prov_oferta_id='".$valor_prov_oferta_id."' and familia_id='".$lin_bloque1['familia_id']."';";
			//echo "$consulta1<br>";
			$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1 " . mysql_error());
			while ($linea1 = mysql_fetch_array($resultado1, MYSQL_ASSOC))
			{
				if ($linea1['dto_porc_oferta_fam'] > 0)
				{
					$dto_porc_oferta_fam = $linea1['dto_porc_oferta_fam'];
				}
				$consulta2 = "select * from $tabla_prov_oferta_fam_art where prov_oferta_familia_id='".$linea1['id']."' and articulo_id='".$lin_bloque1['id']."';";
				//echo "$consulta2<br>";
				$resultado2 = mysql_query($consulta2) or die("La consulta fall&oacute;: $consulta2 " . mysql_error());
				while ($linea2 = mysql_fetch_array($resultado2, MYSQL_ASSOC))
				{
					if ($linea2['precio_neto_oferta_art'] > 0)
					{
						$precio_neto_oferta_art = $linea2['precio_neto_oferta_art'];
						$precio_oferta = $linea2['precio_neto_oferta_art'];
					}
					elseif ($linea2['dto_porc_oferta_art'] > 0)
					{
						$dto_porc_oferta_art = $linea2['dto_porc_oferta_art'];
						$dto_oferta = round(($valor_precio*$linea2['dto_porc_oferta_art'])/100, 2);
						$precio_oferta = $valor_precio-$dto_oferta;
					}
					elseif ($linea1['dto_porc_oferta_fam'] > 0)
					{
						$dto_porc_oferta_fam = $linea1['dto_porc_oferta_fam'];
						$dto_oferta = round(($valor_precio*$linea1['dto_porc_oferta_fam'])/100, 2);
						$precio_oferta = $valor_precio-$dto_oferta;
					}
//jgs => caso especial
					if ($linea2['min_unidades_dto'] > 0 && $linea2['precio_unidad_dto'] > 0)
					{
						$texto_oferta_reduccion_por_cantidad = "Comprando ".$linea2['min_unidades_dto']." unidades, la unidad sale a ".number_format($linea2['precio_unidad_dto'],2,",",".")." &euro;.";
						$min_unidades_dto = $linea2['min_unidades_dto'];
						$precio_unidad_dto = $linea2['precio_unidad_dto'];
					}
					if ($linea2['min_unidades_gratis'] > 0 && $linea2['num_unidades_gratis'] > 0)
					{
						$min_unidades_gratis = $linea2['min_unidades_gratis'];
						$num_unidades_gratis = $linea2['num_unidades_gratis'];
					}
				}
				if ($precio_neto_oferta_art > 0)
				{
					$precio_oferta = $precio_neto_oferta_art;
				}
				elseif ($dto_porc_oferta_art > 0)
				{
					$dto_oferta = round(($valor_precio*$dto_porc_oferta_art)/100, 2);
					$precio_oferta = $valor_precio-$dto_oferta;
				}
				elseif ($dto_porc_oferta_fam > 0)
				{
					$dto_oferta = round(($valor_precio*$dto_porc_oferta_fam)/100, 2);
					$precio_oferta = $valor_precio-$dto_oferta;
				}
			}
			$consulta1 = "select * from $tabla_prov_ofertas where id='".$valor_prov_oferta_id."';";
			//echo "$consulta1<br>";
			$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1 " . mysql_error());
			while ($linea1 = mysql_fetch_array($resultado1, MYSQL_ASSOC))
			{
				if ($linea1['min_importe_regalo'] > 0 && $linea1['regalo'] != "")
				{
					$texto_oferta_regalo_por_compra = "Si compra ".number_format($linea1['min_importe_regalo'],2,",",".")." &euro; le regalamos ".$linea1['regalo'].".";
				}
			}
		}
		$valor_precio_inicial = 0;
		$valor_dto1 = 0; // para guardarlo en pre_articulos_t.dto_porc
		$valor_precio_neto = 0; // para guardarlo en pre_articulos_t.precio_neto
		if ($valor_prov_oferta_id > 0 && $precio_oferta < $valor_precio)
		{
			// hay un descuento por oferta
			if ($precio_neto_oferta_art > 0)
			{
				$valor_precio_neto = $precio_neto_oferta_art;
			}
			elseif ($dto_porc_oferta_art > 0)
			{
				$valor_dto1 = $dto_porc_oferta_art;
			}
			elseif ($dto_porc_oferta_fam > 0)
			{
				$valor_dto1 = $dto_porc_oferta_fam;
			}
			$valor_precio_inicial = $precio_oferta;
		}
		elseif ($precio_cli < $valor_precio)
		{
			// hay un descuento por cliente
			if ($precio_neto_cli_art > 0)
			{
				$valor_precio_neto = $precio_neto_cli_art;
			}
			elseif ($dto_porc_cli_art > 0)
			{
				$valor_dto1 = $dto_porc_cli_art;
			}
			elseif ($dto_porc_cli_fam > 0)
			{
				$valor_dto1 = $dto_porc_cli_fam;
			}
			$valor_precio_inicial = $precio_cli;
		}
		elseif ($precio_prov < $valor_precio)
		{
			// hay un descuento por proveedor
			$valor_dto1 = $dto_porc_prov_fam;
			$valor_precio_inicial = $precio_prov;
		}
		else
		{
			$valor_precio_inicial = $valor_precio;
		}
		
		// en codigo solo se puede calcular el precio final aplicando el pronto pago
		// con javascript se calculara el dto2 y dto3
		$valor_precio_final = $valor_precio_inicial;
		if ($lin_bloque1['dto_porc_pronto_pago'] > 0)
		{
			$dto_pronto_pago = round(($valor_precio_inicial*$lin_bloque1['dto_porc_pronto_pago'])/100, 2);
			$valor_precio_final = $valor_precio_inicial-$dto_pronto_pago;
		}
		
		// calculo del stock disponible (no contar las reservas)
		$cantidad_stock = 0;
		$cantidad_stock = CantidadStock($lin_bloque1['id'], $lin_bloque1['proveedor_id']);
			
		// calculo de las unidades ya pedidas en otros pedidos (aceptados, no finalizados, no entregados y sin marcar como no preparable)
		$cantidad_pedido = CantidadPedido($lin_bloque1['id'], $lin_bloque1['proveedor_id']);
} // fin true
		
		// nombres campos
		$nombre_embalaje = "embalaje_".$cont;
		$nombre_num_embalajes = "num_embalajes_".$cont;
		$nombre_ud_embalajes = "ud_embalajes_".$cont;
		$nombre_unidades = "unidades_".$cont;
		$nombre_txt_gratis = "txt_gratis_".$cont;
		$nombre_precio_inicial = "precio_inicial_".$cont;
		$nombre_precio_inicial_sin_oferta = "precio_inicial_sin_".$cont;
		$nombre_min_unidades_dto = "min_unidades_dto_".$cont;
		$nombre_precio_unidad_dto = "precio_unidad_dto_".$cont;
		$nombre_min_unidades_gratis = "min_unidades_gratis_".$cont;
		$nombre_num_unidades_gratis = "num_unidades_gratis_".$cont;
		$nombre_unidades_gratis = "unidades_gratis_".$cont;
		$nombre_precio_final = "precio_final_".$cont;
		$nombre_precio_neto = "precio_neto_".$cont;
		$nombre_dto1 = "dto1_".$cont; // definido en la oferta, en el cliente o en el proveedor, puede ser por familia o por articulo
		$nombre_dto1_sin_oferta = "dto1_sin_".$cont;
		$nombre_dto2 = "dto2_".$cont;
		$nombre_dto3 = "dto3_".$cont;
		$nombre_pronto_pago = "pronto_pago_".$cont;
		$nombre_fecha_reserva = "fecha_reserva_".$cont;
		$nombre_comprometido = "comprometido_".$cont;
		echo "
	<tr valign='top' $color_fondo_articulo>
		<input type=\"hidden\" name=\"referencia_"."$cont\" value=\"".$lin_bloque1['id']."\">
		<input type=\"hidden\" name=\"proveedor_id_"."$cont\" value=\"".$lin_bloque1['proveedor_id']."\">
		<input type=\"hidden\" name=\"igic_id_"."$cont\" value=\"".$lin_bloque1['igic_id']."\">
		<input type=\"hidden\" name=\"precio_articulo_"."$cont\" value=\"".$valor_precio."\">
		<td>(".$lin_bloque1['referencia'].")</td>
		<td><img src=\"images/help.png\" onClick=\"abrirStock(".$lin_bloque1['id'].", ".$lin_bloque1['proveedor_id'].");\" onLoad=\"cambiarUnidades(".$cont.")\" />&nbsp;".$lin_bloque1['nombre']."</td>
		<td><select name=\"".$nombre_embalaje."\" onChange=\"cambiarEmbalaje(this)\">";
		$valor_inicial = "";
		$texto_opciones_select = "";
		$consulta_cat = "select $tabla_art_embalajes.id, $tabla_art_embalajes.unidades, $tabla_m_embalajes.nombre from $tabla_art_embalajes
join $tabla_m_embalajes on $tabla_m_embalajes.id=$tabla_art_embalajes.tipo_embalaje_id 
where $tabla_art_embalajes.articulo_id='".$lin_bloque1['id']."' and $tabla_art_embalajes.inactivo=''
order by $tabla_art_embalajes.defecto_presupuesto desc, $tabla_art_embalajes.tipo_embalaje_id, $tabla_art_embalajes.unidades;";
		$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
		while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
		{
			if ($valor_existente_embalaje == "")
			{
				if ($valor_inicial == "") { $valor_inicial = $linea_cat['unidades']; }
			}
			else
			{
				if ($valor_existente_embalaje == $linea_cat['id']) { $valor_inicial = $linea_cat['unidades']; }
			}
			echo "<option value='$linea_cat[id]'"; if ($valor_existente_embalaje != "" && $valor_existente_embalaje == $linea_cat['id']) { echo " selected"; }
			echo ">$linea_cat[nombre] de $linea_cat[unidades]</option>";
			$texto_opciones_select .= "
			<input type=\"hidden\" name=\"".$nombre_ud_embalajes."_".$linea_cat['id']."\" id=\"".$nombre_ud_embalajes."_".$linea_cat['id']."\" value=\"".$linea_cat['unidades']."\" >";
		}
		echo "</select>".$texto_opciones_select."
			<input type=\"hidden\" name=\"".$nombre_ud_embalajes."\" id=\"".$nombre_ud_embalajes."\" size=\"4\" value=\"".$valor_inicial."\" ></td>
		<td><input type=\"text\" name=\"".$nombre_num_embalajes."\" id=\"".$nombre_num_embalajes."\" size=\"4\" value=\"\" onkeyup='cambiarUdEmbalajes(this);'></td>
		<td><input type=\"text\" name=\"".$nombre_unidades."\" id=\"".$nombre_unidades."\" size=\"4\" value=\"".$valor_existente_unidades."\" onkeyup='cambiarUnidades(".$cont.");'>
			<span id=\"".$nombre_txt_gratis."\"></span>
		</td>
		<td>$cantidad_stock</td>
		<td>$cantidad_pedido</td>
		<td>$nombre_proveedor</td>
		<td><input type=\"text\" readonly=\"readonly\" name=\"".$nombre_precio_inicial."\" id=\"".$nombre_precio_inicial."\" size=\"4\" value=\"".number_format($valor_precio_inicial, 2)."\"> &euro;</b>
			<input type=\"hidden\" name=\"".$nombre_precio_inicial_sin_oferta."\" id=\"".$nombre_precio_inicial_sin_oferta."\" value=\"".number_format($valor_precio_inicial, 2)."\">
			<input type=\"hidden\" name=\"".$nombre_min_unidades_dto."\" id=\"".$nombre_min_unidades_dto."\" value=\"".$min_unidades_dto."\">
			<input type=\"hidden\" name=\"".$nombre_precio_unidad_dto."\" id=\"".$nombre_precio_unidad_dto."\" value=\"".number_format($precio_unidad_dto, 2)."\">
			<input type=\"hidden\" name=\"".$nombre_min_unidades_gratis."\" id=\"".$nombre_min_unidades_gratis."\" value=\"".$min_unidades_gratis."\">
			<input type=\"hidden\" name=\"".$nombre_num_unidades_gratis."\" id=\"".$nombre_num_unidades_gratis."\" value=\"".$num_unidades_gratis."\">
			<input type=\"hidden\" readonly=\"readonly\" name=\"".$nombre_unidades_gratis."\" id=\"".$nombre_unidades_gratis."\" value=\"\">
		</td>
		<td><input type=\"text\" readonly=\"readonly\" name=\"".$nombre_dto1."\" id=\"".$nombre_dto1."\" size=\"4\" value=\"".number_format($valor_dto1, 2)."\"> %
			<input type=\"hidden\" readonly=\"readonly\" name=\"".$nombre_dto1_sin_oferta."\" id=\"".$nombre_dto1_sin_oferta."\" value=\"".number_format($valor_dto1, 2)."\">
			<input type=\"hidden\" readonly=\"readonly\" name=\"".$nombre_precio_neto."\" id=\"".$nombre_precio_neto."\" value=\"".$valor_precio_neto."\"></td>
		<td><input type=\"text\" name=\"".$nombre_dto2."\" id=\"".$nombre_dto2."\" size=\"4\" value=\"".$valor_existente_dto2."\" onkeyup='cambiarDto(this);'> *</td>
		<td><input type=\"text\" name=\"".$nombre_dto3."\" id=\"".$nombre_dto3."\" size=\"4\" value=\"".$valor_existente_dto3."\" onkeyup='cambiarDto(this);'> *</td>
		<td><input type=\"text\" readonly=\"readonly\" name=\"".$nombre_pronto_pago."\" id=\"".$nombre_pronto_pago."\" size=\"4\" value=\"".number_format($lin_bloque1['dto_porc_pronto_pago'], 2)."\"> %</td>
		<td><input type=\"text\" readonly=\"readonly\" name=\"".$nombre_precio_final."\" id=\"".$nombre_precio_final."\" size=\"4\" value=\"".number_format($valor_precio_final, 2)."\"> &euro;</td>
		<td>";
		if ($lin_bloque1['igic_id'] > 0) { echo number_format($valor_igic, 2, ",", ".")." %"; }
		else { echo "No definido"; }
		echo "</td>";
		echo "<td><input type='text' name='$nombre_fecha_reserva' id='$nombre_fecha_reserva' size='10' value='$dia/$mes/$ano'/ readonly='readonly' >";
	echo "
<img src='images/calendar.gif' name='boton"."$nombre_fecha_reserva' border='0' id='boton"."$nombre_fecha_reserva' style='cursor: pointer; border: 0px solid blue;' title='Seleccione fecha' />
<script type='text/javascript'>
	Calendar.setup({
		inputField	:	'$nombre_fecha_reserva',		// id of the input field
		trigger		:	'boton"."$nombre_fecha_reserva',	// trigger for the calendar (button ID)
		onSelect	:	function() { this.hide() },
		min		:	".date("Ymd")."
	});
</script>";
		echo "</td>
		<td><input type=\"text\" name=\"".$nombre_comprometido."\" size=\"4\" value=\"".$valor_existente_comprometido."\" onkeyup='validar(".$nombre_comprometido.");'></td>
	</tr>";
		$cont ++;
	}
	echo "
	<tr>
		<td colspan='".count($nombres_bloque_1)."' align=\"center\">* Formato 1234.56</td>
	</tr>
	<tr>
		<td colspan='".count($nombres_bloque_1)."' align=\"center\">
		<input class='buttonmario smallmario green' type=submit name='Actualizar' value='$texto_boton_bloque_1'></td>
		<input type=\"hidden\" name=\"cont\" value=\"$cont\">
	</tr>
	</form>";
} // fin if buscar_nombre
echo "</table>";

if (count($_SESSION['presupuestoNuevo']) > 0)
{
	
//***************************** BLOQUE 2: LISTADO ARTICULOS
	echo "<br><br><b>$titulo_listado &nbsp;</b>&nbsp;&nbsp;&nbsp;
<a href=\"$enlacevolver"."$script&accion=borrartodo&buscar_nombre=$buscar_nombre\" class='buttonmario smallmario green'>$texto_boton_limpiar</a><br><br>";
	
	echo "
<table width='100%' bgcolor='#$color_fondo_tabla'>
	<tr align=\"center\" bgcolor=\"#$color_fondo\">";
	foreach ($nombres_bloque_2 as $nombre_colum)
	{
		echo "
		<td><font color='#ffffff'><b>$nombre_colum</b></font></td>";
	}
	echo "
		<td><font color='#ffffff'><b>Acciones</b></font></td>
	</tr>";
	
	$cont1 = 1;
	//leemos array con lo seleccionado y lo volcamos en la tabla
	$array_cesta = array();
	$array_cesta = $_SESSION['presupuestoNuevo'];
	$importe_art_total = 0;
	$importe_igic_total = 0;
	
	list($dia, $mes, $ano) = explode('/', $_SESSION['presupuestoFecha']);
	$valor_fecha_calculo = "$ano-$mes-$dia";
	//echo print_r($array_cesta,true);
	$array_importes_proveedores = array();
	foreach ($array_cesta as $indi_cesta => $valor)
	{
		$consulta_art = "select * from $tabla_articulos where id='".$valor['referencia']."';";
		$resultado_art = mysql_query($consulta_art) or die("La consulta falló: $consulta_art" . mysql_error());
		while ($linea_art = mysql_fetch_array($resultado_art, MYSQL_ASSOC))
		{
			$unidades_pedidas = $valor['unidades'];
			$nombre_embalaje = "";
			$consulta_cat = "select $tabla_art_embalajes.id, $tabla_art_embalajes.unidades, $tabla_m_embalajes.nombre from $tabla_art_embalajes
join $tabla_m_embalajes on $tabla_m_embalajes.id=$tabla_art_embalajes.tipo_embalaje_id 
where $tabla_art_embalajes.id='".$valor['embalaje']."';";
			//echo "$consulta_cat<br>";
			$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
			{
				$nombre_embalaje = "$linea_cat[nombre] de $linea_cat[unidades]";
			}
			$nombre_proveedor = "";
			$consulta_cat = "select * from $tabla_proveedores where $tabla_proveedores.id='".$valor['proveedor_id']."';";
			$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
			{
				$nombre_proveedor = $linea_cat['nombre_corto'];
			}
			$valor_igic = "";
			$consulta_cat = "select * from $tabla_m_igic where $tabla_m_igic.id='".$valor['igic_id']."';";
			$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
			while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
			{
				$valor_igic = $linea_cat['valor'];
			}
			
			if ($valor["fecha_reserva"] != "0000-00-00")
			{
				$importe_art_parcial = 0;
				$importe_igic_parcial = 0;
			}
			else
			{
				//$importe_art_parcial = $precio_articulo*$unidades_pedidas;
				$importe_art_parcial = $valor["precio_unidad"]*$unidades_pedidas;
				$importe_igic_parcial = round(($importe_art_parcial*$valor_igic)/100, 2);
			}
			$array_importes_proveedores[$valor["proveedor_id"]]["importe_arti"] += $importe_art_parcial;
			$array_importes_proveedores[$valor["proveedor_id"]]["importe_igic"] += $importe_igic_parcial;
			
			$importe_art_total += $importe_art_parcial;
			$importe_igic_total += $importe_igic_parcial;
			
			// calculo del stock disponible (no contar las reservas)
			$cantidad_stock = CantidadStock($valor['referencia'], $valor['proveedor_id']);
			
			// calculo de las unidades ya pedidas en otros pedidos (aceptados, no finalizados, no entregados y sin marcar como no preparable)
			$cantidad_pedido = CantidadPedido($valor['referencia'], $valor['proveedor_id']);
			
			echo "
	<tr>
		<td>(".$linea_art['referencia'].")</td>
		<td>".$linea_art['nombre']."</td>
		<td>$nombre_embalaje</td>
		<td>$unidades_pedidas";
			if ($valor['unidades_gratis'] != "")
			{
				echo " + ".$valor['unidades_gratis'];
			}
			echo "</td>
		<td align='right'>$cantidad_stock</td>
		<td align='right'>$cantidad_pedido</td>
		<td>$nombre_proveedor</td>
		<td align='right'>";
/*
			// si existe un precio de oferta => $array_precios[3] > 0
			// y es mayor que el precio de las condiciones de clientes, si hay uno definido => $array_precios[3] > $array_precios[1] && $array_precios[1] > 0
			// o es mayor que el precio de las condiciones de proveedores, si hay uno definido => $array_precios[3] > $array_precios[2] && $array_precios[2] > 0
			// se pone la campana
			if ($array_precios[3] > 0 && (($array_precios[3] > $array_precios[1] && $array_precios[1] > 0) || ($array_precios[3] > $array_precios[2] && $array_precios[2] > 0)))
			{
				//echo "<img src='images/bell.png' alt='Precio oferta superior' title='Precio oferta superior' border='0' /> ";
			}
			echo number_format($precio_articulo,2,",",".");
*/
			echo number_format($valor["precio_unidad"],2,",",".");
			echo " &euro;</td>
		<td align='right'>";
			//if ($dto_articulo > 0) { echo number_format($dto_articulo,2,",",".")." %"; }
			if ($valor["dto1"] > 0) { echo number_format($valor["dto1"],2,",",".")." %"; }
			echo "</td>
		<td align='right'>";
			if ($valor['dto2'] > 0) { echo number_format($valor['dto2'],2,",",".")." %"; }
			echo "</td>
		<td align='right'>";
			if ($valor['dto3'] > 0) { echo number_format($valor['dto3'],2,",",".")." %"; }
			echo "</td>
		<td align='right'>";
			if ($valor['pronto_pago'] > 0) { echo number_format($valor['pronto_pago'],2,",",".")." %"; }
			echo "</td>
		<td align='right'>".number_format($valor_igic,2,",",".")." %</td>
		<td align='right'><b>".number_format($importe_art_parcial,2,",",".")."</b> &euro;</td>
		<td align='right'><b>".number_format($importe_igic_parcial,2,",",".")."</b> &euro;</td>
		<td align='center'>";
			if ($valor["fecha_reserva"] != "0000-00-00")
			{
				echo date("d-m-Y",strtotime($valor["fecha_reserva"]));
			}
			else { echo "00-00-0000"; }
			echo "</td>
		<td>".$valor["comprometido"]."</td>
		<td><img src=\"images/help.png\" onClick=\"abrirStock(".$valor['referencia'].", ".$valor['proveedor_id'].");\" />&nbsp;&nbsp;<a href=\"$enlacevolver"."$script&accion=eliminarlinea&referencia=".$indi_cesta."\" class='buttonmario smallmario red' style=\"color:#ffffff;\">eliminar</a></td>";
/*
			echo "
		<form name='bloque2_$cont1' method=\"post\" action=\"$enlacevolver"."$script\">
		<input type=hidden name=\"accion\" value=\"eliminarlinea\">
		<input type=hidden name=\"buscar_nombre\" value=\"$buscar_nombre\">
		<input type=hidden name=\"referencia\" value=\"".$indi_cesta."\">
		<td><input type=submit class='buttonmario smallmario red' name=eliminar value=eliminar></td>";
*/
			echo "
		</form>
	</tr>";
		}
	}
	echo "
</table>";
	
	// calculo de los portes
	// no contabilizar las reservas
	$mostrar_portes = 0;
	$array_portes = array();
	$texto_error_ag_transporte = "";
	$consulta_cat = "select $tabla_clientes_direcciones.municipio_id from $tabla_clientes_direcciones where $tabla_clientes_direcciones.id='".$_SESSION['presupuestoDireccion']."';";
	$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
	while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
	{
		$cons1 = "select * from maestro_municipios_t where id='".$linea_cat['municipio_id']."';";
		$res1 = mysql_query($cons1) or die("La consulta fall&oacute;: $cons1 " . mysql_error());
		while ($lin1 = mysql_fetch_array($res1, MYSQL_ASSOC))
		{
			if ($lin1['isla_id'] != $identificador_gran_canaria)
			{
				// hay que calcular portes
				$mostrar_portes = 1;
				foreach ($array_importes_proveedores as $valor_prov => $array_datos)
				{
					// busqueda del minimo que hay que superar para que no pague el cliente (primero el definido en clientes_prov_t, despues en proveedores_t)
					//$porc_portes = 0; => ya no se va a calcular
					$importe_min_portes_pagados = 0;
					// primero busco las condiciones del proveedor, si existen condiciones particulares para el cliente se machacaran los valores
					$cons_porte = "select * from $tabla_proveedores where id='".$valor_prov."';";
					$res_porte = mysql_query($cons_porte) or die("La consulta fall&oacute;: $cons_porte " . mysql_error());
					while ($lin_porte = mysql_fetch_array($res_porte, MYSQL_ASSOC))
					{
						//$porc_portes = $lin_porte['prov_porc_portes']; => ya no se va a calcular
						$importe_min_portes_pagados = $lin_porte['prov_importe_min_portes_pagados'];
						$array_portes[$valor_prov]["nombre"] = $lin_porte['nombre_corto'];
					}
					$cons_porte = "select * from $tabla_cli_prov where cliente_id='".$_SESSION['presupuestoCliente']."' and proveedor_id='".$valor_prov."';";
					$res_porte = mysql_query($cons_porte) or die("La consulta fall&oacute;: $cons_porte " . mysql_error());
					while ($lin_porte = mysql_fetch_array($res_porte, MYSQL_ASSOC))
					{
						if ($lin_porte['importe_min_portes_pagados'] > 0)//$lin_porte['porc_portes'] > 0 && 
						{
							//$porc_portes = $lin_porte['porc_portes']; => ya no se va a calcular
							$importe_min_portes_pagados = $lin_porte['importe_min_portes_pagados'];
						}
					}
					// contabilizo el importe sin igic
					$importe_total_prov = $array_datos["importe_arti"];//$array_datos["importe_igic"]
					$array_portes[$valor_prov]["importe_minimo"] = $importe_min_portes_pagados;
					//$array_portes[$valor_prov]["porc_portes"] = $porc_portes; => ya no se va a calcular
					$array_portes[$valor_prov]["importe_art"] = $importe_total_prov;
					if ($importe_total_prov <= $importe_min_portes_pagados)
					{
						// no supera el minimo para que sea el proveedor quien pague los portes
						//$importe_portes = round(($importe_total_prov*$porc_portes)/100, 2); => ya no se va a calcular
						//$array_portes[$valor_prov]["importe_portes"] = $importe_portes; => ya no se va a calcular
						// hay que buscar la agencia del cliente
						$ag_tansp = 0;
						$nombre_ag_transp = "";
						$cons_transporte = "select $tabla_cli_prov.agencia_transporte_id, $tabla_m_ag_transportes.nombre from $tabla_cli_prov join $tabla_m_ag_transportes on $tabla_m_ag_transportes.id=$tabla_cli_prov.agencia_transporte_id where $tabla_cli_prov.cliente_id='".$_SESSION['presupuestoCliente']."' and $tabla_cli_prov.proveedor_id='".$valor_prov."';";
						//echo "$cons_transporte<br>";
						$res_transporte = mysql_query($cons_transporte) or die("La consulta fall&oacute;: $cons_transporte " . mysql_error());
						while ($lin_transporte = mysql_fetch_array($res_transporte, MYSQL_ASSOC))
						{
							$ag_tansp = $lin_transporte['agencia_transporte_id'];
							$nombre_ag_transp = $lin_transporte['nombre'];
						}
						if ($ag_tansp == 0)
						{
							// el cliente no tiene definido la agencia de transporte para este proveedor
							$nombre_proveedor = "";
							$consulta_cat = "select * from $tabla_proveedores where $tabla_proveedores.id='".$valor_prov."';";
							$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
							while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
							{
								$nombre_proveedor = $linea_cat['nombre_corto'];
							}
							if ($texto_error_ag_transporte != "") { $texto_error_ag_transporte .= "<br>"; }
							$texto_error_ag_transporte .= "El cliente no tiene definida una agencia de transporte para el proveedor $nombre_proveedor.";
							$array_portes[$valor_prov]["ag_tansp"] = "Sin definir";
						}
						else
						{
							$array_importes_proveedores[$valor_prov]["agencia_transporte_id"] = $ag_tansp;
							$array_portes[$valor_prov]["ag_tansp"] = $nombre_ag_transp;
						}
						// comprobacion de que el proveedor tiene definida la agencia de transporte
						// solo impedira la creacion del presupuesto pero se calcularan los portes
						$ag_tansp_prov = 0;
						$cons_transporte = "select prov_agencias_transportes_t.agencia_transporte_id, $tabla_m_ag_transportes.nombre from prov_agencias_transportes_t join $tabla_m_ag_transportes on $tabla_m_ag_transportes.id=prov_agencias_transportes_t.agencia_transporte_id where prov_agencias_transportes_t.proveedor_id='".$valor_prov."' order by prov_agencias_transportes_t.id limit 1;";
						//echo "$cons_transporte<br>";
						$res_transporte = mysql_query($cons_transporte) or die("La consulta fall&oacute;: $cons_transporte " . mysql_error());
						while ($lin_transporte = mysql_fetch_array($res_transporte, MYSQL_ASSOC))
						{
							$ag_tansp_prov = $lin_transporte['agencia_transporte_id'];
						}
						if ($ag_tansp_prov == 0)
						{
							// el proveedor no tiene definido ninguna agencia de transporte
							$nombre_proveedor = "";
							$consulta_cat = "select * from $tabla_proveedores where $tabla_proveedores.id='".$valor_prov."';";
							$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
							while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
							{
								$nombre_proveedor = $linea_cat['nombre_corto'];
							}
							if ($texto_error_ag_transporte != "") { $texto_error_ag_transporte .= "<br>"; }
							$texto_error_ag_transporte .= "El proveedor $nombre_proveedor no tiene definida una agencia de transporte.";
						}
					}
					else
					{
						// hay que buscar la agencia del proveedor
						//$array_portes[$valor_prov]["importe_portes"] = 0; => ya no se va a calcular
						$ag_tansp = 0;
						$nombre_ag_transp = "";
						$cons_transporte = "select prov_agencias_transportes_t.agencia_transporte_id, $tabla_m_ag_transportes.nombre from prov_agencias_transportes_t join $tabla_m_ag_transportes on $tabla_m_ag_transportes.id=prov_agencias_transportes_t.agencia_transporte_id where prov_agencias_transportes_t.proveedor_id='".$valor_prov."' order by prov_agencias_transportes_t.id limit 1;";
						//echo "$cons_transporte<br>";
						$res_transporte = mysql_query($cons_transporte) or die("La consulta fall&oacute;: $cons_transporte " . mysql_error());
						while ($lin_transporte = mysql_fetch_array($res_transporte, MYSQL_ASSOC))
						{
							$ag_tansp = $lin_transporte['agencia_transporte_id'];
							$nombre_ag_transp = $lin_transporte['nombre'];
						}
						if ($ag_tansp == 0)
						{
							// el proveedor no tiene definido ninguna agencia de transporte
							$nombre_proveedor = "";
							$consulta_cat = "select * from $tabla_proveedores where $tabla_proveedores.id='".$valor_prov."';";
							$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
							while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
							{
								$nombre_proveedor = $linea_cat['nombre_corto'];
							}
							if ($texto_error_ag_transporte != "") { $texto_error_ag_transporte .= "<br>"; }
							$texto_error_ag_transporte .= "El proveedor $nombre_proveedor no tiene definida una agencia de transporte.";
							$array_portes[$valor_prov]["ag_tansp"] = "Sin definir";
						}
						else
						{
							$array_importes_proveedores[$valor_prov]["agencia_transporte_id"] = $ag_tansp;
							$array_portes[$valor_prov]["ag_tansp"] = $nombre_ag_transp;
						}
						// comprobacion de que el cliente tiene definida la agencia de transporte
						// solo impedira la creacion del presupuesto pero se calcularan los portes
						$ag_tansp_cli = 0;
						$cons_transporte = "select $tabla_cli_prov.agencia_transporte_id, $tabla_m_ag_transportes.nombre from $tabla_cli_prov join $tabla_m_ag_transportes on $tabla_m_ag_transportes.id=$tabla_cli_prov.agencia_transporte_id where $tabla_cli_prov.cliente_id='".$_SESSION['presupuestoCliente']."' and $tabla_cli_prov.proveedor_id='".$valor_prov."';";
						//echo "$cons_transporte<br>";
						$res_transporte = mysql_query($cons_transporte) or die("La consulta fall&oacute;: $cons_transporte " . mysql_error());
						while ($lin_transporte = mysql_fetch_array($res_transporte, MYSQL_ASSOC))
						{
							$ag_tansp_cli = $lin_transporte['agencia_transporte_id'];
						}
						if ($ag_tansp_cli == 0)
						{
							// el cliente no tiene definido la agencia de transporte para este proveedor
							$nombre_proveedor = "";
							$consulta_cat = "select * from $tabla_proveedores where $tabla_proveedores.id='".$valor_prov."';";
							$resultado_cat = mysql_query($consulta_cat) or die("La consulta fall&oacute;: " . mysql_error());
							while ($linea_cat = mysql_fetch_array($resultado_cat, MYSQL_ASSOC))
							{
								$nombre_proveedor = $linea_cat['nombre_corto'];
							}
							if ($texto_error_ag_transporte != "") { $texto_error_ag_transporte .= "<br>"; }
							$texto_error_ag_transporte .= "El cliente no tiene definida una agencia de transporte para el proveedor $nombre_proveedor.";
						}
					}
				}
			}
		}
	}
	//echo print_r($array_importes_proveedores,true)."<br>";
	echo "
	<br><br>
	<form name='bloque3' method=\"post\" action=\"$enlacevolver"."$script\">
	<input type=hidden name=\"accion\" value=\"registrar\">
	<table width='100%' bgcolor='#$color_fondo_tabla'>
		<tr align=\"center\" bgcolor=\"#$color_fondo\">
			<td><font color='#ffffff'><b>Importe base</b></font></td>
			<td><font color='#ffffff'><b>Importe IGIC</b></font></td>
			<td><font color='#ffffff'><b>Importe total</b></font></td>
		</tr>
		<tr>
			<td align='right'>".number_format($importe_art_total,2,",",".")." &euro;</td>
			<td align='right'>".number_format($importe_igic_total,2,",",".")." &euro;</td>
			<td align='right'><b>".number_format(($importe_art_total+$importe_igic_total),2,",",".")." &euro;</b></td>
		</tr>
	</table>";
	if ($mostrar_portes == 1)
	{
//			<td><font color='#ffffff'><b>Porcentaje</b></font></td> => ya no se va a calcular
		echo "
	<table width='100%' bgcolor='#$color_fondo_tabla'>
		<tr align=\"center\" bgcolor=\"#$color_fondo\">
			<td colspan='5'><font color='#ffffff'><b>Importe portes</b></font></td>
		</tr>
		<tr align='right' bgcolor=\"#$color_fondo\">
			<td><font color='#ffffff'><b>Prov.</b></font></td>
			<td><font color='#ffffff'><b>Ag. Transp.</b></font></td>
			<td><font color='#ffffff'><b>Importe minimo</b></font></td>
			<td><font color='#ffffff'><b>Importe articulos</b></font></td>
			<td><font color='#ffffff'><b>Importe para minimo</b></font></td>
		</tr>";
//			<td><font color='#ffffff'><b>Portes</b></font></td> => ya no se va a calcular
		//$importe_portes_total = 0; => ya no se va a calcular
		foreach($array_portes as $valor_prov => $array_datos)
		{
			//$importe_portes_total += $array_datos["importe_portes"]; => ya no se va a calcular
			$importe_restante_para_min = $array_datos["importe_minimo"]-$array_datos["importe_art"];
//			<td>".number_format($array_datos["porc_portes"],2,",",".")." %</td> => ya no se va a calcular
			echo "
		<tr align='right'>
			<td>".$array_datos["nombre"]."</td>
			<td>".$array_datos["ag_tansp"]."</td>
			<td>".number_format($array_datos["importe_minimo"],2,",",".")." &euro;</td>
			<td bgcolor='#ffffff'>".number_format($array_datos["importe_art"],2,",",".")." &euro;</td>
			<td bgcolor='#ffffff'>";
			if ($importe_restante_para_min > 0)
			{
				echo number_format($importe_restante_para_min,2,",",".")." &euro;";
			}
			echo "</td>
		</tr>";
//			<td bgcolor='#ffffff'>".number_format($array_datos["importe_portes"],2,",",".")." &euro;</td> => ya no se va a calcular
		}
/*
		echo "
		<tr align='right'>
			<td colspan='5'><b>Total</b></td>
			<td>".number_format($importe_portes_total,2,",",".")." &euro;</td>
		</tr>";
*/
		echo "
	</table>";
	}
	echo "
	<table width='100%'>
		<tr align=\"center\" bgcolor=\"#$color_fondo\">
			<td><font color='#ffffff'><b>Observaciones albar&aacute;n</b></font></td>
			<td><font color='#ffffff'><b>Observaciones presupuesto</b></font></td>
			<td><font color='#ffffff'><b>Observaciones almac&eacute;n</b></font></td>
		</tr>
		<tr>
			<td><textarea rows='8' cols='42' name='obs_publico'></textarea></td>
			<td><textarea rows='8' cols='42' name='obs_privado'></textarea></td>
			<td><textarea rows='8' cols='42' name='obs_aviso'></textarea></td>
		</tr>
	</table>";
	if ($texto_error_ag_transporte != "")
	{
		echo $texto_error_ag_transporte;
	}
	else
	{
		echo "
	<input type=submit class='buttonmario mediummario orangemario' name='eliminar' value='$texto_boton_registro'>";
	}
	echo "
	</form>
";
}// fin if count articulos

} // fin de se tiene permiso para este script
else
{
	echo "No se tiene permiso para ver esta seccion.<br><br>";
}
// tabla de comienzo de pagina
	echo "
		</td>
	</tr>
</table>
";
?>