<?php 
//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
?>

<?php

   //include("obtener_miperfil.php");
   
   //include("obtener_opciones.php");

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "GESTION DE CONTENIDOS";
// Titulo que aparece en la pesta�a del navegador
$titulo_pagina = "GESTION DE CONTENIDOS";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";

echo '
        <section id="content">
          <section class="vbox">

            <header class="header bg-white b-b b-light">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="#"><i class="fa fa-home"></i> Inicio</a></li>
                <li class="active">Gesti&oacute;n de contenidos</li>
              </ul>
            </header>

            <section class="scrollable wrapper w-f">
              <p class="h4">'.$titulo.'</p>
';
            



// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "GESTION DE CONTENIDOS";
// Titulo que aparece en la pesta�a del navegador
$titulo_pagina = "GESTION DE CONTENIDOS";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";
// Texto que aparece en el boton de crear
$titulo_boton_crear = "CREAR NUEVO CONTENIDO";
// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.
// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 
$clase_boton_crear = " class='btn btn-success' ";
$clase_boton_buscar = " class='btn btn-mini btn-black' ";
$clase_boton_guardar = " class='btn btn-success' ";
$clase_boton_volver  = " class='btn btn-navi' ";
$clase_boton_confirmar_borrado  = " class='btn btn-danger' ";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 1;
$modulo_script = "gestproject";
// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=".$modulo_script."&file=";
// Nombre del script
$script = "index_contenidos_pressroom_new";
// Nombre de la tabla
$tabla = "nuke_stories"; // OJO, la clave principal se debe llamar id

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
$campos_col1 = array('pn_topic','pn_title','fecha_contenido','fecha_caducidad','orden','pn_hometext','pn_bodytext','pn_notes','nombre_fichero','nombre_fichero2','nombre_fichero3','nombre_fichero4','nombre_fichero5','categoria_id','nombre_fichero6','url_video','firma_id','titulo_audio','titulo_video','pn_topic2','pn_topic3','nombre_fichero7','nombre_fichero8','nombre_fichero9','nombre_fichero10');

// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]
$ayudas_col1 = array();

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('','','');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('','','','','','','','','','','','');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('','','','','','','','','','','','','');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php
$tipos_col1  = array('select;nuke_topics;nombre_topico;id;;(id = 19  || id = 22)','text;250','datetime3','datetime3','text;20','textarea;400;100;class="ckeditor"','textarea;400;100;class="ckeditor"','textarea;400;100;class="ckeditor"','file;8388608;descargar_imagen_contenido;tipo_fichero;peso_fichero;;descargar_imagen_contenido'
,'file;8388608;descargar_imagen_contenido2;tipo_fichero2;peso_fichero2;;descargar_imagen_contenido2'
,'file;8388608;descargar_imagen_contenido3;tipo_fichero3;peso_fichero3;;descargar_imagen_contenido3'
,'file;8388608;descargar_imagen_contenido4;tipo_fichero4;peso_fichero4;;descargar_imagen_contenido4'
,'file;8388608;descargar_imagen_contenido5;tipo_fichero5;peso_fichero5;;descargar_imagen_contenido5'
,'select;maestro_categorias_novedades_t;nombre;id;;id = 1'
,'file;8388608;descargar_audio;tipo_fichero6;peso_fichero6;;descargar_audio','text;600'
,'select;firmas_t;nombre;id','text;500','text;500','select;nuke_topics;nombre_topico;id;;(id = 19  || id = 22)','select;nuke_topics;nombre_topico;id;;(id = 19  || id = 22)','file;8388608;descargar_imagen_contenido7;tipo_fichero7;peso_fichero7;;descargar_imagen_contenido7','text;500','text;500','text;500');
//$tipos_col1  = array('datetime3','text;600','textarea;400;100','textarea;400;100');
//textarea;400;100;class="ckeditor

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_insert = " noc='$noc', ";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = "";

// Campo para la busqueda
$campo_busqueda = "pn_title";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript
$plantilla_insercion = "index_contenidos_pressroom_new.plantilla.php";
if ($plantilla_insercion != "") {
  $fichero_absoluto =  "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}

// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 1;
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','fecha_contenido','pn_title','pn_topic');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','Fecha contenido','T&iacute;tulo','T&oacute;pico');
// Decodificacion si existiese de los campos
$campos_listado_decod = array ('','si;datetime','','si;nuke_topics;nombre_topico;id');
// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-striped table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
$filtro_noc_para_listado = " and (pn_topic='22' || pn_topic2 = '22' || pn_topic3 = '22' || pn_topic='19' || pn_topic2 = '19' || pn_topic3 = '19') and id >='4884'";
// Para el paginado
$registros_por_pagina = "30";
// Estilo del paginado
$clase_paginado = "style='color:#000000; text-decoration:none;'";
// Formato paginado: reducido o no (Ej reducido: � [1] ...[3] [4] [5] ...[10] �)
$paginado_reducido = 1;

// Color de los registros
/*
$nombre_funcion_bgcolor_por_registro = "funcion_bgcolor_registro";
function funcion_bgcolor_registro($valor_id) {
   $color_naranja = "#EFB334";
   $color_gris_oscuro = "#C1BFBB";
   // Vemos el estado del recibo
   $tipo_resultado_ultima_gestion_id = obtener_campo('tipo_resultado','recibos_gestiones_t','left join recibos_t on recibos_gestiones_t.recibo_id=recibos_t.id','recibos_t.id='.$valor_id.' order by recibos_gestiones_t.fecha_alta desc limit 1');
   if ($tipo_resultado_ultima_gestion_id != "") { return  $color_naranja; }
   //echo "tipo_resultado: $tipo_resultado_ultima_gestion_id";
}
*/


// Filtros iniciales al listado de registros 
// Puedes definir una consulta inicial para el listado de registros, de forma que se apliquen filtros
// para que no se vean todos los registros que existen en la tabla en funcion de cualquier condicion que definas aqui.
// PONER LA VIA DE COBRO QUE CORRESPONDA:  and via_cobro=
//$filtros_iniciales = " and user_id='$user_id'";

// Otro Ejemplo
/*
if ($hoja_ruta == 1) {
	$filtros_iniciales = " and (empresa_servicio=1 or empresa_servicio=2 or empresa_servicio=6) and (via_cobro=1) and recibos_gestiones_t.user_destino_id='$user_id' ";
	$left_join_inicial = " left join recibos_gestiones_t on $tabla.id=recibos_gestiones_t.recibo_id  ";
} else {
	$filtros_iniciales = " and (empresa_servicio=1 or empresa_servicio=2 or empresa_servicio=6) and (via_cobro=1) ";
}
*/


// Cada linea de registro en la tabla del listado podra tener un conjunto de acciones. Las Mas normales son
// MODIFICAR y BORRAR. En determinado momento nos interesara que esas acciones aparezcan o no (en funcion de una
// condicion que se aplique a cada registro). En otros casos, las opciones son iguales para todos los registros.
// Por lo tanto, tenemos dos modos de trabajo.
// 1. MODO GENERICO: Las acciones son iguales para todos los registros.
// 2. MODO PERSONALIZADO: Las acciones dependen de la ejecucion de una funcion para cada registro.

// 2. MODO PERSONALIZADO
// Nombre de la funcion que se ejecutara para cada registro. Si se deja blanco el modo personalizado NO ESTA ACTIVO
$nombre_funcion_acciones_por_registro = "funcion_acciones_registro";

function funcion_acciones_registro($valor_id)
{
    global $tabla, $enlacevolver, $script;
    // me llega decodificado el id del registro en cuestion (en valor_id) pero tengo que codificarlo para ponerlo
    // en los enlaces
    $id_encript = base64_encode(base64_encode($valor_id));
    
    // Aqui se incluye el codigo necesario sobre el registro id_encript con el objetivo de sacar tantos echos
    // como acciones sean necesarias para este registro
    
    // Lo siguiente es un ejemplo de un proyecto determinado
    /*
    $array_datos_registro = obtener_multiples_campos(array("ano"),$tabla,""," $tabla.id='".$valor_id."' ","","","");
    // compruebo que la funcion solo me devuelve los datos de un registro
    if (count($array_datos_registro) == 1 && $array_datos_registro[0]['ano'] == date("Y"))
    {
            echo '
            <a href="'.$enlacevolver.$script.'&accion=formmodificar&id='.$id_encript.'"><img src="images/table_edit.png" title="Modificar" border="0" /></a> ';
    }
    */
	//echo '<a class="btn btn-default btn-xs" href="modules.php?mod=gestproject&file=index_contenidos_galerias_new&padre_id='.$id_encript.'&pag=0"><i class="fugue-pencil" title="editar"></i>GALERIA</a> ';
		 
    echo '<a class="btn btn-default btn-xs" href="modules.php?mod=gestproject&file=index_contenidos_pressroom_new&accion=formmodificar&id='.$id_encript.'&pag=0"><i class="fugue-pencil" title="editar"></i>VER / MODIFICAR</a> ';
	
	
		 
    echo '<a class="btn btn-default btn-xs" href="modules.php?mod=gestproject&file=index_contenidos_pressroom_new&accion=formborrar&id='.$id_encript.'&pag=0"><i class="fugue-pencil" title="editar"></i>BORRAR</a> ';

	
}
// FIN 2. MODO PERSONALIZADO


// 1. MODO GENERICO
// acciones_por_registro: Es un array de botones u opciones que debe tener cada registro
// Cada registro tendra un conjunto de acciones. Fijarse como en el ID del registro, ponemos #ID#.
// maestro_formulario.php pondra el ID correcto.
$acciones_por_registro = array(); 
$condiciones_visibilidad_por_registro = array();

$acciones_por_registro[] = '<a style="margin-top: 2px;" class="btn btn-xs btn-info" href="modules_new.php?mod=gestproject_new&file='.$script.'&accion=formmodificar&id=#ID#"><i class="icon-pencil2" title="editar"></i></a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a style="margin-top: 2px;" class="btn btn-xs btn-danger" href="modules_new.php?mod=gestproject_new&file='.$script.'&accion=formborrar&id=#ID#"><i class="icon-cancel-circle2" title="borrar"></i></a>';
$condiciones_visibilidad_por_registro[] = "";
// FIN 1. MODO GENERICO

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear
$proceso_pre_formcrear = "";
$proceso_pre_formmodificar = "";
$proceso_pre_accioncrear= "";
$proceso_pre_accionmodificar= "";
$proceso_post_accioncrear= "";
$proceso_post_accionmodificar= "";
$proceso_pre_listado = "modules/gestproject_new/index_contenidos_new_proceso_pre_listado.php";
$proceso_post_listado = "";

// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// IMPORTANTE: el padre debe venir desde el script anterior en la forma: &padre_id=XYZ
//             donde XYZ es el valor con doble codificacion base64
//             Por ejemplo, si un script index_facturas_new es el padre de index_facturas_cobros_new
//             dentro de index_facturas_new hay que hacer un:
//             $id_encript = base64_encode(base64_encode($linea[id]));
//             En este caso, el index_facturas_new tendria un enlace a index_facturas_cobros_new de
//             la siguiente forma:
//             <a href=\"modules.php?mod=gestproject&file=index_cobros_facturas_new&padre_id=$id_encript\">COBROS</a>

// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
// $campo_padre = "presupuesto_id";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
// $consulta_nombre_padre = "select numero as nombre from presupuestos_t where id=#PADREID#;";
// Enlace al que volver al padre
// $enlace_volver_a_padre = "modules.php?mod=gestproject&file=index_presupuestos_new&pag=0";
// Texto del enlace volver al padre
// $texto_volver_a_padre = "Volver a presupuestos";

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 1;
// estilos de los buscadores
$clase_buscador_input = "";
$clase_buscador_select = "";
$clase_buscador_checkbox = "";

$buscadores = array();
$buscadores[] = "input;pn_title;;;;buscar por titulo";
//$buscadores[] = "select;pn_topic;nuke_topics;nombre_topico;id;buscar por topico";
//$buscadores[] = "intervalo_fechas;fecha_creacion;;;;Fecha de creaci&oacute;n";
//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";


// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

?>

 

            </section>

