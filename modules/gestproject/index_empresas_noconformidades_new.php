<?php 
//error_reporting(E_ALL); 
//ini_set("display_errors", 1); 

//echo "<center>Esta usted validado como:<b> $uname</b></center><br>";
if ($user_id == "") {
  echo "DEBE INICIAR UNA SESION. <a href=index.php>Introduzca nuevamente si nombre de usuario y password</a>";
  include ("footer.php");
  exit;
 }
 


// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "NO CONFORMIDADES ";
// Titulo que aparece en la pestna del navegador
$titulo_pagina = "NO CONFORMIDADES";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";
// Texto que aparece en el boton de crear
$titulo_boton_crear = "CREAR NO CONFORMIDAD";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 1;
// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=gestproject&file=";
// Nombre del script
$script = "index_empresas_noconformidades_new";
// Nombre de la tabla
$tabla = "empresas_noconformidades_t"; // OJO, la clave principal se debe llamar id
$clase_boton_crear = " class='buttonmario mediummario green' ";
$clase_boton_buscar = " class='buttonmario mediummario green' ";
$clase_boton_guardar = " class='buttonmario mediummario green' ";
$clase_boton_volver  = " class='buttonmario mediummario green' ";
$clase_boton_confirmar_borrado  = " class='buttonmario mediummario red' ";

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
//$campos_col1 = array('contrato_id','local_id','contacto_id','tipo_incidencia_id','observaciones');
$campos_col1 = array('local_id','contacto_id','tipo_incidencia_id','observaciones');

// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]
//$ayudas_col1 = array();

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('on','','on','on');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('','','','');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('','','','');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php
//echo "padre_id: $_REQUEST[padre_id]";
$padre_id_desencript = base64_decode(base64_decode($_REQUEST[padre_id]));

// Al final no mostramos el tipo de contrato (no da informacion interesante)
//$tipos_col1  = array('select_join;contratos_t;id|total_base_imp|f_aceptado_date;id;;;select maestro_tipos_contrato_t.nombre as nombre, contratos_t.id as id, date(contratos_t.f_aceptado_date) as f_aceptado_date, contratos_t.total_base_imp as total_base_imp from maestro_tipos_contrato_t left join contratos_t on contratos_t.tipo_contrato=maestro_tipos_contrato_t.id where contratos_t.empresa_id='.$padre_id_desencript.' and contratos_t.estado=4','select_join;locales_t;nombre|direccion;id;;;select locales_t.nombre as nombre, locales_t.id as id, locales_t.direccion as direccion from locales_t where locales_t.empresa_id='.$padre_id_desencript.' order by locales_t.nombre asc ','select_join;empresas_contactos_t;nombre;id;;;select empresas_contactos_t.nombre as nombre, empresas_contactos_t.id as id from empresas_contactos_t where empresas_contactos_t.empresa_id='.$padre_id_desencript.' order by empresas_contactos_t.nombre asc','select;maestro_noconformidad_tipo_incidencia_t;nombre;id','textarea;800;300');

$tipos_col1  = array('select_join;locales_t;nombre|direccion;id;;;select locales_t.nombre as nombre, locales_t.id as id, locales_t.direccion as direccion from locales_t where locales_t.empresa_id='.$padre_id_desencript.' order by locales_t.nombre asc ','select_join;empresas_contactos_t;nombre;id;;;select empresas_contactos_t.nombre as nombre, empresas_contactos_t.id as id from empresas_contactos_t where empresas_contactos_t.empresa_id='.$padre_id_desencript.' order by empresas_contactos_t.nombre asc','select;maestro_noconformidad_tipo_incidencia_t;nombre;id','textarea;800;300');


//select bancos_pagadores_t.id as id, bancos_pagadores_t.num_cuenta as num_cuenta from bancos_pagadores_t left join facturas_t on bancos_pagadores_t.alumno_id=facturas_t.pagador_id where facturas_t.id='.$padre_id_desencript


//$tipos_col1  = array('select;locales_t;nombre;id;nombre;;habilitado_popup|modules.php?mod=gestproject&file=index_locales_busquedas_new|BUSCAR','select;maestro_nivel_infestacion_t;nombre;id','text;100','text;100','textarea;400;40','textarea;400;40','textarea;400;40','textarea;400;40');

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_insert = " noc='$noc', ";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = " user_id='$user_id', fecha_alta=now() , estado_noconformidad_id=1 ,";

// Campo para la busqueda
$campo_busqueda = "fecha_alta";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript

$plantilla_insercion = "index_empresas_noconformidades_new.plantilla.php";

if ($plantilla_insercion != "") {
  $fichero_absoluto = $dir_raiz . "modules/gestproject/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}

// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
if ($grupo==5) {
	$visualizar_listado = 1;
} else {
	$visualizar_listado = 0;
}
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','fecha_alta','user_id','estado_noconformidad_id','contrato_id','local_id','contacto_id','tipo_incidencia_id');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','Fecha de alta','Usuario','Estado','Contrato','Local','Contacto','Tipo de incidencia');
// Decodificacion si existiese de los campos
//echo "login: $login";

/*
if ($login != "Admin" && $login != "mteresa") {
	$campos_listado_decod = array ('','','','si;maestro_recibos_tipo_gestion_solo_cobrador_t;nombre;id','si;usuarios_t;login;id','si;usuarios_t;login;id','');
} else {
	$campos_listado_decod = array ('','','','si;maestro_recibos_tipo_gestion_t;nombre;id','si;usuarios_t;login;id','si;usuarios_t;login;id','');
}
*/
$campos_listado_decod = array ('','si;datetime','si;usuarios_t;login;id','si;maestro_estados_noconformidad_t;nombre;id','','si;locales_t;nombre;id','si;empresas_contactos_t;nombre;id','si;maestro_noconformidad_tipo_incidencia_t;nombre;id');


// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-striped table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_listado = " and noc='$noc'";
// Para el paginado
$registros_por_pagina = "30";

// acciones_por_registro: Es un array de botones u opciones que debe tener cada registro
// Cada registro tendra un conjunto de acciones. Fijarse como en el ID del registro, ponemos #ID#.
// maestro_formulario.php pondra el ID correcto.
$acciones_por_registro = array();
$condiciones_visibilidad_por_registro = array();

	//$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_empresas_noconformidades_gestiones_new&padre_id='.$id_encript.'&pag=0">Incidencias</a>';


	// La accion de CONTRATOS queremos que sea via popup
	$url .= "modules.php?mod=gestproject&file=index_empresas_noconformidades_gestiones_new&padre_id=#PADREID#&pag=0";

	$popup = '
	<style>
	/* para los popups */
	.white-popup-gestiones {
	  position: relative;
	  background: #FFF;
	  padding: 20px;
	  width: auto;
	  max-width: 650px;
	  margin: 20px auto;
	}
	</style>
	<!-- POPUP -->
	<div id="login-dialog-gestiones#ID_DECOD#" class="mfp-with-anim mfp-hide mfp-dialog clearfix white-popup-gestiones">
		<!-- <h3>Entrar</h3> -->
		<iframe width=650 height=350 src='.$url.'></iframe>
	</div>
	';
				
	$acciones_por_registro[] = $popup . '<a class="smallmario green popup-text" href="#login-dialog-gestiones#ID_DECOD#" data-effect="mfp-move-from-top"><i class="fugue-cross-circle" title="gestiones"></i> GESTIONES</a>';
	$condiciones_visibilidad_por_registro[] = "";

	
	// La accion de PARTES queremos que sea via popup
	$url .= "modules.php?mod=gestproject&file=index_empresas_noconformidades_partes_adhoc&padre_id=#ID#&pag=0";

	$popup = '
	<style>
	/* para los popups */
	.white-popup-partes {
	  position: relative;
	  background: #FFF;
	  padding: 20px;
	  width: auto;
	  max-width: 650px;
	  margin: 20px auto;
	}
	</style>
	<!-- POPUP -->
	<div id="login-dialog-partes#ID_DECOD#" class="mfp-with-anim mfp-hide mfp-dialog clearfix white-popup-partes">
		<!-- <h3>Entrar</h3> -->
		<iframe width=650 height=350 src='.$url.'></iframe>
	</div>
	';
				
	$acciones_por_registro[] = $popup . '<a class="smallmario green popup-text" href="#login-dialog-partes#ID_DECOD#" data-effect="mfp-move-from-top"><i class="fugue-cross-circle" title="partes"></i> PARTES VINCULADOS</a>';
	$condiciones_visibilidad_por_registro[] = "";
	
	
if ($grupo==5) {
	$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formmodificar&id=#ID#&padre_id=#PADREID#">MODIFICAR</a>';
	$condiciones_visibilidad_por_registro[] = "";


	$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file='.$script.'&accion=formborrar&id=#ID#&padre_id=#PADREID#">BORRAR</a>';
	$condiciones_visibilidad_por_registro[] = "";
	
	$acciones_por_registro[] = '<a class="smallmario green" href="modules.php?mod=gestproject&file=index_empresas_noconformidades_new&accion=cerrarnoconformidad&id=#ID#&padre_id=#PADREID#&pag=0" onclick="return confirm(\'¿Esta usted seguro de cerrar la NO CONFORMIDAD?\')">CERRAR INCIDENCIA</a>';
	$condiciones_visibilidad_por_registro[] = "";
}

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear

/*
$proceso_pre_accioncrear= "modules/gestproject/proceso_pre_gestioncobros_accioncrearmodificar.php";

$proceso_post_accioncrear= "modules/gestproject/proceso_post_gestioncobros_accioncrearmodificar.php";
$proceso_post_accionmodificar= "modules/gestproject/proceso_post_gestioncobros_accioncrearmodificar.php";
*/

/*
$proceso_pre_formcrear = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formcrear.php";
$proceso_pre_formmodificar = "modules/contratos/procesos/proceso_pre_form_alta_contrato_formmodificar.php";
$proceso_pre_accioncrear= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php";
$proceso_pre_accionmodificar= "modules/contratos/procesos/proceso_pre_accioncrearmodificar.php"*/;
$proceso_post_accioncrear= "modules/gestproject/index_empresas_noconformidades_new_proceso_post_accioncrear.php";
/*$proceso_post_accionmodificar= "modules/contratos/procesos/proceso_post_accioncrearmodificar.php";
*/

// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
$campo_padre = "empresa_id";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
$consulta_nombre_padre = " select nombre as nombre from empresas_t where id=#PADREID#";

// Visualizamos los datos del padre, mas completos

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 0;
$buscadores = array();
$buscadores[] = "input;nombre";


/*
echo "<center>Gestiones realizadas sobre: ";
$padre_id_desencript = base64_decode(base64_decode($_GET[padre_id]));
$cons = "select * from recibos_t where id='$padre_id_desencript';";
$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
while ($lin = mysql_fetch_array($res, MYSQL_ASSOC)) {
   echo "<b>$lin[nombre_empresa]</b> | Contrato: <b>$lin[contrato]</b> | Total del recibo: <b>$lin[total_recibo]</b>";
}
echo " </center>";
*/

// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

if ($accion == "cerrarnoconformidad") {
	$id=base64_decode(base64_decode($_REQUEST[id]));
	$sql="update empresas_noconformidades_t set estado_noconformidad_id=4 where id=$id";
	$rs=mysql_query($sql);		
}

?>

