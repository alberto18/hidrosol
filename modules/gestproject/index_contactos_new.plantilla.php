  <div class="row">
  	<div class="col-sm-12">
	  <section class="panel panel-default">
		<header class="panel-heading font-bold">Contactos recibidos en GCMC</b></header>
<table border=1   class='table table-bordered table-condensed table-hover' align=center>
        <tr>
        <td>Nombre:</td><td>[nombre]</td>
        </tr>
		<tr>
        <td>Tipo:</td><td>[tipo_id]</td>
        </tr>
		<tr>
        <td>Tel&eacute;fono:</td><td>[tlf]</td>
        </tr>
		<tr>
        <td>Movil:</td><td>[movil]</td>
        </tr>
		<tr>
        <td>Email:</td><td>[email]</td>
        </tr>
		
		<tr>
        <td>web:</td><td>[web]</td>
        </tr>
		<tr>
        <td>facebook:</td><td>[facebook]</td>
        </tr>
		<tr>
        <td>youtube:</td><td>[youtube]</td>
        </tr>
		<tr>
        <td>instagram:</td><td>[instagram]</td>
        </tr>
		<tr>
        <td>twitter:</td><td>[twitter]</td>
        </tr>
		<tr>
        <td>Una carta original del medio firmada y sellada por el director o responsable del medio certificando la funci&oacute;n del solicitante, donde conste su correo electr&oacute;nico y tel&eacute;fono directo:</td><td>[nombre_fichero]</td>
        </tr>
		<tr>
        <td>medio_comunicacion:</td><td>[medio_comunicacion]</td>
        </tr>
		<tr>
        <td>estado_id:</td><td>[estado_id]</td>
        </tr>
		<tr>
        <td>funcion_solicitante:</td><td>[funcion_solicitante]</td>
        </tr>
		<tr>
        <td>nif:</td><td>[nif]</td>
        </tr>
		<tr>
        <td>observaciones:</td><td>[observaciones]</td>
        </tr>
		
		
		<tr>
        <td>check_lopd:</td><td>[check_lopd]</td>
        </tr>
		<tr>
        <td>nombre_establecimiento:</td><td>[nombre_establecimiento]</td>
        </tr>
		<tr>
        <td>ciudad:</td><td>[ciudad]</td>
        </tr>
		<tr>
        <td>domicilio_fiscal_en_canarias:</td><td>[domicilio_fiscal_en_canarias]</td>
        </tr>
		<tr>
        <td>venta_ropa_bano:</td><td>[venta_ropa_bano]</td>
        </tr>
		<tr>
        <td>direccion:</td><td>[direccion]</td>
        </tr>
		
	
</table>
<br><br>



	  </section>
	</div>
   </div>


