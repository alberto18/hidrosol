<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Presupuesto</title>
<style type="text/css">
*
{
	margin:0;
	padding:0;
}
.tabla1
{
	page-break-after:always;
}
.estilo1, .estilo2
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:9px;
	margin-bottom:12px;
	margin-right:10px;
}
.estilo1
{
	color:#1A9AD7;
}
.estilo2
{
	color:#54616A;
}
.estilo3
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:20px;
	font-weight:bold;
	color:#03144A;
}
.estilo4
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:9px;
	color:#1A9AD7;
	font-weight:bold;
}
.estilo5
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:10px;
	color:#000000;
}
.estilo6
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:11px;
	color:#000000;
}
/*	font:0.75em/1.6em Arial,Helvetica,sans-serif;
*/
.estilo7
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#000000;
	padding-left:2px;
	padding-right:2px;
}
.estilo8
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:8px;
	color:#066F92;
	font-weight:bold;
}
.estilo9, .estilo10
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:12px;
	color:#000000;
}
.estilo10
{
	font-weight:bold;
}
.estilo11
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:9px;
	color:#066F92;
	font-weight:bold;
}
</style>
</head>

<body>
<?php
$get_id=base64_decode(base64_decode($_GET['id']));

if ($get_id > 0)
{
	//Averiguar el n�mero de l�neas del presupuesto
	/* ALB 
	$consulta1  = "Select articulos_t.id, articulos_t.multiple, articulos_t.nombre, articulos_t.referencia, articulos_t.descripcion, articulos_t.codigo, detalle_presupuestos_t.precio, detalle_presupuestos_t.cantidad, detalle_presupuestos_t.dto, detalle_presupuestos_t.dto2, detalle_presupuestos_t.importe from detalle_presupuestos_t Inner Join articulos_t ON articulos_t.id = detalle_presupuestos_t.articulo where presupuesto_id='".$_GET['id']."' order by detalle_presupuestos_t.id asc;"; */
	$consulta1  = "Select articulos_t.id, articulos_t.multiple, articulos_t.nombre, articulos_t.referencia, articulos_t.descripcion, articulos_t.codigo, detalle_presupuestos_t.precio, detalle_presupuestos_t.cantidad, detalle_presupuestos_t.dto, detalle_presupuestos_t.dto2, detalle_presupuestos_t.importe from detalle_presupuestos_t Inner Join articulos_t ON articulos_t.id = detalle_presupuestos_t.articulo where presupuesto_id='".$get_id."' order by detalle_presupuestos_t.id asc;";
	//echo $consulta1;
	$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1" . mysql_error());
	$lineas_presupuesto = 0;
	$articulos_multiples = 0;
	while ($linea1 = mysql_fetch_array($resultado1))
	{
		$lineas_presupuesto++;
		if ($linea1['multiple'] == 'on')
		{
			$articulos_multiples++;
/*
$consultaX  = "Select detalle_articulos_multiples_t.*, articulos_t.nombre,articulos_t.codigo From
detalle_articulos_multiples_t Inner Join articulos_t ON articulos_t.id = detalle_articulos_multiples_t.articulo_base where detalle_articulos_multiples_t.articulo=$linea1[id]";
$resultadoX = mysql_query($consultaX) or die("La consulta fall&oacute;: $consultaX" . mysql_error());
while ($lineaX = mysql_fetch_array($resultadoX)) {
	$lineas_presupuesto++;
}
*/
		}
	}
	
	// Voy a fijar 30 l�neas por presupuesto , los articulos multiples se cuenta como 1, no se muestra composici�n
	$paginas_presupuestos = (int)ceil($lineas_presupuesto/30);
	
	$consulta1 = "select * from presupuestos_t where id='".$get_id."';";
	$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1" . mysql_error());
	while ($linea1 = mysql_fetch_array($resultado1))
	{
		$consulta2 = "select nombre, direccion, cp, localidad, provincia, cif,telefono,fax from almacenes_t where id='$linea1[cliente]';";
		$resultado2 = mysql_query($consulta2) or die("1La consulta fall&oacute;: " . mysql_error());
		while ($linea2 = mysql_fetch_array($resultado2))
		{
			$razonsocialcliente = $linea2['nombre'];
			$direccioncliente = $linea2['direccion'];
			$cpcliente = $linea2['cp'];
			$poblacioncliente = $linea2['localidad'];
			$provinciacliente = $linea2['provincia'];
			$nifcliente = $linea2['cif'];
			$tlfcliente = $linea2['telefono'];
			$faxcliente = $linea2['fax'];
			$fecha = $linea1['f_registro'];
			$num_presupuesto = $linea1['num_presupuesto'];
			$num_cliente = $linea1['cliente'];
			$observaciones = nl2br($linea1['observaciones']);
			$igic = $linea1['igic'];
			$base_imponible = $linea1['base_imponible'];
			$base_imponible = number_format($base_imponible, 2, ",", "."); //01/06/2017 CAMBIO DE FORMATO DE DECIMALES
			$cuota = $linea1['cuota'];
			$cuota = number_format($cuota, 2, ",", "."); //01/06/2017 CAMBIO DE FORMATO DE DECIMALES
			$total_bruto = $linea1['total_bruto'];
			$total_bruto = number_format($total_bruto, 2, ",", "."); //01/06/2017 CAMBIO DE FORMATO DE DECIMALES
			$total_neto = $linea1['total_neto'];
			$total_neto = number_format($total_neto, 2, ",", "."); //01/06/2017 CAMBIO DE FORMATO DE DECIMALES
			$obra = $linea1['obra'];
		}
	}
	
	for ($pag=0; $pag<$paginas_presupuestos; $pag++)
	{
		$limit_ini = $pag*30;
		$limit_fin = 30;
?>
<table width="830" height="1172" border="0" cellpadding="0" cellspacing="0" class="tabla1">
	<tr>
		<td width="28" height="26" bgcolor="#018ED3"></td>
		<td width="802" height="26" colspan="3"></td>
	</tr>
	<tr valign="top">
		<td width="28" height="1146">
		<table width="28" height="1146" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="28" height="51"></td>
			</tr>
			<tr>
				<td width="28" height="1095" bgcolor="#018ED3"></td>
			</tr>
		</table>
		</td>
		<td width="59" height="1146"></td>
		<td width="698" height="1146" rowspan="2">
<!--inicio presupuesto-->
		<table width="698" height="1146" border="0" cellpadding="0" cellspacing="0">
<!--inicio cabecera-->
			<tr height="195">
				<td><table width="698" height="195" border="0" cellpadding="0" cellspacing="0">
					<tr valign="top" height="195">
<!-- inicio col sup izq-->
						<td width="279"><table width="279" height="195" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td height="51"><img src="images/fact_nueva_01.jpg" /></td>
							</tr>
							<tr>
								<td height="58"><span class="estilo3">PRESUPUESTO</span></td>
							</tr>
							<tr valign="top">
								<td height="86"><table width="279" height="80" cellpadding="0" cellspacing="0">
									<tr>
										<td height="17" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">&nbsp;&nbsp;OBRA</span></td>
									</tr>
									<tr valign="top">
										<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0;"><span class="estilo5"><?php
		if ($obra != '')
		{
			echo "OBRA: $obra<br />";
		}
		echo $observaciones;
?></span></td>
									</tr>
								</table></td>
							</tr>
						</table></td>
<!-- fin col sup izq-->
						<td width="139" align="right"><p class="estilo1">HIDROPRESI&Oacute;N</p><p class="estilo2">PISCINAS</p><p class="estilo1">CLIMATIZACI&Oacute;N</p><p class="estilo2">ENERG&Iacute;A SOLAR</p><p class="estilo1">AIRE ACONDICIONADO</p><p class="estilo2">CALEFACCI&Oacute;N</p></td>
<!-- inicio col sup dch-->
						<td width="280"><table width="280" height="190" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td colspan="2" height="17" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">&nbsp;&nbsp;DATOS CLIENTE</span></td>
							</tr>
							<tr valign="top">
								<td colspan="2" height="125" style="border-style:solid; border-left-width:2px; border-left-color:#1A9AD7; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:0; padding:2px;"><span class="estilo6"><?php
		echo "N&ordm; CLIENTE: $num_cliente<br>
 CLIENTE: $razonsocialcliente<br>
 NIF/CIF: $nifcliente<br>
 DOMICILIO: $direccioncliente<br>
 LOCALIDAD: $poblacioncliente<br>
 CP: $cpcliente<br>
 TLF: $tlfcliente<br>
 FAX: $faxcliente";
?></span></td>
							</tr>
							<tr>
								<td width="140" height="17" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">&nbsp;&nbsp;FECHA</span></td>
								<td width="140" height="17" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;PRESUPUESTO N&ordm;</span></td>
							</tr>
							<tr height="21" align="center">
								<td width="140" style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0;"><span class="estilo6"><?php
		list($ano,$mes,$dia)=split('[-]',$fecha);
		list($dia,$nada)=split('[ ]',$dia);
		echo "$dia/$mes/$ano";
?></span></td>
								<td width="140" style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;"><span class="estilo6"><?php echo "$num_presupuesto"; ?></span></td>
							</tr>
						</table></td>
<!-- fin col sup dch-->
					</tr>
				</table></td>
			</tr>
<!--fin cabecera-->
<!--inicio cuerpo-->
			<tr valign="top">
				<td><table width="698" height="843" border="0" cellpadding="0" cellspacing="0">
					<tr height="17" align="center">
						<td width="18%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">ART&Iacute;CULO</span></td>
						<td width="38%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">DESCRIPCI&Oacute;N</span></td>
						<td width="8%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">CANTIDAD</span></td>
						<td width="10%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">PRECIO</span></td>
						<td width="6%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">DTO.</span></td>
						<td width="10%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">IMPORTE</span></td>
					</tr>
<?php
		/*$consulta1 = "Select articulos_t.id, articulos_t.multiple, articulos_t.nombre, articulos_t.referencia, articulos_t.descripcion, articulos_t.codigo, detalle_presupuestos_t.precio, detalle_presupuestos_t.cantidad, detalle_presupuestos_t.nombre_articulo, detalle_presupuestos_t.dto, detalle_presupuestos_t.dto2, detalle_presupuestos_t.importe From detalle_presupuestos_t Inner Join articulos_t ON articulos_t.id = detalle_presupuestos_t.articulo where presupuesto_id='".$_GET['id']."' order by detalle_presupuestos_t.articulo asc limit $limit_ini,$limit_fin"; */
		$consulta1 = "Select articulos_t.id, articulos_t.multiple, articulos_t.nombre, articulos_t.referencia, articulos_t.descripcion, articulos_t.codigo, detalle_presupuestos_t.precio, detalle_presupuestos_t.cantidad, detalle_presupuestos_t.nombre_articulo, detalle_presupuestos_t.dto, detalle_presupuestos_t.dto2, detalle_presupuestos_t.importe From detalle_presupuestos_t Inner Join articulos_t ON articulos_t.id = detalle_presupuestos_t.articulo where presupuesto_id='".$get_id."' order by detalle_presupuestos_t.id asc limit $limit_ini,$limit_fin";
		$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1" . mysql_error());
		while ($linea1 = mysql_fetch_array($resultado1))
		{
		
		$precio = number_format($linea1[precio], 2, ",", "."); //01/06/2017 CAMBIO DE FORMATO DE DECIMALES
		$importe = number_format($linea1[importe], 2, ",", "."); //01/06/2017 CAMBIO DE FORMATO DE DECIMALES
		
			if ($linea1['id'] == '9854')   // Articulo generico
			{
				$nombre = $linea1['nombre_articulo'];
			}elseif($linea1['id'] == '56718'){ //CAPITULO
			 
			 $nombre = $linea1['nombre_articulo'];
			
			}else
			{
				$nombre = $linea1['nombre'];
			}
			
			if ($linea1['dto2'] == '' || $linea1['dto2'] == 0)
			{
				echo "
					<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">$linea1[codigo]</span></td>
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">";
						
						if($linea1['id'] == '56718'){
						
						echo "<b>$nombre</b>";
						
						}else{
						echo "$nombre";
						}
						
						
						
						echo "</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">
						";
						
						if($linea1['id'] != '56718'){
						
						echo "$linea1[cantidad]";
						
						}
						echo "</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">
						";
						
						if($linea1['id'] != '56718'){
						
						echo "<!--$linea1[precio]-->$precio";
						
						}
						echo "
						
						
						</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">
						";
						
						if($linea1['id'] != '56718'){
						
						echo "$linea1[dto]";
						
						}
						echo "
						
						</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">
						";
						
						if($linea1['id'] != '56718'){
						
						echo "<!--$linea1[importe]--> $importe";
						
						}
						echo "
						</span></td>
					</tr>";
				if ($linea1['multiple'] == 'on')
				{
					$consultaX = "Select detalle_articulos_multiples_t.*, articulos_t.nombre, articulos_t.codigo From detalle_articulos_multiples_t Inner Join articulos_t ON articulos_t.id = detalle_articulos_multiples_t.articulo_base where detalle_articulos_multiples_t.articulo='$linea1[id]';";
					$resultadoX = mysql_query($consultaX) or die("La consulta fall&oacute;: $consultaX" . mysql_error());
					while ($lineaX = mysql_fetch_array($resultadoX))
					{
						$cantidad = $linea1['cantidad']*$lineaX['cantidad'];
						echo "
					<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">$lineaX[codigo]</span></td>
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;&nbsp;-$lineaX[nombre]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$cantidad</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
					</tr>";
					}
				}
			}
			else
			{
				echo "
					<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">$linea1[codigo]</span></td>
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$nombre</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[cantidad]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[precio]-->$precio</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[dto]+$linea1[dto2]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[importe]--> $importe</span></td>
					</tr>";
				if ($linea1['multiple'] == 'on')
				{
					$consultaX = "Select detalle_articulos_multiples_t.*, articulos_t.nombre, articulos_t.codigo From detalle_articulos_multiples_t Inner Join articulos_t ON articulos_t.id = detalle_articulos_multiples_t.articulo_base where detalle_articulos_multiples_t.articulo='$linea1[id]';";
					$resultadoX = mysql_query($consultaX) or die("La consulta fall&oacute;: $consultaX" . mysql_error());
					while ($lineaX = mysql_fetch_array($resultadoX))
					{
						$cantidad = $linea1['cantidad']*$lineaX['cantidad'];
						echo "
					<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">$lineaX[codigo]</span></td>
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;&nbsp;-$lineaX[nombre]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$cantidad</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
					</tr>";
					}
				}
			}
		}
		//Todas las paginas menos la ultima, poner Suma y Sigue ...
		if (($pag+1)<$paginas_presupuestos)
		{
			echo "
					<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;&nbsp;&nbsp;&nbsp;Suma y sigue ...</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
					</tr>";
		}
?>
					<tr valign="top">
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
					</tr>
				</table></td>
			</tr>
<!--fin cuerpo-->
<!--inicio pie-->
			<tr>
				<td><table width="100%" height="108" border="0" cellpadding="0" cellspacing="0">
					<tr valign="top">
						<td width="218"><br /><br /><br /><span class="estilo8">HIDROSOL CANARIAS, S.L.<br />C/De la Cizalla M3 37-P3N <br> Polig. Ind. Arinaga CP: 35118<br />Tel. 928 184 180  Fax. 928 180 729 CIF: B 35724814 <br>www.hidrosolcanarias.com&nbsp;&nbsp;info@hidrosolcanarias.com</span></td>
						<td style="padding-top:5px;" align="center">
						<table width="480" border="0" cellpadding="0" cellspacing="0">
							<tr align="center" height="17">
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">&nbsp;&nbsp;DTO PP</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;BASE IMPONIBLE</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;IGIC</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;CUOTA</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;TOTAL BRUTO</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;TOTAL NETO</span></td>
							</tr>
							<tr height="35" align="center">
								<td style="border-style:solid; border-left-width:2px; border-left-color:#1A9AD7; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9"><?php echo $dto_pp; ?></span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9"><?php echo $base_imponible; ?></span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9"><?php echo $igic; ?></span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9"><?php echo $cuota; ?></span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9"><?php echo $total_bruto; ?></span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo10"><?php echo $total_neto; ?></span></td>
							</tr>
						</table><br><span class="estilo8"><p align="center">Hidrosol Canarias S.L. inscrita en el Registro Mercantil de Las Palmas de G. C. Folio 140 del libro 0, Hoja GC 28934, Inscripci&oacute;n 1.</p></span></td>
					</tr>
				</table></td>
			</tr>
<!--fin pie-->
		</table>
<!--fin presupuesto-->
		</td>
		<td width="45" height="1146"></td>
	</tr>
</table>
<?php
	}
} // fin get[id]>0
?>
</body>
</html>