<!DOCTYPE HTML>
<html class="full">

<head>
    <title>TACGestorcontenidos Hidrosol Canarias</title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="inicio/css/bootstrap.css">
    <link rel="stylesheet" href="inicio/css/font-awesome.css">
    <link rel="stylesheet" href="inicio/css/icomoon.css">
    <link rel="stylesheet" href="inicio/css/styles.css">
    <link rel="stylesheet" href="inicio/css/mystyles.css">
    <script src="inicio/js/modernizr.js"></script>

    <link rel="stylesheet" href="inicio/css/switcher.css" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/bright-turquoise.css" title="bright-turquoise" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/turkish-rose.css" title="turkish-rose" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/salem.css" title="salem" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/hippie-blue.css" title="hippie-blue" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/mandy.css" title="mandy" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/green-smoke.css" title="green-smoke" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/horizon.css" title="horizon" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/cerise.css" title="cerise" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/brick-red.css" title="brick-red" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/de-york.css" title="de-york" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/shamrock.css" title="shamrock" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/studio.css" title="studio" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/leather.css" title="leather" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/denim.css" title="denim" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/scarlet.css" title="scarlet" media="all" />
</head>

<body class="full">


    <div class="global-wrap">
		
		
        <div class="full-page">
            <div class="bg-holder full">
                <div class="bg-mask"></div>
                <div class="bg-img" style="background-image:url('documentos/fondo_extranet3107.jpg');"></div>
                <div class="bg-holder-content full text-white">
                    <a class="logo-holder" href="index.php">
                        <img src="documentos/logo_hidrosol_panel.jpg" alt="TACgestorcontenidos Hidrosol Canarias" title="TACGestorcontenidos Hidrosol Canarias" /> 
                    </a>
                    <div class="full-center">
                        <div class="container">
                            <div class="row row-wrap" data-gutter="60">
                                <div class="col-md-4">
								
                                </div>
                                <div class="col-md-4">
                                    <div class="visible-lg">
                                        <h3>EXTRANET corporativa. HIDROSOL CANARIAS</h3>
                                        <p>Bienvenido a la EXTRANET para clientes de HIDROSOL CANARIAS</b>.</p>
									 <p>Si tiene alguna duda con el funcionamiento de este sistema, puede consultarla en atencioncliente@tac7.com</p><br><br><br>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h3 class="mb15">Login<br><br></h3>
                                    <form action=modules.php method=post>
									<input type=hidden name=mod value=extranet>
									<input type=hidden name=file value=login>
                                        <div class="form-group form-group-ghost form-group-icon-left"><i class="fa fa-user input-icon input-icon-show"></i>
                                            <label>Usuario</label>
                                            <input name="login" id="login" class="form-control" placeholder="Indique su usuario.." type="text" />
                                        </div>
                                        <div class="form-group form-group-ghost form-group-icon-left"><i class="fa fa-lock input-icon input-icon-show"></i>
                                            <label>Contraseña</label>
                                            <input name="passwd" id="passwd" class="form-control" type="password" placeholder="Indique su contraseña.." />
                                        </div>
                                        <input class="btn btn-primary" type="submit" value="Entrar" /><br><br>
                                    </form>
									
                                </div>

                            </div>
                        </div>
                    </div>
                    <ul class="footer-links">
                        <li><a target="_blank" href="http://www.tac7.com">TAC7: Innovacion en las Organizaciones</a>
                        </li>  
						<li><a target="_blank" href="modules.php?mod=portal&file=index">Hidrosol Canarias</a>
                        </li>
						<li> <img src="images/logos_innobonos.jpg"></li>
						
						
                    </ul>
                </div>
            </div>
        </div>



        <script src="inicio/js/jquery.js"></script>
        <script src="inicio/js/bootstrap.js"></script>
        <script src="inicio/js/slimmenu.js"></script>
        <script src="inicio/js/bootstrap-datepicker.js"></script>
        <script src="inicio/js/bootstrap-timepicker.js"></script>
        <script src="inicio/js/nicescroll.js"></script>
        <script src="inicio/js/dropit.js"></script>
        <script src="inicio/js/ionrangeslider.js"></script>
        <script src="inicio/js/icheck.js"></script>
        <script src="inicio/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="inicio/js/typeahead.js"></script>
        <script src="inicio/js/card-payment.js"></script>
        <script src="inicio/js/magnific.js"></script>
        <script src="inicio/js/owl-carousel.js"></script>
        <script src="inicio/js/fitvids.js"></script>
        <script src="inicio/js/tweet.js"></script>
        <script src="inicio/js/countdown.js"></script>
        <script src="inicio/js/gridrotator.js"></script>
        <script src="inicio/js/custom.js"></script>
        <script src="inicio/js/switcher.js"></script>
    </div>
</body>

</html>

 



