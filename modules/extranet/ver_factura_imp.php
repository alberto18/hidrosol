<?php
	include("variables_globales_gestproject.php");
	include("funciones.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Factura</title>
<style type="text/css">
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed,
figure, figcaption, footer, header, hgroup,
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
}
*
{
	margin:0;
	padding:0;
}
.tabla1
{
	page-break-after:always;
}
.estilo1, .estilo2
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:9px;
	margin-bottom:12px;
	margin-right:10px;
}
.estilo1
{
	color:#1A9AD7;
}
.estilo2
{
	color:#54616A;
}
.estilo3
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:20px;
	font-weight:bold;
	color:#03144A;
}
.estilo4
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:9px;
	color:#1A9AD7;
	font-weight:bold;
}
.estilo5
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:10px;
	color:#000000;
}
.estilo6
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:11px;
	color:#000000;
}
/*	font:0.75em/1.6em Arial,Helvetica,sans-serif;
*/
.estilo7
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#000000;
	padding-left:2px;
	padding-right:2px;
}
.estilo8
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:8px;
	color:#066F92;
	font-weight:bold;
}
.estilo9, .estilo10
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:12px;
	color:#000000;
}
.estilo10
{
	font-weight:bold;
}
.estilo11
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:9px;
	color:#066F92;
	font-weight:bold;
}
</style>
</head>

<body>
<?php
/*
$db = "tacproject_hidrosol";
$enlace = mysql_connect("localhost", "root", "")
   or die("No pudo conectarse : " . mysql_error());
//echo "Conexi&oacute;n exitosa";
mysql_select_db($db) or die("No pudo seleccionarse la BD.");
*/

$valor_factura = base64_decode(base64_decode($_REQUEST[id]));
$get_id=base64_decode(base64_decode($_GET['id']));

if ($get_id > 0)
{
	//calculo del numero de paginas
	$consulta1  = "select articulos_t.id, articulos_t.multiple, articulos_t.nombre, articulos_t.referencia, articulos_t.descripcion, articulos_t.nombre, articulos_t.codigo, detalle_facturas_t.precio, detalle_facturas_t.nombre_articulo, detalle_facturas_t.cantidad, detalle_facturas_t.dto, detalle_facturas_t.dto2, detalle_facturas_t.importe from detalle_facturas_t Inner Join articulos_t ON articulos_t.id = detalle_facturas_t.articulo where factura_id='".$get_id."' order by articulos_t.multiple desc, detalle_facturas_t.id asc;";
	$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1" . mysql_error());
	$lineas_albaran = 0;
	$articulos_multiples = 0;
	while ($linea1 = mysql_fetch_array($resultado1))
	{
		$lineas_albaran++;
		if ($linea1['multiple']=='on') { $articulos_multiples++; }
	}
	
	// Voy a fijar 30 l�neas por factura , los articulos multiples se cuenta como 1, no se muestra composici�n
	$paginas_albaranes = (int)ceil($lineas_albaran/30);
	
	$consulta1  = "select * from facturas_t where id='".$get_id."';";
	$resultado1 = mysql_query($consulta1) or die("$consulta1, La consulta fall&oacute;: $consulta1" . mysql_error());
	while ($linea1 = mysql_fetch_array($resultado1))
	{
		$consulta2  = "select nombre, direccion, cp, poblacion, provincia, cif, localidad, telefono, fax, fecha_cambio, nombre_cambio, direccion_cambio, cp_cambio, localidad_cambio, telefono_cambio, fax_cambio from almacenes_t where id='".$linea1['cliente']."';";
		$resultado2 = mysql_query($consulta2) or die("1La consulta fall&oacute;: " . mysql_error());
		while ($linea2 = mysql_fetch_array($resultado2))
		{
			$provinciacliente = $linea2['provincia'];
			$fecha = $linea1['fecha'];
			$num_factura = $linea1['num_factura'];
			$num_cliente = $linea1['cliente'];
			$observaciones = nl2br($linea1['observaciones']);
			
			$igic = $linea1['igic'];
			$igic = number_format($igic,2,'.','');
			$base_imponible = $linea1['base_imponible'];
			$base_imponible = number_format($base_imponible,2,'.','');
			$base_imponible = number_format($base_imponible, 2, ",", "."); //01/06/2017 CAMBIO DE FORMATO DE DECIMALES
			$cuota = $linea1['cuota'];
			$cuota = number_format($cuota,2,'.','');
			$cuota = number_format($cuota, 2, ",", "."); //01/06/2017 CAMBIO DE FORMATO DE DECIMALES
			$total_bruto = $linea1['total_bruto'];
			$total_bruto = number_format($total_bruto,2,'.','');
			$total_bruto = number_format($total_bruto, 2, ",", "."); //01/06/2017 CAMBIO DE FORMATO DE DECIMALES
			$total_neto = $linea1['total_neto'];
			$total_neto = number_format($total_neto,2,'.','');
			$total_neto = number_format($total_neto, 2, ",", "."); //01/06/2017 CAMBIO DE FORMATO DE DECIMALES
			// se a�aden los dos nuevos valores para las retenciones
			$retencion_id = $linea1['retencion_id'];
			$retencion_id = number_format($retencion_id,2,'.','');
			$cuota_retencion = $linea1['cuota_retencion'];
			$cuota_retencion = number_format($cuota_retencion,2,'.','');
			$cuota_retencion = number_format($cuota_retencion, 2, ",", "."); //01/06/2017 CAMBIO DE FORMATO DE DECIMALES
			
			
			$descripcion_factura = $linea1['descripcion_factura'];
			$rectifica_a = $linea1['rectifica_a'];
			$dto_pp = $linea1['dto_pp'];
			$servicio_tecnico = $linea1['servicio_tecnico'];
			$nifcliente = $linea2['cif'];
			
			if (strtotime($linea1['fecha']) <= strtotime($linea2['fecha_cambio']))
			{
				$razonsocialcliente = $linea2['nombre_cambio'];
				$direccioncliente = $linea2['direccion_cambio'];
				$poblacioncliente = $linea2['localidad_cambio'];
				$cpcliente = $linea2['cp_cambio'];
				$tlfcliente = $linea2['telefono_cambio'];
				$faxcliente = $linea2['fax_cambio'];
			}
			else
			{
				$razonsocialcliente = $linea2['nombre'];
				$direccioncliente = $linea2['direccion'];
				$poblacioncliente = $linea2['localidad'];
				$cpcliente = $linea2['cp'];
				$tlfcliente = $linea2['telefono'];
				$faxcliente = $linea2['fax'];
			}
		}
	}
	
	for ($pag=0; $pag<$paginas_albaranes; $pag++)
	{
		$limit_ini = $pag*30;
		$limit_fin = 30;
?>
<table width="830" height="1172" border="0" cellpadding="0" cellspacing="0" class="tabla1">
	<tr>
		<td width="28" height="26" bgcolor="#018ED3"></td>
		<td width="802" height="26" colspan="3"></td>
	</tr>
	<tr valign="top">
		<td width="28" height="1146">
		<table width="28" height="1146" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="28" height="51"></td>
			</tr>
			<tr>
				<td width="28" height="1095" bgcolor="#018ED3"></td>
			</tr>
		</table>
		</td>
		<td width="59" height="1146"></td>
		<td width="698" height="1146" rowspan="2">
<!--inicio factura-->
		<table width="698" height="1146" border="0" cellpadding="0" cellspacing="0">
<!--inicio cabecera-->
			<tr height="195">
				<td><table width="698" height="195" border="0" cellpadding="0" cellspacing="0">
					<tr valign="top" height="195">
<!-- inicio col sup izq-->
						<td width="279"><table width="279" height="195" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td height="51"><img src="images/fact_nueva_01.jpg" /></td>
							</tr>
							<tr>
								<td height="58"><span class="estilo3">
<?php
		if ($rectifica_a == '')
		{
			echo "FACTURA";
			$contenido1 .= "FACTURA";
			//echo "($consulta1)";
		}
		else
		{
			echo "FACTURA RECTIFICATIVA";
			$contenido1 .= "FACTURA RECTIFICATIVA";
		}
?></span></td>
							</tr>
							<tr valign="top">
								<td height="86"><table width="279" height="80" cellpadding="0" cellspacing="0">
									<tr>
										<td height="17" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">&nbsp;&nbsp;OBSERVACIONES</span></td>
									</tr>
									<tr valign="top">
										<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0;"><span class="estilo5"><?php echo $observaciones; ?></span></td>
									</tr>
								</table></td>
							</tr>
						</table></td>
<!-- fin col sup izq-->
						<td width="139" align="right"><p class="estilo1">HIDROPRESI&Oacute;N</p><p class="estilo2">PISCINAS</p><p class="estilo1">CLIMATIZACI&Oacute;N</p><p class="estilo2">ENERG&Iacute;A SOLAR</p><p class="estilo1">AIRE ACONDICIONADO</p><p class="estilo2">CALEFACCI&Oacute;N</p></td>
<!-- inicio col sup dch-->
						<td width="280"><table width="280" height="190" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td colspan="2" height="17" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">&nbsp;&nbsp;DATOS CLIENTE</span></td>
							</tr>
							<tr valign="top">
								<td colspan="2" height="125" style="border-style:solid; border-left-width:2px; border-left-color:#1A9AD7; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:0; padding:2px;"><span class="estilo6"><?php
echo " N&ordm; CLIENTE: $num_cliente<br />
 CLIENTE: $razonsocialcliente<br />
 NIF/CIF: $nifcliente<br />
 DOMICILIO: $direccioncliente<br />
 LOCALIDAD: $poblacioncliente<br />
 CP: $cpcliente<br />
 TLF: $tlfcliente<br />
 FAX: $faxcliente";
?></span></td>
							</tr>
							<tr>
								<td width="140" height="17" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">&nbsp;&nbsp;FECHA</span></td>
								<td width="140" height="17" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;FACTURA N&ordm;</span></td>
							</tr>
							<tr height="21" align="center">
								<td width="140" style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0;"><span class="estilo6"><?php
		list($ano,$mes,$dia)=split('[-]',$fecha);
		list($dia,$nada)=split('[ ]',$dia);
		echo "$dia/$mes/$ano";
?></span></td>
								<td width="140" style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;"><span class="estilo6"><?php echo "$num_factura"; ?></span></td>
							</tr>
						</table></td>
<!-- fin col sup dch-->
					</tr>
				</table></td>
			</tr>
<!--fin cabecera-->
<!--inicio cuerpo-->
			<tr valign="top">
				<td><table width="698" height="843" border="0" cellpadding="0" cellspacing="0">
					<tr height="17" align="center">
						<td width="18%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">ART&Iacute;CULO</span></td>
						<td width="38%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">DESCRIPCI&Oacute;N</span></td>
						<td width="8%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">CANTIDAD</span></td>
						<td width="10%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">PRECIO</span></td>
						<td width="6%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">DTO.</span></td>
						<td width="10%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">IMPORTE</span></td>
					</tr>
<?php
		$consulta1  = "Select articulos_t.id, articulos_t.multiple, articulos_t.nombre, articulos_t.referencia, articulos_t.descripcion, articulos_t.nombre, articulos_t.codigo, detalle_facturas_t.precio, detalle_facturas_t.nombre_articulo, detalle_facturas_t.cantidad, detalle_facturas_t.dto, detalle_facturas_t.dto2, detalle_facturas_t.importe From detalle_facturas_t Inner Join articulos_t ON articulos_t.id = detalle_facturas_t.articulo where factura_id='".$get_id."' order by detalle_facturas_t.id asc limit $limit_ini,$limit_fin;";
		//echo "$consulta1";
		$resultado1 = mysql_query($consulta1) or die("La consulta fall&oacute;: $consulta1 " . mysql_error());
		while ($linea1 = mysql_fetch_array($resultado1))
		{
		
		$precio = number_format($linea1[precio], 2, ",", "."); //01/06/2017 CAMBIO DE FORMATO DE DECIMALES
		$importe = number_format($linea1[importe], 2, ",", "."); //01/06/2017 CAMBIO DE FORMATO DE DECIMALES
		
			if ($linea1['id'] == '9854')   // Articulo generico
			{
				$nombre = $linea1['nombre_articulo'];
			}
			else
			{
				$nombre = $linea1['nombre'];
			}
			
			if ($descripcion_factura == 'on')
			{
				if ($linea1['dto2'] == '' || $linea1['dto2'] == 0)
				{
					echo "
					<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">$linea1[codigo]</span></td>
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$nombre $linea1[descripcion]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[cantidad]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[precio]-->$precio</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[dto]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[importe]--> $importe</span></td>
					</tr>";
					
					$contenido2 .="<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">$linea1[codigo]</span></td>
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$nombre $linea1[descripcion]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[cantidad]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[precio]-->$precio</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[dto]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[importe]--> $importe</span></td>
					</tr>";
				}
				else
				{
					echo "
					<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">$linea1[codigo]</span></td>
						<td align=\"left\"><span class=\"estilo7\">$nombre $linea1[descripcion]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[cantidad]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[precio]--> $precio</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[dto]+$linea1[dto2]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[importe]--> $importe</span></td>
					</tr>";
					
					$contenido2 .= "
					<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">$linea1[codigo]</span></td>
						<td align=\"left\"><span class=\"estilo7\">$nombre $linea1[descripcion]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[cantidad]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[precio]--> $precio</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[dto]+$linea1[dto2]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[importe]--> $importe</span></td>
					</tr>";
				}
			}
			else
			{
				if ($linea1['dto2'] == '' || $linea1['dto2'] == 0)
				{
					echo "
					<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">$linea1[codigo]</span></td>
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$nombre</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[cantidad]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[precio]--> $precio</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[dto]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[importe]--> $importe</span></td>
					</tr>";
					
					$contenido2 .= " <tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">$linea1[codigo]</span></td>
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$nombre</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[cantidad]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[precio]--> $precio</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[dto]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[importe]--> $importe</span></td>
					</tr>";
				}
				else
				{
					echo "
					<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">$linea1[codigo]</span></td>
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$nombre</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[cantidad]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[precio]--> $precio</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[dto]+$linea1[dto2]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[importe]--> $importe</span></td>
					</tr>";
					
					$contenido2 .= "<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">$linea1[codigo]</span></td>
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$nombre</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[cantidad]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[precio]--> $precio</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">$linea1[dto]+$linea1[dto2]</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\"><!--$linea1[importe]--> $importe</span></td>
					</tr>";
				}
			}
		}
		//Todas las paginas menos la ultima, poner Suma y Sigue ...
		if (($pag+1)<$paginas_albaranes)
		{
			echo "
					<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;&nbsp;&nbsp;&nbsp;Suma y sigue ...</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
					</tr>";
					
					$contenido2 .= "<tr valign=\"top\" height=\"20\">
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"left\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;&nbsp;&nbsp;&nbsp;Suma y sigue ...</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
						<td align=\"right\" style=\"border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-bottom-width:0; border-left-width:0;\"><span class=\"estilo7\">&nbsp;</span></td>
					</tr>";
		}
?>
					<tr valign="top">
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
					</tr>
				</table></td>
			</tr>
<!--fin cuerpo-->
<!--inicio pie-->
			<tr>
				<td><table width="100%" height="108" border="0" cellpadding="0" cellspacing="0">
					<tr valign="top">
						<td width="218"><br /><br /><br /><span class="estilo8">HIDROSOL CANARIAS, S.L.<br />C/De la Cizalla M3 37-P3N<br> Polig. Ind. Arinaga CP: 35118<br />Tel. 928 184 180 � Fax. 928 180 729 CIF: B 35724814 <br>www.hidrosolcanarias.com&nbsp;&nbsp;info@hidrosolcanarias.com</span></td>
						<td style="padding-top:5px;" align="center">
						<table width="480" border="0" cellpadding="0" cellspacing="0">
							<tr align="center" height="17">
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">&nbsp;&nbsp;DTO PP</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;BASE IMPONIBLE</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;IGIC</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;CUOTA</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;I.R.P.F</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;CUOTA</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;TOTAL BRUTO</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;TOTAL NETO</span></td>
							</tr>
							<tr height="35" align="center">
								<td style="border-style:solid; border-left-width:2px; border-left-color:#1A9AD7; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9"><?php echo $dto_pp; ?></span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9"><?php echo $base_imponible; ?></span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9"><?php echo $igic; ?></span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9"><?php echo $cuota; ?></span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9"><?php echo $retencion_id; ?></span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9"><?php echo $cuota_retencion; ?></span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9"><?php echo $total_bruto; ?></span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo10"><?php echo $total_neto; ?></span></td>
							</tr>
						</table><?php
		if ($servicio_tecnico != '')
		{
			echo "<span class=\"estilo10\">Servicio T&eacute;cnico. Esta factura se debe pagar a 30 d&iacute;as de la fecha factura</span><br />";
			$contenido3 .= "<span class=\"estilo10\">Servicio T&eacute;cnico. Esta factura se debe pagar a 30 d&iacute;as de la fecha factura</span><br />";
		}
		if ($rectifica_a != '')
		{
			$consultaX = "select num_factura from facturas_t where id='$rectifica_a';";
			$resultadoX = mysql_query($consultaX ) or die("La consulta fall&oacute;: $consultaX" . mysql_error());
			while ($lineaX = mysql_fetch_array($resultadoX))
			{
				$rectifica_a = $lineaX['num_factura'];
			}
			echo "<span class=\"estilo10\">Esta factura rectifica a la factura con n&uacute;mero $rectifica_a</span><br />";
			$contenido3 .= "<span class=\"estilo10\">Esta factura rectifica a la factura con n&uacute;mero $rectifica_a</span><br />";
		}

		if ($num_cliente == 560)
		{
			//cliente contado
			echo "<span class=\"estilo11\">NO SE ADMITEN DEVOLUCIONES PASADAS 48 HORAS Y SIEMPRE ACOMPA&Ntilde;ADO DEL ALBAR&Aacute;N DE COMPRA Y EN EMBALAJE ORIGINAL</span><br /><span class=\"estilo8\"><p align=\"center\">Hidrosol Canarias S.L. inscrita en el Registro Mercantil de Las Palmas de G. C. Folio 140 del libro 0, Hoja GC 28934, Inscripci&oacute;n 1.</p></span>";
			$contenido3 .= "<span class=\"estilo11\">NO SE ADMITEN DEVOLUCIONES PASADAS 48 HORAS Y SIEMPRE ACOMPA&Ntilde;ADO DEL ALBAR&Aacute;N DE COMPRA Y EN EMBALAJE ORIGINAL</span><br /><span class=\"estilo8\"><p align=\"center\">Hidrosol Canarias S.L. inscrita en el Registro Mercantil de Las Palmas de G. C. Folio 140 del libro 0, Hoja GC 28934, Inscripci&oacute;n 1.</p></span>";
		}
		else
		{
			echo "<span class=\"estilo11\">NO SE ADMITEN DEVOLUCIONES PASADOS 30 D&Iacute;AS Y SIEMPRE ACOMPA&Ntilde;ADO DEL ALBAR&Aacute;N DE COMPRA Y EN EMBALAJE ORIGINAL</span><br /><span class=\"estilo8\"><p align=\"center\">Hidrosol Canarias S.L. inscrita en el Registro Mercantil de Las Palmas de G. C. Folio 140 del libro 0, Hoja GC 28934, Inscripci&oacute;n 1.</p></span>";
			$contenido3 .= "<span class=\"estilo11\">NO SE ADMITEN DEVOLUCIONES PASADOS 30 D&Iacute;AS Y SIEMPRE ACOMPA&Ntilde;ADO DEL ALBAR&Aacute;N DE COMPRA Y EN EMBALAJE ORIGINAL</span><br /><span class=\"estilo8\"><p align=\"center\">Hidrosol Canarias S.L. inscrita en el Registro Mercantil de Las Palmas de G. C. Folio 140 del libro 0, Hoja GC 28934, Inscripci&oacute;n 1.</p></span>";
		}
?></td>
					</tr>
				</table></td>
			</tr>
<!--fin pie-->
		</table>
<!--fin factura-->
		</td>
		<td width="45" height="1146"></td>
	</tr>
</table>
<?php
	}
} // fin get[id]>0

?>
</body>
</html>

<?php

$file = fopen("facturas/factura$valor_factura.html", "w");
fwrite($file, 'this is some text');
fclose($file);

$doc = new DOMDocument();
$doc->formatOutput = true;
$doc->loadHTML('<link rel="StyleSheet" href="styles/styleNN.css" type="text/css"><link rel="StyleSheet" href="styles/style_maestro.css" type="text/css"><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Factura</title>
<head><style type="text/css">
*
{
	margin:0;
	padding:0;
}
.tabla1
{
	page-break-after:always;
}
.estilo1, .estilo2
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:9px;
	margin-bottom:12px;
	margin-right:10px;
}
.estilo1
{
	color:#1A9AD7;
}
.estilo2
{
	color:#54616A;
}
.estilo3
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:20px;
	font-weight:bold;
	color:#03144A;
}
.estilo4
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:9px;
	color:#1A9AD7;
	font-weight:bold;
}
.estilo5
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:10px;
	color:#000000;
}
.estilo6
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:11px;
	color:#000000;
}
/*	font:0.75em/1.6em Arial,Helvetica,sans-serif;
*/
.estilo7
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#000000;
	padding-left:2px;
	padding-right:2px;
}
.estilo8
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:8px;
	color:#066F92;
	font-weight:bold;
}
.estilo9, .estilo10
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:12px;
	color:#000000;
}
.estilo10
{
	font-weight:bold;
}
.estilo11
{
	font-family:Tahoma,Verdana,sans-serif;
	font-size:9px;
	color:#066F92;
	font-weight:bold;
}
</style></head><body><table width="830" height="1172" border="0" cellpadding="0" cellspacing="0" class="tabla1">
	<tr>
		<td width="28" height="26" bgcolor="#018ED3"></td>
		<td width="802" height="26" colspan="3"></td>
	</tr>
	<tr valign="top">
		<td width="28" height="1146">
		<table width="28" height="1146" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="28" height="51"></td>
			</tr>
			<tr>
				<td width="28" height="1095" bgcolor="#018ED3"></td>
			</tr>
		</table>
		</td>
		<td width="59" height="1146"></td>
		<td width="698" height="1146" rowspan="2">
<!--inicio factura-->
		<table width="698" height="1146" border="0" cellpadding="0" cellspacing="0">
<!--inicio cabecera-->
			<tr height="195">
				<td><table width="698" height="195" border="0" cellpadding="0" cellspacing="0">
					<tr valign="top" height="195">
<!-- inicio col sup izq-->
						<td width="279"><table width="279" height="195" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td height="51"><img src="http://www.hidrosolcanarias.com/images/fact_nueva_01.jpg" /></td>
							</tr>
							<tr>
								<td height="58"><span class="estilo3">'.$contenido1.'</span></td>
							</tr>
							<tr valign="top">
								<td height="86"><table width="279" height="80" cellpadding="0" cellspacing="0">
									<tr>
										<td height="17" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">&nbsp;&nbsp;OBSERVACIONES</span></td>
									</tr>
									<tr valign="top">
										<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0;"><span class="estilo5">'.$observaciones.'</span></td>
									</tr>
								</table></td>
							</tr>
						</table></td>
<!-- fin col sup izq-->
						<td width="139" align="right"><p class="estilo1">HIDROPRESI&Oacute;N</p><p class="estilo2">PISCINAS</p><p class="estilo1">CLIMATIZACI&Oacute;N</p><p class="estilo2">ENERG&Iacute;A SOLAR</p><p class="estilo1">AIRE ACONDICIONADO</p><p class="estilo2">CALEFACCI&Oacute;N</p></td>
<!-- inicio col sup dch-->
						<td width="280"><table width="280" height="190" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td colspan="2" height="17" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">&nbsp;&nbsp;DATOS CLIENTE</span></td>
							</tr>
							<tr valign="top">
								<td colspan="2" height="125" style="border-style:solid; border-left-width:2px; border-left-color:#1A9AD7; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:0; padding:2px;"><span class="estilo6"> N� CLIENTE: '.$num_cliente.'<br />
 CLIENTE: '.$razonsocialcliente.'<br />
 NIF/CIF: '.$nifcliente.'<br />
 DOMICILIO: '.$direccioncliente.'<br />
 LOCALIDAD: '.$poblacioncliente.'<br />
 CP: '.$cpcliente.'<br />
 TLF: '.$tlfcliente.'<br />
 FAX: '.$faxcliente.'</span></td>
							</tr>
							<tr>
								<td width="140" height="17" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">&nbsp;&nbsp;FECHA</span></td>
								<td width="140" height="17" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;FACTURA N&ordm;</span></td>
							</tr>
							<tr height="21" align="center">
								<td width="140" style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0;"><span class="estilo6">'.$dia.'/'.$mes.'/'.$ano.'
</span></td>
								<td width="140" style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;"><span class="estilo6">'.$num_factura.'</span></td>
							</tr>
						</table></td>
<!-- fin col sup dch-->
					</tr>
				</table></td>
			</tr>
<!--fin cabecera-->
<!--inicio cuerpo-->
			<tr valign="top">
				<td><table width="698" height="843" border="0" cellpadding="0" cellspacing="0">
					<tr height="17" align="center">
						<td width="18%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">ART&Iacute;CULO</span></td>
						<td width="38%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">DESCRIPCI&Oacute;N</span></td>
						<td width="8%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">CANTIDAD</span></td>
						<td width="10%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">PRECIO</span></td>
						<td width="6%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">DTO.</span></td>
						<td width="10%" style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">IMPORTE</span></td>
					</tr>'.$contenido2.'	<tr valign="top">
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
						<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; border-top-width:0; border-left-width:0;">&nbsp;</td>
					</tr>
				</table></td>
			</tr>
<!--fin cuerpo-->
<!--inicio pie-->
			<tr>
				<td><table width="100%" height="108" border="0" cellpadding="0" cellspacing="0">
					<tr valign="top">
						<td width="218"><br /><br /><br /><span class="estilo8">HIDROSOL CANARIAS, S.L.<br />C/De la Cizalla M3 37-P3N <br />Tel. 928 184 180 � Fax. 928 180 729 CIF: B 35724814 <br>www.hidrosolcanarias.com&nbsp;&nbsp;info@hidrosolcanarias.com</span></td>
						<td style="padding-top:5px;" align="center">
						<table width="480" border="0" cellpadding="0" cellspacing="0">
							<tr align="center" height="17">
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7;"><span class="estilo4">&nbsp;&nbsp;DTO PP</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;BASE IMPONIBLE</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;IGIC</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;CUOTA</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;TOTAL BRUTO</span></td>
								<td style="border-style:solid; border-width:2px; border-color:#1A9AD7; background-color:#D7E9F7; border-left-width:0;"><span class="estilo4">&nbsp;&nbsp;TOTAL NETO</span></td>
							</tr>
							<tr height="35" align="center">
								<td style="border-style:solid; border-left-width:2px; border-left-color:#1A9AD7; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9">'.$dto_pp.'</span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9">'.$base_imponible.'</span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9">'.$igic.'</span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9">'.$cuota.'</span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo9">'.$total_bruto.'</span></td>
								<td style="border-style:solid; border-left-width:0; border-right-width:2px; border-right-color:#1A9AD7; border-top-width:0; border-bottom-width:2px; border-bottom-color:#1A9AD7; padding:2px;"><span class="estilo10">'.$total_neto.'</span></td>
							</tr>
						</table>'.$contenido3.'</td>
					</tr>
				</table></td>
			</tr>
<!--fin pie-->
		</table>
<!--fin factura-->
		</td>
		<td width="45" height="1146"></td>
	</tr>
</table></body></html>');
$doc->saveHTMLFile("facturas/factura$valor_factura.html");


/*
$doc = new DOMDocument('1.0');
// queremos una impresi�n buena
$doc->formatOutput = true;

$root = $doc->createElement('html');
$root = $doc->appendChild($root);

$head = $doc->createElement('head');
$head = $root->appendChild($head);

$title = $doc->createElement('title');
$title = $head->appendChild($title);

$text = $doc->createTextNode('Factura');
$text = $title->appendChild($text);

echo 'Escrito: ' . $doc->saveHTMLFile("facturas/factura$valor_factura.html") . ' bytes'; // Escrito: 129 bytes
*/
?>

