<!DOCTYPE HTML>
<html class="full">

<head>
    <title>TACGestorcontenidos Hidrosol Canarias</title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="inicio/css/bootstrap.css">
    <link rel="stylesheet" href="inicio/css/font-awesome.css">
    <link rel="stylesheet" href="inicio/css/icomoon.css">
    <link rel="stylesheet" href="inicio/css/styles.css">
    <link rel="stylesheet" href="inicio/css/mystyles.css">
    <script src="inicio/js/modernizr.js"></script>

    <link rel="stylesheet" href="inicio/css/switcher.css" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/bright-turquoise.css" title="bright-turquoise" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/turkish-rose.css" title="turkish-rose" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/salem.css" title="salem" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/hippie-blue.css" title="hippie-blue" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/mandy.css" title="mandy" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/green-smoke.css" title="green-smoke" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/horizon.css" title="horizon" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/cerise.css" title="cerise" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/brick-red.css" title="brick-red" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/de-york.css" title="de-york" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/shamrock.css" title="shamrock" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/studio.css" title="studio" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/leather.css" title="leather" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/denim.css" title="denim" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="inicio/css/scarlet.css" title="scarlet" media="all" />
</head>

<body class="full">


    <div class="global-wrap">
	
	<!--
        <div class="demo_changer" id="demo_changer">
            <div class="demo-icon fa fa-sliders"></div>
            <div class="form_holder">
                <div class="line"></div>
                <p>Color Scheme</p>
                <div class="predefined_styles" id="styleswitch_area">
                    <a class="styleswitch" href="?default=true" style="background:#ED8323;"></a>
                    <a class="styleswitch" href="#" data-src="bright-turquoise" style="background:#0EBCF2;"></a>
                    <a class="styleswitch" href="#" data-src="turkish-rose" style="background:#B66672;"></a>
                    <a class="styleswitch" href="#" data-src="salem" style="background:#12A641;"></a>
                    <a class="styleswitch" href="#" data-src="hippie-blue" style="background:#4F96B6;"></a>
                    <a class="styleswitch" href="#" data-src="mandy" style="background:#E45E66;"></a>
                    <a class="styleswitch" href="#" data-src="green-smoke" style="background:#96AA66;"></a>
                    <a class="styleswitch" href="#" data-src="horizon" style="background:#5B84AA;"></a>
                    <a class="styleswitch" href="#" data-src="cerise" style="background:#CA2AC6;"></a>
                    <a class="styleswitch" href="#" data-src="brick-red" style="background:#cf315a;"></a>
                    <a class="styleswitch" href="#" data-src="de-york" style="background:#74C683;"></a>
                    <a class="styleswitch" href="#" data-src="shamrock" style="background:#30BBB1;"></a>
                    <a class="styleswitch" href="#" data-src="studio" style="background:#7646B8;"></a>
                    <a class="styleswitch" href="#" data-src="leather" style="background:#966650;"></a>
                    <a class="styleswitch" href="#" data-src="denim" style="background:#1A5AE4;"></a>
                    <a class="styleswitch" href="#" data-src="scarlet" style="background:#FF1D13;"></a>
                </div>
                <div class="line"></div>
                <p>Layout</p>
                <div class="predefined_styles"><a class="btn btn-sm" href="#" id="btn-wide">Wide</a><a class="btn btn-sm" href="#" id="btn-boxed">Boxed</a>
                </div>
                <div class="line"></div>
                <p>Background Patterns</p>
                <div class="predefined_styles" id="patternswitch_area">
                    <a class="patternswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/patterns/binding_light.png);"></a>
                    <a class="patternswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/patterns/binding_dark.png);"></a>
                    <a class="patternswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/patterns/dark_fish_skin.png);"></a>
                    <a class="patternswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/patterns/dimension.png);"></a>
                    <a class="patternswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/patterns/escheresque_ste.png);"></a>
                    <a class="patternswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/patterns/food.png);"></a>
                    <a class="patternswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/patterns/giftly.png);"></a>
                    <a class="patternswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/patterns/grey_wash_wall.png);"></a>
                    <a class="patternswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/patterns/ps_neutral.png);"></a>
                    <a class="patternswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/patterns/pw_maze_black.png);"></a>
                    <a class="patternswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/patterns/pw_pattern.png);"></a>
                    <a class="patternswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/patterns/simple_dashed.png);"></a>
                </div>
                <div class="line"></div>
                <p>Background Images</p>
                <div class="predefined_styles" id="bgimageswitch_area">
                    <a class="bgimageswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/switcher/bike.jpg);" data-src="http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/backgrounds/bike.jpg"></a>
                    <a class="bgimageswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/switcher/flowers.jpg);" data-src="http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/backgrounds/flowers.jpg"></a>
                    <a class="bgimageswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/switcher/wood.jpg);" data-src="http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/backgrounds/wood.jpg"></a>
                    <a class="bgimageswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/switcher/taxi.jpg);" data-src="http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/backgrounds/taxi.jpg"></a>
                    <a class="bgimageswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/switcher/phone.jpg);" data-src="http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/backgrounds/phone.jpg"></a>
                    <a class="bgimageswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/switcher/road.jpg);" data-src="http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/backgrounds/road.jpg"></a>
                    <a class="bgimageswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/switcher/keyboard.jpg);" data-src="http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/backgrounds/keyboard.jpg"></a>
                    <a class="bgimageswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/switcher/beach.jpg);" data-src="http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/backgrounds/beach.jpg"></a>
                    <a class="bgimageswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/switcher/street.jpg);" data-src="http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/backgrounds/street.jpg"></a>
                    <a class="bgimageswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/switcher/nature.jpg);" data-src="http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/backgrounds/nature.jpg"></a>
                    <a class="bgimageswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/switcher/bridge.jpg);" data-src="http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/backgrounds/bridge.jpg"></a>
                    <a class="bgimageswitch" href="#" style="background-image: url(http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/switcher/cameras.jpg);" data-src="http://remtsoy.com/tf_templates/traveler/demo_v1_6/img/backgrounds/cameras.jpg"></a>
                </div>
                <div class="line"></div>
            </div>
        </div>
		-->
		
		
        <div class="full-page">
            <div class="bg-holder full">
                <div class="bg-mask"></div>
                <div class="bg-img" style="background-image:url(documentos/_K4A0099_res.jpg);"></div>
                <div class="bg-holder-content full text-white">
                    <a class="logo-holder" href="index.php">
                        <img src="images/logo_hidrosol.png" alt="TACGestorcontenidos Hidrosol Canarias" title="TACGestorcontenidos Hidrosol Canarias" />
                    </a>
                    <div class="full-center">
                        <div class="container">
                            <div class="row row-wrap" data-gutter="60">
                                <div class="col-md-4">
								<!--
                                    <h3 class="mb15">New To Traveler?</h3>
                                    <form>
                                        <div class="form-group form-group-ghost form-group-icon-left"><i class="fa fa-user input-icon input-icon-show"></i>
                                            <label>Full Name</label>
                                            <input class="form-control" placeholder="e.g. John Doe" type="text" />
                                        </div>
                                        <div class="form-group form-group-ghost form-group-icon-left"><i class="fa fa-envelope input-icon input-icon-show"></i>
                                            <label>Emai</label>
                                            <input class="form-control" placeholder="e.g. johndoe@gmail.com" type="text" />
                                        </div>
                                        <div class="form-group form-group-ghost form-group-icon-left"><i class="fa fa-lock input-icon input-icon-show"></i>
                                            <label>Password</label>
                                            <input class="form-control" type="password" placeholder="my secret password" />
                                        </div>
                                        <input class="btn btn-primary" type="submit" value="Sign up for Traveler" />
                                    </form>
								-->
                                </div>
                                <div class="col-md-4">
                                    <div class="visible-lg">
                                        <h3>Sistema de Gestión de contenidos de Hidrosol Canarias</h3>
                                        <p>Bienvenido al sistema de gesti&oacute;n</b>.</p>
									 <p>Si tiene alguna duda con el funcionamiento de este sistema, puede consultarla en atencioncliente@tac7.com</p><br><br>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h3 class="mb15">Login<br><br></h3>
                                    <form action=modules.php method=post>
									<input type=hidden name=mod value=inicio>
									<input type=hidden name=file value=login>
                                        <div class="form-group form-group-ghost form-group-icon-left"><i class="fa fa-user input-icon input-icon-show"></i>
                                            <label>Usuario</label>
                                            <input name="login" id="login" class="form-control" placeholder="usuario.." type="text" />
                                        </div>
                                        <div class="form-group form-group-ghost form-group-icon-left"><i class="fa fa-lock input-icon input-icon-show"></i>
                                            <label>Contraseña</label>
                                            <input name="passwd" id="passwd" class="form-control" type="password" placeholder="contraseña.." />
                                        </div>
                                        <input class="btn btn-primary" type="submit" value="Entrar" /><br><br>
                                    </form>
									
                                </div>

                            </div>
                        </div>
                    </div>
                    <ul class="footer-links">
                        <li><a target="_blank" href="http://www.tac7.com">TAC7: Innovacion en las Organizaciones</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>



        <script src="inicio/js/jquery.js"></script>
        <script src="inicio/js/bootstrap.js"></script>
        <script src="inicio/js/slimmenu.js"></script>
        <script src="inicio/js/bootstrap-datepicker.js"></script>
        <script src="inicio/js/bootstrap-timepicker.js"></script>
        <script src="inicio/js/nicescroll.js"></script>
        <script src="inicio/js/dropit.js"></script>
        <script src="inicio/js/ionrangeslider.js"></script>
        <script src="inicio/js/icheck.js"></script>
        <script src="inicio/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="inicio/js/typeahead.js"></script>
        <script src="inicio/js/card-payment.js"></script>
        <script src="inicio/js/magnific.js"></script>
        <script src="inicio/js/owl-carousel.js"></script>
        <script src="inicio/js/fitvids.js"></script>
        <script src="inicio/js/tweet.js"></script>
        <script src="inicio/js/countdown.js"></script>
        <script src="inicio/js/gridrotator.js"></script>
        <script src="inicio/js/custom.js"></script>
        <script src="inicio/js/switcher.js"></script>
    </div>
</body>

</html>



