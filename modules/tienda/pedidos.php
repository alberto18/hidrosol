
<div id="page" class="common-home"> 

  
  <!-- main-container -->
  <div class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
        <section class="col-sm-9 wow bounceInUp animated">
          <div class="col-main">
            <div class="my-account">
              <div class="page-title">
                <h2>Mis pedidos</h2>
              </div>
              <div class="dashboard">
                <div class="welcome-msg"> <strong>Hola, <?php echo ucfirst($_SESSION['tienda']['login']); ?></strong>
                  <p>Desde aqu&iacute; tiene la opci&oacute;n de ver la actividad de sus pedidos recientes y actualizar su informaci&oacute;n personal. 
                </div>
                <div class="recent-orders">
                  <!--<div class="title-buttons"><strong>Ordenes recientes</strong> <a href="#">Ver todas </a> </div>-->
                  <div class="table-responsive">
                   
						<?php 
						
							include ("index_pedidos_new.php");
						?>
                        
                        
                  </div>
                <!--</div>-->
                
              </div>
            </div>
          </div>
        </section>
       <!-- <aside class="col-right sidebar col-sm-3 wow bounceInUp animated">
          <div class="block block-account">
            <div class="block-title">Mi cuenta</div>
            <div class="block-content">
              <ul>
                <li class="current"><a>Account Dashboard</a></li>
                <li><a href="http://demo.themessoft.com/computerstore/customer/account/edit/">Informaci&oacute;n</a></li>
                <li><a href="http://demo.themessoft.com/computerstore/customer/address/">Direcciones</a></li>
                <li><a href="http://demo.themessoft.com/computerstore/sales/order/history/">Mis pedidos</a></li>
                <li><a href="http://demo.themessoft.com/computerstore/sales/billing_agreement/">Billing Agreements</a></li>
                <li><a href="http://demo.themessoft.com/computerstore/sales/recurring_profile/">Recurring Profiles</a></li>
                <li><a href="http://demo.themessoft.com/computerstore/review/customer/">My Product Reviews</a></li>
                <li><a href="http://demo.themessoft.com/computerstore/tag/customer/">Mis Tags</a></li>
                <li><a href="http://demo.themessoft.com/computerstore/wishlist/">Mi lista de deseados</a></li>
                <li><a href="http://demo.themessoft.com/computerstore/downloadable/customer/products/">Mis descargas</a></li>
                <li class="last"><a href="http://demo.themessoft.com/computerstore/newsletter/manage/">Newsletter Subscriptions</a></li>
              </ul>
            </div>
          </div>
         
        </aside>-->
      </div>
    </div>
  </div>
  <!--End main-container --> 
  
</div>



