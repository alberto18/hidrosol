

<div id="main">               
<section>
<div class="blog-wrapper blog-sidebar">
    <div class="container">
        <div class="row">
		<div class="darker-stripe">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb">
                        <li>
                            <a href="modules.php?mod=portal&file=listado_productos">Tienda</a><span class="icon-chevron-right"></span>
                        </li>
                     
                        <li>
                            Mis Pedidos
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
		 <div class="span12">
			<div class="title-area">
				<h1 class="inline"><span class="light"><?php echo "Mis Pedidos";?></span></h1>
			</div>
         </div>
		
		
		
         <div class="col-sm-9 wow bounceInup animated">
			<div id="imprimir">
                <article class="post" style="margin-bottom:0px;">

                      
					
                      
						
						
                 
                       

<?php 

$user_id = $_SESSION['tienda']['id'];


if (($_SESSION['tienda']['login']=="")) {
echo '<meta http-equiv="refresh" content="0;URL=modules.php?mod=portal&file=index">';

}
 
 
?>
 
<div class="content">
	<div class="container_16">
	<?php
	//include ("izquierda2_new.php");
	?>
       
<div class="capa_contenidos">

            
<?php

   /*include("obtener_miperfil.php");
   
   include("obtener_opciones.php");*/
   
    include("abrir_db_erp.php");
?>
            

<?php

// CONFIGURACION GENERAL DEL INDEX_METASCRIPT_NEW
// Titulo que aparece en la parte superior del script
$titulo = "GESTION DE PEDIDOS";
// Titulo que aparece en la pestaña del navegador
$titulo_pagina = "GESTION DE PEDIDOS";
echo "<script>document.title = \"".$titulo_pagina."\";</script>";
// Texto que aparece en el boton de crear
//$titulo_boton_crear = "CREAR NUEVO PEDIDO";
// Puedes cambiar la apariencia de los botones en el maestro_formulario indicando su clase.
// Por ejemplo: $clase_boton_crear = " class='buttonmario mediummario orangemario' "; 
$clase_boton_crear = " class='buttonmario mediummario orangemario' ";
$clase_boton_buscar = " class='buttonmario mediummario orangemario' ";
$clase_boton_guardar = " class='buttonmario mediummario orangemario' ";
$clase_boton_volver  = " class='buttonmario mediummario orangemario' ";
$clase_boton_confirmar_borrado  = " class='buttonmario mediummario red' ";
// Permitir_creacion_de_registros: Si esta a 1, se mostrara al usuario la posibilidad de visualizar el boton de crear
$permitir_creacion_de_registros = 0;
$ocultar_imprimir_listado_final_rejilla = 1;
$modulo_script = "tienda";
// Direccion en la que se encuentra el script
$enlacevolver = "modules.php?mod=".$modulo_script."&file=";
// Nombre del script
$script = "index_pedidos_new";
$script_lineas = "index_pedidos_lineas_new";
// Nombre de la tabla
$tabla = "pedidos_web_t"; // OJO, la clave principal se debe llamar id

// CONFIGURACION DE LOS CAMPOS EN LOS FORMULARIOS
// Campos con los que se trabajara en el insert y modify. En la plantilla deben aparecer como [campo]
$campos_col1 = array('numero_pedido','fecha_pedido','estado_pedido','cliente_id','direccion_entrega','horario_entrega','franja_horaria','fecha_entrega');

// Nombres col1 ya no se usa, dado que es en la plantilla del formulario donde se colocan las etiquetas de los campos
//$nombres_col1 = array('Referencia','Nombre','Categoria','Descripci&oacute;n','Marca','Precio','Estado de publicaci&oacute;n','En primera p&aacute;gina?');

// Ayuda para que el usuario tenga mas informacion sobre el campo. En la plantilla apareceran como [campo_ayuda]
$ayudas_col1 = array();

// Definir que campos son onbligatorios (colocando 'on')
$campos_col1_obligatorios = array('on');

// Definir que tipo de dato se puede escribir (usando los pattern de HTML5). Mas info: http://html5pattern.com/ 
$campos_col1_mascaras = array('','','','','','','','');

// Definir que campos seran de solo lectura (no se puede escribir dentro de los mismos). OJO: El textedit no puede solo lectura
$campos_col1_readonly = array('','','','','','','','','');

// Tipos de los campos. Mas info en la ayuda de: maestro_formulario.php
$tipos_col1  = array('mostrar_dato_text;250','datetime3','select;maestro_estados_pedidos_web_t;nombre;id','select;clientes_web_t;nombre;id','select;direcciones_clientes_web_t;direccion;id', 'mostrar_dato_text;250', 'select;maestro_franjas_horarias_t;nombre;id','datetime3');
//$tipos_col1  = array('datetime3','text;600','textarea;400;100','textarea;400;100');

// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
//$filtro_noc_para_insert = " noc='$noc', ";
// campos_automaticos_para_insert: Es posible que el insert requiera campos automaticos del tipo fecha=now()
// se especificarian aqui: $campos_automaticos_para_insert = " fecha=now(), ";
$campos_automaticos_para_insert = "";

// Campo para la busqueda
$campo_busqueda = "id desc";

// PLANTILLAS VISUALES
// IMPORTANTE: Los campos de la plantilla deben coincidir con los del metascript
$plantilla_insercion = "index_pedidos_new.plantilla.php";
if ($plantilla_insercion != "") {
  $fichero_absoluto =  "modules/tienda/" . $plantilla_insercion;
  //echo $fichero_absoluto;
  if (file_exists($fichero_absoluto)) {
   $gestor = fopen($fichero_absoluto, "r");
   $contenido_plantilla_insercion = fread($gestor, filesize($fichero_absoluto));
   fclose($gestor);
  }
}

// CONFIGURACION DEL LISTADO DE REGISTRO
// Si se desea visualizar el listado o no (poner 1 o 0)
$visualizar_listado = 1;
// Campos, por orden, para el listado inicial de registros
$campos_listado = array ('id','fecha_pedido','estado_pedido','direccion_entrega','recoger_en');
// Nombres para el encabezado de la tabla del listado de registros
$nombres_listado = array ('','Fecha pedido','Estado','Entregar en','Recoger y pagar en');
// Decodificacion si existiese de los campos
$campos_listado_decod = array ('','si;datetime','si;maestro_estados_pedidos_web_t;nombre;id','si;direcciones_clientes_web_t;direccion;id','si;maestro_delegacion_t;direccion;id');
// Hoja de estilos para la tabla
$clase_tabla_listado = "class='table table-bordered table-striped table-condensed table-hover'";
// Para proyectos multiempresa. En caso contrario, comentar la siguiente linea.
$filtro_noc_para_listado = " and cliente_id='$user_id'";
// Para el paginado
$registros_por_pagina = "30";
// Estilo del paginado
$clase_paginado = "style='color:#000000; text-decoration:none;'";
// Formato paginado: reducido o no (Ej reducido: « [1] ...[3] [4] [5] ...[10] »)
$paginado_reducido = 1;

// Color de los registros
/*
$nombre_funcion_bgcolor_por_registro = "funcion_bgcolor_registro";
function funcion_bgcolor_registro($valor_id) {
   $color_naranja = "#EFB334";
   $color_gris_oscuro = "#C1BFBB";
   // Vemos el estado del recibo
   $tipo_resultado_ultima_gestion_id = obtener_campo('tipo_resultado','recibos_gestiones_t','left join recibos_t on recibos_gestiones_t.recibo_id=recibos_t.id','recibos_t.id='.$valor_id.' order by recibos_gestiones_t.fecha_alta desc limit 1');
   if ($tipo_resultado_ultima_gestion_id != "") { return  $color_naranja; }
   //echo "tipo_resultado: $tipo_resultado_ultima_gestion_id";
}
*/


// Filtros iniciales al listado de registros 
// Puedes definir una consulta inicial para el listado de registros, de forma que se apliquen filtros
// para que no se vean todos los registros que existen en la tabla en funcion de cualquier condicion que definas aqui.
// PONER LA VIA DE COBRO QUE CORRESPONDA:  and via_cobro=
$filtros_iniciales = " and cliente_id='{$_SESSION['tienda']['id']}'";

// Otro Ejemplo
/*
if ($hoja_ruta == 1) {
	$filtros_iniciales = " and (empresa_servicio=1 or empresa_servicio=2 or empresa_servicio=6) and (via_cobro=1) and recibos_gestiones_t.user_destino_id='$user_id' ";
	$left_join_inicial = " left join recibos_gestiones_t on $tabla.id=recibos_gestiones_t.recibo_id  ";
} else {
	$filtros_iniciales = " and (empresa_servicio=1 or empresa_servicio=2 or empresa_servicio=6) and (via_cobro=1) ";
}
*/


// Cada linea de registro en la tabla del listado podra tener un conjunto de acciones. Las Mas normales son
// MODIFICAR y BORRAR. En determinado momento nos interesara que esas acciones aparezcan o no (en funcion de una
// condicion que se aplique a cada registro). En otros casos, las opciones son iguales para todos los registros.
// Por lo tanto, tenemos dos modos de trabajo.
// 1. MODO GENERICO: Las acciones son iguales para todos los registros.
// 2. MODO PERSONALIZADO: Las acciones dependen de la ejecucion de una funcion para cada registro.

// 2. MODO PERSONALIZADO
// Nombre de la funcion que se ejecutara para cada registro. Si se deja blanco el modo personalizado NO ESTA ACTIVO
$nombre_funcion_acciones_por_registro = "funcion_acciones_registro";

function funcion_acciones_registro($valor_id)
{
    global $tabla, $enlacevolver, $script;
    // me llega decodificado el id del registro en cuestion (en valor_id) pero tengo que codificarlo para ponerlo
    // en los enlaces
    $id_encript = base64_encode(base64_encode($valor_id));
    
    // Aqui se incluye el codigo necesario sobre el registro id_encript con el objetivo de sacar tantos echos
    // como acciones sean necesarias para este registro
    
    // Lo siguiente es un ejemplo de un proyecto determinado
    /*
    $array_datos_registro = obtener_multiples_campos(array("ano"),$tabla,""," $tabla.id='".$valor_id."' ","","","");
    // compruebo que la funcion solo me devuelve los datos de un registro
    if (count($array_datos_registro) == 1 && $array_datos_registro[0]['ano'] == date("Y"))
    {
            echo '
            <a href="'.$enlacevolver.$script.'&accion=formmodificar&id='.$id_encript.'"><img src="images/table_edit.png" title="Modificar" border="0" /></a> ';
    }
    */
    
    /* $cons_promo = "select * from pedidos_web_t where id = '$valor_id'";
    $res_promo = mysql_query($cons_promo) or die("La consulta fall&oacute;: $cons_promo " . mysql_error());
    while ($lin_promo = mysql_fetch_array($res_promo, MYSQL_ASSOC)) {
       $valor_promo = $lin_promo['promocion'];
    }*/
    
	/*if ($valor_promo == "1"){
    
     echo "<img src=\"images/ico_regalo.jpg\" width=\"20\" height=\"20\" title=\"En su primer pedido le llevamos una caja de 0,33 litros. Totalmente gratis.\" alt=\"En su primer pedido le llevamos una caja de 0,33 litros. Totalmente gratis.\" />";
    } */
   
      echo '
       <a class="buttonmario smallmario green" href="mis-articulos-'.substr($id_encript,0,-2).'"><i class="fugue-pencil" title="editar"></i> ARTICULOS</a>
       ';
       
    
}
// FIN 2. MODO PERSONALIZADO


// 1. MODO GENERICO
// acciones_por_registro: Es un array de botones u opciones que debe tener cada registro
// Cada registro tendra un conjunto de acciones. Fijarse como en el ID del registro, ponemos #ID#.
// maestro_formulario.php pondra el ID correcto.
$acciones_por_registro = array(); 
$condiciones_visibilidad_por_registro = array();

$acciones_por_registro[] = '<a class="buttonmario smallmario green" href="modules.php?mod=tienda&file='.$script_lineas.'&padre_id=#ID#"><i class="fugue-pencil" title="editar"></i> ART&Iacute;CULOS</a>';
$condiciones_visibilidad_por_registro[] = "";

/*
$acciones_por_registro[] = '<a class="buttonmario smallmario green" href="modules.php?mod=gestproject_new&file='.$script.'&accion=formmodificar&id=#ID#"><i class="fugue-pencil" title="editar"></i> MODIFICAR</a>';
$condiciones_visibilidad_por_registro[] = "";

$acciones_por_registro[] = '<a class="buttonmario smallmario green" href="modules.php?mod=gestproject_new&file='.$script.'&accion=formborrar&id=#ID#"><i class="fugue-cross-circle" title="borrar"></i> BORRAR</a>';
$condiciones_visibilidad_por_registro[] = "";

*/
// FIN 1. MODO GENERICO

// Procesos PRE y POST de las acciones formcrear, formmodificar, etc
// Ejemplo: El script proceso_pre_formcrear se ejecutara ANTES de que maestro_formulario.php genere el formulario
// Ejemplo: El script $proceso_post_accioncrear se ejcutara DESPUES de que maestro_formulario.php haya realizado
//          el insert de accioncrear
$proceso_pre_formcrear = "";
$proceso_pre_formmodificar = "";
$proceso_pre_accioncrear= "";
$proceso_pre_accionmodificar= "";
$proceso_post_accioncrear= "";
$proceso_post_accionmodificar= "";
//$proceso_pre_listado = "modules/gestproject_new/index_blog_new_proceso_pre_listado.php";
$proceso_post_listado = "";

// CONFIGURACION DEL PADRE
// Si este script no tiene padre, dejar el resto de los campos en blanco
// IMPORTANTE: el padre debe venir desde el script anterior en la forma: &padre_id=XYZ
//             donde XYZ es el valor con doble codificacion base64
//             Por ejemplo, si un script index_facturas_new es el padre de index_facturas_cobros_new
//             dentro de index_facturas_new hay que hacer un:
//             $id_encript = base64_encode(base64_encode($linea[id]));
//             En este caso, el index_facturas_new tendria un enlace a index_facturas_cobros_new de
//             la siguiente forma:
//             <a href=\"modules.php?mod=gestproject&file=index_cobros_facturas_new&padre_id=$id_encript\">COBROS</a>

// campo_padre: Nombre del campo padre en la tabla: Normalmente: producto_id, proyecto_id, etc
 $campo_padre = "cliente_id";
// Consulta para obtener el nombre del padre a visualizar en pantalla. Debe contener un 'as nombre'
 $consulta_nombre_padre = "select numero_pedido  as nombre from pedidos_web_t where id=#PADREID#;";
// Enlace al que volver al padre
 //$enlace_volver_a_padre = "modules.php?mod=tienda&file=index_pedidos_new&pag=0";
// Texto del enlace volver al padre
 //$texto_volver_a_padre = "Volver a presupuestos";

// CONFIGURACION DEL BUSCADOR
$habilitar_buscador = 1;
// estilos de los buscadores
$clase_buscador_input = "";
$clase_buscador_select = "";
$clase_buscador_checkbox = "";

$buscadores = array();
$buscadores[] = "input;numero_pedido";
//$buscadores[] = "select;cliente_id;clientes_web_t;nombre;id;buscar por cliente";
//$buscadores[] = "intervalo_fechas;fecha_creacion;;;;Fecha de creaci&oacute;n";
//$buscadores[] = "checkbox;primera_pagina;;;;Primera pagina";


// INCLUSION DEL MAESTRO_FORMULARIO.PHP
include ("maestro_formulario.php");

?>






				
						

								</div>
							</div>
						</div>
					</article><!-- /.post -->
                </div>
			</div>
			<div class="col-md-3">

			<?php
				include("dcha.php");	
			?>			
						
            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog-wrapper -->

</section>

</div>
