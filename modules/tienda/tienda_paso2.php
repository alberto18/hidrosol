

<div id="main">               
<section>
<div class="blog-wrapper blog-sidebar">
    <div class="container">
        <div class="row">
			<div class="darker-stripe">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb">
                        <li>
                            <a href="modules.php?mod=portal&file=listado_productos">Tienda</a><span class="icon-chevron-right"></span>
                        </li>
                  
                        <li>
                            Mi cesta
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
		<!-- <div class="span12">
			<div class="title-area">
				<h1 class="inline"><span class="light"><?php echo "Mi Perfil";?></span></h1>
			</div>
         </div>-->
		
			 <div class="col-sm-9 wow bounceInup animated">
				<div id="imprimir">
					<article class="post" style="margin-bottom:0px;">
						<div class="content">
							<div class="container_16">
								<div class="capa_contenidos">

 
 <?php

if (isset($_SESSION['tienda'])) {

?>



    
        <div class="cart wow bounceInUp animated">
          <div class="page-title">
		  <?php if (!isset($_SESSION['tienda']['cesta'])){
			?>
				  <h2>No dispone de articulos en la cesta</h2>
			<?php
			}
			else{
			?>
			  <h2>Cesta</h2>
			  <?php
			}
			?>
          
          </div>
          <div class="table-responsive">
            <form method="post" action="modules.php?">
             <input type="hidden" value="Vwww7itR3zQFe86m" name="form_key">
              <fieldset>
                <table class="data-table cart-table" id="shopping-cart-table">
                  <colgroup>
                  <col width="1">
                  <col>
                  <col width="1">
                  <col width="1">
                  <col width="1">
                  <col width="1">
                  <col width="1">
                  </colgroup>
                



<?php
				if( $_SESSION['tienda']['cesta']['total_precio'] < 0){
				    
				echo "<script>alert('No ha seleccionado ningun producto. No puede continuar');</script>";
				
				?>
					<form method=get name=form_volver id=form_volver action=modules.php>
					<input type=hidden name=mod value=tienda>
					<input type=hidden name=file value=listado_productos>
					<input type=hidden name=debug value=1>
					</form>
					<script>document.form_volver.submit();</script>    
				
				<?php    
				}

		
	
	
				
				$total_pedido = 0.0;
				foreach($_SESSION['tienda']['articulos'] as $id => $articulo){
					$nombre_producto = $articulo['nombre'];
					$producto_id = $id;
					$precio = $articulo['precio_web'];
					$nombre_fichero = $articulo['img'];
					$cantidad = $articulo['cantidad'];
					$total_parcial = $cantidad * $precio;
					$total_pedido += $cantidad * $precio;
					$categoria= $articulo['categoria_id'] ;
					$producto_id = base64_encode(base64_encode($producto_id));		
					//$categoria = base64_encode(base64_encode($categoria));	
					$total_pedido_coma=str_replace(".",",",$total_pedido);
					$total_parcial_coma=str_replace(".",",",$total_parcial);
					$precio_coma=str_replace(".",",",$precio);

				?>
					


				<tbody>
                    <tr class="first odd">
					  
					  <td class="image"><a class="product-image" title="Imagen de <?php echo $nombre_producto ?>" href="modules.php?mod=portal&file=ver_productos&categoria=<?php echo $categoria ?>&id=<?php echo $producto_id?>"><img width="75" alt="Imagen de <?php echo $nombre_producto ?>" src="<?php echo $nombre_fichero ?>"></a></td>
                      <td><h2 class="product-name"> <a href="modules.php?mod=portal&file=ver_productos&id=<?php echo $producto_id ?>=&categoria=<?php echo $categoria ?>"><?php echo $nombre_producto ?></a> </h2></td>
                     <!-- <td class="a-center"><a title="Editar" class="edit-bnt" href="modules.php?mod=portal&file=ver_productos&id=<?php echo $producto_id ?>=&categoria=<?php echo $categoria ?>"></a></td>-->
                      <td class="a-right"><span class="cart-price"> <span class="price"><?php echo $precio_coma ?> &#8364;</span> </span></td>
                      
	
					  <td class="a-center movewishlist  wrapper-qty"><input maxlength="12" type="text" class="input-text qty" title="Cantidad" size="4" value="<?php echo $cantidad ?>" name="qty"></td>
					 
					  <input type="hidden" name="id" id="id" value="<?php echo $producto_id?>">
					  <input type="hidden" name="categoria" id="categoria" value="<?php echo $categoria ?>">
					
					 
					 
					  
					  
					  
                      <td class="a-right movewishlist"><span class="cart-price"> <span class="price"><?php echo $total_parcial_coma ?> &#8364;</span> </span></td>
					  

					 <!-- <td type="button" class="update1_cart_btn" class="a-center last btn-edit "><a  title="Editar cantidad de elementos" ><i class="icon-pencil"></i><span class="hidden">Editar</span></a></td>-->
				
					<td type="button" class="a-center last btn-edit ">
					<a class="update1_cart_btn" title="Editar cantidad de elementos" ><i class="icon-pencil"></i><span class=""></span></a>
					</td>
				
					
					  
					  
                    					
					 <td class="a-center last"><a class="button remove-item" title="Remove item" href="modules.php?mod=tienda&file=eliminar_articulo_cesta&id=<?php echo base64_encode(base64_encode($id));?>&cantidad=<?php echo $articulo['cantidad'];?>"><span><span>Eliminar producto</span></span></a></td>
					  
                    </tr>
                    
                  </tbody>

				<?php
				  }
				  ?>

				
                  <tfoot>
                    <tr class="first last">
					
					
					
                      <td class="a-right last" colspan="50">
					  <button  class="button" title="Continuar comprando" type="button"><a href="modules.php?mod=portal&file=listado_productos&categoria=0">Continuar comprando</a></button>
                      <?php if (isset($_SESSION['tienda']['cesta'])){
			?> 
                      <button  class="button btn-empty" title="Limpiar cesta" value="empty_cart" name="update_cart_action" type="submit"><span><a href="modules.php?mod=tienda&file=eliminar_cesta">Limpiar cesta</a></span></button></td>
					  <?php
					  }
					  ?>
                    </tr>
                  </tfoot>
				  
				  
				  
				  
                  
				  
				
                </table>
              </fieldset>
            </form>
          </div>
          <!-- BEGIN CART COLLATERALS -->
          <div class="cart-collaterals row">
            <div class="col-sm-4">
              <div class="shipping">
            
              </div>
            </div>
            <div class="col-sm-4">
            
            </div>
            <div class="col-sm-4">
              <div class="totals">
                <h3>Coste total</h3>
                <div class="inner">
                  <table class="table shopping-cart-table-total" id="shopping-cart-totals-table">
                    <colgroup>
                    <col>
                    <col width="1">
                    </colgroup>
                    <tfoot>
                      <tr>
                        <td colspan="1" class="a-left" style=""><strong>Precio Total</strong></td>
                        <td class="a-right" style=""><strong><span class="price"><?php echo $total_pedido_coma ?> &#8364;</span></strong></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <tr>
                        <td colspan="1" class="a-left" style=""> Subtotal (Impuestos incluidos) </td>
                        <td class="a-right" style=""><span class="price"><?php echo $total_pedido_coma ?>&#8364;</span></td>
                      </tr>
                    </tbody>
                  </table>
                  <ul class="checkout">
				   <!-- <form name="formulario" action="modules.php?mod=tienda&file=tienda3" method="post">
					<button class="button btn-proceed-checkout"><a href="javascript:history.back()" type="button">Volver</a>
					</form>	-->
                    <li>
                      <a href="modules.php?mod=tienda&file=tienda_paso3"><button class="button btn-proceed-checkout" title="Proceder al pago" type="button"><span>Continuar</span></button></a>
					  
					  
                    </li>
                   <!-- <li><a title="Checkout with Multiple Addresses" href="multiple_addresses.html">Enviar a distintas direcciones</a> </li>-->
                    <br>
                  </ul>
                </div>
                <!--inner--> 
              </div>
            </div>
          </div>
          
          <!--cart-collaterals--> 
          
        </div>
      </div>
   
<!--End feature--> 

<!-- End Footer -->
<?php
 
}

else{
  
echo "<br><br><p align=\"center\">No puede realizar ning&uacute;n pedido si no est&aacute; validado.</p><br><br>";  
  
}
?>


 		
						

									</div>
								</div>
							</div>
						</article><!-- /.post -->
						</div>
						 <?php
							include("dcha.php");	
						?>	

					</div>
					<div class="col-md-3">
					</div>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.blog-wrapper -->
	</section>
</div>
