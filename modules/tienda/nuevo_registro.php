 
 
 <?php 
	$id_cod=$_REQUEST[id];
 ?>
	<!-- main-container -->
  <div class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
	  
       <div class="darker-stripe">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb">
                        <li>
                            <a href="modules.php?mod=portal&file=listado_productos&categoria=0">Tienda</a> <span class="icon-chevron-right"></span>
                        </li>
                      
                        <li>
                            Registro
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
        <section class="col-sm-9 wow bounceInUp animated">
          <div class="col-main">
            <div class="page-title">
              <h2>Registro</h2>
            </div>
			<form action="modules.php?mod=tienda&file=comprobar_registro&id=<?php echo $id_cod ?>" method="POST" enctype="multipart/form-data" >
            <div class="static-contain">
              <fieldset class="group-select">
                <ul>
                  <li>
                    <div class="customer-name">
                      <div class="input-box name-nombre">
                        <label for="nombre">Nombre completo<span class="required">*</span></label>
                        <br>
                        <input type="text" id="nombre" name="nombre" value="" title="Nombre" class="input-text " required>
                      </div>
				
					  <div class="input-box email">
						  <label for="inputEmailRegister">Email <span class="required">*</span></label>
						  <br>
						<input type="text" name="email" id="inputEmailRegister" value="" title="Email" class="input-text validate-email"  required>
                      </div>
					  
					  <div class="input-box">
						<label for="billing:telefono">Tel&eacute;fono </label>
						<br>
						<input type="text" name="telefono" id="telefono" value="" title="Tel&eacute;fono / Movil" class="input-text"  required>
                      </div>
					  
					  <div class="input-box name-Login">
							<label for="login">Nombre de usuario <span class="required">*</span></label>
							<br>
							<input type="text" id="login" name="login" value="" title="Nombre de usuario"  class="input-text " required>
                      </div>
					   
                     
                    </div>
                  </li>
                 
                  <li>
				  <div class="customer-name">
                      <div class="input-box name-nombre">
                    <label for="password">Contrase&#241;a <span class="required">*</span></label>
                    <br>
                    <input type="password" title="Contrase&#241;a" name="passwd" id="passwd" value="" class="input-text required-entry" required>
					</div>
					</div>
                  </li>
                
                
                </ul>
				<div class="form-group">
				
					<?php
						echo '<img src="extras/cool-php-captcha-0.3.1/captcha.php" id="captcha" /><br/>';
					?>
					<!-- CHANGE TEXT LINK -->
					<a href="#" onclick="document.getElementById('captcha').src='extras/cool-php-captcha-0.3.1/captcha.php?'+Math.random();document.getElementById('captcha-form').focus();"
					id="change-image">Cambiar la imagen</a> <span class="required">*</span> <br/><br/>
					  <label for="comment">Escriba el texto de la imagen <em class="required">*</em></label><input type="text" name="captcha" id="captcha-form"   class="form-control c-square c-theme input-lg"/>
					 
				</div>
                <input type="text" name="hideit" id="hideit" value="" style="display:none !important;">
                <div class="buttons-set">
                  <button type="submit" title="Enviar" class="button submit"> <span> Enviar </span> </button>
                </div>
              </fieldset>
            </div>
			</form>
		
          </div>
        </section>
      
        <?php 		
			include("dcha.php");	
		?>
      </div>
    </div>
  </div>
 
	
	
