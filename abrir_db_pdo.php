 <?php 
// Parametros de conexion
$servername = 'localhost';
$dbname     = 'tacgestorcontenidos_hidrosol2017';
$port       = 3306;
$username   = 'root';
$password   = 'root';
$charset    = 'utf8';

// Persistencia
class DatabaseManager {

    private $conn;
    private $servername;
    private $dbname;
    private $port;
    private $username;
    private $password;
    private $charset;

    /**
     * DatabaseManager constructor.
     */
    public function __construct($servername, $dbname, $port, $username, $password, $charset = 'utf8' )
    {
        $this->servername = $servername;
        $this->dbname = $dbname;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->charset = $charset;

        try {
            //;charset=$this->charset
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname;port=$this->port", $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            echo "Connection failed: {$e->getMessage()}";
            die();
        }

    }

    public function getConnection()
    {
        return $this->conn;
    }

} // Fin DatabaseManager

// Conexion
$databaseManager = new DatabaseManager( $servername,
                                        $dbname,
                                        $port,
                                        $username,
                                        $password,
                                        $charset
);

$conn = $databaseManager->getConnection();
?>
