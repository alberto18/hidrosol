<?php 
if($lang == "en"){
	
	//VERSION INGLES
?>	

<div class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">	
        <aside class="col-right sidebar col-sm-3 wow bounceInUp animated">
          <div class="block block-company">
            <div class="block-title">HIDROSOL </div>
            <div class="block-content">
              <ol id="recently-viewed-items">
                <li class="item odd"><a href="Representaciones">Representaciones</a></li>
                <li class="item even"><a href="tienda">Productos</a></li>
                <li class="item  odd"><a href="clientes" target="_blank">Clientes</a></li>
                <li class="item even"><a href="sat">SAT</a></li>
              </ol>
            </div>
          </div>
        </aside>
      </div>
    </div>
  </div>
</div>
	
				
				
<?php 
}else{
// VERSION CASTELLANO
?>

<div class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">	
        <aside class="col-right sidebar col-sm-3 wow bounceInUp animated">
          <div class="block block-company">
            <div class="block-title">HIDROSOL </div>
            <div class="block-content">
              <ol id="recently-viewed-items">
                <li class="item odd"><a href="representaciones">Representaciones</a></li>
                <li class="item even"><a href="tienda">Productos</a></li>
                <li class="item  odd"><a href="clientes" target="_blank">Clientes</a></li>
                <li class="item even"><a href="sat">SAT</a></li>
              </ol>
            </div>
          </div>
        </aside>
      </div>
    </div>
  </div>
</div>
	


<?php 
} // del if
?>					