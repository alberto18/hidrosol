<?php
/*
Funciones en el script:

ComprobarDetalleAgenda ($valor_dato)
ComprobarMaestroIgic ($valor_dato)
ComprobarMaestroTipoTransporte ($valor_dato)
ComprobarMaestroCargoContacto ($valor_dato)
ComprobarOrigenPresupuesto ($valor_dato)
ComprobarClienteProvPotencialFam ($valor_dato)
ComprobarAlmacen ($valor_dato)
ComprobarMaestroUnidadMedida ($valor_dato)
ComprobarMaestroFrecuencias ($valor_dato)
ComprobarMaestroMotivosAusencia ($valor_dato)
ComprobarArticulosEmbalajes ($valor_dato)
ComprobarArticulos ($valor_dato)
ComprobarMaestroFamilias ($valor_dato)
ComprobarMaestroProvincias ($valor_dato)
ComprobarMaestroMunicipios ($valor_dato)
ComprobarIncidencias ($valor_dato)
ComprobarMaestroTiposIncidencia ($valor_dato)
ComprobarMaestroTiposDocProv ($valor_dato)
ComprobarMaestroTiposDocCliProv ($valor_dato)
ComprobarProvOfertasFamArt ($valor_dato)
ComprobarProvOfertasFam ($valor_dato)
ComprobarProvOfertasCli ($valor_dato)
ComprobarProvOfertas ($valor_dato)
ComprobarProvPlanningLogistica ($valor_dato)
ComprobarProvPlanning ($valor_dato)
ComprobarProvFamArt ($valor_dato)
ComprobarProvFamilia ($valor_dato)
ComprobarProveedor ($valor_dato)
ComprobarMaestroTransportes ($valor_dato)
ComprobarMaestroBancos ($valor_dato)
ComprobarMaestroAduanas ($valor_dato)
ComprobarGrupoEmpresa ($valor_dato)
ComprobarClienteAgTransporte ($valor_dato)
ComprobarClienteComercial ($valor_dato)
ComprobarClienteContactos ($valor_dato)
ComprobarClientesDirecciones ($valor_dato)
ComprobarClienteBanco ($valor_dato)
ComprobarClienteProvFamArt ($valor_dato)
ComprobarClienteProvFam ($valor_dato)
ComprobarClienteProv ($valor_dato)
ComprobarClienteProvPotencial ($valor_dato)
ComprobarCliente ($valor_dato)
ComprobarMaestroFormaPago ($valor_dato)
ComprobarMaestroGrupo ($valor_dato)
ComprobarPresupuesto ($valor_dato)
ComprobarUsuario ($valor_dato)
*/

function ComprobarDetalleAgenda ($valor_dato)
{
$tabla_1 = "clientes_prov_t";
$tabla_2 = "clientes_prov_potenciales_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.detalle_agenda_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.detalle_agenda_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	return $posibilidad;
}

function ComprobarMaestroIgic ($valor_dato)
{
$tabla_1 = "prov_familias_t";
$tabla_2 = "pre_articulos_t";
$tabla_3 = "ped_articulos_t";
$tabla_4 = "alb_articulos_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.igic_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.igic_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.igic_id='$valor_dato';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_4 = "select count($tabla_4.id) as total from $tabla_4 where $tabla_4.igic_id='$valor_dato';";
		//echo "$consulta_4";
		$resultado_4 = mysql_query($consulta_4) or die("$consulta_4, La consulta fall&oacute;: " . mysql_error());
		while ($linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC))
		{ $posibilidad += $linea_4['total']; }
	}
	return $posibilidad;
}

function ComprobarMaestroTipoTransporte ($valor_dato)
{
$tabla_1 = "prov_plan_logis_desplaza_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.tipo_transporte_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
/*
$tabla_2 = "prov_contactos_t";
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.cargo_contacto_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
*/
	return $posibilidad;
}

function ComprobarMaestroCargoContacto ($valor_dato)
{
$tabla_1 = "clientes_contactos_t";
$tabla_2 = "prov_contactos_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.cargo_contacto_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.cargo_contacto_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	return $posibilidad;
}

function ComprobarOrigenPresupuesto ($valor_dato)
{
$tabla_1 = "presupuestos_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.origen_presupuesto_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
/*
$tabla_2 = "historico_ajustes_t";
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.almacen_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
*/
	return $posibilidad;
}

function ComprobarClienteProvPotencialFam ($valor_dato)
{
$tabla_1 = "stock_t";
$tabla_2 = "historico_ajustes_t";
	$posibilidad = 0;
/*
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.almacen_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.almacen_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
*/
	return $posibilidad;
}

function ComprobarAlmacen ($valor_dato)
{
$tabla_1 = "stock_t";
$tabla_2 = "historico_ajustes_t";
$tabla_3 = "historico_entradas_t";
$tabla_4 = "stock_reserva_t";
$tabla_5 = "alb_art_stock_t";

	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.almacen_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.almacen_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.almacen_id='$valor_dato';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_4 = "select count($tabla_4.id) as total from $tabla_4 where $tabla_4.almacen_id='$valor_dato';";
		//echo "$consulta_4";
		$resultado_4 = mysql_query($consulta_4) or die("$consulta_4, La consulta fall&oacute;: " . mysql_error());
		while ($linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC))
		{ $posibilidad += $linea_4['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_5 = "select count($tabla_5.id) as total from $tabla_5 where $tabla_5.almacen_id='$valor_dato';";
		//echo "$consulta_5";
		$resultado_5 = mysql_query($consulta_5) or die("$consulta_5, La consulta fall&oacute;: " . mysql_error());
		while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
		{ $posibilidad += $linea_5['total']; }
	}
	return $posibilidad;
}

function ComprobarMaestroUnidadMedida ($valor_dato)
{
$tabla_1 = "articulos_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.unidad_medida_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	return $posibilidad;
}

function ComprobarMaestroFrecuencias ($valor_dato)
{
$tabla_1 = "programacion_visitas_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.frecuencia_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	return $posibilidad;
}

function ComprobarMaestroMotivosAusencia ($valor_dato)
{
$tabla_1 = "usuarios_ausencias_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.motivo_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	return $posibilidad;
}

function ComprobarArticulosEmbalajes ($valor_dato)
{
$tabla_1 = "clientes_prov_t";
	$posibilidad = 0;
/*
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.cliente_banco_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
*/
	return $posibilidad;
}

function ComprobarArticulos ($valor_dato)
{
//$tabla_2 = "prov_fam_articulos_t";
$tabla_1 = "clientes_prov_fam_articulos_t";
$tabla_2 = "articulos_embalajes_t";
$tabla_3 = "prov_ofertas_fam_articulos_t";
$tabla_4 = "ped_articulos_t";
$tabla_5 = "pre_articulos_t";
$tabla_6 = "alb_articulos_t";
$tabla_7 = "articulos_precios_t";
$tabla_8 = "historico_ajustes_t";
$tabla_9 = "historico_entradas_t";
$tabla_10 = "stock_t";
$tabla_11 = "stock_reserva_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.articulo_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.articulo_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.articulo_id='$valor_dato';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_4 = "select count($tabla_4.id) as total from $tabla_4 where $tabla_4.articulo_id='$valor_dato';";
		//echo "$consulta_4";
		$resultado_4 = mysql_query($consulta_4) or die("$consulta_4, La consulta fall&oacute;: " . mysql_error());
		while ($linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC))
		{ $posibilidad += $linea_4['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_5 = "select count($tabla_5.id) as total from $tabla_5 where $tabla_5.articulo_id='$valor_dato';";
		//echo "$consulta_5";
		$resultado_5 = mysql_query($consulta_5) or die("$consulta_5, La consulta fall&oacute;: " . mysql_error());
		while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
		{ $posibilidad += $linea_5['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_6 = "select count($tabla_6.id) as total from $tabla_6 where $tabla_6.articulo_id='$valor_dato';";
		//echo "$consulta_6";
		$resultado_6 = mysql_query($consulta_6) or die("$consulta_6, La consulta fall&oacute;: " . mysql_error());
		while ($linea_6 = mysql_fetch_array($resultado_6, MYSQL_ASSOC))
		{ $posibilidad += $linea_6['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_7 = "select count($tabla_7.id) as total from $tabla_7 where $tabla_7.articulo_id='$valor_dato';";
		//echo "$consulta_7";
		$resultado_7 = mysql_query($consulta_7) or die("$consulta_7, La consulta fall&oacute;: " . mysql_error());
		while ($linea_7 = mysql_fetch_array($resultado_7, MYSQL_ASSOC))
		{ $posibilidad += $linea_7['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_8 = "select count($tabla_8.id) as total from $tabla_8 where $tabla_8.articulo_id='$valor_dato';";
		//echo "$consulta_8";
		$resultado_8 = mysql_query($consulta_8) or die("$consulta_8, La consulta fall&oacute;: " . mysql_error());
		while ($linea_8 = mysql_fetch_array($resultado_8, MYSQL_ASSOC))
		{ $posibilidad += $linea_8['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_9 = "select count($tabla_9.id) as total from $tabla_9 where $tabla_9.articulo_id='$valor_dato';";
		//echo "$consulta_9";
		$resultado_9 = mysql_query($consulta_9) or die("$consulta_9, La consulta fall&oacute;: " . mysql_error());
		while ($linea_9 = mysql_fetch_array($resultado_9, MYSQL_ASSOC))
		{ $posibilidad += $linea_9['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_10 = "select count($tabla_10.id) as total from $tabla_10 where $tabla_10.articulo_id='$valor_dato';";
		//echo "$consulta_10";
		$resultado_10 = mysql_query($consulta_10) or die("$consulta_10, La consulta fall&oacute;: " . mysql_error());
		while ($linea_10 = mysql_fetch_array($resultado_10, MYSQL_ASSOC))
		{ $posibilidad += $linea_10['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_11 = "select count($tabla_11.id) as total from $tabla_11 where $tabla_11.articulo_id='$valor_dato';";
		//echo "$consulta_11";
		$resultado_11 = mysql_query($consulta_11) or die("$consulta_11, La consulta fall&oacute;: " . mysql_error());
		while ($linea_11 = mysql_fetch_array($resultado_11, MYSQL_ASSOC))
		{ $posibilidad += $linea_11['total']; }
	}
	return $posibilidad;
}

function ComprobarMaestroFamilias ($valor_dato)
{
$tabla_1 = "articulos_t";
$tabla_3 = "clientes_prov_familias_t";
$tabla_2 = "prov_familias_t";
$tabla_4 = "prov_ofertas_familias_t";
$tabla_5 = "clientes_prov_potenciales_familias_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.familia_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.familia_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.familia_id='$valor_dato';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_4 = "select count($tabla_4.id) as total from $tabla_4 where $tabla_4.familia_id='$valor_dato';";
		//echo "$consulta_4";
		$resultado_4 = mysql_query($consulta_4) or die("$consulta_4, La consulta fall&oacute;: " . mysql_error());
		while ($linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC))
		{ $posibilidad += $linea_4['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_5 = "select count($tabla_5.id) as total from $tabla_5 where $tabla_5.familia_id='$valor_dato';";
		//echo "$consulta_5";
		$resultado_5 = mysql_query($consulta_5) or die("$consulta_5, La consulta fall&oacute;: " . mysql_error());
		while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
		{ $posibilidad += $linea_5['total']; }
	}
	return $posibilidad;
}

function ComprobarMaestroProvincias ($valor_dato)
{
$tabla_1 = "maestro_municipios_t";
$tabla_3 = "maestro_islas_t";
$tabla_2 = "maestro_almacenes_t";
$tabla_4 = "clientes_direcciones_t";
$tabla_5 = "prov_direcciones_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.provincia_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.provincia_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.provincia_id='$valor_dato';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_4 = "select count($tabla_4.id) as total from $tabla_4 where $tabla_4.provincia_id='$valor_dato';";
		//echo "$consulta_4";
		$resultado_4 = mysql_query($consulta_4) or die("$consulta_4, La consulta fall&oacute;: " . mysql_error());
		while ($linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC))
		{ $posibilidad += $linea_4['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_5 = "select count($tabla_5.id) as total from $tabla_5 where $tabla_5.provincia_id='$valor_dato';";
		//echo "$consulta_5";
		$resultado_5 = mysql_query($consulta_5) or die("$consulta_5, La consulta fall&oacute;: " . mysql_error());
		while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
		{ $posibilidad += $linea_5['total']; }
	}
	return $posibilidad;
}

function ComprobarMaestroMunicipios ($valor_dato)
{
$tabla_1 = "maestro_almacenes_t";
$tabla_3 = "clientes_direcciones_t";
$tabla_2 = "prov_direcciones_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.municipio_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.municipio_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.municipio_id='$valor_dato';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	return $posibilidad;
}

function ComprobarIncidencias ($valor_dato)
{
$tabla_1 = "detalle_incidencias_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.incidencia_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	return $posibilidad;
}

function ComprobarMaestroTiposIncidencia ($valor_dato)
{
$tabla_1 = "incidencias_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.tipo_incidencia_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	return $posibilidad;
}

function ComprobarMaestroTiposDocProv ($valor_dato)
{
$tabla_1 = "prov_documentos_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.tipo_doc_prov_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	return $posibilidad;
}

function ComprobarMaestroTiposDocCliProv ($valor_dato)
{
$tabla_1 = "clientes_prov_documentos_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.tipo_doc_cli_prov_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	return $posibilidad;
}

function ComprobarProvOfertasFamArt ($valor_dato)
{
$tabla_1 = "prov_ofertas_fam_articulos_t";
$tabla_2 = "prov_ofertas_familias_t";
	$posibilidad = 0;
/*
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.prov_oferta_familia_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.prov_oferta_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
*/
	return $posibilidad;
}

function ComprobarProvOfertasFam ($valor_dato)
{
$tabla_1 = "prov_ofertas_fam_articulos_t";
$tabla_2 = "prov_ofertas_familias_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.prov_oferta_familia_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
/*
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.prov_oferta_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
*/
	return $posibilidad;
}

function ComprobarProvOfertasCli ($valor_dato)
{
$tabla_1 = "prov_ofertas_clientes_t";
$tabla_2 = "prov_ofertas_familias_t";
	$posibilidad = 0;
/*
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.prov_oferta_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.prov_oferta_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
*/
	return $posibilidad;
}

function ComprobarProvOfertas ($valor_dato)
{
$tabla_1 = "prov_ofertas_clientes_t";
$tabla_2 = "prov_ofertas_familias_t";
$tabla_3 = "prov_ofertas_imagenes_t";
$tabla_4 = "log_ofertas_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.prov_oferta_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.prov_oferta_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.prov_oferta_id='$valor_dato';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_4 = "select count($tabla_4.id) as total from $tabla_4 where $tabla_4.prov_oferta_id='$valor_dato';";
		//echo "$consulta_4";
		$resultado_4 = mysql_query($consulta_4) or die("$consulta_4, La consulta fall&oacute;: " . mysql_error());
		while ($linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC))
		{ $posibilidad += $linea_4['total']; }
	}
	return $posibilidad;
}

function ComprobarProvPlanningLogistica ($valor_dato)
{
	$prov_planning_id = 0;
	$fecha_logistica = "";
	$cons = "select * from prov_planning_logistica_t where id='".$valor_dato."';";
	$res = mysql_query($cons) or die("La consulta fall&oacute;: $cons " . mysql_error());
	while ($lin = mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$prov_planning_id = $lin['prov_planning_id'];
		$fecha_logistica = $lin['fecha'];
	}
	
$tabla_1 = "agenda_t";
	$posibilidad = 0;
	if ($prov_planning_id > 0)
	{
		$array_datos_padre = array();
		$array_datos_padre = DatosPlanningProv($prov_planning_id);
		
		$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.tipo_agenda_id='2' and $tabla_1.fecha='$fecha_logistica' and $tabla_1.proveedor_id='".$array_datos_padre[0]."';";
		//echo "$consulta_1";
		$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
		while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
		{ $posibilidad += $linea_1['total']; }
	}
	return $posibilidad;
}

function ComprobarProvPlanning ($valor_dato)
{
$tabla_1 = "prov_planning_logistica_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.prov_planning_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	return $posibilidad;
}

function ComprobarProvFamArt ($valor_dato)
{
$tabla_1 = "clientes_prov_familias_t";
	$posibilidad = 0;
/*
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.cliente_prov_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
*/
	return $posibilidad;
}

function ComprobarProvFamilia ($valor_dato)
{
$tabla_1 = "clientes_prov_familias_t";
$tabla_2 = "prov_ofertas_familias_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 join clientes_prov_t on clientes_prov_t.id=$tabla_1.cliente_prov_id join prov_familias_t on prov_familias_t.familia_id=$tabla_1.familia_id where clientes_prov_t.proveedor_id=prov_familias_t.proveedor_id and prov_familias_t.id='$valor_dato';";
	//echo "$consulta_1<br>";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 
join prov_ofertas_t on prov_ofertas_t.id=$tabla_2.prov_oferta_id 
join prov_familias_t on prov_familias_t.familia_id=$tabla_2.familia_id 
where prov_ofertas_t.proveedor_id=prov_familias_t.proveedor_id and prov_familias_t.id='$valor_dato';";
		//echo "$consulta_2<br>";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	return $posibilidad;
}

function ComprobarProveedor ($valor_dato)
{
$tabla_9 = "agenda_t";
$tabla_6 = "clientes_prov_t";
$tabla_12 = "clientes_prov_potenciales_t";
$tabla_10 = "incidencias_t";
$tabla_4 = "prov_agencias_transportes_t";
$tabla_3 = "prov_bancos_t";
$tabla_2 = "prov_contactos_t";
$tabla_1 = "prov_direcciones_t";
$tabla_5 = "prov_documentos_t";
$tabla_7 = "prov_familias_t";
$tabla_8 = "prov_planning_t";
$tabla_11 = "prov_ofertas_t";
$tabla_13 = "log_ofertas_t";
$tabla_14 = "detalle_agenda_t";
$tabla_15 = "articulos_precios_t";
$tabla_16 = "pre_articulos_t";
$tabla_17 = "pre_portes_t";
$tabla_18 = "ped_articulos_t";
$tabla_19 = "ped_portes_t";
$tabla_20 = "albaranes_t";
$tabla_21 = "stock_t";
$tabla_22 = "stock_reserva_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.proveedor_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.proveedor_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.proveedor_id='$valor_dato';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_4 = "select count($tabla_4.id) as total from $tabla_4 where $tabla_4.proveedor_id='$valor_dato';";
		//echo "$consulta_4";
		$resultado_4 = mysql_query($consulta_4) or die("$consulta_4, La consulta fall&oacute;: " . mysql_error());
		while ($linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC))
		{ $posibilidad += $linea_4['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_5 = "select count($tabla_5.id) as total from $tabla_5 where $tabla_5.proveedor_id='$valor_dato';";
		//echo "$consulta_5";
		$resultado_5 = mysql_query($consulta_5) or die("$consulta_5, La consulta fall&oacute;: " . mysql_error());
		while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
		{ $posibilidad += $linea_5['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_6 = "select count($tabla_6.id) as total from $tabla_6 where $tabla_6.proveedor_id='$valor_dato';";
		//echo "$consulta_6";
		$resultado_6 = mysql_query($consulta_6) or die("$consulta_6, La consulta fall&oacute;: " . mysql_error());
		while ($linea_6 = mysql_fetch_array($resultado_6, MYSQL_ASSOC))
		{ $posibilidad += $linea_6['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_7 = "select count($tabla_7.id) as total from $tabla_7 where $tabla_7.proveedor_id='$valor_dato';";
		//echo "$consulta_7";
		$resultado_7 = mysql_query($consulta_7) or die("$consulta_7, La consulta fall&oacute;: " . mysql_error());
		while ($linea_7 = mysql_fetch_array($resultado_7, MYSQL_ASSOC))
		{ $posibilidad += $linea_7['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_8 = "select count($tabla_8.id) as total from $tabla_8 where $tabla_8.proveedor_id='$valor_dato';";
		//echo "$consulta_8";
		$resultado_8 = mysql_query($consulta_8) or die("$consulta_8, La consulta fall&oacute;: " . mysql_error());
		while ($linea_8 = mysql_fetch_array($resultado_8, MYSQL_ASSOC))
		{ $posibilidad += $linea_8['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_9 = "select count($tabla_9.id) as total from $tabla_9 where $tabla_9.proveedor_id='$valor_dato';";
		//echo "$consulta_9";
		$resultado_9 = mysql_query($consulta_9) or die("$consulta_9, La consulta fall&oacute;: " . mysql_error());
		while ($linea_9 = mysql_fetch_array($resultado_9, MYSQL_ASSOC))
		{ $posibilidad += $linea_9['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_10 = "select count($tabla_10.id) as total from $tabla_10 where ($tabla_10.origen_proveedor_id='$valor_dato' or $tabla_10.destino_proveedor_id='$valor_dato');";
		//echo "$consulta_10";
		$resultado_10 = mysql_query($consulta_10) or die("$consulta_10, La consulta fall&oacute;: " . mysql_error());
		while ($linea_10 = mysql_fetch_array($resultado_10, MYSQL_ASSOC))
		{ $posibilidad += $linea_10['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_11 = "select count($tabla_11.id) as total from $tabla_11 where $tabla_11.proveedor_id='$valor_dato';";
		//echo "$consulta_11";
		$resultado_11 = mysql_query($consulta_11) or die("$consulta_11, La consulta fall&oacute;: " . mysql_error());
		while ($linea_11 = mysql_fetch_array($resultado_11, MYSQL_ASSOC))
		{ $posibilidad += $linea_11['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_12 = "select count($tabla_12.id) as total from $tabla_12 where $tabla_12.proveedor_id='$valor_dato';";
		//echo "$consulta_12";
		$resultado_12 = mysql_query($consulta_12) or die("$consulta_12, La consulta fall&oacute;: " . mysql_error());
		while ($linea_12 = mysql_fetch_array($resultado_12, MYSQL_ASSOC))
		{ $posibilidad += $linea_12['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_13 = "select count($tabla_13.id) as total from $tabla_13 where $tabla_13.proveedor_id='$valor_dato';";
		//echo "$consulta_13";
		$resultado_13 = mysql_query($consulta_13) or die("$consulta_13, La consulta fall&oacute;: " . mysql_error());
		while ($linea_13 = mysql_fetch_array($resultado_13, MYSQL_ASSOC))
		{ $posibilidad += $linea_13['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_14 = "select count($tabla_14.id) as total from $tabla_14 where $tabla_14.proveedor_id='$valor_dato';";
		//echo "$consulta_14";
		$resultado_14 = mysql_query($consulta_14) or die("$consulta_14, La consulta fall&oacute;: " . mysql_error());
		while ($linea_14 = mysql_fetch_array($resultado_14, MYSQL_ASSOC))
		{ $posibilidad += $linea_14['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_15 = "select count($tabla_15.id) as total from $tabla_15 where $tabla_15.proveedor_id='$valor_dato';";
		//echo "$consulta_15";
		$resultado_15 = mysql_query($consulta_15) or die("$consulta_15, La consulta fall&oacute;: " . mysql_error());
		while ($linea_15 = mysql_fetch_array($resultado_15, MYSQL_ASSOC))
		{ $posibilidad += $linea_15['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_16 = "select count($tabla_16.id) as total from $tabla_16 where $tabla_16.proveedor_id='$valor_dato';";
		//echo "$consulta_16";
		$resultado_16 = mysql_query($consulta_16) or die("$consulta_16, La consulta fall&oacute;: " . mysql_error());
		while ($linea_16 = mysql_fetch_array($resultado_16, MYSQL_ASSOC))
		{ $posibilidad += $linea_16['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_17 = "select count($tabla_17.id) as total from $tabla_17 where $tabla_17.proveedor_id='$valor_dato';";
		//echo "$consulta_17";
		$resultado_17 = mysql_query($consulta_17) or die("$consulta_17, La consulta fall&oacute;: " . mysql_error());
		while ($linea_17 = mysql_fetch_array($resultado_17, MYSQL_ASSOC))
		{ $posibilidad += $linea_17['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_18 = "select count($tabla_18.id) as total from $tabla_18 where $tabla_18.proveedor_id='$valor_dato';";
		//echo "$consulta_18";
		$resultado_18 = mysql_query($consulta_18) or die("$consulta_18, La consulta fall&oacute;: " . mysql_error());
		while ($linea_18 = mysql_fetch_array($resultado_18, MYSQL_ASSOC))
		{ $posibilidad += $linea_18['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_19 = "select count($tabla_19.id) as total from $tabla_19 where $tabla_19.proveedor_id='$valor_dato';";
		//echo "$consulta_19";
		$resultado_19 = mysql_query($consulta_19) or die("$consulta_19, La consulta fall&oacute;: " . mysql_error());
		while ($linea_19 = mysql_fetch_array($resultado_19, MYSQL_ASSOC))
		{ $posibilidad += $linea_19['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_20 = "select count($tabla_20.id) as total from $tabla_20 where $tabla_20.proveedor_id='$valor_dato';";
		//echo "$consulta_20";
		$resultado_20 = mysql_query($consulta_20) or die("$consulta_20, La consulta fall&oacute;: " . mysql_error());
		while ($linea_20 = mysql_fetch_array($resultado_20, MYSQL_ASSOC))
		{ $posibilidad += $linea_20['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_21 = "select count($tabla_21.id) as total from $tabla_21 where $tabla_21.proveedor_id='$valor_dato';";
		//echo "$consulta_21";
		$resultado_21 = mysql_query($consulta_21) or die("$consulta_21, La consulta fall&oacute;: " . mysql_error());
		while ($linea_21 = mysql_fetch_array($resultado_21, MYSQL_ASSOC))
		{ $posibilidad += $linea_21['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_22 = "select count($tabla_22.id) as total from $tabla_22 where $tabla_22.proveedor_id='$valor_dato';";
		//echo "$consulta_22";
		$resultado_22 = mysql_query($consulta_22) or die("$consulta_22, La consulta fall&oacute;: " . mysql_error());
		while ($linea_22 = mysql_fetch_array($resultado_22, MYSQL_ASSOC))
		{ $posibilidad += $linea_22['total']; }
	}
	return $posibilidad;
}

function ComprobarMaestroTransportes ($valor_dato)
{
$tabla_1 = "prov_agencias_transportes_t";
$tabla_2 = "clientes_agencias_transportes_t";
$tabla_3 = "clientes_prov_t";
$tabla_4 = "pre_portes_t";
$tabla_5 = "ped_portes_t";
$tabla_6 = "albaranes_t";
$tabla_7 = "alb_avisos_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.agencia_transporte_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.agencia_transporte_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.agencia_transporte_id='$valor_dato';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_4 = "select count($tabla_4.id) as total from $tabla_4 where ($tabla_4.cli_ag_transporte_id='$valor_dato' or $tabla_4.prov_ag_transporte_id='$valor_dato');";
		//echo "$consulta_4";
		$resultado_4 = mysql_query($consulta_4) or die("$consulta_4, La consulta fall&oacute;: " . mysql_error());
		while ($linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC))
		{ $posibilidad += $linea_4['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_5 = "select count($tabla_5.id) as total from $tabla_5 where ($tabla_5.cli_ag_transporte_id='$valor_dato' or $tabla_5.prov_ag_transporte_id='$valor_dato');";
		//echo "$consulta_5";
		$resultado_5 = mysql_query($consulta_5) or die("$consulta_5, La consulta fall&oacute;: " . mysql_error());
		while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
		{ $posibilidad += $linea_5['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_6 = "select count($tabla_6.id) as total from $tabla_6 where $tabla_6.agencia_transporte_id='$valor_dato';";
		//echo "$consulta_6";
		$resultado_6 = mysql_query($consulta_6) or die("$consulta_6, La consulta fall&oacute;: " . mysql_error());
		while ($linea_6 = mysql_fetch_array($resultado_6, MYSQL_ASSOC))
		{ $posibilidad += $linea_6['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_7 = "select count($tabla_7.id) as total from $tabla_7 where $tabla_7.agencia_transporte_id='$valor_dato';";
		//echo "$consulta_7";
		$resultado_7 = mysql_query($consulta_7) or die("$consulta_7, La consulta fall&oacute;: " . mysql_error());
		while ($linea_7 = mysql_fetch_array($resultado_7, MYSQL_ASSOC))
		{ $posibilidad += $linea_7['total']; }
	}
	return $posibilidad;
}

function ComprobarMaestroBancos ($valor_dato)
{
$tabla_1 = "clientes_bancos_t";
$tabla_2 = "prov_bancos_t";
$tabla_3 = "prov_bancos_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.banco_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.banco_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	return $posibilidad;
}

function ComprobarMaestroAduanas ($valor_dato)
{
$tabla_1 = "clientes_agencias_aduanas_t";
$tabla_2 = "clientes_prov_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.agencia_aduana_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.agencia_aduana_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	return $posibilidad;
}

function ComprobarGrupoEmpresa ($valor_dato)
{
	// los grupos de empresas son clientes pero de tipo grupo
$tabla_1 = "grupos_emp_componentes_t";
$tabla_2 = "clientes_prov_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.grupo_empresa_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.cliente_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	return $posibilidad;
}

function ComprobarClienteAgTransporte ($valor_dato)
{
$tabla_1 = "clientes_prov_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 join clientes_agencias_transportes_t on clientes_agencias_transportes_t.cliente_id=$tabla_1.cliente_id where clientes_agencias_transportes_t.id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	return $posibilidad;
}

function ComprobarClienteComercial ($valor_dato)
{
$tabla_1 = "agenda_t";
$tabla_2 = "presupuestos_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 join clientes_comerciales_t on clientes_comerciales_t.cliente_id=$tabla_1.cliente_id where $tabla_1.comercial_visita_id=clientes_comerciales_t.usuario_id and clientes_comerciales_t.id='$valor_dato';";
	//echo "$consulta_1<br>";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_1_1 = "select count($tabla_1.id) as total from $tabla_1 join clientes_comerciales_t on clientes_comerciales_t.cliente_id=$tabla_1.cliente_visitado_id where $tabla_1.comercial_visita_id=clientes_comerciales_t.usuario_id and clientes_comerciales_t.id='$valor_dato';";
		//echo "$consulta_1_1";
		$resultado_1_1 = mysql_query($consulta_1_1) or die("$consulta_1_1, La consulta fall&oacute;: " . mysql_error());
		while ($linea_1_1 = mysql_fetch_array($resultado_1_1, MYSQL_ASSOC))
		{ $posibilidad += $linea_1_1['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.comercial_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	return $posibilidad;
}

function ComprobarClienteContactos ($valor_dato)
{
$tabla_1 = "presupuestos_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.contacto_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
/*
$tabla_2 = "agenda_t";
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.contacto_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
*/
	return $posibilidad;
}

function ComprobarClientesDirecciones ($valor_dato)
{
$tabla_1 = "agenda_t";
$tabla_2 = "clientes_prov_t";
$tabla_3 = "albaranes_t";
$tabla_4 = "presupuestos_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.cliente_direccion_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.cliente_direccion_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.cliente_direccion_id='$valor_dato';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_4 = "select count($tabla_4.id) as total from $tabla_4 where $tabla_4.cliente_direccion_id='$valor_dato';";
		//echo "$consulta_4";
		$resultado_4 = mysql_query($consulta_4) or die("$consulta_4, La consulta fall&oacute;: " . mysql_error());
		while ($linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC))
		{ $posibilidad += $linea_4['total']; }
	}
	return $posibilidad;
}

function ComprobarClienteBanco ($valor_dato)
{
$tabla_1 = "clientes_prov_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.cliente_banco_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	return $posibilidad;
}

function ComprobarClienteProvFamArt ($valor_dato)
{
$tabla_1 = "clientes_prov_familias_t";
	$posibilidad = 0;
/*
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.cliente_prov_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
*/
	return $posibilidad;
}

function ComprobarClienteProvFam ($valor_dato)
{
$tabla_1 = "clientes_prov_fam_articulos_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.cliente_prov_familia_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	return $posibilidad;
}

function ComprobarClienteProv ($valor_dato)
{
$tabla_1 = "clientes_prov_familias_t";
$tabla_2 = "clientes_prov_documentos_t";
$tabla_3 = "agenda_t";
$tabla_4 = "detalle_agenda_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.cliente_prov_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.cliente_prov_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 join clientes_prov_t on clientes_prov_t.proveedor_id=$tabla_3.proveedor_id where $tabla_3.cliente_visitado_id=clientes_prov_t.cliente_id and clientes_prov_t.id='$valor_dato';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_4 = "select count($tabla_4.id) as total from $tabla_4
join $tabla_3 on $tabla_3.id=$tabla_4.agenda_id 
join clientes_prov_t on clientes_prov_t.proveedor_id=$tabla_4.proveedor_id where $tabla_3.cliente_id=clientes_prov_t.cliente_id and clientes_prov_t.id='$valor_dato';";
		//echo "$consulta_4";
		$resultado_4 = mysql_query($consulta_4) or die("$consulta_4, La consulta fall&oacute;: " . mysql_error());
		while ($linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC))
		{ $posibilidad += $linea_4['total']; }
	}
	return $posibilidad;
}

function ComprobarClienteProvPotencial ($valor_dato)
{
$tabla_1 = "agenda_t";
$tabla_3 = "clientes_prov_potenciales_familias_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 join clientes_prov_potenciales_t on clientes_prov_potenciales_t.proveedor_id=$tabla_1.proveedor_id where $tabla_1.cliente_visitado_id=clientes_prov_potenciales_t.cliente_id and clientes_prov_potenciales_t.id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
/*
// las visitas de proveedores no tienen detalle de agenda
$tabla_2 = "detalle_agenda_t";
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2
join $tabla_1 on $tabla_1.id=$tabla_2.agenda_id 
join clientes_prov_t on clientes_prov_t.proveedor_id=$tabla_2.proveedor_id where $tabla_1.cliente_id=clientes_prov_t.cliente_id and clientes_prov_t.id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
*/
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.cliente_prov_potencial_id='$valor_dato';";
		//echo "$consulta_3<br>";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	return $posibilidad;
}

function ComprobarCliente ($valor_dato)
{
$tabla_10 = "agenda_t";
$tabla_4 = "clientes_agencias_aduanas_t";
$tabla_7 = "clientes_agencias_transportes_t";
$tabla_6 = "clientes_bancos_t";
$tabla_3 = "clientes_comerciales_t";
$tabla_5 = "clientes_contactos_t";
$tabla_2 = "clientes_direcciones_t";
$tabla_8 = "clientes_prov_t";
$tabla_13 = "clientes_prov_potenciales_t";
$tabla_1 = "grupos_emp_componentes_t";
$tabla_11 = "incidencias_t";
$tabla_9 = "programacion_visitas_t";
$tabla_12 = "prov_ofertas_clientes_t";
$tabla_14 = "log_ofertas_t";
$tabla_15 = "presupuestos_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.cliente_id='$valor_dato';";
	//echo "$consulta_1<br>";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.cliente_id='$valor_dato';";
		//echo "$consulta_2<br>";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.cliente_id='$valor_dato';";
		//echo "$consulta_3<br>";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_4 = "select count($tabla_4.id) as total from $tabla_4 where $tabla_4.cliente_id='$valor_dato';";
		//echo "$consulta_4<br>";
		$resultado_4 = mysql_query($consulta_4) or die("$consulta_4, La consulta fall&oacute;: " . mysql_error());
		while ($linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC))
		{ $posibilidad += $linea_4['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_5 = "select count($tabla_5.id) as total from $tabla_5 where $tabla_5.cliente_id='$valor_dato';";
		//echo "$consulta_5<br>";
		$resultado_5 = mysql_query($consulta_5) or die("$consulta_5, La consulta fall&oacute;: " . mysql_error());
		while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
		{ $posibilidad += $linea_5['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_6 = "select count($tabla_6.id) as total from $tabla_6 where $tabla_6.cliente_id='$valor_dato';";
		//echo "$consulta_6<br>";
		$resultado_6 = mysql_query($consulta_6) or die("$consulta_6, La consulta fall&oacute;: " . mysql_error());
		while ($linea_6 = mysql_fetch_array($resultado_6, MYSQL_ASSOC))
		{ $posibilidad += $linea_6['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_7 = "select count($tabla_7.id) as total from $tabla_7 where $tabla_7.cliente_id='$valor_dato';";
		//echo "$consulta_7<br>";
		$resultado_7 = mysql_query($consulta_7) or die("$consulta_7, La consulta fall&oacute;: " . mysql_error());
		while ($linea_7 = mysql_fetch_array($resultado_7, MYSQL_ASSOC))
		{ $posibilidad += $linea_7['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_8 = "select count($tabla_8.id) as total from $tabla_8 where $tabla_8.cliente_id='$valor_dato';";
		//echo "$consulta_8<br>";
		$resultado_8 = mysql_query($consulta_8) or die("$consulta_8, La consulta fall&oacute;: " . mysql_error());
		while ($linea_8 = mysql_fetch_array($resultado_8, MYSQL_ASSOC))
		{ $posibilidad += $linea_8['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_9 = "select count($tabla_9.id) as total from $tabla_9 where $tabla_9.cliente_id='$valor_dato';";
		//echo "$consulta_9";
		$resultado_9 = mysql_query($consulta_9) or die("$consulta_9, La consulta fall&oacute;: " . mysql_error());
		while ($linea_9 = mysql_fetch_array($resultado_9, MYSQL_ASSOC))
		{ $posibilidad += $linea_9['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_10 = "select count($tabla_10.id) as total from $tabla_10 where ($tabla_10.cliente_id='$valor_dato' or $tabla_10.cliente_visitado_id='$valor_dato');";
		//echo "$consulta_10";
		$resultado_10 = mysql_query($consulta_10) or die("$consulta_10, La consulta fall&oacute;: " . mysql_error());
		while ($linea_10 = mysql_fetch_array($resultado_10, MYSQL_ASSOC))
		{ $posibilidad += $linea_10['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_11 = "select count($tabla_11.id) as total from $tabla_11 where ($tabla_11.origen_cliente_id='$valor_dato' or $tabla_11.destino_cliente_id='$valor_dato');";
		//echo "$consulta_11";
		$resultado_11 = mysql_query($consulta_11) or die("$consulta_11, La consulta fall&oacute;: " . mysql_error());
		while ($linea_11 = mysql_fetch_array($resultado_11, MYSQL_ASSOC))
		{ $posibilidad += $linea_11['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_12 = "select count($tabla_12.id) as total from $tabla_12 where $tabla_12.cliente_id='$valor_dato';";
		//echo "$consulta_12";
		$resultado_12 = mysql_query($consulta_12) or die("$consulta_12, La consulta fall&oacute;: " . mysql_error());
		while ($linea_12 = mysql_fetch_array($resultado_12, MYSQL_ASSOC))
		{ $posibilidad += $linea_12['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_13 = "select count($tabla_13.id) as total from $tabla_13 where $tabla_13.cliente_id='$valor_dato';";
		//echo "$consulta_13";
		$resultado_13 = mysql_query($consulta_13) or die("$consulta_13, La consulta fall&oacute;: " . mysql_error());
		while ($linea_13 = mysql_fetch_array($resultado_13, MYSQL_ASSOC))
		{ $posibilidad += $linea_13['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_14 = "select count($tabla_14.id) as total from $tabla_14 where $tabla_14.cliente_id='$valor_dato';";
		//echo "$consulta_14";
		$resultado_14 = mysql_query($consulta_14) or die("$consulta_14, La consulta fall&oacute;: " . mysql_error());
		while ($linea_14 = mysql_fetch_array($resultado_14, MYSQL_ASSOC))
		{ $posibilidad += $linea_14['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_15 = "select count($tabla_15.id) as total from $tabla_15 where $tabla_15.cliente_id='$valor_dato';";
		//echo "$consulta_15";
		$resultado_15 = mysql_query($consulta_15) or die("$consulta_15, La consulta fall&oacute;: " . mysql_error());
		while ($linea_15 = mysql_fetch_array($resultado_15, MYSQL_ASSOC))
		{ $posibilidad += $linea_15['total']; }
	}
	return $posibilidad;
}

function ComprobarMaestroFormaPago ($valor_dato)
{
$tabla_1 = "clientes_t";
$tabla_2 = "proveedores_t";
$tabla_3 = "clientes_prov_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.forma_pago_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where $tabla_2.forma_pago_id='$valor_dato';";
		//echo "$consulta_2";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.forma_pago_id='$valor_dato';";
		//echo "$consulta_3";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	return $posibilidad;
}

function ComprobarMaestroGrupo ($valor_dato)
{
$tabla_1 = "usuarios_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.grupo_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	return $posibilidad;
}

function ComprobarPresupuesto ($valor_dato)
{
$tabla_1 = "usuarios_t";
	$posibilidad = 1;
/*
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.grupo_id='$valor_dato';";
	//echo "$consulta_1";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
*/
	return $posibilidad;
}

function ComprobarUsuario ($valor_dato)
{
$tabla_8 = "agenda_t";
$tabla_1 = "clientes_comerciales_t";
$tabla_6 = "clientes_prov_documentos_t";
$tabla_7 = "programacion_visitas_t";
$tabla_4 = "prov_documentos_t";
$tabla_5 = "usuarios_ausencias_t";
$tabla_2 = "incidencias_t";
$tabla_3 = "detalle_incidencias_t";
$tabla_9 = "log_incidencias_t";
$tabla_10 = "alb_art_stock_t";
$tabla_11 = "alb_documentos_t";
$tabla_12 = "albaranes_t";
$tabla_13 = "articulos_t";
$tabla_14 = "historico_ajustes_t";
$tabla_15 = "log_ofertas_t";
$tabla_16 = "pre_documentos_t";
$tabla_17 = "ped_documentos_t";
$tabla_18 = "ped_art_preparado_t";
	$posibilidad = 0;
	$consulta_1 = "select count($tabla_1.id) as total from $tabla_1 where $tabla_1.usuario_id='$valor_dato';";
	//echo "$consulta_1<br>";
	$resultado_1 = mysql_query($consulta_1) or die("$consulta_1, La consulta fall&oacute;: " . mysql_error());
	while ($linea_1 = mysql_fetch_array($resultado_1, MYSQL_ASSOC))
	{ $posibilidad += $linea_1['total']; }
	if ($posibilidad == 0)
	{
		$consulta_2 = "select count($tabla_2.id) as total from $tabla_2 where ($tabla_2.usuario_inicio_id='$valor_dato' or $tabla_2.usuario_gestion_id='$valor_dato' or $tabla_2.usuario_fin_id='$valor_dato');";
		//echo "$consulta_2<br>";
		$resultado_2 = mysql_query($consulta_2) or die("$consulta_2, La consulta fall&oacute;: " . mysql_error());
		while ($linea_2 = mysql_fetch_array($resultado_2, MYSQL_ASSOC))
		{ $posibilidad += $linea_2['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_3 = "select count($tabla_3.id) as total from $tabla_3 where $tabla_3.user_id='$valor_dato';";
		//echo "$consulta_3<br>";
		$resultado_3 = mysql_query($consulta_3) or die("$consulta_3, La consulta fall&oacute;: " . mysql_error());
		while ($linea_3 = mysql_fetch_array($resultado_3, MYSQL_ASSOC))
		{ $posibilidad += $linea_3['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_4 = "select count($tabla_4.id) as total from $tabla_4 where $tabla_4.user_id='$valor_dato';";
		//echo "$consulta_4<br>";
		$resultado_4 = mysql_query($consulta_4) or die("$consulta_4, La consulta fall&oacute;: " . mysql_error());
		while ($linea_4 = mysql_fetch_array($resultado_4, MYSQL_ASSOC))
		{ $posibilidad += $linea_4['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_5 = "select count($tabla_5.id) as total from $tabla_5 where $tabla_5.usuario_id='$valor_dato';";
		//echo "$consulta_5<br>";
		$resultado_5 = mysql_query($consulta_5) or die("$consulta_5, La consulta fall&oacute;: " . mysql_error());
		while ($linea_5 = mysql_fetch_array($resultado_5, MYSQL_ASSOC))
		{ $posibilidad += $linea_5['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_6 = "select count($tabla_6.id) as total from $tabla_6 where $tabla_6.user_id='$valor_dato';";
		//echo "$consulta_6<br>";
		$resultado_6 = mysql_query($consulta_6) or die("$consulta_6, La consulta fall&oacute;: " . mysql_error());
		while ($linea_6 = mysql_fetch_array($resultado_6, MYSQL_ASSOC))
		{ $posibilidad += $linea_6['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_7 = "select count($tabla_7.id) as total from $tabla_7 where $tabla_7.usuario_id='$valor_dato';";
		//echo "$consulta_7<br>";
		$resultado_7 = mysql_query($consulta_7) or die("$consulta_7, La consulta fall&oacute;: " . mysql_error());
		while ($linea_7 = mysql_fetch_array($resultado_7, MYSQL_ASSOC))
		{ $posibilidad += $linea_7['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_8 = "select count($tabla_8.id) as total from $tabla_8 where $tabla_8.comercial_visita_id='$valor_dato';";
		//echo "$consulta_8<br>";
		$resultado_8 = mysql_query($consulta_8) or die("$consulta_8, La consulta fall&oacute;: " . mysql_error());
		while ($linea_8 = mysql_fetch_array($resultado_8, MYSQL_ASSOC))
		{ $posibilidad += $linea_8['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_9 = "select count($tabla_9.id) as total from $tabla_9 where $tabla_9.user_id='$valor_dato';";
		//echo "$consulta_9<br>";
		$resultado_9 = mysql_query($consulta_9) or die("$consulta_9, La consulta fall&oacute;: " . mysql_error());
		while ($linea_9 = mysql_fetch_array($resultado_9, MYSQL_ASSOC))
		{ $posibilidad += $linea_9['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_10 = "select count($tabla_10.id) as total from $tabla_10 where $tabla_10.usuario_id='$valor_dato';";
		//echo "$consulta_10<br>";
		$resultado_10 = mysql_query($consulta_10) or die("$consulta_10, La consulta fall&oacute;: " . mysql_error());
		while ($linea_10 = mysql_fetch_array($resultado_10, MYSQL_ASSOC))
		{ $posibilidad += $linea_10['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_11 = "select count($tabla_11.id) as total from $tabla_11 where $tabla_11.user_id='$valor_dato';";
		//echo "$consulta_11<br>";
		$resultado_11 = mysql_query($consulta_11) or die("$consulta_11, La consulta fall&oacute;: " . mysql_error());
		while ($linea_11 = mysql_fetch_array($resultado_11, MYSQL_ASSOC))
		{ $posibilidad += $linea_11['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_12 = "select count($tabla_12.id) as total from $tabla_12 where ($tabla_12.usuario_entrega_id='$valor_dato' or $tabla_12.usuario_creacion_id='$valor_dato');";
		//echo "$consulta_12<br>";
		$resultado_12 = mysql_query($consulta_12) or die("$consulta_12, La consulta fall&oacute;: " . mysql_error());
		while ($linea_12 = mysql_fetch_array($resultado_12, MYSQL_ASSOC))
		{ $posibilidad += $linea_12['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_13 = "select count($tabla_13.id) as total from $tabla_13 where $tabla_13.user_id='$valor_dato';";
		//echo "$consulta_13<br>";
		$resultado_13 = mysql_query($consulta_13) or die("$consulta_13, La consulta fall&oacute;: " . mysql_error());
		while ($linea_13 = mysql_fetch_array($resultado_13, MYSQL_ASSOC))
		{ $posibilidad += $linea_13['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_14 = "select count($tabla_14.id) as total from $tabla_14 where $tabla_14.user_id='$valor_dato';";
		//echo "$consulta_14<br>";
		$resultado_14 = mysql_query($consulta_14) or die("$consulta_14, La consulta fall&oacute;: " . mysql_error());
		while ($linea_14 = mysql_fetch_array($resultado_14, MYSQL_ASSOC))
		{ $posibilidad += $linea_14['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_15 = "select count($tabla_15.id) as total from $tabla_15 where $tabla_15.user_id='$valor_dato';";
		//echo "$consulta_15<br>";
		$resultado_15 = mysql_query($consulta_15) or die("$consulta_15, La consulta fall&oacute;: " . mysql_error());
		while ($linea_15 = mysql_fetch_array($resultado_15, MYSQL_ASSOC))
		{ $posibilidad += $linea_15['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_16 = "select count($tabla_16.id) as total from $tabla_16 where $tabla_16.user_id='$valor_dato';";
		//echo "$consulta_16<br>";
		$resultado_16 = mysql_query($consulta_16) or die("$consulta_16, La consulta fall&oacute;: " . mysql_error());
		while ($linea_16 = mysql_fetch_array($resultado_16, MYSQL_ASSOC))
		{ $posibilidad += $linea_16['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_17 = "select count($tabla_17.id) as total from $tabla_17 where $tabla_17.user_id='$valor_dato';";
		//echo "$consulta_17<br>";
		$resultado_17 = mysql_query($consulta_17) or die("$consulta_17, La consulta fall&oacute;: " . mysql_error());
		while ($linea_17 = mysql_fetch_array($resultado_17, MYSQL_ASSOC))
		{ $posibilidad += $linea_17['total']; }
	}
	if ($posibilidad == 0)
	{
		$consulta_18 = "select count($tabla_18.id) as total from $tabla_18 where $tabla_18.usuario_id='$valor_dato';";
		//echo "$consulta_18<br>";
		$resultado_18 = mysql_query($consulta_18) or die("$consulta_18, La consulta fall&oacute;: " . mysql_error());
		while ($linea_18 = mysql_fetch_array($resultado_18, MYSQL_ASSOC))
		{ $posibilidad += $linea_18['total']; }
	}
	return $posibilidad;
}

?>